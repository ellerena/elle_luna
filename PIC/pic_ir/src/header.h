/*
 * header.h
 *
 *  Created on: Feb 1, 2018
 *      Author: eddie.llerena
 */

#ifndef SRC_HEADER_H_
#define SRC_HEADER_H_

#ifndef __SDCC_PIC12F1822
#define __SDCC_PIC12F1822
#endif

#include <pic16regs.h>

/****************************** Constants ******************************/
#define LOGO            "12F1822 - v1.0\n"
//#define LOGO            "12F1822 - v1.0 " __DATE__ " " __TIME__ "\n"
#define START_BIT_H     (120)       /* min */
#define STOP_BIT_H      (250)       /* max */
#define BIT_IS_1        (35)        /* max */
#define ST_LOW          (149)       /* carrier cycles for start_low pattern */
#define ST_HIGH         (149)       /* carrier cycles for start_high pattern */
#define BIT_LOW         (18)        /* carrier cycles for bit sync low pattern */
#define BIT_HI_0        (20)        /* carrier cycles for bit high value 0 */
#define BIT_HI_1        (56)        /* carrier cycles for bit high value 1 */
#define GCOMINSZ        (12)
#define CCPR1L_VAL      (49)        /* x31 duty cycle 50% */
#define CCPR1L_OFF      (255)       /* duty cycle > 100% (carrier off) */
#define PR2_VAL         (100)       /* carrier period = 128 = 32kHz */

#define CONFIG_VAL      (_FCMEN_OFF & _IESO_OFF & _CLKOUTEN_OFF & _BOREN_OFF & _CPD_OFF & _CP_OFF & _MCLRE_OFF & _PWRTE_OFF & _WDTE_OFF & _WDTE_OFF & _FOSC_INTOSC)
#define CONFIG2_VAL     (_LVP_OFF & _BORV_LO & _STVREN_ON & _PLLEN_OFF & _WRT_OFF)
#define OPTION_REG_VAL  (0b11010110)

/******************************* Macros *******************************/
#define IR_IS_HIGH      (PORTAbits.RA3)
#define LED             (PORTAbits.RA2)
#define LED2            (PORTAbits.RA4)
#define IR_OUT          (PORTAbits.RA5)

extern void COM_puts(const char * s);
extern void COM_TXHex(uint8_t v);


#endif /* SRC_HEADER_H_ */
