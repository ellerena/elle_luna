/*
 * main.c
 *
 *  Created on: Jan 13, 2018
 *      Author: Eddie.Llerena
 */

#include <stdint.h>
#include "header.h"

#define TASK      gComIn[0]   /* variable to hold the requested 'task' */
#define MODE      gComIn[1]   /* variable to hold the 'mode' for the TASK*/
#define ARGS(n)   gComIn[n+2] /* 1st argument */

/* Setup chip configuration */
static __code uint16_t __at (_CONFIG1) __CONFIG = CONFIG_VAL;
static __code uint16_t __at (_CONFIG2) __CONFIG2 = CONFIG2_VAL;

uint8_t gComIn[GCOMINSZ];
uint8_t n;
volatile uint8_t gFlags = 0;

static void irqHandler(void) __interrupt (0)
{
   static uint8_t *pacCommand = gComIn;

   if(RCIF)                            /* if a character has been received via UART RX */
   {
      uint8_t  t;

      RCIF = 0;
      t = RCREG;
      *pacCommand++ = t;
      if (t < ' ')                     /* <return> ends the uart command entry */
      {
         pacCommand = gComIn;
         gFlags = 1;                   /* rise a flag to alert main() */
      }
   }
}

/**
   @brief   converts a string of chars representing a vector of hex numbers into a vector of bytes equivalent.
   @param   p contains the address of the first char in the string.
   @retval  the count of numbers comverted.
   @verbatim
         Text entered by a user via the keyboard is in the form of a string of printable chars that represent hex numbers.
         This functions takes that string of chars and extracts the meaningful hex numbers represented in it, then
         it places those numbers (bytes) in an array at the same ram location. Returning the number of numbers
         converted.
   @endverbatim
 */
static uint8_t ParseCharsToValueHex(char *p)
{
   char *q, d;
   uint8_t i, k;
   uint8_t c;

   q = p;
   k = 0;

   while (*p > 0x1f)                            /* Will process until a non printable character is found */
   {
      d = 0;                                    /* initialize the 'number equivalent value' */
      i = 0;                                    /* initialize the digit counter (assume 2 digits max per number)*/
      do
      {
         d <<= 4;                            /* each digit uses the first 4 bits, so on each loop we must push the others */
         c = *p++;                              /* read the new char */
         if ((c > 0x29) && (c < 0x3a))             /* char is between 0 and 9 */
         {
            d += (c - 0x30);                    /* add the char equivalen value to our total */
            i++;                             /* increment our digit counter */
         }
         if ((c > 0x60 ) && (c < 0x67))               /* char is between 0xa and 0xf */
         {
            d += (c - 0x60 + 0x9);                 /* add the char equivalen value to our total */
            i++;                             /* increment our digit counter */
         }
      } while (( i == 0 ) || ((*p > 0x2f) && (i < 2)));  /* repeat: if no char found or: if found a space and less than 2 digits */

      if(i)                                  /* if the digit counter is non-zero then a value has been found */
      {
         *q++ = d;                              /* write the number found to memory */
         k++;                                /* increment 'detected numbers counter' */
      }
   }

   return k;
}

static void IrTx(uint8_t delay, uint8_t carrier)
{
   while(delay--)
   {
      if(carrier)                /* transmit carrier */
      {
         CCPR1L = CCPR1L_VAL;
      }
      else                       /* consume time but no carrier is output */
      {
         CCPR1L = CCPR1L_OFF;
      }
      TMR2IF = 0;
      T2CON = (0b00000100);      /* TMR2: 1:1 PRE, Timer ON, 1:1 POST */
      while (!TMR2IF);
      T2CON = (0b00000000);      /* TMR2: 1:1 PRE, Timer OFF, 1:1 POST */
   }
}

static void fn_t(void)
{
   uint8_t *p;

   p = (uint8_t*)&ARGS(0);

   IrTx(ST_LOW, 1);
   IrTx(ST_HIGH, 0);

   while(n--)
   {
      unsigned i, k, v;

      v = *p++;
      for (i = 0; i < 8; i++)
      {
         IrTx(BIT_LOW, 1);
         k = (v & 0x80) ? BIT_HI_1 : BIT_HI_0;
         IrTx(k, 0);
         v <<= 1;
      }
   }
   IrTx(BIT_LOW, 1);
   IrTx(BIT_LOW, 0);
}

static void fn_c(void)
{
   unsigned t = 0;
   uint8_t in, sz, *p;

   in = 0;
   sz = 0;
   p = gComIn;

   while (IR_IS_HIGH);
   LED = 0;                            /* blink start */
   while (!IR_IS_HIGH);                /* start pulse low */
   while (IR_IS_HIGH);                 /* start pulse high */
   while (1)
   {
      while(!IR_IS_HIGH);              /* bit pulse low */
      TMR0 = 0;
      OPTION_REG = OPTION_REG_VAL;
      while(IR_IS_HIGH)                /* bit pulse high (1/0) */
      {
         if ((t = TMR0) > STOP_BIT_H)  /* if capture complete... */
         {
            sz >>= 3;                  /* get number of bytes captured */
            p = gComIn;
            while (sz--)               /* transmit each grabbed byte */
            {
              COM_TXHex(*p++);
            }
            LED = 1;                   /* blink end */
            return;
         }
      }
      in <<= 1;                        /* shift previous bits to receive next one */
      sz++;                            /* increase bit counter */
      if (t >= BIT_IS_1)               /* capture when bit is 1 */
      {
         in |= 1;
      }
      if (0 == (sz & 7))               /* if 8 bits received, make it a byte */
      {
         *p++ = in;
      }
   }
}

int main ()
{
   OSCCON = (0b01111000);             /* Oscilator 16MHz, Internal Clk */
   ANSELA = 0;
   TRISA = (0b00001010);
   BAUDCON = (0b00001000);            /* BRG16 = 1 */
   SPBRG = 103;                       /* 38400 bps */
   RCIF = 0;
   RCIE = 1;
   RCSTA = (0b10110000);              /* SPEN = 1, SREN = 1, CREN = 1 */
   TXSTA = (0b00100100);              /* BRGH  = 1; SYNC = 0, TXEN = 1 */

   TMR0IF = 0;                        /* Turn off Pending Interrupt Requests */
   OPTION_REG = OPTION_REG_VAL;       /* TMR0CS = 0 PSA = 0 PS = 101b (1:128) */
   INTCON = (0b11000000);             /* GIE, PEIE = 1; T0IE = 0 */

   CCP1CON = (0b00001111);            /* PWM standard, CCP1 active low */
   PR2 = PR2_VAL;                     /* carrier period = 128 = 32kHz */
   CCP1SEL = 1;                       /* CCP1 present in RA5 */

   LED = 1;
   COM_puts(LOGO);
   gFlags = 0;

   while(1)
   {
     COM_puts("\r\n#");
     while (!gFlags);

     n = ParseCharsToValueHex((char*)&ARGS(0));
     switch (TASK)
     {
        case 'i':
           COM_puts(LOGO);
           break;
        case 'c':          /* capture (once) strem via IR */
           fn_c();
           break;
        case 't':          /* transmit stream via IR */
           LED = 0;
           fn_t();
           LED = 1;
           break;
        case 'r':          /* read internal register value */
        case 'w':          /* write internal register value (8bit) */
        {
           uint16_t v;
           uint8_t __data *p;

           v = ARGS(0);
           v <<= 8;
           v |= ARGS(1);
           p = (uint8_t __data *)v;
           if(TASK == 'w') *p = ARGS(2);
           COM_TXHex(*p);
           LED ^= 1;
           break;
        }
        default:
           break;
     }

      gFlags &= ~1;
   }
}

//void _sdcc_gsinit_startup(void)
//{
//  __asm pagesel _main __endasm;
//  __asm goto _main __endasm;
//}

