/*
 * uart.c
 *
 *  Created on: Jan 31, 2018
 *      Author: eddie.llerena
 */

#include <stdint.h>
#include <pic16regs.h>
#include "header.h"


void COM_puts(const char * s)
{
   uint8_t c;

    do
    {
       c = *s++;
       if (c < '\n') break;
       TXREG = c;                 /* Load the transmitter buffer with the received value */
       while (!TXIF);             /* TX buffer empty? */
    } while(1);
}


void COM_TXHex(uint8_t v)
{
   uint8_t msg[5], c;
   uint8_t i= 2;

   *msg = 'x';
   *(msg+3) = ' ';
   *(msg+4) = 0;
   do
   {
      c = v & 0x0f;                         /* extract less significant nibble for processing */
      msg[i] = c < 0x0a ? c+0x30 : c+0x37;  /* convert the nibble into a printable hex */
      v>>=4;                                /* extract more significant nibble for processing */
   } while(--i);

   COM_puts(msg);                           /* send the whole thing for printing */
}

