/*
 * main.c
 *
 *  Created on: Jan 13, 2018
 *      Author: Eddie.Llerena
 */

#include <stdint.h>
#include <pic16regs.h>

/* Setup chip configuration */

static __code uint16_t __at (0x2007) __CONFIG = _INTRC_OSC_NOCLKOUT & _WDT_OFF & _PWRTE_OFF & _MCLRE_OFF & _CP_OFF & _BOR_OFF & _IESO_OFF & _FCMEN_OFF;

#define FOSC 1000000L

 void delay()
{
  int i = 0;
  volatile int k = 0;
  for (i=0;i<8000;i++) k=i;//*2.1;

}

int main ()
{

  int j = 1;

  char estados[5] = {0x8, 0xc, 0x6, 0x3, 0x1};

  TRISC0 = 0;
  TRISC1 = 0;
  TRISC2 = 0;
  TRISC3 = 0;
  TRISA3 = 1;

  ANSEL = 0;
  ANSELH = 0;

  while(1)
  {
    for (j=0;j<5;j++)
    {
       PORTC = estados[j];
       delay();
    }
    for (j=4;j>=0;j--)
    {
       PORTC = estados[j];
       delay();
    }
  }
//  return 0;
}



