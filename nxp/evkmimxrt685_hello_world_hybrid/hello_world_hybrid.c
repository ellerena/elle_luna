/*
 * Copyright 2021 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "fsl_device_registers.h"
#include "fsl_debug_console.h"

#include "pin_mux.h"
#include "clock_config.h"
#include "board.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define WELCOME "\n\nParis et Libere! - HW v1.0 - " __DATE__ " " __TIME__ "\n"

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
// void demo_func_sram(void);
// __attribute__((section(".xip_section"))) void Demo_FuncXip(void);

/*!
 * @brief XIP function.
 * Define the function which runs in external memory(XIP).
 * The XIP code can be put into ".xip_section" section.
 */
__attribute__((section(".xip_section"))) void Demo_FuncXip(void)
{
    /* Do something. Run the code in external flash(XIP). */
    for (int i = 0; i < 5; ++i)
    {
       PRINTF("%d - from XIP\n\r", i);
    }
}

void demo_func_sram(void)
{
    /* Do something. Run the code in external flash(XIP). */
    for (int i = 0; i < 5; ++i)
    {
        PRINTF("%d - from sram\r\n", i);
    }
}

/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Main function
 */
int main(void)
{
    char ch;

    /* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitDebugConsole();

    PRINTF(WELCOME);
    PRINTF("xip is @ %p, sram is @ %p\r\n", Demo_FuncXip, demo_func_sram);
    Demo_FuncXip();
    demo_func_sram();

    while (1)
    {
        ch = GETCHAR();
        PUTCHAR(ch-1);
    }
}
