/*
 * splits a 32 bit counters into 2 x 16 bit counters.
 * the lower counter is used to generate 1us pulses from a 48MHz clock.
 * the higher counter uses the same clock and counts to a multiple N of the lower
 * counter.
 * this results in N pulses being presented in GPIO_2_15.
 */

#include "fsl_debug_console.h"
#include "fsl_sctimer.h"
#include "clock_config.h"
#include "board.h"
#include "pin_mux.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define GPIO214_SCTIM8           kSCTIMER_Out_8
#define GPIO215_SCTIM9           kSCTIMER_Out_9
#define MAX_UP_COUNTER_VALUE     (0xFFFFU * 256U)
#define MAX_UPDOWN_COUNTER_VALUE (0x1FFFFU * 256U)
#define NUM_PULSES               1u

#define CONT_H_LIMIT(x) (((x) * 2) + 1)

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Code
 ******************************************************************************/
static status_t SCTIMER_Calculate16BitCounterConfig(uint64_t rawCountValue,
                                                    uint8_t *prescale,
                                                    uint16_t *matchValue,
                                                    sctimer_event_active_direction_t *activeDir)
{
    status_t status = kStatus_Success;

    if (rawCountValue < MAX_UP_COUNTER_VALUE)
    {
        *prescale   = (uint8_t)(rawCountValue / 0xFFFFU);
        *matchValue = (uint16_t)(rawCountValue / (*prescale + 1U));
        *activeDir  = kSCTIMER_ActiveIndependent;
    }
    else if (rawCountValue < MAX_UPDOWN_COUNTER_VALUE)
    {
        *prescale   = (uint8_t)(rawCountValue / 0x1FFFFU);
        *matchValue = (uint16_t)(0x1FFFFU - rawCountValue / (*prescale + 1));
        *activeDir  = kSCTIMER_ActiveInCountDown;
    }
    else
    {
        assert(1);
    }
    return status;
}

/*!
 * @brief Main function
 */
int main(void)
{
    /* Board pin, clock, debug console init */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitDebugConsole();

    /* attach FFRO clock to SCTimer input7 (48MHz / 1 = 48MHz). */
    CLOCK_AttachClk(kFFRO_to_SCT_CLK);
    CLOCK_SetClkDiv(kCLOCK_DivSctClk, 1);

    sctimer_config_t sctimerInfo;

    SCTIMER_GetDefaultConfig(&sctimerInfo);

    sctimerInfo.clockMode          = kSCTIMER_Input_ClockMode;
    sctimerInfo.clockSelect        = kSCTIMER_Clock_On_Rise_Input_7;
    sctimerInfo.enableCounterUnify = false;

    uint16_t matchValueL, matchValueH;
    sctimer_event_active_direction_t activeDirL, activeDirH;

    uint32_t sctimerClock = CLOCK_GetSctClkFreq();
    uint64_t us2countL    = USEC_TO_COUNT(1U, sctimerClock);
    uint64_t us2countH    = USEC_TO_COUNT(CONT_H_LIMIT(NUM_PULSES), sctimerClock);

    /* Calculate prescaler, match value and active direction for the 16-bit low counter for 100ms interval */
    SCTIMER_Calculate16BitCounterConfig(us2countL,
                                        &sctimerInfo.prescale_l,
                                        &matchValueL,
                                        &activeDirL);

    /* Calculate prescaler, match value and active direction for the 16-bit high counter for 200ms interval */
    SCTIMER_Calculate16BitCounterConfig(us2countH,
                                        &sctimerInfo.prescale_h,
                                        &matchValueH,
                                        &activeDirH);

    /* Enable bidirectional mode to extended 16-bit count range*/
    if (activeDirL != kSCTIMER_ActiveIndependent)
    {
        sctimerInfo.enableBidirection_l = true;
    }
    if (activeDirH != kSCTIMER_ActiveIndependent)
    {
        sctimerInfo.enableBidirection_h = true;
    }

    /* inform the present configuration */
    PRINTF("\tunify:\t%d\tsctimerClock: %u\n", sctimerInfo.enableCounterUnify, sctimerClock);
    PRINTF("\tclkmode:\t%d\tclksel:\t%d\n", sctimerInfo.clockMode, sctimerInfo.clockSelect);
    PRINTF("\tenabidirL:\t%d\tenabidirH:\t%d\n", sctimerInfo.enableBidirection_l, sctimerInfo.enableBidirection_h);
    PRINTF("\tprescalL:\t%u\tprescalH\t%u\n", sctimerInfo.prescale_l, sctimerInfo.prescale_h);
    PRINTF("\toutInitSt:\t%u\tinputSyc:\t%u\n", sctimerInfo.outInitState, sctimerInfo.inputsync);
    PRINTF("\tmatchValL:\t%u\tmatchValH:\t%u\n\n", matchValueL, matchValueH);

    /* Initialize SCTimer module */
    SCTIMER_Init(SCT0, &sctimerInfo);

    uint32_t eventCounterL;

    /* Schedule a match event for the 16-bit low counter every 0.1 seconds */
    if (SCTIMER_CreateAndScheduleEvent(SCT0,
                                        kSCTIMER_MatchEventOnly,
                                        matchValueL,
                                        0,
                                        kSCTIMER_Counter_L,
                                        &eventCounterL) == kStatus_Fail)
    {
        assert(1);
    }

    /* Toggle first output when the 16-bit low counter event occurs */
    SCTIMER_SetupOutputToggleAction(SCT0, GPIO215_SCTIM9, eventCounterL);

    /* Reset Counter L when the 16-bit low counter event occurs */
    SCTIMER_SetupCounterLimitAction(SCT0, kSCTIMER_Counter_L, eventCounterL);

    /* Setup the 16-bit low counter event active direction */
    SCTIMER_SetupEventActiveDirection(SCT0, activeDirL, eventCounterL);

    uint32_t eventCounterH;

    /* Schedule a match event for the 16-bit high counter every 0.2 seconds */
    if (SCTIMER_CreateAndScheduleEvent(SCT0,
                                        kSCTIMER_MatchEventOnly,
                                        matchValueH,
                                        0,
                                        kSCTIMER_Counter_H,
                                        &eventCounterH) == kStatus_Fail)
    {
        assert(1);
    }

    /* Setup the 16-bit high counter event active direction */
    SCTIMER_SetupEventActiveDirection(SCT0, activeDirH, eventCounterH);

    /* Stop all counters when the 16-bit high counter event occurs */
    SCTIMER_SetupCounterStopAction(SCT0, kSCTIMER_Counter_H, eventCounterH);
    SCTIMER_SetupCounterStopAction(SCT0, kSCTIMER_Counter_L, eventCounterH);

    /* Start the 16-bit low and high counter */
    SCTIMER_StartTimer(SCT0, kSCTIMER_Counter_L | kSCTIMER_Counter_H);

    while (1)
    {
    }
}
