/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file dmic_dma.h
 * @date 08.26.2023
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#pragma once

#if __cplusplus
extern "C" {
#endif

/***************************************************************************************************
 * Includes
 **************************************************************************************************/

/***************************************************************************************************
 * Macro Definitions
 **************************************************************************************************/
#define BUFF_LENGTH 1024u
#define WELCOME "\n\nParis et Libere! - DMA_M2M - " __DATE__ " " __TIME__ "\n"
#define DMA_CHANNEL 0

#define USER2_IRQ GPIO_INTA_IRQn
#define BOARD_USER2_GPIO_PORT BOARD_SW2_GPIO_PORT
#define BOARD_USER2_GPIO_PIN BOARD_SW2_GPIO_PIN
#define BOARD_LED_GPIO_PORT BOARD_LED_GREEN_GPIO_PORT
#define BOARD_LED_GPIO_PIN  BOARD_LED_GREEN_GPIO_PIN
#define BOARD_DBG1_GPIO_PORT 2
#define BOARD_DBG1_GPIO_PIN  14
#define BOARD_DBG2_GPIO_PORT 2
#define BOARD_DBG2_GPIO_PIN  15
#define BOARD_DBG1_BIT (1 << BOARD_DBG1_GPIO_PIN)
#define BOARD_DBG2_BIT (1 << BOARD_DBG2_GPIO_PIN)

#define DLEDON GPIO_PortSet(GPIO, BOARD_LED_GPIO_PORT, 1U << BOARD_LED_GPIO_PIN);
#define DLEDOFF GPIO_PortClear(GPIO, BOARD_LED_GPIO_PORT, 1U << BOARD_LED_GPIO_PIN);
#define DBG_H(bits) GPIO_PortSet(GPIO, BOARD_DBG1_GPIO_PORT, bits);
#define DBG_L(bits) GPIO_PortClear(GPIO, BOARD_DBG1_GPIO_PORT, bits);
#define DBG1_H GPIO_PortSet(GPIO, BOARD_DBG1_GPIO_PORT, 1U << BOARD_DBG1_GPIO_PIN);
#define DBG1_L GPIO_PortClear(GPIO, BOARD_DBG1_GPIO_PORT, 1U << BOARD_DBG1_GPIO_PIN);
#define DBG2_H GPIO_PortSet(GPIO, BOARD_DBG2_GPIO_PORT, 1U << BOARD_DBG2_GPIO_PIN);
#define DBG2_L GPIO_PortClear(GPIO, BOARD_DBG2_GPIO_PORT, 1U << BOARD_DBG2_GPIO_PIN);
#define DBG1N(n) \
    do \
    { \
        for (volatile int k = 0; k < n; k++) \
        { \
            DBG1_H; \
            DBG1_L; \
        } \
    } while (0)
#define DBG2N(n) \
    do \
    { \
        for (volatile int k = 0; k < n; k++) \
        { \
            DBG2_H; \
            DBG2_L; \
        } \
    } while (0)

#define DBG(n, bits) \
    do \
    { \
        for (volatile int k = 0; k < n; k++) \
        { \
            DBG_H(bits); \
            DBG_L(bits); \
        } \
    } while (0)

/***************************************************************************************************
 * Type Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Constant Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Variable Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Function Prototypes
 **************************************************************************************************/

#if __cplusplus
}
#endif