/*
 * Copyright (c) 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "pin_mux.h"
#include "clock_config.h"
#include "board.h"
#include "fsl_gpio.h"
#include "fsl_debug_console.h"
#include "fsl_dma.h"

#include "dma_memory_to_memory.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define SRC_BUF 0
#define DST_BUF1 1
#define DST_BUF2 2
#define NUM_DESCRIPTORS 4

#define GET_XFERCOUNT(x) (((x) & DMA_CHANNEL_XFERCFG_XFERCOUNT_MASK) >> DMA_CHANNEL_XFERCFG_XFERCOUNT_SHIFT)

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static void print_dma_params(void);
static void print_buffer(uint8_t buffer_id);
static void init_gpio(void);
static void init_buffers(void);
static void init_board();
static void init_user2_input(void);
static void DMA_Callback(dma_handle_t *handle, void *param, bool transferDone, uint32_t tcds);

/*******************************************************************************
 * Variables
 ******************************************************************************/
dma_handle_t g_DMA_Handle;
volatile int k = 10;
volatile bool g_Transfer_Done = false;
volatile bool g_USER2_active = false;

// generate buffers, ensure proper alignment and storage location
DMA_ALLOCATE_LINK_DESCRIPTORS(s_dma_table, NUM_DESCRIPTORS);
DMA_ALLOCATE_DATA_TRANSFER_BUFFER(uint32_t s_srcBuffer0[2 * BUFF_LENGTH + 1], sizeof(uint32_t))  = {0x0};
DMA_ALLOCATE_DATA_TRANSFER_BUFFER(uint32_t s_dstBuffer1[BUFF_LENGTH], sizeof(uint32_t))  = {0x0};
DMA_ALLOCATE_DATA_TRANSFER_BUFFER(uint32_t s_dstBuffer2[BUFF_LENGTH], sizeof(uint32_t)) = {0x0};

/*******************************************************************************
 * Code
 ******************************************************************************/

static void print_dma_params(void)
{
    PRINTF("\nsrc_array: %p, dst_array1: %p, dst_array2: %p\n", s_srcBuffer0, s_dstBuffer1, s_dstBuffer2);
    for (int i = 0; i < NUM_DESCRIPTORS; ++i)
    {
        PRINTF("descr%d: [%p] xfercfg: %08xh srcEndAddr: %p dstEndAddr: %p linkToNextDesc: %p\n",
                i,
                &s_dma_table[i],
                s_dma_table[i].xfercfg,
                s_dma_table[i].srcEndAddr,
                s_dma_table[i].dstEndAddr,
                s_dma_table[i].linkToNextDesc);
    }

    dma_descriptor_t *desc_map = (dma_descriptor_t*)DMA0->SRAMBASE;
    PRINTF("active descr%d: [%p] xfercfg: %08xh srcEndAddr: %p dstEndAddr: %p linkToNextDesc: %p\n",
                desc_map,
                desc_map[0].xfercfg,
                desc_map[0].srcEndAddr,
                desc_map[0].dstEndAddr,
                desc_map[0].linkToNextDesc);
    PRINTF("xfercount: %u\n", GET_XFERCOUNT(DMA0->CHANNEL[0].XFERCFG));
}

static void print_buffer(uint8_t buffer_id)
{
    // 0: src, 1: dst1, 2: dst2
    uint32_t * buf = SRC_BUF == buffer_id ? s_srcBuffer0 :
                     (DST_BUF1 == buffer_id ? s_dstBuffer1 : s_dstBuffer2);
    uint32_t len = SRC_BUF == buffer_id ? 2 * BUFF_LENGTH : BUFF_LENGTH;

    PRINTF("Buffer: %s:\r\n", SRC_BUF == buffer_id ? "src" :
                              DST_BUF1 == buffer_id ? "dst1" : "dst2");

    int k = 0;

    for (volatile int i = 0; i < len; i++)
    {
        PRINTF("%4x%s", buf[i], 31 == k++%32 ? "\n" : "\t");
    }

    PRINTF("\n");
}

static void init_gpio(void)
{
    CLOCK_EnableClock(kCLOCK_HsGpio0);
    CLOCK_EnableClock(kCLOCK_HsGpio2);

    RESET_PeripheralReset(kHSGPIO0_RST_SHIFT_RSTn);
    RESET_PeripheralReset(kHSGPIO2_RST_SHIFT_RSTn);

    gpio_pin_config_t led_config = { .pinDirection = kGPIO_DigitalOutput, .outputLogic = 0};
    GPIO_PinInit(GPIO, BOARD_LED_GPIO_PORT, BOARD_LED_GPIO_PIN, &led_config);
    GPIO_PinWrite(GPIO, BOARD_LED_GPIO_PORT, BOARD_LED_GPIO_PIN, LOGIC_LED_OFF);
    GPIO_PinInit(GPIO, BOARD_DBG1_GPIO_PORT, BOARD_DBG1_GPIO_PIN, &led_config);
    GPIO_PinWrite(GPIO, BOARD_DBG1_GPIO_PORT, BOARD_DBG1_GPIO_PIN, 0);
    GPIO_PinInit(GPIO, BOARD_DBG2_GPIO_PORT, BOARD_DBG2_GPIO_PIN, &led_config);
    GPIO_PinWrite(GPIO, BOARD_DBG2_GPIO_PORT, BOARD_DBG2_GPIO_PIN, 0);

    DBG(5, BOARD_DBG1_BIT + BOARD_DBG2_BIT);
}

static void init_buffers(void)
{
    PRINTF("Destination Buffer:\r\n");

    uint32_t *s_dstBuffer = s_dstBuffer1;
    volatile int i;

    for (i = 0; i < (2 * BUFF_LENGTH); ++i)
    {
        s_srcBuffer0[i] = i;
        s_dstBuffer[i] = 0;
    }

    // we add an extra value for the src buffer
    s_srcBuffer0[i] = i;
}

static void init_board()
{
    /* Board pin, clock, debug console init */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitDebugConsole();

    PRINTF(WELCOME);
}

static void init_user2_input(void)
{
    /* Define the init structure for the input switch pin */
    gpio_pin_config_t sw_config    = {kGPIO_DigitalInput, 0};
    gpio_interrupt_config_t config = {kGPIO_PinIntEnableEdge, kGPIO_PinIntEnableLowOrFall};

    EnableIRQ(USER2_IRQ);
    GPIO_PortInit(GPIO, BOARD_USER2_GPIO_PORT);
    GPIO_PinInit(GPIO, BOARD_USER2_GPIO_PORT, BOARD_USER2_GPIO_PIN, &sw_config);

    /* Enable GPIO pin interrupt */
    GPIO_SetPinInterruptConfig(GPIO, BOARD_USER2_GPIO_PORT, BOARD_USER2_GPIO_PIN, &config);
    GPIO_PinEnableInterrupt(GPIO, BOARD_USER2_GPIO_PORT, BOARD_USER2_GPIO_PIN, 0);
}

static void pause_dma(void)
{
    DMA_DisableChannel(DMA0, DMA_CHANNEL);
    while (DMA_ChannelIsBusy(DMA0, DMA_CHANNEL));
}

/* User callback function for DMA transfer. */
static void DMA_Callback(dma_handle_t *handle, void *param, bool transferDone, uint32_t tcds)
{
    DBG2N(2);
    // if (0 >= --k)
    // {
    //     g_Transfer_Done = transferDone;
    //    DMA_AbortTransfer(&g_DMA_Handle);
    // }
}

/*!
 * @brief Interrupt service fuction of switch.
 */
void GPIO_INTA_DriverIRQHandler(void)
{
    DBG1_H;

    GPIO_PinClearInterruptFlag(GPIO, BOARD_USER2_GPIO_PORT, BOARD_USER2_GPIO_PIN, 0); // clear the interrupt status

    pause_dma();

    g_USER2_active = true; // Change state of switch
    DBG1_L;
    SDK_ISR_EXIT_BARRIER;
}

/*!
 * @brief Main function
 */
int main(void)
{
    init_board();
    init_buffers();
    init_gpio();
    init_user2_input();

    DMA_Init(DMA0);
    DMA_CreateHandle(&g_DMA_Handle, DMA0, DMA_CHANNEL); // enable NVIC & channel interrupt
    DMA_EnableChannel(DMA0, DMA_CHANNEL); // enable channel @ ENABLESETn
    DMA_SetCallback(&g_DMA_Handle, DMA_Callback, NULL); // fill cb in handle only

    uint32_t xfercfg = DMA_CHANNEL_XFER(true, false, true, false, sizeof(uint32_t), kDMA_AddressInterleave1xWidth,
                                        kDMA_AddressInterleave1xWidth, sizeof s_dstBuffer1);

    DMA_SetupDescriptor(&s_dma_table[0], xfercfg, &s_srcBuffer0[0], &s_dstBuffer1[0], &s_dma_table[1]);
    DMA_SetupDescriptor(&s_dma_table[1], xfercfg, &s_srcBuffer0[BUFF_LENGTH], &s_dstBuffer2[0], &s_dma_table[2]);
    DMA_SetupDescriptor(&s_dma_table[2], xfercfg, &s_srcBuffer0[1], &s_dstBuffer1[0], &s_dma_table[3]);
    DMA_SetupDescriptor(&s_dma_table[3], xfercfg, &s_srcBuffer0[BUFF_LENGTH + 1], &s_dstBuffer2[0], &s_dma_table[0]);

    DMA_SubmitChannelDescriptor(&g_DMA_Handle, &s_dma_table[0]);

    // dma_channel_config_t transferConfig;
    // DMA_PrepareChannelTransfer(&transferConfig, s_srcBuffer0, &s_destBuffer[0], xfercfg, kDMA_MemoryToMemory, NULL, NULL);
    // DMA_SubmitChannelTransfer(&g_DMA_Handle, &transferConfig);

    print_dma_params();

    DMA_StartTransfer(&g_DMA_Handle);

    while (g_Transfer_Done != true)
    {
        /* Wait for DMA transfer finish */

        if (g_USER2_active)
        {
            g_USER2_active = false;
            PRINTF("SW2!\n");
            break;
        }
    }

    /* Print destination buffer */
    print_buffer(DST_BUF1);
    print_buffer(DST_BUF2);
    print_dma_params();

    return 0;
}
