/*
 * - peripherals used: dmic, debug console
 * - runs dmic0 and dmic1 continuously
 * - mcu waits in sleep mode
 * - when dmic0 fifo achieves a threshold either:
 * -    if USE_DMIC_IRQ is defined, a dmic interrupt is issued
 * -       - mcu then transfer the dmic fifo data to a local buffer
 * -       - mcu return to sleep
 * -    otherwise, a DMA request is issued
 * -       - DMA transfer the dmic fifo data to dma buffer
 * - once the buffer is filled, all samples are printed
 */

#include "fsl_debug_console.h"
#include "pin_mux.h"
#include "board.h"

#include "config_common.h"
#include "src/gpio.h"
#include "src/dma.h"
#include "src/dmic.h"
#include "src/input.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define APP_DEEPSLEEP_RUNCFG0 (SYSCTL0_PDSLEEPCFG0_FFRO_PD_MASK | SYSCTL0_PDSLEEPCFG0_RBB_PD_MASK)
#define APP_DEEPSLEEP_RAM_APD 0x1000U
#define APP_DEEPSLEEP_RAM_PPD 0x1000U
#define APP_EXCLUDE_FROM_DEEPSLEEP                                                         \
    (((const uint32_t[]){                                                                  \
        (SYSCTL0_PDSLEEPCFG0_FFRO_PD_MASK | SYSCTL0_PDSLEEPCFG0_RBB_PD_MASK),              \
        (SYSCTL0_PDSLEEPCFG1_SRAM_SLEEP_MASK | SYSCTL0_PDSLEEPCFG1_FLEXSPI_SRAM_APD_MASK), \
        APP_DEEPSLEEP_RAM_APD,                                                             \
        APP_DEEPSLEEP_RAM_PPD}))

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static void init_board();
static bool process_dmic_fifo_data(void);
static void print_buffer(void *buffer, uint32_t buffer_words);

static void DMA_DMIC_filler_cb(
                        DMIC_Type *base,
                        dmic_dma_handle_t *handle,
                        status_t status,
                        void *userData
                        );

static void DMA_roller_cb(
                        dma_handle_t *handle,
                        void *param,
                        bool transferDone,
                        uint32_t tcds
                        );

#ifdef USE_DMIC_IRQ
static void DMIC0_fifo_cb(void);
#endif

typedef struct __attribute__((packed))
{
    int16_t value;
    int16_t unused;
} sample_value_t;

typedef struct
{
    uint8_t dmic_fifo_has_triggered : 1;
    uint8_t dma_filled_has_triggered : 1;
    uint8_t sw1_is_pressed : 1;
    uint8_t sw2_is_pressed : 1;
    uint8_t dma_roller_slot_transferred: 1;
} trigger_flags_t;

/*******************************************************************************
 * Variables
 ******************************************************************************/
static trigger_flags_t triggers = {0};

static DMIC_DATA_TYPE audio_buf[AUDIO_SAMPLES];
static int samples_to_print = -1;
static int last_sample_printed = 0;
static uint16_t preroll_remaining_slots = 0;
static void * last_pp_buffer_filled = NULL;

/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Main function
 */
int main(void)
{
    init_board();
    init_gpio();
    init_user_input();

    PRINTF("audio  :\t%u samples @ %uHz using %u bytes\n",
           sizeof audio_buf / sizeof(audio_buf[0]),
           DMIC_SAMPLING_RATE_HZ,
           sizeof audio_buf);

    init_dmic();
    start_dmic();

#ifdef USE_DMIC_IRQ
    start_dmic_interrupt(DMIC0_fifo_cb, kDMIC_Channel0); // nvic
#else                                                    // use dma requests from dmic instead
    dma_init();
    dma_config_filler(DMA_DMIC_filler_cb, &preroll_remaining_slots);
    dma_config_roller(DMA_roller_cb, &last_pp_buffer_filled);
    dma_start_filler();
#endif

    SYSCTL0->HWWAKE = 0xC;

    for (;;)
    {
        if (triggers.dmic_fifo_has_triggered)
        {
            DBG2_H;

            /* collect the audio samples from the dmic fifo. Though our
                main purpose here is to empty the fifo so that the interrupt
                flag is cleared */
            if (process_dmic_fifo_data())
            {
                break;
            }

            triggers.dmic_fifo_has_triggered = false;
            DMIC_EnableChannelInterrupt(DMIC0, kDMIC_Channel0, true);

            DBG2_L;
        }

        if (triggers.dma_filled_has_triggered)
        {
            triggers.dma_filled_has_triggered = false;
        }

        if (triggers.dma_roller_slot_transferred)
        {
            if (preroll_remaining_slots > 0)
            {
                if (samples_to_print > 0)
                {
                    memcpy (&audio_buf[last_sample_printed],
                            last_pp_buffer_filled,
                            SLOT_SAMPLES * sizeof(DMIC_DATA_TYPE));
                    PRINTF("%p %d %d\n",
                        last_pp_buffer_filled,
                        last_sample_printed,
                        samples_to_print
                        );
                    last_sample_printed += SLOT_SAMPLES;
                    samples_to_print -= SLOT_SAMPLES;
                }

                triggers.dma_roller_slot_transferred = false;
                trigger_next_filler(NULL);
            }

            if (samples_to_print == 0)
            {
                print_buffer(audio_buf, AUDIO_SAMPLES);
                samples_to_print = -1;
            }
        }

        if (triggers.sw1_is_pressed)
        {
            PRINTF("SW1\n");
            triggers.sw1_is_pressed = false;
        }

        if (triggers.sw2_is_pressed)
        {
            samples_to_print = AUDIO_SAMPLES;
            last_sample_printed = 0;

            dma_start_roller();
            PRINTF("SW2\n");
            triggers.sw2_is_pressed = false;
        }

        // __WFI(); // go sleep and wait for interrupt
    }

    PRINTF("au revoir!");

    return 0;
}

/*******************************************************************************
 * Local Code
 ******************************************************************************/
#ifdef USE_DMIC_IRQ
static void DMIC0_fifo_cb(void)
{
    /* we should clear the flag first, however since the fifo threshold
        remains filled, the flag would be set immediately. So, instead,
        we will use an interrupt enable/disable mechanism instead */
    DMIC_EnableChannelInterrupt(DMIC0, kDMIC_Channel0, false);
    DBG1N(1);
    triggers.dmic_fifo_has_triggered = true;
}
#endif

/*!
 * @brief Interrupt service fuction of switch2.
 */
void GPIO_INTA_DriverIRQHandler(void)
{
    DBG(1, BOARD_DBG1_BIT + BOARD_DBG2_BIT);

    if (irq_is_sw1())
    {
        triggers.sw1_is_pressed = true;
        clear_interrupt_sw1();
    }

    if (irq_is_sw2())
    {
        triggers.sw2_is_pressed = true;
        clear_interrupt_sw2();
    }

    SDK_ISR_EXIT_BARRIER;
}

static void init_board()
{
    /* Board pin, clock, debug console init */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitDebugConsole();

    PRINTF(WELCOME);
}

static bool process_dmic_fifo_data(void)
{
    static int index = 0;
    DMIC_DATA_TYPE *g_rxBuffer = 0;
    dma_get_preroll_buffer((void **)&g_rxBuffer);

    for (volatile int i = 0; i <= DMIC_FIFO_DEPTH; ++i)
    {
        g_rxBuffer[index++] = DMIC0->CHANNEL[0].FIFO_DATA;

        if (index >= PREROLL_SAMPLES)
        {
            return true; // buffer is filled.
        }
    }

    return false;
}

static void print_buffer(void *buffer, uint32_t buffer_words)
{
    sample_value_t *sample = (sample_value_t *)buffer;

    for (volatile int i = 0; i < buffer_words; ++i)
    {
        PRINTF("%x%c", sample[i].value, (PRINT_WIDTH - 1) == (i) % PRINT_WIDTH ? '\n' : ' ');
    }
}

static void DMA_DMIC_filler_cb(
                        DMIC_Type *base,
                        dmic_dma_handle_t *handle,
                        status_t status,
                        void *userData
                        )
{

    // the filler triggers every time a slot has been transferred
    // so the handler will just increase the pre-roll buffer counter
    // TODO: variable must be protected against race conditions

    preroll_remaining_slots++;

    DBG1N(1);
}

static void DMA_roller_cb(dma_handle_t *handle, void *param, bool transferDone, uint32_t tcds)
{
    DBG2_L;

    preroll_remaining_slots--;

    triggers.dma_roller_slot_transferred = true;
}
