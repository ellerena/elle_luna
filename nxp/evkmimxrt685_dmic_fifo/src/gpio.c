#include "pin_mux.h"
#include "board.h"

#include "src/gpio.h"

void init_gpio(void)
{
    CLOCK_EnableClock(kCLOCK_HsGpio0);
    CLOCK_EnableClock(kCLOCK_HsGpio2);

    RESET_PeripheralReset(kHSGPIO0_RST_SHIFT_RSTn);
    RESET_PeripheralReset(kHSGPIO2_RST_SHIFT_RSTn);

    gpio_pin_config_t generic_out_pin = { .pinDirection = kGPIO_DigitalOutput, .outputLogic = 0};
    GPIO_PinInit(GPIO, BOARD_LED_GPIO_PORT, BOARD_LED_GPIO_PIN, &generic_out_pin);
    GPIO_PinWrite(GPIO, BOARD_LED_GPIO_PORT, BOARD_LED_GPIO_PIN, LOGIC_LED_OFF);
    GPIO_PinInit(GPIO, BOARD_DBG1_GPIO_PORT, BOARD_DBG1_GPIO_PIN, &generic_out_pin);
    GPIO_PinWrite(GPIO, BOARD_DBG1_GPIO_PORT, BOARD_DBG1_GPIO_PIN, 0);
    GPIO_PinInit(GPIO, BOARD_DBG2_GPIO_PORT, BOARD_DBG2_GPIO_PIN, &generic_out_pin);
    GPIO_PinWrite(GPIO, BOARD_DBG2_GPIO_PORT, BOARD_DBG2_GPIO_PIN, 0);

    DBG(5, BOARD_DBG1_BIT + BOARD_DBG2_BIT);
}
