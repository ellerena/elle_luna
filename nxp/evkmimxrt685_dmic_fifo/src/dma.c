
/***************************************************************************************************
 *
 * @file <dma.c>
 *
 * @brief dma driver and processing
 *
 * @details
 *
 **************************************************************************************************/

#include "fsl_debug_console.h"
#include "fsl_gpio.h"

#include "dma.h"

#include "src/gpio.h"

/***************************************************************************************************
 * Local Macro Definitions
 **************************************************************************************************/
#define DMA_DMIC_CHANNEL_MASK (1 << DMA_REQ_CHANNEL_DMIC_CH0)
#define GET_SLOT_NUMBER(x) (((x) - (void*)preroll_buf)/sizeof preroll_buf[0])

/***************************************************************************************************
 * Local Type Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Local Constant Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Global Variable Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Local Variable Declarations
 **************************************************************************************************/
DMA_ALLOCATE_LINK_DESCRIPTORS(dma_desc_filler, DMA_SLOTS);
DMA_ALLOCATE_LINK_DESCRIPTORS(dma_desc_roller, DMA_PINPONG_BUFFERS);
DMA_ALLOCATE_DATA_TRANSFER_BUFFER(DMIC_DATA_TYPE preroll_buf[DMA_SLOTS][SLOT_SAMPLES], sizeof(DMIC_DATA_TYPE))  = {0x0};
DMA_ALLOCATE_DATA_TRANSFER_BUFFER(DMIC_DATA_TYPE pp_buf[DMA_PINPONG_BUFFERS][SLOT_SAMPLES], sizeof(DMIC_DATA_TYPE))  = {0x0};

static dmic_dma_handle_t dmicDmaHandle;
static dma_handle_t dmaFillerHdl;
static dma_handle_t dmaRollerHdl;

static uint16_t *preroll_remaining_slots = NULL;
static void ** last_pp_buffer_filled = NULL;

/***************************************************************************************************
 * Local Function Declarations
 **************************************************************************************************/
static void dma_setup_filler_descriptors(dmic_transfer_t * dmic_dma_desc, void* filler_buffer, int slots);

/***************************************************************************************************
 * Public Function Definitions
 **************************************************************************************************/
void dma_init(void)
{
    DMA_Init(DMA_CTRLR);
    DMA_EnableChannel(DMA_CTRLR, DMA_REQ_CHANNEL_DMIC_CH0);

    /* Request dma channels from DMA manager. */
    DMA_CreateHandle(&dmaFillerHdl, DMA_CTRLR, DMA_REQ_CHANNEL_DMIC_CH0);

    // DMA_DisableChannelInterrupts(DMA_CTRLR, DMA_REQ_CHANNEL_DMIC_CH0);
    // EnableDeepSleepIRQ(DMA0_IRQn);
}

void dma_config_filler(void * DMA_filler_cb, void * userData)
{
    preroll_remaining_slots = (uint16_t*)userData;

    /* Create DMIC DMA handle associated to the DMA handle */
    DMIC_TransferCreateHandleDMA(
            DMIC0,
            &dmicDmaHandle,
            DMA_filler_cb,
            userData,
            &dmaFillerHdl);

    /* spare a space in ram for the active descriptors */
    DMIC_InstallDMADescriptorMemory(
            &dmicDmaHandle,
            &dma_desc_filler /* this is the descriptor table*/,
            DMA_SLOTS);

    /* initially keep DMA interrupt masked for the dmic channels */
    DMA_CTRLR->COMMON[0].INTENCLR = DMA_DMIC_CHANNEL_MASK;
}

void dma_config_roller(void * DMA_roller_cb, void * userData)
{
    last_pp_buffer_filled = userData;

    DMA_CreateHandle(&dmaRollerHdl, DMA_CTRLR, DMA_ROLLER_DMIC0);
    DMA_SetCallback(&dmaRollerHdl, DMA_roller_cb, userData);

    uint32_t roller_xfercfg = DMA_CHANNEL_XFER(
            true,
            true,
            true,
            false,
            sizeof(uint32_t),
            kDMA_AddressInterleave1xWidth,
            kDMA_AddressInterleave1xWidth,
            sizeof preroll_buf[0]
        );

    // setup the transfer descriptors: source addresses are not important for now so use any.
    DMA_SetupDescriptor(&dma_desc_roller[0], roller_xfercfg, preroll_buf[0], pp_buf[0], &dma_desc_roller[1]);
    DMA_SetupDescriptor(&dma_desc_roller[1], roller_xfercfg, preroll_buf[1], pp_buf[1], &dma_desc_roller[0]);
}

void dma_start_filler (void)
{
    /* we use malloc here, only to demonstrate that this variable need not be static */
    dmic_transfer_t * dmic_desc_filler = malloc (DMA_SLOTS * sizeof (dmic_transfer_t));

    dma_setup_filler_descriptors(dmic_desc_filler, preroll_buf, SLOT_SAMPLES);

    DMIC_TransferReceiveDMA(DMIC0, &dmicDmaHandle, &dmic_desc_filler[0], DMIC_CHANNEL);

    free (dmic_desc_filler);

    PRINTF("\tsrcAddr: %u\txfrcfg: %08x\n",
        (uintptr_t)preroll_buf,
        DMA_CTRLR->CHANNEL[DMA_REQ_CHANNEL_DMIC_CH0].XFERCFG);
}

void dma_start_roller(void)
{
    while (DMA_ChannelIsBusy(DMA_CTRLR, DMA_REQ_CHANNEL_DMIC_CH0))
            ;

    DMA_CTRLR->COMMON[0].INTENSET = DMA_DMIC_CHANNEL_MASK;

    dma_descriptor_t * dma_descritor = (dma_descriptor_t *)DMA_CTRLR->SRAMBASE;
    void * curr_dst_address = dma_descritor[DMA_REQ_CHANNEL_DMIC_CH0].dstEndAddr;

    trigger_next_filler(curr_dst_address);
}

void trigger_next_filler(void * p_src_end)
{
    static uint8_t nxt_pp = 0;
    static uint16_t nxt_slot = 0;

    if (p_src_end)
    {
        /* configure the first transfer from preroll to pp buffer 0.
           the source preroll slot is selected as the oldest one filled plus 1
           so we're effectively skipping the oldest to avoid any race with the
           slot currently being filled */
        nxt_slot = (GET_SLOT_NUMBER(p_src_end) + 2) % DMA_SLOTS; // note: skip 1 slot
        dma_desc_roller[0].srcEndAddr = &preroll_buf[nxt_slot++][SLOT_SAMPLES-1];

        DMA_SubmitChannelDescriptor(&dmaRollerHdl, &dma_desc_roller[0]);

        nxt_pp = 1u;

        *preroll_remaining_slots = DMA_SLOTS - 2; // assume we always start with almost full preroll
    }

    if (nxt_slot >= DMA_SLOTS)
    {
        nxt_slot = 0;
    }

    dma_desc_roller[1 & nxt_pp++].srcEndAddr = &preroll_buf[nxt_slot++][SLOT_SAMPLES-1];

    *last_pp_buffer_filled = (void*)pp_buf[nxt_pp & 1];

    DBG2_H;

    DMA_StartTransfer(&dmaRollerHdl);
}

uint32_t dma_get_preroll_buffer(void ** pbuffer)
{
    if (pbuffer)
    {
        *pbuffer = (void*) preroll_buf;
    }

    return sizeof preroll_buf;
}

uint32_t dma_get_pp_buffer(void ** pbuffer)
{
    if (pbuffer)
    {
        *pbuffer = (void*) pp_buf;
    }

    return sizeof pp_buf[0] / sizeof pp_buf[0][0];
}

/***************************************************************************************************
 * Local Function Definitions
 **************************************************************************************************/
static void dma_setup_filler_descriptors(dmic_transfer_t * dmic_dma_desc, void* filler_buffer, int slot_samples)
{
    PRINTF("preroll:\t%u samples, %u bytes @ %p\n%d pp buffers of %u samples each @ %p, %p\n",
            sizeof preroll_buf / sizeof (preroll_buf[0][0]),
            sizeof preroll_buf,
            preroll_buf,
            DMA_PINPONG_BUFFERS,
            sizeof pp_buf[0] / sizeof pp_buf[0][0],
            pp_buf[0],
            pp_buf[1]
            );

    int slot_size = sizeof (DMIC_DATA_TYPE) * slot_samples;

    for (int i = 0; i < DMA_SLOTS; ++i)
    {
        dmic_dma_desc[i].dataSize               = slot_size,
        dmic_dma_desc[i].data                   = filler_buffer + (slot_size * i),
        dmic_dma_desc[i].dataAddrInterleaveSize = kDMA_AddressInterleave1xWidth,
        dmic_dma_desc[i].dataWidth              = sizeof(DMIC_DATA_TYPE),
        dmic_dma_desc[i].linkTransfer           = &dmic_dma_desc[(i+1)%DMA_SLOTS];

        PRINTF("\t%4d dataSize: %u\tdatawidth: %u\tdataintl: %u\tlnkNxt: %p\tdata: %p\n",
            i,
            dmic_dma_desc[i].dataSize,
            dmic_dma_desc[i].dataWidth,
            dmic_dma_desc[i].dataAddrInterleaveSize,
            dmic_dma_desc[i].linkTransfer,
            dmic_dma_desc[i].data);
    }
}

