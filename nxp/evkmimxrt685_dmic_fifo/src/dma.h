/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file dma.h
 * @date 08.27.2023
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#pragma once

#if __cplusplus
extern "C" {
#endif

/***************************************************************************************************
 * Includes
 **************************************************************************************************/
#include "../config_common.h"
#include "fsl_dmic.h"
#include "fsl_dmic_dma.h"
#include "fsl_dma.h"

/***************************************************************************************************
 * Macro Definitions
 **************************************************************************************************/
#define DMIC_CHANNEL kDMIC_Channel0
#define DMA_ROLLER_DMIC0 0

#if (1 == FSL_FEATURE_DMIC_CHANNEL_HAS_SIGNEXTEND)
#define DMIC_DATA_TYPE int
#else
#define DMIC_DATA_TYPE uint16_t
#endif

/***************************************************************************************************
 * Type Definitions
 **************************************************************************************************/
typedef enum {
    DMA_REQ_CHANNEL_DMIC_CH0 = 16u,
    DMA_REQ_CHANNEL_DMIC_CH1,
    DMA_REQ_CHANNEL_DMIC_CH2,
    DMA_REQ_CHANNEL_DMIC_CH3,
    DMA_REQ_CHANNEL_DMIC_CH4,
    DMA_REQ_CHANNEL_DMIC_CH5,
    DMA_REQ_CHANNEL_DMIC_CH6,
    DMA_REQ_CHANNEL_DMIC_CH7
} dma_request_channels_dmics_t;

/***************************************************************************************************
 * Constant Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Variable Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Function Prototypes
 **************************************************************************************************/
void dma_init(void);
void dma_config_filler(void * DMA_filler_cb, void * userData);
void dma_config_roller(void * DMA_roller_cb, void * userData);
void dma_start_filler (void);
void dma_start_roller(void);
void trigger_next_filler(void * p_src_end);

uint32_t dma_get_preroll_buffer(void ** pbuffer);
uint32_t dma_get_pp_buffer(void ** pbuffer);
void dma_start_roller(void);

#if __cplusplus
}
#endif