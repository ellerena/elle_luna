
#include "fsl_dmic.h"

#include "dmic.h"

void init_dmic(void)
{
    // Attach SFRO to DMIC_CLK (16MHz)
    CLOCK_AttachClk(kSFRO_to_DMIC_CLK);
    // set the Dmic Clk Divider: 16MHz / 20 = 800KHz (for 16kHz sampling)
    CLOCK_SetClkDiv(kCLOCK_DivDmicClk, 20);

    dmic_channel_config_t dmic_channel_cfg = {
#if defined(FSL_FEATURE_DMIC_CHANNEL_HAS_SIGNEXTEND) && (FSL_FEATURE_DMIC_CHANNEL_HAS_SIGNEXTEND)
        .enableSignExtend = false,
#endif
        .divhfclk            = kDMIC_PdmDiv1,
        .osr                 = 25U,
        .gainshft            = 3U,
        .preac2coef          = kDMIC_CompValueZero,
        .preac4coef          = kDMIC_CompValueZero,
        .dc_cut_level        = kDMIC_DcCut155,
        .post_dc_gain_reduce = 0U,
        .saturate16bit       = 1U,
        .sample_rate         = kDMIC_PhyFullSpeed,
    };

    DMIC_Init(DMIC_CTRLR);
    DMIC_Use2fs(DMIC_CTRLR, true);

    DMIC_ConfigChannel(DMIC_CTRLR, kDMIC_Channel0, kDMIC_Left, &dmic_channel_cfg);
    DMIC_ConfigChannel(DMIC_CTRLR, kDMIC_Channel1, kDMIC_Right, &dmic_channel_cfg);
    DMIC_FifoChannel(DMIC_CTRLR, kDMIC_Channel0, DMIC_FIFO_DEPTH, true, true);
    DMIC_FifoChannel(DMIC_CTRLR, kDMIC_Channel1, DMIC_FIFO_DEPTH, true, true);
}

void start_dmic(void)
{
    // start running the dmics
    // DisableIRQ(DMIC0_IRQn);
    DMIC_EnableChannnel(DMIC_CTRLR, (DMIC_CHANEN_EN_CH0(1) /*| DMIC_CHANEN_EN_CH1(1)*/));
}

void start_dmic_interrupt(void * user_callback, uint8_t dmic_channel)
{
    DMIC_EnableChannelInterrupt(DMIC_CTRLR, dmic_channel, true);
    DMIC_EnableIntCallback(DMIC_CTRLR, user_callback); // nvic
}

void start_dmic_dma_requests(uint8_t dmic_channel)
{
    DMIC_EnableChannelDma(DMIC_CTRLR, dmic_channel, true); // DMIC_CTRLR fifo_ctrl[dmic_channel].DMAEN
}
