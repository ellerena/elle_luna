#include <fsl_gpio.h>

void init_user_input(void);
void clear_interrupt_sw1(void);
void clear_interrupt_sw2(void);
bool irq_is_sw1(void);
bool irq_is_sw2(void);
