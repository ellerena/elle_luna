#define BOARD_LED_GPIO_PORT BOARD_LED_GREEN_GPIO_PORT
#define BOARD_LED_GPIO_PIN  BOARD_LED_GREEN_GPIO_PIN
#define BOARD_DBG1_GPIO_PORT 2
#define BOARD_DBG1_GPIO_PIN  14
#define BOARD_DBG2_GPIO_PORT 2
#define BOARD_DBG2_GPIO_PIN  15
#define BOARD_DBG1_BIT (1 << BOARD_DBG1_GPIO_PIN)
#define BOARD_DBG2_BIT (1 << BOARD_DBG2_GPIO_PIN)

#define DLEDON GPIO_PortSet(GPIO, BOARD_LED_GPIO_PORT, 1U << BOARD_LED_GPIO_PIN);
#define DLEDOFF GPIO_PortClear(GPIO, BOARD_LED_GPIO_PORT, 1U << BOARD_LED_GPIO_PIN);
#define DBG_H(bits) GPIO_PortSet(GPIO, BOARD_DBG1_GPIO_PORT, bits);
#define DBG_L(bits) GPIO_PortClear(GPIO, BOARD_DBG1_GPIO_PORT, bits);
#define DBG1_H GPIO_PortSet(GPIO, BOARD_DBG1_GPIO_PORT, 1U << BOARD_DBG1_GPIO_PIN);
#define DBG1_L GPIO_PortClear(GPIO, BOARD_DBG1_GPIO_PORT, 1U << BOARD_DBG1_GPIO_PIN);
#define DBG2_H GPIO_PortSet(GPIO, BOARD_DBG2_GPIO_PORT, 1U << BOARD_DBG2_GPIO_PIN);
#define DBG2_L GPIO_PortClear(GPIO, BOARD_DBG2_GPIO_PORT, 1U << BOARD_DBG2_GPIO_PIN);
#define DBG1N(n) \
    do \
    { \
        for (volatile int k = 0; k < n; k++) \
        { \
            DBG1_H; \
            DBG1_L; \
        } \
    } while (0)
#define DBG2N(n) \
    do \
    { \
        for (volatile int k = 0; k < n; k++) \
        { \
            DBG2_H; \
            DBG2_L; \
        } \
    } while (0)

#define DBG(n, bits) \
    do \
    { \
        for (volatile int k = 0; k < n; k++) \
        { \
            DBG_H(bits); \
            DBG_L(bits); \
        } \
    } while (0)


extern void init_gpio(void);
