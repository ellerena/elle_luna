/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file dmic_hwvad.h
 * @date 08.27.2023
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#pragma once

#if __cplusplus
extern "C" {
#endif

/***************************************************************************************************
 * Includes
 **************************************************************************************************/

/***************************************************************************************************
 * Macro Definitions
 **************************************************************************************************/
#define WELCOME "\n\nParis et Libere! - FIFO - " __DATE__ " " __TIME__ "\n"

// #define USE_DMIC_IRQ

#define DMIC_CTRLR DMIC0
#define DMIC_SAMPLING_RATE_HZ (16000)
#define DMIC_MSEC_TO_SAMPLES(m) (m * (DMIC_SAMPLING_RATE_HZ/1000))

#define PREROLL_MS (2000) // desired: 200 ms samples per mic
#define SLOT_MS (8u) // 64ms is max @ 16kHz.
#define AUDIO_MS (5600u) // total audio sample recorded for testing

#define PREROLL_SAMPLES DMIC_MSEC_TO_SAMPLES(PREROLL_MS) //
#define AUDIO_SAMPLES DMIC_MSEC_TO_SAMPLES(AUDIO_MS)
#define SLOT_SAMPLES DMIC_MSEC_TO_SAMPLES(SLOT_MS) // 128
#define DMA_SLOTS (PREROLL_MS/SLOT_MS) // division must be even integer 2000/8 = 250

#define PRINT_WIDTH (1)
#define DMIC_FIFO_DEPTH 15U
#define DMA_PINPONG_BUFFERS (2u)

#define DMA_CTRLR DMA1

/***************************************************************************************************
 * Type Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Constant Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Variable Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Function Prototypes
 **************************************************************************************************/


#if __cplusplus
}
#endif