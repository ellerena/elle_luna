#include <fsl_dmic.h>

#define FIFO_DEPTH 15U

typedef enum {
    DMIC_CHANNEL0 = kDMIC_Channel0,
    DMIC_CHANNEL1,
    DMIC_CHANNEL2,
    DMIC_CHANNEL3,
    DMIC_CHANNEL4,
    DMIC_CHANNEL5,
    DMIC_CHANNEL6,
    DMIC_CHANNEL7
} dmic_channel_index_t;

extern void init_dmic(uint8_t pair);
extern void start_dmic(void);
extern void start_dmic_interrupt(void * user_callback, uint8_t dmic_channel);
