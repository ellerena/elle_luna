#include "fsl_dmic.h"
#include "fsl_dmic_dma.h"
#include "fsl_debug_console.h"

#include "dma.h"

#define GET_XFERCOUNT(x) (((x) & DMA_CHANNEL_XFERCFG_XFERCOUNT_MASK) >> DMA_CHANNEL_XFERCFG_XFERCOUNT_SHIFT)

/*******************************************************************************
 * Variables
 ******************************************************************************/
static dma_handle_t dmaRxHandle;
static dmic_dma_handle_t dmicDmaHandle;

// generate buffers, ensure proper alignment and storage location
DMA_ALLOCATE_LINK_DESCRIPTORS(descriptor, NUM_DESCRIPTORS);
DMA_ALLOCATE_DATA_TRANSFER_BUFFER(DMIC_DATA_TYPE s_dstBuffer1[2*BUFF_LENGTH], sizeof(DMIC_DATA_TYPE))  = {0x0};

static DMIC_DATA_TYPE * s_dstBuffer2 = s_dstBuffer1 + BUFF_LENGTH;

static dmic_transfer_t xfer_decriptor[NUM_DESCRIPTORS] = {
    {
        .dataSize = (sizeof s_dstBuffer1) / 2,
        .data     = s_dstBuffer1,
        .dataAddrInterleaveSize = kDMA_AddressInterleave1xWidth,
        .dataWidth = sizeof(DMIC_DATA_TYPE),
        .linkTransfer = &xfer_decriptor[1]
    },
    {
        .dataSize = (sizeof s_dstBuffer1) / 2,
        .data     = s_dstBuffer1 + BUFF_LENGTH,
        .dataAddrInterleaveSize = kDMA_AddressInterleave1xWidth,
        .dataWidth = sizeof(DMIC_DATA_TYPE),
        .linkTransfer = &xfer_decriptor[0]
    }
};

/*******************************************************************************
 * Code
 ******************************************************************************/
void init_dma(void * user_callback, void * p_dma_fixed_source)
{
    DMA_Init(DMA_CTRLR);
    DMA_EnableChannel(DMA_CTRLR, DMA_REQ_CHANNEL_DMIC_CH0);

    // enable interrupt in DMA channel and NVIC
    DMA_CreateHandle(&dmaRxHandle, DMA_CTRLR, DMA_REQ_CHANNEL_DMIC_CH0);

    /* Create DMIC DMA handle. */
    DMIC_TransferCreateHandleDMA(DMIC0,
                                    &dmicDmaHandle,
                                    user_callback,
                                    NULL,
                                    &dmaRxHandle);

    DMIC_InstallDMADescriptorMemory(&dmicDmaHandle, &descriptor[0], 2U);

    // // configure a user_callback to execute when dma full transfer is complete
    // if (user_callback)
    // {
    //     DMA_SetCallback(&dmaRxHandle, user_callback, NULL);
    // }

    // uint32_t xfercfg = DMA_CHANNEL_XFER(
    //                     true, // reload
    //                     false, // clrTrig
    //                     true, // intA
    //                     false, // intB
    //                     sizeof(DMIC_DATA_TYPE), // width
    //                     kDMA_AddressInterleave0xWidth, //srcInc
    //                     kDMA_AddressInterleave1xWidth, // dstInc
    //                     sizeof s_dstBuffer1 // bytes
    //                     );

    // // configure the descriptor, use dual buffer in ping-pong mode
    // DMA_SetupDescriptor(&descriptor[0], xfercfg, p_dma_fixed_source, &s_dstBuffer1[0], &descriptor[1]);
    // DMA_SetupDescriptor(&descriptor[1], xfercfg, p_dma_fixed_source, &s_dstBuffer2[0], &descriptor[0]);
    // DMA_SubmitChannelDescriptor(&dmaRxHandle, &descriptor[0]);

    // dma_channel_config_t transferConfig;
    // DMA_PrepareChannelTransfer(&transferConfig, p_dma_fixed_source, &s_dstBuffer1[0], xfercfg, kDMA_PeripheralToMemory, NULL, &descriptor[1]);
    // DMA_SubmitChannelTransfer(&dmaRxHandle, &transferConfig);
}

void start_dma(void)
{
    DMIC_TransferReceiveDMA(DMIC0,
                            &dmicDmaHandle,
                            &xfer_decriptor[0],
                            kDMIC_Channel0);

    // DMA_StartTransfer(&dmaRxHandle);
}

void print_dma_params(void)
{
    PRINTF("\ndst_array1: %p, dst_array2: %p\n", s_dstBuffer1, s_dstBuffer2);
    for (int i = 0; i < NUM_DESCRIPTORS; ++i)
    {
        PRINTF("descr%d: [%p] xfercfg: %p srcEndAddr: %p dstEndAddr: %p linkToNextDesc: %p\n",
                i,
                &descriptor[i],
                descriptor[i].xfercfg,
                descriptor[i].srcEndAddr,
                descriptor[i].dstEndAddr,
                descriptor[i].linkToNextDesc);
    }

    dma_descriptor_t *desc_map = (dma_descriptor_t*)DMA_CTRLR->SRAMBASE;
    PRINTF("active: [%p] xfercfg: %p srcEndAddr: %p dstEndAddr: %p linkToNextDesc: %p\n",
                desc_map,
                desc_map[DMA_REQ_CHANNEL_DMIC_CH0].xfercfg,
                desc_map[DMA_REQ_CHANNEL_DMIC_CH0].srcEndAddr,
                desc_map[DMA_REQ_CHANNEL_DMIC_CH0].dstEndAddr,
                desc_map[DMA_REQ_CHANNEL_DMIC_CH0].linkToNextDesc);

    int remain = (GET_XFERCOUNT(DMA_CTRLR->CHANNEL[DMA_REQ_CHANNEL_DMIC_CH0].XFERCFG) + 1);
    DMIC_DATA_TYPE * buff_start = (DMIC_DATA_TYPE *)desc_map[DMA_REQ_CHANNEL_DMIC_CH0].dstEndAddr - (BUFF_LENGTH - 1);

    PRINTF("xfercount: %p, buffer: %p, processed: %d, remain: %d\n",
            DMA_CTRLR->CHANNEL[DMA_REQ_CHANNEL_DMIC_CH0].XFERCFG,
            buff_start,
            BUFF_LENGTH - remain,
            remain);
}

void print_buffer(uint8_t buffer_id)
{
    // 0: src, 1: dst1, 2: dst2
    DMIC_DATA_TYPE * buf = (DST_BUF1 == buffer_id ? s_dstBuffer1 : s_dstBuffer2);
    uint32_t len = SRC_BUF == buffer_id ? 2 * BUFF_LENGTH : BUFF_LENGTH;

    PRINTF("Buffer: %s:\r\n", SRC_BUF == buffer_id ? "src" :
                              DST_BUF1 == buffer_id ? "dst1" : "dst2");

    int k = 0;

    for (volatile int i = 0; i < len; i++)
    {
        PRINTF("%4x%s", buf[i], 31 == k++%32 ? "\n" : "\t");
    }

    PRINTF("\n");
}

void init_buffers(void)
{
    DMIC_DATA_TYPE *s_dstBuffer = s_dstBuffer1;
    volatile int i;

    for (i = 0; i < (2 * BUFF_LENGTH); ++i)
    {
        // s_srcBuffer0[i] = i;
        s_dstBuffer[i] = 0;
    }
}

void pause_dma(void)
{
    while (DMA_ChannelIsBusy(DMA0, DMA_REQ_CHANNEL_DMIC_CH0));
    DMA_DisableChannel(DMA0, DMA_REQ_CHANNEL_DMIC_CH0);
    while (DMA_ChannelIsBusy(DMA0, DMA_REQ_CHANNEL_DMIC_CH0));
}

void dma_enable_dmic_irq(uint32_t channel_mask, bool enable)
{
    if (enable)
    {
        DMA0->COMMON[0].INTENSET = channel_mask;
    }
    else
    {
        DMA0->COMMON[0].INTENCLR = channel_mask;
    }
}


