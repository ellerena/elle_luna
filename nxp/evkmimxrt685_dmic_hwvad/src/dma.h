#include <fsl_dma.h>

#define DMA_CTRLR DMA0
#define NUM_DESCRIPTORS 2
#define SRC_BUF 0
#define DST_BUF1 1
#define DST_BUF2 2

#define BUFF_LENGTH (24 * (1 + 15)) // 384 words
#if (1 == FSL_FEATURE_DMIC_CHANNEL_HAS_SIGNEXTEND)
#define DMIC_DATA_TYPE int
#else
#define DMIC_DATA_TYPE uint16_t
#endif

typedef enum {
    DMA_REQ_CHANNEL_DMIC_CH0 = 16,
    DMA_REQ_CHANNEL_DMIC_CH1,
    DMA_REQ_CHANNEL_DMIC_CH2,
    DMA_REQ_CHANNEL_DMIC_CH3,
    DMA_REQ_CHANNEL_DMIC_CH4,
    DMA_REQ_CHANNEL_DMIC_CH5,
    DMA_REQ_CHANNEL_DMIC_CH6,
    DMA_REQ_CHANNEL_DMIC_CH7
} dma_request_channels_dmics_t;

void init_dma(void * user_callback, void * p_dma_fixed_source);
void start_dma(void);
void print_dma_params(void);
void print_buffer(uint8_t buffer_id);
void init_buffers(void);
void pause_dma(void);
void dma_enable_dmic_irq(uint32_t channel_mask, bool enable);
