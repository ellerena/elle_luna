#include "dmic.h"

#define DMIC_CHANNEL_FLAG(x) (1 << x)
#define DMIC_CTRLR DMIC0

static uint8_t dmic_channel_mask = 0;

void init_dmic(uint8_t pair)
{
    CLOCK_AttachClk(kSFRO_to_DMIC_CLK); // 16MHz SFRO clk
    CLOCK_SetClkDiv(kCLOCK_DivDmicClk, 20); // 16MHz / 20 = 800 KHz for 16kHz sampling

    dmic_channel_config_t dmic_channel_cfg = {
#if defined(FSL_FEATURE_DMIC_CHANNEL_HAS_SIGNEXTEND) && (FSL_FEATURE_DMIC_CHANNEL_HAS_SIGNEXTEND)
        .enableSignExtend = false,
#endif
        .divhfclk            = kDMIC_PdmDiv1,
        .osr                 = 25U,
        .gainshft            = 3U,
        .preac2coef          = kDMIC_CompValueZero,
        .preac4coef          = kDMIC_CompValueZero,
        .dc_cut_level        = kDMIC_DcCut155,
        .post_dc_gain_reduce = 0U,
        .saturate16bit       = 1U,
        .sample_rate         = kDMIC_PhyFullSpeed,
    };

    DMIC_Init(DMIC_CTRLR);
    DMIC_Use2fs(DMIC_CTRLR, true);

    dmic_channel_t dmicChanL = kDMIC_Channel0 + pair * 2;
    dmic_channel_t dmicChanR = kDMIC_Channel0 + dmicChanL + 1;
    dmic_channel_mask |= DMIC_CHANNEL_FLAG(dmicChanL) + DMIC_CHANNEL_FLAG(dmicChanR);

    // DMIC_CTRLR->Channel[0].fifo_ctrl.INTEN enable interrupt on fifo level
    // DMIC_EnableChannelInterrupt(DMIC_CTRLR, dmicChanL, true);
    // DMIC_EnableChannelInterrupt(DMIC_CTRLR, dmicChanR, true);

    // DMIC_CTRLR->Channel[0].fifo_ctrl.DMAEN enable DMA request on fifo level
    DMIC_EnableChannelDma(DMIC_CTRLR, dmicChanL, true);
    // DMIC_EnableChannelDma(DMIC_CTRLR, dmicChanR, true);

    DMIC_ConfigChannel(DMIC_CTRLR, dmicChanL, kDMIC_Left, &dmic_channel_cfg);
    DMIC_ConfigChannel(DMIC_CTRLR, dmicChanR, kDMIC_Right, &dmic_channel_cfg);

    // DMIC_CTRLR->Channel[i].fifo_ctrl: trig.Level, enable_channel, reset_channel
    DMIC_FifoChannel(DMIC_CTRLR, dmicChanL, FIFO_DEPTH, true, true);
    DMIC_FifoChannel(DMIC_CTRLR, dmicChanR, FIFO_DEPTH, true, true);
}

void start_dmic(void)
{
    // start running the dmics
    // DisableIRQ(DMIC0_IRQn);
    DMIC_EnableChannnel(DMIC_CTRLR, dmic_channel_mask);
}

void start_dmic_interrupt(void * user_callback, uint8_t dmic_channel)
{
    DMIC_EnableChannelInterrupt(DMIC_CTRLR, dmic_channel, true);
    DMIC_EnableIntCallback(DMIC_CTRLR, user_callback); // nvic
}
