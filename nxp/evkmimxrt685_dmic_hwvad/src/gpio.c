#include <fsl_gpio.h>

#include "pin_mux.h"
#include "board.h"

#include "gpio.h"

void init_gpio(void)
{
    // initialize (enable clock and reset) some gpio ports
    GPIO_PortInit(GPIO, 0);
    GPIO_PortInit(GPIO, 1);
    GPIO_PortInit(GPIO, 2);

    gpio_pin_config_t led_config = { .pinDirection = kGPIO_DigitalOutput, .outputLogic = 0};
    GPIO_PinInit(GPIO, BOARD_LED_GPIO_PORT, BOARD_LED_GPIO_PIN, &led_config);
    GPIO_PinWrite(GPIO, BOARD_LED_GPIO_PORT, BOARD_LED_GPIO_PIN, LOGIC_LED_OFF);
    GPIO_PinInit(GPIO, BOARD_DBG1_GPIO_PORT, BOARD_DBG1_GPIO_PIN, &led_config);
    GPIO_PinWrite(GPIO, BOARD_DBG1_GPIO_PORT, BOARD_DBG1_GPIO_PIN, 0);
    GPIO_PinInit(GPIO, BOARD_DBG2_GPIO_PORT, BOARD_DBG2_GPIO_PIN, &led_config);
    GPIO_PinWrite(GPIO, BOARD_DBG2_GPIO_PORT, BOARD_DBG2_GPIO_PIN, 0);

    DBG(5, BOARD_DBG1_BIT + BOARD_DBG2_BIT);
}
