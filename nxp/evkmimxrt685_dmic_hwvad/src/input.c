#include "pin_mux.h"
#include "board.h"

#include "input.h"

#define SW1_IRQ GPIO_INTA_IRQn
#define SW1_IDX kGPIO_InterruptA
#define SW2_IRQ GPIO_INTA_IRQn
#define SW2_IDX kGPIO_InterruptA

/*
    Note: the code below assumes that some gpio features are already configured
            by the gpio.c module
*/

void init_user_input(void)
{
    /* Define the init structure for the input switch pin */
    gpio_pin_config_t user_sw_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0
    };

    // Ports should be already enabled by the gpio module
    GPIO_PinInit(GPIO, BOARD_SW1_GPIO_PORT, BOARD_SW1_GPIO_PIN, &user_sw_config);
    GPIO_PinInit(GPIO, BOARD_SW2_GPIO_PORT, BOARD_SW2_GPIO_PIN, &user_sw_config);

    /* Enable GPIO pin interrupt */
    gpio_interrupt_config_t config = {
        kGPIO_PinIntEnableEdge,
        kGPIO_PinIntEnableLowOrFall
    };
    GPIO_SetPinInterruptConfig(GPIO, BOARD_SW1_GPIO_PORT, BOARD_SW1_GPIO_PIN, &config);
    GPIO_SetPinInterruptConfig(GPIO, BOARD_SW2_GPIO_PORT, BOARD_SW2_GPIO_PIN, &config);
    GPIO_PinEnableInterrupt(GPIO, BOARD_SW1_GPIO_PORT, BOARD_SW1_GPIO_PIN, SW1_IDX);
    GPIO_PinEnableInterrupt(GPIO, BOARD_SW2_GPIO_PORT, BOARD_SW2_GPIO_PIN, SW2_IDX);
    EnableIRQ(SW1_IRQ);
    EnableIRQ(SW2_IRQ);
}

void clear_interrupt_sw1(void)
{
    GPIO_PinClearInterruptFlag(GPIO, BOARD_SW1_GPIO_PORT, BOARD_SW1_GPIO_PIN, SW1_IDX); // clear the interrupt status
}

void clear_interrupt_sw2(void)
{
    GPIO_PinClearInterruptFlag(GPIO, BOARD_SW2_GPIO_PORT, BOARD_SW2_GPIO_PIN, SW2_IDX); // clear the interrupt status
}

bool irq_is_sw1(void)
{
    uint32_t status = GPIO_PortGetInterruptStatus(GPIO, BOARD_SW1_GPIO_PORT, SW1_IDX);
    uint32_t pin_mask = (1 << BOARD_SW1_GPIO_PIN);

    return (pin_mask == (pin_mask & status));
}

bool irq_is_sw2(void)
{
    uint32_t status = GPIO_PortGetInterruptStatus(GPIO, BOARD_SW2_GPIO_PORT, SW2_IDX);
    uint32_t pin_mask = (1 << BOARD_SW2_GPIO_PIN);

    return (pin_mask == (pin_mask & status));
}
