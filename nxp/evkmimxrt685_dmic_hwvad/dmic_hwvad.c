/*
 * Copyright 2016, NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <fsl_debug_console.h>
#include <fsl_power.h>
#include "pin_mux.h"
#include "board.h"

#include "dmic_hwvad.h"
#include "src/dma.h"
#include "src/dmic.h"
#include "src/gpio.h"
#include "src/input.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define FIFO_DEPTH 15U
#define HWVAD_RESET_LOOPS_US 20 // 20us
#define HWVAD_CONVERGE_LOOPS_US 20000 // 20ms
#define HWVAD_GAIN 4
#define HWVAD_THGS 1
#define HWVAD_THGN 2
#define HWVAD_HPFS 1
#define DMIC_PAIR01 (0)
#define DMIC_PAIR23 (1)
#define DMIC_PAIR45 (2)
#define DMIC_PAIR67 (3)

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static void hwvad_toggle_st10(int pulse_width_us);
static void hwvad_reset_filters(void);
static void hwvad_set_params(void);
static void init_board(void);
static void init_hwvad(void);
static void start_hwvad(void);
static void config_inputmux(void);
static void util_empty_dmic_fifo(void);

// cb to run when hwvad triggers
static void DMIC0_HWVAD_Callback(void);

// cb to run when dmic fifo level is reached
static void DMIC0_fifo_cb(void);

// cb to run when dma transfer is complete
static void DMA_buffer_full_cb(dma_handle_t *handle, void *param, bool transferDone, uint32_t tcds);

/*******************************************************************************
 * Variables
 ******************************************************************************/
bool hwvad_has_triggered = false;
bool dmic_fifo_trigger_level_reached = false;
bool sw1_is_pressed = false;
bool sw2_is_pressed = false;

/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Main function
 */
int main(void)
{
    init_board();
    init_gpio();
    init_user_input();
    config_inputmux();
    init_dmic(DMIC_PAIR01);
    init_dma(DMA_buffer_full_cb, (void*)&DMIC0->CHANNEL[0].FIFO_DATA);
    // init_hwvad();

    start_dmic_interrupt(DMIC0_fifo_cb, DMIC_CHANNEL0); // nvic
    // EnableDeepSleepIRQ(HWVAD0_IRQn);
    // EnableDeepSleepIRQ(DMIC0_IRQn);
    // EnableDeepSleepIRQ(DMA0_IRQn);

    start_dma();
    start_dmic();
    // start_hwvad();

    while (1)
    {
        if (hwvad_has_triggered)
        {
            // PRINTF("Going into sleep\n");
            DisableIRQ(HWVAD0_IRQn);
            DBG2N(2);
            hwvad_has_triggered = false;
            EnableIRQ(HWVAD0_IRQn);
        }
        if (dmic_fifo_trigger_level_reached)
        {
            DBG1_H;

            // util_empty_dmic_fifo();

            dmic_fifo_trigger_level_reached = false;

            EnableIRQ(DMIC0_IRQn);
            DMIC_EnableChannelInterrupt(DMIC0, kDMIC_Channel0, true);

            DBG1_L;

            // DMIC_FifoClearStatus(DMIC0, kDMIC_Channel0, 0x7);
        }
        if (sw1_is_pressed || sw2_is_pressed || hwvad_has_triggered)
        {
            if (sw1_is_pressed)
            {
                dma_enable_dmic_irq(3 << 16, true);
                PRINTF("SW1:");
                sw1_is_pressed = false;
            }

            if (sw2_is_pressed)
            {
                // pause/resume dma interrupt
                static bool enable_dma = false;
                dma_enable_dmic_irq(3 << 16, enable_dma);
                enable_dma = !enable_dma;
                sw2_is_pressed = false;
                PRINTF("SW2: dma irq: %s", enable_dma ? "disabled" : "enabled");
            }

            print_dma_params();
            hwvad_has_triggered = false;
        }
        // DBG2N(2);
        // __WFI(); // go sleep and wait for interrupt
        // if (hwvad_has_triggered)

        //     SDK_DelayAtLeastUs(900000, SystemCoreClock); // simulate some long proccessing time
        // }
        // if (dmic_fifo_trigger_level_reached)
        // {
        // }

    }
}

/*!
 * @brief Interrupt service fuction of switch2.
 */
void GPIO_INTA_DriverIRQHandler(void)
{
    DBG2N(9);

    if (irq_is_sw1())
    {
        pause_dma();
        sw1_is_pressed = true;
        clear_interrupt_sw1();
    }

    if (irq_is_sw2())
    {
        sw2_is_pressed = true;
        clear_interrupt_sw2();
    }

    SDK_ISR_EXIT_BARRIER;
}

/*!
 * @brief Interrupt service fuction of switch1.
 */
void GPIO_INTB_DriverIRQHandler(void)
{
    DBG2N(7);

    clear_interrupt_sw1();
    pause_dma();

    sw1_is_pressed = true; // Change state of switch

    SDK_ISR_EXIT_BARRIER;
}

/*******************************************************************************
 * Local Code
 ******************************************************************************/
static void hwvad_toggle_st10(int pulse_width_us)
{
    // DBG2_H;
    DMIC_CtrlClrIntrHwvad(DMIC0, true);

    SDK_DelayAtLeastUs(pulse_width_us, SystemCoreClock);

    DMIC_CtrlClrIntrHwvad(DMIC0, false);
    // DBG2_L;
}

static void hwvad_reset_filters(void)
{
    // DBG2_H;

    DMIC_FilterResetHwvad(DMIC0, true); // toggle flag high
    DMIC_FilterResetHwvad(DMIC0, false); // toggle flag low

    // DBG2_L;
}

static void hwvad_set_params(void)
{
    DMIC_SetGainNoiseEstHwvad(DMIC0, HWVAD_THGN); /* thgn */
    DMIC_SetGainSignalEstHwvad(DMIC0, HWVAD_THGS); /* thgs */
    DMIC_SetFilterCtrlHwvad(DMIC0, HWVAD_HPFS); /* 0 = by-pass 1st filter, 1 = hpf_shifter:1, 2 = hpf_shifter:4 */
    DMIC_SetInputGainHwvad(DMIC0, HWVAD_GAIN); /* gain/shift */
}

static void init_board(void)
{
    /* Board pin, clock, debug console init */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitDebugConsole();

    PRINTF(WELCOME);
}

static void init_hwvad(void)
{
    hwvad_set_params();
    DisableDeepSleepIRQ(HWVAD0_IRQn);
    DisableIRQ(HWVAD0_IRQn);
}

static void start_hwvad(void)
{
    hwvad_reset_filters();
    hwvad_toggle_st10(HWVAD_CONVERGE_LOOPS_US);
}

static void config_inputmux(void)
{
    CLOCK_EnableClock(kCLOCK_InputMux); // next commands require access to the input mux
}

static void DMIC0_HWVAD_Callback(void)
{
    DBG1_H;

    DMIC_CtrlClrIntrHwvad(DMIC0, true); /* reset hwvad internal interrupt */
    for (volatile int i = 0; i <= 500U; i++);/* wait for HWVAD to settle */
    DMIC_CtrlClrIntrHwvad(DMIC0, false); /* HWVAD Normal operation */
    hwvad_has_triggered = true;

    DBG1_L;
}

static void DMIC0_fifo_cb(void)
{
    DBG1N(1);
    DBG1_H;
    DMIC_EnableChannelInterrupt(DMIC0, kDMIC_Channel0, false); // fifo_ctrl0
    // DMIC_FifoClearStatus(DMIC0, kDMIC_Channel0, 0x7);
    DisableIRQ(DMIC0_IRQn); // clear nvic irq

    dmic_fifo_trigger_level_reached = true;
    DBG1_L;
}

/* User callback function for DMA transfer. */
static void DMA_buffer_full_cb(dma_handle_t *handle, void *param, bool transferDone, uint32_t tcds)
{
    DBG2N(1);
    // if (0 >= --k)
    // {
    //     g_Transfer_Done = transferDone;
    //    DMA_AbortTransfer(&g_DMA_Handle);
    // }
}

static void util_empty_dmic_fifo(void)
{
    uint32_t k = 0;
    for (volatile int i = 0; i < 16; ++i)
    {
        k += DMIC0->CHANNEL[0].FIFO_DATA;
    }
}
