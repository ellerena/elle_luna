/*
 * Copyright 2016, NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "fsl_debug_console.h"
#include "pin_mux.h"
#include "board.h"
#include "fsl_dmic.h"
#include "fsl_dma.h"
#include "fsl_dmic_dma.h"

#include "dmic_dma.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

#define APP_DMIC_CHANNEL        kDMIC_Channel0
#define APP_DMIC_CHANNEL_ENABLE DMIC_CHANEN_EN_CH0(1)
#define FIFO_DEPTH    15U
#define BUFF_LENGTH (15 * (1 + FIFO_DEPTH))
#if (1 == FSL_FEATURE_DMIC_CHANNEL_HAS_SIGNEXTEND)
#define DMIC_DATA_TYPE int
#else
#define DMIC_DATA_TYPE uint16_t
#endif

/*******************************************************************************
 * Variables
 ******************************************************************************/
DMA_ALLOCATE_LINK_DESCRIPTORS(s_dma_table, 2);
DMA_ALLOCATE_DATA_TRANSFER_BUFFER(DMIC_DATA_TYPE g_rxBuffer[BUFF_LENGTH], sizeof(DMIC_DATA_TYPE))  = {0x0};

dmic_dma_handle_t g_dmicDmaHandle;
dma_handle_t g_dmicRxDmaHandle;
volatile bool g_Transfer_Done = false;

static dmic_transfer_t receiveXfer[2] = {
    {
        .dataSize               = sizeof g_rxBuffer,
        .data                   = &g_rxBuffer[0],
        .dataAddrInterleaveSize = kDMA_AddressInterleave1xWidth,
        .dataWidth              = sizeof(DMIC_DATA_TYPE),
        .linkTransfer           = &receiveXfer[0]
    },
    {
        .dataSize               = sizeof g_rxBuffer,
        .data                   = &g_rxBuffer[0],
        .dataAddrInterleaveSize = kDMA_AddressInterleave1xWidth,
        .dataWidth              = sizeof(DMIC_DATA_TYPE),
        .linkTransfer           = &receiveXfer[0]
    }
    };

/*******************************************************************************
 * Local Functions
 ******************************************************************************/
/* DMIC user callback */
static void DMIC_UserCallback(DMIC_Type *base, dmic_dma_handle_t *handle, status_t status, void *userData)
{
    DBG1_H;
    userData = userData;
    // if (status == kStatus_DMIC_Idle)
    {
        g_Transfer_Done = true;
    }
    DBG1_L;
}

static void DMIC_cb (void)
{
    DBG2_H;
    DMIC_EnableIntCallback(DMIC0, DMIC_cb);
    DBG2_L;
}

static void dmic_init(void)
{
    CLOCK_AttachClk(kSFRO_to_DMIC_CLK); /* DMIC uses 16MHz SFRO clock */
    CLOCK_SetClkDiv(kCLOCK_DivDmicClk, 20); /* 16MHz / 20 = 800 KHz PDM = 16kHz */

    dmic_channel_config_t dmic_channel_cfg = {
    #if (1 == FSL_FEATURE_DMIC_CHANNEL_HAS_SIGNEXTEND)
        .enableSignExtend = true,
    #endif
        .divhfclk            = kDMIC_PdmDiv1,
        .osr                 = 25U,
        .gainshft            = 2U,
        .preac2coef          = kDMIC_CompValueZero,
        .preac4coef          = kDMIC_CompValueZero,
        .dc_cut_level        = kDMIC_DcCut155,
        .post_dc_gain_reduce = 1,
        .saturate16bit       = 1U,
        .sample_rate         = kDMIC_PhyFullSpeed
    };

    DMIC_Init(DMIC0);
    DMIC_Use2fs(DMIC0, true);

    DMIC_EnableChannelDma(DMIC0, APP_DMIC_CHANNEL, true); // DMIC0 fifo_ctrl[0].DMAEN
#if defined(BOARD_DMIC_CHANNEL_STEREO_SIDE_SWAP) && (BOARD_DMIC_CHANNEL_STEREO_SIDE_SWAP)
    DMIC_ConfigChannel(DMIC0, APP_DMIC_CHANNEL, kDMIC_Right, &dmic_channel_cfg);
#else
    DMIC_ConfigChannel(DMIC0, APP_DMIC_CHANNEL, kDMIC_Left, &dmic_channel_cfg);
#endif

    DMIC_FifoChannel(DMIC0, APP_DMIC_CHANNEL, FIFO_DEPTH, true, true); // config fdmic fifo

    DMIC_EnableChannnel(DMIC0, APP_DMIC_CHANNEL_ENABLE); // DMIC0 fifo_ctrl[0].Enable
}

static void gpio_init(void)
{
    CLOCK_EnableClock(kCLOCK_HsGpio0);
    CLOCK_EnableClock(kCLOCK_HsGpio2);

    RESET_PeripheralReset(kHSGPIO0_RST_SHIFT_RSTn);
    RESET_PeripheralReset(kHSGPIO2_RST_SHIFT_RSTn);

    gpio_pin_config_t generic_out_pin = { .pinDirection = kGPIO_DigitalOutput, .outputLogic = 0};
    GPIO_PinInit(GPIO, BOARD_LED_GPIO_PORT, BOARD_LED_GPIO_PIN, &generic_out_pin);
    GPIO_PinWrite(GPIO, BOARD_LED_GPIO_PORT, BOARD_LED_GPIO_PIN, LOGIC_LED_OFF);
    GPIO_PinInit(GPIO, BOARD_DBG1_GPIO_PORT, BOARD_DBG1_GPIO_PIN, &generic_out_pin);
    GPIO_PinWrite(GPIO, BOARD_DBG1_GPIO_PORT, BOARD_DBG1_GPIO_PIN, 0);
    GPIO_PinInit(GPIO, BOARD_DBG2_GPIO_PORT, BOARD_DBG2_GPIO_PIN, &generic_out_pin);
    GPIO_PinWrite(GPIO, BOARD_DBG2_GPIO_PORT, BOARD_DBG2_GPIO_PIN, 0);

    DBG(5, BOARD_DBG1_BIT + BOARD_DBG2_BIT);
}

static void init_board()
{
    /* Board pin, clock, debug console init */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitDebugConsole();

    PRINTF(WELCOME);
}

static void init_buffers(void)
{
    PRINTF("Buffer Data before transfer:\n");
    for (int i = 0; i < BUFF_LENGTH; i++)
    {
        g_rxBuffer[i] = (i + 1);
        PRINTF("%d, ", g_rxBuffer[i]);
    }
    PRINTF("\n");
}

/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Main function
 */
int main(void)
{
    init_board();

    CLOCK_EnableClock(kCLOCK_InputMux);

    gpio_init();
    dmic_init();
    init_buffers();

    PRINTF("Configure DMA\n");

    DMA_Init(DMA0);
    DMA_EnableChannel(DMA0, DMAREQ_DMIC0);

    /* Request dma channels from DMA manager. */
    DMA_CreateHandle(&g_dmicRxDmaHandle, DMA0, DMAREQ_DMIC0);

    /* Create DMIC DMA handle. */
    DMIC_TransferCreateHandleDMA(DMIC0, &g_dmicDmaHandle, DMIC_UserCallback, NULL, &g_dmicRxDmaHandle);

    // DMIC_InstallDMADescriptorMemory(&g_dmicDmaHandle, &s_dma_table[0], 2U);

    DBG1N(1);
    DBG1_H;
    DMIC_TransferReceiveDMA(DMIC0, &g_dmicDmaHandle, &receiveXfer[0], APP_DMIC_CHANNEL);
    DBG1_L;

    /* Wait for DMA transfer finish */
    while (g_Transfer_Done == false)
    {
    }
    DBG1N(3);

    PRINTF("\nTransfer completed, Buffer Data after transfer:\n");
    for (int i = 0; i < BUFF_LENGTH; i++)
    {
        PRINTF("%x, ", g_rxBuffer[i]);
    }
    PRINTF("\nAll done!\n");
    while (1)
    {
    }
}
