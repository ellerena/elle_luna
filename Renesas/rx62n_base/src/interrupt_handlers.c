/***********************************************************************/
/*  													               */
/*      PROJECT NAME :  test                                           */
/*      FILE         :  interrupt_handlers.c                           */
/*      DESCRIPTION  :  Interrupt Handler                              */
/*      CPU SERIES   :  RX600                                          */
/*      CPU TYPE     :  RX62T                                          */
/*  													               */
/*  													               */
/***********************************************************************/                                                                      




#include "interrupt_handlers.h"

// Exception(Supervisor Instruction)
void INT_Excep_SuperVisorInst(void){/* brk(); */}

// Exception(Undefined Instruction)
void INT_Excep_UndefinedInst(void){/* brk(); */}

// Exception(Floating Point)
void INT_Excep_FloatingPoint(void){/* brk(); */}

// NMI
void INT_NonMaskableInterrupt(void){/* brk(); */}

// Dummy
void Dummy(void){/* brk(); */}

// BRK
void INT_Excep_BRK(void){ /*wait(); */}

//Reserved  0 0000h

void INT_Excep_BUSERR(void) { }

void INT_Excep_FCU_FCUERR(void) { }

void INT_Excep_FCU_FRDYI(void) { }

void INT_Excep_ICU_SWINT(void) { }

void INT_Excep_CMTU0_CMI0(void) { }

void INT_Excep_CMTU0_CMI1(void) { }

void INT_Excep_CMTU1_CMI2(void) { }

void INT_Excep_CMTU1_CMI3(void) { }

void INT_Excep_ETHER_EINT(void) { }

void INT_Excep_USB0_D0FIFO0(void) { }

void INT_Excep_USB0_D1FIFO0(void) { }

void INT_Excep_USB0_USBI0(void) { }

void INT_Excep_USB1_D0FIFO1(void) { }

void INT_Excep_USB1_D1FIFO1(void) { }

void INT_Excep_USB1_USBI1(void) { }

void INT_Excep_RSPI0_SPEIO(void) { }

void INT_Excep_RSPI0_SPRI0(void) { }

void INT_Excep_RSPI0_SPTI0(void) { }

void INT_Excep_RSPI0_SPII0(void) { }

void INT_Excep_RSPI1_SPEI1(void) { }

void INT_Excep_RSPI1_SPRI1(void) { }

void INT_Excep_RSPI1_SPTI1(void) { }

void INT_Excep_RSPI1_SPII1(void) { }

void INT_Excep_CAN0_ERS0(void) { }

void INT_Excep_CAN0_RXF0(void) { }

void INT_Excep_CAN0_TXF0(void) { }

void INT_Excep_CAN0_RXM0(void) { }

void INT_Excep_CAN0_TXM0(void) { }

void INT_Excep_RTC_PRD(void) { }

void INT_Excep_RTC_CUP(void) { }

void INT_Excep_IRQ0(void) { }

void INT_Excep_IRQ1(void) { }

void INT_Excep_IRQ2(void) { }

void INT_Excep_IRQ3(void) { }

void INT_Excep_IRQ4(void) { }

void INT_Excep_IRQ5(void) { }

void INT_Excep_IRQ6(void) { }

void INT_Excep_IRQ7(void) { }

void INT_Excep_IRQ8(void) { }

void INT_Excep_IRQ9(void) { }

void INT_Excep_IRQ10(void) { }

void INT_Excep_IRQ11(void) { }

void INT_Excep_IRQ12(void) { }

void INT_Excep_IRQ13(void) { }

void INT_Excep_IRQ14(void) { }

void INT_Excep_IRQ15(void) { }

void INT_Excep_USBR_USBR0(void) { }

void INT_Excep_USBR_USBR1(void) { }

void INT_Excep_RTC_ALM(void) { }

void INT_Excep_WDT_WOVI(void) { }

void INT_Excep_AD0_ADI0(void) { }

void INT_Excep_AD1_ADI1(void) { }

void INT_Excep_S12AD_ADI12_0(void) { }

void INT_Excep_MTU0_TGIA0(void) { }

void INT_Excep_MTU0_TGIB0(void) { }

void INT_Excep_MTU0_TGICO(void) { }

void INT_Excep_MTU0_TGID0(void) { }

void INT_Excep_MTU0_TCIV0(void) { }

void INT_Excep_MTU0_TGIE0(void) { }

void INT_Excep_MTU0_TGIF0(void) { }

void INT_Excep_MTU1_TGIA1(void) { }

void INT_Excep_MTU1_TGIB1(void) { }

void INT_Excep_MTU1_TCIV1(void) { }

void INT_Excep_MTU1_TCIU1(void) { }

void INT_Excep_MTU2_TGIA2(void) { }

void INT_Excep_MTU2_TGIB2(void) { }

void INT_Excep_MTU2_TCIV2(void) { }

void INT_Excep_MTU2_TCIU2(void) { }

void INT_Excep_MTU3_TGIA3(void) { }

void INT_Excep_MTU3_TGIB3(void) { }

void INT_Excep_MTU3_TGIC3(void) { }

void INT_Excep_MTU3_TGID3(void) { }

void INT_Excep_MTU3_TCIV3(void) { }

void INT_Excep_MTU4_TGIA4(void) { }

void INT_Excep_MTU4_TGIB4(void) { }

void INT_Excep_MTU4_TGIC4(void) { }

void INT_Excep_MTU4_TGID4(void) { }

void INT_Excep_MTU4_TCIV4(void) { }

void INT_Excep_MTU5_TGIU5(void) { }

void INT_Excep_MTU5_TGIV5(void) { }

void INT_Excep_MTU5_TGIW5(void) { }

void INT_Excep_MTU6_TGIA6(void) { }

void INT_Excep_MTU6_TGIB6(void) { }

void INT_Excep_MTU6_TGIC6(void) { }

void INT_Excep_MTU6_TGID6(void) { }

void INT_Excep_MTU6_TCIV6(void) { }

void INT_Excep_MTU6_TGIE6(void) { }

void INT_Excep_MTU6_TGIF6(void) { }

void INT_Excep_MTU7_TGIA7(void) { }

void INT_Excep_MTU7_TGIB7(void) { }

void INT_Excep_MTU7_TCIV7(void) { }

void INT_Excep_MTU7_TCIU7(void) { }

void INT_Excep_MTU8_TGIA8(void) { }

void INT_Excep_MTU8_TGIB8(void) { }

void INT_Excep_MTU8_TCIV8(void) { }

void INT_Excep_MTU8_TCIU8(void) { }

void INT_Excep_MTU9_TGIA9(void) { }

void INT_Excep_MTU9_TGIB9(void) { }

void INT_Excep_MTU9_TGIC9(void) { }

void INT_Excep_MTU9_TGID9(void) { }

void INT_Excep_MTU9_TCIV9(void) { }

void INT_Excep_MTU10_TGIA10(void) { }

void INT_Excep_MTU10_TGIB10(void) { }

void INT_Excep_MTU10_TGIC10(void) { }

void INT_Excep_MTU10_TGID10(void) { }

void INT_Excep_MTU10_TCIV10(void) { }

void INT_Excep_MTU11_TGIU11(void) { }

void INT_Excep_MTU11_TGIV11(void) { }

void INT_Excep_MTU11_TGIW11(void) { }

void INT_Excep_POE_OEI1(void) { }

void INT_Excep_POE_OEI2(void) { }

void INT_Excep_POE_OEI3(void) { }

void INT_Excep_POE_OEI4(void) { }

void INT_Excep_TMR0_CMIA0(void) { }

void INT_Excep_TMR0_CMIB0(void) { }

void INT_Excep_TMR0_OVI0(void) { }

void INT_Excep_TMR1_CMIA1(void) { }

void INT_Excep_TMR1_CMIB1(void) { }

void INT_Excep_TMR1_OVI1(void) { }

void INT_Excep_TMR2_CMIA2(void) { }

void INT_Excep_TMR2_CMIB2(void) { }

void INT_Excep_TMR2_OVI2(void) { }

void INT_Excep_TMR3_CMIA3(void) { }

void INT_Excep_TMR3_CMIB3(void) { }

void INT_Excep_TMR3_OVI3(void) { }

void INT_Excep_DMACA_DMAC0I(void) { }

void INT_Excep_DMACA_DMAC1I(void) { }

void INT_Excep_DMACA_DMAC2I(void) { }

void INT_Excep_DMACA_DMAC3I(void) { }

void INT_Excep_EXDMAC_EXDMAC_0I(void) { }

void INT_Excep_EXDMAC_EXDMAC_1I(void) { }

void INT_Excep_SCI0_ERI0(void) { }

void INT_Excep_SCI0_RXI0(void) { }

void INT_Excep_SCI0_TXI0(void) { }

void INT_Excep_SCI0_TEI0(void) { }

void INT_Excep_SCI1_ERI1(void) { }

void INT_Excep_SCI1_RXI1(void) { }

void INT_Excep_SCI1_TXI1(void) { }

void INT_Excep_SCI1_TEI1(void) { }

void INT_Excep_SCI2_ERI2(void) { }

void INT_Excep_SCI2_RXI2(void) { }

void INT_Excep_SCI2_TXI2(void) { }

void INT_Excep_SCI2_TEI2(void) { }

void INT_Excep_SCI3_ERI3(void) { }

void INT_Excep_SCI3_RXI3(void) { }

void INT_Excep_SCI3_TXI3(void) { }

void INT_Excep_SCI3_TEI3(void) { }

void INT_Excep_SCI5_ERI5(void) { }

void INT_Excep_SCI5_RXI5(void) { }

void INT_Excep_SCI5_TXI5(void) { }

void INT_Excep_SCI5_TEI5(void) { }

void INT_Excep_SCI6_ERI6(void) { }

void INT_Excep_SCI6_RXI6(void) { }

void INT_Excep_SCI6_TXI6(void) { }

void INT_Excep_SCI6_TEI6(void) { }

void INT_Excep_RIIC0_ICEEI0(void) { }

void INT_Excep_RIIC0_ICRXI0(void) { }

void INT_Excep_RIIC0_ICTXI0(void) { }

void INT_Excep_RIIC0_ICTEI0(void) { }

void INT_Excep_RIIC1_ICEEI1(void) { }

void INT_Excep_RIIC1_ICRXI1(void) { }

void INT_Excep_RIIC1_ICTXI1(void) { }

void INT_Excep_RIIC1_ICTEI1(void) { }


