/***********************************************************************/
/*  													               */
/*      PROJECT NAME :  test                                           */
/*      FILE         :  interrupt_handlers.h                           */
/*      DESCRIPTION  :  Interrupt Handler Declarations                 */
/*      CPU SERIES   :  RX600                                          */
/*      CPU TYPE     :  RX62T                                          */
/*  													               */
/*  													               */
/***********************************************************************/   
                                                                           





#ifndef INTERRUPT_HANDLERS_H
#define INTERRUPT_HANDLERS_H

// Exception(Supervisor Instruction)
void INT_Excep_SuperVisorInst(void)__attribute__ ((interrupt));

// Exception(Undefined Instruction)
void INT_Excep_UndefinedInst(void)__attribute__ ((interrupt));

// Exception(Floating Point)
void INT_Excep_FloatingPoint(void)__attribute__ ((interrupt));

// NMI
void INT_NonMaskableInterrupt(void)__attribute__ ((interrupt));

// Dummy
void Dummy(void)__attribute__ ((interrupt));

// BRK
void INT_Excep_BRK(void)__attribute__ ((interrupt));

void INT_Excep_BUSERR(void)__attribute__ ((interrupt));
void INT_Excep_FCU_FCUERR(void)__attribute__ ((interrupt));
void INT_Excep_FCU_FRDYI(void)__attribute__ ((interrupt));
void INT_Excep_ICU_SWINT(void)__attribute__ ((interrupt));
void INT_Excep_CMTU0_CMI0(void)__attribute__ ((interrupt));
void INT_Excep_CMTU0_CMI1(void)__attribute__ ((interrupt));
void INT_Excep_CMTU1_CMI2(void)__attribute__ ((interrupt));
void INT_Excep_CMTU1_CMI3(void)__attribute__ ((interrupt));
void INT_Excep_ETHER_EINT(void)__attribute__ ((interrupt));
void INT_Excep_USB0_D0FIFO0(void)__attribute__ ((interrupt));
void INT_Excep_USB0_D1FIFO0(void)__attribute__ ((interrupt));
void INT_Excep_USB0_USBI0(void)__attribute__ ((interrupt));
void INT_Excep_USB1_D0FIFO1(void)__attribute__ ((interrupt));
void INT_Excep_USB1_D1FIFO1(void)__attribute__ ((interrupt));
void INT_Excep_USB1_USBI1(void)__attribute__ ((interrupt));
void INT_Excep_RSPI0_SPEIO(void)__attribute__ ((interrupt));
void INT_Excep_RSPI0_SPRI0(void)__attribute__ ((interrupt));
void INT_Excep_RSPI0_SPTI0(void)__attribute__ ((interrupt));
void INT_Excep_RSPI0_SPII0(void)__attribute__ ((interrupt));
void INT_Excep_RSPI1_SPEI1(void)__attribute__ ((interrupt));
void INT_Excep_RSPI1_SPRI1(void)__attribute__ ((interrupt));
void INT_Excep_RSPI1_SPTI1(void)__attribute__ ((interrupt));
void INT_Excep_RSPI1_SPII1(void)__attribute__ ((interrupt));
void INT_Excep_CAN0_ERS0(void)__attribute__ ((interrupt));
void INT_Excep_CAN0_RXF0(void)__attribute__ ((interrupt));
void INT_Excep_CAN0_TXF0(void)__attribute__ ((interrupt));
void INT_Excep_CAN0_RXM0(void)__attribute__ ((interrupt));
void INT_Excep_CAN0_TXM0(void)__attribute__ ((interrupt));
void INT_Excep_RTC_PRD(void)__attribute__ ((interrupt));
void INT_Excep_RTC_CUP(void)__attribute__ ((interrupt));
void INT_Excep_IRQ0(void)__attribute__ ((interrupt));
void INT_Excep_IRQ1(void)__attribute__ ((interrupt));
void INT_Excep_IRQ2(void)__attribute__ ((interrupt));
void INT_Excep_IRQ3(void)__attribute__ ((interrupt));
void INT_Excep_IRQ4(void)__attribute__ ((interrupt));
void INT_Excep_IRQ5(void)__attribute__ ((interrupt));
void INT_Excep_IRQ6(void)__attribute__ ((interrupt));
void INT_Excep_IRQ7(void)__attribute__ ((interrupt));
void INT_Excep_IRQ8(void)__attribute__ ((interrupt));
void INT_Excep_IRQ9(void)__attribute__ ((interrupt));
void INT_Excep_IRQ10(void)__attribute__ ((interrupt));
void INT_Excep_IRQ11(void)__attribute__ ((interrupt));
void INT_Excep_IRQ12(void)__attribute__ ((interrupt));
void INT_Excep_IRQ13(void)__attribute__ ((interrupt));
void INT_Excep_IRQ14(void)__attribute__ ((interrupt));
void INT_Excep_IRQ15(void)__attribute__ ((interrupt));
void INT_Excep_USBR_USBR0(void)__attribute__ ((interrupt));
void INT_Excep_USBR_USBR1(void)__attribute__ ((interrupt));
void INT_Excep_RTC_ALM(void)__attribute__ ((interrupt));
void INT_Excep_WDT_WOVI(void)__attribute__ ((interrupt));
void INT_Excep_AD0_ADI0(void)__attribute__ ((interrupt));
void INT_Excep_AD1_ADI1(void)__attribute__ ((interrupt));
void INT_Excep_S12AD_ADI12_0(void)__attribute__ ((interrupt));
void INT_Excep_MTU0_TGIA0(void)__attribute__ ((interrupt));
void INT_Excep_MTU0_TGIB0(void)__attribute__ ((interrupt));
void INT_Excep_MTU0_TGICO(void)__attribute__ ((interrupt));
void INT_Excep_MTU0_TGID0(void)__attribute__ ((interrupt));
void INT_Excep_MTU0_TCIV0(void)__attribute__ ((interrupt));
void INT_Excep_MTU0_TGIE0(void)__attribute__ ((interrupt));
void INT_Excep_MTU0_TGIF0(void)__attribute__ ((interrupt));
void INT_Excep_MTU1_TGIA1(void)__attribute__ ((interrupt));
void INT_Excep_MTU1_TGIB1(void)__attribute__ ((interrupt));
void INT_Excep_MTU1_TCIV1(void)__attribute__ ((interrupt));
void INT_Excep_MTU1_TCIU1(void)__attribute__ ((interrupt));
void INT_Excep_MTU2_TGIA2(void)__attribute__ ((interrupt));
void INT_Excep_MTU2_TGIB2(void)__attribute__ ((interrupt));
void INT_Excep_MTU2_TCIV2(void)__attribute__ ((interrupt));
void INT_Excep_MTU2_TCIU2(void)__attribute__ ((interrupt));
void INT_Excep_MTU3_TGIA3(void)__attribute__ ((interrupt));
void INT_Excep_MTU3_TGIB3(void)__attribute__ ((interrupt));
void INT_Excep_MTU3_TGIC3(void)__attribute__ ((interrupt));
void INT_Excep_MTU3_TGID3(void)__attribute__ ((interrupt));
void INT_Excep_MTU3_TCIV3(void)__attribute__ ((interrupt));
void INT_Excep_MTU4_TGIA4(void)__attribute__ ((interrupt));
void INT_Excep_MTU4_TGIB4(void)__attribute__ ((interrupt));
void INT_Excep_MTU4_TGIC4(void)__attribute__ ((interrupt));
void INT_Excep_MTU4_TGID4(void)__attribute__ ((interrupt));
void INT_Excep_MTU4_TCIV4(void)__attribute__ ((interrupt));
void INT_Excep_MTU5_TGIU5(void)__attribute__ ((interrupt));
void INT_Excep_MTU5_TGIV5(void)__attribute__ ((interrupt));
void INT_Excep_MTU5_TGIW5(void)__attribute__ ((interrupt));
void INT_Excep_MTU6_TGIA6(void)__attribute__ ((interrupt));
void INT_Excep_MTU6_TGIB6(void)__attribute__ ((interrupt));
void INT_Excep_MTU6_TGIC6(void)__attribute__ ((interrupt));
void INT_Excep_MTU6_TGID6(void)__attribute__ ((interrupt));
void INT_Excep_MTU6_TCIV6(void)__attribute__ ((interrupt));
void INT_Excep_MTU6_TGIE6(void)__attribute__ ((interrupt));
void INT_Excep_MTU6_TGIF6(void)__attribute__ ((interrupt));
void INT_Excep_MTU7_TGIA7(void)__attribute__ ((interrupt));
void INT_Excep_MTU7_TGIB7(void)__attribute__ ((interrupt));
void INT_Excep_MTU7_TCIV7(void)__attribute__ ((interrupt));
void INT_Excep_MTU7_TCIU7(void)__attribute__ ((interrupt));
void INT_Excep_MTU8_TGIA8(void)__attribute__ ((interrupt));
void INT_Excep_MTU8_TGIB8(void)__attribute__ ((interrupt));
void INT_Excep_MTU8_TCIV8(void)__attribute__ ((interrupt));
void INT_Excep_MTU8_TCIU8(void)__attribute__ ((interrupt));
void INT_Excep_MTU9_TGIA9(void)__attribute__ ((interrupt));
void INT_Excep_MTU9_TGIB9(void)__attribute__ ((interrupt));
void INT_Excep_MTU9_TGIC9(void)__attribute__ ((interrupt));
void INT_Excep_MTU9_TGID9(void)__attribute__ ((interrupt));
void INT_Excep_MTU9_TCIV9(void)__attribute__ ((interrupt));
void INT_Excep_MTU10_TGIA10(void)__attribute__ ((interrupt));
void INT_Excep_MTU10_TGIB10(void)__attribute__ ((interrupt));
void INT_Excep_MTU10_TGIC10(void)__attribute__ ((interrupt));
void INT_Excep_MTU10_TGID10(void)__attribute__ ((interrupt));
void INT_Excep_MTU10_TCIV10(void)__attribute__ ((interrupt));
void INT_Excep_MTU11_TGIU11(void)__attribute__ ((interrupt));
void INT_Excep_MTU11_TGIV11(void)__attribute__ ((interrupt));
void INT_Excep_MTU11_TGIW11(void)__attribute__ ((interrupt));
void INT_Excep_POE_OEI1(void)__attribute__ ((interrupt));
void INT_Excep_POE_OEI2(void)__attribute__ ((interrupt));
void INT_Excep_POE_OEI3(void)__attribute__ ((interrupt));
void INT_Excep_POE_OEI4(void)__attribute__ ((interrupt));
void INT_Excep_TMR0_CMIA0(void)__attribute__ ((interrupt));
void INT_Excep_TMR0_CMIB0(void)__attribute__ ((interrupt));
void INT_Excep_TMR0_OVI0(void)__attribute__ ((interrupt));
void INT_Excep_TMR1_CMIA1(void)__attribute__ ((interrupt));
void INT_Excep_TMR1_CMIB1(void)__attribute__ ((interrupt));
void INT_Excep_TMR1_OVI1(void)__attribute__ ((interrupt));
void INT_Excep_TMR2_CMIA2(void)__attribute__ ((interrupt));
void INT_Excep_TMR2_CMIB2(void)__attribute__ ((interrupt));
void INT_Excep_TMR2_OVI2(void)__attribute__ ((interrupt));
void INT_Excep_TMR3_CMIA3(void)__attribute__ ((interrupt));
void INT_Excep_TMR3_CMIB3(void)__attribute__ ((interrupt));
void INT_Excep_TMR3_OVI3(void)__attribute__ ((interrupt));
void INT_Excep_DMACA_DMAC0I(void)__attribute__ ((interrupt));
void INT_Excep_DMACA_DMAC1I(void)__attribute__ ((interrupt));
void INT_Excep_DMACA_DMAC2I(void)__attribute__ ((interrupt));
void INT_Excep_DMACA_DMAC3I(void)__attribute__ ((interrupt));
void INT_Excep_EXDMAC_EXDMAC_0I(void)__attribute__ ((interrupt));
void INT_Excep_EXDMAC_EXDMAC_1I(void)__attribute__ ((interrupt));
void INT_Excep_SCI0_ERI0(void)__attribute__ ((interrupt));
void INT_Excep_SCI0_RXI0(void)__attribute__ ((interrupt));
void INT_Excep_SCI0_TXI0(void)__attribute__ ((interrupt));
void INT_Excep_SCI0_TEI0(void)__attribute__ ((interrupt));
void INT_Excep_SCI1_ERI1(void)__attribute__ ((interrupt));
void INT_Excep_SCI1_RXI1(void)__attribute__ ((interrupt));
void INT_Excep_SCI1_TXI1(void)__attribute__ ((interrupt));
void INT_Excep_SCI1_TEI1(void)__attribute__ ((interrupt));
void INT_Excep_SCI2_ERI2(void)__attribute__ ((interrupt));
void INT_Excep_SCI2_RXI2(void)__attribute__ ((interrupt));
void INT_Excep_SCI2_TXI2(void)__attribute__ ((interrupt));
void INT_Excep_SCI2_TEI2(void)__attribute__ ((interrupt));
void INT_Excep_SCI3_ERI3(void)__attribute__ ((interrupt));
void INT_Excep_SCI3_RXI3(void)__attribute__ ((interrupt));
void INT_Excep_SCI3_TXI3(void)__attribute__ ((interrupt));
void INT_Excep_SCI3_TEI3(void)__attribute__ ((interrupt));
void INT_Excep_SCI5_ERI5(void)__attribute__ ((interrupt));
void INT_Excep_SCI5_RXI5(void)__attribute__ ((interrupt));
void INT_Excep_SCI5_TXI5(void)__attribute__ ((interrupt));
void INT_Excep_SCI5_TEI5(void)__attribute__ ((interrupt));
void INT_Excep_SCI6_ERI6(void)__attribute__ ((interrupt));
void INT_Excep_SCI6_RXI6(void)__attribute__ ((interrupt));
void INT_Excep_SCI6_TXI6(void)__attribute__ ((interrupt));
void INT_Excep_SCI6_TEI6(void)__attribute__ ((interrupt));
void INT_Excep_RIIC0_ICEEI0(void)__attribute__ ((interrupt));
void INT_Excep_RIIC0_ICRXI0(void)__attribute__ ((interrupt));
void INT_Excep_RIIC0_ICTXI0(void)__attribute__ ((interrupt));
void INT_Excep_RIIC0_ICTEI0(void)__attribute__ ((interrupt));
void INT_Excep_RIIC1_ICEEI1(void)__attribute__ ((interrupt));
void INT_Excep_RIIC1_ICRXI1(void)__attribute__ ((interrupt));
void INT_Excep_RIIC1_ICTXI1(void)__attribute__ ((interrupt));
void INT_Excep_RIIC1_ICTEI1(void)__attribute__ ((interrupt));


#endif




