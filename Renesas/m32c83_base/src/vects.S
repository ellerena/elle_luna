;/****************************************************************
;KPIT Cummins Infosystems Ltd, Pune, India. 17-June-2005.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;
;*****************************************************************/

; Fixed Vector table

	.section	.vects,"a",@progbits
	.type	_HardwareVectors, @object
	.size	_HardwareVectors, 36

_HardwareVectors:
	.long	0	;(Undefined Instruction) Interrupt on UND instruction 
	.long	0	;(Overflow) Interrupt on INTO instruction 
	.long	0	;(BRK) If the content of address FFFFE7H is FFH, a program is executed from the address stored into software interrupt number 0 in the relocatable vector table
	.long	0	;(Address match)The address match interrupt occurs when the AIERi bit in the AIER register is set to "1" (address match interrupt enabled).
	.long	0	;Reserved space
	.long	0	;Watchdog timer
	.long	0	;Reserved space
	.long	0	;(NMI)External interrupt by input to NMI pin
	.long   (_start + 0xFF000000)	; Reset

; Variable Vector table

	.section	.var_vects,"a",@progbits
	.type	_VariableVectors, @object
	.size	_VariableVectors, 256
_VariableVectors:
	.long	0 		;0_BRK_ISR
	.long	0		;1_Reserved
	.long	0		;2_Reserved
	.long	0		;3_Reserved
	.long	0		;4_Reserved
	.long	0		;5_reserved
	.long	0		;6_reserved
	.long	0		;7_reserved
	.long	0		;8_DMA0_ISR
	.long	0		;9_DMA1_ISR
	.long	0		;10_DMA2_ISR
	.long	0		;11_DMA3_ISR
	.long	_ta0_irq		;12_TimerA0_ISR
	.long	0		;13_TimerA1_ISR
	.long	0		;14_TimerA2_ISR
	.long	0		;15_TimerA3_ISR
	.long	0		;16_TimerA4_ISR
	.long	0		;17_UART0 trans_ISR
	.long	0		;18_UART0 recep_ISR
	.long	0		;19_UART1 trans_ISR
	.long	_uart1_rx_irq		;20_UART1 recep_ISR
	.long	0		;21_TimerB0_ISR
	.long	0		;22_TimerB1_ISR
	.long	0		;23_TimerB2_ISR
	.long	0		;24_TimerB3_ISR
	.long	0		;25_TimerB4_ISR
	.long	0		;26_INT5_ISR
	.long	0		;27_INT4_ISR
	.long	0		;28_INT3_ISR
	.long	0		;29_INT2_ISR
	.long	0		;30_INT1_ISR
	.long	0		;31_INT0_ISR
	.long	0		;32_TimerB5_ISR
	.long	0		;33_UART2 trans_ISR
	.long	0		;34_UART2 recep_ISR
	.long	0		;35_UART3 trans_ISR
	.long	0		;36_UART3 recep_ISR
	.long	0		;37_UART4 trans_ISR
	.long	0		;38_UART4 recep_ISR
	.long	0		;39_Serial IO_ISR
	.long	0		;40_Serial IO_ISR
	.long	0		;41_Serial IO_ISR
	.long	0		;42_AD0_ISR
	.long	0		;43_Key Input_ISR
	.long	0		;44_Intelligent I/O Interrupt 0/CAN3_ISR
	.long	0		;45_Intelligent I/O Interrupt 1/CAN4_ISR
	.long	0		;46_Intelligent I/O Interrupt 2_ISR
	.long	0		;47_Intelligent I/O Interrupt 3_ISR
	.long	0		;48_Intelligent I/O Interrupt 4_ISR
	.long	0		;49_CAN5_ISR
	.long	0		;50_Reserved
	.long	0		;51_Reserved
	.long	0		;52_Intelligent I/O Interrupt 8_ISR
	.long	0		;53_Intelligent I/O Interrupt 9/CAN0_ISR
	.long	0		;54_Intelligent I/O Interrupt 10/CAN1_ISR
	.long	0		;55_Reserved
	.long	0		;56_Reserved
	.long	0		;57_CAN2_ISR
	.long	0		;58_Reserved_ISR
	.long	0		;59_Reserved_ISR
	.long	0		;60_Reserved_ISR
	.long	0		;61_Reserved_ISR
	.long	0		;62_Reserved_ISR
	.long	0		;63_Reserved_ISR

.end

