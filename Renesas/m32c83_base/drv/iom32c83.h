/****************************************************************
KPIT Cummins Infosystems Ltd, Pune, India.  27-June-2005.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

*****************************************************************/
/****************************************************************/
/*      M32C/83 Include File                                    */
/****************************************************************/

#ifndef _IOM32C83_H_
#define _IOM32C83_H_

/*------------------------------------------------------
    Processor mode register 0 //0x0004
------------------------------------------------------*/
union st_pm0 {               /* Processor mode register 0           */
  struct {
	 unsigned char 	PM0_0:1; /* Processor mode bit 0 				*/
	 unsigned char 	PM0_1:1; /* Processor mode bit 1 				*/
	 unsigned char 	PM0_2:1; /* R/W mode select bit  				*/
	 unsigned char 	PM0_3:1; /* Software reset bit					*/
	 unsigned char 	PM0_4:1; /* Multiplexed bus space select bit 0	*/
	 unsigned char 	PM0_5:1; /* Multiplexed bus space select bit 1	*/
	 unsigned char 	     :1; /* Reserved bit must be set to 0       */
	 unsigned char 	PM0_7:1; /* BCLK output disable bit		      	*/
  } BIT;   		     		 /* Bit  Access 		   				*/
	unsigned  char BYTE;     /* Byte Access  						*/
};

/*------------------------------------------------------
    Processor mode register 1 // 0x005
 ------------------------------------------------------*/
union st_pm1 {              /* Processor mode register 1 		*/
  struct {
	 unsigned char PM1_0:1; /* External memory area mode bit    */
	 unsigned char PM1_1:1; /* External memory area mode bit    */
	 unsigned char PM1_2:1; /* Internal memory wait bit         */
	 unsigned char PM1_3:1; /* SFR wait bit                     */
	 unsigned char PM1_4:1; /* ALE pin select bit               */
	 unsigned char PM1_5:1; /* ALE pin select bit               */
	 unsigned char      :1; /* Reserved bit must be set to 0 	*/
	 unsigned char      :1; /* Reserved bit must be set to 0 	*/
  } BIT;   		            /* Bit  Access 		   				*/
	 unsigned char BYTE;    /* Byte Access 						*/
};

/*------------------------------------------------------
    System clock control register 0//0x0006
    ----------------------------------------------------*/
union st_cm0 {               /* System clock control register 0         */
  struct {
	 unsigned char 	CM0_0:1; /* Clock output function select bit 	    */
	 unsigned char 	CM0_1:1; /* Clock output function select bit 	    */
	 unsigned char 	CM0_2:1; /* WAIT peripheral function clock stop bit */
	 unsigned char 	CM0_3:1; /* Xcin-Xcout drive capacity select bit    */
	 unsigned char 	CM0_4:1; /* Port Xc switch bit 					    */
	 unsigned char 	CM0_5:1; /* Main clock stop bit					    */
	 unsigned char 	CM0_6:1; /* WDT function select bit                 */
	 unsigned char 	CM0_7:1; /* CPU clock select bit0                   */
  } BIT;   		     		 /* Bit  Access 		   				    */
	unsigned char BYTE;      /* Byte Access 						    */
};


/*------------------------------------------------------
    System clock control register 1 //0x0007
  ------------------------------------------------------*/
union st_cm1 {               /* System clock control register 1		*/
  struct {
	 unsigned char 	CM1_0:1; /* All clock stop control bit 			*/
	 unsigned char 		 :1; /* Reserved bit always set to 0		*/
	 unsigned char 	 	 :1; /* Reserved bit always set to 0		*/
	 unsigned char 		 :1; /* Reserved bit always set to 0		*/
	 unsigned char 		 :1; /* Reserved bit always set to 0		*/
	 unsigned char 	     :1; /* Reserved bit always set to 1		*/
	 unsigned char 	     :1; /* Reserved bit always set to 0		*/
	 unsigned char 	CM1_7:1; /* CPU clock select bit1               */
  } BIT;   		     		 /* Bit  Access 		   				*/
	unsigned char BYTE;      /* Byte Access 						*/
};


/*------------------------------------------------------
    Address match interrupt enable register// 0x0009
------------------------------------------------------*/
union st_aier {               /* Address match interrupt enable register	*/
   struct {
	 unsigned char 	AIER0:1;  /* Address match interrupt 0 enable bit       */
	 unsigned char 	AIER1:1;  /* Address match interrupt 1 enable bit       */
	 unsigned char 	AIER2:1;  /* Address match interrupt 2 enable bit       */
	 unsigned char 	AIER3:1;  /* Address match interrupt 3 enable bit       */
	 unsigned char 	AIER4:1;  /* Address match interrupt 4 enable bit       */
	 unsigned char 	AIER5:1;  /* Address match interrupt 5 enable bit       */
	 unsigned char 	AIER6:1;  /* Address match interrupt 6 enable bit       */
	 unsigned char 	AIER7:1;  /* Address match interrupt 7 enable bit       */
   } BIT;   		      	  /* Bit  Access 		   				        */
	 unsigned char BYTE;      /* Byte Access 						        */
};


/*------------------------------------------------------
   Protect register // 0x000A
-----------------------------------------------------*/
union st_prcr {               /* Protect register                */
   struct {
	 unsigned char 	PRC0:1;   /* Protect bit0                    */
	 unsigned char 	PRC1:1;   /* Protect bit1                    */
	 unsigned char 	PRC2:1;   /* Protect bit2                    */
	 unsigned char 	PRC3:1;   /* Protect bit3                    */
	 unsigned char 		:1;   /* Nothing assigned  				 */
	 unsigned char 		:1;   /* Nothing assigned  				 */
	 unsigned char 	 	:1;   /* Nothing assigned 				 */
	 unsigned char 		:1;   /* Nothing assigned  				 */
   } BIT;   		      	  /* Bit  Access 		   			 */
	 unsigned char BYTE;      /* Byte Access 					 */
};
/* when nothing assigned then, when write, set to "0".When read, its content is indeterminate."*/

/*------------------------------------------------------
   External data bus width control register // 0x000B
  -----------------------------------------------------*/
union st_ds {                /* External data bus width control register   */
   struct {
	 unsigned char 	DS0:1;   /* External space 0 data bus width select bit */
	 unsigned char 	DS1:1;   /* External space 1 data bus width select bit */
	 unsigned char 	DS2:1;   /* External space 2 data bus width select bit */
	 unsigned char 	DS3:1;   /* External space 3 data bus width select bit */
	 unsigned char 	   :1;   /* Nothing assigned  				           */
	 unsigned char 	   :1;   /* Nothing assigned  				           */
	 unsigned char 	   :1;   /* Nothing assigned 				           */
	 unsigned char 	   :1;   /* Nothing assigned  				           */
   } BIT;   		      	 /* Bit  Access 		   			           */
	 unsigned char BYTE;     /* Byte Access 					           */
};
/* when nothing assigned then, when write, set to "0".When write, set to "0".When read, its content is indeterminate."*/

/*------------------------------------------------------
    Main clock division register //0x000C
------------------------------------------------------*/
union st_mcd {              /* Main clock division register	  */
  struct {
	 unsigned char 	MCD0:1; /* Main clock division select bit */
	 unsigned char 	MCD1:1; /* Main clock division select bit */
	 unsigned char 	MCD2:1; /* Main clock division select bit */
	 unsigned char 	MCD3:1; /* Main clock division select bit */
	 unsigned char 	MCD4:1; /* Main clock division select bit */
	 unsigned char 	    :1; /* Reserved bit must be set to 0  */
	 unsigned char 	    :1; /* Reserved bit must be set to 0  */
	 unsigned char 	    :1; /* Reserved bit must be set to 0  */
  } BIT;   		     		/* Bit  Access 		   		      */
	unsigned char BYTE;     /* Byte Access 				      */
};

/*------------------------------------------------------
    Oscillation stop detect register //0x000D
------------------------------------------------------*/
union st_cm2 {               /* Oscillation stop detect register	  */
  struct {
	 unsigned char 	CM2_0:1; /* Oscillation stop detect enable bit    */
	 unsigned char 	CM2_1:1; /* CPU clock select bit2                 */
	 unsigned char 	CM2_2:1; /* Oscillation stop detect flag          */
	 unsigned char 	CM2_3:1; /* Main clock monitor flag               */
	 unsigned char 	     :1; /* Reserved bit must be set to 0	      */
	 unsigned char 	     :1; /* Reserved bit must be set to 0	      */
	 unsigned char 	     :1; /* Reserved bit must be set to 0	      */
	 unsigned char 	     :1; /* Reserved bit must be set to 0	      */
  } BIT;   		     		 /* Bit  Access 		   		          */
	unsigned char BYTE;      /* Byte Access 				          */
};

/*------------------------------------------------------
   Watchdog timer control register //0x000F
-----------------------------------------------------*/
union st_wdc {               /* Watchdog timer control register     */
   struct {
     unsigned char 	   :1; 	 /* High-order bit of watchdog timer	*/
     unsigned char 	   :1; 	 /* High-order bit of watchdog timer	*/
     unsigned char 	   :1; 	 /* High-order bit of watchdog timer	*/
     unsigned char 	   :1;   /* High-order bit of watchdog timer	*/
     unsigned char 	   :1; 	 /* High-order bit of watchdog timer	*/
     unsigned char WDC5:1; 	 /* Cold start/warm start detect flag   */
     unsigned char 	   :1; 	 /* Reserved bit must be set to 0		*/
     unsigned char WDC7:1;   /* Prescaler select bit				*/
   } BIT;   		    	 /* Bit  Access 		   				*/
     unsigned char BYTE;     /* Byte Access 						*/
};

/*------------------------------------------------------
   Address match interrupt register 0 //0x0010
-----------------------------------------------------*/
union st_rmad0 {              /* Address match interrupt register 0 32 bit     */
   struct{
	unsigned char RMAD0L;     /* Address match interrupt register 0 low  8 bit */
	unsigned char RMAD0M;     /* Address match interrupt register 0 mid  8 bit */
	unsigned char RMAD0H;     /* Address match interrupt register 0 high 8 bit */
	unsigned char NC;         /* non use 									   */
   } BYTE;					  /* Byte access								   */
	unsigned long   DWORD;	  /* Word Access								   */
};

/*------------------------------------------------------
   Processor mode register 2//0x0013
-----------------------------------------------------*/
union st_pm2 {              /* Processor mode register 2    */
   struct {
     unsigned char      :1; /* Reserved bit (Set to 0)      */
     unsigned char PM2_1:1; /* System clock protect bit     */
     unsigned char PM2_2:1; /* WDT count source protect bit */
     unsigned char      :1; /* Reserved bit (Set to 0)      */
     unsigned char PM2_4:1; /* CAN clock select bit3        */
     unsigned char PM2_5:1; /* CAN clock select bit         */
     unsigned char PM2_6:1; /* f2n count source select bit  */
     unsigned char PM2_7:1; /* f2n count source select bit  */
   } BIT;   		    	/* Bit  Access 		   			*/
     unsigned char BYTE;    /* Byte Access 					*/
};

/*------------------------------------------------------
   Address match interrupt register 1//0x0014
-----------------------------------------------------*/
union st_rmad1 {              /* Address match interrupt register 1 32 bit     */
   struct{
	unsigned char RMAD1L;     /* Address match interrupt register 1 low  8 bit */
	unsigned char RMAD1M;     /* Address match interrupt register 1 mid  8 bit */
	unsigned char RMAD1H;     /* Address match interrupt register 1 high 8 bit */
	unsigned char NC;         /* non use 									   */
   } BYTE;					  /* Byte access								   */
   unsigned long   DWORD;	  /* Word Access								   */
};

/*------------------------------------------------------
   Voltage detection register 2//0x0017
-----------------------------------------------------*/
union st_vcr2 {             /* Voltage detection register 2   */
   struct {
     unsigned char      :1; /* Reserved bit must be set to 0  */
     unsigned char      :1; /* Reserved bit must be set to 0  */
     unsigned char      :1; /* Reserved bit must be set to 0  */
     unsigned char      :1; /* Reserved bit must be set to 0  */
     unsigned char      :1; /* Reserved bit must be set to 0  */
     unsigned char      :1; /* Reserved bit must be set to 0  */
     unsigned char VC2_6:1; /* Reset level monitor bit        */
     unsigned char VC2_7:1; /* Voltage down monitor bit       */
   } BIT;   		    	/* Bit  Access 		   			  */
     unsigned char BYTE;    /* Byte Access 					  */
};

/*------------------------------------------------------
   Address match interrupt register 2//0x0018
-----------------------------------------------------*/
union st_rmad2 {              /* Address match interrupt register 2 32 bit     */
   struct{
	unsigned char RMAD2L;     /* Address match interrupt register 2 low  8 bit */
	unsigned char RMAD2M;     /* Address match interrupt register 2 mid  8 bit */
	unsigned char RMAD2H;     /* Address match interrupt register 2 high 8 bit */
	unsigned char NC;         /* non use 									   */
   } BYTE;					  /* Byte access								   */
   unsigned long   DWORD;	  /* Word Access								   */
};

/*------------------------------------------------------
   Voltage detection register 1//0x001B
-----------------------------------------------------*/
union st_vcr1 {             /* Voltage detection register 2   */
   struct {
     unsigned char      :1; /* Reserved bit must be set to 0  */
     unsigned char      :1; /* Reserved bit must be set to 0  */
     unsigned char      :1; /* Reserved bit must be set to 0  */
     unsigned char VC1_3:1; /* Voltage down monitor flag      */
     unsigned char      :1; /* Reserved bit must be set to 0  */
     unsigned char      :1; /* Reserved bit must be set to 0  */
     unsigned char      :1; /* Reserved bit must be set to 0  */
     unsigned char      :1; /* Reserved bit must be set to 0  */
   } BIT;   		    	/* Bit  Access 		   			  */
     unsigned char BYTE;    /* Byte Access 					  */
};

/*------------------------------------------------------
   Address match interrupt register 3 //0x001C
  -----------------------------------------------------*/
union st_rmad3 {              /* Address match interrupt register 3 32 bit     */
   struct{
	unsigned char RMAD3L;     /* Address match interrupt register 3 low  8 bit */
	unsigned char RMAD3M;     /* Address match interrupt register 3 mid  8 bit */
	unsigned char RMAD3H;     /* Address match interrupt register 3 high 8 bit */
	unsigned char NC;         /* non use 									   */
   } BYTE;					  /* Byte access								   */
	unsigned long   DWORD;	  /* Word Access								   */
};

/*------------------------------------------------------
   PLL Control Register 0//0x0026
  -----------------------------------------------------*/
union st_plc0 {                /* PLL Control Register 0            */
   struct {
     unsigned char  PLC0_0 :1; /* Programmable counter select bit   */
     unsigned char  PLC0_1 :1; /* Programmable counter select bit   */
     unsigned char  PLC0_2 :1; /* Programmable counter select bit   */
     unsigned char         :1; /* Reserved bit must be set to 0     */
     unsigned char         :1; /* Reserved bit must be set to 0     */
     unsigned char         :1; /* Reserved bit must be set to 0     */
     unsigned char         :1; /* Reserved bit must be set to 0     */
     unsigned char  PLC0_7 :1; /* Operation enable bit              */
   } BIT;   		    	   /* Bit  Access 		   		        */
     unsigned char BYTE;       /* Byte Access 				        */
};

/*------------------------------------------------------
   PLL Control Register 1//0x0027
-----------------------------------------------------*/
union st_plc1 {               /* PLL Control Register 1          */
   struct {
     unsigned char        :1; /* Reserved bit must be set to 0   */
     unsigned char        :1; /* Reserved bit must be set to 1   */
     unsigned char  PLC1_2:1; /* PLL clock division switch bit   */
     unsigned char        :1; /* Reserved bit must be set to 0   */
     unsigned char        :1; /* Reserved bit must be set to 0   */
     unsigned char        :1; /* Reserved bit must be set to 0   */
     unsigned char        :1; /* Reserved bit must be set to 0   */
     unsigned char        :1; /* Reserved bit must be set to 0   */
   } BIT;   		    	  /* Bit  Access 		   		     */
     unsigned char BYTE;      /* Byte Access 				     */
};

/*------------------------------------------------------
   Address match interrupt register 4 //0x0028
 -----------------------------------------------------*/
union st_rmad4 {              /*Address match interrupt register 4 32 bit      */
   struct{
	unsigned char RMAD4L;     /* Address match interrupt register 4 low  8 bit */
	unsigned char RMAD4M;     /* Address match interrupt register 4 mid  8 bit */
	unsigned char RMAD4H;     /* Address match interrupt register 4 high 8 bit */
	unsigned char NC;         /* non use 									   */
   } BYTE;					  /* Byte access								   */
	unsigned long   DWORD;	  /* Word Access								   */
};

/*------------------------------------------------------
   Address match interrupt register 5 //0x002C
-----------------------------------------------------*/
union st_rmad5 {              /*Address match interrupt register 5 32 bit      */
   struct{
	unsigned char RMAD5L;     /* Address match interrupt register 5 low  8 bit */
	unsigned char RMAD5M;     /* Address match interrupt register 5 mid  8 bit */
	unsigned char RMAD5H;     /* Address match interrupt register 5 high 8 bit */
	unsigned char NC;         /* non use 									   */
   } BYTE;					  /* Byte access								   */
	unsigned long   DWORD;	  /* Word Access								   */
};

/*------------------------------------------------------
   Voltage Down Detection Interrupt Register//0x002F
-----------------------------------------------------*/
union st_d4int{             /* Voltage Down Detection Interrupt Register   */
   struct {
     unsigned char  D4_0:1; /* Voltage down detection interrupt enable bit */
     unsigned char  D4_1:1; /* STOP-WAIT mode deactivation control bit     */
     unsigned char  D4_2:1; /* Voltage change detection flag               */
     unsigned char  D4_3:1; /* WDT overflow detect flag                    */
     unsigned char  DF0 :1; /* Sampling clock select bit                   */
     unsigned char  DF1 :1; /* Sampling clock select bit                   */
     unsigned char      :1; /* Reserved bit must be set to 0               */
     unsigned char      :1; /* Reserved bit must be set to 0               */
   } BIT;   		    	/* Bit  Access 		   		                   */
     unsigned char BYTE;    /* Byte Access 				                   */
};

/*------------------------------------------------------
   Address match interrupt register 6 //0x0038
-----------------------------------------------------*/
union st_rmad6 {              /*Address match interrupt register 6 32 bit      */
   struct{
	unsigned char RMAD6L;     /* Address match interrupt register 6 low  8 bit */
	unsigned char RMAD6M;     /* Address match interrupt register 6 mid  8 bit */
	unsigned char RMAD6H;     /* Address match interrupt register 6 high 8 bit */
	unsigned char NC;         /* non use 									   */
   } BYTE;					  /* Byte access								   */
	unsigned long   DWORD;	  /* Word Access								   */
};

/*------------------------------------------------------
   Address match interrupt register 7 //0x003C
-----------------------------------------------------*/
union st_rmad7 {              /* Address match interrupt register 7 32 bit      */
   struct{
	unsigned char RMAD7L;     /* Address match interrupt register 7 low  8 bit */
	unsigned char RMAD7M;     /* Address match interrupt register 7 mid  8 bit */
	unsigned char RMAD7H;     /* Address match interrupt register 7 high 8 bit */
	unsigned char NC;         /* non use 									   */
   } BYTE;					  /* Byte access								   */
	unsigned long   DWORD;	  /* Word Access								   */
};

/*------------------------------------------------------
   External Space Wait Control Register 0//0x0048
-----------------------------------------------------*/
union st_ewcr0{                 /* External Space Wait Control Register 0   */
   struct {
     unsigned char  EWCR0_00:1; /* Bus cycle select bit                     */
     unsigned char  EWCR0_01:1; /* Bus cycle select bit                     */
     unsigned char  EWCR0_02:1; /* Bus cycle select bit                     */
     unsigned char  EWCR0_03:1; /* Bus cycle select bit                     */
     unsigned char  EWCR0_04:1; /* Bus cycle select bit                     */
     unsigned char          :1; /* Nothing is assigned                      */
     unsigned char  EWCR0_06:1; /* Recovery cycle addition select bit       */
     unsigned char          :1; /* Nothing is assigned                      */
   } BIT;   		    	    /* Bit  Access 		        	            */
     unsigned char BYTE;        /* Byte Access 				                */
};
/*when nothing assigned then "When read, its content is indeterminate."     */

/*------------------------------------------------------
   External Space Wait Control Register 1//0x0049
  -----------------------------------------------------*/
union st_ewcr1{                 /* External Space Wait Control Register 1   */
   struct {
     unsigned char  EWCR1_00:1; /* Bus cycle select bit                     */
     unsigned char  EWCR1_01:1; /* Bus cycle select bit                     */
     unsigned char  EWCR1_02:1; /* Bus cycle select bit                     */
     unsigned char  EWCR1_03:1; /* Bus cycle select bit                     */
     unsigned char  EWCR1_04:1; /* Bus cycle select bit                     */
     unsigned char          :1; /* Nothing is assigned                      */
     unsigned char  EWCR1_06:1; /* Recovery cycle addition select bit       */
     unsigned char          :1; /* Nothing is assigned                      */
   } BIT;   		    	    /* Bit  Access 		        	            */
     unsigned char BYTE;        /* Byte Access 				                */
};
/*when nothing assigned then "When read, its content is indeterminate."      */

/*------------------------------------------------------
   External Space Wait Control Register 2//0x004A
-----------------------------------------------------*/
union st_ewcr2{                 /* External Space Wait Control Register 1   */
   struct {
     unsigned char  EWCR2_00:1; /* Bus cycle select bit                     */
     unsigned char  EWCR2_01:1; /* Bus cycle select bit                     */
     unsigned char  EWCR2_02:1; /* Bus cycle select bit                     */
     unsigned char  EWCR2_03:1; /* Bus cycle select bit                     */
     unsigned char  EWCR2_04:1; /* Bus cycle select bit                     */
     unsigned char          :1; /* Nothing is assigned                      */
     unsigned char  EWCR2_06:1; /* Recovery cycle addition select bit       */
     unsigned char          :1; /* Nothing is assigned                      */
   } BIT;   		    	    /* Bit  Access 		        	            */
     unsigned char BYTE;        /* Byte Access 				                */
};
/*when nothing assigned then "When read, its content is indeterminate."*/

/*------------------------------------------------------
   External Space Wait Control Register 3//0x004B
-----------------------------------------------------*/
union st_ewcr3{                 /* External Space Wait Control Register 3   */
   struct {
     unsigned char  EWCR3_00:1; /* Bus cycle select bit                     */
     unsigned char  EWCR3_01:1; /* Bus cycle select bit                     */
     unsigned char  EWCR3_02:1; /* Bus cycle select bit                     */
     unsigned char  EWCR3_03:1; /* Bus cycle select bit                     */
     unsigned char  EWCR3_04:1; /* Bus cycle select bit                     */
     unsigned char          :1; /* Nothing is assigned                      */
     unsigned char  EWCR3_06:1; /* Recovery cycle addition select bit       */
     unsigned char          :1; /* Nothing is assigned                      */
   } BIT;   		    	    /* Bit  Access 		        	            */
     unsigned char BYTE;        /* Byte Access 				                */
};
/*when nothing assigned then "When read, its content is indeterminate."*/

/*------------------------------------------------------
  Flash Memory Control Register 1 ///0x0055
-----------------------------------------------------*/
union st_fmr1{                 /* Flash Memory Control Register 1                       */
   struct {
     unsigned char         :1; /* Reserved bit "When read,its content is indeterminate" */
     unsigned char  FMR1_1 :1; /* EW1 mode select bit                                   */
     unsigned char         :1; /* Reserved bit "When read,its content is indeterminate" */
     unsigned char         :1; /* Reserved bit "When read,its content is indeterminate" */
     unsigned char         :1; /* Reserved bit must be set to 0                         */
     unsigned char         :1; /* Reserved bit must be set to 0                         */
     unsigned char  FMR1_6 :1; /* Lock bit status flag                                  */
     unsigned char         :1; /* Reserved bit "set to 0"                               */
   } BIT;   		    	   /* Bit  Access 		        	                        */
     unsigned char BYTE;       /* Byte Access 				                            */
};

/*------------------------------------------------------
  Flash Memory Control Register 0 ///0x0057
-----------------------------------------------------*/
union st_fmr0{                 /* Flash Memory Control Register 0                       */
   struct {
     unsigned char  FMR0_0 :1; /* RY/BY status flag                                     */
     unsigned char  FMR0_1 :1; /* CPU rewrite mode select bit                           */
     unsigned char  FMR0_2 :1; /* Lock bit disable select bit                           */
     unsigned char  FMSTP  :1; /* Flash memory stop bit                                 */
     unsigned char         :1; /* Reserved bit must be set to 0                         */
     unsigned char  FMR0_5 :1; /* User ROM area select bit (Available in boot mode only)*/
     unsigned char  FMR0_6 :1; /* Program status flag                                   */
     unsigned char  FMR0_7 :1; /* Erase status flag                                     */
   } BIT;   		    	   /* Bit  Access 		        	                        */
     unsigned char BYTE;       /* Byte Access 				                            */
};

/*------------------------------------------------------
  DMA0 interrupt control register //0x0068
-----------------------------------------------------*/
union st_dm0ic{               /* DMA0 interrupt control register                      */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Timer B5 interrupt register //0x0069
-----------------------------------------------------*/
union st_tb5ic{               /* Timer B5 interrupt register                          */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  DMA2 interrupt register //0x006A
-----------------------------------------------------*/
union st_dm2ic{               /* DMA2 interrupt register                              */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  UART2 receive/ack interrupt control register //0x006B
-----------------------------------------------------*/
union st_s2ric{               /* UART2 receive/ack interrupt control register         */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Timer A0 interrupt control register //0x006C
-----------------------------------------------------*/
union st_ta0ic{               /* Timer A0 interrupt control register                  */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  UART3 receive/ack interrupt control register //0x006D
-----------------------------------------------------*/
union st_s3ric{               /* UART3 receive/ack interrupt control register                  */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Timer A2 interrupt control register //0x006E
-----------------------------------------------------*/
union st_ta2ic{               /* Timer A2 interrupt control register                  */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  UART4 receive/ack interrupt control register //0x006F
-----------------------------------------------------*/
union st_s4ric{               /* UART4 receive/ack interrupt control register         */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Timer A4 interrupt control register //0x0070
-----------------------------------------------------*/
union st_ta4ic{               /* Timer A4 interrupt control register                  */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Bus collision (UART0) interrupt control register //0x0071
-----------------------------------------------------*/
union st_bcn0ic{              /* Bus collision (UART0) interrupt control register     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  UART0 receive interrupt control register //0x0072
 -----------------------------------------------------*/
union st_s0ric{               /* UART0 receive interrupt control register             */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  A/D0 conversion interrupt control register //0x0073
 -----------------------------------------------------*/
union st_ad0ic{               /* A/D0 conversion interrupt control register            */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  UART1 receive interrupt control register //0x0074
-----------------------------------------------------*/
union st_s1ric{               /* A/D0 conversion interrupt control register            */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Intelligent I/O interrupt control register 0 //0x0075
-----------------------------------------------------*/
union st_iio0ic{              /* Intelligent I/O interrupt control register 0         */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  CAN interrupt 3 control register //0x0075
-----------------------------------------------------*/
union st_can3ic{              /* CAN interrupt 3 control register                     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Timer B1 interrupt control register //0x0076
-----------------------------------------------------*/
union st_tb1ic{               /* Timer B1 interrupt control register                  */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Intelligent I/O interrupt control register 2 //0x0077
-----------------------------------------------------*/
union st_iio2ic{              /* Intelligent I/O interrupt control register 2         */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Timer B3 interrupt control register //0x0078
-----------------------------------------------------*/
union st_tb3ic{               /* Timer B3 interrupt control register                  */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Intelligent I/O interrupt control register 4  //0x0079
-----------------------------------------------------*/
union st_iio4ic{              /* Intelligent I/O interrupt control register 4         */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  INT5~ interrupt control register  //0x007A
-----------------------------------------------------*/
union st_int5ic{              /* INT5~ interrupt control register                     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char  POL   :1; /* Polarity Switch Bit                                  */
     unsigned char  LVS   :1; /* Level Sensitive/Edge Sensitive Switch Bit            */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  INT3~ interrupt control register  //0x007C
-----------------------------------------------------*/
union st_int3ic{              /* INT3~ interrupt control register                     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char  POL   :1; /* Polarity Switch Bit                                  */
     unsigned char  LVS   :1; /* Level Sensitive/Edge Sensitive Switch Bit            */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Intelligent I/O interrupt control register 8  //0x007D
-----------------------------------------------------*/
union st_iio8ic{              /* Intelligent I/O interrupt control register 8         */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  INT1~ interrupt control register  //0x007E
-----------------------------------------------------*/
union st_int1ic{              /* INT1~ interrupt control register                     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char  POL   :1; /* Polarity Switch Bit                                  */
     unsigned char  LVS   :1; /* Level Sensitive/Edge Sensitive Switch Bit            */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Intelligent I/O interrupt control register 10  //0x007F
-----------------------------------------------------*/
union st_iio10ic{             /* Intelligent I/O interrupt control register 10        */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  CAN Interrupt 1 Control Register  //0x007f
-----------------------------------------------------*/
union st_can1ic{              /* CAN Interrupt 1 Control Register                     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */
/*------------------------------------------------------
  CAN Interrupt 2 Control Register  //0x0081
-----------------------------------------------------*/
union st_can2ic{              /* CAN Interrupt 2 Control Register                     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  DMA1 interrupt control register  //0x0088
-----------------------------------------------------*/
union st_dm1ic{               /* DMA1 interrupt control register                      */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  UART2 transmit/nack interrupt control register  //0x0089
-----------------------------------------------------*/
union st_s2tic{               /* UART2 transmit/nack interrupt control register       */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  DMA3 interrupt control register  //0x008A
-----------------------------------------------------*/
union st_dm3ic{               /* DMA3 interrupt control register                      */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  UART3 transmit/nack interrupt control register  //0x008B
-----------------------------------------------------*/
union st_s3tic{              /* UART3 transmit/nack interrupt control register       */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Timer A1 interrupt control register  //0x008C
-----------------------------------------------------*/
union st_ta1ic{               /* Timer A1 interrupt control register                  */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */


/*------------------------------------------------------
  UART4 transmit/nack interrupt control register //0x008D
-----------------------------------------------------*/
union st_s4tic{               /* UART4 transmit/nack interrupt control register       */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Timer A3 interrupt control register  //0x008E
-----------------------------------------------------*/
union st_ta3ic{               /* Timer A3 interrupt control register                  */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Bus collision (UART2) interrupt control register  //0x008F
-----------------------------------------------------*/
union st_bcn2ic{               /* Bus collision (UART2) interrupt control register    */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  UART0 transmit interrupt control register  //0x0090
-----------------------------------------------------*/
union st_s0tic{               /* UART0 transmit interrupt control register            */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Bus collision (UART1) interrupt control register  //0x0091
-----------------------------------------------------*/
union st_bcn1ic{              /* Bus collision (UART1) interrupt control register     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Bus collision (UART4) interrupt control register  //0x0091
-----------------------------------------------------*/
union st_bcn4ic{              /* Bus collision (UART4) interrupt control register     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  UART1 transmit interrupt control register  //0x0092
-----------------------------------------------------*/
union st_s1tic{               /* Bus collision (UART1) interrupt control register     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Key input interrupt control register  //0x0093
-----------------------------------------------------*/
union st_kupic{               /* Key input interrupt control register                 */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Timer B0 interrupt control register  //0x0094
-----------------------------------------------------*/
union st_tb0ic{               /* Timer B0 interrupt control register                  */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Intelligent I/O interrupt control register 1   //0x0095
-----------------------------------------------------*/
union st_iio1ic{              /* Intelligent I/O interrupt control register 1         */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  CAN Interrupt 4 Control Register   //0x0095
-----------------------------------------------------*/
union st_can4ic{              /* CAN Interrupt 4 Control Register                     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Timer B2 interrupt control register   //0x0096
-----------------------------------------------------*/
union st_tb2ic{               /* Timer B2 interrupt control register                  */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Intelligent I/O interrupt control register 3   //0x0097
-----------------------------------------------------*/
union st_iio3ic{               /* Intelligent I/O interrupt control register 3         */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Timer B4 interrupt control register   //0x0098
-----------------------------------------------------*/
union st_tb4ic{              /* Timer B4 interrupt control register                  */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  CAN Interrupt 5 Control Register   //0x0099
-----------------------------------------------------*/
union st_can5ic{               /* CAN Interrupt 5 Control Register                     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  INT4~ interrupt control register  //0x009A
-----------------------------------------------------*/
union st_int4ic{              /* INT4~ interrupt control register                     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char  POL   :1; /* Polarity Switch Bit                                  */
     unsigned char  LVS   :1; /* Level Sensitive/Edge Sensitive Switch Bit            */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  INT2~ interrupt control register  //0x009C
-----------------------------------------------------*/
union st_int2ic{              /* INT2~ interrupt control register                     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char  POL   :1; /* Polarity Switch Bit                                  */
     unsigned char  LVS   :1; /* Level Sensitive/Edge Sensitive Switch Bit            */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Intelligent I/O interrupt control register 9 //0x009D
-----------------------------------------------------*/
union st_iio9ic{              /* Intelligent I/O interrupt control register 9         */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  CAN0 Interrupt Control Register//0x009D
-----------------------------------------------------*/
union st_can0ic{              /* CAN0 Interrupt Control Register                      */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  INT0~ interrupt control register  //0x009E
-----------------------------------------------------*/
union st_int0ic{              /* INT0~ interrupt control register                     */
   struct {
     unsigned char  ILVL0 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL1 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  ILVL2 :1; /* Interrupt Priority Level Select Bit                  */
     unsigned char  IR    :1; /* Interrupt Request Bit                                */
     unsigned char  POL   :1; /* Polarity Switch Bit                                  */
     unsigned char  LVS   :1; /* Level Sensitive/Edge Sensitive Switch Bit            */
     unsigned char        :1; /* Nothing is assigned.                                 */
     unsigned char        :1; /* Nothing is assigned.                                 */
   } BIT;   		    	  /* Bit  Access 		        	                      */
     unsigned char BYTE;      /* Byte Access 				                          */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Exit priority register  //0x009F
-----------------------------------------------------*/
union st_rlvl{                /* Exit priority register                             */
   struct {
     unsigned char  RLVL0 :1; /* Interrupt priority set bits to exit STOP/WAIT mode */
     unsigned char  RLVL1 :1; /* Interrupt priority set bits to exit STOP/WAIT mode */
     unsigned char  RLVL2 :1; /* Interrupt priority set bits to exit STOP/WAIT mode */
     unsigned char  FSIT  :1; /* High-speed interrupt set bit                       */
     unsigned char        :1; /* Nothing is assigned                                */
     unsigned char  DMAII :1; /* DMAC II select bit.                                */
     unsigned char        :1; /* Nothing is assigned                                */
     unsigned char        :1; /* Nothing is assigned.                               */
   } BIT;   		    	  /* Bit  Access 		        	                    */
     unsigned char BYTE;      /* Byte Access 				                        */
};
/*when nothing assigned then "When write, set to "0".When read, its content is indeterminate."   */

/*------------------------------------------------------
  Interrupt request register 0   //0x00A0
-----------------------------------------------------*/
union st_iio0ir{                    /* Interrupt request register 0                                      */
   struct {
     unsigned char              :1; /* Nothing is assigned                                               */
     unsigned char              :1; /* Reserved bit must be set to 0                                     */
     unsigned char  TM13R_PO13R :1; /* "tm13r"---II/O time measurement 3 interrupt
                                       "po13r"---II/O waveform generation function 3 interrupt           */
     unsigned char              :1; /* Reserved bit must be set to 0                                     */
     unsigned char  G0RIR       :1; /* II/O communication unit 0 HDLC data processing function interrupt */
     unsigned char  SIO0RR      :1; /* II/O communication unit 0 receive interrupt                       */
     unsigned char              :1; /* Reserved bit    "set to 0 "                                       */
     unsigned char  CAN10R      :1; /* CAN1 communication function interrupt                             */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate." */

/*------------------------------------------------------
  Interrupt request register 1   //0x00A1
-----------------------------------------------------*/
union st_iio1ir{                    /* Interrupt request register 0                                      */
   struct {
     unsigned char              :1; /* Nothing is assigned                                               */
     unsigned char              :1; /* Reserved bit must be set to 0                                     */
     unsigned char  TM14R_PO14R :1; /* "tm14r"---II/O time measurement 3 interrupt
                                       "po14r"---II/O waveform generation function 3 interrupt           */
     unsigned char              :1; /* Reserved bit must be set to 0                                     */
     unsigned char  G0RIR       :1; /* II/O communication unit 0 HDLC data processing function interrupt */
     unsigned char  SIO0RR      :1; /* II/O communication unit 0 receive interrupt                       */
     unsigned char              :1; /* Reserved bit    "set to 0 "                                       */
     unsigned char  CAN10R      :1; /* CAN1 communication function interrupt                             */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate." */


/*------------------------------------------------------
  Interrupt request register  2  //0x00A2
-----------------------------------------------------*/
union st_iio2ir{                    /* Interrupt request register 2                                      */
   struct {
     unsigned char              :1; /* Nothing is assigned                                               */
     unsigned char              :1; /* Reserved bit    "set to 0 "                                       */
     unsigned char  TM12R_PO12R :1; /* "tm12r"---II/O time measurement 2 interrupt
                                       "po12r"---II/O waveform generation function 2 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  G1RIR       :1; /* II/O communication unit 1 HDLC data processing function interrupt */
     unsigned char  SIO1RR      :1; /* II/O communication unit 1 receive interrupt                       */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate." */

/*------------------------------------------------------
  Interrupt request register  3  //0x00A3
-----------------------------------------------------*/
union st_iio3ir{                    /* Interrupt request register 3                                      */
   struct {
     unsigned char              :1; /* Nothing is assigned                                               */
     unsigned char              :1; /* Reserved bit    "set to 0 "                                       */
     unsigned char  TM10R_PO10R :1; /* "tm10r"---II/O time measurement 0 interrupt
                                       "po10r"---II/O waveform generation function 0 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  G1TOR       :1; /* II/O communication unit 1 HDLC data processing function interrupt */
     unsigned char  SIO1TR      :1; /* II/O communication unit 1 transmit interrupt                      */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate." */

/*------------------------------------------------------
  Interrupt request register  4  //0x00A4
-----------------------------------------------------*/
union st_iio4ir{                    /* Interrupt request register 4                                      */
   struct {
     unsigned char              :1; /* Nothing is assigned                                               */
     unsigned char              :1; /* Reserved bit    "set to 0 "                                       */
     unsigned char  TM17R_PO17R :1; /* "tm17r"---II/O time measurement 7 interrupt
                                       "po17r"---II/O waveform generation function 7 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  BT1R        :1; /* II/O communication base timer interrupt                           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  SRT1R       :1; /* II/O special communication function interrupt                     */
     unsigned char  SRT0R       :1; /* II/O special communication function interrupt                     */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate." */

/*------------------------------------------------------
  Interrupt request register  5  //0x00A5
-----------------------------------------------------*/
union st_iio5ir{                    /* Interrupt request register 5                                      */
   struct {
     unsigned char              :1; /* Nothing is assigned                                               */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  CAN1WUR     :1; /* CAN1 wake-up interrupt                                            */
     unsigned char  CAN12R      :1; /* CAN1 communication function interrupt                             */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate." */

/*------------------------------------------------------
  Interrupt request register  8  //0x00A8
-----------------------------------------------------*/
union st_iio8ir{                    /* Interrupt request register 8                                      */
   struct {
     unsigned char              :1; /* Nothing is assigned                                               */
     unsigned char  TM11R_PO11R :1; /* "tm11r"---II/O time measurement 0 interrupt
                                       "po11r"---II/O waveform generation function 0 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate." */

/*------------------------------------------------------
  Interrupt request register  9  //0x00A9
-----------------------------------------------------*/
union st_iio9ir{                    /* Interrupt request register 9                                      */
   struct {
     unsigned char              :1; /* Nothing is assigned                                               */
     unsigned char  TM15R_PO15R :1; /* "tm15r"---II/O time measurement 0 interrupt
                                       "po15r"---II/O waveform generation function 0 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char CAN00R       :1; /* CAN0 communication function interrupt                             */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate." */

/*------------------------------------------------------
  Interrupt request register  10  //0x00AA
-----------------------------------------------------*/
union st_iio10ir{                   /* Interrupt request register 10                                      */
   struct {
     unsigned char              :1; /* Nothing is assigned                                               */
     unsigned char  TM16R_PO16R :1; /* "tm16r"---II/O time measurement 6 interrupt
                                       "po16r"---II/O waveform generation function 6 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  CAN01R      :1; /* CAN0 communication function interrupt                             */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate." */

/*------------------------------------------------------
  Interrupt request register  11  //0x00AB
-----------------------------------------------------*/
union st_iio11ir{                   /* Interrupt request register 11                                     */
   struct {
     unsigned char              :1; /* Nothing is assigned                                               */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  CAN02R      :1; /* CAN0 communication function interrupt                             */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate." */

/*------------------------------------------------------
  Interrupt enable register 0 //0x00B0
-----------------------------------------------------*/
union st_iio0ie{                    /* Interrupt enable register 0                                       */
   struct {
     unsigned char  IRLT        :1; /* Interrupt request select bit                                      */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  TM13E_PO13E :1; /* "tm13e"---II/O time measurement 3 interrupt
                                       "po13e"---II/O waveform generation function 3 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  G0RIE       :1; /* II/O communication unit 0 HDLC data processing function interrupt */
     unsigned char  SIO0RE      :1; /* II/O communication unit 0 receive interrupt                       */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  CAN10E      :1; /* CAN1 communication function interrupt                             */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};

/*------------------------------------------------------
  Interrupt enable register 1 //0x00B1
-----------------------------------------------------*/
union st_iio1ie{                    /* Interrupt enable register 1                                       */
   struct {
     unsigned char  IRLT        :1; /* Interrupt request select bit                                      */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  TM14E_PO14E :1; /* "tm14e"---II/O time measurement 4 interrupt
                                       "po14e"---II/O waveform generation function 4 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  G0ROE       :1; /* II/O communication unit 0 HDLC data processing function interrupt */
     unsigned char  SIO0TE      :1; /* II/O communication unit 0 transmit interrupt                       */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  CAN11E      :1; /* CAN1 communication function interrupt                             */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};

/*------------------------------------------------------
  Interrupt enable register 2 //0x00B2
-----------------------------------------------------*/
union st_iio2ie{                    /* Interrupt enable register 2                                       */
   struct {
     unsigned char  IRLT        :1; /* Interrupt request select bit                                      */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  TM12E_PO12E :1; /* "tm12e"---II/O time measurement 2 interrupt
                                       "po12e"---II/O waveform generation function 2 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  G1RIE       :1; /* II/O communication unit 1 HDLC data processing function interrupt */
     unsigned char  SIO1RE      :1; /* II/O communication unit 1 receive interrupt                       */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};

/*------------------------------------------------------
  Interrupt enable register 3 //0x00B3
-----------------------------------------------------*/
union st_iio3ie{                    /* Interrupt enable register 3                                       */
   struct {
     unsigned char  IRLT        :1; /* Interrupt request select bit                                      */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  TM10E_PO10E :1; /* "tm12e"---II/O time measurement 0 interrupt
                                       "po12e"---II/O waveform generation function 0 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  G1TOE       :1; /* II/O communication unit 1 HDLC data processing function interrupt */
     unsigned char  SIO1TE      :1; /* II/O communication unit 1 transmit interrupt                       */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};

/*------------------------------------------------------
  Interrupt enable register 4 //0x00B4
-----------------------------------------------------*/
union st_iio4ie{                    /* Interrupt enable register 4                                       */
   struct {
     unsigned char  IRLT        :1; /* Interrupt request select bit                                      */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  TM17E_PO17E :1; /* "tm17e"---II/O time measurement 7 interrupt
                                       "po17e"---II/O waveform generation function 7 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  BT1E        :1; /* II/O communication base timer interrupt                           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  SRT1E       :1; /* II/O special communication function interrupt                     */
     unsigned char  SRT0E       :1; /* II/O special communication function interrupt                     */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};

/*------------------------------------------------------
  Interrupt enable register 5 //0x00B5
-----------------------------------------------------*/
union st_iio5ie{                    /* Interrupt enable register 5                                       */
   struct {
     unsigned char  IRLT        :1; /* Interrupt request select bit                                      */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  CAN1WUE     :1; /* CAN1 wake-up interrupt                                            */
     unsigned char  CAN12E      :1; /* CAN1 communication function interrupt                             */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};

/*------------------------------------------------------
  Interrupt enable register 8 //0x00B8
-----------------------------------------------------*/
union st_iio8ie{                    /* Interrupt enable register 8                                       */
   struct {
     unsigned char  IRLT        :1; /* Interrupt request select bit                                      */
     unsigned char  TM11E_PO11E :1; /* "tm11e"---II/O time measurement 1 interrupt
                                       "po11e"---II/O waveform generation function 1 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};

/*------------------------------------------------------
  Interrupt enable register 9 //0x00B9
-----------------------------------------------------*/
union st_iio9ie{                    /* Interrupt enable register 9                                       */
   struct {
     unsigned char  IRLT        :1; /* Interrupt request select bit                                      */
     unsigned char  TM15E_PO15E :1; /* "tm15e"---II/O time measurement 5 interrupt
                                       "po15e"---II/O waveform generation function 5 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  CAN00E      :1; /* CAN0 communication function interrupt                             */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};

/*------------------------------------------------------
  Interrupt enable register 10 //0x00BA
-----------------------------------------------------*/
union st_iio10ie{                   /* Interrupt enable register 10                                      */
   struct {
     unsigned char  IRLT        :1; /* Interrupt request select bit                                      */
     unsigned char  TM16E_PO16E :1; /* "tm16e"---II/O time measurement 6 interrupt
                                       "po16e"---II/O waveform generation function 6 interrupt           */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  CAN01E      :1; /* CAN0 communication function interrupt                             */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};

/*------------------------------------------------------
  Interrupt enable register 11 //0x00BB
-----------------------------------------------------*/
union st_iio11ie{                   /* Interrupt enable register 11                                      */
   struct {
     unsigned char  IRLT        :1; /* Interrupt request select bit                                      */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  CAN02E      :1; /* CAN0 communication function interrupt                             */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};

/*------------------------------------------------------
  SI/O receive buffer register 0 //0x00E8
-----------------------------------------------------*/
union st_g0rb {						/*  SI/O receive buffer register 0 	*/
    struct {
	 unsigned char 			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char  		:1;		/*  Nothing is assigned.      		*/
	 unsigned char  		:1;		/*  Nothing is assigned.	   		*/
	 unsigned char  		:1;		/*  Nothing is assigned. 	   		*/
 	 unsigned char          :1;     /* 	Nothing is assigned.			*/
 	 unsigned char  OER     :1;     /*  Overrun error flag 				*/
 	 unsigned char          :1;     /*  Nothing is assigned.			*/
 	 unsigned char          :1;     /*  Nothing is assigned. 			*/
 	 unsigned char          :1;     /*  Nothing is assigned. 			*/
    }BIT;                           /*  Bit  Access 				 	*/
    struct{
     unsigned char G0RBL;        	/* Low  8 bit 						*/
     unsigned char G0RBH;           /* High 8 bit 						*/
    }BYTE;                          /*  Byte  Access 				 	*/
    unsigned short  WORD;           /*  Word Access 				 	*/
};
/*when nothing assigned then "When read, its content is indeterminate." */

/*------------------------------------------------------
  SI/O Communication Mode Register 0 //0x00ED
-----------------------------------------------------*/
union st_g0mr{                      /* SI/O Communication Mode Register 0                                */
   struct {
     unsigned char  GMD0        :1; /* Communication mode select bit                                     */
     unsigned char  GMD1        :1; /* Communication mode select bit                                     */
     unsigned char  CKDIR       :1; /* Internal/external clock select bit                                */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char              :1; /* Reserved bit "set to 0 "                                          */
     unsigned char  UFORM       :1; /* Transfer Format select bit                                        */
     unsigned char  IRS         :1; /* Transmit interrupt cause select bit                               */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};

/*------------------------------------------------------
  SI/O Communication Control Register 0//0x00EF
-----------------------------------------------------*/
union st_g0cr{                      /* SI/O Communication Control Register 0                             */
   struct {
     unsigned char  TI          :1; /* Transmit buffer empty flag                                        */
     unsigned char  TXEPT       :1; /* Transmit register empty flag                                      */
     unsigned char  RI          :1; /* Receive complete flag                                             */
     unsigned char              :1; /* Nothing assigned                                                  */
     unsigned char  TE          :1; /* Transmit enable bit                                               */
     unsigned char  RE          :1; /* Receive enable bit                                                */
     unsigned char  IPOL        :1; /* ISRxD input polarity switch bit                                   */
     unsigned char  OPOL        :1; /* ISTxD output polarity switch bit                                  */
   } BIT;   		    	        /* Bit  Access 		        	                                     */
     unsigned char BYTE;            /* Byte Access 				                                         */
};
/* when nothing assigned then "When write, set to "0".When read, its contents is indeterminate."         */

/*------------------------------------------------------
  Communication Clock Select Register//0x00F6
-----------------------------------------------------*/
union st_ccs{                     /* SI/O Communication Control Register 0                             */
   struct {
     unsigned char  CCS0      :1; /* Communication unit 0 clock select bit                             */
     unsigned char  CCS1      :1; /* Communication unit 0 clock select bit                             */
     unsigned char  CCS2      :1; /* Communication unit 1 clock select bit                             */
     unsigned char  CCS3      :1; /* Communication unit 1 clock select bit                             */
     unsigned char            :1; /* Nothing is assigned                                               */
     unsigned char            :1; /* Nothing is assigned                                               */
     unsigned char            :1; /* Nothing is assigned                                               */
     unsigned char            :1; /* Nothing is assigned                                               */
   } BIT;   		    	      /* Bit  Access 		        	                                   */
     unsigned char BYTE;          /* Byte Access 				                                       */
};
/* when nothing assigned then "When write, set to "0".When read, its contents is indeterminate."       */

/*------------------------------------------------------
  Receive CRC Code Register 0//0x00F8
-----------------------------------------------------*/
union st_g0rcrc {				/* Receive CRC Code Register 0 */
   struct{
	unsigned char G0RCRCL;      /* Receive CRC Code Register 0 low  8 bit 	 */
	unsigned char G0RCRCH;      /* Receive CRC Code Register 0 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Transmit CRC Code Register 0//0x00FA
-----------------------------------------------------*/
union st_g0tcrc {				/* Transmit CRC Code Register 0 */
   struct{
	unsigned char G0TCRCL;      /* Transmit CRC Code Register 0 low  8 bit 	 */
	unsigned char G0TCRCH;      /* Transmit CRC Code Register 0 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  SI/O Expansion Mode Register 0//0x00FC
-----------------------------------------------------*/
union st_g0emr{                /* SI/O Expansion Mode Register 0           */
   struct {
     unsigned char         :1; /* Reserved bit must be set to 0            */
     unsigned char  CRCV   :1; /* CRC default value select bit             */
     unsigned char  ACRC   :1; /* CRC reset select bit                     */
     unsigned char  BSINT  :1; /* Bit stuffing error interrupt select bit  */
     unsigned char  RXSL   :1; /* Receive source switch bit                */
     unsigned char  TXSL   :1; /* Transmit source switch bit               */
     unsigned char  CRC0   :1; /* CRC generation polynomial select bit     */
     unsigned char  CRC1   :1; /* CRC generation polynomial select bit     */
   } BIT;   		    	   /* Bit  Access 		        	           */
     unsigned char BYTE;       /* Byte Access 				               */
};

/*------------------------------------------------------
  SI/O Expansion Receive Control Register 0//0x00FD
-----------------------------------------------------*/
union st_g0erc{                /* SI/O Expansion Receive Control Register 0  */
   struct {
     unsigned char  CMP0E  :1; /* Data compare function 0 select bit         */
     unsigned char  CMP1E  :1; /* Data compare function 1 select bit         */
     unsigned char  CMP2E  :1; /* Data compare function 2 select bit         */
     unsigned char  CMP3E  :1; /* Data compare function 3 select bit         */
     unsigned char  RCRCE  :1; /* Receive CRC enable bit                     */
     unsigned char  RSHTE  :1; /* Receive shift operation enable bit         */
     unsigned char  RBSF0  :1; /* Receive bit stuffing "1" delete select bit */
     unsigned char  RBSF1  :1; /* Receive bit stuffing "0" delete select bit */
   } BIT;   		    	   /* Bit  Access 		        	             */
     unsigned char BYTE;       /* Byte Access 				                 */
};

/*------------------------------------------------------
  SI/O special communication interrupt detect register 0//0x00FE
-----------------------------------------------------*/
union st_g0irf{                /* SI/O special communication interrupt detect register 0     */
   struct {
     unsigned char         :1; /* Reserved bit must be set to 0             */
     unsigned char         :1; /* Reserved bit must be set to 0             */
     unsigned char  BSERR  :1; /* Bit stuffing error detect flag            */
     unsigned char         :1; /* Reserved bit ust be set to 0                   */
     unsigned char  IRF0   :1; /* Interrupt cause determination flag 0      */
     unsigned char  IRF1   :1; /* Interrupt cause determination flag 1      */
     unsigned char  IRF2   :1; /* Interrupt cause determination flag 2      */
     unsigned char  IRF3   :1; /* Interrupt cause determination flag 3      */
   } BIT;   		    	   /* Bit  Access 		        	            */
     unsigned char BYTE;       /* Byte Access 				                */
};

/*------------------------------------------------------
  SI/O expansion transmit control register 0//0x00FF
-----------------------------------------------------*/
union st_g0etc{                /* SI/O expansion transmit control register 0  */
   struct {
     unsigned char         :1; /* Reserved bit must be set to 0               */
     unsigned char         :1; /* Reserved bit must be set to 0               */
     unsigned char         :1; /* Reserved bit must be set to 0               */
     unsigned char         :1; /* Reserved bit must be set to 0               */
     unsigned char  TCRCE  :1; /* Transmit CRC enable bit                     */
     unsigned char         :1; /* Reserved bit must be set to 0               */
     unsigned char  TBSF0  :1; /* Transmit bit stuffing "1" insert select bit */
     unsigned char  TBSF1  :1; /* Transmit bit stuffing "0" insert select bit */
   } BIT;   		    	   /* Bit  Access 		        	              */
     unsigned char BYTE;       /* Byte Access 				                  */
};

/*------------------------------------------------------
  Time Measurement Register 10//0x0100
 -----------------------------------------------------*/
union st_g1tm0 {				/* Time Measurement Register 10 */
   struct{
	unsigned char  G1TM0L;      /* Time Measurement Register 10 low  8 bit 	 */
	unsigned char  G1TM0H;      /* Time Measurement Register 10 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Waveform generate register 10//0x0100
-----------------------------------------------------*/
union st_g1po0 {				/* Waveform generate register 10 */
   struct{
	unsigned char  G1PO0L;      /* Waveform generate register 10 low  8 bit 	 */
	unsigned char  G1PO0H;      /* Waveform generate register 10 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Time Measurement Register 11//0x0102
-----------------------------------------------------*/
union st_g1tm1 {				/* Time Measurement Register 11 */
   struct{
	unsigned char  G1TM1L;      /* Time Measurement Register 11 low  8 bit 	 */
	unsigned char  G1TM1H;      /* Time Measurement Register 11 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Waveform generate register 11//0x0102
-----------------------------------------------------*/
union st_g1po1 {				/* Waveform generate register 11 */
   struct{
	unsigned char  G1PO0L;      /* Waveform generate register 11 low  8 bit 	 */
	unsigned char  G1PO0H;      /* Waveform generate register 11 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Time Measurement Register 12//0x0104
-----------------------------------------------------*/
union st_g1tm2 {				/* Time Measurement Register 12 */
   struct{
	unsigned char  G1TM2L;      /* Time Measurement Register 12 low  8 bit 	 */
	unsigned char  G1TM2H;      /* Time Measurement Register 12 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Waveform generate register 12//0x0104
-----------------------------------------------------*/
union st_g1po2 {				/* Waveform generate register 12 */
   struct{
	unsigned char  G1PO2L;      /* Waveform generate register 12 low  8 bit 	 */
	unsigned char  G1PO2H;      /* Waveform generate register 12 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Time Measurement Register 13//0x0106
-----------------------------------------------------*/
union st_g1tm3 {				/* Time Measurement Register 13 */
   struct{
	unsigned char  G1TM3L;      /* Time Measurement Register 13 low  8 bit 	 */
	unsigned char  G1TM3H;      /* Time Measurement Register 13 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Waveform generate register 13//0x0106
-----------------------------------------------------*/
union st_g1po3 {				/* Waveform generate register 13 */
   struct{
	unsigned char  G1PO3L;      /* Waveform generate register 13 low  8 bit 	 */
	unsigned char  G1PO3H;      /* Waveform generate register 13 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Time Measurement Register 14//0x0108
-----------------------------------------------------*/
union st_g1tm4 {				/* Time Measurement Register 14 */
   struct{
	unsigned char  G1TM4L;      /* Time Measurement Register 14 low  8 bit 	 */
	unsigned char  G1TM4H;      /* Time Measurement Register 14 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Waveform generate register 14//0x0108
-----------------------------------------------------*/
union st_g1po4 {				/* Waveform generate register 14 */
   struct{
	unsigned char  G1PO4L;      /* Waveform generate register 14 low  8 bit 	 */
	unsigned char  G1PO4H;      /* Waveform generate register 14 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Time Measurement Register 15//0x010A
-----------------------------------------------------*/
union st_g1tm5 {				/* Time Measurement Register 15 */
   struct{
	unsigned char  G1TM5L;      /* Time Measurement Register 15 low  8 bit 	 */
	unsigned char  G1TM5H;      /* Time Measurement Register 15 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Waveform generate register 15//0x010A
-----------------------------------------------------*/
union st_g1po5 {				/* Waveform generate register 15 */
   struct{
	unsigned char  G1PO5L;      /* Waveform generate register 15 low  8 bit 	 */
	unsigned char  G1PO5H;      /* Waveform generate register 15 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Time Measurement Register 16//0x010C
-----------------------------------------------------*/
union st_g1tm6 {				/* Time Measurement Register 16 */
   struct{
	unsigned char  G1TM6L;      /* Time Measurement Register 16 low  8 bit 	 */
	unsigned char  G1TM6H;      /* Time Measurement Register 16 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Waveform generate register 16//0x010C
-----------------------------------------------------*/
union st_g1po6 {				/* Waveform generate register 16 */
   struct{
	unsigned char  G1PO6L;      /* Waveform generate register 16 low  8 bit 	 */
	unsigned char  G1PO6H;      /* Waveform generate register 16 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Time Measurement Register 17//0x010E
-----------------------------------------------------*/
union st_g1tm7 {				/* Time Measurement Register 17 */
   struct{
	unsigned char  G1TM7L;      /* Time Measurement Register 17 low  8 bit 	 */
	unsigned char  G1TM7H;      /* Time Measurement Register 17 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Waveform generate register 17//0x010E
-----------------------------------------------------*/
union st_g1po7 {				/* Waveform generate register 17 */
   struct{
	unsigned char  G1PO7L;      /* Waveform generate register 17 low  8 bit 	 */
	unsigned char  G1PO7H;      /* Waveform generate register 17 high 8 bit 	 */
   } BYTE;					    /* Byte access					   				 */
   unsigned short   WORD;	    /* Word Access					   				 */
};

/*------------------------------------------------------
  Waveform Generation Control Register 10//0x0110
-----------------------------------------------------*/
union st_g1pocr0{             /* Waveform Generation Control Register 10       */
   struct {
     unsigned char  MOD0  :1; /* Operation mode select bit                     */
     unsigned char  MOD1  :1; /* Operation mode select bit                     */
     unsigned char  MOD2  :1; /* Operation mode select bit                     */
     unsigned char        :1; /* Nothing is assigned                           */
     unsigned char  IVL   :1; /* Output initial value select bit               */
     unsigned char  RLD   :1; /* G1POj register value reload timing select bit */
     unsigned char  BTRE  :1; /* Base timer reset enable bit                   */
     unsigned char  INV   :1; /* Inverse output function select bit            */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate."  */

/*------------------------------------------------------
  Waveform Generation Control Register 11//0x0111
-----------------------------------------------------*/
union st_g1pocr1{             /* Waveform Generation Control Register 11       */
   struct {
     unsigned char  MOD0  :1; /* Operation mode select bit                     */
     unsigned char  MOD1  :1; /* Operation mode select bit                     */
     unsigned char  MOD2  :1; /* Operation mode select bit                     */
     unsigned char        :1; /* Nothing is assigned                           */
     unsigned char  IVL   :1; /* Output initial value select bit               */
     unsigned char  RLD   :1; /* G1POj register value reload timing select bit */
     unsigned char  BTRE  :1; /* Base timer reset enable bit                   */
     unsigned char  INV   :1; /* Inverse output function select bit            */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate."  */

/*------------------------------------------------------
  Waveform Generation Control Register 12//0x0112
-----------------------------------------------------*/
union st_g1pocr2{             /* Waveform Generation Control Register 12       */
   struct {
     unsigned char  MOD0  :1; /* Operation mode select bit                     */
     unsigned char  MOD1  :1; /* Operation mode select bit                     */
     unsigned char  MOD2  :1; /* Operation mode select bit                     */
     unsigned char        :1; /* Nothing is assigned                           */
     unsigned char  IVL   :1; /* Output initial value select bit               */
     unsigned char  RLD   :1; /* G1POj register value reload timing select bit */
     unsigned char  BTRE  :1; /* Base timer reset enable bit                   */
     unsigned char  INV   :1; /* Inverse output function select bit            */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate."  */

/*------------------------------------------------------
  Waveform Generation Control Register 13//0x0113
-----------------------------------------------------*/
union st_g1pocr3{             /* Waveform Generation Control Register 13       */
   struct {
     unsigned char  MOD0  :1; /* Operation mode select bit                     */
     unsigned char  MOD1  :1; /* Operation mode select bit                     */
     unsigned char  MOD2  :1; /* Operation mode select bit                     */
     unsigned char        :1; /* Nothing is assigned                           */
     unsigned char  IVL   :1; /* Output initial value select bit               */
     unsigned char  RLD   :1; /* G1POj register value reload timing select bit */
     unsigned char  BTRE  :1; /* Base timer reset enable bit                   */
     unsigned char  INV   :1; /* Inverse output function select bit            */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate."  */

/*------------------------------------------------------
  Waveform Generation Control Register 14//0x0114
-----------------------------------------------------*/
union st_g1pocr4{             /* Waveform Generation Control Register 14       */
   struct {
     unsigned char  MOD0  :1; /* Operation mode select bit                     */
     unsigned char  MOD1  :1; /* Operation mode select bit                     */
     unsigned char  MOD2  :1; /* Operation mode select bit                     */
     unsigned char        :1; /* Nothing is assigned                           */
     unsigned char  IVL   :1; /* Output initial value select bit               */
     unsigned char  RLD   :1; /* G1POj register value reload timing select bit */
     unsigned char  BTRE  :1; /* Base timer reset enable bit                   */
     unsigned char  INV   :1; /* Inverse output function select bit            */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate."  */

/*------------------------------------------------------
  Waveform Generation Control Register 15//0x0115
-----------------------------------------------------*/
union st_g1pocr5{             /* Waveform Generation Control Register 15       */
   struct {
     unsigned char  MOD0  :1; /* Operation mode select bit                     */
     unsigned char  MOD1  :1; /* Operation mode select bit                     */
     unsigned char  MOD2  :1; /* Operation mode select bit                     */
     unsigned char        :1; /* Nothing is assigned                           */
     unsigned char  IVL   :1; /* Output initial value select bit               */
     unsigned char  RLD   :1; /* G1POj register value reload timing select bit */
     unsigned char  BTRE  :1; /* Base timer reset enable bit                   */
     unsigned char  INV   :1; /* Inverse output function select bit            */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate."  */

/*------------------------------------------------------
  Waveform Generation Control Register 16//0x0116
-----------------------------------------------------*/
union st_g1pocr6{             /* Waveform Generation Control Register 16       */
   struct {
     unsigned char  MOD0  :1; /* Operation mode select bit                     */
     unsigned char  MOD1  :1; /* Operation mode select bit                     */
     unsigned char  MOD2  :1; /* Operation mode select bit                     */
     unsigned char        :1; /* Nothing is assigned                           */
     unsigned char  IVL   :1; /* Output initial value select bit               */
     unsigned char  RLD   :1; /* G1POj register value reload timing select bit */
     unsigned char  BTRE  :1; /* Base timer reset enable bit                   */
     unsigned char  INV   :1; /* Inverse output function select bit            */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate."  */

/*------------------------------------------------------
  Waveform Generation Control Register 17//0x0117
-----------------------------------------------------*/
union st_g1pocr7{             /* Waveform Generation Control Register 17       */
   struct {
     unsigned char  MOD0  :1; /* Operation mode select bit                     */
     unsigned char  MOD1  :1; /* Operation mode select bit                     */
     unsigned char  MOD2  :1; /* Operation mode select bit                     */
     unsigned char        :1; /* Nothing is assigned                           */
     unsigned char  IVL   :1; /* Output initial value select bit               */
     unsigned char  RLD   :1; /* G1POj register value reload timing select bit */
     unsigned char  BTRE  :1; /* Base timer reset enable bit                   */
     unsigned char  INV   :1; /* Inverse output function select bit            */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate."  */

/*------------------------------------------------------
  Time Measurement Control Register 10//0x0118
-----------------------------------------------------*/
union st_g1tmcr0{             /* Time Measurement Control Register 10          */
   struct {
     unsigned char  CTS0  :1; /* Time measurement trigger select bit           */
     unsigned char  CTS1  :1; /* Time measurement trigger select bit           */
     unsigned char  DF0   :1; /* Digital filter function select bit            */
     unsigned char  DF1   :1; /* Digital filter function select bit            */
     unsigned char  GT    :1; /* Gate function select bit                      */
     unsigned char  GOC   :1; /* Gate function clear select bit                */
     unsigned char  GSC   :1; /* Gate function clear bit                       */
     unsigned char  PR    :1; /* Prescaler function select bit                 */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};

/*------------------------------------------------------
  Time Measurement Control Register 11//0x0119
-----------------------------------------------------*/
union st_g1tmcr1{             /* Time Measurement Control Register 11       */
   struct {
     unsigned char  CTS0  :1; /* Time measurement trigger select bit           */
     unsigned char  CTS1  :1; /* Time measurement trigger select bit           */
     unsigned char  DF0   :1; /* Digital filter function select bit            */
     unsigned char  DF1   :1; /* Digital filter function select bit            */
     unsigned char  GT    :1; /* Gate function select bit                      */
     unsigned char  GOC   :1; /* Gate function clear select bit                */
     unsigned char  GSC   :1; /* Gate function clear bit                       */
     unsigned char  PR    :1; /* Prescaler function select bit                 */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};

/*------------------------------------------------------
  Time Measurement Control Register 12//0x011A
-----------------------------------------------------*/
union st_g1tmcr2{             /* Time Measurement Control Register 12       */
   struct {
     unsigned char  CTS0  :1; /* Time measurement trigger select bit           */
     unsigned char  CTS1  :1; /* Time measurement trigger select bit           */
     unsigned char  DF0   :1; /* Digital filter function select bit            */
     unsigned char  DF1   :1; /* Digital filter function select bit            */
     unsigned char  GT    :1; /* Gate function select bit                      */
     unsigned char  GOC   :1; /* Gate function clear select bit                */
     unsigned char  GSC   :1; /* Gate function clear bit                       */
     unsigned char  PR    :1; /* Prescaler function select bit                 */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};

/*------------------------------------------------------
  Time Measurement Control Register 13//0x011B
-----------------------------------------------------*/
union st_g1tmcr3{             /* Time Measurement Control Register 13       */
   struct {
     unsigned char  CTS0  :1; /* Time measurement trigger select bit           */
     unsigned char  CTS1  :1; /* Time measurement trigger select bit           */
     unsigned char  DF0   :1; /* Digital filter function select bit            */
     unsigned char  DF1   :1; /* Digital filter function select bit            */
     unsigned char  GT    :1; /* Gate function select bit                      */
     unsigned char  GOC   :1; /* Gate function clear select bit                */
     unsigned char  GSC   :1; /* Gate function clear bit                       */
     unsigned char  PR    :1; /* Prescaler function select bit                 */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};

/*------------------------------------------------------
  Time Measurement Control Register 14//0x011C
-----------------------------------------------------*/
union st_g1tmcr4{             /* Time Measurement Control Register 14       */
   struct {
     unsigned char  CTS0  :1; /* Time measurement trigger select bit           */
     unsigned char  CTS1  :1; /* Time measurement trigger select bit           */
     unsigned char  DF0   :1; /* Digital filter function select bit            */
     unsigned char  DF1   :1; /* Digital filter function select bit            */
     unsigned char  GT    :1; /* Gate function select bit                      */
     unsigned char  GOC   :1; /* Gate function clear select bit                */
     unsigned char  GSC   :1; /* Gate function clear bit                       */
     unsigned char  PR    :1; /* Prescaler function select bit                 */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};

/*------------------------------------------------------
  Time Measurement Control Register 15//0x011D
-----------------------------------------------------*/
union st_g1tmcr5{             /* Time Measurement Control Register 15       */
   struct {
     unsigned char  CTS0  :1; /* Time measurement trigger select bit           */
     unsigned char  CTS1  :1; /* Time measurement trigger select bit           */
     unsigned char  DF0   :1; /* Digital filter function select bit            */
     unsigned char  DF1   :1; /* Digital filter function select bit            */
     unsigned char  GT    :1; /* Gate function select bit                      */
     unsigned char  GOC   :1; /* Gate function clear select bit                */
     unsigned char  GSC   :1; /* Gate function clear bit                       */
     unsigned char  PR    :1; /* Prescaler function select bit                 */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};

/*------------------------------------------------------
  Time Measurement Control Register 16//0x011E
-----------------------------------------------------*/
union st_g1tmcr6{             /* Time Measurement Control Register 16       */
   struct {
     unsigned char  CTS0  :1; /* Time measurement trigger select bit           */
     unsigned char  CTS1  :1; /* Time measurement trigger select bit           */
     unsigned char  DF0   :1; /* Digital filter function select bit            */
     unsigned char  DF1   :1; /* Digital filter function select bit            */
     unsigned char  GT    :1; /* Gate function select bit                      */
     unsigned char  GOC   :1; /* Gate function clear select bit                */
     unsigned char  GSC   :1; /* Gate function clear bit                       */
     unsigned char  PR    :1; /* Prescaler function select bit                 */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};

/*------------------------------------------------------
  Time Measurement Control Register 17//0x011F
-----------------------------------------------------*/
union st_g1tmcr7{             /* Time Measurement Control Register 17       */
   struct {
     unsigned char  CTS0  :1; /* Time measurement trigger select bit           */
     unsigned char  CTS1  :1; /* Time measurement trigger select bit           */
     unsigned char  DF0   :1; /* Digital filter function select bit            */
     unsigned char  DF1   :1; /* Digital filter function select bit            */
     unsigned char  GT    :1; /* Gate function select bit                      */
     unsigned char  GOC   :1; /* Gate function clear select bit                */
     unsigned char  GSC   :1; /* Gate function clear bit                       */
     unsigned char  PR    :1; /* Prescaler function select bit                 */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};

/*------------------------------------------------------
  Base Timer Register 1//0x0120
-----------------------------------------------------*/
union st_g1bt {				   /* Base Timer Register 1          */
   struct{
	unsigned char  G1BTL;      /* Base Timer Register 1  8 bit 	 */
	unsigned char  G1BTH;      /* Base Timer Register 1  8 bit 	 */
   } BYTE;					   /* Byte access					 */
   unsigned short   WORD;	   /* Word Access					 */
};

/*------------------------------------------------------
  Base Timer Control Register 10//0x0122
-----------------------------------------------------*/
union st_g1bcr0{              /* Base Timer Control Register 10                */
   struct {
     unsigned char  BCK0  :1; /* Count source select bit                       */
     unsigned char  BCK1  :1; /* Count source select bit                       */
     unsigned char  DIV0  :1; /* Count source division ratio select bit        */
     unsigned char  DIV1  :1; /* Count source division ratio select bit        */
     unsigned char  DIV2  :1; /* Count source division ratio select bit        */
     unsigned char  DIV3  :1; /* Count source division ratio select bit        */
     unsigned char  DIV4  :1; /* Count source division ratio select bit        */
     unsigned char  IT    :1; /* Base timer interrupt select bit               */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};

/*------------------------------------------------------
  Base Timer Control Register 11//0x0123
-----------------------------------------------------*/
union st_g1bcr1{              /* Base Timer Control Register 11                */
   struct {
     unsigned char        :1; /* Nothing is assigned                           */
     unsigned char  RST1  :1; /* Base timer reset cause select bit 1           */
     unsigned char  RST2  :1; /* Base timer reset cause select bit 2           */
     unsigned char        :1; /* Reserved bit must be set to 0                 */
     unsigned char  BTS   :1; /* Base timer start bit                          */
     unsigned char  UD0   :1; /* Counter increment/decrement control bit       */
     unsigned char  UD1   :1; /* Counter increment/decrement control bit       */
     unsigned char        :1; /* Nothing is assigned                           */
   } BIT;   		    	  /* Bit  Access 		        	               */
     unsigned char BYTE;      /* Byte Access 				                   */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate."  */

/*------------------------------------------------------
  Function Enable Register 1//0x0126
-----------------------------------------------------*/
union st_g1fe{               /* Function Enable Register 1    */
   struct {
     unsigned char  IFE0 :1; /* Channel 0 function enable bit */
     unsigned char  IFE1 :1; /* Channel 1 function enable bit */
     unsigned char  IFE2 :1; /* Channel 2 function enable bit */
     unsigned char  IFE3 :1; /* Channel 3 function enable bit */
     unsigned char  IFE4 :1; /* Channel 4 function enable bit */
     unsigned char  IFE5 :1; /* Channel 5 function enable bit */
     unsigned char  IFE6 :1; /* Channel 6 function enable bit */
     unsigned char  IFE7 :1; /* Channel 7 function enable bit */
   } BIT;   		    	 /* Bit  Access 		          */
     unsigned char BYTE;     /* Byte Access 				  */
};

/*------------------------------------------------------
  Function Select Register 1//0x0127
-----------------------------------------------------*/
union st_g1fs{               /* Function Enable Register 1                                         */
   struct {
     unsigned char  FSC0 :1; /* Channel 0 time measurement/waveform generation function select bit */
     unsigned char  FSC1 :1; /* Channel 1 time measurement/waveform generation function select bit */
     unsigned char  FSC2 :1; /* Channel 2 time measurement/waveform generation function select bit */
     unsigned char  FSC3 :1; /* Channel 3 time measurement/waveform generation function select bit */
     unsigned char  FSC4 :1; /* Channel 4 time measurement/waveform generation function select bit */
     unsigned char  FSC5 :1; /* Channel 5 time measurement/waveform generation function select bit */
     unsigned char  FSC6 :1; /* Channel 6 time measurement/waveform generation function select bit */
     unsigned char  FSC7 :1; /* Channel 7 time measurement/waveform generation function select bit */
   } BIT;   		    	 /* Bit  Access 		                                               */
     unsigned char BYTE;     /* Byte Access 				                                       */
};

/*------------------------------------------------------
  SI/O receive buffer register 1 //0x0128
-----------------------------------------------------*/
union st_g1rb {						/*  SI/O receive buffer register 0 	*/
    struct {
	 unsigned char 			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char			:1;		/*  Receive data       				*/
	 unsigned char  		:1;		/*  Nothing is assigned.      		*/
	 unsigned char  		:1;		/*  Nothing is assigned.	   		*/
	 unsigned char  		:1;		/*  Nothing is assigned. 	   		*/
 	 unsigned char          :1;     /* 	Nothing is assigned.			*/
 	 unsigned char  OER     :1;     /*  Overrun error flag 				*/
 	 unsigned char  FER     :1;     /*  Framing error flag  			*/
 	 unsigned char          :1;     /*  Nothing is assigned. 			*/
 	 unsigned char          :1;     /*  Nothing is assigned. 			*/
    }BIT;                           /*  Bit  Access 				 	*/
    struct{
     unsigned char G1RBL;        	/* Low  8 bit 						*/
     unsigned char G1RBH;           /* High 8 bit 						*/
    }BYTE;                          /*  Byte  Access 				 	*/
    unsigned short  WORD;           /*  Word Access 				 	*/
};
/*when nothing assigned then "When read, its content is indeterminate." */

/*------------------------------------------------------
  SI/O Communication Mode Register 1//0x012D
-----------------------------------------------------*/
union st_g1mr{               /* SI/O Communication Mode Register 1  */
   struct {
     unsigned char  GMD0 :1; /* Communication mode select bit       */
     unsigned char  GMD1 :1; /* Communication mode select bit       */
     unsigned char  CKDIR:1; /* Internal/external clock select bit  */
     unsigned char  STPS :1; /* Stop bit length select bit          */
     unsigned char  PRY  :1; /* Odd/Even parity select bit          */
     unsigned char  PRYE :1; /* Parity enable bit                   */
     unsigned char  UFORM:1; /* Transfer direction select bit       */
     unsigned char  IRS  :1; /* Transmit interrupt cause select bit */
   } BIT;   		    	 /* Bit  Access 		                */
     unsigned char BYTE;     /* Byte Access 				        */
};

/*------------------------------------------------------
  SI/O Communication Control Register 1//0x012F
-----------------------------------------------------*/
union st_g1cr{               /* SI/O Communication Control Register 1  */
   struct {
     unsigned char  TI   :1; /* Transmit buffer empty flag          */
     unsigned char  TXEPT:1; /* Transmit register empty flag        */
     unsigned char  RI   :1; /* Receive complete flag               */
     unsigned char       :1; /* Nothing is assigned                 */
     unsigned char  TE   :1; /* Transmit enable bit                 */
     unsigned char  RE   :1; /* Receive enable bit                  */
     unsigned char  IPOL :1; /* ISRxD input polarity switch bit     */
     unsigned char  OPOL :1; /* ISTxD output polarity switch bit    */
   } BIT;   		    	 /* Bit  Access 		                */
     unsigned char BYTE;     /* Byte Access 				        */
};
/* when nothing is assigned then, when write, set to "0".When read, its contents is indeterminate.*/
/*------------------------------------------------------
  Receive CRC code register 1//0x0138
-----------------------------------------------------*/
union st_g1rcrc {				 /* Receive CRC code register 1                  */
   struct{
	unsigned char  G1RCRCL;      /* Receive CRC code register 1       8 bit 	 */
	unsigned char  G1RCRCH;      /* Receive CRC code register 1       8 bit 	 */
   } BYTE;					     /* Byte access					   				 */
   unsigned short   WORD;	     /* Word Access					   				 */
};

/*------------------------------------------------------
  Transmit CRC code register 1//0x013A
-----------------------------------------------------*/
union st_g1tcrc {				 /* Transmit CRC code register 1                  */
   struct{
	unsigned char  G1TCRCL;      /* Transmit CRC code register 1       8 bit 	 */
	unsigned char  G1TCRCH;      /* Transmit CRC code register 1       8 bit 	 */
   } BYTE;					     /* Byte access					   				 */
   unsigned short   WORD;	     /* Word Access					   				 */
};

/*------------------------------------------------------
  SI/O Expansion Mode Register 1//0x013C
-----------------------------------------------------*/
union st_g1emr{              /* SI/O Expansion Mode Register 1          */
   struct {
     unsigned char  SMODE:1; /* Synchronouse mode select bit            */
     unsigned char  CRCV :1; /* CRC initial value select bit            */
     unsigned char  ACRC :1; /* CRC initialization select bit           */
     unsigned char  BSINT:1; /* Bit stuffing error interrupt select bit */
     unsigned char  RXSL :1; /* Receive source switch bit               */
     unsigned char  TXSL :1; /* Transmit source switch bit              */
     unsigned char  CRC0 :1; /* CRC generation polynomial select bit    */
     unsigned char  CRC1 :1; /* CRC generation polynomial select bit    */
   } BIT;   		    	 /* Bit  Access 		                    */
     unsigned char BYTE;     /* Byte Access 				            */
};

/*------------------------------------------------------
  SI/O Expansion Receive Control Register 1//0x013D
-----------------------------------------------------*/
union st_g1erc{              /* SI/O Expansion Receive Control Register 1          */
   struct {
     unsigned char  CMP0E:1; /* Data compare function 0 select bit                 */
     unsigned char  CMP1E:1; /* Data compare function 1 select bit                 */
     unsigned char  CMP2E:1; /* Data compare function 2 select bit                 */
     unsigned char  CMP3E:1; /* Data compare function 3 select bit                 */
     unsigned char  RCRCE:1; /* Receive CRC enable bit                             */
     unsigned char  RSHTE:1; /* Receive shift operation enable bit                 */
     unsigned char  RBSF0:1; /* Receive bit stuffing "1" delete select bit         */
     unsigned char  RBSF1:1; /* Receive bit stuffing "0" delete select bit         */
   } BIT;   		    	 /* Bit  Access 		                               */
     unsigned char BYTE;     /* Byte Access 				                       */
};

/*------------------------------------------------------
  SI/O Special Communication Interrupt Detect Register 1//0x013E
-----------------------------------------------------*/
union st_g1irf{              /* SI/O Expansion Receive Control Register 1          */
   struct {
     unsigned char       :1; /* Reserved bit must be set to 0                      */
     unsigned char       :1; /* Reserved bit must be set to 0                      */
     unsigned char  BSERR:1; /* Bit stuffing error detect flag                     */
     unsigned char  ABT  :1; /* Arbitration lost detect flag                       */
     unsigned char  IRF0 :1; /* Interrupt cause determination flag 0               */
     unsigned char  IRF1 :1; /* Interrupt cause determination flag 1               */
     unsigned char  IRF2 :1; /* Interrupt cause determination flag 2               */
     unsigned char  IRF3 :1; /* Interrupt cause determination flag 3               */
   } BIT;   		    	 /* Bit  Access 		                               */
     unsigned char BYTE;     /* Byte Access 				                       */
};

/*------------------------------------------------------
  SI/O Expansion Transmit Control Register 1//0x013F
-----------------------------------------------------*/
union st_g1etc{              /* SI/O Expansion Receive Control Register 1           */
   struct {
     unsigned char        :1; /* Reserved bit must be set to 0                      */
     unsigned char        :1; /* Reserved bit must be set to 0                      */
     unsigned char        :1; /* Reserved bit must be set to 0                      */
     unsigned char  SOF   :1; /* SOF transmit request bit                           */
     unsigned char  TCRCE :1; /* Transmit CRC enable bit                            */
     unsigned char  ABTE  :1; /* Arbitration enable bit                             */
     unsigned char  TBSF0 :1; /* Transmit bit stuffing "1" insert select bit        */
     unsigned char  TBSF1 :1; /* Transmit bit stuffing "0" insert select bit        */
   } BIT;   		    	 /* Bit  Access 		                                */
     unsigned char BYTE;     /* Byte Access 				                        */
};

/*------------------------------------------------------
    Input Function Select Register //0x0178//
------------------------------------------------------*/
union st_ips {        /* Input Function Select Register                */
	struct {
	 unsigned char 	IPS0 :1;  /* Communication unit 0 input pin select bit 0   */
	 unsigned char 	IPS1 :1;  /* Communication unit 1 input pin select bit 1   */
	 unsigned char 	IPS2 :1;  /* Port P15 input peripheral function select bit */
	 unsigned char  IPS3 :1;  /* CAN0in function pin select bit                */
	 unsigned char       :1;  /* Reserved bit must be set to 0                 */
	 unsigned char       :1;  /* Reserved bit must be set to 0                 */
	 unsigned char       :1;  /* Reserved bit must be set to 0                 */
	 unsigned char 	     :1;  /* Reserved bit must be set to 0                 */
	  	} BIT;        /* Bit  Access                         	       */
	 char BYTE       ; /* Byte Access 	             	               */
};

/*------------------------------------------------------
    Input Function Select Register A//0x0179//
------------------------------------------------------*/
union st_ipsa {         /* Input Function Select Register A        */
	struct {
	 unsigned char 	IPSA_0 :1;  /* Intelligent I/O Two-Phase Pulse Input Pin and Base Timer Reset Pin Switch Bit */
	 unsigned char 	       :1;  /* Reserved bit must be set to 0           */
	 unsigned char 	       :1;  /* Reserved bit must be set to 0           */
	 unsigned char  IPSA_3 :1;  /* CAN1in function pin select bit          */
	 unsigned char         :1;  /* Reserved bit must be set to 0           */
	 unsigned char         :1;  /* Reserved bit must be set to 0           */
	 unsigned char         :1;  /* Reserved bit must be set to 0           */
	 unsigned char 	       :1;  /* Reserved bit must be set to 0           */
	  	} BIT;                  /* Bit  Access                  	       */
	 char BYTE               ;  /* Byte Access 	             	           */
};

/*------------------------------------------------------
    CAN0 message slot buffer 0 standard(ID0)//0x01E0//
------------------------------------------------------*/
union st_c0slot0_0 {    /* CAN0 message slot buffer 0 standard(ID0) */
	struct {
	 unsigned char 	 SID6  :1;  /* Standard ID6 	           */
	 unsigned char 	 SID7  :1;  /* Standard ID7	               */
	 unsigned char 	 SID8  :1;  /* Standard ID8                */
	 unsigned char   SID9  :1;  /* Standard ID9                */
	 unsigned char   SID10 :1;  /* Standard ID10               */
	 unsigned char         :1;  /* Nothing assigned            */
	 unsigned char         :1;  /* Nothing assigned            */
	 unsigned char 	       :1;  /* Nothing assigned            */
	  	} BIT;          /* Bit  Access      	       */
	 char BYTE         ; /* Byte Access 		           */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    CAN0 message slot buffer 0 standard(ID1)//0x01E1//
------------------------------------------------------*/
union st_c0slot0_1 {            /* CAN0 message slot buffer 0 standard(ID1) */
	struct {
	 unsigned char 	 SID0  :1;  /* Standard ID0 	           */
	 unsigned char 	 SID1  :1;  /* Standard ID1	               */
	 unsigned char 	 SID2  :1;  /* Standard ID2                */
	 unsigned char   SID3  :1;  /* Standard ID3                */
	 unsigned char   SID4  :1;  /* Standard ID4                */
	 unsigned char   SID5  :1;  /* Standard ID5                */
	 unsigned char         :1;  /* Nothing assigned            */
	 unsigned char 	       :1;  /* Nothing assigned            */
	  	} BIT;          /* Bit  Access      	       */
	 char BYTE          ; /* Byte Access 		           */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------
           CAN0 message slot buffer 0 extended(ID0)//0x01E2//
----------------------------------------------------------------*/
union st_c0slot0_2 {             /* CAN0 message slot buffer 0 extended(ID0) */
	struct {
	 unsigned char 	 EID14  :1;  /* Extended ID14	           */
	 unsigned char 	 EID15  :1;  /* Extended ID15              */
	 unsigned char 	 EID16  :1;  /* Extended ID16              */
	 unsigned char   EID17  :1;  /* Extended ID17              */
	 unsigned char          :1;  /* Nothing assigned           */
	 unsigned char          :1;  /* Nothing assigned           */
	 unsigned char          :1;  /* Nothing assigned           */
	 unsigned char 	        :1;  /* Nothing assigned           */
	  	} BIT;           /* Bit  Access      	       */
	 char BYTE          ; /* Byte Access 		       */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/


/*---------------------------------------------------------------
           CAN0 message slot buffer 0 extended(ID1)//0x01E3
----------------------------------------------------------------*/
union st_c0slot0_3 {     /* CAN0 message slot buffer 0 extended(ID1) */
	struct {
	 unsigned char 	 EID6   :1;  /* Extended ID6	           */
	 unsigned char 	 EID7   :1;  /* Extended ID7               */
	 unsigned char 	 EID8   :1;  /* Extended ID8               */
	 unsigned char   EID9   :1;  /* Extended ID9               */
	 unsigned char   EID10  :1;  /* Extended ID10              */
	 unsigned char   EID11  :1;  /* Extended ID11              */
	 unsigned char   EID12  :1;  /* Extended ID12              */
	 unsigned char 	 EID13  :1;  /* Extended ID13              */
	  	} BIT;           /* Bit  Access      	       */
	 char BYTE          ; /* Byte Access 		       */
};

/*---------------------------------------------------------------
           CAN0 message slot buffer 0 extended(ID2)//0x01E4
----------------------------------------------------------------*/
union st_c0slot0_4 {     /* CAN0 message slot buffer 0 extended(ID2) */
	struct {
	 unsigned char 	 EID0   :1;  /* Extended ID0	               */
	 unsigned char 	 EID1   :1;  /* Extended ID1                   */
	 unsigned char 	 EID2   :1;  /* Extended ID2                   */
	 unsigned char   EID3   :1;  /* Extended ID3                   */
	 unsigned char   EID4   :1;  /* Extended ID4                   */
	 unsigned char   EID5   :1;  /* Extended ID5                   */
	 unsigned char          :1;  /* Nothing assigned               */
	 unsigned char 	        :1;  /* Nothing assigned               */
	  	} BIT;                   /* Bit  Access      	           */
	 char BYTE          ;        /* Byte Access 		           */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-----------------------------------------------------------------------
         CAN0 message slot buffer 0 data length code//0x01E5//
--------------------------------------------------------------------------*/
union st_c0slot0_5 {              /* CAN0 message slot buffer 0 data length code */
	struct {
	 unsigned char   DCL0    :1;  /* Data length set bit            */
	 unsigned char   DCL1    :1;  /* Data length set bit            */
	 unsigned char   DCL2    :1;  /* Data length set bit            */
	 unsigned char   DCL3    :1;  /* Data length set bit            */
	 unsigned char           :1;  /* Nothing assigned               */
	 unsigned char           :1;  /* Nothing assigned               */
	 unsigned char           :1;  /* Nothing assigned               */
	 unsigned char 	         :1;  /* Nothing assigned               */
	  	} BIT;                    /* Bit  Access      	            */
	 char BYTE                 ;   /* Byte Access 		            */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    CAN0 message slot buffer 1 standard(ID0)//0x01F0
------------------------------------------------------*/
union st_c0slot1_0 {            /* CAN0 message slot buffer 1 standard(ID0) */
	struct {
	 unsigned char 	 SID6  :1;  /* Standard ID6 	           */
	 unsigned char 	 SID7  :1;  /* Standard ID7	               */
	 unsigned char 	 SID8  :1;  /* Standard ID8                */
	 unsigned char   SID9  :1;  /* Standard ID9                */
	 unsigned char   SID10 :1;  /* Standard ID10               */
	 unsigned char         :1;  /* Nothing assigned            */
	 unsigned char         :1;  /* Nothing assigned            */
	 unsigned char 	       :1;  /* Nothing assigned            */
	  	} BIT;                  /* Bit  Access      	       */
	 char BYTE               ;  /* Byte Access 		           */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    CAN0 message slot buffer 1 standard(ID1)//0x01F1//
------------------------------------------------------*/
union st_c0slot1_1 {            /* CAN0 message slot buffer 1 standard(ID1) */
	struct {
	 unsigned char 	 SID0  :1;  /* Standard ID0 	           */
	 unsigned char 	 SID1  :1;  /* Standard ID1	               */
	 unsigned char 	 SID2  :1;  /* Standard ID2                */
	 unsigned char   SID3  :1;  /* Standard ID3                */
	 unsigned char   SID4  :1;  /* Standard ID4                */
	 unsigned char   SID5  :1;  /* Standard ID5                */
	 unsigned char         :1;  /* Nothing assigned            */
	 unsigned char 	       :1;  /* Nothing assigned            */
	  	} BIT;                  /* Bit  Access      	       */
	 char BYTE               ;  /* Byte Access 		           */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------
           CAN0 message slot buffer 1 extended(ID0)//0x01F2//
 ---------------------------------------------------------------*/
union st_c0slot1_2 {             /* CAN0 message slot buffer 1 extended(ID0) */
	struct {
	 unsigned char 	 EID14  :1;  /* Extended ID14	           */
	 unsigned char 	 EID15  :1;  /* Extended ID15              */
	 unsigned char 	 EID16  :1;  /* Extended ID16              */
	 unsigned char   EID17  :1;  /* Extended ID17              */
	 unsigned char          :1;  /* Nothing assigned           */
	 unsigned char          :1;  /* Nothing assigned           */
	 unsigned char          :1;  /* Nothing assigned           */
	 unsigned char 	        :1;  /* Nothing assigned           */
	  	} BIT;                  /* Bit  Access      	       */
	 char BYTE                ;  /* Byte Access 		       */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/


/*---------------------------------------------------------------
           CAN0 message slot buffer 1 extended(ID1)//0x01F3//
----------------------------------------------------------------*/
union st_c0slot1_3 {             /* CAN0 message slot buffer 1 extended(ID1) */
	struct {
	 unsigned char 	 EID6   :1;  /* Extended ID6	           */
	 unsigned char 	 EID7   :1;  /* Extended ID7               */
	 unsigned char 	 EID8   :1;  /* Extended ID8               */
	 unsigned char   EID9   :1;  /* Extended ID9               */
	 unsigned char   EID10  :1;  /* Extended ID10              */
	 unsigned char   EID11  :1;  /* Extended ID11              */
	 unsigned char   EID12  :1;  /* Extended ID12              */
	 unsigned char 	 EID13  :1;  /* Extended ID13              */
	  	} BIT;                   /* Bit  Access      	       */
	 char BYTE                ;  /* Byte Access 		       */
};

/*---------------------------------------------------------------
           CAN0 message slot buffer 1 extended(ID2)//0x01F4//
----------------------------------------------------------------*/
union st_c0slot1_4 {             /* CAN0 message slot buffer 1 extended(ID2) */
	struct {
	 unsigned char 	 EID0   :1;  /* Extended ID0	               */
	 unsigned char 	 EID1   :1;  /* Extended ID1                   */
	 unsigned char 	 EID2   :1;  /* Extended ID2                   */
	 unsigned char   EID3   :1;  /* Extended ID3                   */
	 unsigned char   EID4   :1;  /* Extended ID4                   */
	 unsigned char   EID5   :1;  /* Extended ID5                   */
	 unsigned char          :1;  /* Nothing assigned               */
	 unsigned char 	        :1;  /* Nothing assigned               */
	  	} BIT;                   /* Bit  Access      	           */
	 char BYTE                ;  /* Byte Access 		           */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-----------------------------------------------------------------------
         CAN0 message slot buffer 1 data length code//0x01F5//
--------------------------------------------------------------------------*/
union st_c0slot1_5 {     /* CAN0 message slot buffer 1 data length code */
	struct {
	 unsigned char   DCL0    :1;  /* Data length set bit            */
	 unsigned char   DCL1    :1;  /* Data length set bit            */
	 unsigned char   DCL2    :1;  /* Data length set bit            */
	 unsigned char   DCL3    :1;  /* Data length set bit            */
	 unsigned char           :1;  /* Nothing assigned               */
	 unsigned char           :1;  /* Nothing assigned               */
	 unsigned char           :1;  /* Nothing assigned               */
	 unsigned char 	         :1;  /* Nothing assigned               */
	  	} BIT;                    /* Bit  Access      	           */
	 char BYTE                 ;   /* Byte Access 		           */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-----------------------------------------------------------------------
         CAN0 control register 0//0x0200-0x0201
--------------------------------------------------------------------------*/
union st_c0ctlr0{               /* CAN0 control register 0 */
	struct {
	   unsigned char    RESET0     :1;  /* Reset0                 */
       unsigned char    LOOPBACK   :1;  /* Loop Back Mode         */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char    BASICCAN   :1;  /* Basic CAN Mode         */
       unsigned char    RESET1     :1;  /* Reset1                 */
       unsigned char               :1;  /* Reserved bit set to 0  */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char    TSPRE0     :1;  /* Time Stamp Prescaler select bit  */
       unsigned char    TSPRE1     :1;  /* Time Stamp Prescaler select bit  */
       unsigned char    TSRESET    :1;  /* Time Stamp Reset bit             */
       unsigned char    ECRESET    :1;  /* Error Counter Reset              */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char               :1;  /* Nothing assigned       */
	  	} BIT;                           /* Bit  Access 	          */

	 struct{
		        char  C0CTLR0L;    /* Low  8 bit             */
		        char  C0CTLR0H;   /* High 8 bit             */
    }BYTE;                      /* Byte Access            */
    unsigned short  WORD;
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    CAN0 status register//0x0202-0x0203
------------------------------------------------------*/
union st_c0str{                       /* CAN0 status register  */
    struct{
       unsigned char    MBOX0          :1;    /* Active Slot Determination Bit   */
       unsigned char    MBOX1          :1;    /* Active Slot Determination Bit   */
       unsigned char    MBOX2          :1;    /* Active Slot Determination Bit   */
       unsigned char    MBOX3          :1;    /* Active Slot Determination Bit   */
       unsigned char    TRMSUCC        :1;    /* Transmit Complete State Flag    */
       unsigned char    RECSUCC        :1;    /* Receive Complete State Flag     */
       unsigned char    TRMSTATE       :1;    /* Transmit State Flag             */
       unsigned char    RECSTATE       :1;    /* Receive State Flag              */
       unsigned char    STATE_RESET    :1;    /* CAN Reset State Flag            */
       unsigned char    STATE_LOOPBACK :1;    /* Loop Back State Flag            */
       unsigned char                   :1;    /* Nothing assigned                */
       unsigned char    STATE_BASICCAN :1;    /* BasicCAN State Flag             */
       unsigned char    STATE_BUSERROR :1;    /* CAN Bus Error State Flag        */
       unsigned char    STATE_ERRPAS   :1;    /* Error Passive State Flag        */
       unsigned char    STATE_BUSOFF   :1;    /* BusOff State Flag               */
       unsigned char                   :1;    /* Nothing assigned                */
    }BIT;                                     /* Bit access                      */
    struct{
        char   C0STRL;                        /* Low  8 bit            */
        char   C0STRH;                        /* High 8 bit            */
    }BYTE;                                    /* Byte access           */
    unsigned short  WORD;                     /* Word access           */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/


/*------------------------------------------------------
    CAN0 extended ID register //0x0204-0x0205
 ------------------------------------------------------*/
union st_c0idr{            /* CAN0 extended ID register*/
    struct{
       unsigned char    IDE15 :1;  /* For Slot15 */
       unsigned char    IDE14 :1;  /* For Slot14 */
       unsigned char    IDE13 :1;  /* For Slot13 */
       unsigned char    IDE12 :1;  /* For Slot12 */
       unsigned char    IDE11 :1;  /* For Slot11 */
       unsigned char    IDE10 :1;  /* For Slot10 */
       unsigned char    IDE9  :1;  /* For Slot9  */
       unsigned char    IDE8  :1;  /* For Slot8  */
       unsigned char    IDE7  :1;  /* For Slot7  */
       unsigned char    IDE6  :1;  /* For Slot6  */
       unsigned char    IDE5  :1;  /* For Slot5  */
       unsigned char    IDE4  :1;  /* For Slot4  */
       unsigned char    IDE3  :1;  /* For Slot3  */
       unsigned char    IDE2  :1;  /* For Slot2  */
       unsigned char    IDE1  :1;  /* For Slot1  */
       unsigned char    IDE0  :1;  /* For Slot0  */
    }BIT;                  /* Bit access */
    struct{
        char    LOW;       /* Low  8 bit */
        char    HIGH;      /* High 8 bit */
    }BYTE;                 /* Byte access*/
    unsigned short  WORD;
};

/*------------------------------------------------------
    CAN0 configuration register//0x0206-0x0207
------------------------------------------------------*/
union st_c0conr{                     /* CAN0 configuration register    */
    struct{
       unsigned char            :1;  /* Nothing assigned               */
       unsigned char            :1;  /* Nothing assigned               */
       unsigned char            :1;  /* Nothing assigned               */
       unsigned char            :1;  /* Nothing assigned               */
       unsigned char    SAM     :1;  /* Sampling Number                */
       unsigned char    PTS0    :1;  /* Propagation Time Segment       */
       unsigned char    PTS1    :1;  /* Propagation Time Segment       */
       unsigned char    PTS2    :1;  /* Propagation Time Segment       */
       unsigned char    PBS10   :1;  /* Phase Buffer Segment 1         */
       unsigned char    PBS11   :1;  /* Phase Buffer Segment 1         */
       unsigned char    PBS12   :1;  /* Phase Buffer Segment 1         */
       unsigned char    PBS20   :1;  /* Phase Buffer Segment 2         */
       unsigned char    PBS21   :1;  /* Phase Buffer Segment 2         */
       unsigned char    PBS22   :1;  /* Phase Buffer Segment 2         */
       unsigned char    SJW0    :1;  /* ReSynchronization Jump Width   */
       unsigned char    SJW1    :1;  /* ReSynchronization Jump Width   */
    }BIT;                            /* Bit access                     */
    struct{
        char    C0CONRL;             /* Low  8 bit                     */
        char    C0CONRH;             /* High 8 bit                     */
    }BYTE;                           /* Byte access                    */
    unsigned short  WORD;            /* Word access                    */
};

/*------------------------------------------------------
    CAN0 time stamp register //0x0208-0x0209
------------------------------------------------------*/
union st_c0tsr{                     /* CAN0 time stamp register*/
    struct{
       unsigned char    C0TSRL;     /* Low  8 bit  */
       unsigned char    C0TSRH;     /* High 8 bit  */
    }BYTE;                          /* Byte access */
    unsigned short  WORD;           /* Word access */
};


/*------------------------------------------------------
    CAN0 slot interrupt status register//0x020C-0x020D
------------------------------------------------------*/
union st_c0sistr{                     /* CAN0 slot interrupt status register*/
    struct{
        unsigned char    SIS15 :1;    /* For Slot15  */
        unsigned char    SIS14 :1;    /* For Slot14  */
        unsigned char    SIS13 :1;    /* For Slot13  */
        unsigned char    SIS12 :1;    /* For Slot12  */
        unsigned char    SIS11 :1;    /* For Slot11  */
        unsigned char    SIS10 :1;    /* For Slot10  */
        unsigned char    SIS9  :1;    /* For Slot9   */
        unsigned char    SIS8  :1;    /* For Slot8   */
        unsigned char    SIS7  :1;    /* For Slot7   */
        unsigned char    SIS6  :1;    /* For Slot6   */
        unsigned char    SIS5  :1;    /* For Slot5   */
        unsigned char    SIS4  :1;    /* For Slot4   */
        unsigned char    SIS3  :1;    /* For Slot3   */
        unsigned char    SIS2  :1;    /* For Slot2   */
        unsigned char    SIS1  :1;    /* For Slot1   */
        unsigned char    SIS0  :1;    /* For Slot0   */
    }BIT;                             /* Bit access  */
    struct{
        char    C0SISTRL;             /* Low  8 bit  */
        char    C0SISTRH;             /* High 8 bit  */
    }BYTE;                            /* Byte access */
    unsigned short  WORD;             /* Word access */
};

/*------------------------------------------------------
    CAN0 slot interrupt mask register//0x0210-0x0211
------------------------------------------------------*/
union st_c0simkr{            /*CAN0 slot interrupt mask register */
    struct{
        unsigned char    SIM15  :1;   /* For Slot15  */
        unsigned char    SIM14  :1;   /* For Slot14  */
        unsigned char    SIM13  :1;   /* For Slot13  */
        unsigned char    SIM12  :1;   /* For Slot12  */
        unsigned char    SIM11  :1;   /* For Slot11  */
        unsigned char    SIM10  :1;   /* For Slot10  */
        unsigned char    SIM9   :1;   /* For Slot9   */
        unsigned char    SIM8   :1;   /* For Slot8   */
        unsigned char    SIM7   :1;   /* For Slot7   */
        unsigned char    SIM6   :1;   /* For Slot6   */
        unsigned char    SIM5   :1;   /* For Slot5   */
        unsigned char    SIM4   :1;   /* For Slot4   */
        unsigned char    SIM3   :1;   /* For Slot3   */
        unsigned char    SIM2   :1;   /* For Slot2   */
        unsigned char    SIM1   :1;   /* For Slot1   */
        unsigned char    SIM0   :1;   /* For Slot0   */
    }BIT;                    /* Bit access  */
    struct{
        char    C0SIMKRL;    /* Low  8 bit  */
        char    C0SIMKRH;    /* High 8 bit  */
    }BYTE;                   /* Byte access */
    unsigned short  WORD;
};

/*------------------------------------------------------
     CAN0 error interrupt mask register//0x0214
------------------------------------------------------*/
union st_c0eimkr{            /* CAN0 error interrupt mask  register*/
    struct{
       unsigned char    BOIM   :1;   /* BusOff Interrupt         */
       unsigned char    EPIM   :1;   /* Error Passive Interrupt  */
       unsigned char    BEIM   :1;   /* CAN BusError Interrupt   */
       unsigned char           :1;   /* Nothing assigned         */
       unsigned char           :1;   /* Nothing assigned         */
       unsigned char           :1;   /* Nothing assigned         */
       unsigned char           :1;   /* Nothing assigned         */
       unsigned char           :1;   /* Nothing assigned         */
    }BIT;                            /* Bit access               */
    char    BYTE;                    /* Byte access              */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/


/*------------------------------------------------------
     CAN0 error interrupt status register//0x0215
------------------------------------------------------*/
union st_c0eistr{                    /* CAN0 error interrupt status register*/
    struct{
        unsigned char    BOIS  :1;   /* BusOff Interrupt          */
        unsigned char    EPIS  :1;   /* Error Passive Interrupt   */
        unsigned char    BEIS  :1;   /* CAN BusError Interrupt    */
        unsigned char          :1;   /* Nothing assigned          */
        unsigned char          :1;   /* Nothing assigned 		  */
        unsigned char          :1;   /* Nothing assigned 		  */
        unsigned char          :1;   /* Nothing assigned 		  */
        unsigned char          :1;   /* Nothing assigned 		  */
    }BIT;                            /* Bit access                */
    char    BYTE;                    /* Byte access               */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/


/*-------------------------------------------------------
    CAN0 error factor register //0x0216
---------------------------------------------------------*/
union st_c0efr{              /* CAN0 error factor register*/
    struct{
        unsigned char    ACKE  :1;    /* ACK Error Detect Bit      */
        unsigned char    CRCE  :1;    /* CRC Error Detect Bit      */
        unsigned char    FORME :1;    /* FORM Error Detect Bit     */
        unsigned char    STFE  :1;    /* Stuff Error Detect Bit    */
        unsigned char    BITE0 :1;    /* Bit Error Detect Bit 0    */
        unsigned char    BITE1 :1;    /* Bit Error Detect Bit 1    */
        unsigned char    RCVE  :1;    /* Receive Error Detect Bit  */
        unsigned char    TRE   :1;    /* Transmit Error Detect Bit */
    }BIT;                    /* Bit access                */
    char    BYTE;            /* Byte access               */
};

/*-------------------------------------------------------
    CAN0 mode register // 0x0219
-------------------------------------------------------*/
union st_c0mdr{                      /* CAN0 mode register*/
    struct{
       unsigned char    CMOD    :2;  /* CAN Operating Mode Select Bit */
       unsigned char            :1;  /* Nothing assigned              */
       unsigned char            :1;  /* Nothing assigned              */
       unsigned char            :1;  /* Nothing assigned              */
       unsigned char            :1;  /* Nothing assigned              */
       unsigned char            :1;  /* Nothing assigned              */
       unsigned char            :1;  /* Nothing assigned              */
       }BIT;                         /* Bit access                    */
    char    BYTE;                    /* Byte access                   */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    CAN0 single shot control register //0x0220-0x0221
------------------------------------------------------*/
union st_c0ssctlr{          /* CAN0 single shot control register*/
    struct{
       unsigned char    SSC15 :1;   /* For Slot15   */
       unsigned char    SSC14 :1;   /* For Slot14   */
       unsigned char    SSC13 :1;   /* For Slot13   */
       unsigned char    SSC12 :1;   /* For Slot12   */
       unsigned char    SSC11 :1;   /* For Slot11   */
       unsigned char    SSC10 :1;   /* For Slot10   */
       unsigned char    SSC9  :1;   /* For Slot9    */
       unsigned char    SSC8  :1;   /* For Slot8    */
       unsigned char    SSC7  :1;   /* For Slot7    */
       unsigned char    SSC6  :1;   /* For Slot6    */
       unsigned char    SSC5  :1;   /* For Slot5    */
       unsigned char    SSC4  :1;   /* For Slot4    */
       unsigned char    SSC3  :1;   /* For Slot3    */
       unsigned char    SSC2  :1;   /* For Slot2    */
       unsigned char    SSC1  :1;   /* For Slot1    */
       unsigned char    SSC0  :1;   /* For Slot0    */
    }BIT;                           /* Bit access   */
    struct{
        char    C0SSCTLRL;          /* Low  8 bit   */
        char    C0SSCTLRH;          /* High 8 bit   */
    }BYTE;                          /* Byte access  */
    unsigned short  WORD;
};


/*------------------------------------------------------
    CAN0 single shot status register// 0x0224-0x225
------------------------------------------------------*/
union st_c0ssstr{           /* CAN0 single shot status register*/
    struct{
       unsigned char    SSS15 :1;   /* For Slot15  */
       unsigned char    SSS14 :1;   /* For Slot14  */
       unsigned char    SSS13 :1;   /* For Slot13  */
       unsigned char    SSS12 :1;   /* For Slot12  */
       unsigned char    SSS11 :1;   /* For Slot11  */
       unsigned char    SSS10 :1;   /* For Slot10  */
       unsigned char    SSS9  :1;   /* For Slot9   */
       unsigned char    SSS8  :1;   /* For Slot8   */
       unsigned char    SSS7  :1;   /* For Slot7   */
       unsigned char    SSS6  :1;   /* For Slot6   */
       unsigned char    SSS5  :1;   /* For Slot5   */
       unsigned char    SSS4  :1;   /* For Slot4   */
       unsigned char    SSS3  :1;   /* For Slot3   */
       unsigned char    SSS2  :1;   /* For Slot2   */
       unsigned char    SSS1  :1;   /* For Slot1   */
       unsigned char    SSS0  :1;   /* For Slot0   */
    }BIT;                           /* Bit access  */
    struct{
       unsigned char    LOW;        /* Low  8 bit  */
       unsigned char    HIGH;       /* High 8 bit  */
    }BYTE;                          /* Byte access */
    unsigned short  WORD;
};

/*-------------------------------------------------------
    CAN0 Global Mask Register Standard ID0//0x0228
-------------------------------------------------------*/
union st_c0gmr0{             /* CAN0 Global Mask Register Standard ID0*/
    struct{
       unsigned char   SID6M  :1;    /* Standard ID6     */
       unsigned char   SID7M  :1;    /* Standard ID7     */
       unsigned char   SID8M  :1;    /* Standard ID8     */
       unsigned char   SID9M  :1;    /* Standard ID9     */
       unsigned char   SID10M :1;    /* Standard ID10    */
       unsigned char          :1;    /* Nothing assigned */
       unsigned char          :1;    /* Nothing assigned */
       unsigned char          :1;    /* Nothing assigned */
    }BIT;                    /* Bit access       */
    char    BYTE;            /* Byte access      */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-------------------------------------------------------
    CAN0 Global Mask Register Standard ID1//0x0229
-------------------------------------------------------*/
union st_c0gmr1{             /* CAN0 Global Mask Register Standard ID1*/
    struct{
       unsigned char   SID0M  :1;    /* Standard ID0     */
       unsigned char   SID1M  :1;    /* Standard ID1     */
       unsigned char   SID2M  :1;    /* Standard ID2     */
       unsigned char   SID3M  :1;    /* Standard ID3     */
       unsigned char   SID4M  :1;    /* Standard ID4     */
       unsigned char   SID5M  :1;    /* Standard ID5     */
       unsigned char          :1;    /* Nothing assigned */
       unsigned char          :1;    /* Nothing assigned */
    }BIT;                    /* Bit access       */
    char    BYTE;            /* Byte access      */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/


/*-------------------------------------------------------
    CAN0 Global Mask Register Extended ID0//0x022A
-------------------------------------------------------*/
union st_c0gmr2{             /* CAN0 Global Mask Register Extended ID0*/
    struct{
       unsigned char  EID14M  :1;    /* Extended ID14      */
       unsigned char  EID15M  :1;    /* Extended ID15      */
       unsigned char  EID16M  :1;    /* Extended ID16      */
       unsigned char  EID17M  :1;    /* Extended ID17      */
       unsigned char          :1;    /* Nothing assigned   */
       unsigned char          :1;    /* Nothing assigned   */
       unsigned char          :1;    /* Nothing assigned   */
       unsigned char          :1;    /* Nothing assigned   */
    }BIT;                    /* Bit access         */
    char    BYTE;            /* Byte access        */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-------------------------------------------------------
    CAN0 Global Mask Register Extended ID1//0x022B
-------------------------------------------------------*/
union st_c0gmr3{             /* CAN0 Global Mask Register Extended ID1 */
    struct{
       unsigned char  EID6M   :1;    /* Extended ID6       */
       unsigned char  EID7M   :1;    /* Extended ID7       */
       unsigned char  EID8M   :1;    /* Extended ID8       */
       unsigned char  EID9M   :1;    /* Extended ID9       */
       unsigned char  EID10M  :1;    /* Extended ID10      */
       unsigned char  EID11M  :1;    /* Extended ID11      */
       unsigned char  EID12M  :1;    /* Extended ID12      */
       unsigned char  EID13M  :1;    /* Extended ID13      */
    }BIT;                    /* Bit access         */
    char    BYTE;            /* Byte access        */
};


/*-------------------------------------------------------
    CAN0 Global Mask Register Extended ID2//0x022C
-------------------------------------------------------*/
union st_c0gmr4{             /* CAN0 Global Mask Register Extended ID2 */
    struct{
       unsigned char  EID0M   :1;    /* Extended ID0       */
       unsigned char  EID1M   :1;    /* Extended ID1       */
       unsigned char  EID2M   :1;    /* Extended ID2       */
       unsigned char  EID3M   :1;    /* Extended ID3       */
       unsigned char  EID4M   :1;    /* Extended ID4       */
       unsigned char  EID5M   :1;    /* Extended ID5       */
       unsigned char          :1;    /* Nothing assigned   */
       unsigned char          :1;    /* Nothing assigned   */
    }BIT;                    /* Bit access         */
    char    BYTE;            /* Byte access        */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-------------------------------------------------------------
           CAN0 Message Slot0 Control Register//0x230
---------------------------------------------------------------*/
union st_c0mctl0 {                                 /* CAN0 Message Slot0 Control Register           */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                          /* Bit access                                    */
    char    BYTE;                                  /* Byte access                                   */
};


/*-------------------------------------------------------------
           CAN0 Message Slot1 Control Register//0x231
---------------------------------------------------------------*/
union st_c0mctl1 {                                 /* CAN0 Message Slot1 Control Register           */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answeringz Disable Mode Select Bit  */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                  /* Bit access                                    */
    char    BYTE;                          /* Byte access                                   */
};


/*-------------------------------------------------------------
           CAN0 Message Slot2 Control Register//0x232
---------------------------------------------------------------*/
union st_c0mctl2 {                         /* CAN0 Message Slot2 Control Register           */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                  /* Bit access                                    */
    char    BYTE;                          /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN0 Message Slot3 Control Register//0x233
---------------------------------------------------------------*/
union st_c0mctl3 {                         /* CAN0 Message Slot3 Control Register           */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                  /* Bit access                                    */
    char    BYTE;                          /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN0 Message Slot4 Control Register//0x234
---------------------------------------------------------------*/
union st_c0mctl4{                         /* CAN0 Message Slot4 Control Register */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;   /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag             */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                               */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag    */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit  */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                         */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                          */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                         */
    }BIT;                                  /* Bit access                                   */
    char    BYTE;                          /* Byte access                                  */
};

/*-------------------------------------------------------------
           CAN0 Message Slot5 Control Register//0x235
---------------------------------------------------------------*/
union st_c0mctl5{                         /* CAN0 Message Slot5 Control Register           */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;   /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag             */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                               */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag    */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit  */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                         */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                          */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                         */
    }BIT;                                  /* Bit access                                   */
    char    BYTE;                          /* Byte access                                  */
};

/*-------------------------------------------------------------
           CAN0 Message Slot6 Control Register//0x236

---------------------------------------------------------------*/
union st_c0mctl6{                         /* CAN0 Message Slot6 Control Register */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;   /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;   /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;   /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag    */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit  */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                         */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                          */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                         */
    }BIT;                                  /* Bit access                                   */
    char    BYTE;                          /* Byte access                                  */
};

/*-------------------------------------------------------------
           CAN0 Message Slot7 Control Register//0x237
---------------------------------------------------------------*/
union st_c0mctl7{                         /* CAN0 Message Slot7 Control Register            */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                  /* Bit access                                    */
    char    BYTE;                          /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN0 Message Slot8 Control Register//0x238

---------------------------------------------------------------*/
union st_c0mctl8 {                         /* CAN0 Message Slot 8 Control Register          */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                  /* Bit access                                    */
    char    BYTE;                          /* Byte access                                   */
};


/*-------------------------------------------------------------
           CAN0 Message Slot9 Control Register//0x239

---------------------------------------------------------------*/
union st_c0mctl9{                         /* CAN0 Message Slot 9 Control Register          */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;   /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag             */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                               */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag    */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit  */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                         */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                          */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                         */
    }BIT;                                  /* Bit access                                   */
    char    BYTE;                          /* Byte access                                  */
};


/*-------------------------------------------------------------
           CAN0 Message Slot10 Control Register//0x23A
---------------------------------------------------------------*/
union st_c0mctl10{                         /* CAN0 Message Slot 10 Control Register         */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                  /* Bit access                                    */
    char    BYTE;                          /* Byte access                                   */
};


/*-------------------------------------------------------------
           CAN0 Message Slot 11 Control Register//0x23B
---------------------------------------------------------------*/
union st_c0mctl11{                         /* CAN0 Message Slot 11 Control Register         */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                  /* Bit access                                    */
    char    BYTE;                          /* Byte access                                   */
};


/*-------------------------------------------------------------
           CAN0 Message Slot12 Control Register//0x23C
---------------------------------------------------------------*/
union st_c0mctl12{                         /* CAN0 Message Slot 12 Control Register         */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                  /* Bit access                                    */
    char    BYTE;                          /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN0 Message Slot13 Control Register//0x23D
---------------------------------------------------------------*/
union st_c0mctl13{                         /* CAN0 Message Slot 13 Control Register         */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                  /* Bit access                                    */
    char    BYTE;                          /* Byte access                                   */
};


/*-------------------------------------------------------------
           CAN0 Message Slot14 Control Register//0x23E
---------------------------------------------------------------*/
union st_c0mctl14{                         /* CAN0 Message Slot 14 Control Register         */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                  /* Bit access                                    */
    char    BYTE;                          /* Byte access                                   */
};


/*-------------------------------------------------------------
           CAN0 Message Slot15 Control Register//0x23F
---------------------------------------------------------------*/
union st_c0mctl15{                         /* CAN0 Message Slot15 Control Register */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                  /* Bit access                                    */
    char    BYTE;                          /* Byte access                                   */
};

/*-------------------------------------------------------
    CAN0 slot buffer select register//0x0240
-------------------------------------------------------*/
union st_c0sbs {         /* CAN0 slot buffer select register	 */
	struct {
	//  char SBS0_0	  :1;/* Number Select Bit 	*/
	//  char SBS0_1	  :1;/* Number Select Bit	*/
	//  char SBS0_2	  :1;/* Number Select Bit	*/
	//  char SBS0_3	  :1;/* Number Select Bit	*/
	//  char SBS1_0	  :1;/* Number Select Bit	*/
	//  char SBS1_1	  :1;/* Number Select Bit	*/
	//  char SBS1_2	  :1;/* Number Select Bit	*/
	//  char SBS1_3	  :1;/* Number Select Bit	*/

	   unsigned char BUFFER0_0:4;/* Message Slot Buffer 0 Number Select Bit  */
       unsigned char BUFFER0_1:4;/* Message Slot Buffer 1 Number Select Bit  */
	} BIT;     	         /* Bit access        		          		 */
	 char BYTE;          /* Byte Access 			                 */
};                       /* 						                 */


/*-------------------------------------------------------
    CAN0 control register 1//0x0241
-------------------------------------------------------*/
union st_c0ctlr1 {       /* CAN0 slot buffer select register	 */
	struct {
	 unsigned char 	        :1;  /* Nothing assigned 			    	 */
	 unsigned char 	        :1;  /*	Nothing assigned		             */
	 unsigned char 	        :1;  /* Reserved bit must be set to 0        */
	 unsigned char BANKSEL  :1;  /* CAN0 Bank Switch Bit				 */
	 unsigned char 	        :1;  /* Reserved bit must be set to 0		 */
	 unsigned char 	        :1;  /* Reserved bit must be set to 0		 */
	 unsigned char INTSEL	:1;  /* Interrupt Mode Select Bit			 */
	 unsigned char 	        :1;  /* Nothing assigned			       	 */
	} BIT;     	                 /* Bit  Access      	         		 */
	 char BYTE;                  /* Byte Access 		     	         */
};
/* when nothing assigned then "When write, set to "0".When read, its content is indeterminate. "  */

/*-------------------------------------------------------
    CAN0 sleep control register//0x0242
-------------------------------------------------------*/
union st_c0slpr {               /* CAN1 sleep control register */
	struct {
	 unsigned char 	SLEEP   :1;  /* Sleep Mode Control Bit 	   */
	 unsigned char 	        :1;  /*	Nothing assigned		   */
	 unsigned char 	        :1;  /* Nothing assigned           */
	 unsigned char          :1;  /* Nothing assigned		   */
	 unsigned char          :1;  /* Nothing assigned	       */
	 unsigned char          :1;  /* Nothing assigned	       */
	 unsigned char          :1;  /* Nothing assigned	       */
	 unsigned char 	        :1;  /* Nothing assigned	       */
	} BIT;     	                 /* Bit  Access      	       */
	 char BYTE;                  /* Byte Access 		       */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-------------------------------------------------------
     CAN0 acceptance filter support register//0x0244-0x0245
-------------------------------------------------------*/
union st_c0afs {	         /* CAN0 acceptance filter support register    */
   struct{
	unsigned char C0AFSL;            /* Low  8 bit  */
	unsigned char C0AFSH;            /* High 8 bit  */
   } BYTE;				     /* Byte access */
   unsigned short   WORD;    /* Word access */
};

/*-------------------------------------------------------
    CAN1 slot buffer select register//0x0250
-------------------------------------------------------*/
union st_c1sbs {         /* CAN1 slot buffer select register	 */
	struct {
	//  char SBS0_0	  :1;/* Number Select Bit 	*/
	//  char SBS0_1	  :1;/* Number Select Bit	*/
	//  char SBS0_2	  :1;/* Number Select Bit	*/
	//  char SBS0_3	  :1;/* Number Select Bit	*/
	//  char SBS1_0	  :1;/* Number Select Bit	*/
	//  char SBS1_1	  :1;/* Number Select Bit	*/
	//  char SBS1_2	  :1;/* Number Select Bit	*/
	//  char SBS1_3	  :1;/* Number Select Bit	*/

	   unsigned char BUFFER1_0:4;/* Message Slot Buffer 0 Number Select Bit  */
       unsigned char BUFFER1_1:4;/* Message Slot Buffer 1 Number Select Bit  */
	} BIT;     	                 /* Bit access        		          		 */
	 char BYTE;                  /* Byte Access 			                 */
};

/*-------------------------------------------------------
    CAN1 control register 1//0x0251
-------------------------------------------------------*/
union st_c1ctlr1 {               /* CAN1 slot buffer select register	 */
	struct {
	 unsigned char 	        :1;  /* Nothing assigned 			    	 */
	 unsigned char 	        :1;  /*	Nothing assigned		             */
	 unsigned char 	        :1;  /* Reserved bit must be set to 0        */
	 unsigned char BANKSEL  :1;  /* CAN1 Bank Switch Bit				 */
	 unsigned char 	        :1;  /* Reserved bit must be set to 0		 */
	 unsigned char 	        :1;  /* Reserved bit must be set to 0		 */
	 unsigned char INTSEL	:1;  /* Interrupt Mode Select Bit			 */
	 unsigned char 	        :1;  /* Nothing assigned			       	 */
	} BIT;     	                 /* Bit  Access      	         		 */
	 char BYTE;                  /* Byte Access 		     	         */
};

/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-------------------------------------------------------
    CAN1 sleep control register//0x0252
-------------------------------------------------------*/
union st_c1slpr {        /* CAN1 sleep control register */
	struct {
	 unsigned char 	SLEEP   :1;  /* Sleep Mode Control Bit 	   */
	 unsigned char 	        :1;  /*	Nothing assigned		   */
	 unsigned char 	        :1;  /* Nothing assigned           */
	 unsigned char          :1;  /* Nothing assigned		   */
	 unsigned char          :1;  /* Nothing assigned	       */
	 unsigned char          :1;  /* Nothing assigned	       */
	 unsigned char          :1;  /* Nothing assigned	       */
	 unsigned char 	        :1;  /* Nothing assigned	       */
	} BIT;     	         /* Bit  Access      	       */
	 char BYTE;          /* Byte Access 		       */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-------------------------------------------------------
     CAN1 acceptance filter support register//0x0254
-------------------------------------------------------*/
union st_c1afs {	         /* CAN1 acceptance filter support register    */
   struct{
	unsigned char TCR0L;             /* Low  8 bit                                 */
	unsigned char TCR0H;             /* High 8 bit                                 */
   } BYTE;				     /* Byte access                                */
   unsigned short   WORD;    /* Word access                                */
};

/*------------------------------------------------------
    CAN1 message slot buffer 0 standard(ID0)//0x0260
------------------------------------------------------*/
union st_c1slot0_0 {    /* CAN1 message slot buffer 0 standard(ID0) */
	struct {
	 unsigned char 	 SID6  :1;  /* Standard ID6 	           */
	 unsigned char 	 SID7  :1;  /* Standard ID7	               */
	 unsigned char 	 SID8  :1;  /* Standard ID8                */
	 unsigned char   SID9  :1;  /* Standard ID9                */
	 unsigned char   SID10 :1;  /* Standard ID10               */
	 unsigned char         :1;  /* Nothing assigned            */
	 unsigned char         :1;  /* Nothing assigned            */
	 unsigned char 	       :1;  /* Nothing assigned            */
	  	} BIT;          /* Bit  Access      	       */
	 char BYTE         ; /* Byte Access 		           */
};
/*when nothing assigned then "when write, set to "0".When read, its content is indeterminate."  */

/*------------------------------------------------------
    CAN1 message slot buffer 0 standard(ID1)//0x0261
------------------------------------------------------*/
union st_c1slot0_1 {    /* CAN1 message slot buffer 0 standard(ID1) */
	struct {
	 unsigned char 	 SID0  :1;  /* Standard ID0 	           */
	 unsigned char 	 SID1  :1;  /* Standard ID1	               */
	 unsigned char 	 SID2  :1;  /* Standard ID2                */
	 unsigned char   SID3  :1;  /* Standard ID3                */
	 unsigned char   SID4  :1;  /* Standard ID4                */
	 unsigned char   SID5  :1;  /* Standard ID5                */
	 unsigned char         :1;  /* Nothing assigned            */
	 unsigned char 	       :1;  /* Nothing assigned            */
	  	} BIT;          /* Bit  Access      	       */
	 char BYTE         ; /* Byte Access 		           */
};
/*when nothing assigned then " when write, set to "0".When read, its content is indeterminate." */

/*---------------------------------------------------------------
           CAN1 message slot buffer 0 extended(ID0)//0x0262
----------------------------------------------------------------*/
union st_c1slot0_2 {             /* CAN1 message slot buffer 0 extended(ID0) */
	struct {
	 unsigned char 	 EID14  :1;  /* Extended ID14	           */
	 unsigned char 	 EID15  :1;  /* Extended ID15              */
	 unsigned char 	 EID16  :1;  /* Extended ID16              */
	 unsigned char   EID17  :1;  /* Extended ID17              */
	 unsigned char          :1;  /* Nothing assigned           */
	 unsigned char          :1;  /* Nothing assigned           */
	 unsigned char          :1;  /* Nothing assigned           */
	 unsigned char 	        :1;  /* Nothing assigned           */
	  	} BIT;                   /* Bit  Access      	       */
	 char BYTE               ;    /* Byte Access 		       */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------
           CAN1 message slot buffer 0 extended(ID1)//0x0263
----------------------------------------------------------------*/
union st_c1slot0_3 {             /* CAN1 message slot buffer 0 extended(ID1) */
	struct {
	 unsigned char 	 EID6   :1;  /* Extended ID6	           */
	 unsigned char 	 EID7   :1;  /* Extended ID7               */
	 unsigned char 	 EID8   :1;  /* Extended ID8               */
	 unsigned char   EID9   :1;  /* Extended ID9               */
	 unsigned char   EID10  :1;  /* Extended ID10              */
	 unsigned char   EID11  :1;  /* Extended ID11              */
	 unsigned char   EID12  :1;  /* Extended ID12              */
	 unsigned char 	 EID13  :1;  /* Extended ID13              */
	  	} BIT;                   /* Bit  Access      	       */
	 char BYTE                ;   /* Byte Access 		       */
};


/*---------------------------------------------------------------
           CAN1 message slot buffer 0 extended(ID2)//0x0264
----------------------------------------------------------------*/
union st_c1slot0_4 {             /* CAN1 message slot buffer 0 extended(ID2) */
	struct {
	 unsigned char 	 EID0   :1;  /* Extended ID0	               */
	 unsigned char 	 EID1   :1;  /* Extended ID1                   */
	 unsigned char 	 EID2   :1;  /* Extended ID2                   */
	 unsigned char   EID3   :1;  /* Extended ID3                   */
	 unsigned char   EID4   :1;  /* Extended ID4                   */
	 unsigned char   EID5   :1;  /* Extended ID5                   */
	 unsigned char          :1;  /* Nothing assigned               */
	 unsigned char 	        :1;  /* Nothing assigned               */
	  	} BIT;                   /* Bit  Access      	           */
	 char BYTE                ;   /* Byte Access 		           */
};
/*when nothing assigned then " when write, set to "0".When read, its content is indeterminate."  */

/*-----------------------------------------------------------------------
         CAN1 message slot buffer 0 data length code//0x0265
--------------------------------------------------------------------------*/
union st_c1slot0_5 {              /* CAN1 message slot buffer 0 data length code */
	struct {
	 unsigned char   DCL0    :1;  /* Data length set bit            */
	 unsigned char   DCL1    :1;  /* Data length set bit            */
	 unsigned char   DCL2    :1;  /* Data length set bit            */
	 unsigned char   DCL3    :1;  /* Data length set bit            */
	 unsigned char           :1;  /* Nothing assigned               */
	 unsigned char           :1;  /* Nothing assigned               */
	 unsigned char           :1;  /* Nothing assigned               */
	 unsigned char 	         :1;  /* Nothing assigned               */
	  	} BIT;                    /* Bit  Access      	            */
	 char BYTE                 ;   /* Byte Access 		           */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    CAN1 message slot buffer 1 standard(ID0)//0x0270
------------------------------------------------------*/
union st_c1slot1_0 {            /* CAN1 message slot buffer 1 standard(ID0) */
	struct {
	 unsigned char 	 SID6  :1;  /* Standard ID6 	           */
	 unsigned char 	 SID7  :1;  /* Standard ID7	               */
	 unsigned char 	 SID8  :1;  /* Standard ID8                */
	 unsigned char   SID9  :1;  /* Standard ID9                */
	 unsigned char   SID10 :1;  /* Standard ID10               */
	 unsigned char         :1;  /* Nothing assigned            */
	 unsigned char         :1;  /* Nothing assigned            */
	 unsigned char 	       :1;  /* Nothing assigned            */
	  	} BIT;                  /* Bit  Access      	       */
	 char BYTE               ;   /* Byte Access 		           */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    CAN1 message slot buffer 1 standard(ID1)//0x0271
------------------------------------------------------*/
union st_c1slot1_1 {            /* CAN1 message slot buffer 1 standard(ID1) */
	struct {
	 unsigned char 	 SID0  :1;  /* Standard ID0 	           */
	 unsigned char 	 SID1  :1;  /* Standard ID1	               */
	 unsigned char 	 SID2  :1;  /* Standard ID2                */
	 unsigned char   SID3  :1;  /* Standard ID3                */
	 unsigned char   SID4  :1;  /* Standard ID4                */
	 unsigned char   SID5  :1;  /* Standard ID5                */
	 unsigned char         :1;  /* Nothing assigned            */
	 unsigned char 	       :1;  /* Nothing assigned            */
	  	} BIT;                  /* Bit  Access      	       */
	 char BYTE               ;   /* Byte Access 		           */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------
           CAN1 message slot buffer 1 extended(ID0)//0x0272
----------------------------------------------------------------*/
union st_c1slot1_2 {             /* CAN1 message slot buffer 1 extended(ID0) */
	struct {
	 unsigned char 	 EID14  :1;  /* Extended ID14	           */
	 unsigned char 	 EID15  :1;  /* Extended ID15              */
	 unsigned char 	 EID16  :1;  /* Extended ID16              */
	 unsigned char   EID17  :1;  /* Extended ID17              */
	 unsigned char          :1;  /* Nothing assigned           */
	 unsigned char          :1;  /* Nothing assigned           */
	 unsigned char          :1;  /* Nothing assigned           */
	 unsigned char 	        :1;  /* Nothing assigned           */
	  	} BIT;                   /* Bit  Access      	       */
	 char BYTE                ;   /* Byte Access 		       */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------
           CAN1 message slot buffer 1 extended(ID1)//0x0273
----------------------------------------------------------------*/
union st_c1slot1_3 {             /* CAN1 message slot buffer 1 extended(ID1) */
	struct {
	 unsigned char 	 EID6   :1;  /* Extended ID6	           */
	 unsigned char 	 EID7   :1;  /* Extended ID7               */
	 unsigned char 	 EID8   :1;  /* Extended ID8               */
	 unsigned char   EID9   :1;  /* Extended ID9               */
	 unsigned char   EID10  :1;  /* Extended ID10              */
	 unsigned char   EID11  :1;  /* Extended ID11              */
	 unsigned char   EID12  :1;  /* Extended ID12              */
	 unsigned char 	 EID13  :1;  /* Extended ID13              */
	  	} BIT;                   /* Bit  Access      	       */
	 char BYTE                ;   /* Byte Access 		       */
};

/*---------------------------------------------------------------
           CAN1 message slot buffer 1 extended(ID2)//0x0274
----------------------------------------------------------------*/
union st_c1slot1_4 {             /* CAN1 message slot buffer 1 extended(ID2) */
	struct {
	 unsigned char 	 EID0   :1;  /* Extended ID0	               */
	 unsigned char 	 EID1   :1;  /* Extended ID1                   */
	 unsigned char 	 EID2   :1;  /* Extended ID2                   */
	 unsigned char   EID3   :1;  /* Extended ID3                   */
	 unsigned char   EID4   :1;  /* Extended ID4                   */
	 unsigned char   EID5   :1;  /* Extended ID5                   */
	 unsigned char          :1;  /* Nothing assigned               */
	 unsigned char 	        :1;  /* Nothing assigned               */
	  	} BIT;                   /* Bit  Access      	           */
	 char BYTE                ;   /* Byte Access 		           */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-----------------------------------------------------------------------
         CAN1 message slot buffer 1 data length code//0x0275
--------------------------------------------------------------------------*/
union st_c1slot1_5 {              /* CAN1 message slot buffer 1 data length code */
	struct {
	 unsigned char   DCL0    :1;  /* Data length set bit            */
	 unsigned char   DCL1    :1;  /* Data length set bit            */
	 unsigned char   DCL2    :1;  /* Data length set bit            */
	 unsigned char   DCL3    :1;  /* Data length set bit            */
	 unsigned char           :1;  /* Nothing assigned               */
	 unsigned char           :1;  /* Nothing assigned               */
	 unsigned char           :1;  /* Nothing assigned               */
	 unsigned char 	         :1;  /* Nothing assigned               */
	  	} BIT;                    /* Bit  Access      	            */
	 char BYTE                 ;   /* Byte Access 		            */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-----------------------------------------------------------------------
         CAN1 control register 0//0x0280-0x0281
--------------------------------------------------------------------------*/
union st_c1ctlr0{                       /* CAN1 message slot buffer 1 data length code */
	struct {
	   unsigned char    RESET0     :1;  /* Reset0                 */
       unsigned char    LOOPBACK   :1;  /* Loop Back Mode         */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char    BASICCAN   :1;  /* Basic CAN Mode         */
       unsigned char    RESET1     :1;  /* Reset1                 */
       unsigned char               :1;  /* Reserved bitn set to 0 */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char    TSPRE0     :1;  /* Time Stamp Prescaler   */
       unsigned char    TSPRE1     :1;  /* Time Stamp Prescaler   */
       unsigned char    TSRESET    :1;  /* Time Stamp Reset       */
       unsigned char    ECRESET    :1;  /* Error Counter Reset    */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char               :1;  /* Nothing assigned       */
       unsigned char               :1;  /* Nothing assigned       */

	  	} BIT;                           /* Bit  Access 	          */

	 struct{
		        char    LOW;            /* Low  8 bit             */
		        char    HIGH;           /* High 8 bit             */
    }BYTE;                              /* Byte Access            */
    unsigned short  WORD;
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    CAN1 status register//0x0282-0x0283
------------------------------------------------------*/
union st_c1str{                       /* CAN1 status register  */
    struct{
       unsigned char    MBOX0           :1;    /* Message Slot Number   */
       unsigned char    MBOX1           :1;    /* Message Slot Number   */
       unsigned char    MBOX2           :1;    /* Message Slot Number   */
       unsigned char    MBOX3           :1;    /* Message Slot Number   */
       unsigned char    TRMSUCC         :1;    /* Transmit Success      */
       unsigned char    RECSUCC         :1;    /* Receive Success       */
       unsigned char    TRMSTATE        :1;    /* Transmit Status       */
       unsigned char    RECSTATE        :1;    /* Receive Status        */
       unsigned char    STATE_RESET     :1;    /* CAN Reset Status      */
       unsigned char    STATE_LOOPBACK  :1;    /* LoopBack Status       */
       unsigned char    DUMMY0          :1;    /* Nothing assigned      */
       unsigned char    STATE_BASICCAN  :1;    /* BasicCAN Status       */
       unsigned char    STATE_BUSERROR  :1;    /* CAN BusError          */
       unsigned char    STATE_ERRPAS    :1;    /* Error Passive Status  */
       unsigned char    STATE_BUSOFF    :1;    /* BusOff Status         */
       unsigned char    DUMMY1          :1;    /* Nothing assigned      */
    }BIT;                              /* Bit access            */
    struct{
        char    LOW;                  /* Low  8 bit            */
        char    HIGH;                 /* High 8 bit            */
    }BYTE;                            /* Byte access           */
    unsigned short  WORD;
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    CAN1 extended ID register //0x0284-0x0285
------------------------------------------------------*/
union st_c1idr{                    /* CAN1 extended ID register*/
    struct{
       unsigned char    IDE15 :1;  /* For Slot15 */
       unsigned char    IDE14 :1;  /* For Slot14 */
       unsigned char    IDE13 :1;  /* For Slot13 */
       unsigned char    IDE12 :1;  /* For Slot12 */
       unsigned char    IDE11 :1;  /* For Slot11 */
       unsigned char    IDE10 :1;  /* For Slot10 */
       unsigned char    IDE9  :1;  /* For Slot9  */
       unsigned char    IDE8  :1;  /* For Slot8  */
       unsigned char    IDE7  :1;  /* For Slot7  */
       unsigned char    IDE6  :1;  /* For Slot6  */
       unsigned char    IDE5  :1;  /* For Slot5  */
       unsigned char    IDE4  :1;  /* For Slot4  */
       unsigned char    IDE3  :1;  /* For Slot3  */
       unsigned char    IDE2  :1;  /* For Slot2  */
       unsigned char    IDE1  :1;  /* For Slot1  */
       unsigned char    IDE0  :1;  /* For Slot0  */
    }BIT;                          /* Bit access */
    struct{
        char    LOW;               /* Low  8 bit */
        char    HIGH;              /* High 8 bit */
    }BYTE;                         /* Byte access*/
    unsigned short  WORD;
};

/*------------------------------------------------------
    CAN1 configuration register//0x0286-0x0287
------------------------------------------------------*/
union st_c1conr{                         /* CAN1 configuration register    */
    struct{
	       unsigned char            :1;  /* Nothing assigned               */
	       unsigned char            :1;  /* Nothing assigned               */
	       unsigned char            :1;  /* Nothing assigned               */
	       unsigned char            :1;  /* Nothing assigned               */
	       unsigned char    SAM     :1;  /* Sampling Number                */
	       unsigned char    PTS0    :1;  /* Propagation Time Segment       */
	       unsigned char    PTS1    :1;  /* Propagation Time Segment       */
	       unsigned char    PTS2    :1;  /* Propagation Time Segment       */
	       unsigned char    PBS10   :1;  /* Phase Buffer Segment 1         */
	       unsigned char    PBS11   :1;  /* Phase Buffer Segment 1         */
	       unsigned char    PBS12   :1;  /* Phase Buffer Segment 1         */
	       unsigned char    PBS20   :1;  /* Phase Buffer Segment 2         */
	       unsigned char    PBS21   :1;  /* Phase Buffer Segment 2         */
	       unsigned char    PBS22   :1;  /* Phase Buffer Segment 2         */
	       unsigned char    SJW0    :1;  /* ReSynchronization Jump Width   */
           unsigned char    SJW1    :1;  /* ReSynchronization Jump Width   */
    }BIT;                                /* Bit access                     */
    struct{
        char    LOW;                     /* Low  8 bit                     */
        char    HIGH;                    /* High 8 bit                     */
    }BYTE;                               /* Byte access                    */
    unsigned short  WORD;
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/
/*------------------------------------------------------
    CAN1 time stamp register //0x0288-0x0289
------------------------------------------------------*/
union st_c1tsr{                     /* CAN1 time stamp register*/
    struct{
       unsigned char    LOW;        /* Low  8 bit  */
       unsigned char    HIGH;       /* High 8 bit  */
    }BYTE;                          /* Byte access */
    unsigned short  WORD;
};

/*------------------------------------------------------
    CAN1 slot interrupt status register//0x028C-0x028D
------------------------------------------------------*/
union st_c1sistr{                    /* CAN1 slot interrupt status register*/
    struct{
       unsigned char    SIS15 :1;    /* For Slot15  */
       unsigned char    SIS14 :1;    /* For Slot14  */
       unsigned char    SIS13 :1;    /* For Slot13  */
       unsigned char    SIS12 :1;    /* For Slot12  */
       unsigned char    SIS11 :1;    /* For Slot11  */
       unsigned char    SIS10 :1;    /* For Slot10  */
       unsigned char    SIS9  :1;    /* For Slot9   */
       unsigned char    SIS8  :1;    /* For Slot8   */
       unsigned char    SIS7  :1;    /* For Slot7   */
       unsigned char    SIS6  :1;    /* For Slot6   */
       unsigned char    SIS5  :1;    /* For Slot5   */
       unsigned char    SIS4  :1;    /* For Slot4   */
       unsigned char    SIS3  :1;    /* For Slot3   */
       unsigned char    SIS2  :1;    /* For Slot2   */
       unsigned char    SIS1  :1;    /* For Slot1   */
       unsigned char    SIS0  :1;    /* For Slot0   */
    }BIT;                            /* Bit access  */
    struct{
        char    LOW;                 /* Low  8 bit  */
        char    HIGH;                /* High 8 bit  */
    }BYTE;                           /* Byte access */
    unsigned short  WORD;
};

/*------------------------------------------------------
    CAN1 slot interrupt mask register//0x0290-0x0291
------------------------------------------------------*/
union st_c1simkr{            /*CAN1 slot interrupt mask register */
    struct{
       unsigned char    SIM15  :1;   /* For Slot15  */
       unsigned char    SIM14  :1;   /* For Slot14  */
       unsigned char    SIM13  :1;   /* For Slot13  */
       unsigned char    SIM12  :1;   /* For Slot12  */
       unsigned char    SIM11  :1;   /* For Slot11  */
       unsigned char    SIM10  :1;   /* For Slot10  */
       unsigned char    SIM9   :1;   /* For Slot9   */
       unsigned char    SIM8   :1;   /* For Slot8   */
       unsigned char    SIM7   :1;   /* For Slot7   */
       unsigned char    SIM6   :1;   /* For Slot6   */
       unsigned char    SIM5   :1;   /* For Slot5   */
       unsigned char    SIM4   :1;   /* For Slot4   */
       unsigned char    SIM3   :1;   /* For Slot3   */
       unsigned char    SIM2   :1;   /* For Slot2   */
       unsigned char    SIM1   :1;   /* For Slot1   */
       unsigned char    SIM0   :1;   /* For Slot0   */
    }BIT;                    /* Bit access  */
    struct{
        char    LOW;         /* Low  8 bit  */
        char    HIGH;        /* High 8 bit  */
    }BYTE;                   /* Byte access */
    unsigned short  WORD;
};

/*------------------------------------------------------
     CAN1 error interrupt mask register//0x0294
------------------------------------------------------*/
union st_c1eimkr{                   /* CAN1 error interrupt status register*/
    struct{
       unsigned char    BOIM   :1;   /* BusOff Interrupt         */
       unsigned char    EPIM   :1;   /* Error Passive Interrupt  */
       unsigned char    BEIM   :1;   /* CAN BusError Interrupt   */
       unsigned char           :1;   /* Nothing assigned         */
       unsigned char           :1;   /* Nothing assigned         */
       unsigned char           :1;   /* Nothing assigned         */
       unsigned char           :1;   /* Nothing assigned         */
       unsigned char           :1;   /* Nothing assigned         */
    }BIT;                            /* Bit access               */
    char    BYTE;                    /* Byte access              */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
     CAN1 error interrupt status register //0x0295
------------------------------------------------------*/
union st_c1eistr{                   /* CAN1 error interrupt status register*/
    struct{
       unsigned char    BOIS  :1;   /* BusOff Interrupt         */
       unsigned char    EPIS  :1;   /* Error Passive Interrupt  */
       unsigned char    BEIS  :1;   /* CAN BusError Interrupt   */
       unsigned char          :1;   /* Nothing assigned         */
       unsigned char          :1;   /* Nothing assigned 		*/
       unsigned char          :1;   /* Nothing assigned 		*/
       unsigned char          :1;   /* Nothing assigned 		*/
       unsigned char          :1;   /* Nothing assigned 		*/
    }BIT;                           /* Bit access               */
    char    BYTE;                   /* Byte access              */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-------------------------------------------------------
    CAN1 error factor register //0x0296
-------------------------------------------------------*/
union st_c1efr{                      /* CAN1 error factor register*/
    struct{
       unsigned char    ACKE  :1;    /* ACK Error Detect Bit      */
       unsigned char    CRCE  :1;    /* CRC Error Detect Bit      */
       unsigned char    FORME :1;    /* FORM Error Detect Bit     */
       unsigned char    STFE  :1;    /* Stuff Error Detect Bit    */
       unsigned char    BITE0 :1;    /* Bit Error Detect Bit 0    */
       unsigned char    BITE1 :1;    /* Bit Error Detect Bit 1    */
       unsigned char    RCVE  :1;    /* Receive Error Detect Bit  */
       unsigned char    TRE   :1;    /* Transmit Error Detect Bit */
    }BIT;                            /* Bit access                */
    char    BYTE;                    /* Byte access               */
};

/*-------------------------------------------------------
    CAN1 mode register // 0x0299
-------------------------------------------------------*/
union st_c1mdr{                      /* CAN1 mode register            */
    struct{
       unsigned char    CMOD    :2;  /* CAN Operating Mode Select Bit */
       unsigned char            :1;  /* Nothing assigned              */
       unsigned char            :1;  /* Nothing assigned              */
       unsigned char            :1;  /* Nothing assigned              */
       unsigned char            :1;  /* Nothing assigned              */
       unsigned char            :1;  /* Nothing assigned              */
       unsigned char            :1;  /* Nothing assigned              */
           }BIT;                     /* Bit access                    */
    char    BYTE;                    /* Byte access                   */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    CAN1 single shot control register //0x02A0-0x02A1
------------------------------------------------------*/
union st_c1ssctlr{                  /* CAN1 single shot control register*/
    struct{
       unsigned char    SSC15 :1;   /* For Slot15   */
       unsigned char    SSC14 :1;   /* For Slot14   */
       unsigned char    SSC13 :1;   /* For Slot13   */
       unsigned char    SSC12 :1;   /* For Slot12   */
       unsigned char    SSC11 :1;   /* For Slot11   */
       unsigned char    SSC10 :1;   /* For Slot10   */
       unsigned char    SSC9  :1;   /* For Slot9    */
       unsigned char    SSC8  :1;   /* For Slot8    */
       unsigned char    SSC7  :1;   /* For Slot7    */
       unsigned char    SSC6  :1;   /* For Slot6    */
       unsigned char    SSC5  :1;   /* For Slot5    */
       unsigned char    SSC4  :1;   /* For Slot4    */
       unsigned char    SSC3  :1;   /* For Slot3    */
       unsigned char    SSC2  :1;   /* For Slot2    */
       unsigned char    SSC1  :1;   /* For Slot1    */
       unsigned char    SSC0  :1;   /* For Slot0    */
    }BIT;                           /* Bit access   */
    struct{
        char    LOW;                /* Low  8 bit   */
        char    HIGH;               /* High 8 bit   */
    }BYTE;                          /* Byte access  */
    unsigned short  WORD;
};

/*------------------------------------------------------
    CAN1 single shot status register// 0x02A4-0x2A5
------------------------------------------------------*/
union st_c1ssstr{                   /* CAN1 single shot status register*/
    struct{
       unsigned char    SSS15 :1;   /* For Slot15  */
       unsigned char    SSS14 :1;   /* For Slot14  */
       unsigned char    SSS13 :1;   /* For Slot13  */
       unsigned char    SSS12 :1;   /* For Slot12  */
       unsigned char    SSS11 :1;   /* For Slot11  */
       unsigned char    SSS10 :1;   /* For Slot10  */
       unsigned char    SSS9  :1;   /* For Slot9   */
       unsigned char    SSS8  :1;   /* For Slot8   */
       unsigned char    SSS7  :1;   /* For Slot7   */
       unsigned char    SSS6  :1;   /* For Slot6   */
       unsigned char    SSS5  :1;   /* For Slot5   */
       unsigned char    SSS4  :1;   /* For Slot4   */
       unsigned char    SSS3  :1;   /* For Slot3   */
       unsigned char    SSS2  :1;   /* For Slot2   */
       unsigned char    SSS1  :1;   /* For Slot1   */
       unsigned char    SSS0  :1;   /* For Slot0   */
    }BIT;                           /* Bit access  */
    struct{
        char    LOW;                /* Low  8 bit  */
        char    HIGH;               /* High 8 bit  */
    }BYTE;                          /* Byte access */
    unsigned short  WORD;
};

/*-------------------------------------------------------
    CAN1 Global Mask Register Standard ID0//0x02A8
-------------------------------------------------------*/
union st_c1gmr0{                     /* CAN1 Global Mask Register Standard ID0*/
    struct{
       unsigned char   SID6M  :1;    /* Standard ID6     */
       unsigned char   SID7M  :1;    /* Standard ID7     */
       unsigned char   SID8M  :1;    /* Standard ID8     */
       unsigned char   SID9M  :1;    /* Standard ID9     */
       unsigned char   SID10M :1;    /* Standard ID10    */
       unsigned char          :1;    /* Nothing assigned */
       unsigned char          :1;    /* Nothing assigned */
       unsigned char          :1;    /* Nothing assigned */
    }BIT;                            /* Bit access       */
    char    BYTE;                    /* Byte access      */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-------------------------------------------------------
    CAN1 Global Mask Register Standard ID1//0x02A9
-------------------------------------------------------*/
union st_c1gmr1{                     /* CAN1 Global Mask Register Standard ID1*/
    struct{
       unsigned char   SID0M  :1;    /* Standard ID0     */
       unsigned char   SID1M  :1;    /* Standard ID1     */
       unsigned char   SID2M  :1;    /* Standard ID2     */
       unsigned char   SID3M  :1;    /* Standard ID3     */
       unsigned char   SID4M  :1;    /* Standard ID4     */
       unsigned char   SID5M  :1;    /* Standard ID5     */
       unsigned char          :1;    /* Nothing assigned */
       unsigned char          :1;    /* Nothing assigned */
    }BIT;                            /* Bit access       */
    char    BYTE;                    /* Byte access      */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-------------------------------------------------------
    CAN1 Global Mask Register Extended ID0//0x02AA
-------------------------------------------------------*/
union st_c1gmr2{                     /* CAN1 Global Mask Register Extended ID0*/
    struct{
       unsigned char  EID14M  :1;    /* Extended ID14      */
       unsigned char  EID15M  :1;    /* Extended ID15      */
       unsigned char  EID16M  :1;    /* Extended ID16      */
       unsigned char  EID17M  :1;    /* Extended ID17      */
       unsigned char          :1;    /* Nothing assigned   */
       unsigned char          :1;    /* Nothing assigned   */
       unsigned char          :1;    /* Nothing assigned   */
       unsigned char          :1;    /* Nothing assigned   */
    }BIT;                            /* Bit access         */
    char    BYTE;                    /* Byte access        */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-------------------------------------------------------
    CAN1 Global Mask Register Extended ID1//0x02AB
-------------------------------------------------------*/
union st_c1gmr3{                     /* CAN1 Global Mask Register Extended ID1 */
    struct{
       unsigned char  EID6M   :1;    /* Extended ID6       */
       unsigned char  EID7M   :1;    /* Extended ID7       */
       unsigned char  EID8M   :1;    /* Extended ID8       */
       unsigned char  EID9M   :1;    /* Extended ID9       */
       unsigned char  EID10M  :1;    /* Extended ID10      */
       unsigned char  EID11M  :1;    /* Extended ID11      */
       unsigned char  EID12M  :1;    /* Extended ID12      */
       unsigned char  EID13M  :1;    /* Extended ID13      */
    }BIT;                            /* Bit access         */
    char    BYTE;                    /* Byte access        */
};


/*-------------------------------------------------------
    CAN1 Global Mask Register Extended ID2//0x02AC
-------------------------------------------------------*/
union st_c1gmr4{                     /* CAN1 Global Mask Register Extended ID2 */
    struct{
       unsigned char  EID0M   :1;    /* Extended ID0       */
       unsigned char  EID1M   :1;    /* Extended ID1       */
       unsigned char  EID2M   :1;    /* Extended ID2       */
       unsigned char  EID3M   :1;    /* Extended ID3       */
       unsigned char  EID4M   :1;    /* Extended ID4       */
       unsigned char  EID5M   :1;    /* Extended ID5       */
       unsigned char          :1;    /* Nothing assigned   */
       unsigned char          :1;    /* Nothing assigned   */
    }BIT;                            /* Bit access         */
    char    BYTE;                    /* Byte access        */
};
/*when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*-------------------------------------------------------------
           CAN1 Message Slot0 Control Register//0x2B0
---------------------------------------------------------------*/
union st_c1mctl0{                                  /* CAN1 Message Slot0 Control Register           */
    struct{
       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                          /* Bit access                                    */
    char    BYTE;                                  /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot1 Control Register//0x2B1
---------------------------------------------------------------*/
union st_c1mctl1{                                        /* CAN1 Message Slot1 Control Register */
    struct{
		     unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
		     unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
		     unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
		     unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
		     unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
		     unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
		     unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
             unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                                /* Bit access                                    */
    char    BYTE;                                        /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot2 Control Register//0x2B2
---------------------------------------------------------------*/
union st_c1mctl2{                                      /* CAN1 Message Slot2 Control Register           */
    struct{
	       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
	       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
	       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
	       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
	       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
	       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
	       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
           unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                              /* Bit access                                    */
    char    BYTE;                                      /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot3 Control Register//0x2B3
---------------------------------------------------------------*/
union st_c1mctl3{                                      /* CAN1 Message Slot3 Control Register           */
    struct{
	       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
	       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
	       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
	       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
	       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
	       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
	       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
           unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                              /* Bit access                                    */
    char    BYTE;                                      /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot4 Control Register//0x2B4
---------------------------------------------------------------*/
union st_c1mctl4{                                      /* CAN1 Message Slot4 Control Register           */
    struct{
	       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
	       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
	       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
	       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
	       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
	       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
	       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
           unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                              /* Bit access                                    */
    char    BYTE;                                      /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot5 Control Register//0x2B5
---------------------------------------------------------------*/
union st_c1mctl5{                                      /* CAN1 Message Slot5 Control Register           */
    struct{
	       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
	       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
	       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
	       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
	       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
	       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
	       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
           unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                              /* Bit access                                    */
    char    BYTE;                                      /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot6 Control Register//0x2B6
---------------------------------------------------------------*/
union st_c1mctl6{                                          /* CAN1 Message Slot6 Control Register           */
    struct{
		       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
		       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
		       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
		       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
		       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
		       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
		       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
               unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                                  /* Bit access                                    */
    char    BYTE;                                          /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot7 Control Register//0x2B7
---------------------------------------------------------------*/
union st_c1mctl7{                                          /* CAN1 Message Slot7 Control Register */
    struct{
		       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
		       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
		       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
		       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
		       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
		       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
		       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
               unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                                  /* Bit access                                    */
    char    BYTE;                                          /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot8 Control Register//0x2B8
---------------------------------------------------------------*/
union st_c1mctl8{                                          /* CAN1 Message Slot8 Control Register */
    struct{
		       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
		       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
		       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
		       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
		       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
		       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
		       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
               unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                                  /* Bit access                                    */
    char    BYTE;                                          /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot9 Control Register//0x2B9
---------------------------------------------------------------*/
union st_c1mctl9{                                          /* CAN1 Message Slot9 Control Register */
    struct{
		       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
		       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
		       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
		       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
		       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
		       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
		       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
               unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                                  /* Bit access                                    */
    char    BYTE;                                          /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot10 Control Register//0x2BA
---------------------------------------------------------------*/
union st_c1mctl10{                                             /* CAN1 Message Slot10 Control Register          */
    struct{
			       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
			       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
			       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
			       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
			       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
			       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
			       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
                   unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                                      /* Bit access                                    */
    char    BYTE;                                              /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot11 Control Register//0x2BB
---------------------------------------------------------------*/
union st_c1mctl11{                                             /* CAN1 Message Slot11 Control Register */
    struct{
			       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
			       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
			       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
			       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
			       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
			       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
			       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
                   unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                                      /* Bit access              */
    char    BYTE;                                              /* Byte access             */
};

/*-------------------------------------------------------------
           CAN1 Message Slot12 Control Register//0x2BC
---------------------------------------------------------------*/
union st_c1mctl12{                                             /* CAN1 Message Slot12 Control Register          */
    struct{
			       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
			       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
			       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
			       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
			       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
			       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
			       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
                   unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                                      /* Bit access                                    */
    char    BYTE;                                              /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot13 Control Register//0x2BD
---------------------------------------------------------------*/
union st_c1mctl13{                                             /* CAN1 Message Slot13 Control Register */
    struct{
			       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
			       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
			       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
			       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
			       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
			       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
			       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
                   unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                                      /* Bit access              */
    char    BYTE;                                              /* Byte access             */
};

/*-------------------------------------------------------------
           CAN1 Message Slot14 Control Register//0x2BE
---------------------------------------------------------------*/
union st_c1mctl14{                                                 /* CAN1 Message Slot14 Control Register          */
    struct{
				       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
				       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
				       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
				       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
				       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
				       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
				       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
                       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                                          /* Bit access                                    */
    char    BYTE;                                                  /* Byte access                                   */
};

/*-------------------------------------------------------------
           CAN1 Message Slot15 Control Register//0x2BF
---------------------------------------------------------------*/
union st_c1mctl15{                                                 /* CAN1 Message Slot15 Control Register          */
    struct{
				       unsigned char NEWDATA_SENTDATA       :1;    /* Receive Complete Flag Transmit Complete Flag  */
				       unsigned char INVALDATA_TRMACTIVE    :1;    /* Receiving Flag Transmitting Flag              */
				       unsigned char MSGLOST                :1;    /* Overwrite Flag                                */
				       unsigned char REMACTIVE              :1;    /* Remote Frame Transmit/Receive Status Flag     */
				       unsigned char RSPLOCK                :1;    /* Automatic Answering Disable Mode Select Bit   */
				       unsigned char REMOTE                 :1;    /* Remote Frame Set Bit                          */
				       unsigned char RECREQ                 :1;    /* Receive Request Bit                           */
                       unsigned char TRMREQ                 :1;    /* Transmit Request Bit                          */
    }BIT;                                                          /* Bit access                                    */
    char    BYTE;                                                  /* Byte access                                   */
};

/*------------------------------------------------------
  X0 register Y0 register//0x02C0
-----------------------------------------------------*/
union st_x0r_y0r {				/* X0 register Y0 register                   */
   struct{
	unsigned char X0R_Y0RL;     /* X0 register Y0 register    low  8 bit 	 */
	unsigned char X0R_Y0RH;     /* X0 register Y0 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X1 register Y1 register//0x02C2
-----------------------------------------------------*/
union st_x1r_y1r {				/* X1 register Y1 register                   */
   struct{
	unsigned char X1R_Y1RL;     /* X1 register Y1 register    low  8 bit 	 */
	unsigned char X1R_Y1RH;     /* X1 register Y1 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X2 register Y2 register//0x02C4
-----------------------------------------------------*/
union st_x2r_y2r {				/* X2 register Y2 register                   */
   struct{
	unsigned char X2R_Y2RL;     /* X2 register Y2 register    low  8 bit 	 */
	unsigned char X2R_Y2RH;     /* X2 register Y2 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X3 register Y3 register//0x02C6
-----------------------------------------------------*/
union st_x3r_y3r {				/* X3 register Y3 register                   */
   struct{
	unsigned char X3R_Y3RL;     /* X3 register Y3 register    low  8 bit 	 */
	unsigned char X3R_Y3RH;     /* X3 register Y3 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X4 register Y4 register//0x02C8
-----------------------------------------------------*/
union st_x4r_y4r {				/* X4 register Y4 register                   */
   struct{
	unsigned char X4R_Y4RL;     /* X4 register Y4 register    low  8 bit 	 */
	unsigned char X4R_Y4RH;     /* X4 register Y4 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X5 register Y5 register//0x02CA
-----------------------------------------------------*/
union st_x5r_y5r {				/* X5 register Y5 register                   */
   struct{
	unsigned char X5R_Y5RL;     /* X5 register Y5 register    low  8 bit 	 */
	unsigned char X5R_Y5RH;     /* X5 register Y5 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X6 register Y6 register//0x02CC
-----------------------------------------------------*/
union st_x6r_y6r {				/* X6 register Y6 register                   */
   struct{
	unsigned char X6R_Y6RL;     /* X6 register Y6 register    low  8 bit 	 */
	unsigned char X6R_Y6RH;     /* X6 register Y6 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X7 register Y7 register//0x02CE
-----------------------------------------------------*/
union st_x7r_y7r {				/* X7 register Y7 register                   */
   struct{
	unsigned char X7R_Y7RL;     /* X7 register Y7 register    low  8 bit 	 */
	unsigned char X7R_Y7RH;     /* X7 register Y7 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X8 register Y8 register//0x02D0
-----------------------------------------------------*/
union st_x8r_y8r {				/* X8 register Y8 register                   */
   struct{
	unsigned char X8R_Y8RL;     /* X8 register Y8 register    low  8 bit 	 */
	unsigned char X8R_Y8RH;     /* X8 register Y8 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X9 register Y9 register//0x02D2
-----------------------------------------------------*/
union st_x9r_y9r {				/* X9 register Y9 register                   */
   struct{
	unsigned char X9R_Y9RL;     /* X9 register Y9 register    low  8 bit 	 */
	unsigned char X9R_Y9RH;     /* X9 register Y9 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X10 register Y10 register//0x02D4
-----------------------------------------------------*/
union st_x10r_y10r {		    /* X10 register Y10 register                   */
   struct{
	unsigned char X10R_Y10RL;   /* X10 register Y10 register    low  8 bit 	 */
	unsigned char X10R_Y10RH;   /* X10 register Y10 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X11 register Y11 register//0x02D6
-----------------------------------------------------*/
union st_x11r_y11r {		    /* X11 register Y11 register                   */
   struct{
	unsigned char X11R_Y11RL;   /* X11 register Y11 register    low  8 bit 	 */
	unsigned char X11R_Y11RH;   /* X11 register Y11 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X12 register Y12 register//0x02D8
-----------------------------------------------------*/
union st_x12r_y12r {		    /* X12 register Y12 register                   */
   struct{
	unsigned char X12R_Y12RL;   /* X12 register Y12 register    low  8 bit 	 */
	unsigned char X12R_Y12RH;   /* X12 register Y12 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X13 register Y13 register//0x02DA
-----------------------------------------------------*/
union st_x13r_y13r {		    /* X13 register Y13 register                   */
   struct{
	unsigned char X13R_Y13RL;   /* X13 register Y13 register    low  8 bit 	 */
	unsigned char X13R_Y13RH;   /* X13 register Y13 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X14 register Y14 register//0x02DC
-----------------------------------------------------*/
union st_x14r_y14r {		    /* X14 register Y14 register                 */
   struct{
	unsigned char X14R_Y14RL;   /* X14 register Y14 register    low  8 bit 	 */
	unsigned char X14R_Y14RH;   /* X14 register Y14 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*------------------------------------------------------
  X15 register Y15 register//0x02DE
-----------------------------------------------------*/
union st_x15r_y15r {		    /* X15 register Y15 register                   */
   struct{
	unsigned char X15R_Y15RL;   /* X15 register Y15 register    low  8 bit 	 */
	unsigned char X15R_Y15RH;   /* X15 register Y15 register    high 8 bit 	 */
   } BYTE;					    /* Byte access					   			 */
   unsigned short   WORD;	    /* Word Access					   			 */
};

/*---------------------------------------------------------------------------------
            X-Y control register //0x02E0
-----------------------------------------------------------------------------------*/
union st_xyc{                      /* X-Y control register */
    struct{
       unsigned char  XYC0  :1;    /* Read Mode Set Bit    */
       unsigned char  XYC1  :1;    /* Write Mode Set Bit   */
       unsigned char        :1;    /* Noting assigned.     */
       unsigned char        :1;    /* Noting assigned.     */
       unsigned char        :1;    /* Noting assigned.     */
       unsigned char        :1;    /* Noting assigned.     */
       unsigned char        :1;    /* Noting assigned.     */
       unsigned char        :1;    /* Noting assigned.     */
    }BIT;                          /* Bit access           */
    char    BYTE;                  /* Byte access          */
};
/*When nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------------------------
       UART1 special mode register 4//0x02E4
-----------------------------------------------------------------------------------*/
union st_u1smr4{                      /* UART1 special mode register 4  */
    struct{
       unsigned char  STAREQ   :1;    /* Start Condition Generate Bi    */
       unsigned char  RSTAREQ  :1;    /* Restart Condition Generate Bit */
       unsigned char  STPREQ   :1;    /* Stop Condition Generate Bit    */
       unsigned char  STSPSEL  :1;    /* SCL, SDA Output Select Bit     */
       unsigned char  ACKD     :1;    /* ACK Data Bit                   */
       unsigned char  ACKC     :1;    /* ACK Data Output Enable Bit     */
       unsigned char  SCLHI    :1;    /* SCL Output Stop Enable Bit     */
       unsigned char  SWC9     :1;    /* SCL Wait Output Bit 3          */
    }BIT;                             /* Bit access                     */
    char    BYTE;                     /* Byte access                    */
};

/*---------------------------------------------------------------------------------
       UART1 special mode register 3//0x02E5
-----------------------------------------------------------------------------------*/
union st_u1smr3{                   /* UART1 special mode register 3  */
    struct{
       unsigned char  SSE   :1;    /* SS Pin Function Enable Bit        */
       unsigned char  CKPH  :1;    /* Clock Phase Set Bit               */
       unsigned char  DINC  :1;    /* Serial Input Port Set Bit         */
       unsigned char  NODC  :1;    /* Clock Output Select Bit           */
       unsigned char  ERR   :1;    /* Fault Error Flag                  */
       unsigned char  DL0   :1;    /* SDAi Digital Delay Time Set Bit   */
       unsigned char  DL1   :1;    /* SDAi Digital Delay Time Set Bit   */
       unsigned char  DL2   :1;    /* SDAi Digital Delay Time Set Bit   */
    }BIT;                          /* Bit access                        */
    char    BYTE;                  /* Byte access                       */
};

/*---------------------------------------------------------------------------------
       UART1 special mode register 2//0x02E6
-----------------------------------------------------------------------------------*/
union st_u1smr2{            /* UART1 special mode register 2         */
    struct{
       unsigned char  IICM2  :1;    /* I2C Mode Select Bit 2                 */
       unsigned char  CSC    :1;    /* Clock Synchronous Bit                 */
       unsigned char  SWC    :1;    /* SCL Wait Output Bit                   */
       unsigned char  ALS    :1;    /* SDA Output Stop Bit                   */
       unsigned char  STC    :1;    /* UART1 Initialize Bit                  */
       unsigned char  SWC2   :1;    /* SCL Wait Output Bit 2                 */
       unsigned char  SDHI   :1;    /* SDA Output Inhibit Bit                */
       unsigned char  SU1HIM :1;    /* External Clock Synchronous Enable Bit */
    }BIT;                   /* Bit access                            */
    char    BYTE;           /* Byte access                           */
};

/*---------------------------------------------------------------------------------
       UART1 special mode register //0x02E7
-----------------------------------------------------------------------------------*/
union st_u1smr{              /* UART1 special mode register                            */
    struct{
       unsigned char  IICM    :1;    /* I2C Mode Select Bit                  				   */
       unsigned char  ABC     :1;    /* Arbitration Lost Detect Flag Control Bit			   */
       unsigned char  BBS     :1;    /* Bus Busy Flag                                          */
       unsigned char  LSYN    :1;    /* SCLL Sync Output Enable Bit                            */
       unsigned char  ABSCS   :1;    /* Bus Conflict Detect Sampling Clock Select Bit          */
       unsigned char  ACSE    :1;    /* Auto Clear Function Select Bit for Transmit Enable Bit */
       unsigned char  SSS     :1;    /* Transmit Start Condition Select Bit                    */
       unsigned char  SCLKDIV :1;    /* Clock Divide Synchronous Bit						   */
    }BIT;                    /* Bit access                                             */
    char    BYTE;            /* Byte access                                            */
};

/*---------------------------------------------------------------------------------
       UART1 transmit/receive mode register //0x02E8
-----------------------------------------------------------------------------------*/
union st_u1mr{             /* UART1 transmit/receive mode register      */
    struct{
       unsigned char  SMD0  :1;    /* Serial I/O Mode Select Bit                */
       unsigned char  SMD1  :1;    /* Serial I/O Mode Select Bit	            */
       unsigned char  SMD2  :1;    /* Serial I/O Mode Select Bit                */
       unsigned char  CKDIR :1;    /* Internal/External Clock Select Bit        */
       unsigned char  STPS  :1;    /* Stop Bit Length Select Bit                */
       unsigned char  PRY   :1;    /* Odd/Even Parity Select Bit                */
       unsigned char  PRYE  :1;    /* Parity Enable Bit                         */
       unsigned char  IOPOL :1;    /* TxD,RxD Input/Output Polarity Switch Bit  */
    }BIT;                  /* Bit access                                */
    char    BYTE;          /* Byte access                               */
};

/*----------------------------------------------------------------------------------
             UART1 transmit buffer register//0x02EA
------------------------------------------------------------------------------------*/
union st_u1tb{             /* UART1 transmit buffer register*/
    struct{
       unsigned char    B0   :1;   /* Transmit data    */
       unsigned char    B1   :1;   /* Transmit data    */
       unsigned char    B2   :1;   /* Transmit data    */
       unsigned char    B3   :1;   /* Transmit data    */
       unsigned char    B4   :1;   /* Transmit data    */
       unsigned char    B5   :1;   /* Transmit data    */
       unsigned char    B6   :1;   /* Transmit data    */
       unsigned char    B7   :1;   /* Transmit data    */
       unsigned char    B8   :1;   /* Transmit data    */
       unsigned char    B9   :1;   /* Nothing assigned */
       unsigned char    B10  :1;   /* Nothing assigned */
       unsigned char    B11  :1;   /* Nothing assigned */
       unsigned char    B12  :1;   /* Nothing assigned */
       unsigned char    B13  :1;   /* Nothing assigned */
       unsigned char    B14  :1;   /* Nothing assigned */
       unsigned char    B15  :1;   /* Nothing assigned */
    }BIT;                  /* Bit access       */
    struct{
        char    LOW;       /* Low  8 bit       */
        char    HIGH;      /* High 8 bit       */
    }BYTE;                 /* Byte access      */
    unsigned short  WORD;
};
/* when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------------------------
       UART1 transmit/receive control register 0 //0x02EC
-----------------------------------------------------------------------------------*/
union st_u1c0{             /* UART1 transmit/receive control register 0 */
    struct{
       unsigned char  CLK0  :1;    /* U1BRG CountSource Select Bit     */
       unsigned char  CLK1  :1;    /* UiBRG Count Source Select Bit    */
       unsigned char  CRS   :1;    /* CST/RTS Function Select Bit      */
       unsigned char  TXEPT :1;    /* Transmit Register Empty Flag     */
       unsigned char  CRD   :1;    /* CTS/RTS Disable Bit              */
       unsigned char  NCH   :1;    /* Data Output Select Bit           */
       unsigned char  CKPOL :1;    /* CLK Polarity Select Bit          */
       unsigned char  UFORM :1;    /* Transfer Format Select Bit       */
    }BIT;                  /* Bit access                       */
    char    BYTE;          /* Byte access                      */
};

/*---------------------------------------------------------------------------------
       UART1 transmit/receive control register 1 //0x02ED//===========================================================
-----------------------------------------------------------------------------------*/
union st_u1c1{                /* UART1 transmit/receive control register 1 */
    struct{
       unsigned char TE              :1;    /* Transmit Enable Bit                         */
       unsigned char TI              :1;    /* Transmit Buffer Empty Flag                  */
       unsigned char RE              :1;    /* Receive Enable Bit                          */
       unsigned char RI              :1;    /* Receive Complete Flag                       */
       unsigned char U1IRS           :1;    /* UART1 Transmit Interrupt Cause Select Bit   */
       unsigned char U1RRM           :1;    /* UART1 Continuous Receive Mode Enable Bit    */
       unsigned char U1LCH           :1;    /* Data Logic Select Bit                       */
       unsigned char SCLKSTPB_U1ERE  :1;    /* Clock-Divided Synchronous Stop Bit / Error Signal Output Enable Bit  */
    }BIT;                                   /* Bit access                                  */
    char    BYTE;                           /* Byte access                                 */
};

/*----------------------------------------------------------------------------------
             UART1 receive buffer register//0x02EE
------------------------------------------------------------------------------------*/
union st_u1rb{             /* UART1 receive buffer register*/
    struct{
       unsigned char         :1;   /* Receive data                 */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Nothing assigned             */
       unsigned char         :1;   /* Nothing assigned             */
       unsigned char    ABT  :1;   /* Arbitration Lost Detect Flag */
       unsigned char    OER  :1;   /* Overrun Error Flag           */
       unsigned char    FER  :1;   /* Framing Error Flag           */
       unsigned char    PER  :1;   /* Parity Error Flag            */
       unsigned char    SUM  :1;   /* Error Sum Flag               */
    }BIT;                  /* Bit access                   */
    struct{
        char    LOW;       /* Low  8 bit                   */
        char    HIGH;      /* High 8 bit                   */
    }BYTE;                 /* Byte access                  */
    unsigned short  WORD;
};
/* when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------------------------
       UART4 special mode register 4//0x02F4
-----------------------------------------------------------------------------------*/
union st_u4smr4{              /* UART4 special mode register 4  */
    struct{
       unsigned char  STAREQ   :1;    /* Start Condition Generate Bi    */
       unsigned char  RSTAREQ  :1;    /* Restart Condition Generate Bit */
       unsigned char  STPREQ   :1;    /* Stop Condition Generate Bit    */
       unsigned char  STSPSEL  :1;    /* SCL, SDA Output Select Bit     */
       unsigned char  ACKD     :1;    /* ACK Data Bit                   */
       unsigned char  ACKC     :1;    /* ACK Data Output Enable Bit     */
       unsigned char  SCLHI    :1;    /* SCL Output Stop Enable Bit     */
       unsigned char  SWC9     :1;    /* SCL Wait Output Bit 3          */
    }BIT;                     /* Bit access                     */
    char    BYTE;             /* Byte access                    */
};

/*---------------------------------------------------------------------------------
       UART4 special mode register 3//0x02F5
-----------------------------------------------------------------------------------*/
union st_u4smr3{           /* UART4 special mode register 3     */
    struct{
       unsigned char  SSE   :1;    /* SS Pin Function Enable Bit        */
       unsigned char  CKPH  :1;    /* Clock Phase Set Bit               */
       unsigned char  DINC  :1;    /* Serial Input Port Set Bit         */
       unsigned char  NODC  :1;    /* Clock Output Select Bit           */
       unsigned char  ERR   :1;    /* Fault Error Flag                  */
       unsigned char  DL0   :1;    /* SDA4 Digital Delay Time Set Bit   */
       unsigned char  DL1   :1;    /* SDA4 Digital Delay Time Set Bit   */
       unsigned char  DL2   :1;    /* SDA4 Digital Delay Time Set Bit   */
    }BIT;                  /* Bit access                        */
    char    BYTE;          /* Byte access                       */
};

/*---------------------------------------------------------------------------------
       UART4 special mode register 2//0x02F6
-----------------------------------------------------------------------------------*/
union st_u4smr2{            /* UART4 special mode register 2         */
    struct{
       unsigned char  IICM2  :1;    /* I2C Mode Select Bit 2                 */
       unsigned char  CSC    :1;    /* Clock Synchronous Bit                 */
       unsigned char  SWC    :1;    /* SCL Wait Output Bit                   */
       unsigned char  ALS    :1;    /* SDA Output Stop Bit                   */
       unsigned char  STC    :1;    /* UART4 Initialize Bit                  */
       unsigned char  SWC2   :1;    /* SCL Wait Output Bit 2                 */
       unsigned char  SDHI   :1;    /* SDA Output Inhibit Bit                */
       unsigned char  SU1HIM :1;    /* External Clock Synchronous Enable Bit */
    }BIT;                   /* Bit access                            */
    char    BYTE;           /* Byte access                           */
};

/*---------------------------------------------------------------------------------
       UART4 special mode register //0x02F7
-----------------------------------------------------------------------------------*/
union st_u4smr{              /* UART4 special mode register                            */
    struct{
       unsigned char  IICM    :1;    /* I2C Mode Select Bit                  				   */
       unsigned char  ABC     :1;    /* Arbitration Lost Detect Flag Control Bit			   */
       unsigned char  BBS     :1;    /* Bus Busy Flag                                          */
       unsigned char  LSYN    :1;    /* SCLL Sync Output Enable Bit                            */
       unsigned char  ABSCS   :1;    /* Bus Conflict Detect Sampling Clock Select Bit          */
       unsigned char  ACSE    :1;    /* Auto Clear Function Select Bit for Transmit Enable Bit */
       unsigned char  SSS     :1;    /* Transmit Start Condition Select Bit                    */
       unsigned char  SCLKDIV :1;    /* Clock Divide Synchronous Bit						   */
    }BIT;                    /* Bit access                                             */
    char    BYTE;            /* Byte access                                            */
};

/*---------------------------------------------------------------------------------
       UART4 transmit/receive mode register //0x02F8
-----------------------------------------------------------------------------------*/
union st_u4mr{             /* UART4 transmit/receive mode register      */
    struct{
       unsigned char  SMD0  :1;    /* Serial I/O Mode Select Bit                */
       unsigned char  SMD1  :1;    /* Serial I/O Mode Select Bit	            */
       unsigned char  SMD2  :1;    /* Serial I/O Mode Select Bit                */
       unsigned char  CKDIR :1;    /* Internal/External Clock Select Bit        */
       unsigned char  STPS  :1;    /* Stop Bit Length Select Bit                */
       unsigned char  PRY   :1;    /* Odd/Even Parity Select Bit                */
       unsigned char  PRYE  :1;    /* Parity Enable Bit                         */
       unsigned char  IOPOL :1;    /* TxD,RxD Input/Output Polarity Switch Bit  */
    }BIT;                  /* Bit access                                */
    char    BYTE;          /* Byte access                               */
};

/*----------------------------------------------------------------------------------
             UART4 transmit buffer register//0x02FA
------------------------------------------------------------------------------------*/
union st_u4tb{             /* UART4 transmit buffer register*/
    struct{
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Nothing assigned */
       unsigned char      :1;   /* Nothing assigned */
       unsigned char      :1;   /* Nothing assigned */
       unsigned char      :1;   /* Nothing assigned */
       unsigned char      :1;   /* Nothing assigned */
       unsigned char      :1;   /* Nothing assigned */
       unsigned char      :1;   /* Nothing assigned */
    }BIT;                  /* Bit access       */
    struct{
        char    LOW;       /* Low  8 bit       */
        char    HIGH;      /* High 8 bit       */
    }BYTE;                 /* Byte access      */
    unsigned short  WORD;
};
/* when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------------------------
       UART4 transmit/receive control register 0 //0x02FC
-----------------------------------------------------------------------------------*/
union st_u4c0{             /* UART4 transmit/receive control register 0 */
    struct{
       unsigned char  CLK0  :1;    /* U4BRG CountSource Select Bit     */
       unsigned char  CLK1  :1;    /* U4BRG Count Source Select Bit    */
       unsigned char  CRS   :1;    /* CST/RTS Function Select Bit      */
       unsigned char  TXEPT :1;    /* Transmit Register Empty Flag     */
       unsigned char  CRD   :1;    /* CTS/RTS Disable Bit              */
       unsigned char  NCH   :1;    /* Data Output Select Bit           */
       unsigned char  CKPOL :1;    /* CLK Polarity Select Bit          */
       unsigned char  UFORM :1;    /* Transfer Format Select Bit       */
    }BIT;                  /* Bit access                       */
    char    BYTE;          /* Byte access                      */
};

/*---------------------------------------------------------------------------------
       UART1 transmit/receive control register 1 //0x02FD
-----------------------------------------------------------------------------------*/
union st_u4c1{                      /* UART4 transmit/receive control register 1   */
    struct{
       unsigned char TE              :1;    /* Transmit Enable Bit                         */
       unsigned char TI              :1;    /* Transmit Buffer Empty Flag                  */
       unsigned char RE              :1;    /* Receive Enable Bit                          */
       unsigned char RI              :1;    /* Receive Complete Flag                       */
       unsigned char U4IRS           :1;    /* UART4 Transmit Interrupt Cause Select Bit   */
       unsigned char U4RRM           :1;    /* UART4 Continuous Receive Mode Enable Bit    */
       unsigned char U4LCH           :1;    /* Data Logic Select Bit                       */
       unsigned char SCLKSTPB_U4ERE  :1;    /* Clock-Divided Synchronous Stop Bit  / Error signal output enable bit        */
    }BIT;                           /* Bit access                                  */
    char    BYTE;                   /* Byte access                                 */
};

/*----------------------------------------------------------------------------------
             UART4 receive buffer register//0x02FE
------------------------------------------------------------------------------------*/
union st_u4rb{             /* UART4 receive buffer register*/
    struct{
       unsigned char         :1;   /* Receive data                 */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Nothing assigned             */
       unsigned char         :1;   /* Nothing assigned             */
       unsigned char    ABT  :1;   /* Arbitration Lost Detect Flag */
       unsigned char    OER  :1;   /* Overrun Error Flag           */
       unsigned char    FER  :1;   /* Framing Error Flag           */
       unsigned char    PER  :1;   /* Parity Error Flag            */
       unsigned char    SUM  :1;   /* Error Sum Flag               */
    }BIT;                  /* Bit access                   */
    struct{
        char    LOW;       /* Low  8 bit                   */
        char    HIGH;      /* High 8 bit                   */
    }BYTE;                 /* Byte access                  */
    unsigned short  WORD;
};
/* when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------------------------
           Timer B3,4,5 count start flag //0x0300
-------------------------------------------------------------------------*/
 union st_tbsr{            /* Timer B3,4,5 count start flag */
     struct{
        unsigned char       :1;    /* Nothing assigned.          */
        unsigned char       :1;    /* Nothing assigned.          */
        unsigned char       :1;    /* Nothing assigned.          */
        unsigned char       :1;    /* Nothing assigned.          */
        unsigned char       :1;    /* Nothing assigned.          */
        unsigned char  TB3S :1;    /* Timer B3 Count Start Flag  */
        unsigned char  TB4S :1;    /* Timer B4 Count Start Flag  */
        unsigned char  TB5S :1;    /* Timer B3 Count Start Flag  */
     }BIT;                 /* Bit access                 */
     char    BYTE;         /* Byte access                */
};

/*------------------------------------------------------------------------
      Three-phase PWM control register 0 //0x0308
-------------------------------------------------------------------------*/
 union st_invc0{                     /* Three-phase PWM control register 0            */
     struct{
        unsigned char INV0_0  :1;    /* Interrupt enable output polarity select bit   */
        unsigned char INV0_1  :1;    /* Interrupt enable output specification bit     */
        unsigned char INV0_2  :1;    /* Mode select bit                               */
        unsigned char INV0_3  :1;    /* Output control bit                            */
        unsigned char INV0_4  :1;    /* Positive &amp; negative phases concurrent active disable function enable bit */
        unsigned char INV0_5  :1;    /* Positive &amp; negative phases concurrent active output detect flag          */
        unsigned char INV0_6  :1;    /* Modulation mode select bit                    */
        unsigned char INV0_7  :1;    /* Software trigger select bit                   */
     }BIT;                           /* Bit access                                    */
     char    BYTE;                   /* Byte access                                   */
};

/*------------------------------------------------------------------------
      Three-phase PWM control register 1 //0x0309
-------------------------------------------------------------------------*/
 union st_invc1{             /* Three-phase PWM control register 1  */
     struct{
        unsigned char INV1_0  :1;    /* Timer A1,A2 and A4 start trigger select bit  */
        unsigned char INV1_1  :1;    /* Timer A1-1,A2-1,A4-1 control bit             */
        unsigned char INV1_2  :1;    /* Dead time timer count source select bit      */
        unsigned char INV1_3  :1;    /* Carrier wave detect flag                     */
        unsigned char INV1_4  :1;    /* Output polarity control bit                  */
        unsigned char INV1_5  :1;    /* Dead time disable bit 						 */
        unsigned char INV1_6  :1;    /* Dead time timer trigger select bit           */
        unsigned char         :1;    /* Reserved bit must be set to 0               */
     }BIT;                   /* Bit access                                   */
     char    BYTE;           /* Byte access                                  */
};

/*------------------------------------------------------------------------
      Three-phase output buffer register 0 //0x030A
-------------------------------------------------------------------------*/
 union st_idb0{                    /* Three-phase output buffer register 0  */
     struct{
        unsigned char DU0   :1;    /*  U-phase output buffer  0      */
        unsigned char DUB0  :1;    /* ~U-phase output buffer  0      */
        unsigned char DV0   :1;    /*  V-phase output buffer  0      */
        unsigned char DVB0  :1;    /* ~V-phase output buffer  0      */
        unsigned char DW0   :1;    /*  W-phase output buffer  0      */
        unsigned char DWB0  :1;    /* ~W-phase output buffer  0		 */
        unsigned char       :1;    /*  Reserved bit must be set to 0 */
        unsigned char       :1;    /*  Reserved bit must be set to 0 */
     }BIT;                         /* Bit access                     */
     char    BYTE;                 /* Byte access                    */
};

/*------------------------------------------------------------------------
      Three-phase output buffer register 1 //0x030A
-------------------------------------------------------------------------*/
 union st_idb1{                    /* Three-phase output buffer register 1  */
     struct{
        unsigned char DU1   :1;    /*  U-phase output buffer  1      */
        unsigned char DUB1  :1;    /* ~U-phase output buffer  1      */
        unsigned char DV1   :1;    /*  V-phase output buffer  1      */
        unsigned char DVB1  :1;    /* ~V-phase output buffer  1      */
        unsigned char DW1   :1;    /*  W-phase output buffer  1      */
        unsigned char DWB1  :1;    /* ~W-phase output buffer  1		 */
        unsigned char       :1;    /*  Reserved bit must be set to 0 */
        unsigned char       :1;    /*  Reserved bit must be set to 0 */
     }BIT;                         /* Bit access                     */
     char    BYTE;                 /* Byte access                    */
};

/*------------------------------------------------------------------------
      Timer B3 mode register //0x031B
-------------------------------------------------------------------------*/
 union st_tb3mr{                    /* Timer B3 mode register    */
     struct{
        unsigned char TMOD0  :1;    /* Operating Mode Select Bit */
        unsigned char TMOD1  :1;    /* Operating Mode Select Bit */
        unsigned char MR0    :1;    /* Function varies depending on operating mode   */
        unsigned char MR1    :1;    /* Function varies depending on operating mode   */
        unsigned char MR2    :1;    /* Function varies depending on operating mode   */
        unsigned char MR3    :1;    /* Function varies depending on operating mode   */
        unsigned char TCK0   :1;    /* Count Source Select Bit.                      */
        unsigned char TCK1   :1;    /* Count Source Select Bit.                      */
     }BIT;                          /* Bit access                */
     char    BYTE;                  /* Byte access               */
};

/*------------------------------------------------------------------------
      Timer B4 mode register //0x031C
-------------------------------------------------------------------------*/
 union st_tb4mr{                    /* Timer B4 mode register    */
     struct{
        unsigned char TMOD0  :1;    /* Operating Mode Select Bit                     */
        unsigned char TMOD1  :1;    /* Operating Mode Select Bit                     */
        unsigned char MR0    :1;    /* Function varies depending on operating mode   */
        unsigned char MR1    :1;    /* Function varies depending on operating mode   */
        unsigned char MR2    :1;    /* Function varies depending on operating mode   */
        unsigned char MR3    :1;    /* Function varies depending on operating mode   */
        unsigned char TCK0   :1;    /* Count Source Select Bit.                      */
        unsigned char TCK1   :1;    /* Count Source Select Bit.                      */
     }BIT;                          /* Bit access                */
     char    BYTE;                  /* Byte access               */
};

/*------------------------------------------------------------------------
      Timer B5 mode register //0x031D
-------------------------------------------------------------------------*/
 union st_tb5mr{                    /* Timer B5 mode register    */
     struct{
        unsigned char TMOD0  :1;    /* Operating Mode Select Bit                     */
        unsigned char TMOD1  :1;    /* Operating Mode Select Bit                     */
        unsigned char MR0    :1;    /* Function varies depending on operating mode   */
        unsigned char MR1    :1;    /* Function varies depending on operating mode   */
        unsigned char MR2    :1;    /* Function varies depending on operating mode   */
        unsigned char MR3    :1;    /* Function varies depending on operating mode   */
        unsigned char TCK0   :1;    /* Count Source Select Bit.                      */
        unsigned char TCK1   :1;    /* Count Source Select Bit.                      */
     }BIT;                          /* Bit access                                    */
     char    BYTE;                  /* Byte access                                   */
};


/*------------------------------------------------------------------------
       External interrupt request cause select register //0x031F
-------------------------------------------------------------------------*/
 union st_ifsr{             /* External interrupt request cause select register */
     struct{
        unsigned char IFSR0  :1;    /* INT0 interrupt polarity select bit   */
        unsigned char IFSR1  :1;    /* INT1 interrupt polarity select bit   */
        unsigned char IFSR2  :1;    /* INT2 interrupt polarity select bit   */
        unsigned char IFSR3  :1;    /* INT3 interrupt polarity select bit   */
        unsigned char IFSR4  :1;    /* INT4 interrupt polarity select bit   */
        unsigned char IFSR5  :1;    /* INT5 interrupt polarity select bit   */
        unsigned char IFSR6  :1;    /* UART0,3 interrupt cause select bit   */
        unsigned char IFSR7  :1;    /* UART1,4 interrupt cause select bit   */
     }BIT;                  /* Bit access                           */
     char    BYTE;          /* Byte access                          */
};

/*---------------------------------------------------------------------------------
       UART3 special mode register 4//0x0324
-----------------------------------------------------------------------------------*/
union st_u3smr4{                      /* UART3 special mode register 4  */
    struct{
       unsigned char  STAREQ   :1;    /* Start Condition Generate Bi    */
       unsigned char  RSTAREQ  :1;    /* Restart Condition Generate Bit */
       unsigned char  STPREQ   :1;    /* Stop Condition Generate Bit    */
       unsigned char  STSPSEL  :1;    /* SCL, SDA Output Select Bit     */
       unsigned char  ACKD     :1;    /* ACK Data Bit                   */
       unsigned char  ACKC     :1;    /* ACK Data Output Enable Bit     */
       unsigned char  SCLHI    :1;    /* SCL Output Stop Enable Bit     */
       unsigned char  SWC9     :1;    /* SCL Wait Output Bit 3          */
    }BIT;                             /* Bit access                     */
    char    BYTE;                     /* Byte access                    */
};

/*---------------------------------------------------------------------------------
       UART3 special mode register 3//0x0325
-----------------------------------------------------------------------------------*/
union st_u3smr3{                   /* UART3 special mode register 3  */
    struct{
       unsigned char  SSE   :1;    /* SS Pin Function Enable Bit        */
       unsigned char  CKPH  :1;    /* Clock Phase Set Bit               */
       unsigned char  DINC  :1;    /* Serial Input Port Set Bit         */
       unsigned char  NODC  :1;    /* Clock Output Select Bit           */
       unsigned char  ERR   :1;    /* Fault Error Flag                  */
       unsigned char  DL0   :1;    /* SDA3 Digital Delay Time Set Bit   */
       unsigned char  DL1   :1;    /* SDA3 Digital Delay Time Set Bit   */
       unsigned char  DL2   :1;    /* SDA3 Digital Delay Time Set Bit   */
    }BIT;                          /* Bit access                        */
    char    BYTE;                  /* Byte access                       */
};

/*---------------------------------------------------------------------------------
       UART3 special mode register 2//0x0326
-----------------------------------------------------------------------------------*/
union st_u3smr2{                    /* UART3 special mode register 2         */
    struct{
       unsigned char  IICM2  :1;    /* I2C Mode Select Bit 2                 */
       unsigned char  CSC    :1;    /* Clock Synchronous Bit                 */
       unsigned char  SWC    :1;    /* SCL Wait Output Bit                   */
       unsigned char  ALS    :1;    /* SDA Output Stop Bit                   */
       unsigned char  STC    :1;    /* UART3 Initialize Bit                  */
       unsigned char  SWC2   :1;    /* SCL Wait Output Bit 2                 */
       unsigned char  SDHI   :1;    /* SDA Output Inhibit Bit                */
       unsigned char  SU1HIM :1;    /* External Clock Synchronous Enable Bit */
    }BIT;                           /* Bit access                            */
    char    BYTE;                   /* Byte access                           */
};

/*---------------------------------------------------------------------------------
       UART3 special mode register //0x0327
-----------------------------------------------------------------------------------*/
union st_u3smr{                      /* UART3 special mode register                            */
    struct{
       unsigned char  IICM    :1;    /* I2C Mode Select Bit                  				   */
       unsigned char  ABC     :1;    /* Arbitration Lost Detect Flag Control Bit			   */
       unsigned char  BBS     :1;    /* Bus Busy Flag                                          */
       unsigned char  LSYN    :1;    /* SCLL Sync Output Enable Bit                            */
       unsigned char  ABSCS   :1;    /* Bus Conflict Detect Sampling Clock Select Bit          */
       unsigned char  ACSE    :1;    /* Auto Clear Function Select Bit for Transmit Enable Bit */
       unsigned char  SSS     :1;    /* Transmit Start Condition Select Bit                    */
       unsigned char  SCLKDIV :1;    /* Clock Divide Synchronous Bit						   */
    }BIT;                            /* Bit access                                             */
    char    BYTE;                    /* Byte access                                            */
};

/*---------------------------------------------------------------------------------
       UART3 transmit/receive mode register //0x0328
-----------------------------------------------------------------------------------*/
union st_u3mr{                     /* UART3 transmit/receive mode register      */
    struct{
       unsigned char  SMD0  :1;    /* Serial I/O Mode Select Bit                */
       unsigned char  SMD1  :1;    /* Serial I/O Mode Select Bit	            */
       unsigned char  SMD2  :1;    /* Serial I/O Mode Select Bit                */
       unsigned char  CKDIR :1;    /* Internal/External Clock Select Bit        */
       unsigned char  STPS  :1;    /* Stop Bit Length Select Bit                */
       unsigned char  PRY   :1;    /* Odd/Even Parity Select Bit                */
       unsigned char  PRYE  :1;    /* Parity Enable Bit                         */
       unsigned char  IOPOL :1;    /* TxD,RxD Input/Output Polarity Switch Bit  */
    }BIT;                          /* Bit access                                */
    char    BYTE;                  /* Byte access                               */
};

/*----------------------------------------------------------------------------------
             UART3 transmit buffer register//0x032A
------------------------------------------------------------------------------------*/
union st_u3tb{                     /* UART3 transmit buffer register*/
    struct{
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Transmit data    */
       unsigned char      :1;   /* Nothing assigned */
       unsigned char      :1;   /* Nothing assigned */
       unsigned char      :1;   /* Nothing assigned */
       unsigned char      :1;   /* Nothing assigned */
       unsigned char      :1;   /* Nothing assigned */
       unsigned char      :1;   /* Nothing assigned */
       unsigned char      :1;   /* Nothing assigned */
    }BIT;                       /* Bit access       */
    struct{
        char    LOW;            /* Low  8 bit       */
        char    HIGH;           /* High 8 bit       */
    }BYTE;                      /* Byte access      */
    unsigned short  WORD;
};
/* when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------------------------
       UART3 transmit/receive control register 0 //0x032C
-----------------------------------------------------------------------------------*/
union st_u3c0{                     /* UART3 transmit/receive control register 0 */
    struct{
       unsigned char  CLK0  :1;    /* U3BRG CountSource Select Bit     */
       unsigned char  CLK1  :1;    /* U3BRG Count Source Select Bit    */
       unsigned char  CRS   :1;    /* CST/RTS Function Select Bit      */
       unsigned char  TXEPT :1;    /* Transmit Register Empty Flag     */
       unsigned char  CRD   :1;    /* CTS/RTS Disable Bit              */
       unsigned char  NCH   :1;    /* Data Output Select Bit           */
       unsigned char  CKPOL :1;    /* CLK Polarity Select Bit          */
       unsigned char  UFORM :1;    /* Transfer Format Select Bit       */
    }BIT;                          /* Bit access                       */
    char    BYTE;                  /* Byte access                      */
};

/*---------------------------------------------------------------------------------
       UART3 transmit/receive control register 1 //0x032D
-----------------------------------------------------------------------------------*/
union st_u3c1{                              /* UART3 transmit/receive control register 1 */
    struct{
       unsigned char TE              :1;    /* Transmit Enable Bit                         */
       unsigned char TI              :1;    /* Transmit Buffer Empty Flag                  */
       unsigned char RE              :1;    /* Receive Enable Bit                          */
       unsigned char RI              :1;    /* Receive Complete Flag                       */
       unsigned char U3IRS           :1;    /* UART3 Transmit Interrupt Cause Select Bit   */
       unsigned char U3RRM           :1;    /* UART3 Continuous Receive Mode Enable Bit    */
       unsigned char U3LCH           :1;    /* Data Logic Select Bit                       */
       unsigned char SCLKSTPB_U3ERE  :1;    /* Clock-Divided Synchronous Stop Bit / Error signal output enable bit */
    }BIT;                                   /* Bit access                                  */
    char    BYTE;                           /* Byte access                                 */
};

/*----------------------------------------------------------------------------------
             UART3 receive buffer register//0x032E
------------------------------------------------------------------------------------*/
union st_u3rb{                     /* UART3 receive buffer register*/
    struct{
       unsigned char         :1;   /* Receive data                 */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Nothing assigned             */
       unsigned char         :1;   /* Nothing assigned             */
       unsigned char    ABT  :1;   /* Arbitration Lost Detect Flag */
       unsigned char    OER  :1;   /* Overrun Error Flag           */
       unsigned char    FER  :1;   /* Framing Error Flag           */
       unsigned char    PER  :1;   /* Parity Error Flag            */
       unsigned char    SUM  :1;   /* Error Sum Flag               */
    }BIT;                  /* Bit access                   */
    struct{
        char    LOW;       /* Low  8 bit                   */
        char    HIGH;      /* High 8 bit                   */
    }BYTE;                 /* Byte access                  */
    unsigned short  WORD;
};
/* when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------------------------
       UART2 special mode register 4//0x0334
-----------------------------------------------------------------------------------*/
union st_u2smr4{                      /* UART2 special mode register 4  */
    struct{
       unsigned char  STAREQ   :1;    /* Start Condition Generate Bi    */
       unsigned char  RSTAREQ  :1;    /* Restart Condition Generate Bit */
       unsigned char  STPREQ   :1;    /* Stop Condition Generate Bit    */
       unsigned char  STSPSEL  :1;    /* SCL, SDA Output Select Bit     */
       unsigned char  ACKD     :1;    /* ACK Data Bit                   */
       unsigned char  ACKC     :1;    /* ACK Data Output Enable Bit     */
       unsigned char  SCLHI    :1;    /* SCL Output Stop Enable Bit     */
       unsigned char  SWC9     :1;    /* SCL Wait Output Bit 3          */
    }BIT;                             /* Bit access                     */
    char    BYTE;                     /* Byte access                    */
};

/*---------------------------------------------------------------------------------
       UART2 special mode register 3//0x0335
-----------------------------------------------------------------------------------*/
union st_u2smr3{                   /* UART2 special mode register 3  */
    struct{
       unsigned char  SSE   :1;    /* SS Pin Function Enable Bit        */
       unsigned char  CKPH  :1;    /* Clock Phase Set Bit               */
       unsigned char  DINC  :1;    /* Serial Input Port Set Bit         */
       unsigned char  NODC  :1;    /* Clock Output Select Bit           */
       unsigned char  ERR   :1;    /* Fault Error Flag                  */
       unsigned char  DL0   :1;    /* SDA2 Digital Delay Time Set Bit   */
       unsigned char  DL1   :1;    /* SDA2 Digital Delay Time Set Bit   */
       unsigned char  DL2   :1;    /* SDA2 Digital Delay Time Set Bit   */
    }BIT;                          /* Bit access                        */
    char    BYTE;                  /* Byte access                       */
};

/*---------------------------------------------------------------------------------
       UART2 special mode register 2//0x0336
-----------------------------------------------------------------------------------*/
union st_u2smr2{                    /* UART2 special mode register 2         */
    struct{
       unsigned char  IICM2  :1;    /* I2C Mode Select Bit 2                 */
       unsigned char  CSC    :1;    /* Clock Synchronous Bit                 */
       unsigned char  SWC    :1;    /* SCL Wait Output Bit                   */
       unsigned char  ALS    :1;    /* SDA Output Stop Bit                   */
       unsigned char  STC    :1;    /* UART2 Initialize Bit                  */
       unsigned char  SWC2   :1;    /* SCL Wait Output Bit 2                 */
       unsigned char  SDHI   :1;    /* SDA Output Inhibit Bit                */
       unsigned char  SU1HIM :1;    /* External Clock Synchronous Enable Bit */
    }BIT;                           /* Bit access                            */
    char    BYTE;                   /* Byte access                           */
};

/*---------------------------------------------------------------------------------
       UART2 special mode register //0x0337
-----------------------------------------------------------------------------------*/
union st_u2smr{                      /* UART2 special mode register                            */
    struct{
       unsigned char  IICM    :1;    /* I2C Mode Select Bit                  				   */
       unsigned char  ABC     :1;    /* Arbitration Lost Detect Flag Control Bit			   */
       unsigned char  BBS     :1;    /* Bus Busy Flag                                          */
       unsigned char  LSYN    :1;    /* SCLL Sync Output Enable Bit                            */
       unsigned char  ABSCS   :1;    /* Bus Conflict Detect Sampling Clock Select Bit          */
       unsigned char  ACSE    :1;    /* Auto Clear Function Select Bit for Transmit Enable Bit */
       unsigned char  SSS     :1;    /* Transmit Start Condition Select Bit                    */
       unsigned char  SCLKDIV :1;    /* Clock Divide Synchronous Bit						   */
    }BIT;                            /* Bit access                                             */
    char    BYTE;                    /* Byte access                                            */
};

/*---------------------------------------------------------------------------------
       UART2 transmit/receive mode register //0x0338
-----------------------------------------------------------------------------------*/
union st_u2mr{                     /* UART2 transmit/receive mode register      */
    struct{
       unsigned char  SMD0  :1;    /* Serial I/O Mode Select Bit                */
       unsigned char  SMD1  :1;    /* Serial I/O Mode Select Bit	            */
       unsigned char  SMD2  :1;    /* Serial I/O Mode Select Bit                */
       unsigned char  CKDIR :1;    /* Internal/External Clock Select Bit        */
       unsigned char  STPS  :1;    /* Stop Bit Length Select Bit                */
       unsigned char  PRY   :1;    /* Odd/Even Parity Select Bit                */
       unsigned char  PRYE  :1;    /* Parity Enable Bit                         */
       unsigned char  IOPOL :1;    /* TxD,RxD Input/Output Polarity Switch Bit  */
    }BIT;                          /* Bit access                                */
    char    BYTE;                  /* Byte access                               */
};

/*----------------------------------------------------------------------------------
             UART2 transmit buffer register//0x033A
------------------------------------------------------------------------------------*/
union st_u2tb{                     /* UART2 transmit buffer register*/
    struct{
       unsigned char    B0   :1;   /* Transmit data    */
       unsigned char    B1   :1;   /* Transmit data    */
       unsigned char    B2   :1;   /* Transmit data    */
       unsigned char    B3   :1;   /* Transmit data    */
       unsigned char    B4   :1;   /* Transmit data    */
       unsigned char    B5   :1;   /* Transmit data    */
       unsigned char    B6   :1;   /* Transmit data    */
       unsigned char    B7   :1;   /* Transmit data    */
       unsigned char    B8   :1;   /* Transmit data    */
       unsigned char    B9   :1;   /* Nothing assigned */
       unsigned char    B10  :1;   /* Nothing assigned */
       unsigned char    B11  :1;   /* Nothing assigned */
       unsigned char    B12  :1;   /* Nothing assigned */
       unsigned char    B13  :1;   /* Nothing assigned */
       unsigned char    B14  :1;   /* Nothing assigned */
       unsigned char    B15  :1;   /* Nothing assigned */
    }BIT;                          /* Bit access       */
    struct{
        char    LOW;               /* Low  8 bit       */
        char    HIGH;              /* High 8 bit       */
    }BYTE;                         /* Byte access      */
    unsigned short  WORD;
};
/* when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------------------------
       UART2 transmit/receive control register 0 //0x033C
-----------------------------------------------------------------------------------*/
union st_u2c0{                     /* UART2 transmit/receive control register 0 */
    struct{
       unsigned char  CLK0  :1;    /* U2BRG CountSource Select Bit     */
       unsigned char  CLK1  :1;    /* U2BRG Count Source Select Bit    */
       unsigned char  CRS   :1;    /* CST/RTS Function Select Bit      */
       unsigned char  TXEPT :1;    /* Transmit Register Empty Flag     */
       unsigned char  CRD   :1;    /* CTS/RTS Disable Bit              */
       unsigned char  NCH   :1;    /* Data Output Select Bit           */
       unsigned char  CKPOL :1;    /* CLK Polarity Select Bit          */
       unsigned char  UFORM :1;    /* Transfer Format Select Bit       */
    }BIT;                          /* Bit access                       */
    char    BYTE;                  /* Byte access                      */
};

/*---------------------------------------------------------------------------------
       UART2 transmit/receive control register 1 //0x033D
-----------------------------------------------------------------------------------*/
union st_u2c1{                              /* UART2 transmit/receive control register 1 */
    struct{
       unsigned char TE              :1;    /* Transmit Enable Bit                         */
       unsigned char TI              :1;    /* Transmit Buffer Empty Flag                  */
       unsigned char RE              :1;    /* Receive Enable Bit                          */
       unsigned char RI              :1;    /* Receive Complete Flag                       */
       unsigned char U2IRS           :1;    /* UART2 Transmit Interrupt Cause Select Bit   */
       unsigned char U2RRM           :1;    /* UART2 Continuous Receive Mode Enable Bit    */
       unsigned char U2LCH           :1;    /* Data Logic Select Bit                       */
       unsigned char SCLKSTPB_U2ERE  :1;    /* Clock-Divided Synchronous Stop Bit  / Error signal output enable bit        */
    }BIT;                                   /* Bit access                                  */
    char    BYTE;                           /* Byte access                                 */
};

/*----------------------------------------------------------------------------------
             UART2 receive buffer register//0x033E
------------------------------------------------------------------------------------*/
union st_u2rb{                     /* UART2 receive buffer register*/
    struct{
       unsigned char         :1;   /* Receive data                 */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Nothing assigned             */
       unsigned char         :1;   /* Nothing assigned             */
       unsigned char    ABT  :1;   /* Arbitration Lost Detect Flag */
       unsigned char    OER  :1;   /* Overrun Error Flag           */
       unsigned char    FER  :1;   /* Framing Error Flag           */
       unsigned char    PER  :1;   /* Parity Error Flag            */
       unsigned char    SUM  :1;   /* Error Sum Flag               */
    }BIT;                          /* Bit access                   */
    struct{
        char    LOW;               /* Low  8 bit                   */
        char    HIGH;              /* High 8 bit                   */
    }BYTE;                         /* Byte access                  */
    unsigned short  WORD;          /* Word access                  */
};
/* when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    Count start flag // 0x0340
------------------------------------------------------*/
union st_tabsr{                    /* Count start flag            */
    struct{
       unsigned char TA0S   :1;    /* Timer A0 count start flag   */
       unsigned char TA1S   :1;    /* Timer A1 count start flag   */
       unsigned char TA2S   :1;    /* Timer A2 count start flag   */
       unsigned char TA3S   :1;    /* Timer A3 count start flag   */
       unsigned char TA4S   :1;    /* Timer A4 count start flag   */
       unsigned char TB0S   :1;    /* Timer B0 count start flag   */
       unsigned char TB1S   :1;    /* Timer B1 count start flag   */
       unsigned char TB2S   :1;    /* Timer B2 count start flag   */
    }BIT;                          /* Bit access                  */
    char    BYTE;                  /* Byte access                 */
};

/*------------------------------------------------------
    Clock prescaler reset flag// 0x0341
------------------------------------------------------*/
union st_cpsrf{                     /* Clock prescaler reset flag */
    struct{
        unsigned char        :1;    /* Nothing assigned            */
        unsigned char        :1;    /* Nothing assigned            */
        unsigned char        :1;    /* Nothing assigned            */
        unsigned char        :1;    /* Nothing assigned            */
        unsigned char        :1;    /* Nothing assigned            */
        unsigned char        :1;    /* Nothing assigned            */
        unsigned char        :1;    /* Nothing assigned            */
        unsigned char  CPSR  :1;    /* Clock Prescaler Reset Flag  */
    }BIT;                           /* Bit access                  */
    char    BYTE;                   /* Byte access                 */
};
/* when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    One-shot start flag //0x0342
------------------------------------------------------*/
union st_onsf{              /* One-shot start flag               */
    struct{
       unsigned char  TA0OS  :1;    /* Timer A0 one-shot start flag      */
       unsigned char  TA1OS  :1;    /* Timer A1 one-shot start flag      */
       unsigned char  TA2OS  :1;    /* Timer A2 one-shot start flag 	 */
       unsigned char  TA3OS  :1;    /* Timer A3 one-shot start flag 	 */
       unsigned char  TA4OS  :1;    /* Timer A4 one-shot start flag 	 */
       unsigned char  TAZIE  :1;    /* Z-phase input enable bit          */
       unsigned char  TA0TGL :1;    /* Timer A0 event/trigger select bit */
       unsigned char  TA0TGH :1;    /* Timer A0 event/trigger select bit */
    }BIT;                   /* Bit access                        */
    char    BYTE;           /* Byte access                       */
};

/*------------------------------------------------------
     Trigger select register //0x0343
------------------------------------------------------*/
union st_trgsr{                    /* Trigger select register           */
    struct{
       unsigned char  TA1TGL  :1;  /* Timer A1 event/trigger select bit */
       unsigned char  TA1TGH  :1;  /* Timer A1 event/trigger select bit */
       unsigned char  TA2TGL  :1;  /* Timer A2 event/trigger select bit */
       unsigned char  TA2TGH  :1;  /* Timer A2 event/trigger select bit */
       unsigned char  TA3TGL  :1;  /* Timer A3 event/trigger select bit */
       unsigned char  TA3TGH  :1;  /* Timer A3 event/trigger select bit */
       unsigned char  TA4TGL  :1;  /* Timer A4 event/trigger select bit */
       unsigned char  TA4TGH  :1;  /* Timer A4 event/trigger select bit */
    }BIT;                          /* Bit access                        */
    char    BYTE;                  /* Byte access                       */
};

/*------------------------------------------------------
    Up Down Flag //0x0344
        (1) The MOV instruction should be used to set the UDF register
------------------------------------------------------*/
union st_udf{                     /* Up Down Flag */
    struct{
       unsigned char  TA0UD  :1;  /* Timer A0 up/down flag           */
       unsigned char  TA1UD  :1;  /* Timer A1 up/down flag 			 */
       unsigned char  TA2UD  :1;  /* Timer A2 up/down flag 			 */
       unsigned char  TA3UD  :1;  /* Timer A3 up/down flag 			 */
       unsigned char  TA4UD  :1;  /* Timer A4 up/down flag 			 */
       unsigned char  TA2P   :1;  /* Timer A2 2-phase pulse signal processing function select bit  */
       unsigned char  TA3P   :1;  /* Timer A3 2-phase pulse signal processing function select bit  */
       unsigned char  TA4P   :1;  /* Timer A4 2-phase pulse signal processing function select bit  */
    }BIT;                         /* Bit access                     */
    char    BYTE;                 /* Byte access                    */
};

/*------------------------------------------------------
     Timer A0 mode register //0x0356
------------------------------------------------------*/
union st_ta0mr{           /* Timer A0 mode register */
    struct{
       unsigned char  TMOD0  :1;  /* Operating Mode Select Bit     */
       unsigned char  TMOD1  :1;  /* Operating Mode Select Bit     */
       unsigned char         :1;  /* Reserved Bit must be set to 0 */
       unsigned char  MR1    :1;  /* External Trigger Select Bit   */
       unsigned char  MR2    :1;  /* Trigger Select Bit            */
       unsigned char  MR3    :1;  /* 16/8-Bit PWM Mode Select Bit  */
       unsigned char  TCK0   :1;  /* Count Source Select Bit       */
       unsigned char  TCK1   :1;  /* Count Source Select Bit       */
    }BIT;                 /* Bit access                    */
    char    BYTE;         /* Byte access                   */
};

/*------------------------------------------------------
     Timer A1 mode register //0x0357
------------------------------------------------------*/
union st_ta1mr{                   /* Timer A1 mode register        */
    struct{
       unsigned char  TMOD0  :1;  /* Operating Mode Select Bit     */
       unsigned char  TMOD1  :1;  /* Operating Mode Select Bit     */
       unsigned char         :1;  /* Reserved Bit must be set to 0 */
       unsigned char  MR1    :1;  /* External Trigger Select Bit   */
       unsigned char  MR2    :1;  /* Trigger Select Bit            */
       unsigned char  MR3    :1;  /* 16/8-Bit PWM Mode Select Bit  */
       unsigned char  TCK0   :1;  /* Count Source Select Bit       */
       unsigned char  TCK1   :1;  /* Count Source Select Bit       */
    }BIT;                         /* Bit access                    */
    char    BYTE;                 /* Byte access                   */
};

/*------------------------------------------------------
     Timer A2 mode register //0x0358
------------------------------------------------------*/
union st_ta2mr{                   /* Timer A2 mode register        */
    struct{
       unsigned char  TMOD0  :1;  /* Operating Mode Select Bit     */
       unsigned char  TMOD1  :1;  /* Operating Mode Select Bit     */
       unsigned char         :1;  /* Reserved Bit must be set to 0 */
       unsigned char  MR1    :1;  /* External Trigger Select Bit   */
       unsigned char  MR2    :1;  /* Trigger Select Bit            */
       unsigned char  MR3    :1;  /* 16/8-Bit PWM Mode Select Bit  */
       unsigned char  TCK0   :1;  /* Count Source Select Bit       */
       unsigned char  TCK1   :1;  /* Count Source Select Bit       */
    }BIT;                         /* Bit access                    */
    char    BYTE;                 /* Byte access                   */
};

/*------------------------------------------------------
     Timer A3 mode register //0x0359
------------------------------------------------------*/
union st_ta3mr{                   /* Timer A3 mode register        */
    struct{
       unsigned char  TMOD0  :1;  /* Operating Mode Select Bit     */
       unsigned char  TMOD1  :1;  /* Operating Mode Select Bit     */
       unsigned char         :1;  /* Reserved Bit must be set to 0 */
       unsigned char  MR1    :1;  /* External Trigger Select Bit   */
       unsigned char  MR2    :1;  /* Trigger Select Bit            */
       unsigned char  MR3    :1;  /* 16/8-Bit PWM Mode Select Bit  */
       unsigned char  TCK0   :1;  /* Count Source Select Bit       */
       unsigned char  TCK1   :1;  /* Count Source Select Bit       */
    }BIT;                         /* Bit access                    */
    char    BYTE;                 /* Byte access                   */
};

/*------------------------------------------------------
     Timer A4 mode register //0x035A
------------------------------------------------------*/
union st_ta4mr{                   /* Timer A4 mode register        */
    struct{
       unsigned char  TMOD0  :1;  /* Operating Mode Select Bit     */
       unsigned char  TMOD1  :1;  /* Operating Mode Select Bit     */
       unsigned char         :1;  /* Reserved Bit must be set to 0 */
       unsigned char  MR1    :1;  /* External Trigger Select Bit   */
       unsigned char  MR2    :1;  /* Trigger Select Bit            */
       unsigned char  MR3    :1;  /* 16/8-Bit PWM Mode Select Bit  */
       unsigned char  TCK0   :1;  /* Count Source Select Bit       */
       unsigned char  TCK1   :1;  /* Count Source Select Bit       */
    }BIT;                         /* Bit access                    */
    char    BYTE;                 /* Byte access                   */
};

/*------------------------------------------------------
     Timer B0 mode register //0x035B
------------------------------------------------------*/
union st_tb0mr{                   /* Timer B0 mode register                        */
    struct{
       unsigned char  TMOD0  :1;  /* Operating Mode Select Bit                     */
       unsigned char  TMOD1  :1;  /* Operating Mode Select Bit                     */
       unsigned char  MR0    :1;  /* Function varies depending on operating mode   */
       unsigned char  MR1    :1;  /* Function varies depending on operating mode   */
       unsigned char  MR2    :1;  /* Function varies depending on operating mode   */
       unsigned char  MR3    :1;  /* Function varies depending on operating mode   */
       unsigned char  TCK0   :1;  /* Count Source Select Bit.                      */
       unsigned char  TCK1   :1;  /* Count Source Select Bit.                      */
        }BIT;                      /* Bit access                                    */
    char    BYTE;                 /* Byte access                                   */
};

/*------------------------------------------------------
     Timer B1 mode register //0x035C
------------------------------------------------------*/
union st_tb1mr{           /* Timer B1 mode register        */
    struct{
        char  TMOD0  :1;  /* Operating Mode Select Bit                     */
        char  TMOD1  :1;  /* Operating Mode Select Bit                     */
        char  MR0    :1;  /* Function varies depending on operating mode   */
        char  MR1    :1;  /* Function varies depending on operating mode   */
        char  MR2    :1;  /* Function varies depending on operating mode   */
        char  MR3    :1;  /* Function varies depending on operating mode   */
        char  TCK0   :1;  /* Count Source Select Bit.                      */
        char  TCK1   :1;  /* Count Source Select Bit.                      */
        }BIT;              /* Bit access                    */
    char    BYTE;         /* Byte access                   */
};

/*------------------------------------------------------
     Timer B2 mode register //0x035D
------------------------------------------------------*/
union st_tb2mr{                   /* Timer B2 mode register                        */
    struct{
       unsigned char  TMOD0  :1;  /* Operating Mode Select Bit                     */
       unsigned char  TMOD1  :1;  /* Operating Mode Select Bit                     */
       unsigned char  MR0    :1;  /* Function varies depending on operating mode   */
       unsigned char  MR1    :1;  /* Function varies depending on operating mode   */
       unsigned char  MR2    :1;  /* Function varies depending on operating mode   */
       unsigned char  MR3    :1;  /* Function varies depending on operating mode   */
       unsigned char  TCK0   :1;  /* Count Source Select Bit.                      */
       unsigned char  TCK1   :1;  /* Count Source Select Bit.                      */
        }BIT ;                     /* Bit access                                    */
    char    BYTE;                 /* Byte access                                   */
};

/*------------------------------------------------------
    Timer B2 special mode register//0x035E
------------------------------------------------------*/
union st_tb2sc{                   /* Timer B2 special mode register       */
    struct{
       unsigned char  PWCON  :1;  /* Timer B2 reload timing switching bit */
       unsigned char         :1;  /* Nothing is assigned                  */
       unsigned char         :1;  /* Nothing is assigned                  */
       unsigned char         :1;  /* Nothing is assigned                  */
       unsigned char         :1;  /* Nothing is assigned                  */
       unsigned char         :1;  /* Nothing is assigned                  */
       unsigned char         :1;  /* Nothing is assigned                  */
       unsigned char         :1;  /* Nothing is assigned                  */
        }BIT ;                     /* Bit access                           */
    char    BYTE;                 /* Byte access                          */
};
/* When nothing assigned,then when write, set to "0".When read, its content is "0."*/

/*------------------------------------------------------
   Count source prescaler register //0x035F
------------------------------------------------------*/
union st_tcspr{                  /* Count source prescaler register */
    struct{
       unsigned char  CNT0  :1;  /* Divide ratio select bit        */
       unsigned char  CNT1  :1;  /* Divide ratio select bit        */
       unsigned char  CNT2  :1;  /* Divide ratio select bit        */
       unsigned char  CNT3  :1;  /* Divide ratio select bit 	   */
       unsigned char        :1;  /* Reserved bit must be set to 0  */
       unsigned char        :1;  /* Reserved bit must be set to 0  */
       unsigned char        :1;  /* Reserved bit must be set to 0  */
       unsigned char  CST   :1;  /* Operation enable bit           */
        }BIT ;                    /* Bit access                     */
    char    BYTE;                /* Byte access                    */
};

/*---------------------------------------------------------------------------------
       UART0 special mode register 4//0x0364
-----------------------------------------------------------------------------------*/
union st_u0smr4{              /* UART0 special mode register 4  */
    struct{
       unsigned char  STAREQ   :1;    /* Start Condition Generate Bit   */
       unsigned char  RSTAREQ  :1;    /* Restart Condition Generate Bit */
       unsigned char  STPREQ   :1;    /* Stop Condition Generate Bit    */
       unsigned char  STSPSEL  :1;    /* SCL, SDA Output Select Bit     */
       unsigned char  ACKD     :1;    /* ACK Data Bit                   */
       unsigned char  ACKC     :1;    /* ACK Data Output Enable Bit     */
       unsigned char  SCLHI    :1;    /* SCL Output Stop Enable Bit     */
       unsigned char  SWC9     :1;    /* SCL Wait Output Bit 3          */
    }BIT;                     /* Bit access                     */
    char    BYTE;             /* Byte access                    */
};

/*---------------------------------------------------------------------------------
       UART0 special mode register 3//0x0365
-----------------------------------------------------------------------------------*/
union st_u0smr3{                   /* UART0 special mode register 3     */
    struct{
       unsigned char  SSE   :1;    /* SS Pin Function Enable Bit        */
       unsigned char  CKPH  :1;    /* Clock Phase Set Bit               */
       unsigned char  DINC  :1;    /* Serial Input Port Set Bit         */
       unsigned char  NODC  :1;    /* Clock Output Select Bit           */
       unsigned char  ERR   :1;    /* Fault Error Flag                  */
       unsigned char  DL0   :1;    /* SDA0 Digital Delay Time Set Bit   */
       unsigned char  DL1   :1;    /* SDA0 Digital Delay Time Set Bit   */
       unsigned char  DL2   :1;    /* SDA0 Digital Delay Time Set Bit   */
    }BIT;                          /* Bit access                        */
    char    BYTE;                  /* Byte access                       */
};

/*---------------------------------------------------------------------------------
       UART0 special mode register 2//0x0366
-----------------------------------------------------------------------------------*/
union st_u0smr2{                    /* UART0 special mode register 2         */
    struct{
       unsigned char  IICM2  :1;    /* I2C Mode Select Bit 2                 */
       unsigned char  CSC    :1;    /* Clock Synchronous Bit                 */
       unsigned char  SWC    :1;    /* SCL Wait Output Bit                   */
       unsigned char  ALS    :1;    /* SDA Output Stop Bit                   */
       unsigned char  STC    :1;    /* UART0 Initialize Bit                  */
       unsigned char  SWC2   :1;    /* SCL Wait Output Bit 2                 */
       unsigned char  SDHI   :1;    /* SDA Output Inhibit Bit                */
       unsigned char  SU1HIM :1;    /* External Clock Synchronous Enable Bit */
    }BIT;                           /* Bit access                            */
    char    BYTE;                   /* Byte access                           */
};

/*---------------------------------------------------------------------------------
       UART0 special mode register //0x0367
-----------------------------------------------------------------------------------*/
union st_u0smr{                      /* UART0 special mode register                            */
    struct{
       unsigned char  IICM    :1;    /* I2C Mode Select Bit                  				   */
       unsigned char  ABC     :1;    /* Arbitration Lost Detect Flag Control Bit			   */
       unsigned char  BBS     :1;    /* Bus Busy Flag                                          */
       unsigned char  LSYN    :1;    /* SCLL Sync Output Enable Bit                            */
       unsigned char  ABSCS   :1;    /* Bus Conflict Detect Sampling Clock Select Bit          */
       unsigned char  ACSE    :1;    /* Auto Clear Function Select Bit for Transmit Enable Bit */
       unsigned char  SSS     :1;    /* Transmit Start Condition Select Bit                    */
       unsigned char  SCLKDIV :1;    /* Clock Divide Synchronous Bit						   */
    }BIT;                            /* Bit access                                             */
    char    BYTE;                    /* Byte access                                            */
};

/*---------------------------------------------------------------------------------
       UART0 transmit/receive mode register //0x0368
-----------------------------------------------------------------------------------*/
union st_u0mr{                     /* UART0 transmit/receive mode register      */
    struct{
       unsigned char  SMD0  :1;    /* Serial I/O Mode Select Bit                */
       unsigned char  SMD1  :1;    /* Serial I/O Mode Select Bit	            */
       unsigned char  SMD2  :1;    /* Serial I/O Mode Select Bit                */
       unsigned char  CKDIR :1;    /* Internal/External Clock Select Bit        */
       unsigned char  STPS  :1;    /* Stop Bit Length Select Bit                */
       unsigned char  PRY   :1;    /* Odd/Even Parity Select Bit                */
       unsigned char  PRYE  :1;    /* Parity Enable Bit                         */
       unsigned char  IOPOL :1;    /* TxD,RxD Input/Output Polarity Switch Bit  */
    }BIT;                          /* Bit access                                */
    char    BYTE;                  /* Byte access                               */
};

/*----------------------------------------------------------------------------------
             UART0 transmit buffer register//0x036A
------------------------------------------------------------------------------------*/
union st_u0tb{                     /* UART0 transmit buffer register*/
    struct{
       unsigned char    B0   :1;   /* Transmit data    */
       unsigned char    B1   :1;   /* Transmit data    */
       unsigned char    B2   :1;   /* Transmit data    */
       unsigned char    B3   :1;   /* Transmit data    */
       unsigned char    B4   :1;   /* Transmit data    */
       unsigned char    B5   :1;   /* Transmit data    */
       unsigned char    B6   :1;   /* Transmit data    */
       unsigned char    B7   :1;   /* Transmit data    */
       unsigned char    B8   :1;   /* Transmit data    */
       unsigned char    B9   :1;   /* Nothing assigned */
       unsigned char    B10  :1;   /* Nothing assigned */
       unsigned char    B11  :1;   /* Nothing assigned */
       unsigned char    B12  :1;   /* Nothing assigned */
       unsigned char    B13  :1;   /* Nothing assigned */
       unsigned char    B14  :1;   /* Nothing assigned */
       unsigned char    B15  :1;   /* Nothing assigned */
    }BIT;                          /* Bit access       */
    struct{
        char    LOW;       /* Low  8 bit       */
        char    HIGH;      /* High 8 bit       */
    }BYTE;                         /* Byte access      */
    unsigned short  WORD;
};
/* when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*---------------------------------------------------------------------------------
       UART0 transmit/receive control register 0 //0x036C
-----------------------------------------------------------------------------------*/
union st_u0c0{                     /* UART0 transmit/receive control register 0 */
    struct{
       unsigned char  CLK0  :1;    /* U0BRG CountSource Select Bit     */
       unsigned char  CLK1  :1;    /* U0BRG Count Source Select Bit    */
       unsigned char  CRS   :1;    /* CST/RTS Function Select Bit      */
       unsigned char  TXEPT :1;    /* Transmit Register Empty Flag     */
       unsigned char  CRD   :1;    /* CTS/RTS Disable Bit              */
       unsigned char  NCH   :1;    /* Data Output Select Bit           */
       unsigned char  CKPOL :1;    /* CLK Polarity Select Bit          */
       unsigned char  UFORM :1;    /* Transfer Format Select Bit       */
    }BIT;                          /* Bit access                       */
    char    BYTE;                  /* Byte access                      */
};

/*---------------------------------------------------------------------------------
       UART0 transmit/receive control register 1 //0x036D
-----------------------------------------------------------------------------------*/
union st_u0c1{                              /* UART0 transmit/receive control register 1 */
    struct{
       unsigned char TE              :1;    /* Transmit Enable Bit                         */
       unsigned char TI              :1;    /* Transmit Buffer Empty Flag                  */
       unsigned char RE              :1;    /* Receive Enable Bit                          */
       unsigned char RI              :1;    /* Receive Complete Flag                       */
       unsigned char U0IRS           :1;    /* UART0 Transmit Interrupt Cause Select Bit   */
       unsigned char U0RRM           :1;    /* UART0 Continuous Receive Mode Enable Bit    */
       unsigned char U0LCH           :1;    /* Data Logic Select Bit                       */
       unsigned char SCLKSTPB_U0ERE  :1;    /* Clock-Divided Synchronous Stop Bit /  Error Signal Output Enable Bit        */
    }BIT;                                   /* Bit access                                  */
    char    BYTE;                           /* Byte access                                 */
};

/*----------------------------------------------------------------------------------
             UART0 receive buffer register//0x036E
------------------------------------------------------------------------------------*/
union st_u0rb{                     /* UART0 receive buffer register*/
    struct{
       unsigned char         :1;   /* Receive data                 */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Receive data    			   */
       unsigned char         :1;   /* Nothing assigned             */
       unsigned char         :1;   /* Nothing assigned             */
       unsigned char    ABT  :1;   /* Arbitration Lost Detect Flag */
       unsigned char    OER  :1;   /* Overrun Error Flag           */
       unsigned char    FER  :1;   /* Framing Error Flag           */
       unsigned char    PER  :1;   /* Parity Error Flag            */
       unsigned char    SUM  :1;   /* Error Sum Flag               */
    }BIT;                          /* Bit access                   */
    struct{
        char    LOW;       /* Low  8 bit                   */
        char    HIGH;      /* High 8 bit                   */
    }BYTE;                         /* Byte access                  */
    unsigned short  WORD;
};
/* when nothing assigned then when write, set to "0".When read, its content is indeterminate.*/

/*--------------------------------------------------------
        DMA0 Request Source Select Register //0x0378//
----------------------------------------------------------*/
union st_dm0sl{                    /* DMA0 Request Source Select Register    */
    struct{
       unsigned char DSEL0  :1;    /* DMA request cause select bit   */
       unsigned char DSEL1  :1;    /* DMA request cause select bit   */
       unsigned char DSEL2  :1;    /* DMA request cause select bit   */
       unsigned char DSEL3  :1;    /* DMA request cause select bit   */
       unsigned char DSEL4  :1;    /* DMA request cause select bit   */
       unsigned char DSR    :1;    /* Software DMA request bit       */
       unsigned char        :1;    /* Reserved bit must be set to 0  */
       unsigned char DRQ    :1;    /* DMA request bit                */
    }BIT;                          /* Bit access                     */
    char    BYTE;                  /* Byte access                    */
};

/*--------------------------------------------------------
        DMA1 Request Source Select Register //0x0379//
----------------------------------------------------------*/
union st_dm1sl{                    /* DMA1 Request Source Select Register     */
    struct{
       unsigned char DSEL0  :1;    /* DMA request cause select bit   */
       unsigned char DSEL1  :1;    /* DMA request cause select bit   */
       unsigned char DSEL2  :1;    /* DMA request cause select bit   */
       unsigned char DSEL3  :1;    /* DMA request cause select bit   */
       unsigned char DSEL4  :1;    /* DMA request cause select bit   */
       unsigned char DSR    :1;    /* Software DMA request bit       */
       unsigned char        :1;    /* Reserved bit must be set to 0  */
       unsigned char DRQ    :1;    /* DMA request bit                */
    }BIT;                  /* Bit access                     */
    char    BYTE;          /* Byte access                    */
};

/*--------------------------------------------------------
        DMA2 request source select register //0x037A//
----------------------------------------------------------*/
union st_dm2sl{                    /* DMA2 request source select register*/
    struct{
       unsigned char DSEL0  :1;    /* DMA request cause select bit   */
       unsigned char DSEL1  :1;    /* DMA request cause select bit   */
       unsigned char DSEL2  :1;    /* DMA request cause select bit   */
       unsigned char DSEL3  :1;    /* DMA request cause select bit   */
       unsigned char DSEL4  :1;    /* DMA request cause select bit   */
       unsigned char DSR    :1;    /* Software DMA request bit       */
       unsigned char        :1;    /* Reserved bit must be set to 0  */
       unsigned char DRQ    :1;    /* DMA request bit                */
    }BIT;                  /* Bit access                     */
    char    BYTE;          /* Byte access                    */
};

/*--------------------------------------------------------
        DMA3 request source select register //0x037B//
----------------------------------------------------------*/
union st_dm3sl{                    /* DMA3 request source select register*/
    struct{
       unsigned char DSEL0  :1;    /* DMA request cause select bit   */
       unsigned char DSEL1  :1;    /* DMA request cause select bit   */
       unsigned char DSEL2  :1;    /* DMA request cause select bit   */
       unsigned char DSEL3  :1;    /* DMA request cause select bit   */
       unsigned char DSEL4  :1;    /* DMA request cause select bit   */
       unsigned char DSR    :1;    /* Software DMA request bit       */
       unsigned char        :1;    /* Reserved bit must be set to 0  */
       unsigned char DRQ    :1;    /* DMA request bit                */
    }BIT;                  /* Bit access                     */
    char    BYTE;          /* Byte access                    */
};

/*------------------------------------------------------
    CRC data register // 0x037C//
------------------------------------------------------*/
union st_crcd{             /* CRC data register */
        struct{
        char    LOW;       /* CRC data register low  8 bit  */
        char    HIGH;      /* CRC data register high 8 bit  */
    }BYTE;                 /* Byte access                   */
    unsigned short  WORD;
};

/*------------------------------------------------------
    A/D0 register 0 // 0x0380-0x0381//
------------------------------------------------------*/
union st_ad00{             /* A/D0 register 0 */
        struct{
        char    LOW;       /* A/D0 register 0 low  8 bit  */
        char    HIGH;      /* A/D0 register 0 high 8 bit  */
    }BYTE;                 /* Byte access                 */
    unsigned short  WORD;
};

/*------------------------------------------------------
    A/D0 register 1 // 0x0382-0x0383//
------------------------------------------------------*/
union st_ad01{             /* A/D0 register 1 */
        struct{
        char    LOW;       /* A/D0 register 1 low  8 bit  */
        char    HIGH;      /* A/D0 register 1 high 8 bit  */
    }BYTE;                 /* Byte access                 */
    unsigned short  WORD;
};

/*------------------------------------------------------
    A/D0 register 2 // 0x0384-0x0385//
------------------------------------------------------*/
union st_ad02{             /* A/D0 register 2 */
        struct{
        char    LOW;       /* A/D0 register 2 low  8 bit  */
        char    HIGH;      /* A/D0 register 2 high 8 bit  */
    }BYTE;                 /* Byte access                 */
    unsigned short  WORD;
};

/*------------------------------------------------------
    A/D0 register 3 // 0x0386-0x0387//
------------------------------------------------------*/
union st_ad03{             /* A/D0 register 3 */
        struct{
        char    LOW;       /* A/D0 register 3 low  8 bit  */
        char    HIGH;      /* A/D0 register 3 high 8 bit  */
    }BYTE;                 /* Byte access                 */
    unsigned short  WORD;
};

/*------------------------------------------------------
    A/D0 register 4 // 0x0388-0x0389//
------------------------------------------------------*/
union st_ad04{             /* A/D0 register 4 */
        struct{
        char    LOW;       /* A/D0 register 4 low  8 bit  */
        char    HIGH;      /* A/D0 register 4 high 8 bit  */
    }BYTE;                 /* Byte access                 */
    unsigned short  WORD;
};

/*------------------------------------------------------
    A/D0 register 5 // 0x038A-0x038B//
------------------------------------------------------*/
union st_ad05{             /* A/D0 register 5 */
        struct{
        char    LOW;       /* A/D0 register 5 low  8 bit  */
        char    HIGH;      /* A/D0 register 5 high 8 bit  */
    }BYTE;                 /* Byte access                 */
    unsigned short  WORD;
};

/*------------------------------------------------------
    A/D0 register 6 // 0x038C-0x038D//
------------------------------------------------------*/
union st_ad06{             /* A/D0 register 6 */
        struct{
        char    LOW;       /* A/D0 register 6 low  8 bit  */
        char    HIGH;      /* A/D0 register 6 high 8 bit  */
    }BYTE;                 /* Byte access                 */
    unsigned short  WORD;
};

/*------------------------------------------------------
    A/D0 register 7 // 0x038E-0x038F//
------------------------------------------------------*/
union st_ad07{             /* A/D0 register 7 */
        struct{
        char    LOW;       /* A/D0 register 7 low  8 bit  */
        char    HIGH;      /* A/D0 register 7 high 8 bit  */
    }BYTE;                 /* Byte access                 */
    unsigned short  WORD;
};

/*--------------------------------------------------------
        A/D0 Control Register 4 //0x0392//
----------------------------------------------------------*/
union st_ad0con4{          /* A/D0 Control Register 4           */
    struct{
       unsigned char        :1;    /* Reserved bit must be set to 0     */
       unsigned char        :1;    /* Reserved bit must be set to 0     */
       unsigned char  MPS10 :1;    /* Multi-port sweep port select bit  */
       unsigned char  MPS11 :1;    /* Multi-port sweep port select bit  */
       unsigned char        :1;    /* Reserved bit must be set to 0     */
       unsigned char        :1;    /* Reserved bit must be set to 0     */
       unsigned char        :1;    /* Reserved bit must be set to 0     */
       unsigned char        :1;    /* Reserved bit must be set to 0     */
    }BIT;                  /* Bit access                        */
    char    BYTE;          /* Byte access                       */
};

/*--------------------------------------------------------
        A/D0 Control Register 2 //0x0394//
----------------------------------------------------------*/
union st_ad0con2{         /* A/D0 Control Register 2 */
    struct{
       unsigned char  SMP  :1;    /* A/D conversion method select bit           */
       unsigned char  APS0 :1;    /* Analog input port select bit               */
       unsigned char  APS1 :1;    /* Analog input port select bit               */
       unsigned char       :1;    /* Nothing  assigned                          */
       unsigned char       :1;    /* Nothing  assigned                          */
       unsigned char  TRG0 :1;    /* External trigger request cause select bit  */
       unsigned char       :1;    /* Reserved bit must be set to 0              */
       unsigned char       :1;    /* Reserved bit must be set to 0              */
    }BIT;                 /* Bit access                                 */
    char    BYTE;         /* Byte access                                */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*--------------------------------------------------------
        A/D0 Control Register 3 //0x0395//
----------------------------------------------------------*/
union st_ad0con3{                 /* A/D0 Control Register 3 */
    struct{
       unsigned char  DUS  :1;    /* DMAC operation select bit         */
       unsigned char  MSS  :1;    /* Multi-port sweep mode select bit  */
       unsigned char  CKS2 :1;    /* Frequency select bit              */
       unsigned char  MSF0 :1;    /* Multi-port sweep status flag      */
       unsigned char  MSF1 :1;    /* Multi-port sweep status flag      */
       unsigned char       :1;    /* Reserved bit must be set to 0     */
       unsigned char       :1;    /* Reserved bit must be set to 0     */
       unsigned char       :1;    /* Reserved bit must be set to 0     */
    }BIT;                 /* Bit access                        */
    char    BYTE;         /* Byte access                       */
};

/*--------------------------------------------------------
        A/D0 Control Register 0 //0x0396//
----------------------------------------------------------*/
union st_ad0con0{         /* A/D0 Control Register 0 */
    struct{
       unsigned char  CH0  :1;    /* Analog input pin select bit     */
       unsigned char  CH1  :1;    /* Analog input pin select bit     */
       unsigned char  CH2  :1;    /* Analog input pin select bit     */
       unsigned char  MD0  :1;    /* A/D operation mode select bit 0 */
       unsigned char  MD1  :1;    /* A/D operation mode select bit 0 */
       unsigned char  TRG  :1;    /* Trigger select bit              */
       unsigned char  ADST :1;    /* A/D conversion start flag       */
       unsigned char  CKS0 :1;    /* Frequency select bit 0          */
    }BIT;                 /* Bit access                      */
    char    BYTE;         /* Byte access                     */
};

/*--------------------------------------------------------
        A/D0 Control Register 1 //0x0397
----------------------------------------------------------*/
union st_ad0con1{           /* A/D0 Control Register 1 */
    struct{
       unsigned char  SCAN0  :1;    /* A/D sweep pin select bit            */
       unsigned char  SCAN1  :1;    /* A/D sweep pin select bit            */
       unsigned char  MD2    :1;    /* A/D operation mode select bit 1     */
       unsigned char  BITS   :1;    /* 8/10-bit mode select bit            */
       unsigned char  CKS1   :1;    /* Frequency select bit 1              */
       unsigned char  VCUT   :1;    /* Vref connection bit                 */
       unsigned char  OPA0   :1;    /* External op-amp connection mode bit */
       unsigned char  OPA1   :1;    /* External op-amp connection mode bit */
    }BIT;                   /* Bit access                          */
    char    BYTE;           /* Byte access                         */
};

/*--------------------------------------------------------
        D/A control register //0x039C
----------------------------------------------------------*/
union st_dacon{          /* D/A control register */
    struct{
        char  da0e  :1;  /* D/A0 output enable bit  */
        char  da1e  :1;  /* D/A1 output enable bit  */
        char        :6;  /* Nothing assigned        */
          }BIT;          /* Bit access                          */
    char    BYTE;        /* Byte access                         */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*--------------------------------------------------------
        Function select register A8 //0x03A0
----------------------------------------------------------*/
union st_ps8{               /* Function select register A8 */
    struct{
       unsigned char  PS8_0  :1;    /* Port P140 output function select bit  */
       unsigned char  PS8_1  :1;    /* Port P141 output function select bit  */
       unsigned char  PS8_2  :1;    /* Port P142 output function select bit  */
       unsigned char  PS8_3  :1;    /* Port P143 output function select bit  */
       unsigned char         :1;    /* Reserved bit must be set to 0         */
       unsigned char         :1;    /* Reserved bit must be set to 0         */
       unsigned char         :1;    /* Reserved bit must be set to 0         */
       unsigned char         :1;    /* Nothing  assigned                     */
    }BIT;                   /* Bit access                            */
    char    BYTE;           /* Byte access                           */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*--------------------------------------------------------
        Function select register A9 //0x03A1
----------------------------------------------------------*/
union st_ps9{               /* Function select register A9 */
    struct{
       unsigned char  PS9_0  :1;    /* Port P150 output function select bit  */
       unsigned char  PS9_1  :1;    /* Port P151 output function select bit  */
       unsigned char         :1;    /* Reserved bit must be set to 0         */
       unsigned char         :1;    /* Reserved bit must be set to 0         */
       unsigned char         :1;    /* Reserved bit must be set to 0         */
       unsigned char         :1;    /* Reserved bit must be set to 0         */
       unsigned char         :1;    /* Reserved bit must be set to 0         */
       unsigned char         :1;    /* Reserved bit must be set to 0         */
    }BIT;                   /* Bit access                            */
    char    BYTE;           /* Byte access                           */
};

/*------------------------------------------------------
    Function Select Register D1//0x03A7
------------------------------------------------------*/
union st_psd1{               /* Function Select Register D1 */
    struct{
       unsigned char  PSD1_0  :1;    /* Port P70 output peripheral function select bit */
       unsigned char  PSD1_1  :1;    /* Port P71 output peripheral function select bit */
       unsigned char          :1;    /* Nothing  assigned                              */
       unsigned char          :1;    /* Nothing  assigned                              */
       unsigned char          :1;    /* Nothing  assigned                              */
       unsigned char          :1;    /* Nothing  assigned                              */
       unsigned char  PSD1_6  :1;    /* Port P76 output peripheral function select bit */
       unsigned char          :1;    /* Nothing  assigned                              */
    }BIT;                    /* Bit access                                     */
    char    BYTE;            /* Byte access                                    */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*------------------------------------------------------
    Function Select Register C2//0x03AC
------------------------------------------------------*/
union st_psc2{             /* Function Select Register C2 */
    struct{
       unsigned char        :1;    /* Nothing  assigned                              */
       unsigned char PSC2_1 :1;    /* Port P81 output peripheral function select bit */
       unsigned char PSC2_2 :1;    /* Port P82 output peripheral function select bit */
       unsigned char        :1;    /* Nothing  assigned                              */
       unsigned char        :1;    /* Nothing  assigned                              */
       unsigned char        :1;    /* Nothing  assigned                              */
       unsigned char        :1;    /* Nothing  assigned                              */
       unsigned char        :1;    /* Nothing  assigned                              */
    }BIT;                  /* Bit access                                     */
    char    BYTE;          /* Byte access                                    */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*------------------------------------------------------
    Function Select Register C3//0x03AD
------------------------------------------------------*/
union st_psc3{             /* Function Select Register C3 */
    struct{
       unsigned char        :1;    /* Nothing  assigned                              */
       unsigned char        :1;    /* Nothing  assigned                              */
       unsigned char        :1;    /* Nothing  assigned                              */
       unsigned char        :1;    /* Nothing  assigned                              */
       unsigned char        :1;    /* Nothing  assigned                              */
       unsigned char        :1;    /* Nothing  assigned                              */
       unsigned char  PSC3_6:1;    /* Port P96 output peripheral function select bit */
       unsigned char        :1;    /* Nothing  assigned                              */
    }BIT;                  /* Bit access                                     */
    char    BYTE;          /* Byte access                                    */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*------------------------------------------------------
    Function Select Register C//0x03AF
------------------------------------------------------*/
union st_psc{             /* Function Select Register C */
    struct{
       unsigned char PSC_0 :1;    /* Port P70 output peripheral function select bit  */
       unsigned char PSC_1 :1;    /* Port P71 output peripheral function select bit  */
       unsigned char PSC_2 :1;    /* Port P72 output peripheral function select bit  */
       unsigned char PSC_3 :1;    /* Port P73 output peripheral function select bit  */
       unsigned char PSC_4 :1;    /* Port P74 output peripheral function select bit  */
       unsigned char       :1;    /* Nothing  assigned                               */
       unsigned char PSC_6 :1;    /* Port P76 output peripheral function select bit  */
       unsigned char PSC_7 :1;    /* Port Key input interrupt disable bit            */
    }BIT;                 /* Bit access                                      */
    char    BYTE;         /* Byte access                                     */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*------------------------------------------------------
    Function select register A0//0x03B0
------------------------------------------------------*/
union st_ps0{             /* Function select register A0 */
    struct{
       unsigned char PS0_0 :1;    /* Port P60 output function select bit */
       unsigned char PS0_1 :1;    /* Port P61 output function select bit */
       unsigned char PS0_2 :1;    /* Port P62 output function select bit */
       unsigned char PS0_3 :1;    /* Port P63 output function select bit */
       unsigned char PS0_4 :1;    /* Port P64 output function select bit */
       unsigned char PS0_5 :1;    /* Port P65 output function select bit */
       unsigned char PS0_6 :1;    /* Port P66 output function select bit */
       unsigned char PS0_7 :1;    /* Port P67 output function select bit */
    }BIT;                 /* Bit access                                      */
    char    BYTE;         /* Byte access                                     */
};

/*------------------------------------------------------
    Function select register A1//0x03B1
------------------------------------------------------*/
union st_ps1{             /* Function select register A1 */
    struct{
       unsigned char PS1_0 :1;    /* Port P70 output function select bit */
       unsigned char PS1_1 :1;    /* Port P71 output function select bit */
       unsigned char PS1_2 :1;    /* Port P72 output function select bit */
       unsigned char PS1_3 :1;    /* Port P73 output function select bit */
       unsigned char PS1_4 :1;    /* Port P74 output function select bit */
       unsigned char PS1_5 :1;    /* Port P75 output function select bit */
       unsigned char PS1_6 :1;    /* Port P76 output function select bit */
       unsigned char PS1_7 :1;    /* Port P77 output function select bit */
    }BIT;                 /* Bit access                                      */
    char    BYTE;         /* Byte access                                     */
};

/*------------------------------------------------------
    Function select register B0//0x03B2
------------------------------------------------------*/
union st_psl0{             /* Function select register B0 */
    struct{
       unsigned char        :1;    /* Reserved bit must be set to 0                  */
       unsigned char        :1;    /* Reserved bit must be set to 0                  */
       unsigned char PSL0_2 :1;    /* Port P62 output peripheral function select bit */
       unsigned char        :1;    /* Reserved bit must be set to 0                  */
       unsigned char PSL0_4 :1;    /* Port P62 output peripheral function select bit */
       unsigned char        :1;    /* Reserved bit must be set to 0                  */
       unsigned char PSL0_6 :1;    /* Port P62 output peripheral function select bit */
       unsigned char        :1;    /* Reserved bit must be set to 0                  */
    }BIT;                  /* Bit access                                     */
    char    BYTE;          /* Byte access                                    */
};

/*------------------------------------------------------
    Function select register B1//0x03B3
------------------------------------------------------*/
union st_psl1{             /* Function select register B1 */
    struct{
       unsigned char PSL1_0 :1;    /* Port P70 output peripheral function select bit */
       unsigned char PSL1_1 :1;    /* Port P71 output peripheral function select bit */
       unsigned char PSL1_2 :1;    /* Port P72 output peripheral function select bit */
       unsigned char PSL1_3 :1;    /* Port P73 output peripheral function select bit */
       unsigned char PSL1_4 :1;    /* Port P74 output peripheral function select bit */
       unsigned char PSL1_5 :1;    /* Port P75 output peripheral function select bit */
       unsigned char PSL1_6 :1;    /* Port P76 output peripheral function select bit */
       unsigned char PSL1_7 :1;    /* Port P77 output peripheral function select bit */
    }BIT;                  /* Bit access                                     */
    char    BYTE;          /* Byte access                                    */
};

/*------------------------------------------------------
    Function select register A2//0x03B4
------------------------------------------------------*/
union st_ps2{              /* Function select register A2 */
    struct{
       unsigned char  PS2_0 :1;    /* Port P80 output function select bit */
       unsigned char  PS2_1 :1;    /* Port P81 output function select bit */
       unsigned char  PS2_2 :1;    /* Port P82 output function select bit */
       unsigned char        :1;    /* Reserved bit must be set to 0       */
       unsigned char        :1;    /* Reserved bit must be set to 0       */
       unsigned char        :1;    /* Reserved bit must be set to 0       */
       unsigned char        :1;    /* Reserved bit must be set to 0       */
       unsigned char        :1;    /* Reserved bit must be set to 0       */
    }BIT;                  /* Bit access                                     */
    char    BYTE;          /* Byte access                                    */
};

/*------------------------------------------------------
    Function select register A3//0x03B5
------------------------------------------------------*/
union st_ps3{              /* Function select register A3 */
    struct{
       unsigned char  PS3_0 :1;    /* Port P90 output function select bit */
       unsigned char  PS3_1 :1;    /* Port P91 output function select bit */
       unsigned char  PS3_2 :1;    /* Port P92 output function select bit */
       unsigned char  PS3_3 :1;    /* Port P93 output function select bit */
       unsigned char  PS3_4 :1;    /* Port P94 output function select bit */
       unsigned char  PS3_5 :1;    /* Port P95 output function select bit */
       unsigned char  PS3_6 :1;    /* Port P96 output function select bit */
       unsigned char  PS3_7 :1;    /* Port P97 output function select bit */
    }BIT;                  /* Bit access                                     */
    char    BYTE;          /* Byte access                                    */
};

/*------------------------------------------------------
    Function select register B2//0x03B6//
------------------------------------------------------*/
union st_psl2{             /* Function select register B2 */
    struct{
       unsigned char  PSL2_0 :1;    /* Port P80 output function select bit */
       unsigned char  PSL2_1 :1;    /* Port P81 output function select bit */
       unsigned char  PSL2_2 :1;    /* Port P82 output function select bit */
       unsigned char         :1;    /* Reserved bit must be set to 0       */
       unsigned char         :1;    /* Reserved bit must be set to 0       */
       unsigned char         :1;    /* Nothing assigned                    */
       unsigned char         :1;    /* Reserved bit must be set to 0       */
       unsigned char         :1;    /* Reserved bit must be set to 0       */
    }BIT;                   /* Bit access                                     */
    char    BYTE;           /* Byte access                                    */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*------------------------------------------------------
    Function select register B3//0x03B7
------------------------------------------------------*/
union st_psl3{            /* Function select register B3 */
    struct{
       unsigned char         :1;  /* Reserved bit must be set to 0                  */
       unsigned char  PSL3_1 :1;  /* Port P91 output peripheral function select bit */
       unsigned char  PSL3_2 :1;  /* Port P92 output peripheral function select bit */
       unsigned char  PSL3_3 :1;  /* Port P93 output peripheral function select bit */
       unsigned char  PSL3_4 :1;  /* Port P94 output peripheral function select bit */
       unsigned char  PSL3_5 :1;  /* Port P95 output peripheral function select bit */
       unsigned char  PSL3_6 :1;  /* Port P96 output peripheral function select bit */
       unsigned char  PSL3_7 :1;  /* Port P97 output peripheral function select bit */
    }BIT;                 /* Bit access                                     */
    char    BYTE;         /* Byte access                                    */
};

/*------------------------------------------------------
    Function select register A5//0x03B9
------------------------------------------------------*/
union st_ps5{             /* Function select register A5 */
    struct{
       unsigned char  PS5_0 :1;    /* Port P110 output function select bit */
       unsigned char  PS5_1 :1;    /* Port P111 output function select bit */
       unsigned char  PS5_2 :1;    /* Port P112 output function select bit */
       unsigned char  PS5_3 :1;    /* Port P113 output function select bit */
       unsigned char        :1;    /* Reserved bit must be set to 0        */
       unsigned char        :1;    /* Nothing assigned                     */
       unsigned char        :1;    /* Nothing assigned                     */
       unsigned char        :1;    /* Nothing assigned                     */
    }BIT;                  /* Bit access                           */
    char    BYTE;          /* Byte access                          */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*------------------------------------------------------
    Port P6//0x03C0
------------------------------------------------------*/
union st_p6  {            /* Port P6 */
    struct{
       unsigned char  P6_0 :1;    /* Port P6  bit0 */
       unsigned char  P6_1 :1;    /* Port P6  bit1 */
       unsigned char  P6_2 :1;    /* Port P6  bit2 */
       unsigned char  P6_3 :1;    /* Port P6  bit3 */
       unsigned char  P6_4 :1;    /* Port P6  bit4 */
       unsigned char  P6_5 :1;    /* Port P6  bit5 */
       unsigned char  P6_6 :1;    /* Port P6  bit6 */
       unsigned char  P6_7 :1;    /* Port P6  bit7 */
    }BIT;                 /* Bit access    */
    char    BYTE;         /* Byte access   */
};

/*------------------------------------------------------
    Port P7//0x03C1
------------------------------------------------------*/
union st_p7  {            /* Port P7 */
    struct{
       unsigned char  P7_0 :1;    /* Port P7  bit0 */
       unsigned char  P7_1 :1;    /* Port P7  bit1 */
       unsigned char  P7_2 :1;    /* Port P7  bit2 */
       unsigned char  P7_3 :1;    /* Port P7  bit3 */
       unsigned char  P7_4 :1;    /* Port P7  bit4 */
       unsigned char  P7_5 :1;    /* Port P7  bit5 */
       unsigned char  P7_6 :1;    /* Port P7  bit6 */
       unsigned char  P7_7 :1;    /* Port P7  bit7 */
    }BIT;                 /* Bit access    */
    char    BYTE;         /* Byte access   */
};

/*------------------------------------------------------
    Port P6 direction register//0x03C2
------------------------------------------------------*/
union st_pd6  {            /* Port P6 direction register  */
    struct{
       unsigned char  PD6_0 :1;    /*  P6 direction register bit0 */
       unsigned char  PD6_1 :1;    /*  P6 direction register bit1 */
       unsigned char  PD6_2 :1;    /*  P6 direction register bit2 */
       unsigned char  PD6_3 :1;    /*  P6 direction register bit3 */
       unsigned char  PD6_4 :1;    /*  P6 direction register bit4 */
       unsigned char  PD6_5 :1;    /*  P6 direction register bit5 */
       unsigned char  PD6_6 :1;    /*  P6 direction register bit6 */
       unsigned char  PD6_7 :1;    /*  P6 direction register bit7 */
    }BIT;                  /* Bit access                  */
    char    BYTE;          /* Byte access                 */
};

/*------------------------------------------------------
    Port P7 direction register//0x03C3
------------------------------------------------------*/
union st_pd7  {            /* Port P7 direction register  */
    struct{
       unsigned char  PD7_0 :1;    /* P7 direction register bit0 */
       unsigned char  PD7_1 :1;    /* P7 direction register bit1 */
       unsigned char  PD7_2 :1;    /* P7 direction register bit2 */
       unsigned char  PD7_3 :1;    /* P7 direction register bit3 */
       unsigned char  PD7_4 :1;    /* P7 direction register bit4 */
       unsigned char  PD7_5 :1;    /* P7 direction register bit5 */
       unsigned char  PD7_6 :1;    /* P7 direction register bit6 */
       unsigned char  PD7_7 :1;    /* P7 direction register bit7 */
    }BIT;                  /* Bit access                 */
    char    BYTE;          /* Byte access                */
};

/*------------------------------------------------------
    Port P8//0x03C4
------------------------------------------------------*/
union st_p8  {            /* Port P8 */
    struct{
       unsigned char  P8_0 :1;    /* Port P8  bit0 */
       unsigned char  P8_1 :1;    /* Port P8  bit1 */
       unsigned char  P8_2 :1;    /* Port P8  bit2 */
       unsigned char  P8_3 :1;    /* Port P8  bit3 */
       unsigned char  P8_4 :1;    /* Port P8  bit4 */
       unsigned char  P8_5 :1;    /* Port P8  bit5 */
       unsigned char  P8_6 :1;    /* Port P8  bit6 */
       unsigned char  P8_7 :1;    /* Port P8  bit7 */
    }BIT;                 /* Bit access    */
    char    BYTE;         /* Byte access   */
};

/*------------------------------------------------------
    Port P9//0x03C5
------------------------------------------------------*/
union st_p9  {            /* Port P9 */
    struct{
       unsigned char  P9_0 :1;    /* Port P9  bit0 */
       unsigned char  P9_1 :1;    /* Port P9  bit1 */
       unsigned char  P9_2 :1;    /* Port P9  bit2 */
       unsigned char  P9_3 :1;    /* Port P9  bit3 */
       unsigned char  P9_4 :1;    /* Port P9  bit4 */
       unsigned char  P9_5 :1;    /* Port P9  bit5 */
       unsigned char  P9_6 :1;    /* Port P9  bit6 */
       unsigned char  P9_7 :1;    /* Port P9  bit7 */
    }BIT;                 /* Bit access    */
    char    BYTE;         /* Byte access   */
};

/*------------------------------------------------------
    Port P8 direction register//0x03C6
------------------------------------------------------*/
union st_pd8  {            /* Port P8 direction register  */
    struct{
       unsigned char  PD8_0 :1;    /* P8 direction register bit0 */
       unsigned char  PD8_1 :1;    /* P8 direction register bit1 */
       unsigned char  PD8_2 :1;    /* P8 direction register bit2 */
       unsigned char  PD8_3 :1;    /* P8 direction register bit3 */
       unsigned char  PD8_4 :1;    /* P8 direction register bit4 */
       unsigned char        :1;    /* Nothing assigned           */
       unsigned char  PD8_6 :1;    /* P8 direction register bit6 */
       unsigned char  PD8_7 :1;    /* P8 direction register bit7 */
    }BIT;                  /* Bit access                 */
    char    BYTE;          /* Byte access                */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*------------------------------------------------------
    Port P9 direction register//0x03C7
------------------------------------------------------*/
union st_pd9  {            /* Port P9 direction register  */
    struct{
       unsigned char  PD9_0 :1;    /* P9 direction register bit0 */
       unsigned char  PD9_1 :1;    /* P9 direction register bit1 */
       unsigned char  PD9_2 :1;    /* P9 direction register bit2 */
       unsigned char  PD9_3 :1;    /* P9 direction register bit3 */
       unsigned char  PD9_4 :1;    /* P9 direction register bit4 */
       unsigned char  PD9_5 :1;    /* P9 direction register bit5 */
       unsigned char  PD9_6 :1;    /* P9 direction register bit6 */
       unsigned char  PD9_7 :1;    /* P9 direction register bit7 */
    }BIT;                  /* Bit access                 */
    char    BYTE;          /* Byte access                */
};

/*------------------------------------------------------
    Port P10//0x03C8
------------------------------------------------------*/
union st_p10  {           /* Port P10 */
    struct{
       unsigned char  P10_0 :1;    /* Port P10  bit0 */
       unsigned char  P10_1 :1;    /* Port P10 bit1 */
       unsigned char  P10_2 :1;    /* Port P10 bit2 */
       unsigned char  P10_3 :1;    /* Port P10 bit3 */
       unsigned char  P10_4 :1;    /* Port P10 bit4 */
       unsigned char  P10_5 :1;    /* Port P10 bit5 */
       unsigned char  P10_6 :1;    /* Port P10 bit6 */
       unsigned char  P10_7 :1;    /* Port P10 bit7 */
    }BIT;                  /* Bit access    */
    char    BYTE;          /* Byte access   */
};

/*------------------------------------------------------
    Port P11//0x03C9
------------------------------------------------------*/
union st_p11  {            /* Port P11 */
    struct{
       unsigned char  P11_0 :1;    /* Port P11 bit0    */
       unsigned char  P11_1 :1;    /* Port P11 bit1    */
       unsigned char  P11_2 :1;    /* Port P11 bit2    */
       unsigned char  P11_3 :1;    /* Port P11 bit3    */
       unsigned char  P11_4 :1;    /* Port P11 bit4    */
       unsigned char        :1;    /* Nothing assigned */
       unsigned char        :1;    /* Nothing assigned */
       unsigned char        :1;    /* Nothing assigned */
    }BIT;                  /* Bit access       */
    char    BYTE;          /* Byte access      */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*------------------------------------------------------
    Port P10 direction register//0x03CA
------------------------------------------------------*/
union st_pd10{            /* Port P10 direction register  */
    struct{
       unsigned char  PD10_0 :1;  /* P10 direction register bit0 */
       unsigned char  PD10_1 :1;  /* P10 direction register bit1 */
       unsigned char  PD10_2 :1;  /* P10 direction register bit2 */
       unsigned char  PD10_3 :1;  /* P10 direction register bit3 */
       unsigned char  PD10_4 :1;  /* P10 direction register bit4 */
       unsigned char  PD10_5 :1;  /* P10 direction register bit5 */
       unsigned char  PD10_6 :1;  /* P10 direction register bit6 */
       unsigned char  PD10_7 :1;  /* P10 direction register bit7 */
    }BIT;                 /* Bit access                 */
    char    BYTE;         /* Byte access                */
};

/*------------------------------------------------------
    Port P11 direction register//0x03CB
------------------------------------------------------*/
union st_pd11{            /* Port P11 direction register  */
    struct{
       unsigned char  PD11_0 :1;  /* P11 direction register bit0 */
       unsigned char  PD11_1 :1;  /* P11 direction register bit1 */
       unsigned char  PD11_2 :1;  /* P11 direction register bit2 */
       unsigned char  PD11_3 :1;  /* P11 direction register bit3 */
       unsigned char  PD11_4 :1;  /* P11 direction register bit4 */
       unsigned char         :1;  /* Nothing assigned            */
       unsigned char         :1;  /* Nothing assigned            */
       unsigned char         :1;  /* Nothing assigned            */
    }BIT;                 /* Bit access                  */
    char    BYTE;         /* Byte access                 */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*------------------------------------------------------
    Port P12//0x03C
------------------------------------------------------*/
union st_p12  {            /* Port P12 */
    struct{
       unsigned char  P12_0 :1;    /* Port P12  bit0 */
       unsigned char  P12_1 :1;    /* Port P12 bit1 */
       unsigned char  P12_2 :1;    /* Port P12 bit2 */
       unsigned char  P12_3 :1;    /* Port P12 bit3 */
       unsigned char  P12_4 :1;    /* Port P12 bit4 */
       unsigned char  P12_5 :1;    /* Port P12 bit5 */
       unsigned char  P12_6 :1;    /* Port P12 bit6 */
       unsigned char  P12_7 :1;    /* Port P12 bit7 */
    }BIT;                  /* Bit access    */
    char    BYTE;          /* Byte access   */
};

/*------------------------------------------------------
    Port P13//0x03CD
------------------------------------------------------*/
union st_p13  {           /* Port P13 */
    struct{
       unsigned char  P13_0 :1;    /* Port P13  bit0 */
       unsigned char  P13_1 :1;    /* Port P13 bit1 */
       unsigned char  P13_2 :1;    /* Port P13 bit2 */
       unsigned char  P13_3 :1;    /* Port P13 bit3 */
       unsigned char  P13_4 :1;    /* Port P13 bit4 */
       unsigned char  P13_5 :1;    /* Port P13 bit5 */
       unsigned char  P13_6 :1;    /* Port P13 bit6 */
       unsigned char  P13_7 :1;    /* Port P13 bit7 */
    }BIT;                  /* Bit access    */
    char    BYTE;          /* Byte access   */
};

/*------------------------------------------------------
    Port P12 direction register//0x03CE
------------------------------------------------------*/
union st_pd12{            /* Port P12 direction register  */
    struct{
       unsigned char  PD12_0 :1;  /* P12 direction register bit0 */
       unsigned char  PD12_1 :1;  /* P12 direction register bit1 */
       unsigned char  PD12_2 :1;  /* P12 direction register bit2 */
       unsigned char  PD12_3 :1;  /* P12 direction register bit3 */
       unsigned char  PD12_4 :1;  /* P12 direction register bit4 */
       unsigned char  PD12_5 :1;  /* P12 direction register bit5 */
       unsigned char  PD12_6 :1;  /* P12 direction register bit6 */
       unsigned char  PD12_7 :1;  /* P12 direction register bit7 */
    }BIT;                 /* Bit access                  */
    char    BYTE;         /* Byte access                 */
};

/*------------------------------------------------------
    Port P13 direction register//0x03CF
------------------------------------------------------*/
union st_pd13{            /* Port P13 direction register  */
    struct{
       unsigned char  PD13_0 :1;  /* P13 direction register bit0 */
       unsigned char  PD13_1 :1;  /* P13 direction register bit1 */
       unsigned char  PD13_2 :1;  /* P13 direction register bit2 */
       unsigned char  PD13_3 :1;  /* P13 direction register bit3 */
       unsigned char  PD13_4 :1;  /* P13 direction register bit4 */
       unsigned char  PD13_5 :1;  /* P13 direction register bit5 */
       unsigned char  PD13_6 :1;  /* P13 direction register bit6 */
       unsigned char  PD13_7 :1;  /* P13 direction register bit7 */
    }BIT;                 /* Bit access                  */
    char    BYTE;         /* Byte access                 */
};

/*------------------------------------------------------
    Port P14//0x03D0
------------------------------------------------------*/
union st_p14  {            /* Port P14 */
    struct{
       unsigned char  P14_0 :1;    /* Port P14 bit0    */
       unsigned char  P14_1 :1;    /* Port P14 bit1    */
       unsigned char  P14_2 :1;    /* Port P14 bit2    */
       unsigned char  P14_3 :1;    /* Port P14 bit3    */
       unsigned char  P14_4 :1;    /* Port P14 bit4    */
       unsigned char  P14_5 :1;    /* Port P14 bit5    */
       unsigned char  P14_6 :1;    /* Port P14 bit6    */
       unsigned char        :1;    /* Nothing assigned */
    }BIT;                  /* Bit access       */
    char    BYTE;          /* Byte access      */
};

/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*------------------------------------------------------
    Port P15//0x03D1
------------------------------------------------------*/
union st_p15  {            /* Port P15 */
    struct{
       unsigned char  P15_0 :1;    /* Port P15 bit0 */
       unsigned char  P15_1 :1;    /* Port P15 bit1 */
       unsigned char  P15_2 :1;    /* Port P15 bit2 */
       unsigned char  P15_3 :1;    /* Port P15 bit3 */
       unsigned char  P15_4 :1;    /* Port P15 bit4 */
       unsigned char  P15_5 :1;    /* Port P15 bit5 */
       unsigned char  P15_6 :1;    /* Port P15 bit6 */
       unsigned char  P15_7 :1;    /* Port P15 bit7 */
    }BIT;                  /* Bit access    */
    char    BYTE;          /* Byte access   */
};

/*------------------------------------------------------
    Port P14 direction register//0x03D2
------------------------------------------------------*/
union st_pd14{            /* Port P14 direction register  */
    struct{
       unsigned char  PD14_0 :1;  /* P14 direction register bit0 */
       unsigned char  PD14_1 :1;  /* P14 direction register bit1 */
       unsigned char  PD14_2 :1;  /* P14 direction register bit2 */
       unsigned char  PD14_3 :1;  /* P14 direction register bit3 */
       unsigned char  PD14_4 :1;  /* P14 direction register bit4 */
       unsigned char  PD14_5 :1;  /* P14 direction register bit5 */
       unsigned char  PD14_6 :1;  /* P14 direction register bit6 */
       unsigned char         :1;  /* Nothing assigned            */
    }BIT;                 /* Bit access                  */
    char    BYTE;         /* Byte access                 */
};
/* When nothing assigned then, when write, set to "0".When read, its content is indeterminate. */

/*------------------------------------------------------
    Port P15 direction register//0x03D3
------------------------------------------------------*/
union st_pd15{            /* Port P15 direction register  */
    struct{
       unsigned char  PD15_0 :1;  /* P15 direction register bit0 */
       unsigned char  PD15_1 :1;  /* P15 direction register bit1 */
       unsigned char  PD15_2 :1;  /* P15 direction register bit2 */
       unsigned char  PD15_3 :1;  /* P15 direction register bit3 */
       unsigned char  PD15_4 :1;  /* P15 direction register bit4 */
       unsigned char  PD15_5 :1;  /* P15 direction register bit5 */
       unsigned char  PD15_6 :1;  /* P15 direction register bit6 */
       unsigned char  PD15_7 :1;  /* P15 direction register bit7 */
    }BIT;                 /* Bit access                  */
    char    BYTE;         /* Byte access                 */
};

/*------------------------------------------------------
    Pull-up control register 2//0x03DA
------------------------------------------------------*/
union st_pur2{            /* Pull-up control register 2  */
    struct{
       unsigned char  PU2_0 :1;  /* Pull-up P60 to P63 */
       unsigned char  PU2_1 :1;  /* Pull-up P64 to P67 */
       unsigned char  PU2_2 :1;  /* Pull-up P70 to P73 ; Except P70,P71 */
       unsigned char  PU2_3 :1;  /* Pull-up P74 to P77 */
       unsigned char  PU2_4 :1;  /* Pull-up P80 to P83 */
       unsigned char  PU2_5 :1;  /* Pull-up P84 to P87 ; Except P85 */
       unsigned char  PU2_6 :1;  /* Pull-up P90 to P93 */
       unsigned char  PU2_7 :1;  /* Pull-up P94 to P97 */
    }BIT;                 /* Bit access                  */
    char    BYTE;         /* Byte access                 */
};

/*------------------------------------------------------
    Pull-up control register 3//0x03DB
------------------------------------------------------*/
union st_pur3{            /* Pull-up control register 3  */
    struct{
       unsigned char  PU3_0 :1;  /* Pull-up P100 to P103 */
       unsigned char  PU3_1 :1;  /* Pull-up P104 to P107 */
       unsigned char  PU3_2 :1;  /* Pull-up P110 to P113 */
       unsigned char  PU3_3 :1;  /* Pull-up P114 */
       unsigned char  PU3_4 :1;  /* Pull-up P120 to P123 */
       unsigned char  PU3_5 :1;  /* Pull-up P124 to P127 */
       unsigned char  PU3_6 :1;  /* Pull-up P130 to P133 */
       unsigned char  PU3_7 :1;  /* Pull-up P134 to P137 */
    }BIT;                 /* Bit access                  */
    char    BYTE;         /* Byte access                 */
};

/*------------------------------------------------------
    Pull-up control register 4//0x03DC
------------------------------------------------------*/
union st_pur4{            /* Pull-up control register 4  */
    struct{
       unsigned char  PU4_0 :1;  /* Pull-up P140 to P143 */
       unsigned char  PU4_1 :1;  /* Pull-up P144 to P146 */
       unsigned char  PU4_2 :1;  /* Pull-up P150 to P153 */
       unsigned char  PU4_3 :1;  /* Pull-up P154 to P157 */
       unsigned char        :1;  /* Nothing is assigned */
       unsigned char        :1;  /* Nothing is assigned */
       unsigned char        :1;  /* Nothing is assigned */
       unsigned char        :1;  /* Nothing is assigned */
    }BIT;                 /* Bit access                  */
    char    BYTE;         /* Byte access                 */
};

/*------------------------------------------------------
    Port P0//0x03E0
------------------------------------------------------*/
union st_p0 {             /* Port P0 */
    struct{
       unsigned char  P0_0 :1;    /* Port P0 bit0 */
       unsigned char  P0_1 :1;    /* Port P0 bit1 */
       unsigned char  P0_2 :1;    /* Port P0 bit2 */
       unsigned char  P0_3 :1;    /* Port P0 bit3 */
       unsigned char  P0_4 :1;    /* Port P0 bit4 */
       unsigned char  P0_5 :1;    /* Port P0 bit5 */
       unsigned char  P0_6 :1;    /* Port P0 bit6 */
       unsigned char  P0_7 :1;    /* Port P0 bit7 */
    }BIT;                 /* Bit access   */
    char    BYTE;         /* Byte access  */
};

/*------------------------------------------------------
    Port P1//0x03E1
------------------------------------------------------*/
union st_p1 {             /* Port P1 */
    struct{
       unsigned char  P1_0 :1;    /* Port P1 bit0 */
       unsigned char  P1_1 :1;    /* Port P1 bit1 */
       unsigned char  P1_2 :1;    /* Port P1 bit2 */
       unsigned char  P1_3 :1;    /* Port P1 bit3 */
       unsigned char  P1_4 :1;    /* Port P1 bit4 */
       unsigned char  P1_5 :1;    /* Port P1 bit5 */
       unsigned char  P1_6 :1;    /* Port P1 bit6 */
       unsigned char  P1_7 :1;    /* Port P1 bit7 */
    }BIT;                 /* Bit access   */
    char    BYTE;         /* Byte access  */
};

/*------------------------------------------------------
    Port P0 direction register//0x03E2
------------------------------------------------------*/
union st_pd0 {            /* Port P0 direction register  */
    struct{
       unsigned char  PD0_0 :1;  /* P0 direction register bit0 */
       unsigned char  PD0_1 :1;  /* P0 direction register bit1 */
       unsigned char  PD0_2 :1;  /* P0 direction register bit2 */
       unsigned char  PD0_3 :1;  /* P0 direction register bit3 */
       unsigned char  PD0_4 :1;  /* P0 direction register bit4 */
       unsigned char  PD0_5 :1;  /* P0 direction register bit5 */
       unsigned char  PD0_6 :1;  /* P0 direction register bit6 */
       unsigned char  PD0_7 :1;  /* P0 direction register bit7 */
    }BIT;                /* Bit access                 */
    char    BYTE;        /* Byte access                */
};

/*------------------------------------------------------
    Port P1 direction register//0x03E3
------------------------------------------------------*/
union st_pd1 {           /* Port P1 direction register  */
    struct{
       unsigned char  PD1_0 :1;  /* P1 direction register bit0 */
       unsigned char  PD1_1 :1;  /* P1 direction register bit1 */
       unsigned char  PD1_2 :1;  /* P1 direction register bit2 */
       unsigned char  PD1_3 :1;  /* P1 direction register bit3 */
       unsigned char  PD1_4 :1;  /* P1 direction register bit4 */
       unsigned char  PD1_5 :1;  /* P1 direction register bit5 */
       unsigned char  PD1_6 :1;  /* P1 direction register bit6 */
       unsigned char  PD1_7 :1;  /* P1 direction register bit7 */
    }BIT;                /* Bit access                 */
    char    BYTE;        /* Byte access                */
};

/*------------------------------------------------------
    Port P2//0x03E4
------------------------------------------------------*/
union st_p2 {             /* Port P2 */
    struct{
       unsigned char  P2_0 :1;    /* Port P2 bit0 */
       unsigned char  P2_1 :1;    /* Port P2 bit1 */
       unsigned char  P2_2 :1;    /* Port P2 bit2 */
       unsigned char  P2_3 :1;    /* Port P2 bit3 */
       unsigned char  P2_4 :1;    /* Port P2 bit4 */
       unsigned char  P2_5 :1;    /* Port P2 bit5 */
       unsigned char  P2_6 :1;    /* Port P2 bit6 */
       unsigned char  P2_7 :1;    /* Port P2 bit7 */
    }BIT;                 /* Bit access   */
    char    BYTE;         /* Byte access  */
};

/*------------------------------------------------------
    Port P3//0x03E5
------------------------------------------------------*/
union st_p3 {             /* Port P3 */
    struct{
       unsigned char  P3_0 :1;    /* Port P3 bit0 */
       unsigned char  P3_1 :1;    /* Port P3 bit1 */
       unsigned char  P3_2 :1;    /* Port P3 bit2 */
       unsigned char  P3_3 :1;    /* Port P3 bit3 */
       unsigned char  P3_4 :1;    /* Port P3 bit4 */
       unsigned char  P3_5 :1;    /* Port P3 bit5 */
       unsigned char  P3_6 :1;    /* Port P3 bit6 */
       unsigned char  P3_7 :1;    /* Port P3 bit7 */
    }BIT;                 /* Bit access   */
    char    BYTE;         /* Byte access  */
};

/*------------------------------------------------------
    Port P2 direction register//0x03E6
------------------------------------------------------*/
union st_pd2 {           /* Port P2 direction register  */
    struct{
       unsigned char  PD2_0 :1;  /* P2 direction register bit0 */
       unsigned char  PD2_1 :1;  /* P2 direction register bit1 */
       unsigned char  PD2_2 :1;  /* P2 direction register bit2 */
       unsigned char  PD2_3 :1;  /* P2 direction register bit3 */
       unsigned char  PD2_4 :1;  /* P2 direction register bit4 */
       unsigned char  PD2_5 :1;  /* P2 direction register bit5 */
       unsigned char  PD2_6 :1;  /* P2 direction register bit6 */
       unsigned char  PD2_7 :1;  /* P2 direction register bit7 */
    }BIT;                /* Bit access                 */
    char    BYTE;        /* Byte access                */
};

/*------------------------------------------------------
    Port P3 direction register//0x03E7
------------------------------------------------------*/
union st_pd3 {           /* Port P3 direction register  */
    struct{
       unsigned char  PD3_0 :1;  /* P3 direction register bit0 */
       unsigned char  PD3_1 :1;  /* P3 direction register bit1 */
       unsigned char  PD3_2 :1;  /* P3 direction register bit2 */
       unsigned char  PD3_3 :1;  /* P3 direction register bit3 */
       unsigned char  PD3_4 :1;  /* P3 direction register bit4 */
       unsigned char  PD3_5 :1;  /* P3 direction register bit5 */
       unsigned char  PD3_6 :1;  /* P3 direction register bit6 */
       unsigned char  PD3_7 :1;  /* P3 direction register bit7 */
    }BIT;                /* Bit access                 */
    char    BYTE;        /* Byte access                */
};

/*------------------------------------------------------
    Port P4//0x03E8
------------------------------------------------------*/
union st_p4 {             /* Port P4 */
    struct{
       unsigned char  P4_0 :1;    /* Port P4 bit0 */
       unsigned char  P4_1 :1;    /* Port P4 bit1 */
       unsigned char  P4_2 :1;    /* Port P4 bit2 */
       unsigned char  P4_3 :1;    /* Port P4 bit3 */
       unsigned char  P4_4 :1;    /* Port P4 bit4 */
       unsigned char  P4_5 :1;    /* Port P4 bit5 */
       unsigned char  P4_6 :1;    /* Port P4 bit6 */
       unsigned char  P4_7 :1;    /* Port P4 bit7 */
    }BIT;                 /* Bit access   */
    char    BYTE;         /* Byte access  */
};

/*------------------------------------------------------
    Port P5//0x03E9
------------------------------------------------------*/
union st_p5 {             /* Port P5 */
    struct{
       unsigned char  P5_0 :1;    /* Port P5 bit0 */
       unsigned char  P5_1 :1;    /* Port P5 bit1 */
       unsigned char  P5_2 :1;    /* Port P5 bit2 */
       unsigned char  P5_3 :1;    /* Port P5 bit3 */
       unsigned char  P5_4 :1;    /* Port P5 bit4 */
       unsigned char  P5_5 :1;    /* Port P5 bit5 */
       unsigned char  P5_6 :1;    /* Port P5 bit6 */
       unsigned char  P5_7 :1;    /* Port P5 bit7 */
    }BIT;                 /* Bit access   */
    char    BYTE;         /* Byte access  */
};

/*------------------------------------------------------
    Port P4 direction register//0x03EA
------------------------------------------------------*/
union st_pd4 {           /* Port P4 direction register  */
    struct{
       unsigned char  PD4_0 :1;  /* P4 direction register bit0 */
       unsigned char  PD4_1 :1;  /* P4 direction register bit1 */
       unsigned char  PD4_2 :1;  /* P4 direction register bit2 */
       unsigned char  PD4_3 :1;  /* P4 direction register bit3 */
       unsigned char  PD4_4 :1;  /* P4 direction register bit4 */
       unsigned char  PD4_5 :1;  /* P4 direction register bit5 */
       unsigned char  PD4_6 :1;  /* P4 direction register bit6 */
       unsigned char  PD4_7 :1;  /* P4 direction register bit7 */
    }BIT;                /* Bit access                 */
    char    BYTE;        /* Byte access                */
};

/*------------------------------------------------------
    Port P5 direction register//0x03EB
------------------------------------------------------*/
union st_pd5 {           /* Port P5 direction register  */
    struct{
       unsigned char  PD5_0 :1;  /* P5 direction register bit0 */
       unsigned char  PD5_1 :1;  /* P5 direction register bit1 */
       unsigned char  PD5_2 :1;  /* P5 direction register bit2 */
       unsigned char  PD5_3 :1;  /* P5 direction register bit3 */
       unsigned char  PD5_4 :1;  /* P5 direction register bit4 */
       unsigned char  PD5_5 :1;  /* P5 direction register bit5 */
       unsigned char  PD5_6 :1;  /* P5 direction register bit6 */
       unsigned char  PD5_7 :1;  /* P5 direction register bit7 */
    }BIT;                /* Bit access                 */
    char    BYTE;        /* Byte access                */
};

/*------------------------------------------------------
    Pull-up control register 0//0x03F0
------------------------------------------------------*/
union st_pur0 {          /* Pull-up control register 0 */
    struct{
       unsigned char  PU0_0 :1;  /* Pull-up P00 to P03 */
       unsigned char  PU0_1 :1;  /* Pull-up P04 to P07 */
       unsigned char  PU0_2 :1;  /* Pull-up P10 to P13 */
       unsigned char  PU0_3 :1;  /* Pull-up P14 to P17 */
       unsigned char  PU0_4 :1;  /* Pull-up P20 to P23 */
       unsigned char  PU0_5 :1;  /* Pull-up P24 to P27 */
       unsigned char  PU0_6 :1;  /* Pull-up P30 to P33 */
       unsigned char  PU0_7 :1;  /* Pull-up P34 to P37 */
    }BIT;                /* Bit access         */
    char    BYTE;        /* Byte access        */
};

/*------------------------------------------------------
    Pull-up control register 1//0x03F1
------------------------------------------------------*/
union st_pur1 {          /* Pull-up control register 1 */
    struct{
       unsigned char  PU1_0 :1;  /* Pull-up P40 to P43 */
       unsigned char  PU1_1 :1;  /* Pull-up P44 to P47 */
       unsigned char  PU1_2 :1;  /* Pull-up P50 to P53 */
       unsigned char  PU1_3 :1;  /* Pull-up P54 to P57 */
       unsigned char        :1;  /* Nothing assigned   */
       unsigned char        :1;  /* Nothing assigned   */
       unsigned char        :1;  /* Nothing assigned   */
       unsigned char        :1;  /* Nothing assigned   */
    }BIT;                /* Bit access         */
    char    BYTE;        /* Byte access        */
};
/* When nothing assigned,then when write, set to "0".When read, its content is indeterminate.*/

/*------------------------------------------------------
    Port control register//0x03FF
------------------------------------------------------*/
union st_pcr {           /* Port control register */
    struct{
       unsigned char  PCR0 :1;  /* Port P1 control bit           */
       unsigned char       :1;  /* Reserved bit must be set to 0 */
       unsigned char       :1;  /* Reserved bit must be set to 0 */
       unsigned char       :1;  /* Nothing assigned              */
       unsigned char       :1;  /* Nothing assigned              */
       unsigned char       :1;  /* Nothing assigned              */
       unsigned char       :1;  /* Nothing assigned              */
       unsigned char       :1;  /* Nothing assigned              */
    }BIT;               /* Bit access                    */
    char   BYTE;        /* Byte access                   */
};
/* When nothing assigned,then when write, set to "0".When read, its content is indeterminate.*/


/* Processor mode register 0 */
#define PM0  (*(volatile union st_pm0 *)(0x0004))

/* Processor mode register 1 */
#define PM1  (*(volatile union st_pm1 *)(0x0005))

/* System clock control register 0 */
#define CM0  (*(volatile union st_cm0 *)(0x0006))

/* System clock control register 1 */
#define CM1  (*(volatile union st_cm1 *)(0x0007))

/* Address match interrupt enable register */
#define AIER  (*(volatile union st_aier *)(0x0009))

/* Protect register */
#define PRCR  (*(volatile union st_prcr *)(0x000a))

/* External data bus width control register */
#define DS  (*(volatile union st_ds *)(0x000b))

/* Main clock division register */
#define MCD  (*(volatile union st_mcd *)(0x000c))

/* Oscillation stop detect register */
#define CM2  (*(volatile union st_cm2 *)(0x000d))

/* Watchdog timer start register */
#define WDTS  (*(volatile char *)(0x000e))

/* Watchdog timer control register */
#define WDC  (*(volatile union st_wdc *)(0x000f))

/* Address match interrupt register 0 */
#define RMAD0  (*(volatile union st_rmad0 *)(0x0010))

/* Processor mode register 2 */
#define PM2  (*(volatile union st_pm *)(0x0013)) /* check M32C83 register */

/* Address match interrupt register 1 */
#define RMAD1  (*(volatile union st_rmad1 *)(0x0014))

/* Voltage detection register 2 */
#define VCR2  (*(volatile union st_vcr2 *)(0x0017)) /* check M32C83 register */

/* Address match interrupt register 2*/
#define RMAD2  (*(volatile union st_rmad2 *)(0x0018))

/* Voltage detection register 1*/
#define VCR1  (*(volatile union st_vcr1 *)(0x001B)) /* check M32C83 register */

/* Address match interrupt register 3*/
#define RMAD3  (*(volatile union st_rmad3 *)(0x001c))

/* PLL Control Register 0 */
#define PLC0  (*(volatile union st_plc0 *)(0x0376))

/* PLL control register 1 */
#define PLC1  (*(volatile union st_plc1 *)(0x0377))

/* Address match interrupt register 4 */
#define RMAD4  (*(volatile union st_rmad4 *)(0x0028)) /* check M32C83 register */

/* Address match interrupt register 5 */
#define RMAD5  (*(volatile union st_rmad5 *)(0x002C)) /* check M32C83 register */

/* Voltage down detect interrupt register */
#define D4INT  (*(volatile union st_d4int *)(0x002F)) /* check M32C83 register */

/* Address match interrupt register 6 */
#define RMAD6  (*(volatile union st_rmad6 *)(0x0038)) /* check M32C83 register */

/* Address match interrupt register 7 */
#define RMAD7  (*(volatile union st_rmad7 *)(0x003C)) /* check M32C83 register */

/* External space wait control register 0 */
#define EWCR0  (*(volatile union st_ewcr0 *)(0x0048)) /* check M32C83 register */

/* External space wait control register 1 */
#define EWCR1  (*(volatile union st_ewcr1 *)(0x0049)) /* check M32C83 register */

/* External space wait control register 2 */
#define EWCR2  (*(volatile union st_ewcr2 *)(0x004A)) /* check M32C83 register */

/* External space wait control register 3 */
#define EWCR3  (*(volatile union st_ewcr3 *)(0x004B)) /* check M32C83 register */

/* Flash Memory control register 1 */
#define FMR1  (*(volatile union st_fmr1 *)(0x0056))

/* Flash Memory control register 0 */
#define FMR0  (*(volatile union st_fmr0 *)(0x0057))

/* DMA0 interrupt control register */
#define DM0IC  (*(volatile union st_dm0ic *)(0x0068))

/* Timer B5 interrupt register */
#define TB5IC  (*(volatile union st_tb5ic *)(0x0069))

/* DMA2 interrupt register */
#define DM2IC  (*(volatile union st_dm2ic *)(0x006a))

/* UART2 receive/ack interrupt control register */
#define S2RIC  (*(volatile union st_s2ric *)(0x006b))

/* Timer A0 interrupt control register */
#define TA0IC  (*(volatile union st_ta0ic *)(0x006c))

/* UART3 receive/ack interrupt control register */
#define S3RIC  (*(volatile union st_s3ric *)(0x006d))

/* Timer A2 interrupt control register */
#define TA2IC  (*(volatile union st_ta2ic *)(0x006e))

/* UART4 receive/ack interrupt control register */
#define S4RIC  (*(volatile union st_s4ric *)(0x006f))

/* Timer A4 interrupt control register */
#define TA4IC  (*(volatile union st_ta4ic *)(0x0070))

/* Bus collision (UART0) interrupt control register */
#define BCN0IC  (*(volatile union st_bcn0ic *)(0x0071))

/* Bus collision (UART3) interrupt control register */
#define BCN3IC  (*(volatile union st_bcn0ic *)(0x0071))

/* UART0 receive interrupt control register */
#define S0RIC  (*(volatile union st_s0ric *)(0x0072))

/* A/D0 conversion interrupt control register */
#define AD0IC  (*(volatile union st_s0ric *)(0x0073))

/* UART1 receive interrupt control register */
#define S1RIC  (*(volatile union st_s0ric *)(0x0074))

/* Intelligent I/O interrupt control register 0 */
#define IIO0IC  (*(volatile union st_iio0ic *)(0x0075))

/* CAN interrupt 3 control register */
#define CAN3IC  (*(volatile union st_can3ic *)(0x0075)) /* check M32C83 register */

/* Timer B1 interrupt control register */
#define TB1IC  (*(volatile union st_tb1ic *)(0x0076))

/* Intelligent I/O interrupt control register 2 */
#define IIO2IC  (*(volatile union st_iio2ic *)(0x0077))

/* Timer B3 interrupt control register */
#define TB3IC  (*(volatile union st_tb3ic *)(0x0078))

/* Intelligent I/O interrupt control register 4 */
#define IIO4IC  (*(volatile union st_iio4ic *)(0x0079))

/* INT5~ interrupt control register */
#define INT5IC  (*(volatile union st_int5ic *)(0x007a))

/* INT3~ interrupt control register */
#define INT3IC  (*(volatile union st_int3ic *)(0x007c))

/* Intelligent I/O interrupt control register 8 */
#define IIO8IC  (*(volatile union st_iio8ic *)(0x007d))

/* INT1~ interrupt control register */
#define INT1IC  (*(volatile union st_int1ic *)(0x007e))

/* Intelligent I/O interrupt control register 10 */
#define IIO10IC  (*(volatile union st_iio10ic *)(0x007f))

/* CAN Interrupt 1 Control Register */
#define CAN1IC  (*(volatile union st_can1ic *)(0x007f))

/* CAN Interrupt 2 Control Register */
#define CAN2IC  (*(volatile union st_can2ic *)(0x0081))

/* DMA1 interrupt control register */
#define DM1IC  (*(volatile union st_dm1ic *)(0x0088))

/* UART2 transmit/nack interrupt control register */
#define S2TIC  (*(volatile union st_s2tic *)(0x0089))

/* DMA3 interrupt control register */
#define DM3IC  (*(volatile union st_dm3ic *)(0x008a))

/* UART3 transmit/nack interrupt control register */
#define S3TIC  (*(volatile union st_s3tic *)(0x008b))

/* Timer A1 interrupt control register */
#define TA1IC  (*(volatile union st_ta1ic *)(0x008c))

/* UART4 transmit/nack interrupt control register */
#define S4TIC  (*(volatile union st_s4tic *)(0x008d))

/* Timer A3 interrupt control register */
#define TA3IC  (*(volatile union st_ta3ic *)(0x008e))

/* Bus collision (UART2) interrupt control register */
#define BCN2IC  (*(volatile union st_bcn2ic *)(0x008f))

/* UART0 transmit interrupt control register */
#define S0TIC  (*(volatile union st_s0tic *)(0x0090))

/* Bus collision (UART1) interrupt control register */
#define BCN1IC  (*(volatile union st_bcn1ic *)(0x0091))

/* Bus collision (UART4) interrupt control register */
#define BCN4IC  (*(volatile union st_bcn4ic *)(0x0091))

/* UART1 transmit interrupt control register */
#define S1TIC  (*(volatile union st_s1tic *)(0x0092))

/* Key input interrupt control register */
#define KUPIC  (*(volatile union st_kupic *)(0x0093))

/* Timer B0 interrupt control register */
#define TB0IC  (*(volatile union st_tb0ic *)(0x0094))

/* Intelligent I/O interrupt control register 1 */
#define IIO1IC  (*(volatile union st_iio1ic *)(0x0095))

/* CAN Interrupt 4 Control Register */
#define CAN4IC  (*(volatile union st_can4ic *)(0x0095)) /* check M32C83 register */

/* Timer B2 interrupt control register */
#define TB2IC  (*(volatile union st_tb2ic *)(0x0096))

/* Intelligent I/O interrupt control register 3 */
#define IIO3IC  (*(volatile union st_iio3ic *)(0x0097))

/* Timer B4 interrupt control register */
#define TB4IC  (*(volatile union st_tb4ic *)(0x0098))

/* CAN Interrupt 5 Control Register */
#define CAN5IC  (*(volatile union st_can5ic *)(0x0099)) /* check M32C83 register */

/* INT4~ interrupt control register */
#define INT4IC  (*(volatile union st_int4ic *)(0x009a))

/* INT2~ interrupt control register */
#define INT2IC  (*(volatile union st_int2ic *)(0x009c))

/* Intelligent I/O interrupt control register 9 */
#define IIO9IC  (*(volatile union st_iio9ic *)(0x009d))

/* CAN0 Interrupt Control Register */
#define CAN0IC  (*(volatile union st_iio9ic *)(0x009d))

/* INT0~ interrupt control register */
#define INT0IC  (*(volatile union st_int0ic *)(0x009e))

/* Exit priority register */
#define RLVL  (*(volatile union st_rlvl *)(0x009f))

/* Interrupt request register 0 */
#define IIO0IR  (*(volatile union st_iio0ir *)(0x00a0))

/* Interrupt request register 1 */
#define IIO1IR  (*(volatile union st_iio1ir *)(0x00a1))

/* Interrupt request register 2 */
#define IIO2IR  (*(volatile union st_iio2ir *)(0x00a2))

/* Interrupt request register 3 */
#define IIO3IR  (*(volatile union st_iio3ir *)(0x00a3))

/* Interrupt request register 4 */
#define IIO4IR  (*(volatile union st_iio4ir *)(0x00a4))

/* Interrupt request register 5 */
#define IIO5IR  (*(volatile union st_iio5ir *)(0x00a5))

/* Interrupt request register 8 */
#define IIO8IR  (*(volatile union st_iio8ir *)(0x00a8))

/* Interrupt request register 9 */
#define IIO9IR  (*(volatile union st_iio9ir *)(0x00a9))

/* Interrupt request register 10 */
#define IIO10IR  (*(volatile union st_iio10ir *)(0x00aa))

/* Interrupt request register 11 */
#define IIO11IR  (*(volatile union st_iio11ir *)(0x00ab))

/* Interrupt enable register 0 */
#define IIO0IE  (*(volatile union st_iio0ie *)(0x00b0))

/* Interrupt enable register 1 */
#define IIO1IE  (*(volatile union st_iio1ie *)(0x00b1))

/* Interrupt enable register 2 */
#define IIO2IE  (*(volatile union st_iio2ie *)(0x00b2))

/* Interrupt enable register 3 */
#define IIO3IE  (*(volatile union st_iio3ie *)(0x00b3))

/* Interrupt enable register 4 */
#define IIO4IE  (*(volatile union st_iio4ie *)(0x00b4))

/* Interrupt enable register 5 */
#define IIO5IE  (*(volatile union st_iio5ie *)(0x00b5))

/* Interrupt enable register 8 */
#define IIO8IE  (*(volatile union st_iio8ie *)(0x00b8))

/* Interrupt enable register 9 */
#define IIO9IE  (*(volatile union st_iio9ie *)(0x00b9))

/* Interrupt enable register 10 */
#define IIO10IE  (*(volatile union st_iio10ie *)(0x00ba))

/* Interrupt enable register 11 */
#define IIO11IE  (*(volatile union st_iio11ie *)(0x00bb))

/* SI/O receive buffer register 0 */
#define G0RB  (*(volatile union st_g0rb *)(0x00e8))

/* Transmit buffer register 0 */
#define G0TB  (*(volatile char *)(0x00ea))

/* Receive data register 0 */
#define G0DR  (*(volatile char *)(0x00ea))

/* Receive input register 0 */
#define G0RI  (*(volatile char *)(0x00ec))

/* SI/O communication control register 0 */
#define G0MR  (*(volatile union st_g0mr *)(0x00ed))

/* Transmit Output Register 0 */
#define G0TO  (*(volatile char *)(0x00ee))

/* SI/O Communication Control Register 0 */
#define G0CR  (*(volatile union st_g0cr *)(0x00ef))

/* Data compare register 00 */
#define G0CMP0  (*(volatile char *)(0x00f0))

/* Data compare register 01 */
#define G0CMP1  (*(volatile char *)(0x00f1))

/* Data compare register 02 */
#define G0CMP2  (*(volatile char *)(0x00f2))

/* Data compare register 03 */
#define G0CMP3  (*(volatile char *)(0x00f3))

/* Data mask register 00 */
#define G0MSK0  (*(volatile char *)(0x00f4))

/* Data mask register 01 */
#define G0MSK1  (*(volatile char *)(0x00f5))

/* Communication Clock Select Register */
#define CCS  (*(volatile union st_ccs *)(0x00F6)) /* check M32C83 register */

/* Receive CRC code register 0 */
#define G0RCRC  (*(volatile union st_g0rcrc *)(0x00f8))

/* Transmit CRC code register 0 */
#define G0TCRC  (*(volatile union st_g0tcrc *)(0x00fa))

/* SI/O expansion mode register 0 */
#define G0EMR  (*(volatile union st_g0emr *)(0x00fc))

/* SI/O expansion receive control register 0 */
#define G0ERC  (*(volatile union st_g0erc *)(0x00fd))

/* SI/O special communication interrupt detect register 0 */
#define G0IRF  (*(volatile union st_g0irf *)(0x00fe))

/* SI/O expansion transmit control register 0 */
#define G0ETC  (*(volatile union st_g0etc *)(0x00ff))

/* Time measurement register 10 */
#define G1TM0  (*(volatile union st_g1tm0 *)(0x0100))

/* Waveform generate register 10 */
#define G1PO0  (*(volatile union st_g1po0 *)(0x0100))

/* Time measurement register 11 */
#define G1TM1  (*(volatile union st_g1tm1 *)(0x0102))

/* Waveform generate register 11 */
#define G1PO1  (*(volatile union st_g1po1 *)(0x0102))

/* Time measurement register 12 */
#define G1TM2  (*(volatile union st_g1tm2 *)(0x0104))

/* Waveform generate register 12 */
#define G1PO2  (*(volatile union st_g1po2 *)(0x0104))

/* Time measurement register 13 */
#define G1TM3  (*(volatile union st_g1tm3 *)(0x0106))

/* Waveform generate register 13 */
#define G1PO3  (*(volatile union st_g1po3 *)(0x0106))

/* Time measurement register 14 */
#define G1TM4  (*(volatile union st_g1tm4 *)(0x0108))

/* Waveform generate register 14 */
#define G1PO4  (*(volatile union st_g1po4 *)(0x0108))

/* Time measurement register 15 */
#define G1TM5  (*(volatile union st_g1tm5 *)(0x010a))

/* Waveform generate register 15 */
#define G1PO5  (*(volatile union st_g1po5 *)(0x010a))

/* Time measurement register 16 */
#define G1TM6  (*(volatile union st_g1tm6 *)(0x010c))

/* Waveform generate register 16 */
#define G1PO6  (*(volatile union st_g1po6 *)(0x010c))

/* Time measurement register 15 */
#define G1TM7  (*(volatile union st_g1tm7 *)(0x010e))

/* Waveform generate register 16 */
#define G1PO7  (*(volatile union st_g1po7 *)(0x010e))

/* Waveform generate control register 10 */
#define G1POCR0  (*(volatile union st_g1pocr0 *)(0x0110))

/* Waveform generate control register 11 */
#define G1POCR1  (*(volatile union st_g1pocr1 *)(0x0111))

/* Waveform generate control register 12 */
#define G1POCR2  (*(volatile union st_g1pocr2 *)(0x0112))

/* Waveform generate control register 13 */
#define G1POCR3  (*(volatile union st_g1pocr3 *)(0x0113))

/* Waveform generate control register 14 */
#define G1POCR4  (*(volatile union st_g1pocr4 *)(0x0114))

/* Waveform generate control register 15 */
#define G1POCR5  (*(volatile union st_g1pocr5 *)(0x0115))

/* Waveform generate control register 16 */
#define G1POCR6  (*(volatile union st_g1pocr6 *)(0x0116))

/* Waveform generate control register 17 */
#define G1POCR7  (*(volatile union st_g1pocr7 *)(0x0117))

/* Time measurement control register 10 */
#define G1TMCR0  (*(volatile union st_g1tmcr0 *)(0x0118))

/* Time measurement control register 11 */
#define G1TMCR1  (*(volatile union st_g1tmcr1 *)(0x0119))

/* Time measurement control register 12 */
#define G1TMCR2  (*(volatile union st_g1tmcr2 *)(0x011a))

/* Time measurement control register 13 */
#define G1TMCR3  (*(volatile union st_g1tmcr3 *)(0x011b))

/* Time measurement control register 14 */
#define G1TMCR4  (*(volatile union st_g1tmcr4 *)(0x011c))

/* Time measurement control register 15 */
#define G1TMCR5  (*(volatile union st_g1tmcr5 *)(0x011d))

/* Time measurement control register 16 */
#define G1TMCR6  (*(volatile union st_g1tmcr6 *)(0x011e))

/* Time measurement control register 17 */
#define G1TMCR7  (*(volatile union st_g1tmcr7 *)(0x011f))

/* Base timer register 1 */
#define G1BT  (*(volatile union st_g1bt *)(0x0120))

/* Base timer control register 10 */
#define G1BCR0  (*(volatile union st_g1bcr0 *)(0x0122))

/* Base timer control register 11 */
#define G1BCR1  (*(volatile union st_g1bcr1 *)(0x0123))

/* Time measurement prescaler register 16 */
#define G1TPR6  (*(volatile char *)(0x0124))

/* Time measurement prescaler register 17 */
#define G1TPR7  (*(volatile char *)(0x0125))

/* Function enable register 1 */
#define G1FE  (*(volatile union st_g1fe *)(0x0126))

/* Function select register 1 */
#define G1FS  (*(volatile union st_g1fs *)(0x0127))

/* SI/O receive buffer register 1 */
#define G1RB  (*(volatile union st_g1rb *)(0x0128))

/* Transmit buffer register 1 */
#define G1TB  (*(volatile char *)(0x012a))

/* Receive data register 1 */
#define G1DR  (*(volatile char *)(0x012a))

/* Receive Input register 1 */
#define G1RI  (*(volatile char *)(0x012c))

/* SI/O communication mode register 1 */
#define G1MR  (*(volatile union st_g1mr *)(0x012d))

/* Transmit output register 1 */
#define G1TO  (*(volatile char *)(0x012e))

/* SI/O communication control register 1 */
#define G1CR  (*(volatile union st_g1cr *)(0x012f))

/* Data compare register 10 */
#define G1CMP0  (*(volatile char *)(0x0130))

/* Data compare register 11 */
#define G1CMP1  (*(volatile char *)(0x0131))

/* Data compare register 12 */
#define G1CMP2  (*(volatile char *)(0x0132))

/* Data compare register 13 */
#define G1CMP3  (*(volatile char *)(0x0133))

/* Data mask register 10 */
#define G1MSK0  (*(volatile union st_g1msk0 *)(0x0134))

/* Data mask register 11 */
#define G1MSK1  (*(volatile union st_g1msk1 *)(0x0135))

/* Receive CRC code register 1 */
#define G1RCRC  (*(volatile union st_g1rcrc *)(0x0138))

/* Transmit CRC code register 1 */
#define G1TCRC  (*(volatile union st_g1tcrc *)(0x013a))

/* SI/O extended mode register 1 */
#define G1EMR  (*(volatile union st_g1emr *)(0x013c))

/* SI/O extended receive control register 1 */
#define G1ERC  (*(volatile union st_g1erc *)(0x013d))

/* SI/O special communication interrupt detect register 1 */
#define G1IRF  (*(volatile union st_g1irf *)(0x013e))

/* SI/O extended transmit control register 1 */
#define G1ETC  (*(volatile union st_g1etc *)(0x013f))

/* Input Function Select Register */
#define IPS  (*(volatile union st_ips *)(0x0178))

/* Input Function Select Register A */
#define IPSA  (*(volatile union st_ipsa *)(0x0179)) /* check M32C83 register */

/* CAN0 message slot buffer 0 standard(ID0) */
#define C0SLOT0_0  (*(volatile union st_c0slot0_0 *)(0x01e0))

/* CAN0 message slot buffer 0 standard(ID1) */
#define C0SLOT0_1  (*(volatile union st_c0slot0_1 *)(0x01e1))

/* CAN0 message slot buffer 0 extened(ID0) */
#define C0SLOT0_2  (*(volatile union st_c0slot0_2 *)(0x01e2))

/* CAN0 message slot buffer 0 extended(ID1) */
#define C0SLOT0_3  (*(volatile union st_c0slot0_3 *)(0x01e3))

/* CAN0 message slot buffer 0 extended(ID2) */
#define C0SLOT0_4  (*(volatile union st_c0slot0_4 *)(0x01e4))

/* CAN0 message slot buffer 0 data length code */
#define C0SLOT0_5  (*(volatile union st_c0slot0_5 *)(0x01e5))

/* CAN0 message slot buffer 0 data 0 */
#define C0SLOT0_6  (*(volatile unsigned char *)(0x01e6))

/* CAN0 message slot buffer 0 data 1 */
#define C0SLOT0_7  (*(volatile unsigned char *)(0x01e7))

/* CAN0 message slot buffer 0 data 2 */
#define C0SLOT0_8  (*(volatile unsigned char *)(0x01e8))

/* CAN0 message slot buffer 0 data 3 */
#define C0SLOT0_9  (*(volatile unsigned char *)(0x01e9))

/* CAN0 message slot buffer 0 data 4 */
#define C0SLOT0_10  (*(volatile unsigned char *)(0x01ea))

/* CAN0 message slot buffer 0 data 5 */
#define C0SLOT0_11  (*(volatile unsigned char *)(0x01eb))

/* CAN0 message slot buffer 0 data 6 */
#define C0SLOT0_12  (*(volatile unsigned char *)(0x01ec))

/* CAN0 message slot buffer 0 data 7 */
#define C0SLOT0_13  (*(volatile unsigned char *)(0x01ed))

/* CAN0 message slot buffer 0 time stamp high-ordered */
#define C0SLOT0_14  (*(volatile unsigned char *)(0x01ee))

/* CAN0 message slot buffer 0 time stamp low-ordered */
#define C0SLOT0_15  (*(volatile unsigned char *)(0x01ef))

/* CAN0 message slot buffer 1 standard(ID0) */
#define C0SLOT1_0  (*(volatile union st_c0slot1_0 *)(0x01f0))

/* CAN0 message slot buffer 1 standard(ID1) */
#define C0SLOT1_1  (*(volatile union st_c0slot1_1 *)(0x01f1))

/* CAN0 message slot buffer 1 extened (ID0) */
#define C0SLOT1_2  (*(volatile union st_c0slot1_2 *)(0x01f2))

/* CAN0 message slot buffer 1 extended(ID1) */
#define C0SLOT1_3  (*(volatile union st_c0slot1_3 *)(0x01f3))

/* CAN0 message slot buffer 1 extended(ID2) */
#define C0SLOT1_4  (*(volatile union st_c0slot1_4 *)(0x01f4))

/* CAN0 message slot buffer 1 data length code */
#define C0SLOT1_5  (*(volatile union st_c0slot1_5 *)(0x01f5))

/* CAN0 message slot buffer 1 data 0 */
#define C0SLOT1_6  (*(volatile unsigned char *)(0x01f6))

/* CAN0 message slot buffer 1 data 1 */
#define C0SLOT1_7  (*(volatile unsigned char *)(0x01f7))

/* CAN0 message slot buffer 1 data 2 */
#define C0SLOT1_8  (*(volatile unsigned char *)(0x01f8))

/* CAN0 message slot buffer 1 data 3 */
#define C0SLOT1_9  (*(volatile unsigned char *)(0x01f9))

/* CAN0 message slot buffer 1 data 4 */
#define C0SLOT1_10  (*(volatile unsigned char *)(0x01fa))

/* CAN0 message slot buffer 1 data 5 */
#define C0SLOT1_11  (*(volatile unsigned char *)(0x01fb))

/* CAN0 message slot buffer 1 data 6 */
#define C0SLOT1_12  (*(volatile unsigned char *)(0x01fc))

/* CAN0 message slot buffer 1 data 7 */
#define C0SLOT1_13  (*(volatile unsigned char *)(0x01fd))

/* CAN0 message slot buffer 1 time stamp high-ordered */
#define C0SLOT1_14  (*(volatile unsigned char *)(0x01fe))

/* CAN0 message slot buffer 1 time stamp low-ordered */
#define C0SLOT1_15  (*(volatile unsigned char *)(0x01ff))

/* CAN0 control register 0 */
#define C0CTLR0  (*(volatile union st_c0ctlr0 *)(0x0200))

/* CAN0 status register */
#define C0STR  (*(volatile union st_c0str *)(0x0202))

/* CAN0 extended ID register */
#define C0IDR  (*(volatile union st_c0idr *)(0x0204))

/* CAN0 configuration register */
#define C0CONR  (*(volatile union st_c0conr *)(0x0206))

/* CAN0 time stamp register */
#define C0TSR  (*(volatile union st_c0tsr *)(0x0208))

/* CAN0 transmit error Count Register */
#define C0TEC  (*(volatile unsigned char *)(0x020a))

/* CAN0 receive error Count Register */
#define C0REC  (*(volatile unsigned char *)(0x020b))

/* CAN0 Slot Interrupt Status Register */
#define C0SISTR  (*(volatile union st_c0sistr *)(0x020c))

/* CAN0 Slot Interrupt Mask Register */
#define C0SIMKR  (*(volatile union st_c0simkr *)(0x0210))

/* CAN0 error interrupt mask register */
#define C0EIMKR  (*(volatile union st_c0eimkr *)(0x0214))

/* CAN0 error interrupt status register */
#define C0EISTR  (*(volatile union st_c0eistr *)(0x0215))

/* CAN0 error factor register */
#define C0EFR  (*(volatile union st_c0efr *)(0x0216)) /* check M32C83 register */

/* CAN0 baud rate prescaler */
#define C0BRP  (*(volatile unsigned char *)(0x0217))

/* CAN0 mode register */
#define C0MDR  (*(volatile union st_c0mdr *)(0x0219)) /* check M32C83 register */

/* CAN0 single shot control register register */
#define C0SSCTLR  (*(volatile union st_c0ssctlr *)(0x0220)) /* check M32C83 register */

/* CAN0 single shot status register register */
#define C0SSSTR  (*(volatile union st_c0ssstr *)(0x0224)) /* check M32C83 register */

/* CAN0 Global Mask Register Standard ID0 */
#define C0GMR0  (*(volatile union st_c0gmr0 *)(0x0228))

/* CAN0 Global Mask Register Standard ID1 */
#define C0GMR1  (*(volatile union st_c0gmr1 *)(0x0229))

/* CAN0 Global Mask Register Extended ID0 */
#define C0GMR2  (*(volatile union st_c0gmr2 *)(0x022a))

/* CAN0 Global Mask Register Extended ID1 */
#define C0GMR3  (*(volatile union st_c0gmr3 *)(0x022b))

/* CAN0 Global Mask Register Extended ID2 */
#define C0GMR4  (*(volatile union st_c0gmr4 *)(0x022c))

/* CAN0 Message Slot0 Control Registe */
#define C0MCTL0  (*(volatile union st_c0mctl0 *)(0x0230))

/* CAN0 Message Slot1 Control Registe */
#define C0MCTL1  (*(volatile union st_c0mctl1 *)(0x0231))

/* CAN0 Message Slot2 Control Registe */
#define C0MCTL2  (*(volatile union st_c0mctl2 *)(0x0232))

/* CAN0 Message Slot3 Control Registe */
#define C0MCTL3  (*(volatile union st_c0mctl3 *)(0x0233))

/* CAN0 Message Slot4 Control Registe */
#define C0MCTL4  (*(volatile union st_c0mctl4 *)(0x0234))

/* CAN0 Message Slot5 Control Registe */
#define C0MCTL5  (*(volatile union st_c0mctl5 *)(0x0235))

/* CAN0 Message Slot6 Control Registe */
#define C0MCTL6  (*(volatile union st_c0mctl6 *)(0x0236))

/* CAN0 Message Slot7 Control Registe */
#define C0MCTL7  (*(volatile union st_c0mctl7 *)(0x0237))

/* CAN0 Message Slot8 Control Registe */
#define C0MCTL8  (*(volatile union st_c0mctl8 *)(0x0238))

/* CAN0 Message Slot9 Control Registe */
#define C0MCTL9  (*(volatile union st_c0mctl9 *)(0x0239))

/* CAN0 Message Slot10 Control Registe */
#define C0MCTL10  (*(volatile union st_c0mctl10 *)(0x023a))

/* CAN0 Message Slot11 Control Registe */
#define C0MCTL11  (*(volatile union st_c0mctl11 *)(0x023b))

/* CAN0 Message Slot12 Control Registe */
#define C0MCTL12  (*(volatile union st_c0mctl12 *)(0x023c))

/* CAN0 Message Slot13 Control Registe */
#define C0MCTL13  (*(volatile union st_c0mctl13 *)(0x023d))

/* CAN0 Message Slot14 Control Registe */
#define C0MCTL14  (*(volatile union st_c0mctl14 *)(0x023e))

/* CAN0 Message Slot15 Control Registe */
#define C0MCTL15  (*(volatile union st_c0mctl15 *)(0x023f))

/* CAN0 slot buffer select register*/
#define C0SBS  (*(volatile union st_c0sbs *)(0x0240))

/* CAN0 control register */
#define C0CTLR1  (*(volatile union st_c0ctlr1 *)(0x0241))

/* CAN0 sleep control register */
#define C0SLPR  (*(volatile union st_c0slpr *)(0x0242))

/* CAN0 acceptance filter support register*/
#define C0AFS  (*(volatile union st_c0afs *)(0x0244))

/* CAN1 slot buffer select register*/
#define C1SBS  (*(volatile union st_c1sbs *)(0x0250)) /* check M32C83 register */

/* CAN1 control register */
#define C1CTLR1  (*(volatile union st_c1ctlr1 *)(0x0251)) /* check M32C83 register */

/* CAN1 sleep control register */
#define C1SLPR  (*(volatile union st_c1slpr *)(0x0252)) /* check M32C83 register */

/* CAN1 acceptance filter support register*/
#define C1AFS  (*(volatile union st_c1afs *)(0x0254)) /* check M32C83 register */

/* CAN1 message slot buffer 0 standard(ID0) */
#define C1SLOT0_0  (*(volatile union st_c1slot0_0 *)(0x0260)) /* check M32C83 register */

/* CAN1 message slot buffer 0 standard(ID1) */
#define C1SLOT0_1  (*(volatile union st_c1slot0_1 *)(0x0261)) /* check M32C83 register */

/* CAN1 message slot buffer 0 extended(ID0) */
#define C1SLOT0_2  (*(volatile union st_c1slot0_2 *)(0x0262)) /* check M32C83 register */

/* CAN1 message slot buffer 0 extended(ID1) */
#define C1SLOT0_3  (*(volatile union st_c1slot0_3 *)(0x0263)) /* check M32C83 register */

/* CAN1 message slot buffer 0 extended(ID2) */
#define C1SLOT0_4  (*(volatile union st_c1slot0_4 *)(0x0264)) /* check M32C83 register */

/* CAN1 message slot buffer 0 data length code */
#define C1SLOT0_5  (*(volatile union st_c1slot0_5 *)(0x0265)) /* check M32C83 register */

/* CAN1 message slot buffer 0 data 0 */
#define C1SLOT0_6  (*(volatile unsigned char *)(0x0266)) /* check M32C83 register */

/* CAN1 message slot buffer 0 data 1 */
#define C1SLOT0_7  (*(volatile unsigned char *)(0x0267)) /* check M32C83 register */

/* CAN1 message slot buffer 0 data 2 */
#define C1SLOT0_8  (*(volatile unsigned char *)(0x0268)) /* check M32C83 register */

/* CAN1 message slot buffer 0 data 3 */
#define C1SLOT0_9  (*(volatile unsigned char *)(0x0269)) /* check M32C83 register */

/* CAN1 message slot buffer 0 data 4 */
#define C1SLOT0_10  (*(volatile unsigned char *)(0x026A)) /* check M32C83 register */

/* CAN1 message slot buffer 0 data 5 */
#define C1SLOT0_11  (*(volatile unsigned char *)(0x026B)) /* check M32C83 register */

/* CAN1 message slot buffer 0 data 6 */
#define C1SLOT0_12  (*(volatile unsigned char *)(0x026C)) /* check M32C83 register */

/* CAN1 message slot buffer 0 data 7 */
#define C1SLOT0_13  (*(volatile unsigned char *)(0x026D)) /* check M32C83 register */

/* CAN1 message slot buffer 0 time stamp high-ordered */
#define C1SLOT0_14  (*(volatile unsigned char *)(0x026E)) /* check M32C83 register */

/* CAN1 message slot buffer 0 time stamp low-ordered */
#define C1SLOT0_15  (*(volatile unsigned char *)(0x026F)) /* check M32C83 register */

/* CAN1 message slot buffer 1 standard(ID0) */
#define C1SLOT1_0  (*(volatile union st_c1slot1_0 *)(0x0270)) /* check M32C83 register */

/* CAN1 message slot buffer 1 standard(ID1) */
#define C1SLOT1_1  (*(volatile union st_c1slot1_1 *)(0x0271)) /* check M32C83 register */

/* CAN1 message slot buffer 1 extended(ID0) */
#define C1SLOT1_2  (*(volatile union st_c1slot1_2 *)(0x0272)) /* check M32C83 register */

/* CAN1 message slot buffer 1 extended(ID1) */
#define C1SLOT1_3  (*(volatile union st_c1slot1_3 *)(0x0273)) /* check M32C83 register */

/* CAN1 message slot buffer 1 extended(ID2) */
#define C1SLOT1_4  (*(volatile union st_c1slot1_4 *)(0x0274)) /* check M32C83 register */

/* CAN1 message slot buffer 1 data length code */
#define C1SLOT1_5  (*(volatile union st_c1slot1_5 *)(0x0275)) /* check M32C83 register */

/* CAN1 message slot buffer 1 data 0 */
#define C1SLOT1_6  (*(volatile unsigned char *)(0x0276)) /* check M32C83 register */

/* CAN1 message slot buffer 1 data 1 */
#define C1SLOT1_7  (*(volatile unsigned char *)(0x0277)) /* check M32C83 register */

/* CAN1 message slot buffer 1 data 2 */
#define C1SLOT1_8  (*(volatile unsigned char *)(0x0278)) /* check M32C83 register */

/* CAN1 message slot buffer 1 data 3 */
#define C1SLOT1_9  (*(volatile unsigned char *)(0x0279)) /* check M32C83 register */

/* CAN1 message slot buffer 1 data 4 */
#define C1SLOT1_10  (*(volatile unsigned char *)(0x027A)) /* check M32C83 register */

/* CAN1 message slot buffer 1 data 5 */
#define C1SLOT1_11  (*(volatile unsigned char *)(0x027B)) /* check M32C83 register */

/* CAN1 message slot buffer 1 data 6 */
#define C1SLOT1_12  (*(volatile unsigned char *)(0x027C)) /* check M32C83 register */

/* CAN1 message slot buffer 1 data 7 */
#define C1SLOT1_13  (*(volatile unsigned char *)(0x027D)) /* check M32C83 register */

/* CAN1 message slot buffer 1 time stamp high-ordered */
#define C1SLOT1_14  (*(volatile unsigned char *)(0x027E)) /* check M32C83 register */

/* CAN1 message slot buffer 1 time stamp low-ordered */
#define C1SLOT1_15  (*(volatile unsigned char *)(0x027F)) /* check M32C83 register */

/* CAN1 control register 0 */
#define C1CTLR0  (*(volatile union st_c1ctlr0 *)(0x0280)) /* check M32C83 register */

/* CAN1 status register */
#define C1STR  (*(volatile union st_c1str *)(0x0282)) /* check M32C83 register */

/* CAN1 extended ID register */
#define C1IDR  (*(volatile union st_c1idr *)(0x0284)) /* check M32C83 register */

/* CAN1 configuration register */
#define C1CONR  (*(volatile union st_c1conr *)(0x0286)) /* check M32C83 register */

/* CAN1 time stamp register */
#define C1TSR  (*(volatile union st_c1tsr *)(0x0288)) /* check M32C83 register */

/* CAN1 transmit error counter */
#define C1TEC  (*(volatile unsigned char *)(0x028A)) /* check M32C83 register */

/* CAN1 receive error counter */
#define C1REC  (*(volatile unsigned char *)(0x028B)) /* check M32C83 register */

/* CAN1 slot Interrupt Control Register */
#define C1SISTR  (*(volatile union st_c1sistr *)(0x028C)) /* check M32C83 register */

/* CAN1 slot interrupt mask register */
#define C1SIMKR  (*(volatile union st_c1simkr *)(0x0290)) /* check M32C83 register */

/* CAN1 error interrupt mask register */
#define C1EIMKR  (*(volatile union st_c1eimkr *)(0x0294)) /* check M32C83 register */

/* CAN1 error interrupt status register */
#define C1EISTR  (*(volatile union st_c1eistr *)(0x0295)) /* check M32C83 register */

/* CAN1 error factor register */
#define C1EFR  (*(volatile union st_c1efr *)(0x0296)) /* check M32C83 register */

/* CAN1 baud rate prescaler */
#define C1BRP  (*(volatile unsigned char *)(0x0297)) /* check M32C83 register */

/* CAN1 mode register */
#define C1MDR  (*(volatile union st_c1mdr *)(0x0299)) /* check M32C83 register */

/* CAN1 single shot control register register */
#define C1SSCTLR  (*(volatile union st_c1ssctlr *)(0x02A0)) /* check M32C83 register */

/* CAN1 single shot status register register */
#define C1SSSTR  (*(volatile union st_c1ssstr *)(0x02A4)) /* check M32C83 register */

/* CAN1 Global Mask Register Standard ID0 */
#define C1GMR0  (*(volatile union st_c1gmr0 *)(0x02A8)) /* check M32C83 register */

/* CAN1 Global Mask Register Standard ID1 */
#define C1GMR1  (*(volatile union st_c1gmr1 *)(0x02A9)) /* check M32C83 register */

/* CAN1 Global Mask Register Extended ID0 */
#define C1GMR2  (*(volatile union st_c1gmr2 *)(0x02AA)) /* check M32C83 register */

/* CAN1 Global Mask Register Extended ID1 */
#define C1GMR3  (*(volatile union st_c1gmr3 *)(0x02AB)) /* check M32C83 register */

/* CAN1 Global Mask Register Extended ID2 */
#define C1GMR4  (*(volatile union st_c1gmr4 *)(0x02AC)) /* check M32C83 register */

/* CAN1 Message Slot0 Control Registe */
#define C1MCTL0  (*(volatile union st_c1mctl0 *)(0x02B0)) /* check M32C83 register */

/* CAN1 Message Slot1 Control Registe */
#define C1MCTL1  (*(volatile union st_c1mctl1 *)(0x02B1)) /* check M32C83 register */

/* CAN1 Message Slot2 Control Registe */
#define C1MCTL2  (*(volatile union st_c1mctl2 *)(0x02B2)) /* check M32C83 register */

/* CAN1 Message Slot3 Control Registe */
#define C1MCTL3  (*(volatile union st_c1mctl3 *)(0x02B3)) /* check M32C83 register */

/* CAN1 Message Slot4 Control Registe */
#define C1MCTL4  (*(volatile union st_c1mctl4 *)(0x02B4)) /* check M32C83 register */

/* CAN1 Message Slot5 Control Registe */
#define C1MCTL5  (*(volatile union st_c1mctl5 *)(0x02B5)) /* check M32C83 register */

/* CAN1 Message Slot6 Control Registe */
#define C1MCTL6  (*(volatile union st_c1mctl6 *)(0x02B6)) /* check M32C83 register */

/* CAN1 Message Slot7 Control Registe */
#define C1MCTL7  (*(volatile union st_c1mctl7 *)(0x02B7)) /* check M32C83 register */

/* CAN1 Message Slot8 Control Registe */
#define C1MCTL8  (*(volatile union st_c1mctl8 *)(0x02B8)) /* check M32C83 register */

/* CAN1 Message Slot9 Control Registe */
#define C1MCTL9  (*(volatile union st_c1mctl9 *)(0x02B9)) /* check M32C83 register */

/* CAN1 Message Slot10 Control Registe */
#define C1MCTL10  (*(volatile union st_c1mctl10 *)(0x02BA)) /* check M32C83 register */

/* CAN1 Message Slot11 Control Registe */
#define C1MCTL11  (*(volatile union st_c1mctl11 *)(0x02BB)) /* check M32C83 register */

/* CAN1 Message Slot12 Control Registe */
#define C1MCTL12  (*(volatile union st_c1mctl12 *)(0x02BC)) /* check M32C83 register */

/* CAN1 Message Slot13 Control Registe */
#define C1MCTL13  (*(volatile union st_c1mctl13 *)(0x02BD)) /* check M32C83 register */

/* CAN1 Message Slot14 Control Registe */
#define C1MCTL14  (*(volatile union st_c1mctl14 *)(0x02BE)) /* check M32C83 register */

/* CAN1 Message Slot15 Control Registe */
#define C1MCTL15  (*(volatile union st_c1mctl15 *)(0x02BF)) /* check M32C83 register */

/* X0 register Y0 register */
#define X0R_Y0R  (*(volatile union st_x0r_y0r *)(0x02C0)) /* check M32C83 register */

/* X1 register Y1 register */
#define X1R_Y1R  (*(volatile union st_x1r_y1r *)(0x02C2)) /* check M32C83 register */

/* X2 register Y2 register */
#define X2R_Y2R  (*(volatile union st_x2r_y2r *)(0x02C4)) /* check M32C83 register */

/* X3 register Y3 register */
#define X3R_Y3R  (*(volatile union st_x3r_y3r *)(0x02C6)) /* check M32C83 register */

/* X4 register Y4 register */
#define X4R_Y4R  (*(volatile union st_x4r_y4r *)(0x02C8)) /* check M32C83 register */

/* X5 register Y5 register */
#define X5R_Y5R  (*(volatile union st_x5r_y5r *)(0x02CA)) /* check M32C83 register */

/* X6 register Y6 register */
#define X6R_Y6R  (*(volatile union st_x6r_y6r *)(0x02CC)) /* check M32C83 register */

/* X7 register Y7 register */
#define X7R_Y7R  (*(volatile union st_x7r_y7r *)(0x02CE)) /* check M32C83 register */

/* X8 register Y8 register */
#define X8R_Y8R  (*(volatile union st_x8r_y8r *)(0x02D0)) /* check M32C83 register */

/* X9 register Y9 register */
#define X9R_Y9R  (*(volatile union st_x9r_y9r *)(0x02D2)) /* check M32C83 register */

/* X10 register Y10 register */
#define X10R_Y10R  (*(volatile union st_x10r_y10r *)(0x02D4)) /* check M32C83 register */

/* X11 register Y11 register */
#define X11R_Y11R  (*(volatile union st_x11r_y11r *)(0x02D6)) /* check M32C83 register */

/* X12 register Y12 register */
#define X12R_Y12R  (*(volatile union st_x12r_y12r *)(0x02D8)) /* check M32C83 register */

/* X13 register Y13 register */
#define X13R_Y13R  (*(volatile union st_x13r_y13r *)(0x02DA)) /* check M32C83 register */

/* X14 register Y14 register */
#define X14R_Y14R  (*(volatile union st_x14r_y14r *)(0x02DC)) /* check M32C83 register */

/* X15 register Y15 register */
#define X15R_Y15R  (*(volatile union st_x15r_y15r *)(0x02DE)) /* check M32C83 register */

/* X-Y control register */
#define XYC  (*(volatile union st_xyc *)(0x02e0))

/* UART1 special mode register 4 */
#define U1SMR4  (*(volatile union st_u1smr4 *)(0x02e4))

/* UART1 special mode register 3 */
#define U1SMR3  (*(volatile union st_u1smr3 *)(0x02e5))

/* UART1 special mode register 2 */
#define U1SMR2  (*(volatile union st_u1smr2 *)(0x02e6))

/* UART1 special mode register */
#define U1SMR  (*(volatile union st_u1smr *)(0x02e7))

/* UART1 transmit/receive mode register */
#define U1MR  (*(volatile union st_u1mr *)(0x02e8))

/* UART1 bit rate generator */
#define U1BRG  (*(volatile unsigned char *)(0x02e9))

/* UART1 transmit buffer register */
#define U1TB  (*(volatile union st_u1tb *)(0x02ea))

/* UART1 transmit/receive control register 0 */
#define U1C0  (*(volatile union st_u1c0 *)(0x02ec))

/* UART1 transmit/receive control register 1 */
#define U1C1  (*(volatile union st_u1c1 *)(0x02ed))

/* UART1 receive buffer register 1 */
#define U1RB  (*(volatile union st_u1rb *)(0x02ee))

/* UART4 special mode register 4 */
#define U4SMR4  (*(volatile union st_u4smr4 *)(0x02f4))

/* UART4 special mode register 3 */
#define U4SMR3  (*(volatile union st_u4smr3 *)(0x02f5))

/* UART4 special mode register 2 */
#define U4SMR2  (*(volatile union st_u4smr2 *)(0x02f6))

/* UART4 special mode register */
#define U4SMR  (*(volatile union st_u4smr *)(0x02f7))

/* UART4 transmit/receive mode register */
#define U4MR  (*(volatile union st_u4mr *)(0x02f8))

/* UART4 bit rate generator */
#define U4BRG  (*(volatile unsigned char *)(0x02f9))

/* UART4 transmit buffer register */
#define U4TB  (*(volatile union st_u4tb *)(0x02fa))

/* UART4 transmit/receive control register 0 */
#define U4C0  (*(volatile union st_u4c0 *)(0x02fc))

/* UART4 transmit/receive control register 1 */
#define U4C1  (*(volatile union st_u4c1 *)(0x02fd))

/* UART4 receive buffer register 1 */
#define U4RB  (*(volatile union st_u4rb *)(0x02fe))

/* Timer B3,4,5 count start flag */
#define TBSR  (*(volatile union st_tbsr *)(0x0300))

/* Timer A1-1 Register */
#define TA11  (*(volatile unsigned short *)(0x0302))

/* Timer A2-1 Register */
#define TA21  (*(volatile unsigned short *)(0x0304))

/* Timer A4-1 Register */
#define TA41  (*(volatile unsigned short *)(0x0306))

/* Three-phase PWM control register 0 */
#define INVC0  (*(volatile union st_invc0 *)(0x0308))

/* Three-phase PWM control register 1 */
#define INVC1  (*(volatile union st_invc1 *)(0x0309))

/* Three-phase output buffer register 0 */
#define IDB0  (*(volatile union st_idb0 *)(0x030a))

/* Three-phase output buffer register 1 */
#define IDB1  (*(volatile union st_idb1 *)(0x030b))

/* Dead time timer */
#define DTT  (*(volatile unsigned char *)(0x030c))
/* The MOV instruction should be used to set the DTT register */

/* Timer B2 interrupt generation frequency set counter */
#define ICTB2  (*(volatile unsigned char *)(0x030d))
/* The MOV instruction should be used to the ICTB2 register */

/* Timer B3 register */
#define TB3  (*(volatile unsigned short *)(0x0310))

/* Timer B4 register */
#define TB4  (*(volatile unsigned short *)(0x0312))

/* Timer B5 register */
#define TB5  (*(volatile unsigned short *)(0x0314))

/* Timer B3 mode register */
#define TB3MR  (*(volatile union st_tb3mr *)(0x031b))

/* Timer B4 mode register */
#define TB4MR  (*(volatile union st_tb4mr *)(0x031c))

/* Timer B5 mode register */
#define TB5MR  (*(volatile union st_tb5mr *)(0x031d))

/* External interrupt request cause select register */
#define IFSR  (*(volatile union st_ifsr *)(0x031f))

/* UART3 special mode register 4 */
#define U3SMR4  (*(volatile union st_u3smr4 *)(0x0324))

/* UART3 special mode register 3 */
#define U3SMR3  (*(volatile union st_u3smr3 *)(0x0325))

/* UART3 special mode register 2 */
#define U3SMR2  (*(volatile union st_u3smr2 *)(0x0326))

/* UART3 special mode register */
#define U3SMR  (*(volatile union st_u3smr *)(0x0327))

/* UART3 transmit/receive mode register */
#define U3MR  (*(volatile union st_u3mr *)(0x0328))

/* UART3 bit rate generator */
#define U3BRG  (*(volatile unsigned char *)(0x0329))

/* UART3 transmit buffer register */
#define U3TB  (*(volatile union st_u3tb *)(0x032a))

/* UART3 transmit/receive control register 0 */
#define U3C0  (*(volatile union st_u3c0 *)(0x032c))

/* UART3 transmit/receive control register 1 */
#define U3C1  (*(volatile union st_u3c1 *)(0x032d))

/* UART3 receive buffer register 1 */
#define U3RB  (*(volatile union st_u3rb *)(0x032e))

/* UART2 special mode register 4 */
#define U2SMR4  (*(volatile union st_u2smr4 *)(0x0334))

/* UART2 special mode register 3 */
#define U2SMR3  (*(volatile union st_u2smr3 *)(0x0335))

/* UART2 special mode register 2 */
#define U2SMR2  (*(volatile union st_u2smr2 *)(0x0336))

/* UART2 special mode register */
#define U2SMR  (*(volatile union st_u2smr *)(0x0337))

/* UART2 transmit/receive mode register */
#define U2MR  (*(volatile union st_u2mr *)(0x0338))

/* UART2 bit rate generator */
#define U2BRG  (*(volatile unsigned char *)(0x0339))

/* UART2 transmit buffer register */
#define U2TB  (*(volatile union st_u2tb *)(0x033a))

/* UART2 transmit/receive control register 0 */
#define U2C0  (*(volatile union st_u2c0 *)(0x033c))

/* UART2 transmit/receive control register 1 */
#define U2C1  (*(volatile union st_u2c1 *)(0x033d))

/* UART2 receive buffer register 1 */
#define U2RB  (*(volatile union st_u2rb *)(0x033e))

/* Count start flag */
#define TABSR  (*(volatile union st_tabsr *)(0x0340))

/* Clock prescaler reset flag */
#define CPSRF  (*(volatile union st_cpsrf *)(0x0341))

/* One-shot start flag */
#define ONSF  (*(volatile union st_onsf *)(0x0342))

/* Trigger select register */
#define TRGSR  (*(volatile union st_trgsr *)(0x0343))

/* Up Down Flag */
#define UDF  (*(volatile union st_udf *)(0x0344))

/* Timer A0 register */
#define TA0  (*(volatile unsigned short *)(0x0346))

/* Timer A1 register */
#define TA1  (*(volatile unsigned short *)(0x0348))

/* Timer A2 register */
#define TA2  (*(volatile unsigned short *)(0x034a))

/* Timer A3 register */
#define TA3  (*(volatile unsigned short *)(0x034c))

/* Timer A4 register */
#define TA4  (*(volatile unsigned short *)(0x034e))

/* Timer B0 register */
#define TB0  (*(volatile unsigned short *)(0x0350))

/* Timer B1 register */
#define TB1  (*(volatile unsigned short *)(0x0352))

/* Timer B2 register */
#define TB2  (*(volatile unsigned short *)(0x0354))

/* Timer A0 mode register */
#define TA0MR  (*(volatile union st_ta0mr *)(0x0356))

/* Timer A1 mode register */
#define TA1MR  (*(volatile union st_ta1mr *)(0x0357))

/* Timer A2 mode register */
#define TA2MR  (*(volatile union st_ta2mr *)(0x0358))

/* Timer A3 mode register */
#define TA3MR  (*(volatile union st_ta3mr *)(0x0359))

/* Timer A4 mode register */
#define TA4MR  (*(volatile union st_ta4mr *)(0x035a))

/* Timer B0 mode register */
#define TB0MR  (*(volatile union st_tb0mr *)(0x035b))

/* Timer B1 mode register */
#define TB1MR  (*(volatile union st_tb1mr *)(0x035c))

/* Timer B2 mode register */
#define TB2MR  (*(volatile union st_tb2mr *)(0x035d))

/* Timer B2 special mode register */
#define TB2SC  (*(volatile union st_tb2sc *)(0x035e))

/* Count source prescaler register */
#define TCSPR  (*(volatile union st_tcspr *)(0x035f))

/* UART0 special mode register 4 */
#define U0SMR4  (*(volatile union st_u0smr4 *)(0x0364))

/* UART0 special mode register 3 */
#define U0SMR3  (*(volatile union st_u0smr3 *)(0x0365))

/* UART0 special mode register 2 */
#define U0SMR2  (*(volatile union st_u0smr2 *)(0x0366))

/* UART0 special mode register */
#define U0SMR  (*(volatile union st_u0smr *)(0x0367))

/* UART0 transmit/receive mode register */
#define U0MR  (*(volatile union st_u0mr *)(0x0368))

/* UART0 bit rate generator */
#define U0BRG  (*(volatile unsigned char *)(0x0369))

/* UART0 transmit buffer register */
#define U0TB  (*(volatile union st_u0tb *)(0x036a))

/* UART0 transmit/receive control register 0 */
#define U0C0  (*(volatile union st_u0c0 *)(0x036c))

/* UART0 transmit/receive control register 1 */
#define U0C1  (*(volatile union st_u0c1 *)(0x036d))

/* UART0 receive buffer register 1 */
#define U0RB  (*(volatile union st_u0rb *)(0x036e))

/* DMA0 request source select register */
#define DM0SL  (*(volatile union st_dm0sl *)(0x0378))

/* DMA1 request source select register */
#define DM1SL  (*(volatile union st_dm1sl *)(0x0379))

/* DMA2 request source select register */
#define DM2SL  (*(volatile union st_dm2sl *)(0x037a))

/* DMA3 request source select register */
#define DM3SL  (*(volatile union st_dm3sl *)(0x037b))

/* CRC data register */
#define CRCD  (*(volatile union st_crcd *)(0x037c))

/* CRC input register */
#define CRCIN  (*(volatile unsigned char *)(0x037e))

/* A/D0 register 0 */
#define AD00  (*(volatile union st_ad00 *)(0x0380))

/* A/D0 register 1 */
#define AD01  (*(volatile union st_ad01 *)(0x0382))

/* A/D0 register 2 */
#define AD02  (*(volatile union st_ad02 *)(0x0384))

/* A/D0 register 3 */
#define AD03  (*(volatile union st_ad03 *)(0x0386))

/* A/D0 register 4 */
#define AD04  (*(volatile union st_ad04 *)(0x0388))

/* A/D0 register 5 */
#define AD05  (*(volatile union st_ad05 *)(0x038a))

/* A/D0 register 6 */
#define AD06  (*(volatile union st_ad06 *)(0x038c))

/* A/D0 register 7 */
#define AD07  (*(volatile union st_ad07 *)(0x038e))

/* A/D0 Control Register 4 */
#define AD0CON4  (*(volatile union st_ad0con4 *)(0x0392)) /* check M32C83 register */

/* A/D0 Control Register 2 */
#define AD0CON2  (*(volatile union st_ad0con2 *)(0x0394))

/* A/D0 Control Register 3 */
#define AD0CON3  (*(volatile union st_ad0con3 *)(0x0395)) /* check M32C83 register */

/* A/D0 Control Register 0 */
#define AD0CON0  (*(volatile union st_ad0con0 *)(0x0396))

/* A/D0 Control Register 1 */
#define AD0CON1  (*(volatile union st_ad0con1 *)(0x0397))

/* D/A register 0 */
#define DA0  (*(volatile unsigned char *)(0x0398))

/* D/A register 1 */
#define DA1  (*(volatile unsigned char *)(0x039a))

/* D/A control register */
#define DACON  (*(volatile union st_dacon *)(0x039c))

/* Function select register A8 */
#define PS8  (*(volatile union st_ps8 *)(0x03a0))

/* Function select register A9 */
#define PS9  (*(volatile union st_ps9 *)(0x03a1))

/* Function select register D1 */
#define PSD1  (*(volatile union st_psd1 *)(0x03A7)) /* check M32C83 register */

/* Function select register C2 */
#define PSC2  (*(volatile union st_psc2 *)(0x03AC)) /* check M32C83 register */

/* Function select register C3 */
#define PSC3  (*(volatile union st_psc3 *)(0x03AD)) /* check M32C83 register */

/* Function select register C */
#define PSC  (*(volatile union st_psc *)(0x03af))

/* Function select register A0 */
#define PS0  (*(volatile union st_ps0 *)(0x03b0))

/* Function select register A1 */
#define PS1  (*(volatile union st_ps1 *)(0x03b1))

/* Function select register B0 */
#define PSl0  (*(volatile union st_psl0 *)(0x03b2))

/* Function select register B1 */
#define PSl1  (*(volatile union st_psl1 *)(0x03b3))

/* Function select register A2 */
#define PS2  (*(volatile union st_ps2 *)(0x03b4))

/* Function select register A3 */
#define PS3  (*(volatile union st_ps3 *)(0x03b5))

/* Function select register B2 */
#define PSl2  (*(volatile union st_psl2 *)(0x03b6))

/* Function select register B3 */
#define PSl3  (*(volatile union st_psl3 *)(0x03b7))

/* Function select register A5 */
#define PS5  (*(volatile union st_ps5 *)(0x03b9))

/* Port P6 */
#define P6  (*(volatile union st_p6 *)(0x03c0))

/* Port P7 */
#define P7  (*(volatile union st_p7 *)(0x03c1))

/* Port P6 direction register */
#define PD6  (*(volatile union st_pd6 *)(0x03c2))

/* Port P7 direction register */
#define PD7  (*(volatile union st_pd7 *)(0x03c3))

/* Port P8 */
#define P8  (*(volatile union st_p8 *)(0x03c4))

/* Port P9 */
#define P9  (*(volatile union st_p9 *)(0x03c5))

/* Port P8 direction register */
#define PD8  (*(volatile union st_pd8 *)(0x03c6))

/* Port P9 direction register */
#define PD9  (*(volatile union st_pd9 *)(0x03c7))

/* Port P10 */
#define P10  (*(volatile union st_p10 *)(0x03c8))

/* Port P11 */
#define P11  (*(volatile union st_p11 *)(0x03c9))

/* Port P10 direction register */
#define PD10  (*(volatile union st_pd10 *)(0x03ca))

/* Port P11 direction register */
#define PD11  (*(volatile union st_pd11 *)(0x03cb))

/* Port P12 */
#define P12  (*(volatile union st_p12 *)(0x03cc))

/* Port P13 */
#define P13  (*(volatile union st_p13 *)(0x03cd))

/* Port P12 direction register */
#define PD12  (*(volatile union st_pd12 *)(0x03ce))

/* Port P13 direction register */
#define PD13  (*(volatile union st_pd13 *)(0x03cf))

/* Port P14 */
#define P14  (*(volatile union st_p14 *)(0x03d0))

/* Port P15 */
#define P15  (*(volatile union st_p15 *)(0x03d1))

/* Port P14 direction register */
#define PD14  (*(volatile union st_pd14 *)(0x03d2))

/* Port P15 direction register */
#define PD15  (*(volatile union st_pd15 *)(0x03d3))

/* Pull-up control register 2 */
#define PUR2  (*(volatile union st_pur2 *)(0x03da))

/* Pull-up control register 3 */
#define PUR3  (*(volatile union st_pur3 *)(0x03db))

/* Pull-up control register 4 */
#define PUR4  (*(volatile union st_pur4 *)(0x03dc))

/* Port P0 */
#define P0  (*(volatile union st_p0 *)(0x03e0))

/* Port P1 */
#define P1  (*(volatile union st_p1 *)(0x03e1))

/* Port P0 direction register */
#define PD0  (*(volatile union st_pd0 *)(0x03e2))

/* Port P1 direction register */
#define PD1  (*(volatile union st_pd1 *)(0x03e3))

/* Port P2 */
#define P2  (*(volatile union st_p2 *)(0x03e4))

/* Port P3 */
#define P3  (*(volatile union st_p3 *)(0x03e5))

/* Port P2 direction register */
#define PD2  (*(volatile union st_pd2 *)(0x03e6))

/* Port P3 direction register */
#define PD3  (*(volatile union st_pd3 *)(0x03e7))

/* Port P4 */
#define P4  (*(volatile union st_p4 *)(0x03e8))

/* Port P5 */
#define P5  (*(volatile union st_p5 *)(0x03e9))

/* Port P4 direction register */
#define PD4  (*(volatile union st_pd4 *)(0x03ea))

/* Port P5 direction register */
#define PD5  (*(volatile union st_pd5 *)(0x03eb))

/* Pull-up control register 0 */
#define PUR0  (*(volatile union st_pur0 *)(0x03f0))

/* Pull-up control register 1 */
#define PUR1  (*(volatile union st_pur1 *)(0x03f1))

/* Port control register */
#define PCR  (*(volatile union st_pcr *)(0x03ff))

#endif


