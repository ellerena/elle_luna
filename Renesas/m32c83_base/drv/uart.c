/*
 * uart.c
 *
 *  Created on: Jan 27, 2018
 *      Author: Eddie.Llerena
 */

#include <stdint.h>

#include "iom32c83.h"
#include "skp_bsp.h"
#include "uart.h"

uint8_t gComIn[GCOMINSZ+1] =  "#0123456\0";
uint8_t * const acArgs = gComIn + 2;

#define	U1BRG_VAL		(10)	//(f1_CLK_SPEED/(16*115200)-1) fails so we'll aprox. manually

void COM_init(void)
{
	unsigned char dummy;

/*  Configure Uart1 for 19200 baud, 8 data bits, 1 stop bit, no parity */

  	U1MR.BYTE = 0x05;		// set mode register
  	U1C0.BYTE = 0x10; 		// set control register 0
  	U1C1.BIT.TE = 0;
  	U1C1.BIT.RE = 0;
  	U1C1.BIT.U1IRS = 0;
  	U1C1.BIT.U1RRM = 0;

	U1BRG = (unsigned char) U1BRG_VAL;	// set bit rate generator

	U1TB.BYTE.LOW = 0;			// clear transmit buffer
  	dummy = U1RB.WORD;		// clear receive buffer by reading

	S1RIC.BYTE = 0x03;		// Enable UART1 priority level 3
  	U1C1.BIT.TE = 1;
  	U1C1.BIT.RE = 1;
  	PS0.BIT.PS0_6 = 0;        /* Enable RXD1 input */
  	PS0.BIT.PS0_7 = 1;        /* Enable TXD1 output */
  	PD6.BIT.PD6_6 = 0;
}

void COM_send(const uint8_t * s, uint16_t len)
{
   while (len--)                        /* repeat <len> times */
   {
	   U1TB.WORD = *s++;                /* Send the character */
	   while (!U1C1.BIT.TI);            /* wait for TX buffer empty */
   }
}

void COM_putc(unsigned c)
{
	 U1TB.WORD = c;
	 while (!U1C1.BIT.TI);             /* TX buffer empty? */
}

/**
 *	@brief	This function prints a character string to the UART.<br>
 *			It will stop after the first non-printable character -expected to be '\r'.
 *	@param	s is a pointer to the start of the string.
*/
void COM_puts(const uint8_t *s)
{
	uint8_t c;

    do
    {
       c = *s++;
       if (c < 32) c = '\r';
       U1TB.WORD = c;
       while (!U1C1.BIT.TI);             /* TX buffer empty? */
    } while(c != '\r');
}

void COM_TXHex(uint16_t v)
{
   char msg[] = {"x0000 "}, c;
   unsigned i= 4;

   do
   {
      c = v & 0x0f;                         /* extract less significant nibble for processing */
      msg[i] = c < 0x0a ? c+0x30 : c+0x37;  /* convert the nibble into a printable hex */
      v>>=4;                                /* extract more significant nibble for processing */
   } while(--i);

   COM_send((uint8_t*)msg, 6);                        /* send the whole thing for printing */
}
/* End user code. Do not edit comment generated here */
