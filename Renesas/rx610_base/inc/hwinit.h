#ifndef _HARDWARE_SETUP_H
#define _HARDWARE_SETUP_H

#define LED_ON		0					// LED lights. 
#define LED_OFF		1					// LED is turned off. 


#define led_0_port	PORT8.DR.BIT.B3		// Port data of LED0 (P83)
#define led_1_port	PORT8.DR.BIT.B4		// Port data of LED1 (P84)
#define led_2_port	PORT3.DR.BIT.B3		// Port data of LED2 (P33)
#define led_3_port	PORT3.DR.BIT.B6		// Port data of LED3 (P36)

#define led_0_port_dir	PORT8.DDR.BIT.B3	// Port direction of LED0(P83) 
#define led_1_port_dir	PORT8.DDR.BIT.B4	// Port direction of LED1(P84)
#define led_2_port_dir	PORT3.DDR.BIT.B3	// Port direction of LED2(P33)
#define led_3_port_dir	PORT3.DDR.BIT.B6	// Port direction of LED3(P34)

#endif