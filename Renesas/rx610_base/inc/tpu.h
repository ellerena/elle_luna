#ifndef TPU_H
#define TPU_H

void InitTPU2(void);
void StartTPU2(void);
void StopTPU2(void);

void InitTPU1(void);
void StartTPU1(void);
void StopTPU1(void);

void InitTPU0(void);
void StartTPU0(void);
void StopTPU0(void);

#endif



