#ifndef UART_H
#define UART_H

#define RXBUFSZ		64
#define CRLF		"\n\r"
#define FCOMRX		(1 << 0)
#define	FCNT		(1 << 2)
#define FBRGT		(1 << 3)
#define FBDWN		(1 << 4)
#define FBLFT		(1 << 5)
#define FBUP		(1 << 6)
#define FKEY		(1 << 7)
#define FDIRS		(FBRGT | FBDWN | FBLFT | FBUP | FCNT)
#define BAUDIOS		115200

extern unsigned char gComIn[];
extern volatile unsigned int gFlags;

void COM_init(void);
void COM_puts(const char *s);
void COM_putc(char c);
void COM_TXHex(unsigned v);

#endif



