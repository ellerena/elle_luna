/*
	Copyright (C) 2010 DJ Delorie <dj@redhat.com>

	This file is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 3, or (at your option) any later
	version.

	This file is distributed in the hope that it will be useful, but WITHOUT ANY
	WARRANTY; without even the implied warranty of MERCHANTABILITY or
	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
	for more details.

	You should have received a copy of the GNU General Public License
	along with this file; see the file COPYING3.  If not see
	<http://www.gnu.org/licenses/>.
*/

#include <stdint.h>
#include "inthandler.h"
#include "iodefine.h"
#include "uart.h"

/* This isn't as efficient as an interrupt-driven source; there's an
	 extra "stop" bit in the transmission stream because we can't set
	 the TX data register soon enough.  However, we can't set the baud
	 rate for 115200 accurately enough, and the extra stop bit keeps the
	 host from getting out of sync.  */

#define	BRR_N(b)	((48*1e6)/(16*b) - 1)

unsigned char gComIn[RXBUFSZ] = {'i'};

static void COM_init_port (void)
{
	int i;

	SCI4.SCR.BYTE = 0x00;		/* Disable all */
	SCI4.SMR.BYTE = 0x00;		/* uart 8N1, PCLK (50MHz) */
	SCI4.SCMR.BYTE = 0x00;		/* dis. smart card, enable uart, LSB 1st */
	SCI4.SEMR.BYTE = 0x10;		/* div-8 mode in BRR */
	SCI4.BRR = BRR_N(BAUDIOS);	/* Calculate baud rate. */

	for (i=1100; i; --i) asm("");

	SCI4.SCR.BYTE = 0x70;		/* Enable TX, RX, RX irq */
}

void COM_init(void)
{
	PORT0.ICR.BYTE = 0x20;		/* Enable SCI4 input buffer @P0.5 */

	COM_init_port();

	ICU.IER[0x1C].BIT.IEN7 = 1;
	ICU.IPR[0x84].BYTE = 5;
}

void COM_putc (char c)
{
	while (!SCI4.SSR.BIT.TEND) ;	/* TEND = 0:transmitting, 1:done */
	SCI4.TDR = c;
}

/**
 *	@brief	This function prints a character string to the UART.<br>
 *			It will stop after the first non-printable character -expected to be '\r'.
 *	@param	s is a pointer to the start of the string.
*/
void COM_puts (const char *s)
{
	char c;

    do
    {
       c = *s++;
       if (c < '\r') c = '\r';
       COM_putc (c);
    } while(c != '\r');
}

void COM_TXHex(unsigned int v)
{
   char msg[] = {"x00000000"}, c;
   unsigned i= 8;

   do
   {
      c = v & 0x0f;                         /* extract less significant nibble for processing */
      msg[i] = c < 0x0a ? c+0x30 : c+0x37;  /* convert the nibble into a printable hex */
      v>>=4;                                /* extract more significant nibble for processing */
   } while(--i);

   COM_puts(msg);                        /* send the whole thing for printing */
}

int write (int fd, char *buf, int len)
{
	int ilen = len;

	while (len--)
		COM_putc (*buf++);

	return ilen;
}

void __attribute__ ((interrupt)) INT_Excep_SCI4_RXI4(void)
{
	uint8_t	t;
	static uint8_t *pacCommand = gComIn;					/* pointer used to write characters recvd by UART1 */

	t = SCI4.RDR;
	*pacCommand++ = t;
	if (t < 32)										/* <return> ends the uart command entry */
	{
		pacCommand = gComIn;
		gFlags |= FCOMRX;							/* rise a flag to alert main() */
	}
}
