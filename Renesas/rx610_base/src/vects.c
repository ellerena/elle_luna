/***********************************************************************/
/*                                                                     */
/*  FILE        :vects.c                                               */
/*  DATE        :Fri, Nov 27, 2009                                     */
/*  DESCRIPTION :Vector Table                                          */
/*  CPU TYPE    :RX610                                                 */
/*                                                                     */
/***********************************************************************/

#include "inthandler.h"

typedef void (*fp) (void);
extern void start(void);
extern void stack (void);

#define FVECT_SECT          __attribute__ ((section (".fvectors")))

const fp HardwareVectors[] FVECT_SECT  = {
//;0xffffff80  Reserved
    (fp)0,
//;0xffffff84  Reserved
    (fp)0,
//;0xffffff88  Reserved
    (fp)0,
//;0xffffff8C  Reserved
    (fp)0,
//;0xffffff90  Reserved
    (fp)0,
//;0xffffff94  Reserved
    (fp)0,
//;0xffffff98  Reserved
    (fp)0,
//;0xffffff9C  Reserved
    (fp)0,
//;0xffffffA0  Reserved
    (fp)0,
//;0xffffffA4  Reserved
    (fp)0,
//;0xffffffA8  Reserved
    (fp)0,
//;0xffffffAC  Reserved
    (fp)0,
//;0xffffffB0  Reserved
    (fp)0,
//;0xffffffB4  Reserved
    (fp)0,
//;0xffffffB8  Reserved
    (fp)0,
//;0xffffffBC  Reserved
    (fp)0,
//;0xffffffC0  Reserved
    (fp)0,
//;0xffffffC4  Reserved
    (fp)0,
//;0xffffffC8  Reserved
    (fp)0,
//;0xffffffCC  Reserved
    (fp)0,
//;0xffffffd0  Exception(Supervisor Instruction)
    INT_Excep_SuperVisorInst,
//;0xffffffd4  Reserved
    (fp)0,
//;0xffffffd8  Reserved
    (fp)0,
//;0xffffffdc  Exception(Undefined Instruction)
    INT_Excep_UndefinedInst,
//;0xffffffe0  Reserved
    (fp)0,
//;0xffffffe4  Exception(Floating Point)
    INT_Excep_FloatingPoint,
//;0xffffffe8  Reserved
    (fp)0,
//;0xffffffec  Reserved
    (fp)0,
//;0xfffffff0  Reserved
    (fp)0,
//;0xfffffff4  Reserved
    (fp)0,
//;0xfffffff8  NMI
    INT_NonMaskableInterrupt,
//;0xfffffffc  RESET
//;<<VECTOR DATA START (POWER ON RESET)>>
//;Power On Reset PC
    start                                                                                                                 
//;<<VECTOR DATA END (POWER ON RESET)>>
};

#define RVECT_SECT          __attribute__ ((section (".rvectors")))

const fp RelocatableVectors[] RVECT_SECT  = {
//;0x0000  Reserved; 0
	(fp)0,
//;0x0004  Reserved; 1
	(fp)0,
//;0x0008  Reserved; 2
	(fp)0,
//;0x000C  Reserved; 3
	(fp)0,
//;0x0010  Reserved; 4
	(fp)0,
//;0x0014  Reserved; 5
	(fp)0,
//;0x0018  Reserved; 6
	(fp)0,
//;0x001C  Reserved; 7
	(fp)0,
//;0x0020  Reserved; 8
	(fp)0,
//;0x0024  Reserved; 9
	(fp)0,
//;0x0028  Reserved; 10
	(fp)0,
//;0x002C  Reserved; 11
	(fp)0,
//;0x0030  Reserved; 12
	(fp)0,
//;0x0034  Reserved; 13
	(fp)0,
//;0x0038  Reserved; 14
	(fp)0,
//;0x003C  Reserved; 15
	(fp)0,
//;0x0040  BUSERR; 16
	(fp)INT_Excep_BUSERR,
//;0x0044  Reserved; 17
	(fp)0,
//;0x0048  Reserved; 18
	(fp)0,
//;0x004C  Reserved; 19
	(fp)0,
//;0x0050  Reserved; 20
	(fp)0,
//;0x0054 FCUERR; 21
	(fp)INT_Excep_FCU_FCUERR,
//;0x0058  Reserved; 22
	(fp)0,
//;0x005C  FRDYI; 23
	(fp)INT_Excep_FCU_FRDYI,
//;0x0060  Reserved; 24
	(fp)0,
//;0x0064  Reserved; 25
	(fp)0,
//;0x0068  Reserved; 26
	(fp)0,
//;0x006C  Reserved; 27
	(fp)0,
//;0x0070  CMTU0_CMT0; 28
	(fp)INT_Excep_CMTU0_CMT0,
//;0x0074  CMTU0_CMT1; 29
	(fp)INT_Excep_CMTU0_CMT1,
//;0x0078  CMTU1_CMT2; 30
	(fp)INT_Excep_CMTU1_CMT2,
//;0x007C  CMTU1_CMT3; 31
	(fp)INT_Excep_CMTU1_CMT3,
//;0x0080  Reserved; 32
	(fp)0,
//;0x0084  Reserved; 33
	(fp)0,
//;0x0088  Reserved; 34
	(fp)0,
//;0x008C  Reserved; 35
	(fp)0,
//;0x0090  Reserved; 36
	(fp)0,
//;0x0094  Reserved; 37
	(fp)0,
//;0x0098  Reserved; 38
	(fp)0,
//;0x009C  Reserved; 39
	(fp)0,
//;0x00A0  Reserved; 40
	(fp)0,
//;0x00A4  Reserved; 41
	(fp)0,
//;0x00A8  Reserved; 42
	(fp)0,
//;0x00AC  Reserved; 43
	(fp)0,
//;0x00B0  Reserved; 44
	(fp)0,
//;0x00B4  Reserved; 45
	(fp)0,
//;0x00B8  Reserved; 46
	(fp)0,
//;0x00BC  Reserved; 47
	(fp)0,
//;0x00C0  Reserved; 48
	(fp)0,
//;0x00C4  Reserved; 49
	(fp)0,
//;0x00C8  Reserved; 50
	(fp)0,
//;0x00CC  Reserved; 51
	(fp)0,
//;0x00D0  Reserved; 52
	(fp)0,
//;0x00D4  Reserved; 53
	(fp)0,
//;0x00D8  Reserved; 54
	(fp)0,
//;0x00DC  Reserved; 55
	(fp)0,
//;0x00E0  Reserved; 56
	(fp)0,
//;0x00E4  Reserved; 57
	(fp)0,
//;0x00E8  Reserved; 58
	(fp)0,
//;0x00EC  Reserved; 59
	(fp)0,
//;0x00F0  Reserved; 60
	(fp)0,
//;0x00F4  Reserved; 61
	(fp)0,
//;0x00F8  Reserved; 62
	(fp)0,
//;0x00FC  Reserved; 63
	(fp)0,
//;0x0100  IRQ0; 64
	(fp)INT_Excep_IRQ0,
//;0x0104 IRQ1; 65
	(fp)INT_Excep_IRQ1,
//;0x0108 IRQ2; 66
	(fp)INT_Excep_IRQ2,
//;0x010C IRQ3; 67
	(fp)INT_Excep_IRQ3,
//;0x0110 IRQ4; 68
	(fp)INT_Excep_IRQ4,
//;0x0114 IRQ5; 69
	(fp)INT_Excep_IRQ5,
//;0x0118 IRQ6; 70
	(fp)INT_Excep_IRQ6,
//;0x011C IRQ7; 71
	(fp)INT_Excep_IRQ7,
//;0x0120 IRQ8; 72
	(fp)INT_Excep_IRQ8,
//;0x0124 IRQ9; 73
	(fp)INT_Excep_IRQ9,
//;0x0128 IRQ10; 74
	(fp)INT_Excep_IRQ10,
//;0x012C IRQ11; 75
	(fp)INT_Excep_IRQ11,
//;0x0130 IRQ12; 76
	(fp)INT_Excep_IRQ12,
//;0x0134 IRQ13; 77
	(fp)INT_Excep_IRQ13,
//;0x0138 IRQ14; 78
	(fp)INT_Excep_IRQ14,
//;0x013C IRQ15; 79
	(fp)INT_Excep_IRQ15,
//;0x0140  Reserved; 80
	(fp)0,
//;0x0144  Reserved; 81
	(fp)0,
//;0x0148  Reserved; 82
	(fp)0,
//;0x014C  Reserved; 83
	(fp)0,
//;0x0150  Reserved; 84
	(fp)0,
//;0x0154  Reserved; 85
	(fp)0,
//;0x0158  Reserved; 86
	(fp)0,
//;0x015C  Reserved; 87
	(fp)0,
//;0x0160  Reserved; 88
	(fp)0,
//;0x0164  Reserved; 89
	(fp)0,
//;0x0168  Reserved; 90
	(fp)0,
//;0x016C  Reserved; 91
	(fp)0,
//;0x0170  Reserved; 92
	(fp)0,
//;0x0174  Reserved; 93
	(fp)0,
//;0x0178  Reserved; 94
	(fp)0,
//;0x017C  Reserved; 95
	(fp)0,
//;0x0180  WDT_WOVI; 96
	(fp)INT_Excep_WDT_WOVI,
//;0x0184  Reserved; 97
	(fp)0,
//;0x0188  AD0_ADI0; 98
	(fp)INT_Excep_AD0_ADI0,
//;0x018C  AD1_ADI1; 99
	(fp)INT_Excep_AD1_ADI1,
//;0x0190  AD2_ADI2; 100
	(fp)INT_Excep_AD2_ADI2,
//;0x0194  AD3_ADI3; 101
	(fp)INT_Excep_AD3_ADI3,
//;0x0198  Reserved; 102
	(fp)0,
//;0x019C  Reserved; 103
	(fp)0,
//;0x01A0  TPU0_TGI0A; 104
	(fp)INT_Excep_TPU0_TGI0A,
//;0x01A4  TPU0_TGI0B; 105
	(fp)INT_Excep_TPU0_TGI0B,
//;0x01A8  TPU0_TGI0C; 106
	(fp)INT_Excep_TPU0_TGI0C,
//;0x01AC  TPU0_TGI0D; 107
	(fp)INT_Excep_TPU0_TGI0D,
//;0x01B0  TPU0_TCI0V; 108
	(fp)INT_Excep_TPU0_TCI0V,
//;0x01B4  Reserved; 109
	(fp)0,
//;0x01B8  Reserved; 110
	(fp)0,
//;0x01BC  TPU1_TGI1A; 111
	(fp)INT_Excep_TPU1_TGI1A,
//;0x01C0  TPU1_TGI1B; 112
	(fp)INT_Excep_TPU1_TGI1B,
//;0x01C4  Reserved; 113
	(fp)0,
//;0x01C8  Reserved; 114
	(fp)0,
//;0x01CC  TPU1_TCI1V; 115
	(fp)INT_Excep_TPU1_TCI1V,
//;0x01D0  TPU1_TCI1U; 116
	(fp)INT_Excep_TPU1_TCI1U,
//;0x01D4  TPU2_TGI2A; 117
	(fp)INT_Excep_TPU2_TGI2A,
//;0x01D8  TPU2_TGI2B; 118
	(fp)INT_Excep_TPU2_TGI2B,
//;0x01DC  Reserved; 119
	(fp)0,
//;0x01E0  TPU2_TCI2V; 120
	(fp)INT_Excep_TPU2_TCI2V,
//;0x01E4  TPU2_TCI2U; 121
	(fp)INT_Excep_TPU2_TCI2U,
//;0x01E8  TPU3_TGI3A; 122
	(fp)INT_Excep_TPU3_TGI3A,
//;0x01EC  TPU3_TGI3B; 123
	(fp)INT_Excep_TPU3_TGI3B,
//;0x01F0  TPU3_TGI3C; 124
	(fp)INT_Excep_TPU3_TGI3C,
//;0x01F4  TPU3_TGI3D; 125
	(fp)INT_Excep_TPU3_TGI3D,
//;0x01F8  TPU3_TCI3V; 126
	(fp)INT_Excep_TPU3_TCI3V,
//;0x01FC  TPU4_TGI4A; 127
	(fp)INT_Excep_TPU4_TGI4A,
//;0x0200  TPU4_TGI4B; 128
	(fp)INT_Excep_TPU4_TGI4B,
//;0x0204  Reserved; 129
	(fp)0,
//;0x0208  Reserved; 130
	(fp)0,
//;0x020C TPU4_TCI4V; 131
	(fp)INT_Excep_TPU4_TCI4V,
//;0x0210 TPU4_TCI4U; 132
	(fp)INT_Excep_TPU4_TCI4U,
//;0x0214  TPU5_TGI5A; 133
	(fp)INT_Excep_TPU5_TGI5A,
//;0x0218  TPU5_TGI5B; 134
	(fp)INT_Excep_TPU5_TGI5B,
//;0x021C  Reserved; 135
	(fp)0,
//;0x0220  TPU5_TCI5V; 136
	(fp)INT_Excep_TPU5_TCI5V,
//;0x0224  TPU5_TCI5U; 137
	(fp)INT_Excep_TPU5_TCI5U,
//;0x0228  TPU6_TGI6A; 138
	(fp)INT_Excep_TPU6_TGI6A,
//;0x022C  TPU6_TGI6B; 139
	(fp)INT_Excep_TPU6_TGI6B,
//;0x0230  TPU6_TGI6C; 140
	(fp)INT_Excep_TPU6_TGI6C,
//;0x0234  TPU6_TGI6D; 141
	(fp)INT_Excep_TPU6_TGI6D,
//;0x0238  TPU6_TCI6V; 142
	(fp)INT_Excep_TPU6_TCI6V,
//;0x023C  Reserved; 143
	(fp)0,
//;0x0240  Reserved; 144
	(fp)0,
//;0x0244  TPU7_TGI7A; 145
	(fp)INT_Excep_TPU7_TGI7A,
//;0x0248  TPU7_TGI7B; 146
	(fp)INT_Excep_TPU7_TGI7B,
//;0x024C  Reserved; 147
	(fp)0,
//;0x0250  Reserved; 148
	(fp)0,
//;0x0254  TPU7_TCI7V; 149
	(fp)INT_Excep_TPU7_TCI7V,
//;0x0258  TPU7_TCI7U; 150
	(fp)INT_Excep_TPU7_TCI7U,
//;0x025C  TPU8_TGI8A; 151
	(fp)INT_Excep_TPU8_TGI8A,
//;0x0260  TPU8_TGI8B; 152
	(fp)INT_Excep_TPU8_TGI8B,
//;0x0264  Reserved; 153
	(fp)0,
//;0x0268  TPU8_TCI8V; 154
	(fp)INT_Excep_TPU8_TCI8V,
//;0x026C  TPU8_TCI8U; 155
	(fp)INT_Excep_TPU8_TCI8U,
//;0x0270  TPU9_TGI9A; 156
	(fp)INT_Excep_TPU9_TGI9A,
//;0x0274  TPU9_TGI9B; 157
	(fp)INT_Excep_TPU9_TGI9B,
//;0x0278  TPU9_TGI9C; 158
	(fp)INT_Excep_TPU9_TGI9C,
//;0x027C  TPU9_TGI9D; 159
	(fp)INT_Excep_TPU9_TGI9D,
//;0x0280  TPU9_TCI9V; 160
	(fp)INT_Excep_TPU9_TCI9V,
//;0x0284  TPU10_TGI10A; 161
	(fp)INT_Excep_TPU10_TGI10A,
//;0x0288  TPU10_TGI10B; 162
	(fp)INT_Excep_TPU10_TGI10B,
//;0x028C  Reserved; 163
	(fp)0,
//;0x0290  Reserved; 164
	(fp)0,
//;0x0294  TPU10_TCI10V; 165
	(fp)INT_Excep_TPU10_TCI10V,
//;0x0298  TPU10_TCI10U; 166
	(fp)INT_Excep_TPU10_TCI10U,
//;0x029C  TPU11_TGI11A; 167
	(fp)INT_Excep_TPU11_TGI11A,
//;0x02A0  TPU11_TGI11B; 168
	(fp)INT_Excep_TPU11_TGI11B,
//;0x02A4  Reserved; 169
	(fp)0,
//;0x02A8  TPU11_TCI11V; 170
	(fp)INT_Excep_TPU11_TCI11V,
//;0x02AC  TPU11_TCI11U; 171
	(fp)INT_Excep_TPU11_TCI11U,
//;0x02B0  Reserved; 172
	(fp)0,
//;0x02B4  Reserved; 173
	(fp)0,
//;0x02B8  TMR0_CMI0A; 174
	(fp)INT_Excep_TMR0_CMI0A,
//;0x02BC  TMR0_CMI0B; 175
	(fp)INT_Excep_TMR0_CMI0B,
//;0x02C0  TMR0_OV0I; 176
	(fp)INT_Excep_TMR0_OV0I,
//;0x02C4  TMR1_CMI1A; 177
	(fp)INT_Excep_TMR1_CMI1A,
//;0x02C8  TMR1_CMI1B; 178
	(fp)INT_Excep_TMR1_CMI1B,
//;0x02CC  TMR1_OV1I; 179
	(fp)INT_Excep_TMR1_OV1I,
//;0x02D0 TMR2_CMI2A; 180
	(fp)INT_Excep_TMR2_CMI2A,
//;0x02D4  TMR2_CMI2B; 181
	(fp)INT_Excep_TMR2_CMI2B,
//;0x02D8  TMR2_OV2I; 182
	(fp)INT_Excep_TMR2_OV2I,
//;0x02DC  TMR3_CMI3A; 183
	(fp)INT_Excep_TMR3_CMI3A,
//;0x02E0 TMR3_CMI3B; 184
	(fp)INT_Excep_TMR3_CMI3B,
//;0x02E4  TMR3_OV3I; 185
	(fp)INT_Excep_TMR3_OV3I,
//;0x02E8  Reserved; 186
	(fp)0,
//;0x02EC  Reserved; 187
	(fp)0,
//;0x02F0  Reserved; 188
	(fp)0,
//;0x02F4  Reserved; 189
	(fp)0,
//;0x02F8  Reserved; 190
	(fp)0,
//;0x02FC  Reserved; 191
	(fp)0,
//;0x0300  Reserved; 192
	(fp)0,
//;0x0304  Reserved; 193
	(fp)0,
//;0x0308  Reserved; 194
	(fp)0,
//;0x030C  Reserved; 195
	(fp)0,
//;0x0310  Reserved; 196
	(fp)0,
//;0x0314  Reserved; 197
	(fp)0,
//;0x0318  DMAC_DMTEND0; 198
	(fp)INT_Excep_DMAC_DMTEND0,
//;0x031C  DMAC_DMTEND1; 199
	(fp)INT_Excep_DMAC_DMTEND1,
//;0x0320  DMAC_DMTEND2; 200
	(fp)INT_Excep_DMAC_DMTEND2,
//;0x0324  DMAC_DMTEND3; 201
	(fp)INT_Excep_DMAC_DMTEND3,
//;0x0328  Reserved; 202
	(fp)0,
//;0x032C  Reserved; 203
	(fp)0,
//;0x0330  Reserved; 204
	(fp)0,
//;0x0334  Reserved; 205
	(fp)0,
//;0x0338  Reserved; 206
	(fp)0,
//;0x033C  Reserved; 207
	(fp)0,
//;0x0340  Reserved; 208
	(fp)0,
//;0x0344  Reserved; 209
	(fp)0,
//;0x0348  Reserved; 210
	(fp)0,
//;0x034C  Reserved; 211
	(fp)0,
//;0x0350  Reserved; 212
	(fp)0,
//;0x0354  Reserved; 213
	(fp)0,
//;0x0358  SCI0_ERI0; 214
	(fp)INT_Excep_SCI0_ERI0,
//;0x035C  SCI0_RXI0; 215
	(fp)INT_Excep_SCI0_RXI0,
//;0x0360  SCI0_TXI0; 216
	(fp)INT_Excep_SCI0_TXI0,
//;0x0364  SCI0_TEI0; 217
	(fp)INT_Excep_SCI0_TEI0,
//;0x0368  SCI1_ERI1; 218
	(fp)INT_Excep_SCI1_ERI1,
//;0x036C  SCI1_RXI1; 219
	(fp)INT_Excep_SCI1_RXI1,
//;0x0370  SCI1_TXI1; 220
	(fp)INT_Excep_SCI1_TXI1,
//;0x0374  SCI1_TEI1; 221
	(fp)INT_Excep_SCI1_TEI1,
//;0x0378  SCI2_ERI2; 222
	(fp)INT_Excep_SCI2_ERI2,
//;0x037C  SCI2_RXI2; 223
	(fp)INT_Excep_SCI2_RXI2,
//;0x0380  SCI2_TXI2; 224
	(fp)INT_Excep_SCI2_TXI2,
//;0x0384  SCI2_TEI2; 225
	(fp)INT_Excep_SCI2_TEI2,
//;0x0388  SCI3_ERI3; 226
	(fp)INT_Excep_SCI3_ERI3,
//;0x038C  SCI3_RXI3; 227
	(fp)INT_Excep_SCI3_RXI3,
//;0x0390  SCI3_TXI3; 228
	(fp)INT_Excep_SCI3_TXI3,
//;0x0394  SCI3_TEI3; 229
	(fp)INT_Excep_SCI3_TEI3,
//;0x0398  SCI4_ERI4; 230
	(fp)INT_Excep_SCI4_ERI4,
//;0x039C  SCI4_RXI4; 231
	(fp)INT_Excep_SCI4_RXI4,
//;0x03A0  SCI4_TXI4; 232
	(fp)INT_Excep_SCI4_TXI4,
//;0x03A4  SCI4_TEI4; 233
	(fp)INT_Excep_SCI4_TEI4,
//;0x03A8  SCI5_ERI5; 234
	(fp)INT_Excep_SCI5_ERI5,
//;0x03AC  SCI5_RXI5; 235
	(fp)INT_Excep_SCI5_RXI5,
//;0x03B0  SCI5_TXI5; 236
	(fp)INT_Excep_SCI5_TXI5,
//;0x03B4  SCI5_TEI5; 237
	(fp)INT_Excep_SCI5_TEI5,
//;0x03B8  SCI6_ERI6; 238
	(fp)INT_Excep_SCI6_ERI6,
//;0x03BC  SCI6_RXI6; 239
	(fp)INT_Excep_SCI6_RXI6,
//;0x03C0  SCI6_TXI6; 240
	(fp)INT_Excep_SCI6_TXI6,
//;0x03C4  SCI6_TEI6; 241
	(fp)INT_Excep_SCI6_TEI6,
//;0x03C8  Reserved; 242
	(fp)0,
//;0x03CC  Reserved; 243
	(fp)0,
//;0x03D0  Reserved; 244
	(fp)0,
//;0x03D4  Reserved; 245
	(fp)0,
//;0x03D8  RIIC0_EEI0; 246
	(fp)INT_Excep_RIIC0_EEI0,
//;0x03DC  RIIC0_RXI0; 247
	(fp)INT_Excep_RIIC0_RXI0,
//;0x03E0  RIIC0_TXI0; 248
	(fp)INT_Excep_RIIC0_TXI0,
//;0x03E4  RIIC0_TEI0; 249
	(fp)INT_Excep_RIIC0_TEI0,
//;0x03E8  RIIC1_EEI1; 250
	(fp)INT_Excep_RIIC1_EEI1,
//;0x03EC  RIIC1_RXI1; 251
	(fp)INT_Excep_RIIC1_RXI1,
//;0x03F0  RIIC1_TXI1; 252
	(fp)INT_Excep_RIIC1_TXI1,
//;0x03F4  RIIC1_TEI1; 253
	(fp)INT_Excep_RIIC1_TEI1,
//;0x03F8  Reserved; 254
	(fp)0,
//;0x03FC  Reserved; 255
	(fp)0,
};
