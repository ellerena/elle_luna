#include "iodefine.h"
#include "tpu.h"
#include "inthandler.h"
#include "hwinit.h"

/* TIMER define */
#define TGRA0_VALUE		15625	/* 1/50MHz * 64 * 15625 = 20.0 ms 		*/		
#define TGRA1_VALUE		39062	/* 1/50MHz * 64 * 39062 = 49.99936 ms 	*/		
#define TGRA2_VALUE		39062	/* 1/50MHz * 1024 * 39062 = 799.98 ms 	*/		

/******************************************************************************/
/*																			  */
/* TPU2																		  */
/*																			  */
/******************************************************************************/
void InitTPU2(void)
{
	SYSTEM.MSTPCRA.BIT.MSTPA13 = 0;		/* Take TPU Unit 0 out of Module Stop Mode 		*/
	
	/* TPU0 setting for LED timimg 														*/
	TPU2.TCR.BYTE = 0x27;				/* Clear TCNT when TGRA match, P/1024 phi			*/
	TPU2.TMDR.BYTE = 0x02;				/* PWM mode 1 */
	
	TPU2.TIOR.BYTE = 0x03;
	
	TPU2.TCNT = 0;
	TPU2.TGRA = TGRA2_VALUE;
	TPU2.TGRA = TGRA2_VALUE / 2;

	/* Enable Interrupt */
	TPU2.TIER.BYTE = 0x01;
		
	/* Set up the Interrupt Control Unit for TPU2 TGRA									*/
	ICU.IPR[80].BYTE = 0x03;				/* Interrupt Priority Register 					*/
										/* 	0: lowest (interrupt will not be taken
											7: highest									*/
	/* The TPU is used to trigger the DMAC
		This setting is done as part of the DMAC initialisation process. See dmac.c		*/
	ICU.ISELR[117].BIT.ISEL = 0;		/* Interrupt Destination Setting 				*/
										/*	0: The request is sent to the CPU.
											1: The request activates the DTC (passed to
												CPU on completion of data transfer).
											2: The request activates the DMAC.	
											3: The request activates the DMAC (passed to
												CPU on completion of data transfer).	*/

	ICU.IER[14].BIT.IEN5 = 1;				/* Interrupt Request Enable Register 			*/
	ICU.IR[117].BIT.IR = 0;				/* Ensure that the flag is set to 0, otherwise 
											the interrupt will not be accepted */
}

void StartTPU2(void)
{
	TPUA.TSTR.BIT.CST2 = 1;		/* Start TPU0 	*/
}

void StopTPU2(void)
{
	TPUA.TSTR.BIT.CST2 = 0;		/* Stop TPU0	*/
}

void INT_Excep_TPU2_TGI2A(void)
{
	led_2_port = 1 - led_2_port;
}

/******************************************************************************/
/*																			  */
/* TPU1																		  */
/*																			  */
/******************************************************************************/
void InitTPU1(void)
{
	SYSTEM.MSTPCRA.BIT.MSTPA13 = 0;		/* Take TPU Unit 0 out of Module Stop Mode 		*/
	
	/* TPU0 setting for LED timimg 														*/
	TPU1.TCR.BYTE = 0x23;				/* Clear TCNT when TGRA match, P/64 phi			*/
	TPU1.TMDR.BYTE = 0;
	TPU1.TCNT = 0;
	TPU1.TGRA = TGRA1_VALUE;

	/* Enable Interrupt */
	TPU1.TIER.BYTE = 0x01;
	
	/* Set up the Interrupt Control Unit for TPU0 TGRA									*/
	ICU.IPR[78].BYTE = 0x03;				/* Interrupt Priority Register 					*/
										/* 	0: lowest (interrupt will not be taken
											7: highest									*/
	/* The TPU is used to trigger the DMAC
		This setting is done as part of the DMAC initialisation process. See dmac.c		*/
	ICU.ISELR[111].BIT.ISEL = 0;		/* Interrupt Destination Setting 				*/
										/*	0: The request is sent to the CPU.
											1: The request activates the DTC (passed to
												CPU on completion of data transfer).
											2: The request activates the DMAC.	
											3: The request activates the DMAC (passed to
												CPU on completion of data transfer).	*/

	ICU.IER[13].BIT.IEN7 = 1;				/* Interrupt Request Enable Register 			*/
	ICU.IR[111].BIT.IR = 0;				/* Ensure that the flag is set to 0, otherwise 
											the interrupt will not be accepted			*/
}

void StartTPU1(void)
{
	TPUA.TSTR.BIT.CST1 = 1;;		// Start TPU1
}

void StopTPU1(void)
{
	TPUA.TSTR.BIT.CST1 = 0;		// Stop TPU1
}

void INT_Excep_TPU1_TGI1A(void)
{						
	led_1_port = 1 - led_1_port;
}

/******************************************************************************/
/*																			  */
/* TPU0																		  */
/*																			  */
/******************************************************************************/
void InitTPU0(void)
{
	SYSTEM.MSTPCRA.BIT.MSTPA13 = 0;		/* Take TPU Unit 0 out of Module Stop Mode 		*/
	
	/* TPU0 setting for LED timimg 														*/
	TPU0.TCR.BYTE = 0x23;				/* Clear TCNT when TGRA match, P/64 phi			*/
	TPU0.TMDR.BYTE = 0;
	TPU0.TCNT = 0;
	TPU0.TGRA = TGRA0_VALUE;

	/* Enable Interrupt */
	TPU0.TIER.BYTE = 0x01;
	
	/* Set up the Interrupt Control Unit for TPU0 TGRA									*/
	ICU.IPR[76].BYTE = 0x04;				/* Interrupt Priority Register 					*/
										/* 	0: lowest (interrupt will not be taken
											7: highest									*/
	/* The TPU is used to trigger the DMAC
		This setting is done as part of the DMAC initialisation process. See dmac.c		*/
	ICU.ISELR[104].BIT.ISEL = 0;		/* Interrupt Destination Setting 				*/
										/*	0: The request is sent to the CPU.
											1: The request activates the DTC (passed to
												CPU on completion of data transfer).
											2: The request activates the DMAC.	
											3: The request activates the DMAC (passed to
												CPU on completion of data transfer).	*/

	ICU.IER[13].BIT.IEN0 = 1;				/* Interrupt Request Enable Register 			*/
	ICU.IR[104].BIT.IR = 0;				/* Ensure that the flag is set to 0, otherwise 
											the interrupt will not be accepted			*/
}

void StartTPU0(void)
{
	TPUA.TSTR.BIT.CST0 = 1;	// Start TPU0
}

void StopTPU0(void)
{
	TPUA.TSTR.BIT.CST0 = 0;	// Stop TPU0
}

// TGRA0
void INT_Excep_TPU0_TGI0A(void)
{						
	led_0_port = 1 - led_0_port;
}
