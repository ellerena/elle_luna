/****************************************************************
KPIT Cummins Infosystems Ltd, Pune, India. - 05-Sept-2005.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

*****************************************************************/


void INT_NMI(void)  __attribute__ ((interrupt_handler));
void INT_TRAP0(void)  __attribute__ ((interrupt_handler));
void INT_TRAP1(void)  __attribute__ ((interrupt_handler));
void INT_TRAP2(void)  __attribute__ ((interrupt_handler));
void INT_TRAP3(void)  __attribute__ ((interrupt_handler));
void INT_ABRK(void)  __attribute__ ((interrupt_handler));
void INT_SLEEP(void)  __attribute__ ((interrupt_handler));
void INT_IRQ0(void)  __attribute__ ((interrupt_handler));
void INT_IRQ1(void)  __attribute__ ((interrupt_handler));
void INT_IRQ2(void)  __attribute__ ((interrupt_handler));
void INT_IRQ3(void)  __attribute__ ((interrupt_handler));
void INT_WKP(void)  __attribute__ ((interrupt_handler));
void INT_RTC(void)  __attribute__ ((interrupt_handler));
void INT_TimerV(void)  __attribute__ ((interrupt_handler));
void INT_SCI3(void)  __attribute__ ((interrupt_handler));
void INT_IIC2(void)  __attribute__ ((interrupt_handler));
void INT_ADI(void)  __attribute__ ((interrupt_handler));
void INT_TimerZ0(void)  __attribute__ ((interrupt_handler));
void INT_TimerZ1(void)  __attribute__ ((interrupt_handler));
void INT_TimerB1(void)  __attribute__ ((interrupt_handler));
void INT_SCI3_2(void)  __attribute__ ((interrupt_handler));
void INT_OSCI(void)  __attribute__ ((interrupt_handler));

