/****************************************************************
KPIT Cummins Infosystems Ltd, Pune, India. - 05-Sept-2005.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

*****************************************************************/

#include "inthandler.h"

extern void start (void);     /* Startup code (in start.asm)  */

/**----------------------------------
**  Typedef for the function pointer
**-----------------------------------*/

typedef void (*fp) (void);
#define VECT_SECT          __attribute__ ((section (".vects")))

const fp HardwareVectors[] VECT_SECT = {
start,          /* vector 0 Reset */
(fp)0,			/*Reserved 1*/
(fp)0,			/*Reserved 2*/
(fp)0,			/*Reserved 3*/
(fp)0,			/*Reserved 4*/
(fp)0,			/*Reserved 5*/
(fp)0,			/*Reserved 6*/
INT_NMI,
INT_TRAP0,
INT_TRAP1,
INT_TRAP2,
INT_TRAP3,
INT_ABRK,
INT_SLEEP,
INT_IRQ0,
INT_IRQ1,
INT_IRQ2,
INT_IRQ3,
INT_WKP,
INT_RTC,
(fp)0,			/*Reserved 20*/
(fp)0,			/*Reserved 21*/
INT_TimerV,
INT_SCI3,
INT_IIC2,
INT_ADI,
INT_TimerZ0,
INT_TimerZ1,
(fp)0,			/*Reserved 28*/
INT_TimerB1,
(fp)0,			/*Reserved 30*/
(fp)0,			/*Reserved 31*/
INT_SCI3_2,
(fp)0,			/*Reserved 33*/
INT_OSCI,
};
