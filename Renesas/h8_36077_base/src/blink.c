/****************************************************************
KPIT Cummins Infosystems Ltd, Pune, India. - 10-Mar-2003.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*****************************************************************/

/*****************************************************************
Sample description:
Simple example file using initialised and non-initialised static
data to show the use of startup code
*****************************************************************/
#include <stdio.h>
#include "iodefine.h"

void DELAY(void)
{
	volatile unsigned long FlashDelay ;

	for( FlashDelay = 0; FlashDelay < 20000; FlashDelay++ )
	 	  	;
}

int main(void)
{
	WDT.TCSRWD.BYTE = 0x10;	/* Disable Watchdog */
	WDT.TCSRWD.BYTE = 0x00;

	IO.PCR5 = (1<<7);    	        // Port 5 Bit 7 is output
	IO.PCR6 = (1<<5);    	        // Port 6 Bit 5 is output

	IO.PDR6.BIT.B5 = 1;
	IO.PDR5.BIT.B7 = 0;

	while(1)
	{
		IO.PDR6.BIT.B5 ^= 1;
		IO.PDR5.BIT.B7 ^= 1;
		DELAY();
	}
	return 0;
	
}
