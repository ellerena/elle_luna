/*
 * uart.h
 *
 *  Created on: Jan 27, 2018
 *      Author: Eddie.Llerena
 */

#ifndef DRV_UART_H_
#define DRV_UART_H_

#define	GCOMINSZ		(8)

extern uint8_t gComIn[];
extern unsigned gFlags;


void COM_init(void);
void COM_send(const uint8_t * s, uint16_t len);
void COM_puts(const uint8_t *s);
void COM_TXHex(uint16_t v);
void COM_putc(unsigned c);

#endif /* DRV_UART_H_ */
