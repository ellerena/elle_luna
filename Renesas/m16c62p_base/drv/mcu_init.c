// tab space = 4
/*********************************************************************
* DISCLAIMER:                                                        *
* The software supplied by Renesas Technology America Inc. is        *
* intended and supplied for use on Renesas Technology products.      *
* This software is owned by Renesas Technology America, Inc. or      *
* Renesas Technology Corporation and is protected under applicable   *
* copyright laws. All rights are reserved.                           *
*                                                                    *
* THIS SOFTWARE IS PROVIDED "AS IS". NO WARRANTIES, WHETHER EXPRESS, *
* IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO IMPLIED 		 *
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE *
* APPLY TO THIS SOFTWARE. RENESAS TECHNOLOGY AMERICA, INC. AND       *
* AND RENESAS TECHNOLOGY CORPORATION RESERVE THE RIGHT, WITHOUT      *
* NOTICE, TO MAKE CHANGES TO THIS SOFTWARE. NEITHER RENESAS          *
* TECHNOLOGY AMERICA, INC. NOR RENESAS TECHNOLOGY CORPORATION SHALL, *
* IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR        *
* CONSEQUENTIAL DAMAGES FOR ANY REASON WHATSOEVER ARISING OUT OF THE *
* USE OR APPLICATION OF THIS SOFTWARE.                               *
*********************************************************************/

/*-----------------------------------------------------------------------------
  FILE NAME: mcu_init.c
-----------
DESCRIPTION: System clock and processor mode initilization
-----------
    DETAILS: For M16C/62P

------------------
 Revision History
------------------
   1.0 Sept 15, 2004
       Initial Version

-----------------------------------------------------------------------------*/
#include "skp_bsp.h"


/**************************************************************************
Name       : mcu_init()   
Parameters : none                   
Returns    : nothing      
Description: The starter kit startup file initializes the clock circuit
             to the main crystal with a divide by 1.  This function also sets
			 the main clock to divide by 1 in case the SKP startup file is not 
			 used.  It then enables the PLL 
     
***************************************************************************/
void mcu_init(void)
{
	 volatile unsigned int count = 0xffff;
  
	/* configure clock for divide by 1 mode */
	PRCR.BIT.PRC0 =1;			/* access to CM0, CM1. CM2, PLC0, PCLKR */
	PRCR.BIT.PRC1 =1;			/* access PM0, PM1, PM2, TB2SC, INVC0, INVC1 */
	CM0.BIT.CM0_7 = 0;			// 0: f1, 1:fC
	CM0.BIT.CM0_6 = 0;			/* 0: use CM16 and CM17, 1: divide by 8 */
	CM0.BIT.CM0_2 = 0;			// 0: f1 presend regardless wait mode, 1: stop clocks in wait mode
	CM1.BYTE = 0x20;			/* CM16,CM17: no divide, main clock on in high drive no PLL*/

    /* configure and switch main clock to PLL at 24MHz */
	PM2.BIT.PM2_0 = 0;			/* SFR access to 2 wait (required for >16 MHz) */
	PLC0.BYTE = 0x91;			// PLL (2X) and turn on

	while(count--);			   /* wait for PLL to stabilize (20mS maximum, 240,000 cycles @12Mhz)
                                   this decrement with no optimization is 12 cycles each */
    CM1.BIT.CM1_1 = 1;			/* switch to PLL */
    							/* Start the 32Khz crystal sub clock */
   	PD8.BIT.PD8_7 = 0;			// setting GPIO to inputs (XCin/XCout)
   	PD8.BIT.PD8_6 = 0;

   	CM0.BIT.CM0_4 = 1;			// Start the 32KHz crystal
    CM0.BIT.CM0_0 = 0;			/* Send f/8... */
    CM0.BIT.CM0_1 = 1;			/* ... to CLKOUT channel */

	PM1.BIT.PM1_0 = 1;			// enable data flash area
    PRCR.BIT.PRC0 = 0;   		/* protect clock control register */
    PRCR.BIT.PRC1 = 0;			// Lock the System Clock Control Register
}
