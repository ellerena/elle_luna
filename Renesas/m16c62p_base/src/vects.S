;/****************************************************************
;KPIT Cummins Infosystems Ltd, Pune, India. 17-June-2005.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;
;*****************************************************************/

; Fixed Vector Table

	.section	.vects,"a",@progbits
	.type	_HardwareVectors, @object
	.size	_HardwareVectors, 36

_HardwareVectors:
	.long	0	;(Undefined Instruction) Interrupt on UND instruction 
	.long	0	;(Overflow) Interrupt on INTO instruction 
	.long	0	;(BRK) If the vector contains FF16, program execution starts from the address shown by the vector in the variable vector table
	.long	0	;(Address match)There is an address-matching interrupt enable bit
	.long	0	;Single step (debugger)
	.long	0	;Watchdog timer
	.long	0	;DBC (debugger)
	.long	0	;(NMI)External interrupt by input to NMI pin
	.long	(_start + 0xFF000000)	; Reset


; Variable Vector Table

	.global	_VariableVectors
	.section	.var_vects,"a",@progbits
	.type	_VariableVectors, @object
	.size	_VariableVectors, 256
_VariableVectors:

	.long	0		; 0
	.long	1		; 1
	.long	2		; 2
	.long	3		; 3
	.long	4		; 4
	.long	5		; 5
	.long	6		; 6
	.long	7		; 7
	.long	8		; 8
	.long	9		; 9
	.long	10		; 10
	.long	11		; 11
	.long	12		; 12
	.long	13		; 13
	.long	14		; 14
	.long	15		; 15
	.long	16		; 16
	.long	17		; 17
	.long	18		; 18
	.long	19		; 19
	.long	_uart1_rx_irq		; 20
	.long	_ta0_irq		; 21
	.long	22		; 22
	.long	23		; 23
	.long	24		; 24
	.long	25		; 25
	.long	26		; 26
	.long	27		; 27
	.long	28		; 28
	.long	29		; 29
	.long	30		; 30
	.long	31		; 31
	.long	32		; 32
	.long	33		; 33
	.long	34		; 34
	.long	35		; 35
	.long	36		; 36
	.long	37		; 37
	.long	38		; 38
	.long	39		; 39
	.long	40		; 40
	.long	41		; 41
	.long	42		; 42
	.long	43		; 43
	.long	44		; 44
	.long	45		; 45
	.long	46		; 46
	.long	47		; 47
	.long	48		; 48
	.long	49		; 49
	.long	50		; 50
	.long	51		; 51
	.long	52		; 52
	.long	53		; 53
	.long	54		; 54
	.long	55		; 55
	.long	56		; 56
	.long	57		; 57
	.long	58		; 58
	.long	59		; 59
	.long	60		; 60
	.long	61		; 61
	.long	62		; 62
	.long	63		; 63

	.end 
