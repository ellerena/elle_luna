/****************************************************************
KPIT Cummins Infosystems Ltd, Pune, India.  17-June-2005.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*****************************************************************/

/* This Code is written for M16C/62P, M30626FHPFP processor */
/* Tested on 3 Diamonds starter kit*/

#include <stdint.h>

#include "skp_bsp.h"
#include "skp_lcd.h"
#include "uart.h"

#define	APP_VER		"v1.0.0.0"
#define	LOGO	"\nSKP16C62P - v.0.0.1\n" __DATE__ " " __TIME__ "\n"
#define TASK		gComIn[0]	/* variable to hold the requested 'task' */
#define	MODE		gComIn[1]	/* variable to hold the 'mode' for the TASK*/

extern void mcu_init(void);

extern uint8_t * const acArgs;

static unsigned ParseCharsToValueHex(char *p);
static int fn_i(void);

unsigned gFlags = 0;
unsigned n, iResult;

void DELAY(void)
{
	volatile unsigned long FlashDelay ;

	for( FlashDelay = 0; FlashDelay < 120000; FlashDelay++ )
	 	  	;
}

void delay256(unsigned int delay)
{
	volatile unsigned int FlashDelay ;

	for( FlashDelay = 0; FlashDelay < delay; FlashDelay++ )
	 	  	;
}

#define	TA0_1SEC	(1024)
#define	KDEL	200

int main(void)
{
	int i;

	mcu_init();					// Initialize MCU clock
 	ENABLE_SWITCHES			 	/* Switch initialization - macro defined in skp_bsp.h */
 	ENABLE_LEDS			 		/* LED initialization - macro defined in skp_bsp.h */
   	PD5.BIT.PD5_7 = 1;

	GRN_LED = 1;
	YLW_LED = 1;
 	RED_LED = 1;

	InitDisplay();				// Initialize LCD

	/* Display Renesas Splash Screen for 2 seconds */
	DisplayString(LCD_LINE1,RENESAS_LOGO);	// Display Renesas bitmapped logo on Line 1
//	DisplayString(LCD_LINE1,"Enavili");	// Display Renesas bitmapped logo on Line 1
	DisplayString(LCD_LINE2,APP_VER);

	TA0MR.BYTE = 0xc0;
	TA0 = TA0_1SEC/2;

	DISABLE_IRQ				// disable irqs before setting irq registers - macro defined in skp_bsp.h
	TA0IC.BYTE = 3;			// Set the timer's interrupt priority to level 2
	TABSR.BIT.TA0S = 1;
	COM_init();
	ENABLE_IRQ				// enable interrupts macro defined in skp_bsp.h

	LCD_write(CTRL_WR, LCD_CURSOR_ON);
	LCD_write(CTRL_WR, (unsigned char)(LCD_HOME_L1 + LCD_LINE1));
	COM_send((const uint8_t*)LOGO, sizeof(LOGO));

	while (1)
	{
		COM_send((const uint8_t*)"\n#", 2);
		while(!gFlags)
		{
			if(!S3)
			{
				for(i = 0; i < KDEL; i++)
				{
					if(!S3) i = 0;
				}
				GRN_LED ^= 1;
			    PRCR.BIT.PRC0 = 1;			/* cycle CLKOUT sources */
				i = (CM0.BYTE & 0x3) + 1;
			    CM0.BIT.CM0_0 = i&1;
			    CM0.BIT.CM0_1 = ((i>>1)&1);
			    PRCR.BIT.PRC0 = 0;
			}
			if(!S2)
			{
				for(i = 0; i < KDEL; i++)
				{
					if(!S2) i = 0;
				}
				YLW_LED ^= 1;
			}
			if(!S1)
			{
				for(i = 0; i < KDEL; i++)
				{
					if(!S1) i = 0;
				}
				RED_LED ^= 1;
			}
		}
		n = ParseCharsToValueHex((char*)gComIn + 2);
		switch (TASK)
		{
			case 'i':
				iResult = fn_i();
				break;
			default:
				iResult = -1;
				break;
		}

		COM_send((const uint8_t*)"-->", 3);
		COM_TXHex(iResult);
		LCD_Hex(iResult);
		LCD_write(CTRL_WR, (unsigned char)(LCD_HOME_L1 + LCD_LINE1) );

		gFlags &= ~1;
		GRN_LED ^= 1;
    }
}


static int fn_i(void)
{
	switch(MODE)
	{
		case 'd':		/* print project ID */
		{
			COM_send((const uint8_t*)LOGO, sizeof(LOGO));
			break;
		}
		case 'r':		/* read memory content from a 16bit address */
		{
			return *(int*)(acArgs[1] + (acArgs[0] << 8));
		}
		case 'w':		/* write a byte to memory */
		{
			uint8_t *x;

			x = (uint8_t*)(acArgs[1] + (acArgs[0] << 8));
			*x = acArgs[2];
			return (int)*x;
		}
		default:
			return -1;
	}
	return 0;
}

/**
	@brief	converts a string of chars representing a vector of hex numbers into a vector of bytes equivalent.
	@param	p contains the address of the first char in the string.
	@retval	the count of numbers comverted.
	@verbatim
			Text entered by a user via the keyboard is in the form of a string of printable chars that represent hex numbers.
			This functions takes that string of chars and extracts the meaningful hex numbers represented in it, then
			it places those numbers (bytes) in an array at the same ram location. Returning the number of numbers
			converted.
	@endverbatim
 */
static unsigned ParseCharsToValueHex(char *p)
{
	char *q, d;
	unsigned i, k;
	uint8_t c;

	q = p;
	k = 0;

	while (*p > 0x1f)										/* Will process until a non printable character is found */
	{
		d = 0;												/* initialize the 'number equivalent value' */
		i = 0;												/* initialize the digit counter (assume 2 digits max per number)*/
		do
		{
			d <<= 4;										/* each digit uses the first 4 bits, so on each loop we must push the others */
			c = *p++;										/* read the new char */
			if ((c > 0x29) && (c < 0x3a))					/* char is between 0 and 9 */
			{
				d += (c - 0x30);							/* add the char equivalen value to our total */
				i++;										/* increment our digit counter */
			}
			if ((c > 0x60 ) && (c < 0x67))					/* char is between 0xa and 0xf */
			{
				d += (c - 0x60 + 0x9);						/* add the char equivalen value to our total */
				i++;										/* increment our digit counter */
			}
		} while (( i == 0 ) || ((*p > 0x2f) && (i < 2)));	/* repeat: if no char found or: if found a space and less than 2 digits */

		if(i)												/* if the digit counter is non-zero then a value has been found */
		{
			*q++ = d;										/* write the number found to memory */
			k++;											/* increment 'detected numbers counter' */
		}
	}

	return k;
}


