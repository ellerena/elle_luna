Program Name: 
blink

Purpose: 
Simple program to blink LED continuously.


Target Board and setting: 
M16C/62P, M30626FHPFP, 3-D Starter kit
Red LEDs connected to Port 2 (all bits)


File List:
README		: This file contains information about project.
start.S  	: Startup file. Calls hw_initialise()and calls main.
vects.S		: Contains hardware vector table and variable vector table.
iom16c62p.h	: I/O register description and address definition for M16C/62P.
hwinit.c	: Implementation of function hw_initialise.
makefile.	: Make file for the project.
blink.c    	: Main program. Blink LED with specified delay.
blink.lnk  	: Linker settings and section description.    
blink.id	: This file contains ID code for generated mot file. 


Summary: 
This program will keep LEDs continuously blinking with specific delay.
User can change the delay and observe the difference.

Special Notes / instructions:
Program must be flashed into M16C/62P, M30626FHPFP, 3-D Starter kit ROM, using FlashSta utility.

(1) Setup:
Connect the 3 Diamonds board with an USB connecter to USB slot in your PC.
Place a jumper socket on JP1 and the board gets it power through the USB.
Turn ON the USB power switch on the 3DKM16C/62PU board.
The green LED next to the switch will now glow.
Now reset the M16C target by pressing the RESET Button on the target board.
Run FlashStart.exe (Flash Start utility can be downloaded from the following link,
www.sequoia.co.uk/silicon/manufacturers/Mitsubishi/R8C_Software_tools.php ) and 
the first dialogue box should appear, select Internal Flash Memory and the COM 
port of the USB e.g. COM3 and then select OK.
On the next dialogue box, Flash Start will require the Filepath and ID number. 
Select the M16C target by changing the radio button. To select the Filepath click
on Refer and select the file blink.mot.  For the ID number just enter 00 into each
small box, once this is done select OK.
Note: Before loading the ".mot" file using flash start utility, please ensure that
0x00 is stored as the following ID locations

FFFDF : 00
FFFE3 : 00
FFFEB : 00
FFFEF : 00
FFFF3 : 00
FFFF7 : 00
FFFFB : 00
FFFFF : FF

Incase the blink.id file is saved in the same path as the output (mot file), 
it will get imported automatically, once the output mot file is selected to download.

On the final dialogue box select Setting, have the Baud rate at 57600 and the Program_intervals 
(ms) at 5,and select OK.  
Then select E.P.R which will Erase any data on the Flash, then it will download the Program, 
blink.mot, into the Flash and then it will Read the Flash to check if the program was correctly
downloaded.  Once this is done select Exit, then turn OFF the USB power supply, remove the JP1
jumper socket from the 3 Diamonds Board. 

(2) Testing:
Now turn ON the USB Power supply and press RESET pushbutton, the LEDs will flash continuously.

(3) Fault Finding:
	a) If Flash Start cannot communicate with the board check:
-	USB connector,
-	main clock (24 MHz crystal),
-	jumper JP1,
-	voltage regulator and supply to the MCU.
b) If the program is successfully flashed in but does not run then check the switch connections.
