/****************************************************************
KPIT Cummins Infosystems Ltd, Pune, India. - 05-Sept-2005.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

*****************************************************************/

#define VECT_SECT __attribute__((section (".vects")))
#define ISR __attribute__ ((interrupt))

void ta0_irq(void) ISR;
void uart1_rx_irq(void) ISR;

