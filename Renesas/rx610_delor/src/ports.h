/*
  Copyright (C) 2010 DJ Delorie <dj@redhat.com>

  This file is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 3, or (at your option) any later
  version.
  
  This file is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
  for more details.
  
  You should have received a copy of the GNU General Public License
  along with this file; see the file COPYING3.  If not see
  <http://www.gnu.org/licenses/>.
*/

#include <stdint.h>

#define P8(x)	(*(volatile uint8_t *)(x))
#define P16(x)	(*(volatile uint16_t *)(x))
#define P32(x)	(*(volatile uint32_t *)(x))

/* Clock Generation Circuit */
#define SCKCR	P32 (0x80020)

/* Power control */
#define MSTPCRA P32 (0x80010)
#define MSTPCRB P32 (0x80014)
#define MSTPCRC P32 (0x80018)

/* Serial Communications Interface */
typedef struct
{
	uint8_t smr;
	uint8_t brr;
	uint8_t scr;
	uint8_t tdr;
	uint8_t ssr;
	uint8_t rdr;
	uint8_t scmr;
	uint8_t semr;
} SCI_Registers;

#define SCI		((volatile SCI_Registers *)(0x88240))

/* Timer Pulse Units */
typedef struct
{
	uint8_t tcr;
	uint8_t tmdr;
	uint8_t tiorh;
	uint8_t tiorl;
	uint8_t tier;
	uint8_t tsr;
	uint16_t tcnt;
	uint16_t tgra;
	uint16_t tgrb;
	uint16_t tgrc;
	uint16_t tgre;
} TPU_Registers;

#define TPU		((volatile TPU_Registers *)(0x88110))
#define TSTRA	P8 (0x88100)
#define TSTRB	P8 (0x88700)

/* Port I/O Control */
#define DDR		((volatile uint8_t *)0x8c000)
#define DR		((volatile uint8_t *)0x8c020)
#define PORT	((volatile uint8_t *)0x8c040)
#define ICR		((volatile uint8_t *)0x8c060)
#define PFCR0	P8 (0x8c100)
#define PFCR1	P8 (0x8c101)
#define PFCR2	P8 (0x8c102)
#define PFCR3	P8 (0x8c103)
#define PFCR4	P8 (0x8c104)
#define PFCR5	P8 (0x8c105)
#define PFCR6	P8 (0x8c106)
#define PFCR7	P8 (0x8c107)
#define PFCR8	P8 (0x8c108)
#define PFCR9	P8 (0x8c109)

/* Interrupts */
#define IER0D	P8 (0x8720D)
#define IER1B	P8 (0x8721B)
#define IER1C	P8 (0x8721C)
#define IER1D	P8 (0x8721D)

#define IPR4C	P8 (0x8734C)
#define IPR4D	P8 (0x8734D)
#define IPR81	P8 (0x87381)
#define IPR84	P8 (0x87384)

typedef void (*Interrupt_Handler)(void);
extern Interrupt_Handler _vectors[256];

#define IVEC_TPU0_TGI0A 104
#define IVEC_TPU0_TCI0V	108

#define IVEC_SCI4_RXI4	231
#define IVEC_SCI4_TXI4	232
#define IVEC_SCI4_TEI4	233

#define printf iprintf

void set_system_clock_speed ();
void setup_interrupts ();
