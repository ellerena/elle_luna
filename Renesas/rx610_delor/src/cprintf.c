/*
	Copyright (C) 2010 DJ Delorie <dj@redhat.com>

	This file is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 3, or (at your option) any later
	version.

	This file is distributed in the hope that it will be useful, but WITHOUT ANY
	WARRANTY; without even the implied warranty of MERCHANTABILITY or
	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
	for more details.

	You should have received a copy of the GNU General Public License
	along with this file; see the file COPYING3.  If not see
	<http://www.gnu.org/licenses/>.
*/

#include <stdarg.h>
#include "cprintf.h"

typedef unsigned char byte;

static char hex[] = "0123456789abcdef";

void cput_nibble (int n)
{
	tty_putc (hex[n&0x0f]);
}

void cput_hex_byte (int n)
{
	cput_nibble (n >> 4);
	cput_nibble (n);
}

void cput_binary_byte (int n)
{
	int i;

	for (i=7; i>=0; i--)
		tty_putc((n & (1<<i)) ? '1' : '0');
}

void cput_hex_word (int n)
{
	cput_hex_byte (n >> 8);
	cput_hex_byte (n);
}

void cput_hex_block (byte *block, int n)
{
	int i = 0;

	while (n)
	{
		cput_hex_byte (*block++);
		if (--n == 0) break;
		i++;
		tty_putc(((i & 7) == 0) ? ' ' : ':');
	}
}

void cput_nibble_block (byte *block, int n)
{
	int i = 0;

	while (n)
	{
		cput_nibble (*block);
		if (--n == 0) break;
		i++;
		if ((i & 7) == 0) tty_putc (' ');
	}
}

void cput_number (int n)
{
	char buf[20];
	int i = 0;

	if (n < 0)
	{
		tty_putc ('-');
		n = -n;
	}
	while (n > 9)
	{
		buf[i++] = (n%10) + '0';
		n /= 10;
	}
	buf[i++] = (n%10) + '0';
	while (i > 0)
		tty_putc (buf[--i]);
}

void cprintf (const char *fmt, ...)
{
	va_list v;
	int i;
	char *s;

	va_start (v, fmt);

	while (*fmt)
	{
		if (*fmt != '%') tty_putc (*fmt);
		else
			switch (*++fmt)
			{
				case '%':
					tty_putc ('%');
					break;
				case 'c':
					i = va_arg (v, int);
					tty_putc(i);
					break;
				case 'd':
					i = va_arg (v, int);
					cput_number(i);
					break;
				case 'b':
					i = va_arg (v, int);
					cput_hex_byte (i);
					break;
				case 'B':
					i = va_arg (v, int);
					cput_binary_byte (i);
					break;
				case 'w':
					i = va_arg (v, int);
					cput_hex_word (i);
					break;
				case 'x':
					s = va_arg (v, char *);
					i = va_arg (v, int);
					cput_hex_block ((byte*)s, i);
					break;
				case 'n':
					s = va_arg (v, char *);
					i = va_arg (v, int);
					cput_nibble_block ((byte*)s, i);
					break;
				case 's':
					s = va_arg (v, char *);
					tty_puts (s);
					break;
			}
		fmt ++;
	}
}
