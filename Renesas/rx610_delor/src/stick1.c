/*
  Copyright (C) 2010 DJ Delorie <dj@redhat.com>

  This file is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 3, or (at your option) any later
  version.
  
  This file is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
  for more details.
  
  You should have received a copy of the GNU General Public License
  along with this file; see the file COPYING3.  If not see
  <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>

#include "ports.h"
#include "cprintf.h"

/* LEDS are configured as follows, looking at the stick with the usb
   cable DOWN:

		RED[7:0]  RED[13:8]
		P20..P26  P27,PA0..PA5  LOW = ON
   COM0 PD0
   COM1 PD1
   COM2 PD2
   COM3 PD3
   COM4 PD4

   COM5 PD5
   COM6 PD6
   COM7 PD7
   COM8 PE0  LOW = ON
   COM9 PE1
*/

static unsigned short pixels[10] =
  { ~0, ~0, ~0, ~0, ~0, ~0, ~0, ~0, ~0, ~0 };

unsigned int rand(void)
{
	static unsigned long long next = 1;
	/* This multiplier was obtained from Knuth, D.E., "The Art of
	 	Computer Programming," Vol 2, Seminumerical Algorithms, Third
	 	Edition, Addison-Wesley, 1998, p. 106 (line 26) & p. 108 */
	next = next * 6364136223846793005LL + 1;
	return (int)(next >> 32);
}

/*
	P2.0	CB3
	P2.1	CA3
	P2.2	CC3
	P2.3	CD3
	P2.4	CB4
	P2.5	CA4
	P2.6	CA5
	P2.7	CB5

	PA.0	CA6
	PA.1	CB6
	PA.2	CC6
	PA.3	CD6
	PA.4	CA7
	PA.5	CB7
	PA.6	CA8
	PA.7	CB8
*/

static volatile int timer = 0;

static void __attribute__((interrupt)) tpu0_handler ()
{
	static int row = 0;
	int com;
	timer ++;

	if (timer & 1)
	{
		com = ~(1 << row);
		DR[2] = DR[10] = ~0;
		DR[13] = com;
		DR[14] = com >> 8;
		DR[2] = pixels[row];
		DR[10] = pixels[row] >> 8;
		row = (row + 1) % 10;
	}
}

static void wait_ms (int ms)
{
	int now = timer;

	while ((int)(timer - now) < ms)
	{
		if (tty_ready ())
		{
			char ch = tty_getc ();
			cprintf("[%b]", ch);
			if (ch == 10 || ch == 13)
				tty_puts("\r\n");
		}
	}
}

char banner_message[] = "\n\n\033[32mDJ was here!\033[0m\n";

/* 0,0 is the lower left, 13,9 is the upper right */
static void pixel (int c, int r, int v)
{
	if (r < 0 || r > 9 || c < 0 || c > 13) return;
	if (v) pixels[r] &= ~(1 << c);
	else pixels[r] |= 1 << c;

#if 0
	r = ~(1 << r);
	c = ~(1 << c);

	DR[2] = ~0;
	DR[10] = ~0;

	DR[13] = r;
	DR[14] = r >> 8;

	DR[2] = c;
	DR[10] = c >> 8;
#endif
}

void cls()
{
	memset (pixels, ~0, sizeof(pixels));
}

int main()
{
	int i, j;

	/* Power up the peripherals.  */
	MSTPCRA = 0;
	MSTPCRB = 0;
	MSTPCRC = 0;

	set_system_clock_speed ();
								/* 0: led ON, 1: led OFF */
	DR[2] = 0xfe;
	DR[0xA] = 0xfe;
	DR[0xD] = 0xfe;
	DR[0xE] = 0xfe;
								/* 0: input, 1: output*/
	DDR[2] = 0xff;
	DDR[0xA] = 0xff;
	DDR[0xD] = 0xff;
	DDR[0xE] = 0xff;

	/* Timer */
	_vectors[IVEC_TPU0_TGI0A] = tpu0_handler;
	IER0D |= 0x01;
	IPR4C = 5;

	TPU[0].tcr = 0x20;		/* TGRA, PCLK = 25 MHz */
	TPU[0].tier = 0x01;		/* interrupt on TGRA */
	TPU[0].tgra = 25000;	/* should be 1 mS */

	TSTRA |= 0x01;			/* start timer */

	setup_interrupts ();

	tty_init ();
	wait_ms (4);			/* let console be ready for our start bit */
	tty_puts(banner_message);
	tty_puts ("RX/610 is in ");
	i = 1;
	tty_puts((*(unsigned char *)&i == 1) ? "little" : "big");
	tty_puts (" endian mode\nPress any key to see it's numeric value\n");

	while (1)
	{
		cls();

		for (i=0; i<30; i++)
		{
			pixel(i/6, i/6, 1);
			pixel(0, i/3, 1);
			pixel(i/2, 0, 1);
			wait_ms (10);
		}
		wait_ms (1000);

		for (i=0; i<14; i++)
			for (j=0; j<=i; j++)
			{
				pixel(i-j, j, 1);
				pixel(13-(i-j), 9-j, 1);
				wait_ms (10);
			}

		wait_ms (1000);
		{
			int mix[140];
			for (i=0; i<140; i++) mix[i] = i;
			for (i=0; i<140; i++)
			{
				int t = rand() % 140;
				int v = mix[t];
				mix[t] = mix[i];
				mix[i] = v;
			}
			for (i=0; i<140; i++)
			{
				int x = mix[i] % 14;
				int y = mix[i] / 14;
				pixel (x, y, 0);
				wait_ms (10);
			}
		}
		wait_ms (1000);
	}

  return 0;
}
