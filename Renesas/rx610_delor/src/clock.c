/*
  Copyright (C) 2010 DJ Delorie <dj@redhat.com>

  This file is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 3, or (at your option) any later
  version.
  
  This file is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
  for more details.
  
  You should have received a copy of the GNU General Public License
  along with this file; see the file COPYING3.  If not see
  <http://www.gnu.org/licenses/>.
*/

#include "ports.h"

void
set_system_clock_speed ()
{	/* We start with a 12 or 12.5 MHz crystal. */
	/* 0 = x8  1 = x4  2 = x2  3 = x1 */
	/* ICLK: x8 = 100 MHz  */
	/* PCLK: x4 =  50 MHz   */
	/* BCLK: x2 =  25 MHz   */

	DDR[5] &= ~0x08;		/* set pin P53 as input */
	SCKCR = 0x00810100;		/* ICLK = 100MHz, BCLK = 50MHz, PCLK = 50MHz */
	DDR[5] |= 0x08;			/* set pin P53 as output */
	SCKCR &= ~0x00800000;	/* enable and direct BCLK to P53 */
}
