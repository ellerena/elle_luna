/*
	Copyright (C) 2010 DJ Delorie <dj@redhat.com>

	This file is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 3, or (at your option) any later
	version.

	This file is distributed in the hope that it will be useful, but WITHOUT ANY
	WARRANTY; without even the implied warranty of MERCHANTABILITY or
	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
	for more details.

	You should have received a copy of the GNU General Public License
	along with this file; see the file COPYING3.  If not see
	<http://www.gnu.org/licenses/>.
*/

#include "ports.h"

/* This isn't as efficient as an interrupt-driven source; there's an
	 extra "stop" bit in the transmission stream because we can't set
	 the TX data register soon enough.  However, we can't set the baud
	 rate for 115200 accurately enough, and the extra stop bit keeps the
	 host from getting out of sync.  */

#define CONSOLE 4
#define BAUDIOS	57600

#define RXBUFSZ 128
static char rxbuf[RXBUFSZ];
static volatile int rxput, rxget, rxcount;

static void __attribute__((interrupt))
sci4_rx_handler (void)
{
	int b;

	b = SCI[CONSOLE].rdr;
	rxbuf[rxput++] = b;
	rxput %= RXBUFSZ;
	rxcount ++;
}

static void tty_init_port (int baud)
{
	int i;

	SCI[CONSOLE].scr = 0x00;	/* Disable, internal clock, div-1 */
	SCI[CONSOLE].smr = 0x00;	/* async, 8N1, PCLK (50MHz) */
	SCI[CONSOLE].scmr = 0x00;	/* Disable smart card mode */
	SCI[CONSOLE].semr = 0x10;	/* div-8 mode in BRR */
	SCI[CONSOLE].brr = (12e6 * 4) / (16 * baud) - 1;	/* Calculate baud rate. */

	for (i=1100; i; --i)
		asm("");

	SCI[CONSOLE].scr = 0x70;	/* Enable transmitter, receiver, rx interrupts */
}

void tty_init ()
{
	ICR[0] = 0x20;					/* Enable SCI4 input buffer - P0.5 */

	rxput = rxget = rxcount = 0;
	tty_init_port (BAUDIOS);

	_vectors[IVEC_SCI4_RXI4] = sci4_rx_handler;	/* SCI4 interrupts */
	IER1C |= 0x80;
	IPR84 = 5;
}

void tty_putc (char c)
{
	if (c == '\n') tty_putc ('\r');
	/* TEND = 0:transmitting, 1:done */
	while (! (SCI[CONSOLE].ssr & 0x04)) ;
	SCI[CONSOLE].tdr = c;
}

void tty_puts (char *s)
{
	while (*s)
		tty_putc (*s++);
}

int write (int fd, char *buf, int len)
{
	int ilen = len;

	while (len--)
		tty_putc (*buf++);

	return ilen;
}

char tty_getc ()
{
	char rv;

	while (rxcount == 0) ;
	rv = rxbuf[rxget++];
	rxget %= RXBUFSZ;
	rxcount --;
	return rv;
}

int tty_ready ()
{
	return rxcount;
}
