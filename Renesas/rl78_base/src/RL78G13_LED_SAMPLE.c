/***************************************************************/
/*                                                             */
/*      PROJECT NAME :  RL78G13_LED_SAMPLE                     */
/*      FILE         :  RL78G13_LED_SAMPLE.c                   */
/*      DESCRIPTION  :  Main Program                           */
/*      CPU SERIES   :  RL78 - G13                             */
/*      CPU TYPE     :  R5F100LE                               */
/*                                                             */
/***************************************************************/                                   

#include <string.h>

#include "r_cg_macrodriver.h"
#include "r_cg_cgc.h"
#include "r_cg_port.h"
#include "r_cg_serial.h"

#define LED01       PM7_bit.no7
#define TASK		gComIn[0]	/* variable to hold the requested 'task' */
#define	MODE		gComIn[1]	/* variable to hold the 'mode' for the TASK*/

#ifdef CPPAPP
//Initialize global constructors
extern "C" void __main()
{
  static int initialized;
  if (! initialized)
    {
      typedef void (*pfunc) ();
      extern pfunc __ctors[];
      extern pfunc __ctors_end[];
      pfunc *p;

      initialized = 1;
      for (p = __ctors_end; p > __ctors; )
	(*--p) ();

    }
}
#endif 

#define	LOGO	"\nYRPBRL78G13 - v.0.0.1"

extern uint8_t gComIn[];
uint8_t gFlags = 0;
const uint8_t logo[] = LOGO;
unsigned n, iResult;

static unsigned ParseCharsToValueHex(char *p);
static int fn_i(void);

void delay()
{
	unsigned long long uLcounter = 0;
	for(uLcounter=0;uLcounter<100000;uLcounter++)
	{
		NOP();
		NOP();
		NOP();
		NOP();
	}
}

int main(void)
{
	EI();

    delay();
    delay();
    COM_Start();
	COM_send(logo, sizeof(logo));
	LED01 = 1;

	while(1)
	{
		COM_send("\n#", 2);
		while(!gFlags);
		n = ParseCharsToValueHex((char*)gComIn + 2);
		switch (TASK)
		{
			case 'i':
				iResult = fn_i();
				break;
			default:
				COM_send(gComIn, 2);
				break;
		}

		COM_send("-->", 3);
		COM_TXHex(iResult);
		gFlags &= ~1;
	}
	return 0;
}

static int fn_i(void)
{
	switch(MODE)
	{
		case 'd':
		{
			COM_send(logo, sizeof(logo));
			break;
		}
		default:
			break;
	}
	return 0;
}

/**
	@brief	converts a string of chars representing a vector of hex numbers into a vector of bytes equivalent.
	@param	p contains the address of the first char in the string.
	@retval	the count of numbers comverted.
	@verbatim
			Text entered by a user via the keyboard is in the form of a string of printable chars that represent hex numbers.
			This functions takes that string of chars and extracts the meaningful hex numbers represented in it, then
			it places those numbers (bytes) in an array at the same ram location. Returning the number of numbers
			converted.
	@endverbatim
 */
static unsigned ParseCharsToValueHex(char *p)
{
	char *q, d;
	unsigned i, k;
	uint8_t c;

	q = p;
	k = 0;

	while (*p > 0x1f)										/* Will process until a non printable character is found */
	{
		d = 0;												/* initialize the 'number equivalent value' */
		i = 0;												/* initialize the digit counter (assume 2 digits max per number)*/
		do
		{
			d <<= 4;										/* each digit uses the first 4 bits, so on each loop we must push the others */
			c = *p++;										/* read the new char */
			if ((c > 0x29) && (c < 0x3a))					/* char is between 0 and 9 */
			{
				d += (c - 0x30);							/* add the char equivalen value to our total */
				i++;										/* increment our digit counter */
			}
			if ((c > 0x60 ) && (c < 0x67))					/* char is between 0xa and 0xf */
			{
				d += (c - 0x60 + 0x9);						/* add the char equivalen value to our total */
				i++;										/* increment our digit counter */
			}
		} while (( i == 0 ) || ((*p > 0x2f) && (i < 2)));	/* repeat: if no char found or: if found a space and less than 2 digits */

		if(i)												/* if the digit counter is non-zero then a value has been found */
		{
			*q++ = d;										/* write the number found to memory */
			k++;											/* increment 'detected numbers counter' */
		}
	}

	return k;
}



