/***************************************************************/
/*                                                             */
/*      PROJECT NAME :  RL78G13_LED_SAMPLE                     */
/*      FILE         :  interrupt_handlers.c                   */
/*      DESCRIPTION  :  Interrupt Handler                      */
/*      CPU SERIES   :  RL78 - G13                             */
/*      CPU TYPE     :  R5F100LE                               */
/*                                                             */
/***************************************************************/                            
#include "r_cg_interrupt_handlers.h"

#include "r_cg_macrodriver.h"
#include "r_cg_serial.h"
/* Start user code for include. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

extern volatile uint8_t * gp_uart2_tx_address;         /* uart2 send buffer address */
extern volatile uint16_t  g_uart2_tx_count;            /* uart2 send data number */
uint8_t gComIn[50];
extern uint8_t gFlags;

/*
 * INT_WDTI (0x4)
 */
void INT_WDTI (void) { }

/*
 * INT_LVI (0x6)
 */
void INT_LVI (void) { }

/*
 * INT_P0 (0x8)
 */
void INT_P0 (void) { }

/*
 * INT_P1 (0xA)
 */
void INT_P1 (void) { }

/*
 * INT_P2 (0xC)
 */
void INT_P2 (void) { }

/*
 * INT_P3 (0xE)
 */
void INT_P3 (void) { }

/*
 * INT_P4 (0x10)
 */
void INT_P4 (void) { }

/*
 * INT_P5 (0x12)
 */
void INT_P5 (void) { }

/*
 * INT_CSI20/INT_IIC20/INT_ST2 (0x14)
 */
void INT_ST2 (void) {
    if (g_uart2_tx_count > 0U)
    {
        TXD2 = *gp_uart2_tx_address;
        gp_uart2_tx_address++;
        g_uart2_tx_count--;
    }
    else
    {
        SMR10 &= ~_0001_SAU_BUFFER_EMPTY;
    }
}
//void INT_CSI20 (void) { }
//void INT_IIC20 (void) { }

/*
 * INT_CSI21/INT_IIC21/INT_SR2 (0x16)
 */
void INT_SR2 (void) {
	uint8_t	t;
	static uint8_t *pacCommand = gComIn;					/* pointer used to write characters recvd by UART1 */

	SIR11 = (uint16_t)(SSR11 & 0x0007U);

	t = RXD2;
	*pacCommand++ = t;
	if (t == '\r')										/* <return> ends the uart command entry */
	{
		pacCommand = gComIn;
		gFlags = 1;							/* rise a flag to alert main() */
	}
}
//void INT_CSI21 (void) { }
//void INT_IIC21 (void) { }

/*
 * INT_SRE2 (0x18)
 */
void INT_SRE2 (void) { }

/*
 * INT_DMA0 (0x1A)
 */
void INT_DMA0 (void) { }

/*
 * INT_DMA1 (0x1C)
 */
void INT_DMA1 (void) { }

/*
 * INT_CSI00/INT_IIC00/INT_ST0 (0x1E)
 */
void INT_ST0 (void) { }
//void INT_CSI00 (void) { }
//void INT_IIC00 (void) { }

/*
 * INT_CSI01/INT_IIC01/INT_SR0 (0x20)
 */
void INT_SR0 (void) { }
//void INT_CSI01 (void) { }
//void INT_IIC01 (void) { }

/*
 * INT_SRE0/INT_TM01H (0x22)
 */
void INT_TM01H (void) { }
//void INT_SRE0 (void) { }

/*
 * INT_CSI10/INT_IIC10/INT_ST1 (0x24)
 */
void INT_ST1 (void) { }
//void INT_CSI10 (void) { }
//void INT_IIC10 (void) { }

/*
 * INT_CSI11/INT_IIC11/INT_SR1 (0x26)
 */
void INT_SR1 (void) { }
//void INT_CSI11 (void) { }
//void INT_IIC11 (void) { }

/*
 * INT_SRE1/INT_TM03H (0x28)
 */
void INT_TM03H (void) { }
//void INT_SRE1 (void) { }

/*
 * INT_IICA0 (0x2A)
 */
void INT_IICA0 (void) { }

/*
 * INT_TM00 (0x2C)
 */
void INT_TM00 (void) { }

/*
 * INT_TM01 (0x2E)
 */
void INT_TM01 (void) { }

/*
 * INT_TM02 (0x30)
 */
void INT_TM02 (void) { }

/*
 * INT_TM03 (0x32)
 */
void INT_TM03 (void) { }

/*
 * INT_AD (0x34)
 */
void INT_AD (void) { }

/*
 * INT_RTC (0x36)
 */
void INT_RTC (void) { }

/*
 * INT_IT (0x38)
 */
void INT_IT (void) { }

/*
 * INT_KR (0x3A)
 */
void INT_KR (void) { }

/*
 * INT_TM04 (0x42)
 */
void INT_TM04 (void) { }

/*
 * INT_TM05 (0x44)
 */
void INT_TM05 (void) { }

/*
 * INT_TM06 (0x46)
 */
void INT_TM06 (void) { }

/*
 * INT_TM07 (0x48)
 */
void INT_TM07 (void) { }

/*
 * INT_P6 (0x4A)
 */
void INT_P6 (void) { }

/*
 * INT_P7 (0x4C)
 */
void INT_P7 (void) { }

/*
 * INT_P8 (0x4E)
 */
void INT_P8 (void) { }

/*
 * INT_P9 (0x50)
 */
void INT_P9 (void) { }

/*
 * INT_P10 (0x52)
 */
void INT_P10 (void) { }

/*
 * INT_P11 (0x54)
 */
void INT_P11 (void) { }

/*
 * INT_MD (0x5E)
 */
void INT_MD (void) { }

/*
 * INT_FL (0x62)
 */
void INT_FL (void) { }

/*
 * INT_BRK_I (0x7E)
 */
void INT_BRK_I (void) { }
