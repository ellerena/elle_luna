#ifndef HEADER_
#define HEADER_

#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

void funcion1 (void);
void funcion2 (void);
void funcion3 (void);
void funcion4 (void);
void funcion5 (void);

int menu(void);

// Class declarations

class Clase
{
private:
	string s;
	char c;
	int i;
	double d;

public:
	Clase();
	Clase(string, char, int, double);
	~Clase();
	string getS() const;
	char getC() const;
	int getI() const;
	double getD() const;
	void setS(string);
	void setC(char);
	void setI(int);
	void setD(double);
	void setAll(string, char, int, double);
};

struct board {
	char numero;
	int valor;
	string nombre;
};

struct node {
	int *ID;
	int * payload;
	void (*routine) (int *);
	struct node *pnext = NULL;
};

#endif
