/*
 ============================================================================
 Name        : intel_mingw_windows_hw.c
 Author      : Edison Llerena
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define PRN(...)	printf(__VA_ARGS__);

// Due to odd GPIO mapping, a bit reversal is required for data bits
unsigned bit_reverse(unsigned v)
{
  unsigned r = v; // r will be reversed bits of v; first get LSB of v
  unsigned s = sizeof(v) * 32 - 1; // extra shift needed at end

  for (v >>= 1; v; v >>= 1)
  {
    r <<= 1;
    r |= v & 1;
    s--;
  }
  r <<= s; // shift when v's highest bits are zero

  return r;
}

unsigned bit_Reverse(unsigned v) {

  unsigned rmask, result;

  rmask = (1 << (sizeof(unsigned)*8 - 1));
  result = 0;

  while (rmask) {
    if (v&1) {
      result |= rmask;
    }
    rmask >>= 1;
    v >>= 1;
  }

  return result;
}


int main(void)
{
  unsigned a;

  PRN ("Size of \"unsigned\" is zu\n", sizeof(unsigned));

  while(1)
  {
    PRN("Enter an hexadecimal integer:");
    scanf("%x", &a);
    if(!a) break;
    PRN("\nReversed: 0x%08X\t0x%08X\r\n", bit_reverse(a), bit_Reverse(a));
  }
  system("pause");
  return EXIT_SUCCESS;
}

