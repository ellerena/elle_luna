#ifndef __BR_REGS_H__
#define __BR_REGS_H__

#include <stdint.h>
#include <stdio.h>

#ifdef __MICROBLAZE__
#include "xparameters.h"
#include "xil_types.h"
#endif

/******************************** ALIASES FOR BREDS FW ********************************/
#define BR_IO_FT232H_BASE               XPAR_M01_AXI_BASEADDR
#define BR_IO_REGS0_BASE                XPAR_M07_AXI_BASEADDR
#define BR_IO_REGS1_BASE                XPAR_M00_AXI_BASEADDR
#define BR_IO_REGS2_BASE                XPAR_M09_AXI_BASEADDR
#define BR_IO_STATUS_REGISTER           BIG_ERR_STATUS_OFFSET
#ifdef __LOGGING_H__
#undef ERRORPRINTFV
#undef ERRORPRINTF
#undef PRINTF
#undef PRINTFV
#else
#define __LOGGING_H__
#endif
#define ERRORPRINTFV                    printf
#define ERRORPRINTF                     printf
#define PRNF                            printf
#define PRINTF(format)
#define PRINTFV(format, ...)

/*==========================================================================================*/
/*================================== REGISTER DEFINITIONS ==================================*/
/*==========================================================================================*/
#define DDR_BASE_ADDR                   (XPAR_MIG_7SERIES_0_BASEADDR)       /* external DDR3L 256MB */
#define OFO_BASE_ADDR                   (BR_IO_REGS1_BASE)                  /* Output fifo (ofo) base address */
#define FT_BASE_ADDR                    (BR_IO_FT232H_BASE)                 /* FTDI interface fifo base address */
#define PAG_BASE_ADDR                   (BR_IO_REGS2_BASE)                  /* Pattern generator (pag) base address */
#define BIG_BASE_ADDR                   (BR_IO_REGS0_BASE)                  /* Big Red base address */
#define TXT_BASE_ADDR                   (BR_IO_REGS0_BASE + 0x1000)         /* TX Burst transmitter module base address */
#define DES_BASE_ADDR                   (BR_IO_REGS0_BASE + 0x2000)         /* De-serializer (des) base address */
#define DCO_BASE_ADDR                   (BR_IO_REGS0_BASE + 0x3000)         /* DC Offset base address */
#define PPA_BASE_ADDR                   (BR_IO_REGS0_BASE + 0x4000)         /* Post Processing Algorithm (ppa) base address */
#define MSM_BASE_ADDR                   (BR_IO_REGS0_BASE + 0x5000)         /* Master State Machine (msm) base address */
#define UBL_BASE_ADDR                   (BR_IO_REGS0_BASE + 0x6000)         /* Ublaze control register */
#define DMA_BASE_ADDR                   (XPAR_AXI_CDMA_0_BASEADDR)
#define LCD_LED_BASE_ADDR               (XPAR_GPIO_0_BASEADDR)
#define PB_SW_BASE_ADDR                 (XPAR_GPIO_1_BASEADDR)
#define TIM_BASE_ADDR                   (XPAR_AXI_TIMER_0_BASEADDR)
#define TIM2_BASE_ADDR                  (XPAR_AXI_TIMER_1_BASEADDR)
#define ULI_BASE_ADDR                   (XPAR_AXI_UARTLITE_0_BASEADDR)
#define STM_BASE_ADDR                   (XPAR_UARTNS550_0_BASEADDR)
#define INT_BASE_ADDR                   (XPAR_MICROBLAZE_0_AXI_INTC_BASEADDR)   /* interrupt controller base address */
#define IQ_BASE_ADDR                    (BR_IO_REGS0_BASE + 0x7000)         /* IQ base address */
#define IIC_BASE_ADDR                   (XPAR_AXI_IIC_0_BASEADDR)           /* i2c controller */

/*==========================================================================================*/
/*=============================== REGISTER OFFSET DEFINITIONS ==============================*/
/*==========================================================================================*/
#define DDR_DBG_OFFSET                  (0x00)

#define RD_OFFSET                       (0x00)
#define WR_OFFSET                       (0x04)
#define STATUS_OFFSET                   (0x08)
#define CTRL_OFFSET                     (0x0c)
#define VER_OFFSET                      (0x10)

#define ULI_RX_OFFSET                   (0x00)
#define ULI_TX_OFFSET                   (0x04)
#define ULI_STATUS_OFFSET               (0x08)
#define ULI_CTRL_OFFSET                 (0x0c)

#define DCO_START_OFFSET                (0x00)                                  /* DCO start acquisition offset */
#define DCO_STATUS_OFFET                (0x04)                                  /* DCO collection depth offset */
#define DCO_CONTROL_OFFSET              (0x08)                                  /* DCO status offset */
#define DCO_CAL_START_OFFSET            (0x100)                                 /* DCO calibrated value start */
#define DCO_CAL_END_OFFSET              (0x17c)                                 /* DCO calibrated value end */
#define DCO_PROG_START_OFFSET           (0x200)                                 /* DCO user calibration value start */
#define DCO_PROG_END_OFFSET             (0x27c)                                 /* DCO user calibration value end */

#define DES_CONTROL_OFFSET              (0x0)                                   /* DES control offset */
#define DES_STATUS_OFFSET               (0x4)                                   /* DES status offset */

#define PPA_CONTROL_OFFSET              (0x0)                                   /* PPA control offset */

#define TXT_START_BURST_OFFSET          (0x000)
#define TXT_MASK_OFFSET                 (0x004)
#define TXT_BURST_LENGTH_OFFSET         (0x008)
#define SCRATCH_OFFSET                  (0x00C)
#define TXT_CURRENT_ROW_OFFSET          (0x010)
#define TXT_COARSE_DELAY_OFFSET         (0x20)
#define TXT_FINE_DELAY_OFFSET           (0x60)
#define TXT_LUT_START_OFFSET            (0x100)
#define TXT_LUT_END_OFFSET              (0x2FC)

#define MSM_START_ACQ_OFFSET            (0x00)                                  /* MSM start acquisition offset */
#define MSM_COL_DEPTH_OFFSET            (0x04)                                  /* MSM collection depth offset */
#define MSM_STATUS_OFFSET               (0x08)                                  /* MSM status offset */
#define MSM_CONTROL_OFFSET              (0x0C)                                  /* MSM control register */
#define MSM_WINDOW_OFFSET               (0x10)                                  /* MSM sampling window */
#define MSM_RANGE_CTRL_OFFSET           (0x100)                                 /* MSM range control offset */
#define MSM_RANGE_VALUE_OFFSET          (0x104)                                 /* MSM range value offset */

#define PAG_CONTROL_OFFSET              (0x0)
#define PAG_VERSION_OFFSET              (VER_OFFSET)

#define OFO_READ_DATA_OFFSET            (RD_OFFSET)
#define OFO_WRITE_DATA_OFFET            (WR_OFFSET)
#define OFO_STATUS_OFFSET               (STATUS_OFFSET)
#define OFO_CONTROL_OFFSET              (CTRL_OFFSET)
#define OFO_VERSION_OFFSET              (VER_OFFSET)

#define DMA_CR_OFFSET                   (XAXICDMA_CR_OFFSET)
#define DMA_SR_OFFSET                   (XAXICDMA_SR_OFFSET)
#define DMA_CDESC_OFFSET                (XAXICDMA_CDESC_OFFSET)
#define DMA_TDESC_OFFSET                (XAXICDMA_TDESC_OFFSET)
#define DMA_SRCADDR_OFFSET              (XAXICDMA_SRCADDR_OFFSET)
#define DMA_DSTADDR_OFFSET              (XAXICDMA_DSTADDR_OFFSET)
#define DMA_BTT_OFFSET                  (XAXICDMA_BTT_OFFSET)

#define IQ_CONTROL_OFFSET               (0x0)
#define IQ_DEC_NUM_OFFSET               (0x4)
#define IQ_BP_FIR_COEF_x_OFFSET         (0x100) /* start offsets */
#define IQ_LP_FIR_COEF_x_OFFSET         (0x200) /* "" */
#define IQ_DIG_GAIN_x_OFFSET            (0x300) /* "" */

#define IIC_DGIER_OFFSET                (XIIC_DGIER_OFFSET)
#define IIC_IISR_OFFSET                 (XIIC_IISR_OFFSET)
#define IIC_IIER_OFFSET                 (XIIC_IIER_OFFSET)
#define IIC_RESETR_OFFSET               (XIIC_RESETR_OFFSET)
#define IIC_CR_OFFSET                   (XIIC_CR_REG_OFFSET)
#define IIC_SR_OFFSET                   (XIIC_SR_REG_OFFSET)
#define IIC_DTR_OFFSET                  (XIIC_DTR_REG_OFFSET)
#define IIC_DRR_OFFSET                  (XIIC_DRR_REG_OFFSET)
#define IIC_ADR_OFFSET                  (XIIC_ADR_REG_OFFSET)
#define IIC_TFO_OFFSET                  (XIIC_TFO_REG_OFFSET)
#define IIC_RFO_OFFSET                  (XIIC_RFO_REG_OFFSET)
#define IIC_TBA_OFFSET                  (XIIC_TBA_REG_OFFSET)
#define IIC_RFD_OFFSET                  (XIIC_RFD_REG_OFFSET)
#define IIC_GPO_OFFSET                  (XIIC_GPO_REG_OFFSET)

#define BIG_SW_RST_OFFSET               (0)
#define BIG_CLK_STATUS_OFFSET           (4)
#define BIG_CLK_SELECT_OFFSET           (8)
#define BIG_VERSION_OFFSET              (0x10)
#define BIG_TXT_VER_OFFSET              (0x14)
#define BIG_DES_VER_OFFSET              (0x18)
#define BIG_DCO_VER_OFFSET              (0x1C)
#define BIG_REG_VER_OFFSET              (0x20)
#define BIG_PPA_VER_OFFSET              (0x24)
#define BIG_FIL_VER_OFFSET              (0x28)
#define BIG_RANGE_VER_OFFSET            (0x2C)
#define BIG_ERR_STATUS_OFFSET           (0x100)

#define FT_READ_RX_OFFSET               (RD_OFFSET)
#define FT_WRITE_TX_OFFSET              (WR_OFFSET)
#define FT_STATUS_OFFSET                (STATUS_OFFSET)
#define FT_CONTROL_OFFSET               (CTRL_OFFSET)
#define FT_VERSION                      (VER_OFFSET)

#define LED_DATA_OFFSET                 (XGPIO_DATA_OFFSET)
#define LED_DIR_OFFSET                  (XGPIO_TRI_OFFSET)
#define LCD_DATA_OFFSET                 (XGPIO_DATA2_OFFSET)
#define LCD_DIR_OFFSET                  (XGPIO_TRI2_OFFSET)

#define DIPS_DATA_OFFSET                (XGPIO_DATA_OFFSET)
#define DIPS_DIR_OFFSET                 (XGPIO_TRI_OFFSET)
#define PUSH_DATA_OFFSET                (XGPIO_DATA2_OFFSET)
#define PUSH_DIR_OFFSET                 (XGPIO_TRI2_OFFSET)

#define TIM_TCSR0_OFFSET                (XTC_TCSR_OFFSET)
#define TIM_TLR0_OFFSET                 (XTC_TLR_OFFSET)
#define TIM_TCR0_OFFSET                 (XTC_TCR_OFFSET)
#define TIM_TCSR1_OFFSET                (TIM_TCSR0_OFFSET + XTC_TIMER_COUNTER_OFFSET)
#define TIM_TLR1_OFFSET                 (TIM_TLR0_OFFSET + XTC_TIMER_COUNTER_OFFSET)
#define TIM_TCR1_OFFSET                 (TIM_TCR0_OFFSET + XTC_TIMER_COUNTER_OFFSET)

#define TIM2_TCSR0_OFFSET               (XTC_TCSR_OFFSET)
#define TIM2_TLR0_OFFSET                (XTC_TLR_OFFSET)
#define TIM2_TCR0_OFFSET                (XTC_TCR_OFFSET)
#define TIM2_TCSR1_OFFSET               (TIM2_TCSR0_OFFSET + XTC_TIMER_COUNTER_OFFSET)
#define TIM2_TLR1_OFFSET                (TIM2_TLR0_OFFSET + XTC_TIMER_COUNTER_OFFSET)
#define TIM2_TCR1_OFFSET                (TIM2_TCR0_OFFSET + XTC_TIMER_COUNTER_OFFSET)

#define INT_ISR_OFFSET                  XIN_ISR_OFFSET      /* Interrupt Status Register */
#define INT_IPR_OFFSET                  XIN_IPR_OFFSET      /* Interrupt Pending Register */
#define INT_IER_OFFSET                  XIN_IER_OFFSET      /* Interrupt Enable Register */
#define INT_IAR_OFFSET                  XIN_IAR_OFFSET      /* Interrupt Acknowledge Register */
#define INT_SIE_OFFSET                  XIN_SIE_OFFSET      /* Set Interrupt Enable Register */
#define INT_CIE_OFFSET                  XIN_CIE_OFFSET      /* Clear Interrupt Enable Register */
#define INT_IVR_OFFSET                  XIN_IVR_OFFSET      /* Interrupt Vector Register */
#define INT_MER_OFFSET                  XIN_MER_OFFSET      /* Master Enable Register */
#define INT_IMR_OFFSET                  XIN_IMR_OFFSET      /* Interrupt Mode Register, only for Fast Interrupt */
#define INT_ILR_OFFSET                  XIN_ILR_OFFSET      /* Interrupt level register */
#define INT_IVAR_OFFSET                 XIN_IVAR_OFFSET     /* Interrupt Vector Address Register */

#define UBLAZE_CONTROL_OFFSET           (0)

/*==========================================================================================*/
/*================================== REGISTER DEFINITIONS ==================================*/
/*==========================================================================================*/
#define REG_DDR_DBG                     (DDR_BASE_ADDR + DDR_DBG_OFFSET)

#define REG_ULI_RX                      (ULI_BASE_ADDR + ULI_RX_OFFSET)
#define REG_ULI_TX                      (ULI_BASE_ADDR + ULI_TX_OFFSET)
#define REG_ULI_STATUS                  (ULI_BASE_ADDR + ULI_STATUS_OFFSET)
#define REG_ULI_CTRL                    (ULI_BASE_ADDR + ULI_CTRL_OFFSET)

#define REG_DCO_START                   (DCO_BASE_ADDR + DCO_START_ACQ_OFFSET)
#define REG_DCO_STATUS                  (DCO_BASE_ADDR + DCO_STATUS_OFFSET)
#define REG_DCO_CONTROL                 (DCO_BASE_ADDR + DCO_CONTROL_OFFSET)
#define REG_DCO_CAL_START               (DCO_BASE_ADDR + DCO_CAL_START_OFFSET)
#define REG_DCO_CAL_END                 (DCO_BASE_ADDR + DCO_CAL_END_OFFSET)
#define REG_DCO_PROG_START              (DCO_BASE_ADDR + DCO_PROG_START_OFFSET)
#define REG_DCO_PROG_END                (DCO_BASE_ADDR + DCO_PROG_END_OFFSET)

#define REG_DES_CONTROL                 (DES_BASE_ADDR + DES_CONTROL_OFFSET)
#define REG_DES_STATUS                  (DES_BASE_ADDR + DES_STATUS_OFFSET)

#define REG_PPA_CONTROL                 (PPA_BASE_ADDR + PPA_CONTROL_OFFSET)

#define REG_TXT_START_BURST             (TXT_BASE_ADDR + TXT_START_BURST_OFFSET)
#define REG_TXT_MASK                    (TXT_BASE_ADDR + TXT_MASK_OFFSET)
#define REG_TXT_BURST_LENGTH            (TXT_BASE_ADDR + TXT_BURST_LENGTH_OFFSET)
#define REG_SCRATCH_REG                 (TXT_BASE_ADDR + SCRATCH_OFFSET)
#define REG_TXT_CURRENT_ROW             (TXT_BASE_ADDR + TXT_CURRENT_ROW_OFFSET)
#define REG_TXT_COARSE_DELAY            (TXT_BASE_ADDR + TXT_COARSE_DELAY_OFFSET)
#define REG_TXT_FINE_DELAY              (TXT_BASE_ADDR + TXT_FINE_DELAY_OFFSET)
#define REG_TXT_LUT_START               (TXT_BASE_ADDR + TXT_LUT_START_OFFSET)
#define REG_TXT_LUT_END                 (TXT_BASE_ADDR + TXT_LUT_END_OFFSET)

#define REG_MSM_START_ACQ               (MSM_BASE_ADDR + MSM_START_ACQ_OFFSET)
#define REG_MSM_COL_DEPTH               (MSM_BASE_ADDR + MSM_COL_DEPTH_OFFSET)
#define REG_MSM_STATUS                  (MSM_BASE_ADDR + MSM_STATUS_OFFSET)
#define REG_MSM_CONTROL                 (MSM_BASE_ADDR + MSM_CONTROL_OFFSET)
#define REG_MSM_WDW                     (MSM_BASE_ADDR + MSM_WINDOW_OFFSET)
#define REG_MSM_RANGE_CTRL              (MSM_BASE_ADDR + MSM_RANGE_CTRL_OFFSET)
#define REG_MSM_RANGE_VALUE             (MSM_BASE_ADDR + MSM_RANGE_VALUE_OFFSET)

#define REG_PAT_CONTROL                 (PAG_BASE_ADDR + PAG_CONTROL_OFFSET)
#define REG_PAT_VERSION                 (PAG_BASE_ADDR + PAG_VERSION_OFFSET)

#define REG_OFO_READ_DATA               (OFO_BASE_ADDR + OFO_READ_DATA_OFFSET)
#define REG_OFO_WRITE_DATA              (OFO_BASE_ADDR + OFO_WRITE_DATA_OFFET)
#define REG_OFO_STATUS                  (OFO_BASE_ADDR + OFO_STATUS_OFFSET)
#define REG_OFO_CONTROL                 (OFO_BASE_ADDR + OFO_CONTROL_OFFSET)
#define REG_OFO_VERSION                 (OFO_BASE_ADDR + OFO_VERSION_OFFSET)

#define REG_DMA_CR                      (DMA_BASE_ADDR + DMA_CR_OFFSET)
#define REG_DMA_SR                      (DMA_BASE_ADDR + DMA_SR_OFFSET)
#define REG_DMA_CDESC                   (DMA_BASE_ADDR + DMA_CDESC_OFFSET)
#define REG_DMA_TDESC                   (DMA_BASE_ADDR + DMA_TDESC_OFFSET)
#define REG_DMA_SRCADDR                 (DMA_BASE_ADDR + DMA_SRCADDR_OFFSET)
#define REG_DMA_DSTADDR                 (DMA_BASE_ADDR + DMA_DSTADDR_OFFSET)
#define REG_DMA_BTT                     (DMA_BASE_ADDR + DMA_BTT_OFFSET)

#define REG_IQ_CONTROL                  (IQ_BASE_ADDR + IQ_CONTROL_OFFSET)
#define REG_IQ_DEC_NUM                  (IQ_BASE_ADDR + IQ_DEC_NUM_OFFSET)
#define REG_IQ_BP_FIR_COEF_x            (IQ_BASE_ADDR + IQ_BP_FIR_COEF_x_OFFSET)
#define REG_IQ_LP_FIR_COEF_x            (IQ_BASE_ADDR + IQ_LP_FIR_COEF_x_OFFSET)
#define REG_IQ_DIG_GAIN_x               (IQ_BASE_ADDR + IQ_DIG_GAIN_x_OFFSET)

#define REG_IIC_DGIER                   (IIC_BASE_ADDR + IIC_DGIER_OFFSET)
#define REG_IIC_IISR                    (IIC_BASE_ADDR + IIC_IISR_OFFSET)
#define REG_IIC_IIER                    (IIC_BASE_ADDR + IIC_IIER_OFFSET)
#define REG_IIC_RESETR                  (IIC_BASE_ADDR + IIC_RESETR_OFFSET)
#define REG_IIC_CR_REG                  (IIC_BASE_ADDR + IIC_CR_OFFSET)
#define REG_IIC_SR_REG                  (IIC_BASE_ADDR + IIC_SR_OFFSET)
#define REG_IIC_DTR_REG                 (IIC_BASE_ADDR + IIC_DTR_OFFSET)
#define REG_IIC_DRR_REG                 (IIC_BASE_ADDR + IIC_DRR_OFFSET)
#define REG_IIC_ADR_REG                 (IIC_BASE_ADDR + IIC_ADR_OFFSET)
#define REG_IIC_TFO_REG                 (IIC_BASE_ADDR + IIC_TFO_OFFSET)
#define REG_IIC_RFO_REG                 (IIC_BASE_ADDR + IIC_RFO_OFFSET)
#define REG_IIC_TBA_REG                 (IIC_BASE_ADDR + IIC_TBA_OFFSET)
#define REG_IIC_RFD_REG                 (IIC_BASE_ADDR + IIC_RFD_OFFSET)
#define REG_IIC_GPO_REG                 (IIC_BASE_ADDR + IIC_GPO_OFFSET)

#define REG_BIG_SW_RST                  (BIG_BASE_ADDR + BIG_SW_RST_OFFSET)
#define REG_BIG_CLK_STATUS              (BIG_BASE_ADDR + BIG_CLK_STATUS_OFFSET)
#define REG_BIG_CLK_SELECT              (BIG_BASE_ADDR + BIG_CLK_SELECT_OFFSET)
#define REG_BIG_VERSION                 (BIG_BASE_ADDR + BIG_VERSION_OFFSET)
#define REG_BIG_TXT_VER                 (BIG_BASE_ADDR + BIG_TXT_VER_OFFSET)
#define REG_BIG_DES_VER                 (BIG_BASE_ADDR + BIG_DES_VER_OFFSET)
#define REG_BIG_DCO_VER                 (BIG_BASE_ADDR + BIG_DCO_VER_OFFSET)
#define REG_BIG_REG_VER                 (BIG_BASE_ADDR + BIG_REG_VER_OFFSET)
#define REG_BIG_ERR_STATUS              (BIG_BASE_ADDR + BIG_ERR_STATUS_OFFSET)

#define REG_FT_READ_RX                  (FT_BASE_ADDR + FT_READ_RX_OFFSET)
#define REG_FT_WRITE_TX                 (FT_BASE_ADDR + FT_WRITE_TX_OFFSET)
#define REG_FT_STATUS                   (FT_BASE_ADDR + FT_STATUS_OFFSET)
#define REG_FT_CONTROL                  (FT_BASE_ADDR + FT_CONTROL_OFFSET)
#define REG_FT_VERSION                  (FT_BASE_ADDR + FT_VERSION)

#define REG_LED_DATA                    (LCD_LED_BASE_ADDR + LED_DATA_OFFSET)
#define REG_LED_DIR                     (LCD_LED_BASE_ADDR + LED_DIR_OFFSET)
#define REG_LCD_DATA                    (LCD_LED_BASE_ADDR + LCD_DATA_OFFSET)
#define REG_LCD_DIR                     (LCD_LED_BASE_ADDR + LCD_DIR_OFFSET)

#define REG_DIPS_DATA                   (PB_SW_BASE_ADDR + DIPS_DATA_OFFSET)
#define REG_DIPS_DIR                    (PB_SW_BASE_ADDR + DIPS_DIR_OFFSET)
#define REG_PUSH_DATA                   (PB_SW_BASE_ADDR + PUSH_DATA_OFFSET)
#define REG_PUSH_DIR                    (PB_SW_BASE_ADDR + PUSH_DIR_OFFSET)

#define REG_TIM_TCSR0                   (TIM_BASE_ADDR + TIM_TCSR0_OFFSET)
#define REG_TIM_TLR0                    (TIM_BASE_ADDR + TIM_TLR0_OFFSET)
#define REG_TIM_TCR0                    (TIM_BASE_ADDR + TIM_TCR0_OFFSET)
#define REG_TIM_TCSR1                   (TIM_BASE_ADDR + TIM_TCSR1_OFFSET)
#define REG_TIM_TLR1                    (TIM_BASE_ADDR + TIM_TLR1_OFFSET)
#define REG_TIM_TCR1                    (TIM_BASE_ADDR + TIM_TCR1_OFFSET)

#define REG_TIM2_TCSR0                  (TIM2_BASE_ADDR + TIM2_TCSR0_OFFSET)
#define REG_TIM2_TLR0                   (TIM2_BASE_ADDR + TIM2_TLR0_OFFSET)
#define REG_TIM2_TCR0                   (TIM2_BASE_ADDR + TIM2_TCR0_OFFSET)
#define REG_TIM2_TCSR1                  (TIM2_BASE_ADDR + TIM2_TCSR1_OFFSET)
#define REG_TIM2_TLR1                   (TIM2_BASE_ADDR + TIM2_TLR1_OFFSET)
#define REG_TIM2_TCR1                   (TIM2_BASE_ADDR + TIM2_TCR1_OFFSET)

#define REG_INT_ISR_OFFSET              (INT_BASE_ADDR + INT_ISR_OFFSET)        /* Interrupt Status Register */
#define REG_INT_IPR_OFFSET              (INT_BASE_ADDR + INT_IPR_OFFSET)        /* Interrupt Pending Register */
#define REG_INT_IER_OFFSET              (INT_BASE_ADDR + INT_IER_OFFSET)        /* Interrupt Enable Register */
#define REG_INT_IAR_OFFSET              (INT_BASE_ADDR + INT_IAR_OFFSET)        /* Interrupt Acknowledge Register */
#define REG_INT_SIE_OFFSET              (INT_BASE_ADDR + INT_SIE_OFFSET)        /* Set Interrupt Enable Register */
#define REG_INT_CIE_OFFSET              (INT_BASE_ADDR + INT_CIE_OFFSET)        /* Clear Interrupt Enable Register */
#define REG_INT_IVR_OFFSET              (INT_BASE_ADDR + INT_IVR_OFFSET)        /* Interrupt Vector Register */
#define REG_INT_MER_OFFSET              (INT_BASE_ADDR + INT_MER_OFFSET)        /* Master Enable Register */
#define REG_INT_IMR_OFFSET              (INT_BASE_ADDR + INT_IMR_OFFSET)        /* Interrupt Mode Register, only for Fast Interrupt */
#define REG_INT_ILR_OFFSET              (INT_BASE_ADDR + INT_ILR_OFFSET)        /* Interrupt level register */
#define REG_NT_IVAR_OFFSET              (INT_BASE_ADDR + INT_IVAR_OFFSET)       /* Interrupt Vector Address Register */

#define REG_UBLAZE_CONTROL              (UBL_BASE_ADDR + UBLAZE_CONTROL_OFFSET)

/*==========================================================================================*/
/*=============================== Miscelaneous definitions =================================*/
/*==========================================================================================*/

#if(1)
#define WR_OFF32(x, y)
#define RD_OFF32(x)                     0
#define WR_OFF8(x, y)
#define RD_OFF8(x)
#define BR_OUT32(x, y)
#define BR_IN32(x)                      0
#define BR_OUT8(x, y)
#define BR_IN8(x)
#else
#ifdef UBLAZE_FW
#define WR_OFF32(x, y)                  (*(ptemp + ((x)/4)) = (y))
#define RD_OFF32(x)                     (*(ptemp + ((x)/4)))
#define WR_OFF8(x, y)                   (*((u8*)ptemp + (x))) = (y))
#define RD_OFF8(x)                      (*((u8*)ptemp + (x)))
#define BR_OUT32(x, y)                  (*(volatile u32*)(x) = (y))
#define BR_IN32(x)                      (*(volatile u32*)(x))
#define BR_OUT8(x, y)                   (*(volatile u8*)(x) = (y))
#define BR_IN8(x)                       (*(volatile u8*)(x))
#else
#define WR_OFF32(x, y)                  Xil_Out32(((u32)ptemp + (x)),(y))
#define RD_OFF32(x)                     Xil_In32((u32)ptemp + (x))
#define WR_OFF8(x, y)                   Xil_Out8(((u32)ptemp + (x)),(y))
#define RD_OFF8(x)                      Xil_In8((u32)ptemp + (x))
#define BR_OUT32(x, y)                  Xil_Out32((u32)(x),(y))
#define BR_IN32(x)                      Xil_In32((u32)(x))
#define BR_OUT8(x, y)                   Xil_Out8((u32)(x),(y))
#define BR_IN8(x)                       Xil_In8((u32)(x))
#endif
#endif

#ifdef PRINTF_VERBOSE
#define DBGPRINTF(...)                  xil_printf(__VA_ARGS__)
#else
#define DBGPRINTF(...)
#endif


#define st(x)                           do { x } while (__LINE__ == -1)

#define DBGLED                          *(u32*)REG_LED_DATA = dbgled++;
#define DBGLEDSHOW(x)                   *(u32*)REG_LED_DATA = x;

/*********************************** RF block definitions ***********************************/
#define RF_BLK_RX           (32)            /* number of RX channels in each RF block */
#define RF_BLK_TX           (32)            /* number of TX channels in each RF block */
#define RF_BLK_SAMP         (128)           /* number of samples in each RF block (deprecated) */
#define RF_BLK_BPS          (2)             /* bytes per sample */
#define RF_BLK_RX_BY_FIFO   (2)             /* number of RX channels included in each sample word */
#define RF_BLK_TX_SAMP      (RF_BLK_SAMP * (RF_BLK_TX / RF_BLK_RX_BY_FIFO))

/************************************ Other definitions ************************************/


#endif  /* __BR_REGS_H__ */
