// json.cpp : Defines the entry point for the console application.
//

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "stdafx.h"
#include "stdlib.h"
#include "stdint.h"

#include "json.h"

#include "protocol/txbf.h"
#include "device/BR_stm.h"
#include "device/BR_tx.h"

#ifdef _WIN32
#define MFP      "c:/repos/elle_scripts/octave/nosvc/"
#else
#define MFP		"/home/eddie/git/elle_scripts/Octave/nosvc/"
#endif

//#define MF      "hf_single.bca"
//#define MF      "rxhfff.bca"
#define MF      "test.bca"

#define	DBGSTAMP	do{printf("line: %d\n", __LINE__);} while(0);

BF_STORE_T gBfStore;
u8 gSTMParam[8];
//u32 gTxBfMask = 1;

size_t gDDR3HeapUsed;  //for profiling
void* ddr3malloc(size_t size, int zero, void * user_data)
{
	return zero ? calloc (1, size) : malloc (size);
}

void ddr3free(void * ptr, void * user_data)
{
	free (ptr);
}

int main(int argc, char* argv[])
{
	char * s, *ss, atmp[64] = {0}, c;
	FILE *dst;
	uint32_t nl, *patmp;

	s = (char*) malloc(1024*128);
	ss = s;

	dst = fopen((argc > 1) ? argv[1]:MFP MF, "r");
	if(!dst) return -1;

	BR_STM_initParams();

	patmp = (uint32_t*)atmp;
	patmp[0] = 5; patmp[1] = 31; patmp[2] = 15; patmp[3] = 0;
	BR_TX_Config(patmp, TX_CFGBFMASK | TX_CFGBFLATCH | TX_CFGBFSTOREINI);
    ERRORPRINTFV("%d --------------\n", __LINE__);

    nl = 0;
	do
	{
		fscanf(dst, "%c", &c);
		if(!feof(dst))
		{
			*ss++ = c;
			nl++;
		}
	}while(!feof(dst));
	*ss = 0;
	nl++;

	txbf_initialize();
 	decode_dtd_ex(s, &gBfStore, nl, atmp);
 	txbf_normalize(41666666);
 	if (gBfStore.debug & DEBUG_PRINTBFSTORE) dumpBFStore();
 	BR_STM_store_init();

	printf("%s", atmp);
//	getchar();				/* wait here for a key press */

	return 0;
}

/** Breds translators **/

