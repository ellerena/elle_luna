/**
 * @author Eddie Llerena
 * @version 0.1
 * @brief  A Linux user space program that communicates with the h2tchar.c LKM. It passes a
 * string to the LKM and reads the response from the LKM. For this example to work the device
 * must be called /dev/h2tchar.
 * @see http://www.derekmolloy.ie/ for a full description and follow-up descriptions.
*/
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>

#define BUFFER_LENGTH   (256)       ///< The buffer length (crude but fine)
static char buf[BUFFER_LENGTH] = {0};     ///< The receive buffer from the LKM

#define DEVICE_NAME     "dKOTest"

int main()
{
   size_t cnt;
   int fd;

   printf("Kernel Module Dev Tester %s\nDevice name:",
         __DATE__ " " __TIME__);
   fgets(buf, 50, stdin);
   buf[strlen(buf)-1] = 0;
   if (buf[0] == '.') return 0;

   printf("Opening %s [%zu]\n", buf, strlen(buf));
   if ((fd = open(buf, O_RDWR)) < 0) {
      printf("%s file open failed\n", buf);
      return -1;
   }

   while (1){

      printf("string: ");
      scanf("%[^\n]%*c", buf);             // Read in a string (with spaces)
      if (buf[0] == '.') break;

      cnt = write(fd, buf, strlen(buf));   // Send the string to the LKM
      if (cnt < strlen(buf))
      {
         printf("Failed to write the message to the device.");
         break;
      }

      cnt = read(fd, buf, BUFFER_LENGTH);  // Read the response from the LKM
      printf("\tReceived: %zu\n\t", cnt);
      fwrite (buf, sizeof(char), cnt, stdout);
   }

   if(fd > 0) close (fd);

   printf("End of the program\n");
   return 0;
}
