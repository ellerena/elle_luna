/**
 * @file   h3a.c
 * @author Ezequiel Llerena
*/

#include <linux/init.h>           // Macros used to mark up functions e.g. __init __exit
#include <linux/module.h>         // Core header for loading LKMs into the kernel
#include <linux/device.h>         // Header to support the kernel Driver Model
#include <linux/kernel.h>         // Contains types, macros, functions for the kernel
#include <linux/fs.h>             // Header for the Linux file system support
#include <linux/uaccess.h>        // Required for the copy to user function

#include <linux/mod_devicetable.h>
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <linux/io.h>
#include <linux/interrupt.h>
#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of_platform.h>

#define DRIVER_NAME		"KO_devTestModule"
#define DEVICE_NAME     "dKOTest"
#define CLASS_NAME      "cKOTest"
#define BUFSZ           (256u)

/**
 * @brief Simple function to 'do something'.
 *          in this case, it reverts the
 *          chars of the received array
 */
void revert_array(char * array, size_t size);

static int    maj;                           // Stores the device number -- determined automatically
static char   mdat[BUFSZ] = "Hello World";   // Memory for the string that is passed from userspace
static size_t mlen = 12;                     // Used to remember the size of the string stored
static struct class*  cdClass  = NULL;       // The device-driver class struct pointer
static struct device* cdDevice = NULL;       // The device-driver device struct pointer
//static struct cdev* cdev = NULL;

static int     cd_open(struct inode *, struct file *);
static int     cd_release(struct inode *, struct file *);
static ssize_t cd_read(struct file *, char *, size_t, loff_t *);
static ssize_t cd_write(struct file *, const char *, size_t, loff_t *);

static struct file_operations fo = {
    .open = cd_open,
    .write = cd_write,
    .read = cd_read,
    .release = cd_release,
};

static int __init cd_init(void)
{
    printk(KERN_INFO DRIVER_NAME ".init " __DATE__ " " __TIME__ );

    // Try to dynamically allocate a major number for the device -- more difficult but worth it
    maj = register_chrdev(0, DEVICE_NAME, &fo);
    if (maj<0){
       printk(KERN_ALERT "EBBChar failed to register a major number");
       return maj;
    }
    printk(KERN_INFO "major number: %d", maj);

    // Register the device class
    cdClass = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(cdClass)){                // Check for error and clean up if there is
       unregister_chrdev(maj, DEVICE_NAME);
       printk(KERN_ALERT "Failed to register device class");
       return PTR_ERR(cdClass);          // Correct way to return an error on a pointer
    }
    printk(KERN_INFO "device class registered correctly");

    // Register the device driver
    cdDevice = device_create(cdClass, NULL, MKDEV(maj, 0), NULL, DEVICE_NAME);
    if (IS_ERR(cdDevice)){               // Clean up if there is an error
       class_destroy(cdClass);           // Repeated code but the alternative is goto statements
       unregister_chrdev(maj, DEVICE_NAME);
       printk(KERN_ALERT "Failed to create the device");
       return PTR_ERR(cdDevice);
    }

    printk(KERN_INFO "device class created correctly"); // Made it! device was initialized
    return 0;
}
 
static void __exit cd_exit(void)
{
    device_destroy(cdClass, MKDEV(maj, 0));     // remove the device
    class_unregister(cdClass);                          // unregister the device class
    class_destroy(cdClass);                             // remove the device class
    unregister_chrdev(maj, DEVICE_NAME);             // unregister the major number
    printk(KERN_INFO DRIVER_NAME ".exit");
}
 
static int cd_open(struct inode *inodep, struct file *filep)
{
    printk(KERN_INFO DEVICE_NAME ".open");
    return 0;
}

static ssize_t cd_write(struct file *filep, const char *buffer, size_t len, loff_t *offset)
{
   printk(KERN_INFO DEVICE_NAME ".write");

   mlen = len > BUFSZ ? BUFSZ : len;
   printk ("Received %zu, Copying %zu", len, mlen);

   if(0 == copy_from_user(mdat, buffer, mlen))  /* data transfer ok */
   {
      mdat[mlen-1] = '\n';
   }
   else
   {
      printk("FAIL!");
      mlen = 0;
   }

   return len;
}

static ssize_t cd_read(struct file *filep, char *buffer, size_t len, loff_t *offset)
{
   printk(KERN_INFO DEVICE_NAME ".read");

   revert_array(mdat, mlen-1);
   printk("%s", mdat);
   len = (mlen > BUFSZ) ? BUFSZ : mlen;
   copy_to_user(buffer, mdat, len);

   return len;   /* must return bytes transferred to user */
}

static int cd_release(struct inode *inodep, struct file *filep)
{
    printk(KERN_INFO DEVICE_NAME ".release");
    return 0;
}

module_init(cd_init);
module_exit(cd_exit);

MODULE_LICENSE("GPL");              ///< The license type -- this affects runtime behavior
MODULE_AUTHOR("Eddie Llerena");      ///< The author -- visible when you use modinfo
MODULE_DESCRIPTION("Driver Module");  ///< The description -- see modinfo
MODULE_VERSION("0.1");              ///< The version of the module
MODULE_PARM_DESC(name, "The name to display in /var/log/kern.log");  ///< parameter description

/******* utility functions *******/
void revert_array(char * array, size_t size)
{
   char *p, *q;

   p = array;              /* point to fist byte */
   q = &array[size - 1];   /* point to last byte */

   while (p < q)
   {
      if (*p != *q) {
         *p ^= *q;
         *q ^= *p;
         *p ^= *q;
      }
      p++;
      q--;
   }
}





