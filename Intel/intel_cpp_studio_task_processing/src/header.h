#ifndef HEADER_MAIN
#define HEADER_MAIN

#include <iostream>
using namespace std;

void addnote (struct node*);
struct node * ultimo(void);
void creacion (void*);

struct node {
		int ID;
		int * payload;
		void (*routine) (void *);
		struct node * pnext;
		node() {
			ID = 1;
			payload = new int;
			routine = NULL;
			pnext = NULL;
		}
		node(int i1, int *i2, void(*i3)(void*), struct node * i4) {
					ID = i1;
					payload = i2;
					routine = i3;
					pnext = i4;
				}
};

#endif
