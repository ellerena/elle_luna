#include "header.h"

int a = 100, * i = &a;
struct node *n, n0(0, i, NULL, NULL);
static int nc = 1;

int main (void) {
	creacion(&n0);
	for (int j = 0; j < 5; j++) {
		n = new struct node(nc++, new int, creacion, NULL);
		addnote(n);
	}
}

void addnote (struct node* nn) {
	ultimo()->pnext = nn;
	nn->routine (nn);
}

struct node * ultimo(void) {
	n = &n0;
	while (n->pnext != NULL) n = n->pnext;
	return n;
}

void creacion (void * i) {
	struct node * n = static_cast<struct node*>(i);
	cout << "Creacion #" << n->ID<< "; " << *(n->payload) << "; " << n->pnext << endl;
}
