/*
 * csv2regmap.c
 *
 *  Created on: Sep 29, 2021
 *      Author: ellerena
 *
 *  Description:
 *  	This program is used to generate a .regmap file from
 *  	a .csv that can be used with the fwregmap utility
 *
 *  	execution:
 *  	$mac_gcc_base <.csv_filename_including_path>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define srcfile "/Users/ellerena/Downloads/sx9200_regmap.csv"
#define startstr "{\n"\
"    \"default-endian\": \"be\",\n"\
"    \"default-reg-size\": 4,\n"\
"    \"reg-address-size\": 2,\n"\
"    \"name-prefix\": \"sx9200_\",\n"\
"\n"\
"    \"constants\": [\n"\
"        (\"ID\", 0x00920024),\n"\
"        (\"DEVICE_RESET_VAL\", 0xDE),\n"\
"    ],\n"\
"\n"\
"    \"registers\": [\n"

#define endstr "    ],\n"\
"}"

#define CHKR(fp)	if (fp) {printf("%d\n", __LINE__); return __LINE__;}
#define LINE_MAX	254
#define FIELDS 4

// line parser
void getfield (char * line, int num, char ** field)
{
  *field++ = strtok(line, ",");;

  while (--num)
  {
    *field++ = strtok(NULL, ",\n");
  }
}

int main (int argc, char ** argv)
{
  FILE *fp = fopen(argc > 1 ? argv[1] : srcfile, "r");
  CHKR(NULL == fp);

  char line[LINE_MAX];
  char * field[FIELDS];

  printf("%s", startstr);
  fgets(line, LINE_MAX, fp);
  while (fgets(line, LINE_MAX, fp))
  {
//    printf("%s", line);
    getfield(line, FIELDS, field);
    printf("        {\n");
    printf("            \"name\": \"%s\",\n", field[0]);
    printf("            \"address\": %s,\n", field[1]);
    printf("            \"doc\": \"%s\",\n", field[2]);
    printf("            \"readonly\": %s,\n", field[3]);
    printf("        },\n");
  }

  printf("%s", endstr);

  fclose(fp);

  return 0;
}
