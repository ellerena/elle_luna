/*
 * main.c
 *
 *  Created on: Sep 29, 2021
 *      Author: ellerena
 *
 *  Description:
 *  	This program is used to clean eclipse .cproject files that may exhibit
 *  	a bad formatting issue. This issue happens sometimes when a project is
 *  	modified in Eclipse. The .cproject is re-formatted showing extra lines
 *  	that contain no code/text. In order to resolve this situation, this
 *  	program received the path to the target .cprojet, reads its contents
 *  	and re-creates the file eliminating the extra empty lines.
 *
 *  	execution:
 *  	$mac_gcc_base <.cproject_filename_including_path>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CHKR(fp)	if (fp) {printf("%d\n", __LINE__); return __LINE__;}
#define PRNL(...) do{ printf("[%6d] ", __LINE__); printf(__VA_ARGS__); printf("\n");} while(0)
#define LINE_MAX	1024

int main(int argc, char **argv)
{
	// validate input argument
	CHKR (argc <= 1);
	PRNL("source file: %s", argv[1]);

	// open file and also retrieve its size
	FILE * fp = fopen(argv[1], "r");
	CHKR (NULL == fp);
	fseek(fp, 0L, SEEK_END);
	int sz = ftell(fp);
	rewind(fp);

	// allocate enough ram for the entire file
	char * target = malloc(sz);
	CHKR (NULL == target);

	// read each line of the file, placing it in ram
	char * tar = target;
	while (!feof(fp))
	{
		fgets(tar, LINE_MAX, fp);

		// flag out if the line contains valid text
		char * temp = tar;
		int flag = 0;
		char c;
		do
		{
			c = *temp++;
			if (c > 32)
			{
				flag = 1;
				break;
			}
		} while (c);

		// if line is valid, accept it and advance pointer
		if (flag)
		{
			tar += strlen(tar);
			flag = 0;
		}
	}

	// close and reopen the file and copy our array into it.
	fclose(fp);
	fp = fopen(argv[1], "wb");
	CHKR(NULL == fp);
	sz = tar - target;
	fwrite(target, sizeof(char), sz, fp);
	fclose(fp);
	free(target);

	PRNL("all done!");
	return 0;
}
