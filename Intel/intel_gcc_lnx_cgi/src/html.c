/*
 Name        : main.c
 Author      : elle
 Copyright   : Your copyright notice
 Description : Hello World in C
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define xstr(s) str(s)
#define str(s) #s

int main(void)
{
	char hostname[20], *query, *host;
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	gethostname(hostname, 19);
	query = getenv("QUERY_STRING");
	host = getenv("HTTP_HOST");
	printf("Content-type: text/html\n\n");
	printf("<html>\n");
	printf("<header><title>Hello C/C++\n</title></header>");
	printf("<body><p>\n");
/***************************** BODY BEGIN *****************************/
	printf( "==================================<br>\n"\
			"Intel Debian - v1.0 Linux Test\n" xstr(PROJNAME) \
			" - " xstr(CONFIGNAME) " build alive!<br>\n"\
			"build " __DATE__ " " __TIME__ "<br>\n"\
			"==================================<br><br>\n");

	printf("Soy %s<br>\n\n", hostname);
	printf("Host: %s<br>\n", host);
	printf("Query: %s<br><br>\n", query);
	printf("Last run: %d-%d-%d %d:%d:%d<br><br>\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	printf("Current LED status must be %s<br><br>\n", *query == '1' ? "ON":"OFF");
	printf("<a href=\"http://%s/home/ctest?1\">\n", host);
	printf("Turn LED ON</a><br>\n");
	printf("<a href=\"http://%s/home/ctest?0\">\n", host);
	printf("Turn LED OFF</a><br><br>\n");
	printf("Gracias.");
/***************************** BODY END *****************************/
	printf("</p></body>");
	printf("</html>");

  return 0;
}

