/*
 ============================================================================
 Name        : hw_vs.c
 Author      : elle
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

// Due to odd GPIO mapping, a bit reversal is required for data bits
unsigned bit_reverse(unsigned v)
{
	unsigned r = v; // r will be reversed bits of v; first get LSB of v
	unsigned s = sizeof(v) * 32 - 1; // extra shift needed at end

	for (v >>= 1; v; v >>= 1)
	{
	  r <<= 1;
	  r |= v & 1;
	  s--;
	}
	r <<= s; // shift when v's highest bits are zero

	return r;
}


int main(void)
{
	unsigned a;

	while(1)
	{
		printf("!!!Hello World!!!\r\nEnter an hexadecimal integer:");
		scanf("%x", &a);
		if(!a) break;
		printf("\nReversed: 0x%08X\r\n", bit_reverse(a));
	}
	system("pause");
	return 0;
}
