///  Copyright (c) 2008 - 2009 Advanced Micro Devices, Inc.
#include "header.h"

int main (int c,char* k[],char* s[])
{
	if (r = ePrepareADLs()) return r;
	if (ADL_OK != ADL_Main_Control_Create(ADL_Main_Memory_Alloc, 1)) return 3; // Initialize ADL. 1:: only present adapters
	if (ADL_OK != ADL_Adapter_NumberOfAdapters_Get(&iNumberAdapters)) return 4; // Get number of adapters
	if (iNumberAdapters)
	{
		lpAdapterInfo = (LPAdapterInfo) malloc (sizeof (AdapterInfo) * iNumberAdapters);
		memset ( lpAdapterInfo,'\0', sizeof (AdapterInfo) * iNumberAdapters );
		ADL_Adapter_AdapterInfo_Get(lpAdapterInfo, sizeof (AdapterInfo) * iNumberAdapters); // Get AdapterInfo structure for all adapters
	}
	for ( i = 0; i < iNumberAdapters; i++ )
	{
		iAdapterIndex = lpAdapterInfo[i].iAdapterIndex;
		if (c==1)
		{
			printf("#%i; BUS:%i, DEV:%i, FN:%i, Exst:%i, Prsnt:%i, %s\n", lpAdapterInfo[i].iAdapterIndex, lpAdapterInfo[i].iBusNumber, lpAdapterInfo[i].iDeviceNumber, lpAdapterInfo[i].iFunctionNumber, lpAdapterInfo[i].iExist, lpAdapterInfo[i].iPresent, lpAdapterInfo[i].strAdapterName);
//			ADL_Main_Memory_Free ( &lpAdlDisplayInfo );
			if(ADL_OK == ADL_Display_DisplayInfo_Get(lpAdapterInfo[i].iAdapterIndex, &iNumDisplays, &lpAdlDisplayInfo, 0))
			{
				printf("[%i]", iNumDisplays);
				for(j=0; j<iNumDisplays; j++)
					printf("\t#%i: Typ:%s, Out:%s, Con:%s, State:%.8x, %s\n", j, dtype[lpAdlDisplayInfo[j].iDisplayType], dout[lpAdlDisplayInfo[j].iDisplayOutputType], dcon[lpAdlDisplayInfo[j].iDisplayConnector], lpAdlDisplayInfo[j].iDisplayInfoValue, lpAdlDisplayInfo[j].strDisplayName);
			}
			else
				printf("\tDisplay info not accessible\n");
		}
	}

//	ADL_Main_Memory_Free ( &lpAdapterInfo );
//	ADL_Main_Memory_Free ( &lpAdlDisplayInfo );
	ADL_Main_Control_Destroy ();
	FreeLibrary(hDLL);
	system("Pause");
	return 0;
}

int ePrepareADLs()
{
	int r=0;

	if ((hDLL = LoadLibrary("atiadlxx.dll")) == NULL)
		if (NULL == (hDLL = LoadLibrary("atiadlxy.dll")))
			return 1; // if 32bit application on 64bit OS then use atiadlxy.dll

	ADL_Main_Control_Create = (ADL_MAIN_CONTROL_CREATE) GetProcAddress(hDLL,"ADL_Main_Control_Create");
	ADL_Main_Control_Destroy = (ADL_MAIN_CONTROL_DESTROY) GetProcAddress(hDLL,"ADL_Main_Control_Destroy");
	ADL_Adapter_NumberOfAdapters_Get = (ADL_ADAPTER_NUMBEROFADAPTERS_GET) GetProcAddress(hDLL,"ADL_Adapter_NumberOfAdapters_Get");
	ADL_Adapter_AdapterInfo_Get = (ADL_ADAPTER_ADAPTERINFO_GET) GetProcAddress(hDLL,"ADL_Adapter_AdapterInfo_Get");
	ADL_Display_DisplayInfo_Get = (ADL_DISPLAY_DISPLAYINFO_GET) GetProcAddress(hDLL,"ADL_Display_DisplayInfo_Get");
	ADL_Display_NumberOfDisplays_Get = (ADL_DISPLAY_NUMBEROFDISPLAYS_GET) GetProcAddress(hDLL, "ADL_Display_NumberOfDisplays_Get");
	ADL_Adapter_Crossfire_Caps = (ADL_ADAPTER_CROSSFIRE_CAPS) GetProcAddress(hDLL, "ADL_Adapter_Crossfire_Caps");
	ADL_Adapter_Crossfire_Get = (ADL_ADAPTER_CROSSFIRE_GET) GetProcAddress(hDLL, "ADL_Adapter_Crossfire_Get");
	ADL_Adapter_Crossfire_Set = (ADL_ADAPTER_CROSSFIRE_SET) GetProcAddress(hDLL, "ADL_Adapter_Crossfire_Set");

	if (NULL == ADL_Main_Control_Create || NULL == ADL_Main_Control_Destroy || NULL == ADL_Adapter_NumberOfAdapters_Get ||
		NULL == ADL_Adapter_AdapterInfo_Get || NULL == ADL_Display_DisplayInfo_Get || NULL == ADL_Adapter_Crossfire_Caps ||
		NULL == ADL_Adapter_Crossfire_Get || NULL == ADL_Adapter_Crossfire_Set || NULL == ADL_Display_NumberOfDisplays_Get)
		r=2;
	return r;
}
