///  Copyright (c) 2008 - 2009 Advanced Micro Devices, Inc.
/// Returned errorlevels
/// 1: ADL library not found!
/// 2: ADL's API is missing!
/// 3: ADL Initialization Error!
/// 4: Cannot get the number of adapters!

#include <windows.h>
#include <tchar.h>
#include "adl_sdk.h"
#include <stdio.h>

// Function pointers
typedef int (*ADL_MAIN_CONTROL_CREATE)(ADL_MAIN_MALLOC_CALLBACK, int );
typedef int (*ADL_MAIN_CONTROL_DESTROY)();
typedef int (*ADL_ADAPTER_NUMBEROFADAPTERS_GET) ( int* );
typedef int (*ADL_ADAPTER_ADAPTERINFO_GET) ( LPAdapterInfo, int );
typedef int (*ADL_DISPLAY_DISPLAYINFO_GET) ( int, int *, ADLDisplayInfo **, int );
typedef int (*ADL_DISPLAY_NUMBEROFDISPLAYS_GET) ( int, int *);
typedef int (*ADL_ADAPTER_CROSSFIRE_CAPS) ( int, int *, int *, ADLCrossfireComb ** );
typedef int (*ADL_ADAPTER_CROSSFIRE_GET) ( int, ADLCrossfireComb *, ADLCrossfireInfo * );
typedef int (*ADL_ADAPTER_CROSSFIRE_SET) (int, ADLCrossfireComb * );

void* __stdcall ADL_Main_Memory_Alloc ( int iSize ) // Memory allocation function
{
    void* lpBuffer = malloc ( iSize );
    return lpBuffer;
}

void __stdcall ADL_Main_Memory_Free ( void** lpBuffer ) // Optional Memory de-allocation function
{
    if ( NULL != *lpBuffer )
    {
        free ( *lpBuffer );
        *lpBuffer = NULL;
    }
}

static const char dtype[][4] =
{
	{ 'M', 'O', 'N', '\0' }, { 'T', 'V', 'C', '\0' },	{ 'L', 'C', 'D', '\0' },
	{ 'D', 'F', 'P', '\0' }, { 'C', 'V', 'A', '\0' }, { 'P', 'J', 'T', '\0' },
};

static const char dout[][3] =
{
	{ '?', '?', '\0' }, { 'C', 'P', '\0' },	{ 'S', 'V', '\0' }, { 'A', 'N', '\0' }, { 'D', 'I', '\0' },
};

static const char dcon[][4] =
{
	{ '?', '?', '?', '\0' }, { 'V', 'G', 'A', '\0' },	{ 'D', 'V', 'D', '\0' }, { 'D', 'V', 'I', '\0' },
	{ 'C', 'V', '4', '\0' }, { 'C', 'V', '5', '\0' },	{ 'C', 'V', '6', '\0' }, { 'C', 'V', '7', '\0' },
	{ 'x', 'x', 'x', '\0' }, { 'x', 'x', 'x', '\0' },	{ 'H', 'D', 'A', '\0' }, { 'H', 'D', 'B', '\0' },
	{ 'S', 'V', 'C', '\0' }, { 'C', 'V', 'C', '\0' },	{ 'R', 'C', 'A', '\0' }, { 'D', 'P', ' ', '\0' },
};

HINSTANCE hDLL;		// Handle to DLL
ADL_MAIN_CONTROL_CREATE						ADL_Main_Control_Create;
ADL_MAIN_CONTROL_DESTROY					ADL_Main_Control_Destroy;
ADL_ADAPTER_NUMBEROFADAPTERS_GET	ADL_Adapter_NumberOfAdapters_Get;
ADL_ADAPTER_ADAPTERINFO_GET				ADL_Adapter_AdapterInfo_Get;
ADL_DISPLAY_DISPLAYINFO_GET				ADL_Display_DisplayInfo_Get;
ADL_DISPLAY_NUMBEROFDISPLAYS_GET	ADL_Display_NumberOfDisplays_Get;
ADL_ADAPTER_CROSSFIRE_CAPS				ADL_Adapter_Crossfire_Caps;
ADL_ADAPTER_CROSSFIRE_GET					ADL_Adapter_Crossfire_Get;
ADL_ADAPTER_CROSSFIRE_SET					ADL_Adapter_Crossfire_Set;
LPAdapterInfo											lpAdapterInfo = NULL;
LPADLDisplayInfo									lpAdlDisplayInfo = NULL;
ADLCrossfireInfo									lpAdlCrossfireInfo;
ADLCrossfireComb									*lpAdlCrossfireComb = NULL;
int	i, j, r, iNumberAdapters, iAdapterIndex, iDisplayIndex, iNumDisplays;
int elpPreferred, ellNumComb;

int	ePrepareADLs(void);
