/*
*  Created on: Mar 21, 2020
*      Author: eddie
*/

#include <stdio.h>
#include <stdlib.h>

#define ALGNMT       8u
#define WASTE        (ALGNMT - 1)
#define OFF_T        unsigned char
#define OFF_S        sizeof(OFF_T)
#define TOTSZ(x)     (x + OFF_S + WASTE) /* worst case: waste ALIGMT-1 bytes */

void * align8_up(void * addr)
{
   return (void*)(((size_t)addr + WASTE) & ~(size_t)WASTE);
}

void * malloc8(size_t size)
{
   void *p8 = NULL, *preal;
   OFF_T offset;

   if (size) {

      preal = malloc(TOTSZ(size));
      if (preal) {

         p8 = (void*)((size_t)preal + OFF_S);
         p8 = align8_up(p8);
         offset = (OFF_T)((size_t)p8 - (size_t)preal);
         ((OFF_T*)p8)[-1] = offset;
      }

   }

   return p8;
}

void free8(void * p) {

   int offset = 0;

   offset = ((OFF_T*)p)[-1];
   p = (void*)((size_t)p - offset);

   free(p);
}

int main(void)
{
   void * p;

   for (int i = 10; i < 20; i++)
   {
      p = malloc8(i);
      if (p) {
         free8(p);
      }
   }

   printf("bye\n");
   return 0;
}

