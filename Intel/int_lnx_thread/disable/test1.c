/*
 * test1.c
 *
 *  Created on: Mar 21, 2020
 *      Author: eddie
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

pthread_t th[2];
pthread_mutex_t mtx;

#define MAXC         64
#define ALGNMT       8
#define OFF_T        unsigned char
#define OFF_S        sizeof(OFF_T)
#define TOTSZ(x)     (x + OFF_S + (ALGNMT -1))
#define OFF_V(x,y)   ((y)-((x) + OFF_S)+1u)

static uint32_t tstvar0 = MAXC;

void * align8_up (void * addr)
{
   return (void*) (((size_t)addr + 7u) & ~((size_t)7u));
}

void * malloc8(size_t size)
{
   void * p;
   void * pm = NULL;
   int offset;

   if (size) {

      printf("sz: %zd, Tsz:%zd ", size, TOTSZ(size));
      pm = (void*)malloc(TOTSZ(size)); printf("malloc: %p ", pm);
      if (pm != NULL) {

         p = align8_up((void*)((size_t)pm + OFF_S)); printf("mallox8:%p ", p);
         offset = (int)OFF_V((size_t)pm, (size_t)p); printf("offset: %zd\n", offset);
         ((OFF_T*)p)[-1] = (OFF_T)offset;
         pm = p;

      }

   }

   return pm;
}

void free8(void * p) {
   int offset;
   char * pm;

   pm = (char*)p;
   offset = ((OFF_T*)pm)[-1];
   pm -= offset;

   printf("p:%p, pm:%p, offset:%d", p, pm, offset);

   free (pm);
}

void * procfn(void * arg)
{
   uint32_t ** data;
   uint32_t *src, *dst;
   uint32_t fn, val;
   uint32_t i;
   void * p;

   data = (uint32_t**)arg;

   src = data[0];
   dst = data[1];
   fn = *data[2];

   for (i = 10; i < 20; i++)
   {
      p = malloc8(i);
      if (p) {
         free8(p);
      }
   }


   while (1) {
      pthread_mutex_lock(&mtx);
      val = *src;
      if (val == 0) {
         pthread_mutex_unlock(&mtx);
         break;
      }
      //printf ("fn:%d\tA=%u, ", fn, val);

      val--;
      dst[0] = val;
//      printf("B=%u\t", dst[0]);
//      printf(" pointer: %p\n", align8((void*)(size_t)val));
      malloc8((val));

      pthread_mutex_unlock(&mtx);

   }
   return NULL;
}

int main (void)
{
   uint32_t fn0 = 0, fn1 = 1;
   uint32_t *arg[] = {&tstvar0, &tstvar0, &fn0, &tstvar0, &tstvar0, &fn1};

   printf("Thread process test 1\n");

   pthread_create(&th[0], NULL, procfn, (void*)&arg[0]);
//   pthread_create(&th[0], NULL, procfn, (void*)&arg[3]);

   pthread_join(th[0], NULL);
//   pthread_join(th[1], NULL);

   printf("bye]n");
   return 0;
}




