/*
 * tst.c
 *
 *  Created on: Apr 11, 2020
 *      Author: eddie
 */

#include <stdio.h>

void byVal (int a[], int len) {

	int * p = a;

	while (len--) {
		printf("%d\n",*p++);
	}

}


int main ()
{
	int b[] = {1,2,3,4,5,6,7,8,9,0};

	byVal (b);

	return 0;
}
