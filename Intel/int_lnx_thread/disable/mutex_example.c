/* Example code for starting
 * 2 threads and synchronizing  
 * their operation using a mutex.
 * Date: 12/30/2015
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <sys/time.h>
#include <unistd.h>

#define ASSERT(test) (test) ? assert_good():assert_internal(__FILE__, __LINE__);

pthread_t hthread0;
pthread_t hthread1;
pthread_mutex_t hmtx0;

static unsigned int counter;

void fn_thread0(void);
void fn_thread1(void);
void counter_oper(int thread_num);

void assert_internal(const char* file, int line) {
   printf("Hit ASSERT in file %s, line %d. Aborting\n", file, line);
   exit(1);
}

void assert_good(void) {
   return;
}

void fn_sign0(int signum)
{
   if (signum != SIGINT) {
      printf("Bad signum %d in file %s @ %d\n", signum, __FILE__, __LINE__);
      exit(1);
   }

   printf("SIG - Ending Application\n");

   pthread_cancel(hthread0);
   pthread_cancel(hthread1);

   exit(0);
}

int main(void)
{
   pthread_attr_t attr;
   int status;

   signal(SIGINT, fn_sign0);

   pthread_attr_init(&attr);
   pthread_attr_setstacksize(&attr, 1024*1024);

   counter = 0;

   status = pthread_create(&hthread0, &attr, (void*)&fn_thread0, NULL);
   ASSERT(status == 0);

   status = pthread_create(&hthread1, &attr, (void*)&fn_thread1, NULL);
   ASSERT(status == 0);

   pthread_join(hthread0, NULL);
   pthread_join(hthread1, NULL);

   fn_sign0(SIGINT);

   return 0;
}

void fn_thread0(void)
{
   __useconds_t exec_period_usecs;

   exec_period_usecs = 1000000; /*in micro-seconds*/

   printf("Th1. Execution period = %d us\n", exec_period_usecs);
   while(1) {
      usleep(exec_period_usecs);
      counter_oper(1);
   }
}

void fn_thread1(void)
{
   __useconds_t exec_period_usecs;

   exec_period_usecs = 1000000; /*in micro-seconds*/

   printf("Th2. Execution period = %d us\n", exec_period_usecs);

   while(1) {
      usleep(exec_period_usecs);
      counter_oper(2);
   }
}

void counter_oper(int thread_num)
{
   struct timeval ts;

   gettimeofday(&ts, NULL);

   pthread_mutex_lock(&hmtx0);

   printf("%d.%d\t%ld.%ld\n", thread_num, ++counter, ts.tv_sec, ts.tv_usec);

   pthread_mutex_unlock(&hmtx0);
}

