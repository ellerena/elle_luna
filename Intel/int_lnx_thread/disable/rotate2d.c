/*
 * rotate2d.c
 *
 *  Created on: Apr 15, 2020
 *      Author: eddie
 */

//Given a square matrix, turn it by 90 degrees in anti - clockwise direction without using any extra space.
//
//Examples :
//
//Input:
//Matrix:
//1  2  3  //1 2 3 4 5 6 7 8 9
//4  5  6
//7  8  9
//Output :
//   3  6  9   //3 6 9 2 5 8 1 4 7
//   2  5  8
//   1  4  7
//
//   pItem[0][0] = matrix[2][0]
//   pitem[0][1] = matrix[2][1]
//
//   Input:
//1  2  3  4
//5  6  7  8
//9 10 11 12
//13 14 15 16
//Output :
//   4  8 12 16
//   3  7 11 15
//   2  6 10 14
//   1  5  9 13
//   Explanation : The given matrix is rotated by 90 degree
//   in anti - clockwise direction.

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

/* we can't dynamically define 2D matrix inside a function,
 * so we'll use pointer math to access each cell.
 * Here we define a simple macro to make it mode readable
 * */
#define MAT(r,c)        (*(mat + ((r)*N) + (c)))

void rotateMatrix(int * mat, int N) {
   int *temp, *p;

   p = (int*)malloc(sizeof(int)*N*N);
   temp = p;

   for (int c = (N - 1); c > -1; --c)
   {
      for (int r = 0; r < N; ++r)
      {
         *temp++ = MAT(r, c);
      }

   }

   temp = p;
   for (int i = 0; i < (N*N); ++i)
      *mat++ = *temp++;

   free(p);
}

void rotMtrx(int * mat, int N)
{
   int c, r;

   /* transpose rows & columns */
   for (r = 0; r < N; ++r)
      for (c = r+1; c < N; ++ c){
         if (r != c) {
            MAT(r,c) = MAT(r,c) ^ MAT(c, r);
            MAT(c,r) = MAT(r,c) ^ MAT(c, r);
            MAT(r,c) = MAT(r,c) ^ MAT(c, r);
         }
      }

   /* swap vertically, column by column */
   for (c = 0; c < N; ++c) {
      for (r = 0; r < N/2; ++r) {
         MAT(r,c) = MAT(r,c) ^ MAT(N-r-1, c);
         MAT(N-r-1,c) = MAT(r,c) ^ MAT(N-r-1, c);
         MAT(r,c) = MAT(r,c) ^ MAT(N-r-1, c);
      }
   }
}

void printMat(int * p, int n)
{
   for (int i = 0; i < n; ++i) {
      for (int i = 0; i < n; ++i) {
         printf ("%3d ", *p++);
      }
      printf ("\n");
   }
   printf ("\n");
}

int main(void) {

   int N;
   int * mat;

   printf("Enter 2D array dimension: ");
   scanf("%d", &N);

   mat = (int*)malloc (sizeof(int)*N*N);

   for (int i = 0; i < (N*N); ++i)
      mat[i] = i;
   printMat(mat, N);

   rotMtrx(mat, N);
   printMat(mat, N);

   free(mat);
   return 0;
}



