#ifndef __SOCK_HEADER__
#define __SOCK_HEADER__

#ifdef __cplusplus
extern "C"
{
#endif

  /******************* External API *******************/
  extern int sock_proc_start (void);

  extern int sock_tx_proc_queue (void * request,
                                 int len,
                                 uint8_t command,
                                 uint8_t flag);

#ifdef __cplusplus
}
#endif

#endif
