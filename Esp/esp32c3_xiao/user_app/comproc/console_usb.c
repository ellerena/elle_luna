/*
 * console_usb.c
 *
 *  Created on: Oct 15, 2022
 *      Author: ellerena
 */
#include <fcntl.h>
#include "esp_log.h"
#include "esp_console.h"
#include "esp_vfs_dev.h"
#include "esp_vfs_fat.h"
#include "esp_vfs_usb_serial_jtag.h"
#include "driver/usb_serial_jtag.h"
#include "linenoise/linenoise.h"
#include "comproc.h"

#define BUF_SIZE (90)
#define PROMPT_STR CONFIG_IDF_TARGET

static void initialize_console(void)
{
    // Disable buffering on stdin
    setvbuf(stdin, NULL, _IONBF, 0);

    // Minicom, screen, idf_monitor send CR when ENTER key is pressed
    esp_vfs_dev_usb_serial_jtag_set_rx_line_endings(ESP_LINE_ENDINGS_CR);
    /* Move the caret to the beginning of the next line on '\n' */
    esp_vfs_dev_usb_serial_jtag_set_tx_line_endings(ESP_LINE_ENDINGS_CRLF);

    // Enable non-blocking mode on stdin and stdout
    fcntl(fileno(stdout), F_SETFL, 0);
    fcntl(fileno(stdin), F_SETFL, 0);

    // Install USB-SERIAL-JTAG driver for interrupt-driven reads and writes
    usb_serial_jtag_driver_config_t usb_serial_jtag_config = USB_SERIAL_JTAG_DRIVER_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(usb_serial_jtag_driver_install(&usb_serial_jtag_config));
    // TODO: manage error if previous call fails

    /* Initialize the console */
    esp_console_config_t console_config = ESP_CONSOLE_CONFIG_DEFAULT();

    console_config.hint_color = -1; // atoi(LOG_COLOR_CYAN);

    console_config.max_cmdline_length = 90; //CONFIG_CONSOLE_MAX_COMMAND_LINE_LENGTH;

    ESP_ERROR_CHECK(esp_console_init(&console_config));
    // TODO: manage error if previous call fails

    // no multiline editing. long commands will scroll within single line
    linenoiseSetMultiLine(1);

    // Tell linenoise where to get command completions and hints
    linenoiseSetCompletionCallback(&esp_console_get_completion);
    linenoiseSetHintsCallback((linenoiseHintsCallback*) &esp_console_get_hint);

    // empty lines return NULL
    linenoiseAllowEmpty(false);

    /* Set command history size */
    linenoiseHistorySetMaxLen(5);

#if CONFIG_STORE_HISTORY
    /* Load command history from filesystem */
    linenoiseHistoryLoad(HISTORY_PATH);
#endif

    /* Tell vfs to use usb-serial-jtag driver */
    esp_vfs_usb_serial_jtag_use_driver();
}

static void console_task(void * pvParameters)
{
    // TODO: print welcome banner here

    // Prompt to be printed before each line
    const char* prompt = LOG_COLOR_I PROMPT_STR "> " LOG_RESET_COLOR;

    // Figure out if the terminal supports escape sequences
    if (linenoiseProbe()) { /* zero indicates success */
        printf("\n no escape sequence support.\nLine editing and history " \
               "disabled.\n");
        linenoiseSetDumbMode(1);
        // don't use color codes in the prompt
        prompt = PROMPT_STR "> ";
    }

    /* Main loop */
    while(true) {
        // Get a line using linenoise. Returns when ENTER is pressed.
        char* line = linenoise(prompt);
        if (line == NULL) { /* Ignore empty lines */
            continue;
        }
        /* Add the command to the history */
        linenoiseHistoryAdd(line);

        // printf("%s\n", line);
        command_proc_queue(line);

        /* linenoise allocates line buffer on the heap, so need to free it */
        linenoiseFree(line);
    }

    vTaskDelete(NULL);
}

int console_proc_start(void)
{
    initialize_console();

    xTaskCreate(console_task, "console_task", 1024, NULL, 2, NULL);

    return 0;
}
