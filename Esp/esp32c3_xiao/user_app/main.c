/*
 * main.c
 *
 *  Created on: May 28, 2020
 *      Author: eddie
 */

#include "freertos/FreeRTOS.h"
#include "esp_err.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "os_gpio.h"
#include "os_i2c.h"
#include "comproc.h"
#include "dataout.h"
#include "oledproc.h"
#include "ble.h"
#include "ble_spp.h"
#include "ble_gapper.h"
#include "http.h"
#include "ntp.h"
#include "sock.h"

#ifdef ARCH_ESP32
#include "wifi_sta.h"
#else // esp8266
#include "connect.h"
#endif

/* global scope objects */
volatile unsigned int gFlags = 0;

void app_main ()
{
  esp_log_level_set("sock_server_task", ESP_LOG_INFO);
  esp_log_level_set("wifi_event_handler", ESP_LOG_INFO);

  ESP_ERROR_CHECK(OS_gpio_init()); // initialize gpio (led)

  ESP_ERROR_CHECK(nvs_flash_init());

  ESP_ERROR_CHECK(dataout_proc_start()); // start data presentation layer process
#ifndef DONOTUSEWIFI
  ESP_ERROR_CHECK(wifi_init()); // start wifi socket
  ntp_init(); // start ntp server
  http_start(); // start http server
#endif

  ESP_ERROR_CHECK(command_proc_start()); // start command processor
#ifndef DONOTUSEWIFI
  ESP_ERROR_CHECK(sock_proc_start()); // start socket process
#endif
  ESP_ERROR_CHECK(OS_i2c_master_init()); // start i2c driver
  ESP_ERROR_CHECK(oled_proc_start()); // start oled display processor
  // console_proc_start(); // start console

  /* start BLE process */
#ifdef ARCH_ESP32
#ifdef USE_BLE_GAPPER
  ble_gapper_proc_start();
#elif defined(USE_BLE)
  ESP_ERROR_CHECK(ble_proc_start());
#endif
  // ESP_ERROR_CHECK(ble_spp_proc_start());
#endif

  // start the lsm6dsx raw data collection
  // char cmd[] = "gr";
  // command_proc_queue(cmd);
}
