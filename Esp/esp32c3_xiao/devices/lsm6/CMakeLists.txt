## lsm6dsxx component build configuration

FILE(GLOB allc ${PROJECT_DIR}/../esp8266_app/user_app/lsm6dsxx/*.c
                )

idf_component_register(SRCS ${allc}
                       INCLUDE_DIRS "${PROJECT_DIR}/../esp8266_app/user_app/lsm6dsxx"
                       REQUIRES os dataout
                      )

set_property(TARGET ${COMPONENT_LIB} APPEND PROPERTY LINK_INTERFACE_MULTIPLICITY 3)
add_compile_definitions(LSM6DSOX)
add_compile_definitions(SEN_ADDR7=0x6b)
