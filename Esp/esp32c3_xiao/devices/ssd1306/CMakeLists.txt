## ssd1306 - driver component build configuration

FILE(GLOB allc ${PROJECT_DIR}/../esp8266_app/user_app/ssd1306/*.c
                )

idf_component_register(SRCS ${allc}
                       INCLUDE_DIRS "${PROJECT_DIR}/../esp8266_app/user_app/ssd1306"
                       REQUIRES os
                      )
