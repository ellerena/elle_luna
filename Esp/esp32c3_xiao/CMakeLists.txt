# 09/25/2021

cmake_minimum_required(VERSION 3.5)

## bEgin of extra configuration ##
#########################################################
### Use these build options to customize the target build
# FACTORYONLY, 1: Factory partition build, OTA otherwise
set (FACTORYONLY 1)
#########################################################

set(EXTRA_COMPONENT_DIRS
	"./../../../elle_vs/Projects/socket/ltv"
	cli
	devices/ssd1306
	devices/lsm6
	drv
	misc
	os
	rtosOnly
	user_app
	user_app/comproc
	user_app/dataout
   )

#target_link_libraries(esp32c3_xiao_app.bin PRIVATE misc)

add_compile_definitions(ARCH_ESP32)
add_compile_definitions(ARCH_ESP32C3)
add_compile_definitions(ARCH_ESP32C3_IMU)
add_compile_definitions(USE_OLED_SSD1306)
add_compile_definitions(USE_BLE)
add_compile_definitions(DONOTUSEWIFI)

if (FACTORYONLY EQUAL 1)
add_compile_definitions(FACTORYONLY)
endif()
## End of custom configuration ##

include($ENV{IDF_PATH}/tools/cmake/project.cmake)
project(esp32c3_xiao)
