/*
 * command_handlers.c
 *
 *  Created on: Jun 17, 2020
 *      Author: Eddie Llerena
 */

#include "stdio.h"
#include "string.h"
#include "freertos/FreeRTOS.h"
#include "esp_err.h"
#include "esp_partition.h"
#include "esp_system.h"
#include "command_handlers.h"
#include "nvs_store.h"
#include "mqtt_cl.h"
#include "ota_upgrade.h"
#include "main.h"

#define TAG                "command handler"
#define POINTER_TO_ARGS    (2)            /* index of the location of the 'arguments' in the buffer array */
#define BUFFER_SIZE        (52)           /* max number of bytes that can be received in a command entry */
#define TASK               buf[0]         /* variable to hold the requested 'task' */
#define MODE               buf[1]         /* variable to hold the 'mode' for the TASK*/

/**
 @brief   converts a string of chars representing a vector of hex numbers into a vector of bytes equivalent.
 Note: if first value is lower than 1fh (e.g. tab) then next bytes remain intact.
 @param   p contains the address of the first char in the string.
 @retval  the count of numbers comverted.
 @verbatim
 Text entered by a user via the keyboard is in the form of a string of printable chars that represent hex numbers.
 This functions takes that string of chars and extracts the meaningful hex numbers represented in it, then
 it places those numbers (bytes) in an array at the same ram location. Returning the number of numbers
 converted.
 @endverbatim
 */
static int ParseCharsToValueHex (char *p)
{
   char *q, d;
   int i, k;
   uint8_t c;

   q = p;
   k = 0;

   while (*p > 0x1f) /* Will process until a non printable character is found */
   {
      d = 0; /* initialize the 'number equivalent value' */
      i = 0; /* initialize the digit counter (assume 2 digits max per number)*/
      do
      {
         d <<= 4; /* each digit uses the first 4 bits, so on each loop we must push the others */
         c = *p++; /* read the new char */
         if ((c > 0x29) && (c < 0x3a)) /* char is between 0 and 9 */
         {
            d += (c - 0x30); /* add the char equivalen value to our total */
            i++; /* increment our digit counter */
         }
         if ((c > 0x60) && (c < 0x67)) /* char is between 0xa and 0xf */
         {
            d += (c - 0x60 + 0x9); /* add the char equivalen value to our total */
            i++; /* increment our digit counter */
         }
      }
      while ((i == 0) || ((*p > 0x2f) && (i < 2))); /* repeat: if no char found or: if found a space and less than 2 digits */

      if (i) /* if the digit counter is non-zero then a value has been found */
      {
         *q++ = d; /* write the number found to memory */
         k++; /* increment 'detected numbers counter' */
      }
   }

   *q = 0; /* write 0 at the end */

   return k;
}

void CommandProcessor (char *buf)
{
   char *acArgs = &buf[POINTER_TO_ARGS]; /* pointer to numeric arguments in buffer */
   int n;

   n = ParseCharsToValueHex (acArgs);

   switch (TASK)
   {

      case 's': /* system operations */
      switch (MODE)
      {
         case 'f': /* sf[\t]<pathname>: firmware file url */
         if (acArgs[0] == '\0')
         {
            n = strlen(&acArgs[1]);
            memcpy(sta_cntxt.ota_fw_url, &acArgs[1], n ); /* copy all but EOL */
            memset((void*)&sta_cntxt.ota_fw_url[n], 0, 1); /* zero end string */
         }
         break;
         case 'i': /* si: firmware id (welcome) */
         print_welcome();
         break;
         case 'r': /* sr: restart system */
         esp_restart();
         break;
         case 'u': /* su: firmware update (OTA) */
         ota_request_upgrade();
         break;
#ifdef FACTORYONLY
         case 'p': /* sp: print current and next partitions */
         ota_who_am_i();
         ota_who_is_nxt();
         break;
         case 'y': /* sy: set factory partition for next boot */
         ota_set_boot_app(ESP_PARTITION_SUBTYPE_APP_FACTORY);
         break;
         case 'e': /* se: set OTA partition for next boot */
         ota_set_boot_app(ESP_PARTITION_SUBTYPE_OTA(0));
         break;
#endif
         default:
         GFS(COMMERR);
         break;
      }
      break;

      case 'n': /* NVS flash operations */
      switch (MODE)
      {
         case 'l': /* nl: load NVS context */
         load_nvs_contxt();
         break;
         case 's': /* ns: save NVS context */
         save_nvs_store();
         break;
         case 'c': /* nc: clean NVS store */
         clean_nvs_store();
         break;
         default:
         GFS(COMMERR);
         break;
      }
      break;

#ifndef FACTORYONLY     /* extended operations */
      case 'm': /* MQTT operations */
      switch (MODE)
      {
         case 'i': /* mi: mqtt initialize */
         mqtt_init();
         break;
         case 'd': /* md: mqtt de-initialize */
         mqtt_deinit();
         break;
         case 's': /* ms<\t><topic> mqtt subscribe to topic */
         if (acArgs[0] == '\0')
         {
            mqtt_subscribe(&acArgs[1]);
         }
         break;
         case 'u': /* ms<\t><topic> mqtt un-subscribe from topic */
         if (acArgs[0] == '\0')
         {
            mqtt_unsubscribe(&acArgs[1]);
         }
         break;
         case 'p': /* mp<\t><data> or mp<data> mqtt publish data txt/bin */
         mqtt_publish(n ? acArgs : &acArgs[1], n);
         break;
         default:
         GFS(COMMERR);
         break;
      }
      break;
#endif

      default: /* unknown request */
      GFS(COMMERR);
      break;
   }

   if (GF(COMMERR))
   {
      GFC(COMMERR);
      memcpy (buf, "Unknown Command\0", 16);
   }
   else
   {
      memcpy (buf, "Ok\0", 3);
   }

}

