/*
 * sock.c
 *
 *  Created on: May 28, 2020
 *      Author: eddie

 BSD Socket API Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "freertos/FreeRTOS.h"
#include "command_handlers.h"
#include "esp_log.h"
#include <lwip/sockets.h>
#include "sock.h"
#include "ntp.h"
#include "command_handlers.h"
#include "main.h"

#define TAG             "sock"
#define HTTP_200_TXT    "HTTP/1.0 200 OK\r\n\r\nParis et Libere!\n\0"
#define HTTP_200_SZ     (sizeof(HTTP_200_TXT))
char buf[SOCK_BUF];

static void srv_connection_handler(int scon)
{
   int n;

   for(;;)
   {
      GFC(M_SOCKET);
      while ((n = recv(scon, buf, SOCK_BUF - 1, 0)) > 0) { /* receive loop */
         if (strstr(buf, "HTTP"))      /* request comes from HTTP */
            GFS(F_HTTP);
         buf[n] = '\0';                /* null end the request string */
         PRN("%d>%s\n", n, buf);    /* present len and text of request */
         if (buf[n-1] <= '\n')         /* end of msg */
            break;
      }

      if (n) {
         if (GF(F_HTTP)){
            memcpy(buf, HTTP_200_TXT, HTTP_200_SZ);
            update_time_str(TXT_TIME);
            send(scon, buf, (int)strlen(buf), 0);
            send(scon, timestr, (int)strlen(timestr), 0);
            GFS(F_ENDCON);             /* flag to close the connection */
         }
         else if (buf[0] == '.') {     /* request to close connection */
            GFS(F_ENDCON);             /* flag to close the connection */
         }
         if (buf[1] == '.') {          /* request to shutdown server */
            GFS(F_ENDPROG + F_ENDCON); /* flag to close & exit program */
            break;
         }
         if (GF(F_ENDCON)) {
            break;
         }

         /* if we arrive here, we received an execution command request */
         CommandProcessor(buf);           /* execute request */
         send(scon, buf, strlen(buf), 0); /* feedback to client */
      }
   }
}

void tcp_server_task(void *pvParameters)
{
   int ssrv, scon;
   struct sockaddr_in saddr;

   CHKR((ssrv = socket(AF_INET, SOCK_STREAM, IPPROTO_IP)) < 0);

   saddr.sin_family = AF_INET;
   saddr.sin_addr.s_addr = htonl(INADDR_ANY);
   saddr.sin_port = htons(SSOCK_PRT);

   CHKR (bind(ssrv, (struct sockaddr *)&saddr, sizeof(saddr)) != 0);
   CHKR (listen(ssrv, 1) != 0);

   for(;;) {

      struct sockaddr_in caddr;
      uint slen = sizeof(caddr);

      PRN("Wait for client @ %d\n", SSOCK_PRT);
      scon = accept(ssrv, (struct sockaddr *)&caddr, &slen);
      if (scon < 0) continue;    /* discard invalid requests */
      inet_ntoa_r(caddr.sin_addr.s_addr, buf, SOCK_BUF - 1);
      PRN("Molon Labe: %s\n", buf);

      GFC(M_SOCKET);
      srv_connection_handler(scon);
      closesocket(scon);

      if (GF(F_ENDPROG)) /* if program exit requested */
         break;
   }

   vTaskDelete(NULL);
}

void socket_init(void)
{
   xTaskCreate(tcp_server_task, "tcp_server", 4096, NULL, 5, NULL);
}
