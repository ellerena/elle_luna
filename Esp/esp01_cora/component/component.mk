#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

### Use these build options to customize the target build
# FACTORYONLY, 1: Factory partition build, OTA otherwise
##********************************
## NOTE: FOR NOW ONLY NON FACTORY
##       OTA IMAGE SHOULD BE USED
##       OTA UPGRADE IS NOT POSSIBLE
##       DUE TO FLASH SIZE LIMITATION
##       SO FLASH VIA SERIAL
##********************************
FACTORYONLY=0

## Factory build
COMPONENT_ADD_INCLUDEDIRS += ../inc ../src
COMPONENT_SRCDIRS += ../src

ifeq '$(FACTORYONLY)' '1'
COMPONENT_EMBED_TXTFILES :=  ../server_certs/ca_cert.pem
COMPONENT_SRCDIRS += ../factory
CFLAGS += -DFACTORYONLY
else
COMPONENT_SRCDIRS += ../utilities
endif

