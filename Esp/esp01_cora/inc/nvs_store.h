/*
 * nvs_store.h
 *
 *  Created on: Jun 22, 2020
 *      Author: Eddie Llerena
 */

#ifndef INC_NVS_STORE_H_
#define INC_NVS_STORE_H_

#include <time.h>

#define STORE_NAME         "storage"
#define NVS_KEY_CRC        "sta_cntxt_crc"
#define NVS_KEY_CNTXT      "sta_cntxt"
#define CNTXT_OTA_URL      "http://192.168.0.45/esp01_cora.bin"
#define CNTXT_VERSION      (10)
#define CNTXT_URL_BUF      (256)
#define CNTXT_CRC_INI      (0)

/*
 * This is our context structure, which will be stored
 * in NVS so that it can be restored on each boot.
 */

typedef struct {
   uint32_t cntxtVer;
   time_t globtime;
   uint32_t cntxFlags;
   char ota_fw_url[CNTXT_URL_BUF];
} NVS_cntxt_t;

extern NVS_cntxt_t sta_cntxt;

/********************************************************
 * API interface
 ********************************************************/

/*
 * @brief:  store the NVS context into flash.
 *          This call updates the context before
 *          storing it.
 * */
void save_nvs_store(void);

/*
 * @brief:  load NVS context from flash into ram
 *          updates our global flags and time.
 * */
void load_nvs_contxt(void);

/*
 * @brief:  Completely removes all keys/values
 *          already stored in flash
 * */
void clean_nvs_store(void);

/*
 * @brief:  print NVS context for information purposes
 * */
void print_nvs_store (void);


#endif /* INC_NVS_STORE_H_ */
