/*
 * main.h
 *
 *  Created on: May 28, 2020
 *      Author: eddie
 */

#ifndef INC_MAIN_H_
#define INC_MAIN_H_

/* Common functions for protocol examples, to establish Wi-Fi or Ethernet connection.

 This example code is in the Public Domain (or CC0 licensed, at your option.)

 Unless required by applicable law or agreed to in writing, this
 software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied.
 */

#include "sdkconfig.h"
#include "driver/uart.h"

#define NDEBUG

#define PRN printf //do { char str[1024]; int len = sprintf (str, __VA_ARGS__);
        //uart_write_bytes(UART_NUM_0, str, len);} while (0)

#define PRNS COM_puts

#define INFOMSG(...)       ESP_LOG_LEVEL(ESP_LOG_INFO, TAG, ##__VA_ARGS__);
#define CHKMSG(c)          if(c) {ESP_LOG_LEVEL(ESP_LOG_ERROR, TAG, "error @ %d", __LINE__); break;}
#define CHKRET(c, m)       if(c) {ESP_LOG_LEVEL(ESP_LOG_ERROR, TAG, "error @ %d", __LINE__); return;} INFOMSG(m)
#define CHKR(c)            if(c) {chkr(__LINE__);}
#define CHKBRK(c, m)       CHKMSG(c) ESP_LOGI(TAG, m)
#define CHKERR(f)          if (f != ESP_OK) {ESP_LOG_LEVEL(ESP_LOG_ERROR, TAG, "error @ %d", __LINE__); return;}
#define LOGERR(f)          if (f != ESP_OK) PRN("Error @ %s: %d", __func__, __LINE__)

#define F_ENDCON           (1 << 16)   /* close socket connection */
#define F_ENDPROG          (1 << 17)   /* end socket task */
#define F_HTTP             (1 << 0)    /* HTTP request answered by socket */
#define F_OTA_PEND         (1 << 15)   /* OTA request pending */
#define F_BT_ADVCONF       (1 << 12)   /* Bluetooth ADV configured */
#define F_BT_SCANRSPCONF   (1 << 13)   /* Bluetooth SCAN RSP configured */
#define F_BT_ENA           (1 << 10)   /* Bluetooth is enabled */
#define COMMERR            (1 << 14)   /* request unknown by command processor */
#define M_BT_CONF          (F_BT_ADVCONF | F_BT_SCANRSPCONF)
#define M_SOCKET           (F_ENDCON | F_ENDPROG | F_HTTP)
#define M_CNTXT            (F_OTA_PEND)   /* mask flags to be saved to NVS context */
#define GF(x)              (gFlags & (x))
#define GFS(x)             do {gFlags |= (x);} while(0)
#define GFC(x)             do {gFlags &= ~(x);} while(0)
#define GFCLEAR            do {gFlags = 0;} while(0)

extern unsigned gFlags;
extern char fwurl[];

extern void chkr (int32_t line);
extern void ledc_init (void);
extern void lcd_init (void);

void print_welcome (void);

#ifdef __cplusplus
}
#endif

#endif /* INC_MAIN_H_ */
