/*
 * ota_upgrade.h
 *
 *  Created on: Jul 5, 2020
 *      Author: Eddie Llerena
 */

#ifndef INC_OTA_UPGRADE_H_
#define INC_OTA_UPGRADE_H_

extern void ota_init_tsk(void);
extern void ota_who_am_i(void);
extern void ota_who_is_nxt(void);
extern void ota_set_boot_app(esp_partition_subtype_t subtype);
extern void ota_set_boot_factory(void);
extern void ota_set_boot_image(void);
extern void ota_request_upgrade(void);

#endif /* INC_OTA_UPGRADE_H_ */
