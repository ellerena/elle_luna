#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "spi_flash.h"
#include "nvs_flash.h"
#include "esp_partition.h"

#include "esp8266/pin_mux_register.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "string.h"

#include "sock.h"
#include "ntp.h"
#include "nvs_store.h"
#include "wifi_sta.h"
#include "ota_upgrade.h"
#include "main.h"

/* global scope objects */
unsigned gFlags = 0;

void COM_puts(char * str)
{
   uart_write_bytes(UART_NUM_0, str, strlen(str));
}


void uart1_init(void)
{
   // Configure parameters of an UART driver,
   // communication pins and install the driver
   uart_config_t uart_config = {
       .baud_rate = 115200,
       .data_bits = UART_DATA_8_BITS,
       .parity    = UART_PARITY_DISABLE,
       .stop_bits = UART_STOP_BITS_1,
       .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
   };

   PIN_FUNC_SELECT(PERIPHS_IO_MUX_U0TXD_U, FUNC_U0TXD);
   PIN_FUNC_SELECT(PERIPHS_IO_MUX_U0RXD_U, FUNC_U0RXD);
   uart_param_config(UART_NUM_0, &uart_config);
   uart_driver_install(UART_NUM_0, 200, 200, 0, NULL, 0);
}

void app_main (void)
{
   esp_err_t err;

   err = nvs_flash_init (); /* Initialize NVS */
//   if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
//      // 1.NVS partition size for OTA app is smaller than the non-OTA.
//      // 2.NVS partition contains data in non-recognized new format.
//      CHKR(nvs_flash_erase());
//      CHKR(nvs_flash_init());
//   }

   uart1_init();
   load_nvs_contxt (); /* load NVS context from flash */

   CHKR(esp_netif_init ()); /* initialize underlying TCP/IP stack */
   CHKR(esp_event_loop_create_default ());

   wifi_init_sta (); /* start wifi service in 'station' mode */
   ntp_init (); /* start ntp service */

#ifdef FACTORYONLY

   /* if there's a pending OTA update, run it now and exit */
    {

      PRNS ("OTA FW upgrade\n");

      ota_init_tsk();      /* run OTA update process */

      return;
   }

#else

   socket_init (); /* start socket service */
   /* print indentification and status data */
   print_welcome ();

#endif

}

void print_welcome (void)
{
   esp_chip_info_t chip_info;

#ifndef FACTORYONLY     /* OTA image */
   PRNS ("******** Upgraded OTA IMAGE ********\n");
#else
   PRNS ("******** FACTORY IMAGE ********\n");
#endif

   esp_chip_info (&chip_info);

   PRN("Truth is out there!!\nESP01 build " __DATE__ " "
        __TIME__ "\nSilicon Rev. %d\n", chip_info.revision);

   PRN("using %d CPU cores, WiFi ", chip_info.cores);
   PRN("%dMB flash\n", spi_flash_get_chip_size () / (1 << 20));
   PRN("Free heap: %d\n", esp_get_free_heap_size ());
   print_nvs_store ();
}

void chkr (int32_t line)
{
   PRN ("err_%d\n", line);
   while (1)
      ;
}

