/*
 * wifi_sta.c
 *
 *  Created on: Jun 23, 2020
 *      Author: Eddie Llerena
 */

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "main.h"

#define WIFI_MAX_RETRY        6
#define F_WIFI_JOIN           BIT0   /* connected to the AP with an IP */
#define F_WIFI_FAIL           BIT1   /* failed to connect after the maximum retries */
#define WIFI_SSID             "LlerenaE2"
#define WIFI_PWD              "5934232179"
#define TAG                   "wifi_sta"

static EventGroupHandle_t s_wifi_event_group; /* FreeRTOS event group to signal when we are connected*/

static int s_retry_num = 0;

static void event_handler (void *arg, esp_event_base_t base, int32_t id,
                           void *event_data)
{
   if (base == WIFI_EVENT
         && (id == WIFI_EVENT_STA_DISCONNECTED || id == WIFI_EVENT_STA_START))
   {

      if (s_retry_num < WIFI_MAX_RETRY)
      { /* not yet joined, try */
         esp_wifi_connect ();
         s_retry_num++;
         PRN ("try AP\n");
      }
      else
      { /* could't join, abandon */
         xEventGroupSetBits (s_wifi_event_group, F_WIFI_FAIL);
         PRN ("abandon AP\n");
      }

   }
   else if (base == IP_EVENT && id == IP_EVENT_STA_GOT_IP)
   {

      ip_event_got_ip_t *event = (ip_event_got_ip_t*) event_data;
      PRN ("Me: %s\n", ip4addr_ntoa (&event->ip_info.ip));
      s_retry_num = 0;
      xEventGroupSetBits (s_wifi_event_group, F_WIFI_JOIN);

   }
}

void wifi_init_sta (void)
{
   EventBits_t bits;
   wifi_config_t wifi_config = { .sta = { .ssid = WIFI_SSID, .password =
         WIFI_PWD }, };

   esp_log_level_set("wifi_sta", ESP_LOG_NONE);

   s_wifi_event_group = xEventGroupCreate ();

   tcpip_adapter_init ();

   wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT()
   ;
   CHKR(esp_wifi_init (&cfg));

   CHKR(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL));
   CHKR(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL));

   CHKR(esp_wifi_set_mode (WIFI_MODE_STA));
   CHKR(esp_wifi_set_config (ESP_IF_WIFI_STA, &wifi_config));
   CHKR(esp_wifi_start ());

   /* Wait for JOIN/FAIL bits set by event_handler() */
   bits = xEventGroupWaitBits (s_wifi_event_group, F_WIFI_JOIN | F_WIFI_FAIL,
                               0 /*pdFALSE*/, 0 /*pdFALSE*/, 0xffffffff /*portMAX_DELAY*/);

   CHKR(!(bits & F_WIFI_JOIN));

   /* unregister events */
   CHKR(esp_event_handler_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler));
   CHKR(esp_event_handler_unregister (IP_EVENT, IP_EVENT_STA_GOT_IP,
                                      &event_handler));
   vEventGroupDelete (s_wifi_event_group);
}

void wifi_stop_sta (void)
{
   if (ESP_OK == esp_wifi_stop ())
      if (ESP_OK == esp_wifi_deinit ())
      {
         //   CHKR(esp_wifi_clear_default_wifi_driver_and_handlers(wifi_netif));
         //   esp_netif_destroy(wifi_netif);
         return;
      }
   chkr (-1);
}
