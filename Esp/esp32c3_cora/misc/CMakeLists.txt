## misc - miscelaneous component build configuration

FILE(GLOB allc ${PROJECT_DIR}/../esp8266_app/misc/*.c
			   )

idf_component_register(SRCS ${allc}
                       INCLUDE_DIRS "${PROJECT_DIR}/../esp8266_app/misc/inc"
                      )
