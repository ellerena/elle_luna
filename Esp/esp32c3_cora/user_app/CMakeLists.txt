## user_app - component build configuration

FILE(GLOB allc sock.c main.c)

idf_component_register(SRCS ${allc}
                       INCLUDE_DIRS "inc"
                       REQUIRES rtosOnly os ltv dataout nvs_flash esp_http_server
                      )
