#ifndef __NTP_HEADER__
#define __NTP_HEADER__

#define TIMESTRSZ    (40)
#define NTP_SERVER   "pool.ntp.org"

enum E_REPORT {
   HTTP_TIME,
   TXT_TIME,
   OLED_TIME
};

/*
 * @brief:  perfom NTP client initialization.
 * */
void ntp_init(void);

/*
 * @brief: convert current system time to a string
 *          which can then be used for any purpose
 * @param:  report_format, one of the E_REPORT enum
 *          types and defined the format of the string
 * */
char* get_time_str (int report_format);

#endif
