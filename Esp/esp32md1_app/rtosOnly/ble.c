/*
 * ble.c
 *
 *  Created on: Sep 11, 2022
 *      Author: ellerena
 */

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_bt.h"

#include "esp_gap_ble_api.h"
#include "esp_gatts_api.h"
#include "esp_bt_main.h"
#include "ble.h"
#include "esp_gatt_common_api.h"

#include "comproc.h"

/************************************ Local Macro Definitions ************************************/
#define TAG "BLEB"

#define PROFILE_NUM 1
#define PROFILE_IDX 0
#define ESP_APP_ID 0x55
#define DEVICE_NAME "VkNa_" __DATE__
#define SVC_INST_ID 0

#define GATTS_CHAR_VAL_LEN_MAX 500 // max data for GATT client write or prepare write
#define PREPARE_BUF_MAX_SIZE 1024
#define CHAR_DECLARATION_SIZE (sizeof(uint8_t))

#define CONFIG_SET_RAW_ADV_DATA

/************************************ Local Type Definitions  ************************************/
typedef enum
{
  OFF = 0,
  ON = 1
} flag_state_t;

typedef enum
{
  NOTIFY_INDICATE_OFF_DESC = 0,
  NOTIFY_ON_DESC,
  INDICATE_ON_DEC,
} operation_desc_t;

typedef enum
{
  IDX_SVC = 0,
  IDX_CHAR_A,
  IDX_CHAR_VAL_A,
  IDX_CHAR_CFG_A,
  IDX_CHAR_NAME_A,

  IDX_CHAR_B,
  IDX_CHAR_VAL_B,

  HRS_IDX_NB
} state_machine_attr_t; /* Attributes State Machine */

typedef union
{
  uint8_t all_flags;
  struct
  {
    uint8_t adv_config: 1;
    uint8_t scan_rsp_config: 1;
    uint8_t notify_is_on: 1;
    uint8_t unused: 6;
  } flags;
} adv_config_flags_t;

typedef struct
{
  uint8_t *prepare_buf;
  int prepare_len;
} prepare_type_env_t;

typedef struct
{
  esp_gatts_cb_t gatts_cb;
  uint16_t gatts_if;
  uint16_t app_id;
  uint16_t conn_id;
  uint16_t service_handle;
  esp_gatt_srvc_id_t service_id;
  uint16_t char_handle;
  esp_bt_uuid_t char_uuid;
  esp_gatt_perm_t perm;
  esp_gatt_char_prop_t property;
  uint16_t descr_handle;
  esp_bt_uuid_t descr_uuid;
} gatts_profile_inst_t;

/*********************************** Local Constant Definitions **********************************/
/* Service */
static const uint16_t GATTS_UUID_SRVC = 0x00FF;
static const uint16_t GATTS_UUID_CHAR_A = 0x50DA;
static const uint16_t GATTS_UUID_CHAR_B = 0xCAFE;

static const uint16_t service_type_uuid = ESP_GATT_UUID_PRI_SERVICE;
static const uint16_t char_type_uuid = ESP_GATT_UUID_CHAR_DECLARE;
static const uint16_t char_type_config_client_uuid = ESP_GATT_UUID_CHAR_CLIENT_CONFIG;
static const uint8_t char_prop_write = ESP_GATT_CHAR_PROP_BIT_WRITE;
static const uint8_t char_prop_read_notify = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_NOTIFY;
static const uint8_t char_a_config_params[] = { 0x00, 0x00 };

/********************************** Global Variable Declarations *********************************/
/********************************** Local Function Declarations **********************************/
static void gap_event_handler (esp_gap_ble_cb_event_t event,
                               esp_ble_gap_cb_param_t * param);

static void gatts_event_handler (esp_gatts_cb_event_t event,
                                 esp_gatt_if_t gatts_if,
                                 esp_ble_gatts_cb_param_t * param);

static void gatts_profile_event_handler (esp_gatts_cb_event_t event,
                                         esp_gatt_if_t gatts_if,
                                         esp_ble_gatts_cb_param_t * param);

static void proc_write_prep_exec_event_env (esp_gatt_if_t gatts_if,
                                      prepare_type_env_t * prepare_write_env,
                                      esp_ble_gatts_cb_param_t * param);

static void proc_write_exec_event_env (prepare_type_env_t * prepare_write_env,
                                   esp_ble_gatts_cb_param_t * param);

/********************************** Local Variable Declarations **********************************/
static uint8_t char_a_value[] = { 0xBA, 0xBE, 0xFA, 0xCE };

static adv_config_flags_t adv_config_done_flags = {0};

static uint16_t attribute_handle_table[HRS_IDX_NB];

static prepare_type_env_t prepare_write_env;

#ifdef CONFIG_SET_RAW_ADV_DATA
static uint8_t raw_adv_data[] =
  {
    /* flags */
    0x02,
      0x01, 0x06, // BLE mode (not classic), BR/EDR not supported
    /* tx power*/
    0x02,
      0x0a, 0xeb,
    /* service uuid */
    0x03,
      0x03, 0xFF, 0x00,
    /* device name */
    0x05,
      0x09, 'V', 'k', 'N', 'A', // name cmpl
  };

static uint8_t raw_scan_rsp_data[] =
  {
  /* flags */
  0x02,
    0x01, 0x06, // BLE mode (not classic), BR/EDR not supported
  /* tx power */
  0x02,
    0x0a, 0xeb,
  /* service uuid */
  0x03,
    0x03, 0xFF, 0x00 };

#else
static uint8_t service_uuid[16] = {
    /* LSB <-------------------------------> MSB */
    //first uuid, 16bit, [12],[13] is the value
    0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80,
    0x00, 0x10, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00,
};

/* The length of adv data must be less than 31 bytes */
static esp_ble_adv_data_t adv_data = {
    .set_scan_rsp        = false,
    .include_name        = true,
    .include_txpower     = true,
    .min_interval        = 0x0006, //Time = min_interval * 1.25 msec
    .max_interval        = 0x0010, //Time = max_interval * 1.25 msec
    .appearance          = 0x00,
    .manufacturer_len    = 0,    //TEST_MANUFACTURER_DATA_LEN,
    .p_manufacturer_data = NULL, //test_manufacturer,
    .service_data_len    = 0,
    .p_service_data      = NULL,
    .service_uuid_len    = sizeof(service_uuid),
    .p_service_uuid      = service_uuid,
    .flag = (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT),
};

// scan response data
static esp_ble_adv_data_t scan_rsp_data = {
    .set_scan_rsp        = true,
    .include_name        = true,
    .include_txpower     = true,
    .min_interval        = 0x0006,
    .max_interval        = 0x0010,
    .appearance          = 0x00,
    .manufacturer_len    = 0, //TEST_MANUFACTURER_DATA_LEN,
    .p_manufacturer_data = NULL, //&test_manufacturer[0],
    .service_data_len    = 0,
    .p_service_data      = NULL,
    .service_uuid_len    = sizeof(service_uuid),
    .p_service_uuid      = service_uuid,
    .flag = (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT),
};
#endif /* CONFIG_SET_RAW_ADV_DATA */

static esp_ble_adv_params_t adv_params =
  {
    .adv_int_min = 0x20, // 20ms
    .adv_int_max = 0x40, // 40ms
    .adv_type = ADV_TYPE_IND,
    .own_addr_type = BLE_ADDR_TYPE_PUBLIC,
    .channel_map = ADV_CHNL_ALL,
    .adv_filter_policy = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY };

/* One gatt-based profile one app_id and one gatts_if, this array will store
 *  the gatts_if returned by ESP_GATTS_REG_EVT */
static gatts_profile_inst_t profile_tab[PROFILE_NUM] = {
    [PROFILE_IDX] =
      {.gatts_cb = gatts_profile_event_handler, .gatts_if = ESP_GATT_IF_NONE },
  };

/* Full Database Description - Used to add attributes into the database */
static const esp_gatts_attr_db_t gatt_db[HRS_IDX_NB] =
{
// Service Declaration
  [IDX_SVC] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &service_type_uuid,
        ESP_GATT_PERM_READ,
        sizeof(uint16_t),
        sizeof GATTS_UUID_SRVC,
        (uint8_t*) &GATTS_UUID_SRVC
      }
    },

  /* Characteristic Declaration */
  [IDX_CHAR_A] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &char_type_uuid,
        ESP_GATT_PERM_READ,
        CHAR_DECLARATION_SIZE,
        CHAR_DECLARATION_SIZE,
        (uint8_t*) &char_prop_read_notify
      }
    },

  /* Characteristic Value */
  [IDX_CHAR_VAL_A] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &GATTS_UUID_CHAR_A,
        ESP_GATT_PERM_READ,
        GATTS_CHAR_VAL_LEN_MAX,
        sizeof char_a_value ,
        (uint8_t*) char_a_value
      }
    },

  /* Client Characteristic Configuration Descriptor */
  [IDX_CHAR_CFG_A] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &char_type_config_client_uuid,
        ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
        sizeof(uint16_t),
        sizeof char_a_config_params,
        (uint8_t*) char_a_config_params
      }
    },

  /* Characteristic Declaration */
  [IDX_CHAR_B] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &char_type_uuid,
        ESP_GATT_PERM_READ,
        CHAR_DECLARATION_SIZE,
        CHAR_DECLARATION_SIZE,
        (uint8_t*) &char_prop_write
      }
    },

  /* Characteristic Value */
  [IDX_CHAR_VAL_B] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &GATTS_UUID_CHAR_B,
        ESP_GATT_PERM_WRITE,
        GATTS_CHAR_VAL_LEN_MAX,
        sizeof char_a_value,
        (uint8_t*) char_a_value
      }
    },
};

/********************************** Public Function Declarations *********************************/
int ble_proc_start (void)
{
  ESP_LOGI(TAG, "Hold on.......");
  vTaskDelay(10000 / portTICK_PERIOD_MS);
  ESP_LOGI(TAG, "Continue.......");

  /* Common BLE initialization */
  ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT));
  esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_bt_controller_init(&bt_cfg));
  ESP_ERROR_CHECK(esp_bt_controller_enable(ESP_BT_MODE_BLE));

  ESP_ERROR_CHECK(esp_bluedroid_init());
  ESP_ERROR_CHECK(esp_bluedroid_enable());

  // register the callbacks
  ESP_ERROR_CHECK(esp_ble_gap_register_callback(gap_event_handler));
  ESP_ERROR_CHECK(esp_ble_gatts_register_callback(gatts_event_handler));

  ESP_ERROR_CHECK(esp_ble_gatts_app_register(ESP_APP_ID)); // trigger ESP_GATTS_REG_EVT
  ESP_ERROR_CHECK(esp_ble_gatt_set_local_mtu(500));

  return ESP_OK;
}

int ble_notify_client(const uint8_t * data, uint16_t len)
{
  if (!adv_config_done_flags.flags.notify_is_on)
  {
    return -1;
  }

  return esp_ble_gatts_send_indicate(profile_tab[PROFILE_IDX].gatts_if,
                              profile_tab[PROFILE_IDX].conn_id,
                              attribute_handle_table[IDX_CHAR_VAL_A],
                              len,
                              (uint8_t*)data,
                              false);
}

/*********************************** Local Function Definitions **********************************/
static void gatts_profile_event_handler (esp_gatts_cb_event_t event,
                                         esp_gatt_if_t gatts_if,
                                         esp_ble_gatts_cb_param_t * param)
{
  switch (event)
  {
    case ESP_GATTS_REG_EVT: // 0
    {
      ESP_ERROR_CHECK_WITHOUT_ABORT(esp_ble_gap_set_device_name(DEVICE_NAME));

#ifdef CONFIG_SET_RAW_ADV_DATA
      ESP_ERROR_CHECK_WITHOUT_ABORT(
        esp_ble_gap_config_adv_data_raw(raw_adv_data, sizeof raw_adv_data));

      ESP_ERROR_CHECK_WITHOUT_ABORT(
        esp_ble_gap_config_scan_rsp_data_raw(raw_scan_rsp_data, sizeof raw_scan_rsp_data));

      adv_config_done_flags.flags.adv_config = ON;
      adv_config_done_flags.flags.scan_rsp_config = ON;
#else
      //config adv data
      ESP_ERROR_CHECK_WITHOUT_ABORT(esp_ble_gap_config_adv_data(&adv_data));

      adv_config_done_flags.flags.adv_config = ON;
      //config scan response data
      ESP_ERROR_CHECK_WITHOUT_ABORT(esp_ble_gap_config_adv_data(&scan_rsp_data));

      adv_config_done_flags.flags.scan_rsp_config = ON;
#endif

      ESP_ERROR_CHECK_WITHOUT_ABORT(
        esp_ble_gatts_create_attr_tab(gatt_db, gatts_if, HRS_IDX_NB, SVC_INST_ID));
    }
      break;
    case ESP_GATTS_READ_EVT: // 1
    {
      ESP_LOGI(TAG, "GATTS_READ_EVT");
    }
      break;
    case ESP_GATTS_WRITE_EVT: // 2
    {
      if (param->write.is_prep)
      {
        /* handle prepare write (partial data transfers) */
        proc_write_prep_exec_event_env(gatts_if, &prepare_write_env, param);
      }
      else
      {
        esp_log_buffer_hex(TAG, param->write.value, param->write.len);

        if (attribute_handle_table[IDX_CHAR_VAL_B] == param->write.handle)
        {
          ESP_LOGI(TAG, "custom data received");
          size_t safe_len = param->write.len > sizeof char_a_value - 1 ? sizeof char_a_value : param->write.len;

          memcpy(char_a_value, param->write.value, safe_len);
          char_a_value[safe_len] = '\0';
          command_proc_queue(char_a_value);
        }
        // ble configuration descriptor received:
        else if (attribute_handle_table[IDX_CHAR_CFG_A] == param->write.handle
                  && param->write.len == 2)
        {
          uint16_t descr_value = *(uint16_t*)param->write.value;
          if (descr_value == NOTIFY_ON_DESC || descr_value == INDICATE_ON_DEC) // Notify/Indication On requested
          {
            ESP_LOGI(TAG, "INDI/NTFY ON");
            uint8_t notify_data[/* must be <= MTU size */] = "NTFY ON";
            if (descr_value == INDICATE_ON_DEC)
            {
              memcpy(notify_data, "INDI ON", sizeof(notify_data));
            }

            esp_ble_gatts_send_indicate(gatts_if,
                                        param->write.conn_id,
                                        attribute_handle_table[IDX_CHAR_VAL_A],
                                        sizeof(notify_data),
                                        notify_data,
                                        descr_value == INDICATE_ON_DEC /* NTFY: false, INDI: true */);

            adv_config_done_flags.flags.notify_is_on = ON;
          }
          else if (descr_value == NOTIFY_INDICATE_OFF_DESC) // Notify/Indication Off requested
          {
            ESP_LOGI(TAG, "INDI/NTFY OFF");
            adv_config_done_flags.flags.notify_is_on = OFF;
          }
          else // unknown value received
          {
            ESP_LOGE(TAG, "unknown descriptor");
          }

        }
        /* send response when param->write.need_rsp is true*/
        if (param->write.need_rsp)
        {
          ESP_LOGI(TAG, "requested response sent");
          esp_ble_gatts_send_response(gatts_if,
                                      param->write.conn_id,
                                      param->write.trans_id,
                                      ESP_GATT_OK,
                                      NULL);
        }
      }
    }
      break;
    case ESP_GATTS_EXEC_WRITE_EVT: // 3
    {
      ESP_LOGI(TAG, "GATTS_EXEC_WRITE_EVT");
      proc_write_exec_event_env(&prepare_write_env, param);
    }
      break;
    case ESP_GATTS_MTU_EVT: // 4
    {
      ESP_LOGI(TAG, "GATTS_MTU_EVT, MTU %d", param->mtu.mtu);
    }
      break;
    case ESP_GATTS_CONF_EVT: // 5
    case ESP_GATTS_START_EVT: // 12
        break;
    case ESP_GATTS_CONNECT_EVT: // 14
    {
      ESP_LOGI(TAG,
               "GATTS_CONNECT_EVT, conn_id = %d",
               param->connect.conn_id);
      esp_log_buffer_hex(TAG, param->connect.remote_bda, 6);

      /* For the iOS system, please refer to Apple official documents about
       * the BLE connection parameters restrictions. */
      esp_ble_conn_update_params_t conn_params = {
        .latency = 0,
        .max_int = 0x20, // max_int = 0x20*1.25ms = 40ms
        .min_int = 0x10, // min_int = 0x10*1.25ms = 20ms
        .timeout = 400, // timeout = 400*10ms = 4000ms
      };
      memcpy(conn_params.bda, param->connect.remote_bda, sizeof(esp_bd_addr_t));

      // send the updated connection parameters to the peer device.
      esp_ble_gap_update_conn_params(&conn_params);
    }
      break;
    case ESP_GATTS_DISCONNECT_EVT: // 15
    {
      ESP_LOGI(TAG,
               "GATTS_DISCONNECT_EVT, reason = 0x%x",
               param->disconnect.reason);
      esp_ble_gap_start_advertising(&adv_params);
      adv_config_done_flags.flags.notify_is_on = OFF;
    }
      break;
    case ESP_GATTS_CREAT_ATTR_TAB_EVT: // 22
    {
      if (param->add_attr_tab.status != ESP_GATT_OK)
      {
        ESP_LOGE(TAG,
                 "create attribute table failed: 0x%x",
                 param->add_attr_tab.status);
      }
      else if (param->add_attr_tab.num_handle != HRS_IDX_NB)
      {
        ESP_LOGE(TAG,
                 "create attribute table size: %d, expected %d",
                 param->add_attr_tab.num_handle,
                 HRS_IDX_NB);
      }
      else
      {
        ESP_LOGI(TAG,
                 "create attribute table ok: %d attributes",
                 param->add_attr_tab.num_handle);
        memcpy(attribute_handle_table,
               param->add_attr_tab.handles,
               sizeof(attribute_handle_table));
        esp_ble_gatts_start_service(attribute_handle_table[IDX_SVC]);
      }
      break;
    }

    default:
    {
      ESP_LOGW(TAG,
               "unused event:%d sta: %d hdl: %d",
               event,
               param->conf.status,
               param->conf.handle);
    }
      break;
  }
}

static void gap_event_handler (esp_gap_ble_cb_event_t event,
                               esp_ble_gap_cb_param_t * param)
{
  switch (event)
  {
    case ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT: // 4
    {
      adv_config_done_flags.flags.adv_config = OFF;
      if (!adv_config_done_flags.flags.scan_rsp_config)
      {
        esp_ble_gap_start_advertising(&adv_params);
        ESP_LOGI(TAG, "ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT");
      }
    }
      break;
    case ESP_GAP_BLE_SCAN_RSP_DATA_RAW_SET_COMPLETE_EVT: // 5
    {
      adv_config_done_flags.flags.scan_rsp_config = OFF;
      if (!adv_config_done_flags.flags.adv_config)
      {
        esp_ble_gap_start_advertising(&adv_params);
        ESP_LOGI(TAG, "ESP_GAP_BLE_SCAN_RSP_DATA_RAW_SET_COMPLETE_EVT");
      }
    }
      break;
    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT: // 6
    {
      ESP_ERROR_CHECK_WITHOUT_ABORT(param->adv_start_cmpl.status);
    }
      break;
    case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT: // 17
    {
      ESP_ERROR_CHECK_WITHOUT_ABORT(param->adv_stop_cmpl.status);
    }
      break;
    case ESP_GAP_BLE_UPDATE_CONN_PARAMS_EVT: // 20
    {
      ESP_LOGI(TAG,
               "update connection params " \
               "status = %d, min_int = %d, max_int = %d,conn_int = %d," \
               "latency = %d, timeout = %d",
               param->update_conn_params.status,
               param->update_conn_params.min_int,
               param->update_conn_params.max_int,
               param->update_conn_params.conn_int,
               param->update_conn_params.latency,
               param->update_conn_params.timeout);
    }
      break;

    default:
    {
      ESP_LOGW(TAG, "unknown event %d", event);
    }
      break;
  }
}

static void gatts_event_handler (esp_gatts_cb_event_t event,
                                 esp_gatt_if_t gatts_if,
                                 esp_ble_gatts_cb_param_t * param)
{
  /* register event: assign gatts_if to the profile */
  if (ESP_GATTS_REG_EVT == event)
  {
    ESP_ERROR_CHECK(param->reg.status);
    profile_tab[PROFILE_IDX].gatts_if = gatts_if;
  }

  /* run the correspondent profile's gatts cb */
  for (int idx = 0; idx < PROFILE_NUM; idx++)
  {
    /* ESP_GATT_IF_NONE: used to force running all profiles
        otherwise, run all cbs for the profiles in this gatts_if */
    if (ESP_GATT_IF_NONE == gatts_if
        || profile_tab[idx].gatts_if == gatts_if)
    {
      if (profile_tab[idx].gatts_cb)
      {
        profile_tab[idx].gatts_cb(event, gatts_if, param);
      }
    }
  }
  while (0);
}

static void proc_write_prep_exec_event_env (esp_gatt_if_t gatts_if,
                                      prepare_type_env_t * prepare_write_env,
                                      esp_ble_gatts_cb_param_t * param)
{
  ESP_LOGI(TAG, "prepare write, hdl = %d, len = %d", param->write.handle, param->write.len);

  esp_gatt_status_t status = ESP_GATT_OK;
  if (prepare_write_env->prepare_buf == NULL) // if there's no buffer for data, create it
  {
    prepare_write_env->prepare_buf = (uint8_t*) malloc(PREPARE_BUF_MAX_SIZE * sizeof(uint8_t));
    if (!prepare_write_env->prepare_buf)
    {
      ESP_LOGE(TAG, "%s, Gatt_server prep no mem", __func__);
      status = ESP_GATT_NO_RESOURCES;
    }

    prepare_write_env->prepare_len = 0;
  }
  else // handle overrun cases
  {
    if (param->write.offset > PREPARE_BUF_MAX_SIZE)
    {
      status = ESP_GATT_INVALID_OFFSET;
    }
    else if ((param->write.offset + param->write.len) > PREPARE_BUF_MAX_SIZE)
    {
      status = ESP_GATT_INVALID_ATTR_LEN;
    }
  }

  if (param->write.need_rsp) // if need_rsp is true, send the response now
  {
    esp_gatt_rsp_t *gatt_rsp = (esp_gatt_rsp_t*) malloc(sizeof(esp_gatt_rsp_t));
    if (!gatt_rsp)
    {
      ESP_LOGE(TAG, "%s, malloc failed", __func__);
    }
    else
    {
      gatt_rsp->attr_value.len = param->write.len;
      gatt_rsp->attr_value.handle = param->write.handle;
      gatt_rsp->attr_value.offset = param->write.offset;
      gatt_rsp->attr_value.auth_req = ESP_GATT_AUTH_REQ_NONE;
      memcpy(gatt_rsp->attr_value.value, param->write.value, param->write.len);
      ESP_ERROR_CHECK_WITHOUT_ABORT(esp_ble_gatts_send_response(gatts_if,
                                                           param->write.conn_id,
                                                           param->write.trans_id,
                                                           status,
                                                           gatt_rsp));

      free(gatt_rsp);
    }

  }
  if (ESP_GATT_OK != status)
  {
    return;
  }
  memcpy(prepare_write_env->prepare_buf + param->write.offset,
         param->write.value,
         param->write.len);
  prepare_write_env->prepare_len += param->write.len;

}

static void proc_write_exec_event_env ( prepare_type_env_t * prepare_write_env,
                                        esp_ble_gatts_cb_param_t * param)
{
  if (ESP_GATT_PREP_WRITE_EXEC == param->exec_write.exec_write_flag
      && prepare_write_env->prepare_buf)
  {
    esp_log_buffer_hex(TAG, prepare_write_env->prepare_buf, prepare_write_env->prepare_len);
  }
  else
  {
    ESP_LOGE(TAG, "ESP_GATT_PREP_WRITE_CANCEL");
  }

  // clean up the allocated ram buffer
  if (prepare_write_env->prepare_buf)
  {
    free(prepare_write_env->prepare_buf);
  }

  prepare_write_env->prepare_buf = NULL;
  prepare_write_env->prepare_len = 0;
}
