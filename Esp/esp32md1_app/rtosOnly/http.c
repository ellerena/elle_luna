/* HTTP Server */

#include <sys/param.h>
#include <esp_http_server.h>
#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_event.h"
#include "driver/ledc.h"
#include "html.h"

#define TAG __FUNCTION__

static esp_err_t home_get_handler (httpd_req_t * req)
{
  /* get value of 'Host' field from the header */
  size_t buf_len = httpd_req_get_hdr_value_len(req, "Host") + 1;
  char *buf;
  if (buf_len > 1)
  {
    buf = malloc(buf_len);
    if (httpd_req_get_hdr_value_str(req, "Host", buf, buf_len) == ESP_OK)
    {
      ESP_LOGI(TAG, "Found header => Host: %s", buf);
    }
    free(buf);
  }

  /* get query string and parse params */
  buf_len = httpd_req_get_url_query_len(req) + 1;
  if (buf_len > 1)
  {
    buf = malloc(buf_len);
    if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK)
    {
      ESP_LOGI(TAG, "URLquery => %s", buf);
      char param[32];

      /* Get query strings */
      if (httpd_query_key_value(buf, "d0", param, sizeof(param)) == ESP_OK)
      {
        ESP_LOGI(TAG, "URLpar d0=%s", param);
      }
      if (httpd_query_key_value(buf, "d1", param, sizeof(param)) == ESP_OK)
      {
        ESP_LOGI(TAG, "URLpar d1=%s", param);
      }
    }
    free(buf);
  }

  /* Set some custom headers */
  httpd_resp_set_hdr(req, "C1", "24601");

  /* Send response with custom headers and body */
  const char *resp_str = (const char*) req->user_ctx;
  httpd_resp_send(req, resp_str, strlen(resp_str));

  return ESP_OK;
}

httpd_uri_t hello =
  {
    .uri = "/",
    .method = HTTP_GET,
    .handler = home_get_handler,
    .user_ctx = PAGE_HTML_INDEX0 /* pass response string */
  };

/* An HTTP POST handler */
static esp_err_t echo_post_handler (httpd_req_t * req)
{
  char buf[100];
  int ret, remaining = req->content_len;

  while (remaining > 0)
  {
    /* Read the data for the request */
    if ((ret = httpd_req_recv(req, buf, MIN(remaining, sizeof(buf)))) <= 0)
    {
      if (ret == HTTPD_SOCK_ERR_TIMEOUT)
      {
        /* Retry receiving if timeout occurred */
        continue;
      }
      return ESP_FAIL;
    }

    /* Send back the same data */
    httpd_resp_send_chunk(req, buf, ret);
    remaining -= ret;

    /* Log data received */
    ESP_LOGI(TAG, "=========== RECEIVED DATA ==========");
    ESP_LOGI(TAG, "%.*s", ret, buf);
    ESP_LOGI(TAG, "====================================");
  }

  // End response
  httpd_resp_send_chunk(req, NULL, 0);
  return ESP_OK;
}

httpd_uri_t echo =
  {
    .uri = "/echo",
    .method = HTTP_POST,
    .handler = echo_post_handler,
    .user_ctx = NULL };

/* An HTTP PUT handler. This demonstrates realtime
 * registration and deregistration of URI handlers
 */
esp_err_t ctrl_put_handler (httpd_req_t * req)
{
  char buf;
  int ret;

  if ((ret = httpd_req_recv(req, &buf, 1)) <= 0)
  {
    if (ret == HTTPD_SOCK_ERR_TIMEOUT)
    {
      httpd_resp_send_408(req);
    }
    return ESP_FAIL;
  }

  if (buf == '0')
  {
    /* Handler can be unregistered using the uri string */
    ESP_LOGI(TAG, "Unregistering /hello and /echo URIs");
    httpd_unregister_uri(req->handle, "/hello");
    httpd_unregister_uri(req->handle, "/echo");
  }
  else
  {
    ESP_LOGI(TAG, "Registering /hello and /echo URIs");
    httpd_register_uri_handler(req->handle, &hello);
    httpd_register_uri_handler(req->handle, &echo);
  }

  /* Respond with empty body */
  httpd_resp_send(req, NULL, 0);
  return ESP_OK;
}

httpd_uri_t ctrl =
  {
    .uri = "/ctrl",
    .method = HTTP_PUT,
    .handler = ctrl_put_handler,
    .user_ctx = NULL };

httpd_handle_t start_webserver (void)
{
  httpd_handle_t server = NULL;
  httpd_config_t config = HTTPD_DEFAULT_CONFIG();

  // Start the httpd server
  ESP_LOGI(TAG, "on port: '%d'", config.server_port);
  if (ESP_OK == httpd_start(&server, &config))
  {
    // Set URI handlers
    ESP_LOGI(TAG, "Registering URI handlers");
    httpd_register_uri_handler(server, &hello);
    httpd_register_uri_handler(server, &echo);
    httpd_register_uri_handler(server, &ctrl);
    return server;
  }

  ESP_LOGI(TAG, "can't start server!");
  return NULL;
}

void stop_webserver (httpd_handle_t server)
{
  // Stop the httpd server
  httpd_stop(server);
}

static httpd_handle_t server = NULL;

static void disconnect_handler (void * arg,
                         esp_event_base_t event_base,
                         int32_t event_id,
                         void * event_data)
{
  httpd_handle_t *server = (httpd_handle_t*) arg;
  if (*server)
  {
    ESP_LOGI(TAG, "Stopping webserver");
    stop_webserver(*server);
    *server = NULL;
  }
}

static void connect_handler (void * arg,
                      esp_event_base_t event_base,
                      int32_t event_id,
                      void * event_data)
{
  httpd_handle_t *server = (httpd_handle_t*) arg;
  if (*server == NULL)
  {
    ESP_LOGI(TAG, "Starting webserver");
    *server = start_webserver();
  }
}

void http_start (void)
{
  ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT,
                                    IP_EVENT_STA_GOT_IP,
                                    &connect_handler,
                                    &server));
  ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT,
                                    WIFI_EVENT_STA_DISCONNECTED,
                                    &disconnect_handler,
                                    &server));

  server = start_webserver();
}
