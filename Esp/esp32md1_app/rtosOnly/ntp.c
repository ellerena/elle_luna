/*
 * ntp.c
 *
 *  Created on: Jun 10, 2020
 *      Author: eddie
 */

#include "esp_sntp.h"
#include "ntp.h"

#define TAG __FUNCTION__

static char timestr[TIMESTRSZ];

char* get_time_str (int report_format)
{
  /* get system time */
  time_t timeval;
  time(&timeval);

  /* convert time variable to date/time structure */
  struct tm *loctim = localtime(&timeval);

  switch (report_format)
  {

    case HTTP_TIME: /* format for HTML/HTTP responses */
      strftime(timestr, TIMESTRSZ - 7, "<p>%c</p>", loctim);
      break;

    case TXT_TIME: /* simple text format */
      strftime(timestr, TIMESTRSZ - 7, "%c", loctim);
      break;

    case OLED_TIME:

    default:
      strftime(timestr, TIMESTRSZ - 7, "%D %T", loctim);
      break;
  }

  return timestr;
}

void ntp_init (void)
{
  /* NTP will work in unicast polling mode */
  sntp_setoperatingmode(SNTP_OPMODE_POLL);

  /* set NTP server address (url) */
  sntp_setservername(0, NTP_SERVER);

  /* start ntp synchronization */
  sntp_init();
}

