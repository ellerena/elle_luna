/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file <gapper.h>
 *
 * @brief <One Liner For The Purpose/Scope Of This Module>
 *
 * @details
 *
 **************************************************************************************************/

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************************************
 * Includes
 **************************************************************************************************/

/***************************************************************************************************
 * Macro Definitions
 **************************************************************************************************/
#define ADV_MFG_ID 0xCAFE
#define ADV_DUMMY_DATA  { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, \
                          0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, \
                          0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, \
                        }

/***************************************************************************************************
 * Type Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Constant Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Variable Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Function Prototypes
 **************************************************************************************************/
/**
 * @brief <One Line Description>
 *
 * @param param1 Param 1 One Line
 * @param param2 Param 2 One Line
 *
 * @return 0 if successful, otherwise a negative error code
 */
void ble_gapper_proc_start(void);

int gapper_update_data(const uint8_t * data, uint8_t len);

#ifdef __cplusplus
}    // end extern "C"
#endif
