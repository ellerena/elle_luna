/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file <gapper.c>
 *
 * @brief <One Liner For The Purpose/Scope Of This Module>
 *
 * @details
 *
 **************************************************************************************************/

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "esp_bt.h"
#include "esp_bt_main.h"
#include "esp_gap_ble_api.h"
#include "esp_log.h"
#include "ble_gapper.h"

/***************************************************************************************************
 * Local Macro Definitions
 **************************************************************************************************/
#define TAG "BLEG"
#define ADV_PACKET_MAX_SIZE 31
#define ADV_DATA_FIELD_SIZE (ADV_PACKET_MAX_SIZE - 11) // 20 bytes o fuser data
#define ADV_DATA_LEN_VAL (ADV_DATA_FIELD_SIZE + 3)

/***************************************************************************************************
 * Local Type Definitions
 **************************************************************************************************/
typedef struct {
    uint8_t flags[3];
    uint8_t name[4];
    uint8_t data_len;
    uint8_t data_type;
    uint16_t data_mfg;
    uint8_t data[ADV_DATA_FIELD_SIZE];
} __attribute__((packed)) esp_ble_gapper_adv_packed_t;

/***************************************************************************************************
 * Local Constant Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Global Variable Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Local Variable Declarations
 **************************************************************************************************/
static esp_ble_adv_params_t adv_params = {
    .adv_int_min        = 0x3C, // 60 x 0.625ms = 37.5ms
    .adv_int_max        = 0x3D, // 61 x 0.625ms = 38.125ms
    .adv_type           = ADV_TYPE_NONCONN_IND,
    .own_addr_type      = BLE_ADDR_TYPE_PUBLIC,
    .channel_map        = ADV_CHNL_ALL,
    .adv_filter_policy = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY,
};

esp_ble_gapper_adv_packed_t adv_packet = {
    .flags     = {0x02, 0x01, 0x04},
    .name      = {0x03, 0x09, 'D', 'C'},
    .data_len  = ADV_DATA_LEN_VAL,
    .data_type = ESP_BLE_AD_MANUFACTURER_SPECIFIC_TYPE,
    .data_mfg  = ADV_MFG_ID,
    .data      = ADV_DUMMY_DATA,
};

/***************************************************************************************************
 * Local Function Declarations
 **************************************************************************************************/
static void gapper_event_stepper_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param);

/***************************************************************************************************
 * Public Function Definitions
 **************************************************************************************************/
void ble_gapper_proc_start(void)
{
    ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT));
    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    esp_bt_controller_init(&bt_cfg);
    esp_bt_controller_enable(ESP_BT_MODE_BLE);

    esp_bluedroid_init();
    esp_bluedroid_enable();

    // register the callback
    ESP_ERROR_CHECK(esp_ble_gap_register_callback(gapper_event_stepper_cb));

    /* configure advertising parameters */
    esp_ble_gap_config_adv_data_raw((uint8_t*)&adv_packet, sizeof(adv_packet));
}

int gapper_update_data(const uint8_t * data, uint8_t len)
{
    if (sizeof adv_packet.data < len)
    {
        len = sizeof adv_packet.data;
    }

    memcpy(&adv_packet.data, data, len);
    esp_ble_gap_config_adv_data_raw((uint8_t*)&adv_packet, sizeof adv_packet);

    return 0;
}

/***************************************************************************************************
 * Local Function Definitions
 **************************************************************************************************/
static void gapper_event_stepper_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
  switch (event)
  {
    case ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT:
    {
        esp_ble_gap_start_advertising(&adv_params);
        break;
    }

    case ESP_GAP_BLE_PERIODIC_ADV_START_COMPLETE_EVT:
    {
        ESP_LOGI(TAG, "ESP_GAP_BLE_PERIODIC_ADV_START_COMPLETE_EVT");
        break;
    }

    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
    {
        break;
    }

    case ESP_GAP_BLE_ADV_DATA_SET_COMPLETE_EVT:
    case ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT:
    case ESP_GAP_BLE_SCAN_START_COMPLETE_EVT:
    case ESP_GAP_BLE_SCAN_RESULT_EVT:
    case ESP_GAP_BLE_SCAN_STOP_COMPLETE_EVT:
    case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
    {
        ESP_LOGE(TAG, "Unused event");
        break;
    }

    default:
        break;
  }
}
