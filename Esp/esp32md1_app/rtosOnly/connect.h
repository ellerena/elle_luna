#ifndef __CONNECT_HEADER__
#define __CONNECT_HEADER__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief Configure Wi-Fi or Ethernet, connect, wait for IP
 *
 * @return ESP_OK on successful connection
 */
extern esp_err_t live_connect(void);

extern esp_err_t live_disconnect(void);

#ifdef __cplusplus
}
#endif

#endif
