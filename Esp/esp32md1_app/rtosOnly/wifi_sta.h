/*
 * wifi_sta.h
 *
 *  Created on: Jun 23, 2020
 *      Author: Eddie Llerena
 */

#ifndef INC_WIFI_STA_H_
#define INC_WIFI_STA_H_

#ifdef __cplusplus
extern "C"
{
#endif

extern int wifi_init (void);
extern void wifi_stop (void);

#ifdef __cplusplus
}
#endif

#endif /* INC_WIFI_STA_H_ */
