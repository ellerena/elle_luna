/*
 * wifi_sta.c
 *
 *  Created on: Jun 23, 2020
 *      Author: Eddie Llerena
 */

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_bit_defs.h"
#include "esp_wifi.h"

#define WIFI_MAX_RETRY        6
#define F_WIFI_JOIN           BIT0   /* connected to the AP with an IP */
#define F_WIFI_FAIL           BIT1   /* failed to connect after the maximum retries */
#define WIFI_SSID             "LlerenaE2"
#define WIFI_PWD              "5934232179"
#define TAG                   __FUNCTION__

static EventGroupHandle_t s_wifi_event_group; /* FreeRTOS event group to signal when we are connected*/

static int s_retry_num = 0;

static void wifi_event_handler (void * arg,
                           esp_event_base_t base,
                           int32_t id,
                           void * event_data)
{
  if (base == WIFI_EVENT)
  {
    ESP_LOGI(TAG, "::%d", id);

    if ((WIFI_EVENT_STA_DISCONNECTED == id || WIFI_EVENT_STA_START == id))
    {
      if (s_retry_num < WIFI_MAX_RETRY)
      { /* not yet joined, try */
        esp_wifi_connect();
        s_retry_num++;
        ESP_LOGI(TAG, "trying AP");
      }
      else
      { /* could't join, abandon */
        xEventGroupSetBits(s_wifi_event_group, F_WIFI_FAIL);
        ESP_LOGE(TAG, "failed AP\n");
      }
    }


  }
  else if (base == IP_EVENT && id == IP_EVENT_STA_GOT_IP)
  {
    ip_event_got_ip_t *event = (ip_event_got_ip_t*) event_data;
    s_retry_num = 0;
    xEventGroupSetBits(s_wifi_event_group, F_WIFI_JOIN);
    ESP_LOGI(TAG, "I'm @ " IPSTR "\n", IP2STR(&event->ip_info.ip));
  }
}

int wifi_init (void)
{
  /* create a group of events to monitor the connection process */
  s_wifi_event_group = xEventGroupCreate();

  // initialize TCP/IP stack and loop to help handle events
  ESP_ERROR_CHECK(esp_netif_init());
  ESP_ERROR_CHECK(esp_event_loop_create_default());

  // create wifi station
  esp_netif_create_default_wifi_sta();

  // initialize the wifi module with default settings
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT()
  ;
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));

  // register the events we want to monitor
  esp_event_handler_instance_t instance_any_id;
  esp_event_handler_instance_t instance_got_ip;
  ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &wifi_event_handler, NULL, &instance_any_id));
  ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                      IP_EVENT_STA_GOT_IP,
                                                      &wifi_event_handler,
                                                      NULL,
                                                      &instance_got_ip));

  wifi_config_t wifi_config =
    { .sta =
      {
        .ssid = WIFI_SSID,
        .password = WIFI_PWD,
        .threshold.authmode = WIFI_AUTH_WPA2_PSK,

        .pmf_cfg =
          { .capable = true, .required = false }, }, };

  // configure and start Wifi
  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
  ESP_ERROR_CHECK(esp_wifi_start());

  /* Wait for JOIN/FAIL bits set by wifi_event_handler() */
  EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
                                         (F_WIFI_JOIN | F_WIFI_FAIL),
                                         pdFALSE,
                                         pdFALSE,
                                         portMAX_DELAY);

  ESP_ERROR_CHECK(!(bits & F_WIFI_JOIN));

  /* unregister events */
  ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID, instance_any_id));
  ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT,
                                             IP_EVENT_STA_GOT_IP,
                                             instance_got_ip));
  vEventGroupDelete(s_wifi_event_group);

  return 0;
}

void wifi_stop (void)
{
  if (ESP_OK == esp_wifi_stop())
    if (ESP_OK == esp_wifi_deinit())
    {
      //   CHKR(esp_wifi_clear_default_wifi_driver_and_handlers(wifi_netif));
      //   esp_netif_destroy(wifi_netif);
      return;
    }
  ESP_ERROR_CHECK(-1);
}
