/*
 * ble.h
 *
 *  Created on: Sep 11, 2022
 *      Author: ellerena
 */

#ifndef RTOSONLY_BLE_H_
#define RTOSONLY_BLE_H_

int ble_proc_start (void);
int ble_notify_client(const uint8_t * data, uint16_t len);

#endif /* RTOSONLY_BLE_H_ */
