/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file ble_spp.c
 * @date 05.17.2022
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_bt.h"
#include "driver/uart.h"
#include "string.h"
#include "comproc.h"

#include "esp_gap_ble_api.h"
#include "esp_gatts_api.h"
#include "esp_bt_defs.h"
#include "esp_bt_main.h"
#include "ble_spp.h"

/************************************ Local Macro Definitions ************************************/
#define BLESSER_TAG  "BLESER"

#define SPP_PROFILE_NUM             1
#define SPP_PROFILE_APP_IDX         0
#define ESP_SPP_APP_ID              0x56
#define ADV_DEVICE_NAME             "Enavili_DC" // Device Name in GAP
#define SPP_SVC_INST_ID             0
#define CHAR_DECLARATION_SIZE       (sizeof(uint8_t))

/// Characteristic UUID
#define ESP_GATT_UUID_SPP_DATA_RECEIVE      0xABF1
#define ESP_GATT_UUID_SPP_DATA_NOTIFY       0xABF2
#define ESP_GATT_UUID_SPP_COMMAND_RECEIVE   0xABF3
#define ESP_GATT_UUID_SPP_COMMAND_NOTIFY    0xABF4

#ifdef SUPPORT_HEARTBEAT
#define ESP_GATT_UUID_SPP_HEARTBEAT         0xABF5
#endif

#define IF_ERROR_RETURN(msg) if(ret) {ESP_LOGE(BLESSER_TAG, \
            "%s " msg " %s", __func__, esp_err_to_name(ret)); \
            return ret;}

/************************************ Local Type Definitions  ************************************/
struct gatts_profile_inst
{
  esp_gatts_cb_t gatts_cb;
  uint16_t gatts_if;
  uint16_t app_id;
  uint16_t conn_id;
  uint16_t service_handle;
  esp_gatt_srvc_id_t service_id;
  uint16_t char_handle;
  esp_bt_uuid_t char_uuid;
  esp_gatt_perm_t perm;
  esp_gatt_char_prop_t property;
  uint16_t descr_handle;
  esp_bt_uuid_t descr_uuid;
};

typedef struct spp_receive_data_node
{
  int32_t len;
  uint8_t *node_buff;
  struct spp_receive_data_node *next_node;
} spp_data_node_t;

typedef struct spp_receive_data_buff
{
  int32_t node_num;
  int32_t buff_size;
  spp_data_node_t *first_node;
} spp_receive_data_buff_t;

/*********************************** Local Constant Definitions **********************************/
/// SPP Service
static const uint16_t spp_service_uuid = 0xABF0;

static const uint8_t spp_adv_data[23] =
{
  /* Flags */
  0x02,
    0x01, 0x06,
  /* Complete List of 16-bit Service Class UUIDs */
  0x03,
    0x03, 0xF0, 0xAB,
  /* Local Name */
  0x0B,
    0x09, 'E', 'N', 'a', 'V', 'i', 'L', 'i', '-', 'D', 'C'
};

static const uint16_t primary_service_uuid = ESP_GATT_UUID_PRI_SERVICE;
static const uint16_t character_declaration_uuid = ESP_GATT_UUID_CHAR_DECLARE;
static const uint16_t character_client_config_uuid = ESP_GATT_UUID_CHAR_CLIENT_CONFIG;

static const uint8_t char_prop_read_notify = ESP_GATT_CHAR_PROP_BIT_READ
                                           | ESP_GATT_CHAR_PROP_BIT_NOTIFY;
static const uint8_t char_prop_read_write = ESP_GATT_CHAR_PROP_BIT_READ
                                          | ESP_GATT_CHAR_PROP_BIT_WRITE_NR;

#ifdef SUPPORT_HEARTBEAT
static const uint8_t char_prop_read_write_notify = ESP_GATT_CHAR_PROP_BIT_READ|ESP_GATT_CHAR_PROP_BIT_WRITE_NR|ESP_GATT_CHAR_PROP_BIT_NOTIFY;
#endif

///SPP Service - data receive characteristic, read&write without response
static const uint16_t spp_data_receive_uuid   = ESP_GATT_UUID_SPP_DATA_RECEIVE;
static const uint8_t spp_data_receive_val[20] = {0};

///SPP Service - data notify characteristic, notify&read
static const uint16_t spp_data_notify_uuid   = ESP_GATT_UUID_SPP_DATA_NOTIFY;
static const uint8_t spp_data_notify_val[20] = {0};
static const uint8_t spp_data_notify_ccc[2]  = {0};

///SPP Service - command characteristic, read&write without response
static const uint16_t spp_command_uuid   = ESP_GATT_UUID_SPP_COMMAND_RECEIVE;
static const uint8_t spp_command_val[10] = {0};

///SPP Service - status characteristic, notify&read
static const uint16_t spp_status_uuid   = ESP_GATT_UUID_SPP_COMMAND_NOTIFY;
static const uint8_t spp_status_val[10] = {0};
static const uint8_t spp_status_ccc[2]  = {0};

#ifdef SUPPORT_HEARTBEAT
///SPP Server - Heart beat characteristic, notify&write&read
static const uint16_t spp_heart_beat_uuid = ESP_GATT_UUID_SPP_HEARTBEAT;
static const uint8_t  spp_heart_beat_val[2] = {0x00, 0x00};
static const uint8_t  spp_heart_beat_ccc[2] = {0x00, 0x00};
#endif

/********************************** Global Variable Declarations *********************************/
/********************************** Local Function Declarations **********************************/
static uint8_t find_char_and_desr_index (uint16_t handle);

static bool store_wr_buffer (esp_ble_gatts_cb_param_t * p_data);

static void free_write_buffer (void);

static void print_write_buffer (void);

static void spp_task_init (void);

static void gap_event_handler (esp_gap_ble_cb_event_t event,
                               esp_ble_gap_cb_param_t * param);

static void gatts_event_handler (esp_gatts_cb_event_t event,
                                 esp_gatt_if_t gatts_if,
                                 esp_ble_gatts_cb_param_t * param);

static void gatts_profile_event_handler (esp_gatts_cb_event_t event,
                                         esp_gatt_if_t gatts_if,
                                         esp_ble_gatts_cb_param_t * param);

/********************************** Local Variable Declarations **********************************/
static uint16_t spp_mtu_size = 23;
static uint16_t spp_conn_id = 0xffff;
static esp_gatt_if_t spp_gatts_if = 0xff;
QueueHandle_t spp_uart_queue = NULL;
static QueueHandle_t cmd_queue_h = NULL;
static bool enable_data_ntf = false;
static bool is_connected = false;
static esp_bd_addr_t spp_remote_bda = { 0x0, };
static uint16_t spp_handle_table[SPP_IDX_NB];

#ifdef SUPPORT_HEARTBEAT
static QueueHandle_t cmd_heartbeat_queue = NULL;
static uint8_t  heartbeat_s[6] = {'B','L','E','s','p','p'};
static bool enable_heart_ntf = false;
static uint8_t heartbeat_count_num = 0;
#endif

static spp_data_node_t *data_node_tail = NULL;
static spp_receive_data_buff_t SppRecvDataBuff = {0};

static esp_ble_adv_params_t spp_adv_params =
{
  .adv_int_min = 0x20,
  .adv_int_max = 0x40,
  .adv_type = ADV_TYPE_IND,
  .own_addr_type = BLE_ADDR_TYPE_PUBLIC,
  .channel_map = ADV_CHNL_ALL,
  .adv_filter_policy = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY,
};

/* One gatt-based profile one app_id and one gatts_if, this array will
    store the gatts_if returned by ESP_GATTS_REG_EVT */
static struct gatts_profile_inst spp_profile_tab[SPP_PROFILE_NUM] =
{ [SPP_PROFILE_APP_IDX] =
  {
    .gatts_cb = gatts_profile_event_handler,
    .gatts_if = ESP_GATT_IF_NONE, /* Not get the gatt_if, initial is ESP_GATT_IF_NONE */
  },
};

///Full HRS Database Description - Used to add attributes into the database
static const esp_gatts_attr_db_t spp_gatt_db[SPP_IDX_NB] =
{
//SPP -  Service Declaration
  [SPP_IDX_SVC] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &primary_service_uuid,
        ESP_GATT_PERM_READ,
        sizeof(spp_service_uuid),
        sizeof(spp_service_uuid),
        (uint8_t*) &spp_service_uuid } },

  //SPP -  data receive characteristic Declaration
  [SPP_IDX_SPP_DATA_RECV_CHAR] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &character_declaration_uuid,
        ESP_GATT_PERM_READ,
        CHAR_DECLARATION_SIZE,
        CHAR_DECLARATION_SIZE,
        (uint8_t*) &char_prop_read_write } },

  //SPP -  data receive characteristic Value
  [SPP_IDX_SPP_DATA_RECV_VAL] =
    {
      { ESP_GATT_RSP_BY_APP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &spp_data_receive_uuid,
        ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
        SPP_DATA_MAX_LEN,
        sizeof(spp_data_receive_val),
        (uint8_t*) spp_data_receive_val } },

  //SPP -  data notify characteristic Declaration
  [SPP_IDX_SPP_DATA_NOTIFY_CHAR] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &character_declaration_uuid,
        ESP_GATT_PERM_READ,
        CHAR_DECLARATION_SIZE,
        CHAR_DECLARATION_SIZE,
        (uint8_t*) &char_prop_read_notify } },

  //SPP -  data notify characteristic Value
  [SPP_IDX_SPP_DATA_NTY_VAL] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &spp_data_notify_uuid,
        ESP_GATT_PERM_READ,
        SPP_DATA_MAX_LEN,
        sizeof(spp_data_notify_val),
        (uint8_t*) spp_data_notify_val } },

  //SPP -  data notify characteristic - Client Characteristic Configuration Descriptor
  [SPP_IDX_SPP_DATA_NTF_CFG] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &character_client_config_uuid,
        ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
        sizeof(uint16_t),
        sizeof(spp_data_notify_ccc),
        (uint8_t*) spp_data_notify_ccc } },

  //SPP -  command characteristic Declaration
  [SPP_IDX_SPP_COMMAND_CHAR] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &character_declaration_uuid,
        ESP_GATT_PERM_READ,
        CHAR_DECLARATION_SIZE,
        CHAR_DECLARATION_SIZE,
        (uint8_t*) &char_prop_read_write } },

  //SPP -  command characteristic Value
  [SPP_IDX_SPP_COMMAND_VAL] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16, (uint8_t*) &spp_command_uuid,
        ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
        SPP_CMD_MAX_LEN, sizeof(spp_command_val), (uint8_t*) spp_command_val } },

  //SPP -  status characteristic Declaration
  [SPP_IDX_SPP_STATUS_CHAR] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &character_declaration_uuid,
        ESP_GATT_PERM_READ,
        CHAR_DECLARATION_SIZE,
        CHAR_DECLARATION_SIZE,
        (uint8_t*) &char_prop_read_notify } },

  //SPP -  status characteristic Value
  [SPP_IDX_SPP_STATUS_VAL] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &spp_status_uuid,
        ESP_GATT_PERM_READ,
        SPP_STATUS_MAX_LEN,
        sizeof(spp_status_val),
        (uint8_t*) spp_status_val } },

  //SPP -  status characteristic - Client Characteristic Configuration Descriptor
  [SPP_IDX_SPP_STATUS_CFG] =
    {
      { ESP_GATT_AUTO_RSP },
      {
        ESP_UUID_LEN_16,
        (uint8_t*) &character_client_config_uuid,
        ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
        sizeof(uint16_t), sizeof(spp_status_ccc), (uint8_t*) spp_status_ccc } },

#ifdef SUPPORT_HEARTBEAT
  //SPP -  Heart beat characteristic Declaration
  [SPP_IDX_SPP_HEARTBEAT_CHAR]  =
  {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&character_declaration_uuid, ESP_GATT_PERM_READ,
  CHAR_DECLARATION_SIZE,CHAR_DECLARATION_SIZE, (uint8_t *)&char_prop_read_write_notify}},

  //SPP -  Heart beat characteristic Value
  [SPP_IDX_SPP_HEARTBEAT_VAL]   =
  {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&spp_heart_beat_uuid, ESP_GATT_PERM_READ|ESP_GATT_PERM_WRITE,
  sizeof(spp_heart_beat_val), sizeof(spp_heart_beat_val), (uint8_t *)spp_heart_beat_val}},

  //SPP -  Heart beat characteristic - Client Characteristic Configuration Descriptor
  [SPP_IDX_SPP_HEARTBEAT_CFG]         =
  {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&character_client_config_uuid, ESP_GATT_PERM_READ|ESP_GATT_PERM_WRITE,
  sizeof(uint16_t),sizeof(spp_data_notify_ccc), (uint8_t *)spp_heart_beat_ccc}},
#endif
  };

/********************************** Public Function Declarations *********************************/
int ble_spp_proc_start (void)
{
  esp_err_t ret;
  esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT()
  ;

  ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT));

  ret = esp_bt_controller_init(&bt_cfg);
  IF_ERROR_RETURN("init controller failed:");

  ret = esp_bt_controller_enable(ESP_BT_MODE_BLE);
  IF_ERROR_RETURN("enable controller failed:");

  ESP_LOGI(BLESSER_TAG, "%s init bluetooth", __func__);

  ret = esp_bluedroid_init();
  IF_ERROR_RETURN("init bluetooth failed:");

  ret = esp_bluedroid_enable();
  IF_ERROR_RETURN("enable bluetooth failed:");

  esp_ble_gatts_register_callback(gatts_event_handler);
  esp_ble_gap_register_callback(gap_event_handler);
  esp_ble_gatts_app_register(ESP_SPP_APP_ID);

  spp_task_init();

  return ret;
}

#ifdef SUPPORT_HEARTBEAT
void spp_heartbeat_task(void * arg)
{
    uint16_t cmd_id;

    for(;;) {
        vTaskDelay(50 / portTICK_PERIOD_MS);
        if(xQueueReceive(cmd_heartbeat_queue, &cmd_id, portMAX_DELAY)) {
            while(1){
                heartbeat_count_num++;
                vTaskDelay(5000/ portTICK_PERIOD_MS);
                if((heartbeat_count_num >3)&&(is_connected)){
                    esp_ble_gap_disconnect(spp_remote_bda);
                }
                if(is_connected && enable_heart_ntf){
                    esp_ble_gatts_send_indicate(spp_gatts_if,
                                                spp_conn_id,
                                                spp_handle_table[SPP_IDX_SPP_HEARTBEAT_VAL],
                                                sizeof(heartbeat_s),
                                                heartbeat_s,
                                                false);
                }else if(!is_connected){
                    break;
                }
            }
        }
    }
    vTaskDelete(NULL);
}
#endif

void spp_cmd_task (void * arg)
{
  char *cmd_id;

  for (;;)
  {
    vTaskDelay(50 / portTICK_PERIOD_MS);
    if (xQueueReceive(cmd_queue_h, &cmd_id, portMAX_DELAY))
    {
      ESP_LOG_BUFFER_CHAR(BLESSER_TAG, cmd_id, strlen(cmd_id));
      free(cmd_id);
    }
  }

  vTaskDelete(NULL);
}

/*********************************** Local Function Definitions **********************************/
static uint8_t find_char_and_desr_index (uint16_t handle)
{
  uint8_t error = 0xff;

  for (int i = 0; i < SPP_IDX_NB; i++)
  {
    if (handle == spp_handle_table[i])
    {
      return i;
    }
  }

  return error;
}

static bool store_wr_buffer (esp_ble_gatts_cb_param_t * p_data)
{
  spp_data_node_t *data_node = (spp_data_node_t*) malloc(sizeof(spp_data_node_t));

  if (data_node == NULL)
  {
    ESP_LOGI(BLESSER_TAG, "malloc error %s %d", __func__, __LINE__);
    return false;
  }
  if (data_node_tail != NULL)
  {
    data_node_tail->next_node = data_node;
  }
  data_node->len       = p_data->write.len;
  data_node->next_node = NULL;
  data_node->node_buff = (uint8_t*) malloc(p_data->write.len);
  data_node_tail       = data_node;

  memcpy(data_node->node_buff, p_data->write.value, p_data->write.len);
  if (SppRecvDataBuff.node_num == 0)
  {
    SppRecvDataBuff.first_node = data_node;
  }
  SppRecvDataBuff.node_num++;
  SppRecvDataBuff.buff_size += p_data->write.len;

  return true;
}

static void free_write_buffer (void)
{
  spp_data_node_t *data_node = SppRecvDataBuff.first_node;

  while (data_node != NULL)
  {
    data_node_tail = data_node->next_node;
    free(data_node->node_buff);
    free(data_node);
    data_node = data_node_tail;
  }

  memset(&SppRecvDataBuff, 0, sizeof SppRecvDataBuff);
}

static void print_write_buffer (void)
{
  spp_data_node_t *data_node = SppRecvDataBuff.first_node;

  while (data_node != NULL)
  {
    char * c = (char*) data_node->node_buff;

    for(int i = data_node->len; i > 0; --i)
    {
      printf("%02x", *c);
    }
    printf("\n");

    data_node = data_node->next_node;
  }
}

static void spp_task_init (void)
{
#ifdef SUPPORT_HEARTBEAT
  cmd_heartbeat_queue = xQueueCreate(10, sizeof(uint32_t));
  xTaskCreate(spp_heartbeat_task, "spp_heartbeat_task", 2048, NULL, 10, NULL);
#endif

  cmd_queue_h = xQueueCreate(10, sizeof(uint32_t));
  xTaskCreate(spp_cmd_task, "spp_cmd_task", 2048, NULL, 10, NULL);
}

static void gap_event_handler (esp_gap_ble_cb_event_t event,
                               esp_ble_gap_cb_param_t * param)
{
  ESP_LOGI(BLESSER_TAG, "GAP_EVT, event %d", event);

  switch (event)
  {
    case ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT:
    {
      esp_ble_gap_start_advertising(&spp_adv_params);
      break;
    }
    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
    { // advertising start successfully or failed
      esp_err_t err = param->adv_start_cmpl.status;

      if (ESP_BT_STATUS_SUCCESS != err)
      {
        ESP_LOGE(BLESSER_TAG, "Advertising start fail: %s", esp_err_to_name(err));
      }
      break;
    }
    default:
      break;
  }
}

static void gatts_event_handler (esp_gatts_cb_event_t event,
                                 esp_gatt_if_t gatts_if,
                                 esp_ble_gatts_cb_param_t * param)
{
  ESP_LOGI(BLESSER_TAG, "GATTS_EVT %d, gatts_if %d", event, gatts_if);

  /* If event is register event, store the gatts_if for each profile */
  if (event == ESP_GATTS_REG_EVT)
  {
    if (param->reg.status == ESP_GATT_OK)
    {
      spp_profile_tab[SPP_PROFILE_APP_IDX].gatts_if = gatts_if;
    }
    else
    {
      ESP_LOGE(BLESSER_TAG,
               "Reg app failed, app_id %04x, status %d",
               param->reg.app_id,
               param->reg.status);
      return;
    }
  }

  do
  {
    for (int idx = 0; idx < SPP_PROFILE_NUM; ++idx)
    {
      /* ESP_GATT_IF_NONE, need to call every profile cb */
      if (gatts_if == ESP_GATT_IF_NONE || gatts_if == spp_profile_tab[idx].gatts_if)
      {
        if (spp_profile_tab[idx].gatts_cb)
        {
          spp_profile_tab[idx].gatts_cb(event, gatts_if, param);
        }
      }
    }
  }
  while (0);
}

static void gatts_profile_event_handler (esp_gatts_cb_event_t event,
                                         esp_gatt_if_t gatts_if,
                                         esp_ble_gatts_cb_param_t * p_data)
{
  uint8_t res = 0xff;

  ESP_LOGI(BLESSER_TAG, "GATTS_PROFILE_EVT - event %d", event);

  switch (event)
  {
    case ESP_GATTS_REG_EVT:
    {
      esp_ble_gap_set_device_name(ADV_DEVICE_NAME);
      esp_ble_gap_config_adv_data_raw((uint8_t*) spp_adv_data, sizeof spp_adv_data);
      esp_ble_gatts_create_attr_tab(spp_gatt_db, gatts_if, SPP_IDX_NB, SPP_SVC_INST_ID);
      break;
    }
    case ESP_GATTS_READ_EVT:
    {
      res = find_char_and_desr_index(p_data->read.handle);
      if (res == SPP_IDX_SPP_DATA_RECV_VAL)
      {
        esp_gatt_rsp_t *gatt_rsp = (esp_gatt_rsp_t *)malloc(sizeof(esp_gatt_rsp_t));
        if (NULL == gatt_rsp)
        {
          ESP_LOGE(BLESSER_TAG, "malloc fail");
          break;
        }
        gatt_rsp->attr_value.len = 2;
        gatt_rsp->attr_value.handle = p_data->read.handle;
        gatt_rsp->attr_value.offset = 0;
        gatt_rsp->attr_value.auth_req = ESP_GATT_AUTH_REQ_NONE;
        memcpy(gatt_rsp->attr_value.value, "ok\n", 3);
        esp_err_t response_err = esp_ble_gatts_send_response(gatts_if, p_data->write.conn_id, p_data->write.trans_id, ESP_GATT_OK, gatt_rsp);
        if (ESP_OK != response_err)
        {
          ESP_LOGE(BLESSER_TAG, "Send response error");
        }
        free(gatt_rsp);
      }
      break;
    }
    case ESP_GATTS_WRITE_EVT:
    {
      res = find_char_and_desr_index(p_data->write.handle);
      if (p_data->write.is_prep == false)
      {
        ESP_LOGI(BLESSER_TAG, "ESP_GATTS_WRITE_EVT : handle = %d", res);
        if (res == SPP_IDX_SPP_COMMAND_VAL)
        {
          uint8_t *spp_cmd_buff = NULL;
          spp_cmd_buff = (uint8_t*) malloc((spp_mtu_size - 3)
              * sizeof(uint8_t));
          if (spp_cmd_buff == NULL)
          {
            ESP_LOGE(BLESSER_TAG, "%s malloc failed", __func__);
            break;
          }
          memset(spp_cmd_buff, 0x0, (spp_mtu_size - 3));
          memcpy(spp_cmd_buff, p_data->write.value, p_data->write.len);
          xQueueSend(cmd_queue_h, &spp_cmd_buff, 10 / portTICK_PERIOD_MS);
        }
        else if (res == SPP_IDX_SPP_DATA_NTF_CFG)
        {
          if ((p_data->write.len == 2) && (p_data->write.value[0] == 0x01)
              && (p_data->write.value[1] == 0x00))
          {
            enable_data_ntf = true;
          }
          else if ((p_data->write.len == 2) && (p_data->write.value[0] == 0x00)
              && (p_data->write.value[1] == 0x00))
          {
            enable_data_ntf = false;
          }
        }
#ifdef SUPPORT_HEARTBEAT
        else if(res == SPP_IDX_SPP_HEARTBEAT_CFG){
            if((p_data->write.len == 2)&&(p_data->write.value[0] == 0x01)&&(p_data->write.value[1] == 0x00)){
                enable_heart_ntf = true;
            }else if((p_data->write.len == 2)&&(p_data->write.value[0] == 0x00)&&(p_data->write.value[1] == 0x00)){
                enable_heart_ntf = false;
            }
        }else if(res == SPP_IDX_SPP_HEARTBEAT_VAL){
            if((p_data->write.len == sizeof(heartbeat_s))&&(memcmp(heartbeat_s,p_data->write.value,sizeof(heartbeat_s)) == 0)){
                heartbeat_count_num = 0;
            }
        }
#endif
        else if (res == SPP_IDX_SPP_DATA_RECV_VAL)
        {
#ifdef SPP_DEBUG_MODE
          ESP_LOG_BUFFER_CHAR(BLESSER_TAG,(char *)(p_data->write.value),p_data->write.len);
#else
          // TODO: uart consumers process
          char * msg = (char*) p_data->write.value;
          msg[p_data->write.len] = '\0';
          command_proc_queue((void*) msg);
          ESP_LOGI(BLESSER_TAG, "%s [END]", msg);
#endif
        }
        else
        {
          //TODO:
        }
      }
      else if ((p_data->write.is_prep == true)
          && (res == SPP_IDX_SPP_DATA_RECV_VAL))
      {
        ESP_LOGI(BLESSER_TAG,
                 "ESP_GATTS_PREP_WRITE_EVT : handle = %d",
                 res);
        store_wr_buffer(p_data);
      }
      break;
    }
    case ESP_GATTS_EXEC_WRITE_EVT:
    {
      if (p_data->exec_write.exec_write_flag)
      {
        print_write_buffer();
        free_write_buffer();
      }
      break;
    }
    case ESP_GATTS_MTU_EVT:
      spp_mtu_size = p_data->mtu.mtu;
      break;
    case ESP_GATTS_CONF_EVT:
      break;
    case ESP_GATTS_UNREG_EVT:
      break;
    case ESP_GATTS_DELETE_EVT:
      break;
    case ESP_GATTS_START_EVT:
      break;
    case ESP_GATTS_STOP_EVT:
      break;
    case ESP_GATTS_OPEN_EVT:
      break;
    case ESP_GATTS_CANCEL_OPEN_EVT:
      break;
    case ESP_GATTS_CLOSE_EVT:
      break;
    case ESP_GATTS_LISTEN_EVT:
      break;
    case ESP_GATTS_CONGEST_EVT:
      break;
    case ESP_GATTS_CREAT_ATTR_TAB_EVT:
    {
      if (ESP_GATT_OK != p_data->add_attr_tab.status)
      {
        ESP_LOGE(BLESSER_TAG,
                 "Create attribute table failed, error code=0x%x",
                 p_data->add_attr_tab.status);
      }
      else if (SPP_IDX_NB != p_data->add_attr_tab.num_handle)
      {
        ESP_LOGE(BLESSER_TAG,
                 "Create attribute table abnormally, num_handle (%d) doesn't equal to HRS_IDX_NB(%d)",
                 p_data->add_attr_tab.num_handle,
                 SPP_IDX_NB);
      }
      else
      {
        memcpy(spp_handle_table,
               p_data->add_attr_tab.handles,
               sizeof(spp_handle_table));
        esp_ble_gatts_start_service(spp_handle_table[SPP_IDX_SVC]);
      }
      break;
    }
    case ESP_GATTS_CONNECT_EVT:
    {
      spp_conn_id = p_data->connect.conn_id;
      spp_gatts_if = gatts_if;
      is_connected = true;
      memcpy(&spp_remote_bda,
             &p_data->connect.remote_bda,
             sizeof spp_remote_bda);
#ifdef SUPPORT_HEARTBEAT
      uint16_t cmd = 0;
      xQueueSend(cmd_heartbeat_queue,&cmd,10/portTICK_PERIOD_MS);
#endif
      break;
    }
    case ESP_GATTS_DISCONNECT_EVT:
    {
      is_connected = false;
      enable_data_ntf = false;
#ifdef SUPPORT_HEARTBEAT
      enable_heart_ntf = false;
      heartbeat_count_num = 0;
#endif
      esp_ble_gap_start_advertising(&spp_adv_params);
      break;
    }
    default:
      break;
  }
}
