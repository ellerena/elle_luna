/* Common functions for protocol examples, to establish Wi-Fi or Ethernet connection.

 This example code is in the Public Domain (or CC0 licensed, at your option.)

 Unless required by applicable law or agreed to in writing, this
 software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied.
 */
#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_netif.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_err.h"

#define GOT_IPV4_BIT BIT(0)
#define GOT_IPV6_BIT BIT(1)
#define EVENT_BITS_EXPECTED GOT_IPV4_BIT
#define TAG __FUNCTION__

#define SSID "LlerenaE2"
#define PWD "5934232179"

static EventGroupHandle_t s_wifi_event_group = NULL;
static ip4_addr_t s_ip_addr;

static void on_wifi_disconnect (void * arg,
                                esp_event_base_t event_base,
                                int32_t event_id,
                                void * event_data)
{
  system_event_sta_disconnected_t *event = (system_event_sta_disconnected_t*) event_data;

  ESP_LOGI(TAG, "Wi-Fi disconnected, trying to reconnect...");
  if (event->reason == WIFI_REASON_BASIC_RATE_NOT_SUPPORT)
  { /*Switch to 802.11 bgn mode */
    esp_wifi_set_protocol(ESP_IF_WIFI_STA,
    WIFI_PROTOCOL_11B | WIFI_PROTOCOL_11G | WIFI_PROTOCOL_11N);
  }

  ESP_ERROR_CHECK(esp_wifi_connect());
}

static void on_got_ip (void * arg,
                       esp_event_base_t event_base,
                       int32_t event_id,
                       void * event_data)
{
  ip_event_got_ip_t *event = (ip_event_got_ip_t*) event_data;
  memcpy(&s_ip_addr, &event->ip_info.ip, sizeof(s_ip_addr));
  xEventGroupSetBits(s_wifi_event_group, GOT_IPV4_BIT);
}

/* external APIs */

esp_err_t live_connect (void)
{
  /* make sure no other connections exist already */
  if (NULL != s_wifi_event_group)
  {
    return ESP_ERR_INVALID_STATE;
  }

  s_wifi_event_group = xEventGroupCreate();

  // initialize the wifi driver
  ESP_ERROR_CHECK(esp_netif_init());
  ESP_ERROR_CHECK(esp_event_loop_create_default());

  // initialize the wifi module with default settings
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT()
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));

  // register the events we want to monitor
  ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &on_wifi_disconnect, NULL));
  ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &on_got_ip, NULL));
  ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
  wifi_config_t wifi_config =
    { .sta =
      {
        .ssid = SSID,
        .password = PWD,
        .threshold.authmode = WIFI_AUTH_WPA2_PSK,
        .pmf_cfg =
          { .capable = true, .required = false }, } };

  ESP_LOGI(TAG, "Connecting to %s...", wifi_config.sta.ssid);
  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
  ESP_ERROR_CHECK(esp_wifi_start());
  ESP_ERROR_CHECK(esp_wifi_connect());

  xEventGroupWaitBits(s_wifi_event_group,
                      EVENT_BITS_EXPECTED,
                      true /* clear on exit */,
                      true /* wait for all bits */,
                      portMAX_DELAY);
  ESP_LOGI(TAG, "Connected to %s  @ " IPSTR, SSID, IP2STR(&s_ip_addr));

  return ESP_OK;
}

esp_err_t live_disconnect (void)
{
  /* make sure a session already exists */
  if (NULL == s_wifi_event_group)
  {
    return ESP_ERR_INVALID_STATE;
  }

  vEventGroupDelete(s_wifi_event_group);
  s_wifi_event_group = NULL;

  esp_err_t err = esp_wifi_stop();
  ESP_ERROR_CHECK(err);

  ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT,
                                               WIFI_EVENT_STA_DISCONNECTED,
                                               &on_wifi_disconnect));
  ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT,
                                               IP_EVENT_STA_GOT_IP,
                                               &on_got_ip));
  ESP_ERROR_CHECK(esp_wifi_deinit());

  ESP_LOGI(TAG, "Wifi disconnected");

  return ESP_OK;
}

