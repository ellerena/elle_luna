#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

COMPONENT_ADD_INCLUDEDIRS += inc
COMPONENT_ADD_INCLUDEDIRS += ../../esp32md1_app/rtosOnly
COMPONENT_ADD_INCLUDEDIRS += ../../esp8266_app/user_app/comproc
COMPONENT_ADD_INCLUDEDIRS += ../../esp8266_app/user_app/dataout
COMPONENT_ADD_INCLUDEDIRS += ../../../../elle_vs/Projects/socket/ltv

#COMPONENT_OBJS := main.o sock.o
#COMPONENT_SRCDIRS += ./
