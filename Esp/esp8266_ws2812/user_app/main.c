/*
 * main.c
 *
 *  Created on: May 28, 2020
 *      Author: eddie
 */

#include "freertos/FreeRTOS.h"
#include "esp_err.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "dataout.h"
#include "comproc.h"
#include "http.h"
#include "ntp.h"
#include "sock.h"
#include "connect.h"

/* global scope objects */
volatile unsigned int gFlags = 0;

void app_main ()
{
  esp_log_level_set("sock_server_task", ESP_LOG_INFO);
  esp_log_level_set("wifi_event_handler", ESP_LOG_INFO);

  ESP_ERROR_CHECK(nvs_flash_init());

  ESP_ERROR_CHECK(dataout_proc_start()); // start data presentation layer process
  ESP_ERROR_CHECK(live_connect()); // start wifi socket

  ntp_init(); // start ntp server
  http_start(); // start http server

  ESP_ERROR_CHECK(command_proc_start()); // start command processor
  ESP_ERROR_CHECK(sock_proc_start()); // start socket process

  //  ble_gatt_init(); /* start BLE service */
}


