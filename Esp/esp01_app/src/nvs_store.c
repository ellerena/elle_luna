/*
 * nvs_store.c
 *
 *  Created on: Jun 22, 2020
 *      Author: Eddie Llerena
 */

#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_crc.h"
#include "nvs_flash.h"
#include "nvs_store.h"
#include "main.h"

#define TAG                "nvs_store"
#define TMSTRSZ          40

NVS_cntxt_t sta_cntxt;

void print_nvs_store (void)
{
   struct tm *loctim ;
   char tmstr[TMSTRSZ];

   loctim = localtime( &sta_cntxt.globtime );
   strftime(tmstr, TMSTRSZ-7, "%D %T", loctim);

   printf(">>> NVS Store Info\n");
   printf("Date: %s\nFw url: %s\nContext Ver: %u\n",
         tmstr,
         sta_cntxt.ota_fw_url,
         sta_cntxt.cntxtVer);
   printf("<<<\n");
}

void save_nvs_store(void)
{
    nvs_handle data_hdl;
    uint32_t crc_val;

    /* open the NVS store */
    CHKR(nvs_open(STORE_NAME, NVS_READWRITE, &data_hdl));

    /* this call updates time in context */
    time( &sta_cntxt.globtime );

    /* save only a few specific flags to nvs */
    sta_cntxt.cntxFlags = GF(M_CNTXT);

    /* update context version */
    sta_cntxt.cntxtVer = CNTXT_VERSION;

    /* calculate new crc */
    crc_val = crc32_le(CNTXT_CRC_INI, (const uint8_t*)&sta_cntxt, sizeof(NVS_cntxt_t));

    /* store context and CRC*/
    if (ESP_OK ==  nvs_set_blob(data_hdl, NVS_KEY_CNTXT, &sta_cntxt, sizeof(NVS_cntxt_t))) {
       if (ESP_OK == nvs_set_u32 (data_hdl, NVS_KEY_CRC, crc_val)) {
          nvs_commit(data_hdl);
          printf("context saved\n");
       }
    }

    nvs_close(data_hdl);
}

void load_nvs_contxt(void)
{
   nvs_handle data_hdl;
   uint32_t crc_nvs, crc_chk;
   size_t   cntxt_len;

   /* set global time zone */
   setenv("TZ", "EST5EDT,M3.2.0/2,M11.1.0", 1);
   tzset();

   /* open the NVS store */
   CHKR(nvs_open(STORE_NAME, NVS_READONLY, &data_hdl));

   cntxt_len = sizeof(NVS_cntxt_t);

   /* in this case we must clean first before read */
   memset ((void*)&sta_cntxt, 0, cntxt_len);

   /* read sta_cntxt from store */
   nvs_get_blob(data_hdl, NVS_KEY_CNTXT, &sta_cntxt, &cntxt_len);

   /* read context crc */
   nvs_get_u32(data_hdl, NVS_KEY_CRC, &crc_nvs);

   /* check/validate crc */
   crc_chk = crc32_le(CNTXT_CRC_INI, (const uint8_t*)&sta_cntxt, sizeof(NVS_cntxt_t));
   if (crc_chk == crc_nvs) {         /* crc matches */
      gFlags = sta_cntxt.cntxFlags;  /* update flags from context */
   }
   else {
      printf ("nvs crc %u mismatch\n", crc_nvs);
   }

   /* close nvs store */
   nvs_close(data_hdl);
}

void clean_nvs_store(void)
{
   nvs_handle data_hdl;

   /* open the NVS store */
   CHKR(nvs_open(STORE_NAME, NVS_READWRITE, &data_hdl));

   /* erase all keys & data in the store */
   if (ESP_OK == nvs_erase_all(data_hdl)) {
      nvs_commit(data_hdl);
   }

   /* close the store */
   nvs_close(data_hdl);

}
