#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "spi_flash.h"
#include "nvs_flash.h"
#include "esp_partition.h"

#include "sock.h"
#include "ntp.h"
#include "nvs_store.h"
#include "wifi_sta.h"
#include "ota_upgrade.h"
#include "i2c.h"
#include "main.h"

/* global scope objects */
unsigned gFlags = 0;

void app_main(void)
{
   esp_err_t err;

   err = nvs_flash_init();      /* Initialize NVS */
//   if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
//      // 1.NVS partition size for OTA app is smaller than the non-OTA.
//      // 2.NVS partition contains data in non-recognized new format.
//      CHKR(nvs_flash_erase());
//      CHKR(nvs_flash_init());
//   }

   load_nvs_contxt();           /* load NVS context from flash */

   CHKR(esp_netif_init());      /* initialize underlying TCP/IP stack */
   CHKR(esp_event_loop_create_default());

   wifi_init_sta();             /* start wifi service in 'station' mode */
   ntp_init();                  /* start ntp service */

#ifdef FACTORYONLY

   /* if there's a pending OTA update, run it now and exit */
    {

      printf ("OTA FW upgrade\n");

      ota_init_tsk();      /* run OTA update process */

      return;
   }

#else

   i2c1_init();
   socket_init();       /* start socket service */
   /* print indentification and status data */
   print_welcome();
#endif


}

void print_welcome (void)
{
   esp_chip_info_t chip_info;

#ifndef FACTORYONLY     /* OTA image */
   printf("******** Upgraded OTA IMAGE ********\n");
#else
   printf("******** FACTORY IMAGE ********\n");
#endif

   esp_chip_info(&chip_info);
   printf("Truth is out there!!\nESP01 build " \
         __DATE__ " " __TIME__ "\nSilicon Rev. %d\n",
         chip_info.revision);
   printf("using %d CPU cores, WiFi ", chip_info.cores);
   printf("%dMB flash\n", spi_flash_get_chip_size() / (1 << 20));
   printf("Free heap: %d\n", esp_get_free_heap_size());
   print_nvs_store();
}

void chkr(int32_t line)
{
   printf("error %d\n", line);
   while (1);
}

