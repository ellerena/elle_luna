/*
 * i2c.c
 *
 *  Created on: Jun 12, 2020
 *      Author: Eddie Llerena
 */

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "command_handlers.h"
#include "esp_log.h"
#include "i2c.h"
#include "GFX_Library.h"
#include "SSD1306.h"
#include "main.h" /* just to avoid CONFIG_FREERTOS_HZ syntax error */

#define TAG "i2c"

#define I2C_TX_BUF_DIS    0                   /*!< I2C master doesn't need buffer */
#define I2C_RX_BUF_DIS    0                   /*!< I2C master doesn't need buffer */

int dst_addr = OLED1_I2C_ADR;

esp_err_t i2c1_init(void)
{
   esp_err_t ret;

   int i2c_port = I2C_NUM_0 ;//I2C1_PRT_STR;
   i2c_config_t conf;
   conf.mode = I2C_MODE_MASTER;
   conf.sda_io_num = I2C1_SDA;
   conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
   conf.scl_io_num = I2C1_SCL;
   conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
   conf.clk_stretch_tick = 300;
   ret = i2c_driver_install(i2c_port, conf.mode);
   i2c_param_config(i2c_port, &conf);
   vTaskDelay(pdMS_TO_TICKS(1000));
#ifndef FACTORYONLY
   display_Init();
#endif

   return ret;

}

esp_err_t i2c_master_rd(i2c_port_t i2c_num, uint8_t *data_rd, size_t size)
{
   i2c_cmd_handle_t cmd;
   if (size == 0) {
      return ESP_OK;
   }
   cmd = i2c_cmd_link_create();
   i2c_master_start(cmd);
   i2c_master_write_byte(cmd, dst_addr | I2C_MASTER_READ, ACK_CHK_EN);
   if (size > 1) {
     i2c_master_read(cmd, data_rd, size - 1, ACK_VAL);
   }
   i2c_master_read_byte(cmd, data_rd + size - 1, NACK_VAL);
   i2c_master_stop(cmd);
   esp_err_t ret = i2c_master_cmd_begin(i2c_num, cmd, WAIT_1S);
   i2c_cmd_link_delete(cmd);
   return ret;
}

esp_err_t i2c_master_wr(i2c_port_t i2c_num, uint8_t *data_wr, size_t size)
{
    i2c_cmd_handle_t cmd;

    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, dst_addr | I2C_MASTER_WRITE, ACK_CHK_EN);
    i2c_master_write(cmd, data_wr, size, ACK_CHK_EN);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(i2c_num, cmd, WAIT_200MS);
    i2c_cmd_link_delete(cmd);
    return ret;
}

void i2c_set_slave_addr(uint8_t addr)
{
   dst_addr = addr;
   printf("i2c address set to 0x%x\n", dst_addr);
}

void i2c_scan(void)
{
   int d;
   i2c_cmd_handle_t cmd;
   esp_err_t r;      /* result storage */

   for (int i = I2C_MASTER_READ; i <= 0x7f; i+=2)
   {
      cmd = i2c_cmd_link_create();
      i2c_master_start(cmd);
      i2c_master_write_byte(cmd, i, ACK_CHK_EN);
      i2c_master_read_byte(cmd, (uint8_t*)&d , NACK_VAL);
      i2c_master_stop(cmd);
      r = i2c_master_cmd_begin(I2C1_PRT_STR, cmd, WAIT_1S);
      i2c_cmd_link_delete(cmd);
      printf((r == ESP_OK) ? "\rPing Ok from 0x%02x\n" : "\r%02x", i&(~1));
      fflush(stdout);
   }
}

void i2c1_cmd(char *acComm, int n)
{
   while (n--) {
      ssd1306_command(*acComm++);
   }
}

