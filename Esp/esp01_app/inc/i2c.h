/*
 * i2c.h
 *
 *  Created on: Jun 12, 2020
 *      Author: Eddie Llerena
 */

#ifndef SRC_I2C_H_
#define SRC_I2C_H_

#include "driver/i2c.h"

#define _STR(num) I2C_NUM_##num
#define STR(num) _STR(num)

#define OLED_MELIFE       (0x78)
#define OLED_BLUE         (0x7a)

#define OLED1_I2C_ADR      OLED_BLUE
#define I2C1_SCL           0 //4//14//18//18//22//26
#define I2C1_SDA           2 //5//13//19//23//21//27
#define I2C1_PORT          0
#define I2C1_FREQ          100000
#define I2C1_PRT_STR       STR(I2C1_PORT)      /*!< I2C port number for master dev */
#define WAIT_1S            pdMS_TO_TICKS(1000) /*!< delay time between different test items */
#define WAIT_200MS         pdMS_TO_TICKS(200) /*!< delay time between different test items */
#define I2C_BUFSZ          128
#define ESP_SLAVE_ADDR     OLED1_I2C_ADR       /*!< ESP32 slave address, you can set any 7bit value */
#define ACK_CHK_EN         0x1                 /*!< I2C master will check ack from slave*/
#define ACK_VAL            0x0                 /*!< I2C ack value */
#define NACK_VAL           0x1                 /*!< I2C nack value */
#define OLED_COMMAND       0x0                 /* co: command */
#define OLED_DATA_FIELD    0x40                /* co: data to follow */

extern int dst_addr;

esp_err_t i2c1_init(void);
esp_err_t i2c_master_rd(i2c_port_t i2c_num, uint8_t *data_rd, size_t size);
esp_err_t i2c_master_wr(i2c_port_t i2c_num, uint8_t *data_wr, size_t size);
void i2c_set_slave_addr(uint8_t addr);
void i2c_scan(void);
void i2c1_cmd(char *acComm, int n);

#endif /* SRC_I2C_H_ */
