/*
 * wifi_sta.h
 *
 *  Created on: Jun 23, 2020
 *      Author: Eddie Llerena
 */

#ifndef INC_WIFI_STA_H_
#define INC_WIFI_STA_H_

extern void wifi_init_sta(void);
extern void wifi_stop_sta(void);

#endif /* INC_WIFI_STA_H_ */
