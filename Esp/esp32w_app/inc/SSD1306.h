/*
 * SSD1306.h
 *
 *  Created on: Jun 13, 2020
 *      Author: Eddie Llerena
 */

#ifndef SRC_SSD1306_H_
#define SRC_SSD1306_H_

#include <stdint.h>
#include <stdbool.h>

//------------------------------ Definitions ---------------------------------//
#define SSD1306_LCDWIDTH             128
#define SSD1306_LCDHEIGHT            64

#define SSD1306_CO_COMMAND           0x00                 /* co: command */
#define SSD1306_CO_DATA_FIELD        0x40                /* co: data to follow */

#define SSD1306_SETCONTRAST          0x81
#define SSD1306_DISPLAYALLON_RESUME  0xA4
#define SSD1306_DISPLAYALLON         0xA5
#define SSD1306_NORMALDISP           0xA6
#define SSD1306_INVERTDISP           0xA7
#define SSD1306_DISPLAYOFF           0xAE
#define SSD1306_DISPLAYON            0xAF
#define SSD1306_SETDISPLAYOFFSET     0xD3
#define SSD1306_SETCOMPINS           0xDA
#define SSD1306_SETVCOMDETECT        0xDB
#define SSD1306_SETDISPLAYCLOCKDIV   0xD5
#define SSD1306_SETPRECHARGE         0xD9
#define SSD1306_SETMULTIPLEX         0xA8
#define SSD1306_SETLOWCOLUMN         0x00
#define SSD1306_SETHIGHCOLUMN        0x10
#define SSD1306_SETSTARTLINE         0x40
#define SSD1306_MEMORYMODE           0x20
#define SSD1306_COLUMNADDR           0x21
#define SSD1306_PAGEADDR             0x22
#define SSD1306_COMSCANINC           0xC0    /* flip vertical */
#define SSD1306_COMSCANDEC           0xC8    /* flip vertical */
#define SSD1306_SEGREMAP             0xA0
#define SSD1306_CHARGEPUMP           0x8D
#define SSD1306_EXTERNALVCC          0x01
#define SSD1306_SWITCHCAPVCC         0x02

// Scrolling #defines
#define SSD1306_ACTIVATE_SCROLL                      0x2F
#define SSD1306_DEACTIVATE_SCROLL                    0x2E
#define SSD1306_SET_VERTICAL_SCROLL_AREA             0xA3
#define SSD1306_RIGHT_HORIZONTAL_SCROLL              0x26
#define SSD1306_LEFT_HORIZONTAL_SCROLL               0x27
#define SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL 0x29
#define SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL  0x2A

#define BLACK   0
#define WHITE   1
#define INVERSE 2

extern uint8_t disp_w, disp_h;

//*************************** User Functions ***************************//
void ssd1306_command(uint8_t c);
void SSD1306_Init(void);  // I2C mode
void display(void);
void display_invert(int invert);

void display_startScrollRight(uint8_t start, uint8_t stop);
void display_startScrollLeft(uint8_t start, uint8_t stop);
void display_startScrollDiagRight(uint8_t start, uint8_t stop);
void display_startScrollDiagLeft(uint8_t start, uint8_t stop);
void display_stopScroll(void);

void display_dim(uint8_t level);
void display_clear(void);
void display_puts(const char * str);

////////////////
void display_fillRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color);
void display_fillScreen(void);
void display_setRotation(uint8_t m);
uint8_t display_getRotation(void);
void display_drawPixel(uint8_t x, uint8_t y, uint8_t color);
void display_drawHLine(uint8_t x, uint8_t y, uint8_t w, uint8_t color);
void display_drawVLine(uint8_t x, uint8_t y, uint8_t h, uint8_t color);
void drawFastHLineInternal(uint8_t x, uint8_t y, uint8_t w, uint8_t color);
void drawFastVLineInternal(uint8_t x, uint8_t __y, uint8_t __h, uint8_t color);

//--------------------------------------------------------------------------//

#endif /* SRC_SSD1306_H_ */
