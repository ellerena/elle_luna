/*
 * mqtt_client.h
 *
 *  Created on: Jul 4, 2020
 *      Author: Eddie Llerena
 */

#ifndef INC_MQTT_CL_H_
#define INC_MQTT_CL_H_

#define BRKR_URL     "mqtt://mqtt.eclipse.org"
#define BRKR_TOP_T   "/enavili/0"      /* topic for txt messages */
#define BRKR_TOP_D   "/enavili/1"      /* topic for binary data */
#define TOP_TYPE(x)  ((x)[9])


extern char * pFreeTopic;

void mqtt_init(void);
void mqtt_deinit(void);
void mqtt_publish(void * data, int top_len);
void mqtt_publish_free(void * data, int top_len);
void mqtt_free_topic_name(const char * topic);
esp_err_t mqtt_subscribe(const char * data);
void mqtt_unsubscribe(const char * data);

#endif /* INC_MQTT_CLIENT_H_ */
