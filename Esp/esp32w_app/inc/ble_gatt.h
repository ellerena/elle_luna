/*
 * ble_gatt.h
 *
 *  Created on: Jun 10, 2020
 *      Author: Eddie Llerena
 */

#ifndef SRC_BLE_GATT_H_
#define SRC_BLE_GATT_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Attributes State Machine */
enum
{
    IDX_SVC,

    IDX_CHAR_A,
    IDX_CHAR_VAL_A,
    IDX_CHAR_CFG_A,

    IDX_CHAR_B,
    IDX_CHAR_VAL_B,

    IDX_CHAR_C,
    IDX_CHAR_VAL_C,

    BT_IDX_NB,
};

void ble_gatt_init(void);
void ble_gatt_deinit(void);

#endif /* SRC_BLE_GATT_H_ */
