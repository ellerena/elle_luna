/*
 * uart_ifs.h
 *
 *  Created on: Jun 24, 2020
 *      Author: Eddie Llerena
 */

#ifndef INC_UART_IFS_H_
#define INC_UART_IFS_H_

#include "driver/uart.h"

#define QUE_UART        UART_NUM_0
#define METER_UART      UART_NUM_1
#define OPTOCOM_UART    UART_NUM_2

#define TX_A         (UART_PIN_NO_CHANGE)
#define RX_A         (UART_PIN_NO_CHANGE)
#define RTS_A        (UART_PIN_NO_CHANGE)
#define CTS_A        (UART_PIN_NO_CHANGE)
#define BAUDR_A      (115200)
#define TRESH_A      (122)
#define BUF_RA       (1024)

#ifdef WROVER        /* for Wrover */
#define TX_B         (GPIO_NUM_14)
#define RX_B         (GPIO_NUM_26)
#define TX_C         (GPIO_NUM_12)
#define RX_C         (GPIO_NUM_13)
#else                /* for Wroom */
#define TX_B         (GPIO_NUM_14)
#define RX_B         (GPIO_NUM_12)
#define TX_C         (GPIO_NUM_13)
#define RX_C         (GPIO_NUM_2)
#endif

#define RTS_B        (UART_PIN_NO_CHANGE)
#define CTS_B        (UART_PIN_NO_CHANGE)
#define BAUDR_B      (115200)

#define RTS_C        (UART_PIN_NO_CHANGE)
#define CTS_C        (UART_PIN_NO_CHANGE)
#define BAUDR_C      (115200)

/* Quectel EC-25 UART APIs */
#define QUE_TX(pdata, len)  uart_write_bytes(QUE_UART, pdata, len)
#define QUE_RX(pdata)       uart_read_bytes(QUE_UART, pdata, BUF_RA, pdMS_TO_TICKS(20))

/* Meter Optocom UART APIs */
#define OPTCM_TX(pdata, len)  uart_write_bytes(OPTOCOM_UART, pdata, len)
#define OPTCM_RX(pdata)       uart_read_bytes(OPTOCOM_UART, pdata, 50, pdMS_TO_TICKS(20))

/* Meter UART APIs */
#define METER_TX(pdata, len)  uart_write_bytes(METER_UART, pdata, len)
#define METER_RX(pdata)       uart_read_bytes(METER_UART, pdata, 50, pdMS_TO_TICKS(20))

/* Uart generic APIs */
void uart_ifs_init(void);

#endif /* INC_UART_IFS_H_ */
