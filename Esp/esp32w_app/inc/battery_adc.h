/*
 * battery_adc.h
 *
 *  Created on: Jun 28, 2020
 *      Author: Eddie Llerena
 */

#ifndef INC_BATTERY_ADC_H_
#define INC_BATTERY_ADC_H_

#define DEFAULT_VREF       (1100)        //Use adc2_vref_to_gpio() to obtain a better estimate
#define ADC_SAMPLES        (16)
#define BATT_CHAN          ADC_CHANNEL_0        /* for now ADC samples IO_36 (channel 0) */
#define BATT_ATTENUATION   ADC_ATTEN_DB_0
#define BATT_WIDTH         ADC_WIDTH_BIT_12

/********************************************************
 * API interface
 ********************************************************/

/*
 * @brief:  Initialize ADC module so it can be used to
 *          measure analog inputs. For now only one input
 *          channel is defined, intended for battery.
 * */
void adc_init(void);

/*
 * @brief:  Perform actual ADC sampling and conversion
 * @return: A/D result.
 * */
int adc_read_val(void);


#endif /* INC_BATTERY_ADC_H_ */
