/*
 * spim.c
 *
 *  Created on: Jun 18, 2020
 *      Author: Eddie Llerena
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "main.h" /* just to avoid CONFIG_FREERTOS_HZ syntax error */

/* Lcd module: ILI9341 */
#define LCD_HOST     SPI2_HOST //HSPI_HOST
#define DMA_CHAN     2

#define SPI_LCD_MI   25
#define SPI_LCD_MO   23
#define SPI_LCD_CK   19
#define SPI_LCD_CS   22

#define LCD_DC       21
#define LCD_RST      18
#define LCD_BKL      5

#define SPI_LCD_FREQ 26*1000*1000   // clk out at 26 MHz (default 10*1000*1000)
#define PACK_LINES   16     /* 16 lines/pack. 15 packs/frame. 1 frame = 240 lines */
//#define LCD_OVERCLOCK

/* data/cmd encapsulation use structure */
typedef struct {
   uint8_t cmd;
   uint8_t dat[16];
   uint8_t len; //length of dat; bit 7 = apply delay; 0xFF = end of cmds.
} lcd_init_cmd_t;

spi_device_handle_t lcd_spi;

/* LCD initialization commands,
 * stored in RAM because DMA can't access DROM
 */
DRAM_ATTR static const lcd_init_cmd_t lcd_config[]={
    /* Power control B, power control = 0, DC_ENA = 1 */
    {0xCF, {0x00, 0x83, 0X30}, 3},
    /* Power on sequence, cp1 keeps 1 frame, 1st frame enable
     * vcl = 0, ddvdh = 3, vgh = 1, vgl = 2, DDVDH_ENH = 1
     */
    {0xED, {0x64, 0x03, 0X12, 0X81}, 4},
    /* Driver timing control A, non-overlap = default +1,
     * EQ = default - 1, CR = default, pre-charge = default - 1
     */
    {0xE8, {0x85, 0x01, 0x79}, 3},
    /* Power control A, Vcore=1.6V, DDVDH=5.6V */
    {0xCB, {0x39, 0x2C, 0x00, 0x34, 0x02}, 5},
    /* Pump ratio control, DDVDH=2xVCl */
    {0xF7, {0x20}, 1},
    /* Driver timing control, all=0 unit */
    {0xEA, {0x00, 0x00}, 2},
    /* Power control 1, GVDD=4.75V */
    {0xC0, {0x26}, 1},
    /* Power control 2, DDVDH=VCl*2, VGH=VCl*7, VGL=-VCl*3 */
    {0xC1, {0x11}, 1},
    /* VCOM control 1, VCOMH=4.025V, VCOML=-0.950V */
    {0xC5, {0x35, 0x3E}, 2},
    /* VCOM control 2, VCOMH=VMH-2, VCOML=VML-2 */
    {0xC7, {0xBE}, 1},
    /* Memory access contorl, MX=MY=0, MV=1, ML=0, BGR=1, MH=0 */
    {0x36, {0x28}, 1},
    /* Pixel format, 16bits/pixel for RGB/MCU interface */
    {0x3A, {0x55}, 1},
    /* Frame rate control, f=fosc, 70Hz fps */
    {0xB1, {0x00, 0x1B}, 2},
    /* Enable 3G, disabled */
    {0xF2, {0x08}, 1},
    /* Gamma set, curve 1 */
    {0x26, {0x01}, 1},
    /* Positive gamma correction */
    {0xE0, {0x1F, 0x1A, 0x18, 0x0A, 0x0F, 0x06, 0x45, 0X87, 0x32, 0x0A, 0x07, 0x02, 0x07, 0x05, 0x00}, 15},
    /* Negative gamma correction */
    {0XE1, {0x00, 0x25, 0x27, 0x05, 0x10, 0x09, 0x3A, 0x78, 0x4D, 0x05, 0x18, 0x0D, 0x38, 0x3A, 0x1F}, 15},
    /* Column address set, SC=0, EC=0xEF */
    {0x2A, {0x00, 0x00, 0x00, 0xEF}, 4},
    /* Page address set, SP=0, EP=0x013F */
    {0x2B, {0x00, 0x00, 0x01, 0x3f}, 4},
    /* Memory write */
    {0x2C, {0}, 0},
    /* Entry mode set, Low vol detect disabled, normal display */
    {0xB7, {0x07}, 1},
    /* Display function control */
    {0xB6, {0x0A, 0x82, 0x27, 0x00}, 4},
    /* Sleep out */
    {0x11, {0}, 0x80},
    /* Display on */
    {0x29, {0}, 0x80},
    /* end transmission */
    {0, {0}, 0xff},
};

/* Send a command to the LCD. Blocking process.
 * Use polling since packages are small.
 */
void lcd_cmd(spi_device_handle_t spi, const uint8_t cmd)
{
   esp_err_t ret;
   spi_transaction_t t;

   memset(&t, 0, sizeof(t));                   //Zero out the transaction
   t.length = 8;                               //Command is 8 bits
   t.tx_buffer = &cmd;                         //The data is the cmd itself
   t.user = (void*)0;                          //D/C needs to be set to 0
   ret = spi_device_polling_transmit(spi, &t); //Transmit!
   assert(ret == ESP_OK);                      //Should have had no issues.
}

/* Send data to the LCD. Blocking process
 * Use polling since packages are small.
 * */
void lcd_data(spi_device_handle_t spi, const uint8_t *data, int len)
{
   esp_err_t ret;
   spi_transaction_t t;

   if (len > 0) {                                 //no data no play
      memset(&t, 0, sizeof(t));                   //Zero out the transaction
      t.length = len * 8;                         //Len is in bytes, transaction length is in bits.
      t.tx_buffer = data;                         //Data
      t.user = (void*)1;                          //D/C needs to be set to 1
      ret = spi_device_polling_transmit(spi, &t); //Transmit!
      assert(ret == ESP_OK);                      //Should have had no issues.
   }
}

uint32_t lcd_get_id(void)
{
   spi_transaction_t t;

   memset(&t, 0, sizeof(t));
   t.length = 8 * 3;                /* we'll read 3 bytes */
   t.flags = SPI_TRANS_USE_RXDATA;
   t.user = (void*)1;

   lcd_cmd(lcd_spi, 0x04);          /* get_id cmd */
   esp_err_t ret = spi_device_polling_transmit(lcd_spi, &t);
   assert( ret == ESP_OK );

   return *(uint32_t*)t.rx_data;
}

/* pre-transfer callback function used to control
 * the LCD D/C line.
 * This callback must be execute immediately before
 * any transmission starts.
*/
void lcd_spi_pre_transfer_callback(spi_transaction_t *t)
{
    gpio_set_level(LCD_DC, (int)t->user);
}

/* To send a set of lines: 1 command, 2 data bytes, 1 command, 2 data bytes , 1 command
 * then the line data itself; a total of 6 transactions. Toggling D/C line before each.
 * Below code queues the commands up as interrupt transactions to make them happen faster
 * (compared to calling spi_device_transmit each time), while calculating next lines.
 */
//static void send_lines(spi_device_handle_t spi, int ypos, uint16_t *linedata)
//{
//   esp_err_t ret;
//   int x;
//
//   static spi_transaction_t trans[6];     /* must be available for future calls */
//
//   //In theory, it's better to initialize trans and data only once and hang on to the initialized
//   //variables. We allocate them on the stack, so we need to re-init them each call.
//   for (x = 0; x < 6; x++) {
//      memset(&trans[x], 0, sizeof(spi_transaction_t));
//
//      if (x & 1) {                          /* Odd transfers are data */
//         trans[x].length = 8 * 4;
//         trans[x].user = (void*)1;
//      }
//      else
//      {                                     /* Even transfers are commands */
//         trans[x].length = 8;
//         trans[x].user = (void*)0;
//      }
//
//      trans[x].flags=SPI_TRANS_USE_TXDATA;
//   }
//
//   trans[0].tx_data[0] = 0x2A;                        //Column Address Set
//   trans[1].tx_data[0] = 0;                           //Start Col High
//   trans[1].tx_data[1] = 0;                           //Start Col Low
//   trans[1].tx_data[2] = (320) >> 8;                  //End Col High
//   trans[1].tx_data[3] = (320) & 0xff;                //End Col Low
//   trans[2].tx_data[0] = 0x2B;                        //Page address set
//   trans[3].tx_data[0] = ypos >> 8;                   //Start page high
//   trans[3].tx_data[1] = ypos & 0xff;                 //start page low
//   trans[3].tx_data[2] = (ypos+PACK_LINES) >> 8;  //end page high
//   trans[3].tx_data[3] = (ypos+PACK_LINES) & 0xff;//end page low
//   trans[4].tx_data[0] = 0x2C;                        //memory write
//   trans[5].tx_buffer = linedata;                     //finally send the line data
//   trans[5].length = 320*2*8*PACK_LINES;          //Data length, in bits
//   trans[5].flags = 0;                                //undo SPI_TRANS_USE_TXDATA flag
//
//   //Queue all transactions.
//   for (x = 0; x < 6; x++) {
//      ret = spi_device_queue_trans(spi, &trans[x], portMAX_DELAY);
//      assert(ret == ESP_OK);
//   }
//
//   //When we are here, the SPI driver is busy (in the background) getting the transactions sent. That happens
//   //mostly using DMA, so the CPU doesn't have much to do here. We're not going to wait for the transaction to
//   //finish because we may as well spend the time calculating the next line. When that is done, we can call
//   //send_line_finish, which will wait for the transfers to be done and check their status.
//}
//
//static void send_line_finish(spi_device_handle_t spi)
//{
//    spi_transaction_t *rtrans;
//    esp_err_t ret;
//
//    //Wait for all 6 transactions to be done and get back the results.
//    for (int x = 0; x < 6; x++) {
//        ret = spi_device_get_trans_result(spi, &rtrans, portMAX_DELAY);
//        assert(ret == ESP_OK);
//        // rtrans may contain data to analyze, however we treat LCD
//        // as write only for now.
//    }
//}

//Simple routine to generate some patterns and send them to the LCD. Don't expect anything too
//impressive. Because the SPI driver handles transactions in the background, we can calculate the next line
//while the previous one is being sent.
//static void display_pretty_colors(spi_device_handle_t spi)
//{
//    uint16_t *lines[2];
//    //Allocate memory for the pixel buffers
//    for (int i=0; i<2; i++) {
//        lines[i]=heap_caps_malloc(320*PACK_LINES*sizeof(uint16_t), MALLOC_CAP_DMA);
//        assert(lines[i]!=NULL);
//    }
//    int frame=0;
//    //Indexes of the line currently being sent to the LCD and the line we're calculating.
//    int sending_line=-1;
//    int calc_line=0;
//
//    while(1) {
//        frame++;
//        for (int y=0; y<240; y+=PACK_LINES) {
//            //Calculate a line.
//            pretty_effect_calc_lines(lines[calc_line], y, frame, PACK_LINES);
//            //Finish up the sending process of the previous line, if any
//            if (sending_line!=-1) send_line_finish(spi);
//            //Swap sending_line and calc_line
//            sending_line=calc_line;
//            calc_line=(calc_line==1)?0:1;
//            //Send the line we currently calculated.
//            send_lines(spi, y, lines[sending_line]);
//            //The line set is queued up for sending now; the actual sending happens in the
//            //background. We can go on to calculate the next line set as long as we do not
//            //touch line[sending_line]; the SPI sending process is still reading from that.
//        }
//    }
//}

//Initialize the display
void lcd_init(void)
{
   int cmd;
   esp_err_t ret;

   spi_bus_config_t buscfg = {
      .miso_io_num = SPI_LCD_MI, .mosi_io_num = SPI_LCD_MO,
      .sclk_io_num = SPI_LCD_CK, .quadwp_io_num = -1,
      .quadhd_io_num = -1, .max_transfer_sz = PACK_LINES * 320 * 2 + 8
   };
   spi_device_interface_config_t devcfg={
      .clock_speed_hz=SPI_LCD_FREQ, .mode=0,  //SPI mode 0
      .spics_io_num=SPI_LCD_CS, .queue_size=7,//queue 7 transactions each time
      .pre_cb=lcd_spi_pre_transfer_callback,  //Specify pre-transfer callback to handle D/C line
   };

   ret = spi_bus_initialize(LCD_HOST, &buscfg, DMA_CHAN);   //Initialize the SPI bus
   ESP_ERROR_CHECK(ret);

   ret = spi_bus_add_device(LCD_HOST, &devcfg, &lcd_spi);       //Attach the LCD to the SPI bus
   ESP_ERROR_CHECK(ret);

   // Configure non-SPI LCD controls
   gpio_set_direction(LCD_DC, GPIO_MODE_OUTPUT);
   gpio_set_direction(LCD_RST, GPIO_MODE_OUTPUT);
   gpio_set_direction(LCD_BKL, GPIO_MODE_OUTPUT);

   //Reset the display
   gpio_set_level(LCD_RST, 0);
   vTaskDelay(pdMS_TO_TICKS(10));
   gpio_set_level(LCD_RST, 1);
   vTaskDelay(pdMS_TO_TICKS(100));

   printf("Initializing LCD ILI9341...\n");

   //Send the initialization commands
   cmd = 0;
   while (lcd_config[cmd].len!=0xff) {
      lcd_cmd(lcd_spi, lcd_config[cmd].cmd);
      lcd_data(lcd_spi, lcd_config[cmd].dat, lcd_config[cmd].len&0x1F);
      if (lcd_config[cmd].len&0x80) {
         vTaskDelay(pdMS_TO_TICKS(100));
      }
      cmd++;
   }

   gpio_set_level(LCD_BKL, 0);                              //Enable backlight

//    Initialize the effect displayed
//    ret=pretty_effect_init();
//    ESP_ERROR_CHECK(ret);
//    Go do nice stuff.
//    display_pretty_colors(spi);

}


