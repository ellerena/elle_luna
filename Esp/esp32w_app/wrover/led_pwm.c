/* LEDC (LED Controller) fade example
   This example code is in the Public Domain (or CC0 licensed, at your option.)
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"

#ifdef WROVER        /* Wrover board has 3 leds (RGB) */
#define LEDC_CH_NUM       (3)
#else
#define LEDC_CH_NUM       (1)
#endif

#define LED_RESOLUTION    (LEDC_TIMER_13_BIT)
#define LED_MAX           ((1 << LED_RESOLUTION) - 1)
#define LEDC_DUTY         (4096)
#define LEDC_FADE_TIME    (5000)
#define TST_DELAY         (1)
#define CH                (1)
#define UNUSED            (0)

static ledc_channel_config_t ledc_channel[LEDC_CH_NUM] = {
   {
      .channel    = LEDC_CHANNEL_0,
      .duty       = 0,
      .gpio_num   = 0,     /* RGB: red */
      .speed_mode = LEDC_HIGH_SPEED_MODE,
      .hpoint     = 0,
      .timer_sel  = LEDC_TIMER_0,
   },
#ifdef WROVER
   {
      .channel    = LEDC_CHANNEL_1,
      .duty       = 0,
      .gpio_num   = 2,     /* RGB: green */
      .speed_mode = LEDC_HIGH_SPEED_MODE,
      .hpoint     = 0,
      .timer_sel  = LEDC_TIMER_0,
   },
   {
      .channel    = LEDC_CHANNEL_2,
      .duty       = 0,
      .gpio_num   = 4,     /* RGB: blue */
      .speed_mode = LEDC_HIGH_SPEED_MODE,
      .hpoint     = 0,
      .timer_sel  = LEDC_TIMER_0,
   }
#endif
};

void ledc_bright(int valRGB)    /* RGB leds <<xx:8><rr:8><gg:8><bb:8>> */
{
   for (int i = 0; i < LEDC_CH_NUM; i++, valRGB>>=8) {
      ledc_set_duty(UNUSED, ledc_channel[i].channel, (valRGB & 0xff) << 5);
      ledc_update_duty(UNUSED, ledc_channel[i].channel);
      printf("ledc channel:%d, val:%d\n", i, (valRGB & 0xff) << 5);
   }
}

void ledc_init(void)
{
   ledc_timer_config_t ledc_timer = {
      .duty_resolution = LED_RESOLUTION,    // resolution of PWM duty
      .freq_hz = 5000,                      // frequency of PWM signal
      .speed_mode = LEDC_HIGH_SPEED_MODE,    // timer mode
      .timer_num = LEDC_TIMER_0,            // timer index
      .clk_cfg = LEDC_AUTO_CLK             // Auto select the source clock
   };

   ledc_timer_config(&ledc_timer);

   for (int i = 0; i < LEDC_CH_NUM; i++) {
      ledc_channel_config(&ledc_channel[i]);
   }
//   ledc_fade_func_install(0);   // Initialize fade service.

   ledc_bright(0x0f0f0f);
}

