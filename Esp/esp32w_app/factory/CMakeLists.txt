FILE(GLOB allc *.c)

idf_component_register(SRCS ${allc}
                       INCLUDE_DIRS "../inc"
                       EMBED_TXTFILES ../config/ca_cert.pem
                       REQUIRES nvs_flash app_update esp_adc_cal esp_http_client
                       esp_https_ota)
