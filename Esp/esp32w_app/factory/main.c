#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "esp_chip_info.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "sock.h"
#include "http_server.h"
#include "ntp.h"
#include "nvs_store.h"
#include "ble_gatt.h"
#include "i2c.h"
#include "wifi_sta.h"
#include "ota_upgrade.h"
#include "main.h"

#define GPIO_CHECKS   (5)

/* global scope objects */
unsigned gFlags = 0;

void app_main (void)
{
  esp_err_t err;

  printf("*** ...and ne forhtedon na ***\n");
  CHKR((err = nvs_flash_init())); /* Initialize NVS */

  if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
  {
    // 1.NVS partition size for OTA app is smaller than the non-OTA.
    // 2.NVS partition contains data in non-recognized new format.
    CHKR(nvs_flash_erase());
    CHKR(nvs_flash_init());
  }

  load_nvs_contxt(); /* load NVS context from flash */
  print_nvs_store();

  wifi_init_sta(); /* start wifi service in 'station' mode */
  ntp_init(); /* start ntp service */

#ifdef WROVER
   ledc_init();         /* configure led controller (pwm) */
#else
  i2c1_init(); /* configure i2c module for OLED */
#endif

  extern esp_err_t gpio_initialize (void);
  printf("gpio init %s\n", gpio_initialize() ? "NG" : "Ok");

#ifdef FACTORYONLY

  /* check for pending OTA requests, if there's a pending request,
   * execute it, otherwise, wait. Only way to force an OTA upgrade
   * is by grounding GPIO 4 for 10 seconds */
  if (!GF(F_OTA_PEND))
  {
    /* no pending requests, wait for user action */
    printf("to force OTA, ground secret pin for 10 secs\n");
    gpio_set_direction(GPIO_NUM_4, GPIO_MODE_INPUT);
    gpio_set_pull_mode(GPIO_NUM_4, GPIO_PULLUP_ONLY);
    /* loop until GPIO4 is grounded for 10 seconds */
    int tries = GPIO_CHECKS; /* number of checks to perform */
    while (1)
    {
      if (0 == gpio_get_level(GPIO_NUM_4))
      { /* GPIO4 is grounded */
        if (0 == tries--)
        { /* grounded for 10 secs: loop out */
          break;
        }
      }
      else
      {
        /* de-glitch any false positives */
        tries = GPIO_CHECKS;
      }

      vTaskDelay(pdMS_TO_TICKS(2000)); /* allow 2 secs in between checks */
    }
  }

  ota_init_tsk(); /* execute OTA initialization */

  return;

#else                   /* OTA image */
  ble_gatt_init(); /* start BLE service */
#ifdef WROVER
   lcd_init();          /* configure LCD (spi) */
#endif

  socket_init(); /* start socket service */
  /* print indentification and status data */
  print_welcome();
#endif

}

void print_welcome (void)
{
  esp_chip_info_t chip_info;

#ifndef FACTORYONLY     /* OTA image */
  printf("******** Upgraded OTA IMAGE ********\n");
#else
   printf("******** FACTORY IMAGE ********\n");
#endif

#ifdef WROVER
#define MSG_PARTNAME "Wrover"
#else
#define MSG_PARTNAME "Wroom"
#endif

#define MSG_WELCOME "Truth is out there!!\nESP32 "\
  MSG_PARTNAME \
  "\n"__DATE__ " " __TIME__ "\nSilicon Rev. "

  esp_chip_info(&chip_info);
  printf(MSG_WELCOME "%d\n", chip_info.revision);
  printf("using %d CPU cores, WiFi%s%s, ",
         chip_info.cores,
         (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
         (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");
  printf("%dMB %s flash\n",
         spi_flash_get_chip_size() / (1 << 20),
         (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" :
             "external");
  printf("Free heap: %d\n", esp_get_free_heap_size());
  print_nvs_store();
}

void chkr (int32_t line, int error)
{
  printf("%d:err_%xh\n", line, error);
  while (1);
}

