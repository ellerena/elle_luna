/*
 * gpio.c
 *
 *  Created on: Sep 28, 2021
 *      Author: ellerena
 */

#include <stdio.h>
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_intr_alloc.h"

#define ESP_INTR_FLAGS ESP_INTR_FLAG_EDGE

static xQueueHandle gpio_Q = NULL;
static int k = 0;

void IRAM_ATTR button_isr_handler (void * arg)
{
  k++;
  xQueueSendFromISR(gpio_Q, &k, NULL);
}

// task that will handle button clicks
void button_task (void * arg)
{
  printf("... ready to rock...\n");

  int i = 0;

  for (;;)
  {
    // wait for ISR notification
    if (xQueueReceive(gpio_Q, &i, portMAX_DELAY))
    {
      printf("B:%d\n", ++i);
    }
  }
}

esp_err_t gpio_initialize (void)
{
  gpio_config_t pin =
    { .pin_bit_mask = GPIO_SEL_15,
      .mode = GPIO_MODE_INPUT,
      .pull_up_en = GPIO_PULLUP_DISABLE,
      .pull_down_en = GPIO_PULLDOWN_ENABLE,
      .intr_type = GPIO_INTR_NEGEDGE };

  esp_err_t result = gpio_config(&pin);
  if (ESP_OK == result)
  {
    // create IPC framework to handle ISR
    gpio_Q = xQueueCreate(20, sizeof(int));
    if (gpio_Q)
    {
      xTaskCreate(&button_task, "button_task", 1024, NULL, 5, NULL);

      // install ISR service and connect ISR handler
      result = gpio_install_isr_service(ESP_INTR_FLAGS);
      gpio_isr_handler_add(GPIO_NUM_15, button_isr_handler, NULL);
    }
    else
    {
      result = ESP_FAIL;
    }
  }

  // configure button and led pins as GPIO pins
//  gpio_pad_select_gpio (CONFIG_BUTTON_PIN);
//  gpio_set_direction (CONFIG_BUTTON_PIN, GPIO_MODE_INPUT);
//  gpio_set_intr_type (CONFIG_BUTTON_PIN, GPIO_INTR_NEGEDGE);

  return result;
}

