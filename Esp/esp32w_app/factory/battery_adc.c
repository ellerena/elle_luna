/*
 * battery_adc.c
 *
 *  Created on: Jun 27, 2020
 *      Author: Eddie Llerena
 */

#include <stdio.h>
#include "sdkconfig.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "battery_adc.h"

static esp_adc_cal_characteristics_t *adc_chars;

/* TODO: accommodate channel selection */
int adc_read_val (void)
{
  uint32_t voltage;
  uint32_t adc_reading = 0;

  for (int i = 0; i < ADC_SAMPLES; ++i)
  {
    adc_reading += adc1_get_raw(BATT_CHAN);
  }
  adc_reading /= ADC_SAMPLES;

  //Convert adc_reading to voltage in mV
  voltage = esp_adc_cal_raw_to_voltage(adc_reading, adc_chars);
  printf("Raw: %u [%umV]\n", adc_reading, voltage);

  return voltage;
}

void adc_init (void)
{
  esp_adc_cal_value_t val_type;

  //Configure ADC
  adc1_config_width(BATT_WIDTH);
  adc1_config_channel_atten(BATT_CHAN, BATT_ATTENUATION);

  //Characterize ADC
  adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
  val_type = esp_adc_cal_characterize(ADC_UNIT_1, BATT_ATTENUATION, BATT_WIDTH,
  DEFAULT_VREF,
                                      adc_chars);

  printf("ADC Vref: ");
  switch (val_type)
  {
    case ESP_ADC_CAL_VAL_EFUSE_TP:
      printf("Two Point Value\n");
      break;

    case ESP_ADC_CAL_VAL_EFUSE_VREF:
      printf("eFuse Vref\n");
      break;

    default:
      printf("Default Vref\n");
  }

}

