/*
 * ntp.c
 *
 *  Created on: Jun 10, 2020
 *      Author: eddie
 */

#include "esp_sntp.h"
#include "ntp.h"
#include "nvs_store.h"

#define TAG    "ntp"

char timestr[TIMESTRSZ];

void sntp_sync_time (struct timeval * tv)
{
  /* update system time */
  settimeofday(tv, NULL);

  /* we don't need periodic sync for now */
  sntp_set_sync_status(SNTP_SYNC_STATUS_COMPLETED);

  /* update NVS store (context) in flash */
  save_nvs_store();
}

void update_time_str (int e_report)
{
  struct tm *loctim;

  /* update system time in global context variable */
  time(&sta_cntxt.globtime);

  /* convert time variable to date/time structure */
  loctim = localtime(&sta_cntxt.globtime);

  switch (e_report)
  {

    case HTTP_TIME: /* format for HTML/HTTP responses */
      strftime(timestr, TIMESTRSZ - 7, "<p>%c</p>", loctim);
      break;

    case TXT_TIME: /* simple text format */
      strftime(timestr, TIMESTRSZ - 7, "%c", loctim);
      break;

    case OLED_TIME:

    default:
      strftime(timestr, TIMESTRSZ - 7, "%D %T", loctim);
      break;
  }
}

void ntp_init (void)
{
  /* NTP will work in unicast polling mode */
  sntp_setoperatingmode(SNTP_OPMODE_POLL);

  /* set NTP server address (url) */
  sntp_setservername(0, NTP_SERVER);

  /* start ntp synchronization */
  sntp_init();
}

