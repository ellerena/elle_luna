/* OTA example

 This example code is in the Public Domain (or CC0 licensed, at your option.)

 Unless required by applicable law or agreed to in writing, this
 software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied.
 */
#include <string.h>           /* required for strcmp */
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include "esp_wifi.h"
#include "nvs_store.h"
#include "ota_upgrade.h"
#include "main.h"

#define TAG       "ota"

extern const uint8_t server_cert_pem_start[] asm("_binary_ca_cert_pem_start");
//extern const uint8_t server_cert_pem_end[] asm("_binary_ca_cert_pem_end");

static esp_err_t _http_event_handler (esp_http_client_event_t * evt)
{
  switch (evt->event_id)
  {
    case HTTP_EVENT_ERROR:
    case HTTP_EVENT_ON_CONNECTED:
    case HTTP_EVENT_HEADER_SENT:
    case HTTP_EVENT_ON_HEADER:
    case HTTP_EVENT_ON_FINISH:
    case HTTP_EVENT_DISCONNECTED:
      printf("Ota %d\n", evt->event_id);
      break;

    case HTTP_EVENT_ON_DATA:
      printf(".");
      break;
  }

  return ESP_OK;
}

static void ota_task (void * pvParameter)
{
  /* flash may be empty, in such case use
   * hard coded OTA FW url location */
  if (memcmp("http", sta_cntxt.ota_fw_url, 4))
  { /* if no url is stored */

    size_t len;

    printf("using factory url\n");
    len = sizeof(CNTXT_OTA_URL); /* size of fix FW url (with '\0') */
    memcpy(sta_cntxt.ota_fw_url, CNTXT_OTA_URL, len); /* use hard coded FW url */
  }

  printf("FW url: %s\n", sta_cntxt.ota_fw_url);

  esp_http_client_config_t config =
    { .url = sta_cntxt.ota_fw_url,
      .cert_pem = (char*) server_cert_pem_start,
      .event_handler = _http_event_handler };

  if (ESP_OK == esp_https_ota(&config))
  {
    GFC(F_OTA_PEND); /* clear pending OTA update flag */
    save_nvs_store(); /* update NVS context and save it */
    esp_restart();
    vTaskDelay(pdMS_TO_TICKS(1000));
  }
  else
  {
    printf("FW upgrade fail\n");
    vTaskDelete(NULL);
    return;
  }
}

/*
 * @brief   this is the external call used to start
 *          OTA FW upgrade
 * */
void ota_init_tsk (void)
{
  esp_wifi_set_ps(WIFI_PS_NONE);
  xTaskCreate(&ota_task, "ota_task", 8192, NULL, 5, NULL);
}

void print_part_info (const esp_partition_t * piam)
{
  printf("Name: %s\nType: %10d, Sub:%d\n",
         piam->label,
         piam->type,
         piam->subtype);
  printf("Addr: 0x%08x, Sz :%u\nEncr:%d\n",
         piam->address,
         piam->size,
         piam->encrypted);
}

void ota_who_am_i (void)
{
  print_part_info(esp_ota_get_running_partition());
}

void ota_who_is_nxt (void)
{
  print_part_info(esp_ota_get_next_update_partition(NULL));
}

void ota_set_boot_app (esp_partition_subtype_t subtype)
{
  esp_partition_t *ppart;

  /* search for an existing subtype partition in NVS */
  ppart = (esp_partition_t*) esp_partition_find_first(ESP_PARTITION_TYPE_APP,
                                                      subtype,
                                                      NULL);

  /* if subtype partition exists, set it as next boot */
  if (ppart)
  {
    CHKR(esp_ota_set_boot_partition(ppart));
    printf("next boot:\n");
    print_part_info(ppart);
  }
}

void ota_request_upgrade (void)
{
  /* set OTA flag, upgrade will occur later */
  GFS(F_OTA_PEND);

  /* save context so the flag is found on next boot */
  save_nvs_store();

  /* request next boot from factory partition */
  ota_set_boot_app(ESP_PARTITION_SUBTYPE_APP_FACTORY);

  /* now let's restart the system */
  printf("system is restarting\n");
  vTaskDelay(pdMS_TO_TICKS(6000));

  esp_restart();
}

