/*
 * http_server.c
 *
 *  Created on: Jun 22, 2020
 *      Author: Eddie Llerena
 */

#include <sys/param.h>
#include <esp_http_server.h>
#include "esp_system.h"
#include "esp_log.h"
#include "esp_event.h"
#include "driver/ledc.h"
#include <time.h>
#include "http_server.h"
#include "ntp.h"
#include "html.h"
#include "main.h"

#define TAG          "http"
#define FAKEHOST     "10.0.0.200"

static httpd_handle_t server = NULL;

static void send_response_html(httpd_req_t *req, const char * host_name)
{
   /* update our global time string */
   update_time_str(HTTP_TIME);

   /* Send response with custom headers and body */
   httpd_resp_sendstr_chunk(req, PAGE_HTML_INDEX0);                  /* 1st part */
   httpd_resp_sendstr_chunk(req, host_name ? host_name : FAKEHOST);  /* insert our host name */
   httpd_resp_sendstr_chunk(req, PAGE_HTML_INDEX1);                  /* 2nd part */
   httpd_resp_sendstr_chunk(req, timestr);                           /* insert request time */
   httpd_resp_sendstr_chunk(req, NULL);                              /* End response */
}

esp_err_t get_handler(httpd_req_t *req)
{
   size_t buf_len;
   char * buf,  *host_name = NULL;

   /* get value of 'Host' field from the header */
   buf_len = httpd_req_get_hdr_value_len(req, "Host") + 1;
   if (buf_len > 1) {
      host_name = malloc(buf_len);
      httpd_req_get_hdr_value_str(req, "Host", host_name, buf_len);
   }

   /* get query string and parse params */
   buf_len = httpd_req_get_url_query_len(req) + 1;
   if (buf_len > 1) {
      buf = malloc(buf_len);
      if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK) {
         INFOMSG("Host %s, query: %s", host_name, buf);
         char param[32];
         /* Get query strings */
         if (httpd_query_key_value(buf, "par0", param, sizeof(param)) == ESP_OK) {
            INFOMSG("URL par0=%s", param);
         }
      }
      free(buf);
   }

   /* Set some custom headers */
   httpd_resp_set_hdr(req, "Custom0", "Ok");

   /* Send response with custom headers and body */
   send_response_html(req, host_name);
   free(host_name);

   return ESP_OK;
}

httpd_uri_t http_get = {
    .uri       = "/",
    .method    = HTTP_GET,
    .handler   = get_handler,
    .user_ctx  = NULL //PAGE_HTML_INDEX0  /* pass response string */
};

esp_err_t echo_post_handler(httpd_req_t *req)
{
   char buf[100];
   int ret, remaining = req->content_len;

   while (remaining > 0) {
      /* Read the data for the request */
      if ((ret = httpd_req_recv(req, buf, MIN(remaining, sizeof(buf)))) <= 0) {
         if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
            continue;    /* Retry receiving if timeout occurred */
         }
         return ESP_FAIL;
      }

      httpd_resp_send_chunk(req, buf, ret);
      remaining -= ret;

     /* Log data received */
     INFOMSG("=========== HTTP POST ==========");
     INFOMSG("%.*s", ret, buf);
     INFOMSG("================================");
   }

   send_response_html(req, NULL);

   return ESP_OK;
}

httpd_uri_t http_post = {
    .uri       = "/",
    .method    = HTTP_POST,
    .handler   = echo_post_handler,
    .user_ctx  = NULL
};

/* An HTTP PUT handler. realtime registration and deregistration of URI handlers */
esp_err_t ctrl_put_handler(httpd_req_t *req)
{
   char buf;
   int ret;

   if ((ret = httpd_req_recv(req, &buf, 1)) <= 0) {
      if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
         httpd_resp_send_408(req);
      }
      return ESP_FAIL;
   }

   if (buf == '0') {
      /* Handler can be unregistered using the uri string */
      INFOMSG("Unregistering /http_get and /http_post URIs");
      httpd_unregister_uri(req->handle, "/http_get");
      httpd_unregister_uri(req->handle, "/");
   }
   else {
      INFOMSG("Registering /http_get and /http_post URIs");
      httpd_register_uri_handler(req->handle, &http_get);
      httpd_register_uri_handler(req->handle, &http_post);
   }

   /* Respond with empty body */
   httpd_resp_send(req, NULL, 0);
   return ESP_OK;
}

httpd_uri_t http_put = {
   .uri       = "/",
   .method    = HTTP_PUT,
   .handler   = ctrl_put_handler,
   .user_ctx  = NULL
};

httpd_handle_t start_webserver(void)
{
   httpd_handle_t server = NULL;
   httpd_config_t config = HTTPD_DEFAULT_CONFIG();

   // Start the httpd server
   printf("Starting server on port: '%d'\n", config.server_port);
   if (httpd_start(&server, &config) == ESP_OK) {
      // Set URI handlers
      INFOMSG("Registering URI handlers");
      httpd_register_uri_handler(server, &http_get);
      httpd_register_uri_handler(server, &http_post);
      httpd_register_uri_handler(server, &http_put);
      return server;
   }

   INFOMSG("Error starting server!");
   return NULL;
}

inline void stop_webserver(httpd_handle_t server)
{  // Stop the httpd server
   httpd_stop(server);
}

void disconnect_handler(void* arg, esp_event_base_t event_base, int32_t event_id,
      void* event_data)
{
   httpd_handle_t* server = (httpd_handle_t*) arg;
   if (*server) {
      printf("Stopping webserver\n");
      stop_webserver(*server);
      *server = NULL;
   }
}

void connect_handler(void* arg, esp_event_base_t event_base, int32_t event_id,
      void* event_data)
{
   httpd_handle_t* server = (httpd_handle_t*) arg;
   if (*server == NULL) {
      *server = start_webserver();
      printf("WEBserver started\n");
   }
}

/*
 * @brief   This is the main function to start web server
 * */
void http_start(void)
{
   CHKERR(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &connect_handler, &server));
   CHKERR(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &disconnect_handler, &server));

   server = start_webserver();
}

