/*
 * uart_ifs.c
 *
 *  Created on: Jun 23, 2020
 *      Author: Eddie Llerena
 */

//#include <stdio.h>
#include "driver/gpio.h"
#include "uart_ifs.h"
#include "main.h"

#define TAG       "uart_ifs"

static void uart_ifs_init_task(void *arg)
{
   /* Configure parameters for UART-A driver */
   uart_config_t uart_cnf_A = {
         .baud_rate = BAUDR_A,
         .data_bits = UART_DATA_8_BITS,
         .parity    = UART_PARITY_DISABLE,
         .stop_bits = UART_STOP_BITS_1,
         .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
         .rx_flow_ctrl_thresh = TRESH_A,
         .source_clk = UART_SCLK_APB,
   };

//   uart_driver_install(UART_NUM_0, BUF_RA * 2, 0, 0, NULL, 0);
//   uart_param_config(UART_NUM_0, &uart_cnf_A);
//   uart_set_pin(UART_NUM_0, TX_A, RX_A, RTS_A, CTS_A);

   LOGERR(uart_driver_install(UART_NUM_1, BUF_RA * 2, 0, 0, NULL, 0));
   LOGERR(uart_param_config(UART_NUM_1, &uart_cnf_A));
   LOGERR(uart_set_pin(UART_NUM_1, TX_B, RX_B, RTS_B, CTS_B));

   LOGERR(uart_driver_install(UART_NUM_2, BUF_RA * 2, 0, 0, NULL, 0));
   LOGERR(uart_param_config(UART_NUM_2, &uart_cnf_A));
   LOGERR(uart_set_pin(UART_NUM_2, TX_C, RX_C, RTS_C, CTS_C));
}

void uart_ifs_init(void)
{
   uart_ifs_init_task(NULL);
//   xTaskCreate(uart_ifs_init_task, "uart_if_init_task", 1024, NULL, 10, NULL);
}

