/*
 * mqtt_client.c
 *
 *  Created on: Jul 3, 2020
 *      Author: Eddie Llerena
 */

#include <stdio.h>
#include "esp_event.h"
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "mqtt_client.h"
#include "mqtt_cl.h"
#include "main.h"

#define TAG __FUNCTION__

static esp_mqtt_client_handle_t client = NULL;

char * pFreeTopic = NULL;     /* pointer to free topic name */

static void process_data_txt (void * str, int len)
{
   printf(BRKR_TOP_T ":%.*s\n", len, (const char *)str);
}

static void process_data_bin (void * data, int len)
{
   const uint8_t *pdata;

   pdata = (const uint8_t *) data;
   printf(BRKR_TOP_D ":");
   while (len--)
   {
      printf(" %02xh", *pdata++);
   }
   printf("\n");
}

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
   switch (event->event_id) {

      case MQTT_EVENT_CONNECTED:       /* connected to broker, let's subscribe */
         mqtt_subscribe(BRKR_TOP_T);   /* subscribe text topic */
         mqtt_subscribe(BRKR_TOP_D);   /* subscribe data topic */
         break;

      case MQTT_EVENT_DISCONNECTED:
         printf(TAG ": disconnected\n");
         break;

      case MQTT_EVENT_DATA:            /* MQTT data received */
        if (TOP_TYPE(event->topic) == '0') {
           process_data_txt(event->data, event->data_len);
        }
        if (TOP_TYPE(event->topic) == '1') {
           process_data_bin(event->data, event->data_len);
        }
        break;

      case MQTT_EVENT_ERROR:
         printf(TAG ": event error\n");
         break;

      case MQTT_EVENT_SUBSCRIBED:
      case MQTT_EVENT_UNSUBSCRIBED:
      case MQTT_EVENT_PUBLISHED:
         break;

      default:
         printf(TAG "unknown event %d\n", event->event_id);
         break;
   }

   return ESP_OK;
}

static void mqtt_app_start(void)
{
    esp_mqtt_client_config_t mqtt_cfg = {
        .uri = BRKR_URL,
        .event_handle = mqtt_event_handler,
    };

    client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_start(client);
}

void mqtt_init(void)
{
//    esp_log_level_set("*", ESP_LOG_INFO);
//    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
//    esp_log_level_set("MQTT_EXAMPLE", ESP_LOG_VERBOSE);
//    esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
//    esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
//    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
//    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

   if (NULL == client) {
      mqtt_app_start();
   }
}

void mqtt_deinit(void)
{
   if (client) {
      mqtt_unsubscribe(BRKR_TOP_T);
      mqtt_unsubscribe(BRKR_TOP_D);
      esp_mqtt_client_stop(client);
      esp_mqtt_client_disconnect(client);
      esp_mqtt_client_destroy(client);
      client = NULL;
   }

   if (pFreeTopic) {
      free(pFreeTopic);
      pFreeTopic = NULL;
   }
}

/**
 * @brief:  publish data or text to the broker
 * @param:  data: pointer to data location
 * @param:  top_len: 0: data is text, otherwise it
 *          represents length of binary data
 * */
void mqtt_publish(void * data, int top_len) {

   if (client) {

      if (top_len == 0) {
         esp_mqtt_client_publish(client, BRKR_TOP_T,
            (const char *)data, strlen(data),
            1, 0);
      }
      else {
         esp_mqtt_client_publish(client, BRKR_TOP_D,
            (const char *)data, top_len,
            1, 0);
      }
   }
}

/**
 * @brief:  publish data or text to the broker
 *          without connection/subscription
 * @param:  data: pointer to data location
 * @param:  top_len: 0: data is text, otherwise it
 *          represents length of binary data
 * */
void mqtt_publish_free(void * data, int top_len) {

   if (client && pFreeTopic) {

      esp_mqtt_client_publish(client, pFreeTopic,
         (const char *)data,
         (top_len == 0) ? strlen(data) : top_len,
         1, 0);

   }
}

/**
 * @brief   this function sets the topic name for
 *          un-connected posts to the broker
 * @param   topic: string name of the topic
 *          (must be null ended)
 * */
void mqtt_free_topic_name(const char * topic)
{
   size_t len;

   len = strlen(topic) + sizeof(char);

   if (len > sizeof(char)) {
      if (pFreeTopic) {
         free (pFreeTopic);
      }

      pFreeTopic = (char *)malloc(len);

      memcpy(pFreeTopic, topic, len);
   }
}

esp_err_t mqtt_subscribe(const char * data) {

   esp_err_t result = ESP_FAIL;

   if (client) {

      result = esp_mqtt_client_subscribe(client, data, 1);

   }

   printf(TAG ": %s subscribe: %d\n", data, result);

   return result;
}

void mqtt_unsubscribe(const char * data) {
   if (client) {
      esp_mqtt_client_unsubscribe(client, data);
   }
}


