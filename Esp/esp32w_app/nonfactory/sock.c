/*
 * sock.c
 *
 *  Created on: May 28, 2020
 *      Author: eddie

 BSD Socket API Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "freertos/FreeRTOS.h"
#include "command_handlers.h"
#include "esp_log.h"
#include <lwip/sockets.h>
#include "sock.h"
#include "ntp.h"
#include "command_handlers.h"
#include "main.h"

#define TAG             "sock"
#define HTTP_200_TXT    "HTTP/1.0 200 OK\r\n\r\nParis et Libere!\n\0"
#define HTTP_200_SZ     (sizeof(HTTP_200_TXT))
char buf[SOCK_BUF];

/**
 * @brief   handling of each single connection
 *
 * */
static void srv_connection_handler(int scli)
{
   int n;

   /* loop for each received complete package */
   for(;;)
   {
      GFC(M_SOCKET);                   /* clear any pending socket flags */
      while ((n = recv(scli, buf, SOCK_BUF - 1, 0)) > 0) { /* receive loop */
         if (strstr(buf, "HTTP"))      /* request comes from HTTP? */
            GFS(F_HTTP);               /* flag as HTTP request */
         buf[n] = '\0';                /* null end the request string */
         printf("%d>%s\n", n, buf);    /* present len and text of request */
         if (buf[n-1] <= '\n')         /* end of msg - package received complete */
            break;
      }

      if (n) {                         /* data has been received... */
         if (GF(F_HTTP)){              /* data received from HTTP client */
            memcpy(buf, HTTP_200_TXT, HTTP_200_SZ);   /* build valid response */
            update_time_str(TXT_TIME); /* add current time to our response */
            send(scli, buf, (int)strlen(buf), 0);  /* send partial response */
            send(scli, timestr, (int)strlen(timestr), 0); /* send partial response */
            GFS(F_ENDCON);             /* flag to close the connection */
         }
         else if (buf[0] == '.') {     /* request to close connection */
            GFS(F_ENDCON);             /* flag to close the connection */
         }
         if (buf[1] == '.') {          /* request to shutdown server */
            GFS(F_ENDPROG + F_ENDCON); /* flag to close & exit program */
            break;
         }
         if (GF(F_ENDCON)) {           /* if end of connection has been requested */
            break;
         }

         /* if we arrive here, we received an execution command request */
         CommandProcessor(buf);           /* execute request */
         send(scli, buf, strlen(buf), 0); /* send response to client */
      }
   }
}

/**
 * @brief   main socket server task
 * */
void tcp_server_task(void *pvParameters)
{
   int ssrv, scli;
   struct sockaddr_in saddr;

   /* create a main server socket */
   CHKR((ssrv = socket(AF_INET, SOCK_STREAM, IPPROTO_IP)) < 0);

   /* configure server socket type and address */
   saddr.sin_family = AF_INET;
   saddr.sin_addr.s_addr = htonl(INADDR_ANY);
   saddr.sin_port = htons(SSOCK_PRT);

   /* bind socket to a port and start listening for connections */
   CHKR (bind(ssrv, (struct sockaddr *)&saddr, sizeof(saddr)) != 0);
   CHKR (listen(ssrv, 1) != 0);        /* no backlog queue */

   /* main socket loop, for each connection received */
   for(;;) {

      struct sockaddr_in caddr;
      uint slen = sizeof(caddr);

      /* wait here, if a client calls, create a client socket */
      printf("Wait for client @ %d\n", SSOCK_PRT);
      scli = accept(ssrv, (struct sockaddr *)&caddr, &slen);

      /* client connection requested, validate it */
      if (scli < 0) continue;    /* discard invalid requests */

      /* we got a valid request, get client's credentials */
      inet_ntoa_r(caddr.sin_addr.s_addr, buf, SOCK_BUF - 1);
      printf("Molon Labe: %s\n", buf);

      /* clear remains of any previous client connections */
      GFC(M_SOCKET);

      /* handle the data flow - not multi-threaded */
      srv_connection_handler(scli);

      /* done with this transmission, close client's socket */
      closesocket(scli);

      if (GF(F_ENDPROG)) /* if program exit requested */
         break;
   }

   vTaskDelete(NULL);
}

void socket_init(void)
{
   xTaskCreate(tcp_server_task, "tcp_server", 4096, NULL, 5, NULL);
}
