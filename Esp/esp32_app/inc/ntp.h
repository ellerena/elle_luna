#ifndef __NTP_HEADER__
#define __NTP_HEADER__

#define TIMESTRSZ    (40)

enum E_REPORT {
   HTTP_TIME,
   TXT_TIME
};

extern char timestr[];

void ntp_init(void);
void update_time_str(int e_report);

#endif
