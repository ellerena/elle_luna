#ifndef __SOCK_HEADER__
#define __SOCK_HEADER__

#define SOCK_BUF        (512)

extern unsigned gFlags;

extern void tcp_server_task(void *pvParameters);

#endif
