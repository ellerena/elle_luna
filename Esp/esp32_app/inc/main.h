/*
 * main.h
 *
 *  Created on: May 28, 2020
 *      Author: eddie
 */

#ifndef INC_MAIN_H_
#define INC_MAIN_H_

/* Common functions for protocol examples, to establish Wi-Fi or Ethernet connection.

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "esp_err.h"

#ifdef LOG_LOCAL_LEVEL
#undef LOG_LOCAL_LEVEL
#define LOG_LOCAL_LEVEL    (3)
#endif

#ifndef CONFIG_FREERTOS_HZ
#define CONFIG_FREERTOS_HZ (100)
#endif

#define NDEBUG
#define SSOCK_PRT          (5005)
#define INFOMSG(...)       ESP_LOG_LEVEL(ESP_LOG_INFO, TAG, ##__VA_ARGS__);
#define CHKMSG(c)          if(c) {ESP_LOG_LEVEL(ESP_LOG_ERROR, TAG, "error @ %d", __LINE__); break;}
#define CHKRET(c, m)       if(c) {ESP_LOG_LEVEL(ESP_LOG_ERROR, TAG, "error @ %d", __LINE__); return;} INFOMSG(m)
#define CHKBRK(c, m)       CHKMSG(c) ESP_LOGI(TAG, m)
#define CHKERR(f)          if (f != ESP_OK) {ESP_LOG_LEVEL(ESP_LOG_ERROR, TAG, "error @ %d", __LINE__); return;}

#define F_ENDCON     (1 << 16)
#define F_ENDPROG    (1 << 17)
#define F_SOCKET     (1 << 0)
#define GF(x)        (gFlags & (x))
#define GFS(x)       do {gFlags |= (x);} while(0)
#define GFC(x)       do {gFlags &= ~(x);} while(0)
#define GFCLEAR      do {gFlags = 0;} while(0)

extern unsigned gFlags;

#ifdef __cplusplus
}
#endif



#endif /* INC_MAIN_H_ */
