#ifndef __CONNECT_HEADER__
#define __CONNECT_HEADER__

/**
 * @brief Configure Wi-Fi or Ethernet, connect, wait for IP
 *
 * @return ESP_OK on successful connection
 */
extern esp_err_t live_connect(void);

extern esp_err_t live_disconnect(void);

#ifndef CONFIG_ESP32_WIFI_STATIC_RX_BUFFER_NUM
#define CONFIG_ESP32_WIFI_STATIC_RX_BUFFER_NUM     10
#define CONFIG_ESP32_WIFI_DYNAMIC_RX_BUFFER_NUM    32
#define CONFIG_ESP32_WIFI_TX_BUFFER_TYPE           1
#endif

#endif
