/*
 * ble_gatt.h
 *
 *  Created on: Jun 10, 2020
 *      Author: Eddie Llerena
 */

#ifndef MAIN_BLE_GATT_H_
#define MAIN_BLE_GATT_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Attributes State Machine */
enum
{
    IDX_SVC,

    IDX_CHAR_A,
    IDX_CHAR_VAL_A,
    IDX_CHAR_CFG_A,

    IDX_CHAR_B,
    IDX_CHAR_VAL_B,

    IDX_CHAR_C,
    IDX_CHAR_VAL_C,

    HRS_IDX_NB,
};

void ble_gatt_init(void);

#endif /* MAIN_BLE_GATT_H_ */
