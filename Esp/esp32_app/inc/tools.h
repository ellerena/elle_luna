/*
 * tools.h
 *
 *  Created on: Jun 12, 2020
 *      Author: Eddie Llerena
 */

#ifndef MAIN_TOOLS_H_
#define MAIN_TOOLS_H_

int ParseCharsToValueHex(char *p);

#endif /* MAIN_TOOLS_H_ */
