/*
 * sock.c
 *
 *  Created on: May 28, 2020
 *      Author: eddie

 BSD Socket API Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "sock.h"

#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include <lwip/sockets.h>

#include "GFX_Library.h"
#include "i2c.h"
#include "main.h"
#include "ntp.h"
#include "tools.h"

#define TAG    "sock"

char buf[SOCK_BUF];

static void comm_proccessor(char * buf)
{
   switch (buf[0]) {
      case 'i':      /* i: i2c commands (oled) */
         switch (buf[1]) {
            case 'a':         /* ia<xx : 1 hex> address of slave */
               i2c_addr(buf);
               break;
            case 's':         /* scan slaves present in bus */
               i2c_scan();
               break;
            case 'w':         /* write data bytes to slave */
               i2c1_wr(buf);
               break;
            default:
               break;
         }
         break;

      case 'o':
         switch (buf[1]) {
            case 'i':         /* oi: oled initialization */
               gfx_init();
               break;
            case 'a':         /* oa: print ascii pattern */
               gfx_ascii();
               break;
            case 'c':         /* ic<xxyy....: n hex> commands to send to slave */
               i2c1_cmd(buf);
               break;
            default:
               break;
         }
         break;
   }
}

static void srv_connection_handler(int scon)
{
   int n;

   for(;;)
   {
      GFCLEAR;
      while ((n = recv(scon, buf, SOCK_BUF - 1, 0)) > 0) { /* receive loop */
         if (strstr(buf, "HTTP"))
            GFS(F_SOCKET);
         buf[n] = '\0';
         printf("%d>%s\n", n, buf);
         comm_proccessor(buf);
         if (buf[n-1] <= '\n') /* end of msg */
            break;
      }

      if (n) {
         if (GF(F_SOCKET)){
            snprintf(buf, SOCK_BUF, "HTTP/1.0 200 OK\r\n\r\nParis et Libere!\n");
            update_time_str(TXT_TIME);
            send(scon, buf, (int)strlen(buf), 0);
            send(scon, timestr, (int)strlen(timestr), 0);
            GFS(F_ENDCON);     /* flag to close the connection */
         }
         else if (buf[0] == '.') {
            GFS(F_ENDCON);     /* flag to close the connection */
         }
         if (buf[1] == '.') {
            GFS(F_ENDPROG + F_ENDCON);     /* flag to close & exit program */
            break;
         }
         if (GF(F_ENDCON)) {
            break;
         }

         send(scon, "Ok", 2, 0);
      }
   }
}

void tcp_server_task(void *pvParameters)
{
   int ssrv, scon;
   struct sockaddr_in saddr;

   ssrv = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
   CHKRET (ssrv < 0, "Socket created");

   saddr.sin_family = AF_INET;
   saddr.sin_addr.s_addr = htonl(INADDR_ANY);
   saddr.sin_port = htons(SSOCK_PRT);

   CHKRET (bind(ssrv, (struct sockaddr *)&saddr, sizeof(saddr)) != 0,"Socket binded");
   CHKRET (listen(ssrv, 1) != 0, "Socket listening");

   for(;;) {

      struct sockaddr_in caddr;
      uint slen = sizeof(caddr);

      printf("Waiting for client @ %d...\n", SSOCK_PRT);
      scon = accept(ssrv, (struct sockaddr *)&caddr, &slen);
      if (scon < 0) continue;
      inet_ntoa_r(caddr.sin_addr.s_addr, buf, SOCK_BUF - 1);
      printf("Molon Labe: %s\n", buf);

      GFCLEAR;
      srv_connection_handler(scon);
      closesocket(scon);

      if (GF(F_ENDPROG)) /* if program exit requested */
         break;
   }

   vTaskDelete(NULL);
}
