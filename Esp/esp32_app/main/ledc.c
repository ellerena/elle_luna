/* LEDC (LED Controller) fade example
   This example code is in the Public Domain (or CC0 licensed, at your option.)
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"

#define LED_RESOLUTION    (LEDC_TIMER_13_BIT)
#define LED_MAX           ((1 << LED_RESOLUTION) - 1)
#define LEDC_CH_NUM       (1)
#define LEDC_DUTY         (4096)
#define LEDC_FADE_TIME    (5000)
#define TST_DELAY         (1)
#define CH                (1)
#define UNUSED            (0)

static ledc_channel_config_t ledc_channel[LEDC_CH_NUM] = {
   {
      .channel    = LEDC_CHANNEL_0,
      .duty       = 0,
      .gpio_num   = 0,
      .speed_mode = LEDC_HIGH_SPEED_MODE,
      .hpoint     = 0,
      .timer_sel  = LEDC_TIMER_0,
   }
};

void ledc_bright(int channel, int val)
{
   val = val % 16;         /* 0..15 */
   val = (val << (LED_RESOLUTION - 4));
   printf("\tledc channel:%d, val:%d\n", channel, val);
   ledc_set_duty(UNUSED, ledc_channel[channel].channel, val);
   ledc_update_duty(UNUSED, ledc_channel[channel].channel);
}

void ledc_init(void)
{
   ledc_timer_config_t ledc_timer = {
      .duty_resolution = LED_RESOLUTION,    // resolution of PWM duty
      .freq_hz = 5000,                      // frequency of PWM signal
      .speed_mode = LEDC_HIGH_SPEED_MODE,    // timer mode
      .timer_num = LEDC_TIMER_0,            // timer index
      .clk_cfg = LEDC_AUTO_CLK             // Auto select the source clock
   };

   ledc_timer_config(&ledc_timer);

   ledc_channel_config(&ledc_channel[0]);
   ledc_fade_func_install(0);   // Initialize fade service.

   printf("LEDC On\n");
   ledc_bright(0,14);
}


