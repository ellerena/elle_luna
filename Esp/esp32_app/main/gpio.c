/*
 * gpio.c
 *
 *  Created on: Sep 28, 2021
 *      Author: ellerena
 */

#include <stdio.h>

#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"

#define ESP_INTR_FLAG_DEFAULT 0

SemaphoreHandle_t xSemaphore = NULL;

// interrupt service routine, called when the button is pressed
void IRAM_ATTR button_isr_handler (void *arg)
{

  // notify the button task
  xSemaphoreGiveFromISR(xSemaphore, NULL);
}

// task that will react to button clicks
void button_task (void *arg)
{
  int i = 0;

  // infinite loop
  for (;;)
  {

    // wait for the notification from the ISR
    if (xSemaphoreTake(xSemaphore,portMAX_DELAY) == pdTRUE)
    {
      printf ("B:%d", ++i);
    }
  }
}

esp_err_t gpio_initialize (void)
{
  esp_err_t result;
  gpio_config_t pin =
    { .pin_bit_mask = GPIO_SEL_0, .mode = GPIO_MODE_INPUT, .pull_up_en =
        GPIO_PULLUP_ENABLE, .pull_down_en = GPIO_PULLDOWN_DISABLE, .intr_type =
        GPIO_INTR_NEGEDGE };

  result = gpio_config (&pin);

  // configure button and led pins as GPIO pins
//  gpio_pad_select_gpio (CONFIG_BUTTON_PIN);
//  gpio_set_direction (CONFIG_BUTTON_PIN, GPIO_MODE_INPUT);
//  gpio_set_intr_type (CONFIG_BUTTON_PIN, GPIO_INTR_NEGEDGE);

// start the task that will handle the button
  xTaskCreate (button_task, "button_task", 2048, NULL, 10, NULL);

  if (ESP_OK == result)
  {
    // install ISR service with default configuration
    result = gpio_install_isr_service (ESP_INTR_FLAG_DEFAULT);

    if (ESP_OK == result)
    {
      // attach the interrupt service routine
      gpio_isr_handler_add (GPIO_NUM_0, button_isr_handler, NULL);
    }
  }

  return result;
}

