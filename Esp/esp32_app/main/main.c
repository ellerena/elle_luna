#include "main.h"

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/FreeRTOSConfig.h"
#include "freertos/timers.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_netif.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "driver/uart.h"
#include "driver/gpio.h"

#include "ble_gatt.h"
#include "connect.h"
#include "http.h"
#include "i2c.h"
#include "ntp.h"
#include "sock.h"
#include "tools.h"
//#include "gpio.h"

extern void ledc_init (void);
extern void ledc_bright (int channel, int val);

extern void timer0 (TimerHandle_t htimer);
static TimerHandle_t htimer0 = NULL;

unsigned gFlags = 0;

int app_main (void)
{
  esp_chip_info_t chip_info;

  vTaskDelay (pdMS_TO_TICKS(3000));
  esp_chip_info (&chip_info);
  printf (
      "Truth is out there!!\nESP32 build " __DATE__ " " __TIME__ "\nSilicon Rev. %d\n",
      chip_info.revision);
  printf ("using %d CPU cores, WiFi%s%s, ", chip_info.cores,
          (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
          (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");
  printf (
      "%dMB %s flash\n", spi_flash_get_chip_size () / (1 << 20),
      (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");
  printf ("Free heap: %d\n", esp_get_free_heap_size ());

  ESP_ERROR_CHECK(nvs_flash_init ());
  ESP_ERROR_CHECK(esp_netif_init ());
  ESP_ERROR_CHECK(esp_event_loop_create_default ());
//   gpio_pad_select_gpio(5);
//   gpio_set_direction(5, GPIO_MODE_OUTPUT);
  ble_gatt_init ();
  ESP_ERROR_CHECK(live_connect ());
  http_start ();
  ntp_init ();
  ledc_init ();
  i2c1_init ();

  xTaskCreate (tcp_server_task, "tcp_server", 4096, NULL, 5, NULL);
  htimer0 = xTimerCreate ("tim0", pdMS_TO_TICKS(6000), pdTRUE, (void*) 1,
                          timer0);
  xTimerStart(htimer0, 0);

  return 0;
}

void timer0 (TimerHandle_t htimer)
{
  static int fl = 0;
  long lTimerId;
  configASSERT( htimer );

  lTimerId = (long) pvTimerGetTimerID (htimer);

  fl ^= 0xf;
//   gpio_set_level(5, fl);
//   ledc_bright(0, fl);
//   i2c2_test();
  if (lTimerId != 1)
  {
    printf (".");
  }

}

