/*
 * ntp.c
 *
 *  Created on: Jun 10, 2020
 *      Author: eddie
 */

#include "ntp.h"

#include <time.h>
#include "esp_log.h"
#include "esp_sntp.h"

#include "main.h"

#define TAG    "ntp"

time_t t ;
struct tm *loctim ;
char timestr[TIMESTRSZ];

#if 0
void sntp_sync_time(struct timeval *tv)
{
   settimeofday(tv, NULL);
   sntp_set_sync_status(SNTP_SYNC_STATUS_COMPLETED);
}
#endif

void time_sync_notification_cb(struct timeval *tv)
{
   INFOMSG("Notification of a time synchronization event");
}

void update_time_str(int e_report)
{
   time( &t );
   loctim = localtime( &t );

   switch (e_report) {
      case HTTP_TIME:
         strftime(timestr, TIMESTRSZ-7, "<p>%c</p>", loctim);
         break;
      case TXT_TIME:
      default:
         strftime(timestr, TIMESTRSZ-7, "%c", loctim);
         break;
   }

}

void ntp_init(void)
{
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_set_time_sync_notification_cb(time_sync_notification_cb);
    sntp_set_sync_mode(SNTP_SYNC_MODE_IMMED);
    sntp_init();
    setenv("TZ", "EST5EDT,M3.2.0/2,M11.1.0", 1);
    tzset();
}



