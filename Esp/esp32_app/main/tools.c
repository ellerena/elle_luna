/*
 * tools.c
 *
 *  Created on: Jun 12, 2020
 *      Author: Eddie Llerena
 */
#include "stdio.h"
#include "stdint.h"


/**
   @brief   converts a string of chars representing a vector of hex numbers into a vector of bytes equivalent.
   @param   p contains the address of the first char in the string.
   @retval  the count of numbers comverted.
   @verbatim
         Text entered by a user via the keyboard is in the form of a string of printable chars that represent hex numbers.
         This functions takes that string of chars and extracts the meaningful hex numbers represented in it, then
         it places those numbers (bytes) in an array at the same ram location. Returning the number of numbers
         converted.
   @endverbatim
 */
int ParseCharsToValueHex(char *p)
{
   char *q, d;
   int i, k;
   uint8_t c;

   q = p;
   k = 0;

   while (*p > 0x1f)                            /* Will process until a non printable character is found */
   {
      d = 0;                                    /* initialize the 'number equivalent value' */
      i = 0;                                    /* initialize the digit counter (assume 2 digits max per number)*/
      do
      {
         d <<= 4;                            /* each digit uses the first 4 bits, so on each loop we must push the others */
         c = *p++;                              /* read the new char */
         if ((c > 0x29) && (c < 0x3a))             /* char is between 0 and 9 */
         {
            d += (c - 0x30);                    /* add the char equivalen value to our total */
            i++;                             /* increment our digit counter */
         }
         if ((c > 0x60 ) && (c < 0x67))               /* char is between 0xa and 0xf */
         {
            d += (c - 0x60 + 0x9);                 /* add the char equivalen value to our total */
            i++;                             /* increment our digit counter */
         }
      } while (( i == 0 ) || ((*p > 0x2f) && (i < 2)));  /* repeat: if no char found or: if found a space and less than 2 digits */

      if(i)                                  /* if the digit counter is non-zero then a value has been found */
      {
         *q++ = d;                              /* write the number found to memory */
         k++;                                /* increment 'detected numbers counter' */
      }
   }

   return k;
}

/**
 * @brief test function to show buffer
 */
//static void disp_buf(uint8_t *buf, int len)
//{
//   int i;
//   printf("\t");
//   for (i = 0; i < len; i++) {
//      printf("%02x ", buf[i]);
//      if ((i + 1) % 16 == 0) {
//         printf("\n\t");
//      }
//   }
//   printf("\n");
//}

