/* Simple HTTP Server Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "http.h"

#include <sys/param.h>
#include <esp_http_server.h>
#include "esp_system.h"
#include "esp_log.h"
#include "esp_event.h"
#include "driver/ledc.h"
#include <time.h>

#include "html.h"
#include "main.h"
#include "ntp.h"

#define TAG    "http"

extern void ledc_bright(int channel, int val);

esp_err_t home_get_handler(httpd_req_t *req)
{
   char*  buf;
   size_t buf_len;
   int d0 = 0, d1 = 0;
   char* resp_str = (char*) req->user_ctx;

   update_time_str(HTTP_TIME);
   /* get value of 'Host' field from the header */
   buf_len = httpd_req_get_hdr_value_len(req, "Host") + 1;
   if (buf_len > 1) {
      buf = malloc(buf_len);
      if (httpd_req_get_hdr_value_str(req, "Host", buf, buf_len) == ESP_OK) {
         INFOMSG("header => Host: %s", buf);
      }
      free(buf);
   }

   /* get query string and parse params */
   buf_len = httpd_req_get_url_query_len(req) + 1;
   if (buf_len > 1) {
      buf = malloc(buf_len);
      if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK) {
         INFOMSG("URL query => %s", buf);
         char param[32];
         /* Get query strings */
         if (httpd_query_key_value(buf, "d0", param, sizeof(param)) == ESP_OK) {
            INFOMSG("URL param d0=%s", param);
            d0 = param[0] & 1;
         }
         if (httpd_query_key_value(buf, "d1", param, sizeof(param)) == ESP_OK) {
            INFOMSG("URL param d1=%s", param);
            d1 = param[0] - 'a';
         }

         ledc_bright(d0, d1);
      }
      free(buf);
   }

   /* Set some custom headers */
   httpd_resp_set_hdr(req, "C1", "24601");

   /* Send response with custom headers and body */
//   httpd_resp_send(req, resp_str, strlen(resp_str));
   httpd_resp_send_chunk(req, resp_str, strlen(resp_str));
   httpd_resp_send_chunk(req, timestr, strlen(timestr));
   httpd_resp_send_chunk(req, resp_str, 0);

   return ESP_OK;
}

httpd_uri_t hello = {
    .uri       = "/",
    .method    = HTTP_GET,
    .handler   = home_get_handler,
    .user_ctx  = PAGE_HTML_INDEX0  /* pass response string */
};

/* An HTTP POST handler */
esp_err_t echo_post_handler(httpd_req_t *req)
{
    char buf[100];
    int ret, remaining = req->content_len;

    while (remaining > 0) {
        /* Read the data for the request */
        if ((ret = httpd_req_recv(req, buf,
                        MIN(remaining, sizeof(buf)))) <= 0) {
            if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
                /* Retry receiving if timeout occurred */
                continue;
            }
            return ESP_FAIL;
        }

        /* Send back the same data */
        httpd_resp_send_chunk(req, buf, ret);
        remaining -= ret;

        /* Log data received */
        INFOMSG("=========== RECEIVED DATA ==========");
        INFOMSG("%.*s", ret, buf);
        INFOMSG("====================================");
    }

    // End response
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

httpd_uri_t echo = {
    .uri       = "/echo",
    .method    = HTTP_POST,
    .handler   = echo_post_handler,
    .user_ctx  = NULL
};

/* An HTTP PUT handler. This demonstrates realtime
 * registration and deregistration of URI handlers
 */
esp_err_t ctrl_put_handler(httpd_req_t *req)
{
    char buf;
    int ret;

    if ((ret = httpd_req_recv(req, &buf, 1)) <= 0) {
        if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
            httpd_resp_send_408(req);
        }
        return ESP_FAIL;
    }

    if (buf == '0') {
        /* Handler can be unregistered using the uri string */
        INFOMSG("Unregistering /hello and /echo URIs");
        httpd_unregister_uri(req->handle, "/hello");
        httpd_unregister_uri(req->handle, "/echo");
    }
    else {
       INFOMSG("Registering /hello and /echo URIs");
        httpd_register_uri_handler(req->handle, &hello);
        httpd_register_uri_handler(req->handle, &echo);
    }

    /* Respond with empty body */
    httpd_resp_send(req, NULL, 0);
    return ESP_OK;
}

httpd_uri_t ctrl = {
    .uri       = "/ctrl",
    .method    = HTTP_PUT,
    .handler   = ctrl_put_handler,
    .user_ctx  = NULL
};

httpd_handle_t start_webserver(void)
{
   httpd_handle_t server = NULL;
   httpd_config_t config = HTTPD_DEFAULT_CONFIG();

   // Start the httpd server
   INFOMSG("Starting server on port: '%d'", config.server_port);
   if (httpd_start(&server, &config) == ESP_OK) {
      // Set URI handlers
      INFOMSG("Registering URI handlers");
      httpd_register_uri_handler(server, &hello);
      httpd_register_uri_handler(server, &echo);
      httpd_register_uri_handler(server, &ctrl);
      return server;
   }

   INFOMSG("Error starting server!");
   return NULL;
}

void stop_webserver(httpd_handle_t server)
{
    // Stop the httpd server
    httpd_stop(server);
}

static httpd_handle_t server = NULL;

void disconnect_handler(void* arg, esp_event_base_t event_base,
                               int32_t event_id, void* event_data)
{
    httpd_handle_t* server = (httpd_handle_t*) arg;
    if (*server) {
       INFOMSG("Stopping webserver");
        stop_webserver(*server);
        *server = NULL;
    }
}

void connect_handler(void* arg, esp_event_base_t event_base,
                            int32_t event_id, void* event_data)
{
    httpd_handle_t* server = (httpd_handle_t*) arg;
    if (*server == NULL) {
       INFOMSG("Starting webserver");
        *server = start_webserver();
    }
}

void http_start(void)
{
   CHKERR(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &connect_handler, &server));
   CHKERR(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &disconnect_handler, &server));

   server = start_webserver();
}
