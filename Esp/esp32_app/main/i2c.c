/*
 * i2c.c
 *
 *  Created on: Jun 12, 2020
 *      Author: Eddie Llerena
 */

#include "i2c.h"

#include <stdio.h>
#include "freertos/FreeRTOSConfig.h"
#include "esp_log.h"

#include "GFX_Library.h"
#include "SSD1306.h"
#include "tools.h"

#define TAG "i2c"

#define I2C_TX_BUF_DIS    0                   /*!< I2C master doesn't need buffer */
#define I2C_RX_BUF_DIS    0                   /*!< I2C master doesn't need buffer */

int dst_addr = OLED1_I2C_ADR;
uint8_t data_wr[] = {0xae, 0x00, 0x10, 0x81, 0x7f, 0xa1, 0xa6, 0xa8, \
                     0x1f, 0xd3, 0x00, 0xd5, 0xf0, 0xd9, 0x22, 0xda, \
                     0x02, 0xdb, 0x49, 0x8d, 0x14, 0xaf, 0xa4};

esp_err_t i2c1_init(void)
{
   esp_err_t ret;

   int i2c_port = I2C1_PRT_STR;
   i2c_config_t conf;
   conf.mode = I2C_MODE_MASTER;
   conf.sda_io_num = I2C1_SDA;
   conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
   conf.scl_io_num = I2C1_SCL;
   conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
   conf.master.clk_speed = I2C1_FREQ;
   i2c_param_config(i2c_port, &conf);
   ret = i2c_driver_install(i2c_port, conf.mode, I2C_RX_BUF_DIS, I2C_TX_BUF_DIS, 0);
   vTaskDelay(pdMS_TO_TICKS(1000));
   gfx_init();

   return ret;

}

/**
 * @brief test code to read esp-i2c-slave
 *        We need to fill the buffer of esp slave device, then master can read them out.
 *
 * _______________________________________________________________________________________
 * | start | slave_addr + rd_bit +ack | read n-1 bytes + ack | read 1 byte + nack | stop |
 * --------|--------------------------|----------------------|--------------------|------|
 *
 */
esp_err_t i2c_master_rd(i2c_port_t i2c_num, uint8_t *data_rd, size_t size)
{
   i2c_cmd_handle_t cmd;
   if (size == 0) {
      return ESP_OK;
   }
   cmd = i2c_cmd_link_create();
   i2c_master_start(cmd);
   i2c_master_write_byte(cmd, dst_addr | I2C_MASTER_READ, ACK_CHK_EN);
   if (size > 1) {
     i2c_master_read(cmd, data_rd, size - 1, ACK_VAL);
   }
   i2c_master_read_byte(cmd, data_rd + size - 1, NACK_VAL);
   i2c_master_stop(cmd);
   esp_err_t ret = i2c_master_cmd_begin(i2c_num, cmd, WAIT_1S);
   i2c_cmd_link_delete(cmd);
   return ret;
}

/**
 * @brief Test code to write esp-i2c-slave
 *        Master device write data to slave(both esp32),
 *        the data will be stored in slave buffer.
 *        We can read them out from slave buffer.
 *
 * ___________________________________________________________________
 * | start | slave_addr + wr_bit + ack | write n bytes + ack  | stop |
 * --------|---------------------------|----------------------|------|
 *
 */
esp_err_t i2c_master_wr(i2c_port_t i2c_num, uint8_t *data_wr, size_t size)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, dst_addr | I2C_MASTER_WRITE, ACK_CHK_EN);
    i2c_master_write(cmd, data_wr, size, ACK_CHK_EN);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(i2c_num, cmd, WAIT_200MS);
    i2c_cmd_link_delete(cmd);
    return ret;
}

void i2c_addr(char *acComm)
{
   ParseCharsToValueHex((char*)acComm+2);
   dst_addr = acComm[2];
   printf("i2c address set to 0x%x\n", dst_addr);
}

void i2c_scan(void)
{
   int d;
   i2c_cmd_handle_t cmd;
   esp_err_t r;      /* result storage */

   for (int i = I2C_MASTER_READ; i <= 0x7f; i+=2)
   {
      cmd = i2c_cmd_link_create();
      i2c_master_start(cmd);
      i2c_master_write_byte(cmd, i, ACK_CHK_EN);
      i2c_master_read_byte(cmd, (uint8_t*)&d , NACK_VAL);
      i2c_master_stop(cmd);
      r = i2c_master_cmd_begin(I2C1_PRT_STR, cmd, WAIT_1S);
      i2c_cmd_link_delete(cmd);
      printf((r == ESP_OK) ? "\rPing Ok from 0x%02x\n" : "\r%02x", i&(~1));
      fflush(stdout);
   }
}

void i2c1_cmd(char *acComm)
{
   int n;
   i2c_cmd_handle_t cmd;

   acComm += 2;
   n = ParseCharsToValueHex(acComm);

   while (n--) {
      cmd = i2c_cmd_link_create();
      i2c_master_start(cmd);
      i2c_master_write_byte(cmd, dst_addr | I2C_MASTER_WRITE, ACK_CHK_EN);
      i2c_master_write_byte(cmd, OLED_COMMAND, ACK_CHK_EN);
      i2c_master_write_byte(cmd, *acComm++, ACK_CHK_EN);
      i2c_master_stop(cmd);
      i2c_master_cmd_begin(I2C1_PRT_STR, cmd, WAIT_200MS);
      i2c_cmd_link_delete(cmd);
   }
}

void i2c1_wr(char *acComm)
{
   int n;

   acComm += 2;
   n = ParseCharsToValueHex(acComm);
   n = i2c_master_wr(I2C1_PRT_STR, (uint8_t*)acComm, n);
   printf("wrote 0x%02x, return %d\n", *acComm, n);

}

