

#include <zephyr.h>
#include <sys/printk.h>

#include "autoconf.h"

void main(void)
{
  printk ("Paris et Libere!\n" __DATE__ " " __TIME__ "\n%s\n", CONFIG_BOARD);
}
