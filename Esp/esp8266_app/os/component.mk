#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

## Factory build
COMPONENT_ADD_INCLUDEDIRS += inc
COMPONENT_ADD_INCLUDEDIRS += ../misc/inc
COMPONENT_ADD_INCLUDEDIRS += ../user_app/inc

COMPONENT_OBJS := os_i2c.o os_led.o os_gpio.o

COMPONENT_SRCDIRS += ./

