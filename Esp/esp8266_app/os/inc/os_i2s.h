/*
 * os_i2s.h
 *
 *  Created on: Feb 19, 2022
 *      Author: ellerena
 */

#ifndef OS_INC_OS_I2S_H_
#define OS_INC_OS_I2S_H_

#define I2S_MIC_SAMPLE_RATE (48000)
#define I2S_MIC_BYTES_PER_SAMPLE (4)
#define I2S_PIN_MIC_WS GPIO_NUM_4
#define I2S_PIN_MIC_DATA GPIO_NUM_34
#define I2S_PIN_MIC_CLK GPIO_NUM_33
#define I2S_MIC_CHANNEL I2S_NUM_0


/***************************** Translated APIs *****************************/

int os_i2s_master_init (void);


#endif /* OS_INC_OS_I2S_H_ */
