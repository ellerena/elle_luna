/*
 * os_led.h
 *
 *  Created on: Oct 14, 2021
 *      Author: ellerena
 */

#ifndef OS_INC_DRV_LED_H_
#define OS_INC_DRV_LED_H_

void OS_led_init (void);
void OS_led_bright (int channel, int val);
int OS_led_proc_start (void);

/**
 * @brief changes/set the led blinking rate
 * @param speed: [0..16]
 * @return 0: success, error otherwise
 *  */
int OS_led_blink_speed (int speed);

#endif /* OS_INC_DRV_LED_H_ */
