/*
 * os_i2c.h
 *
 *  Created on: Sep 10, 2021
 *      Author: ellerena
 */

#ifndef OS_I2C_H_
#define OS_I2C_H_

/* required includes */
#include <stdint.h>
#include <stdbool.h>

/* main API */
extern int OS_i2c_master_init (void);
extern uint8_t OS_i2c_master_slv_addr (uint8_t addr);
extern bool OS_i2c_master_is_slave_present (uint8_t i2c_addr7);

/**
 * @brief i2c write raw data to default slave address
 *  ===============================================================
 * | ST | dst_addr_w < ack | dat[0] < ack ... dat[n-1] < ack | STP |
 *  ===============================================================
 *
 * @param data, data to send
 * @param len, data length
 *
 * @return number of bytes sent (<len> on sucess)
 */
extern uint32_t OS_i2c_master_wr (const uint8_t * data, uint32_t len);

/**
 * @brief i2c read raw data from default slave address
 *  ===============================================================
 * | ST | dst_addr_r < ack | dat[0] > ack ... dat[n-1] > ack | STP |
 *  ===============================================================
 *
 * @param data, data buffer to receive
 * @param len, data length
 *
 * @return number of bytes received (<len> on sucess)
 */
extern uint32_t OS_i2c_master_rd (uint8_t * data, uint32_t len);

/**
 * @brief i2c write raw data to the specified slave address
 *  ===========================================================
 * | ST | addr_w < ack | dat[0] < ack ... dat[n-1] < ack | STP |
 *  ===========================================================
 *
 * @param data, data to send
 * @param len, data length
 * @param addr, desired address of slave
 *
 * @return number of bytes sent (<len> on sucess)
 */
extern uint32_t OS_i2c_master_wr_a (const uint8_t * data,
                                    uint32_t len,
                                    int addr);

/**
 * @brief i2c read data from specified slave address
 *  ===========================================================
 * | ST | addr_r < ack | dat[0] > ack ... dat[n-1] > ack | STP |
 *  ===========================================================
 *
 * @param data, data buffer to receive
 * @param len, data length
 * @param addr, desired address of slave
 *
 * @return number of bytes received (<len> on sucess)
 */
extern uint32_t OS_i2c_master_rd_a (uint8_t * data, uint32_t len, int addr);

/**
 * @brief i2c write data to the slave address starting
 *          at the specified register address
 *  ===================================================================
 * | ST | addr_w < ack | reg < ack | d[0] < ack ... d[n-1] < ack | STP |
 *  ===================================================================
 *
 * @param data, data to send
 * @param len, data length
 * @param addr, slave address (7 bit)
 * @param reg, pointer to register address
 *
 * @return number of bytes sent (<len> on sucess)
 */
extern uint32_t OS_i2c_master_wr_ar (const uint8_t * data,
                                     uint32_t len,
                                     int addr,
                                     const uint8_t * reg);

/**
 * @brief i2c read data from the slave address starting
 *          at the specified register address
 *  ===================================================================
 * | ST | addr_r < ack | reg < ack | d[0] > ack ... d[n-1] > ack | STP |
 *  ===================================================================
 *
 * @param data, data to send
 * @param len, data length
 * @param addr, slave address (7 bit)
 * @param reg, starting register address
 *
 * @return number of bytes sent (<len> on sucess)
 */
extern uint32_t OS_i2c_master_rd_ar (uint8_t * data,
                                     uint32_t len,
                                     int addr,
                                     const uint8_t * reg);
extern int OS_i2c_master_option_set (uint32_t opt);
extern int OS_i2c_toggle_repeat_start (void);
extern int OS_i2c_repeat_start_get (void);

extern int OS_i2c_clock_set (int clk_speed);

extern int OS_i2c_set_reg_address_len (int len);
extern int OS_i2c_get_reg_address_len (void);

#endif /* OS_I2C_H_ */
