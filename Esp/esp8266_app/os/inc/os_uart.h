/*
 * os_uart.h
 *
 *  Created on: Sep 13, 2021
 *      Author: ellerena
 */

#ifndef OS_INC_OS_UART_H_
#define OS_INC_OS_UART_H_

int OS_uart_init (void);
int OS_uart_init_irq (void);
int OS_putchar (int c);
int OS_puts (const char * str);

/**
 @brief  prints a 32 bit number in hexadecimal format.
 @param  n is the number to print
 #param  width is the number of nibbles to print [1-8]
 */
void OS_put_hex (unsigned int n, int width);

/**
 @brief  this is the external function to print text and numbers (as hex)
 @param  pre, post are pointers to text we want before/after the number
 @param  n, is the number we want to print
 */
void OS_put_txt_hex_txt (const char *pre, unsigned int n, const char *post);

#endif /* OS_INC_OS_UART_H_ */
