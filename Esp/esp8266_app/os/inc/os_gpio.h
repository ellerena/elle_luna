/*
 * os_gpio.h
 *
 *  Created on: Oct 22, 2021
 *      Author: ellerena
 */

#ifndef OS_INC_OS_GPIO_H_
#define OS_INC_OS_GPIO_H_

#ifdef ARCH_ESP32
#define GPIO_LED_ON (1)
#define GPIO_LED_OFF (0)
#else
#define GPIO_LED_ON (0)
#define GPIO_LED_OFF (1)
#endif

int OS_gpio_init (void);
int OS_gpio_state_set (int gpio, int state);
int OS_gpio_togle (int);

#endif /* OS_INC_OS_GPIO_H_ */
