/*
 * os_i2s.c
 *
 *  Created on: Feb 19, 2022
 *      Author: ellerena
 */

#include "esp_log.h"
#include "esp_err.h"
#include "driver/gpio.h"
#include "driver/i2s.h"
#include "os_i2s.h"

int os_i2s_master_init (void)
{
  // Set the I2S configuration: 48kHz PDM, 16bps
  i2s_config_t i2s_config =
    {
      .mode = I2S_MODE_MASTER | I2S_MODE_RX,
      .sample_rate = I2S_MIC_SAMPLE_RATE,
      .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT,
      .channel_format = I2S_CHANNEL_FMT_ONLY_RIGHT,
      .communication_format = I2S_COMM_FORMAT_STAND_I2S,
      .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,
      .dma_buf_count = 8,
      .dma_buf_len = 200,
      .use_apll = 0, };

  // Set the pinout configuration
  i2s_pin_config_t pin_config =
    {
      .mck_io_num = I2S_PIN_NO_CHANGE,
      .bck_io_num = I2S_PIN_MIC_CLK,
      .ws_io_num = I2S_PIN_MIC_WS,
      .data_out_num = I2S_PIN_NO_CHANGE,
      .data_in_num = I2S_PIN_MIC_DATA, };

  // Call driver installation function before any I2S R/W operation.
  ESP_ERROR_CHECK(i2s_driver_install(I2S_MIC_CHANNEL, &i2s_config, 0, NULL));
  ESP_ERROR_CHECK(i2s_set_pin(I2S_MIC_CHANNEL, &pin_config));
  ESP_ERROR_CHECK(i2s_set_clk(I2S_MIC_CHANNEL, I2S_MIC_SAMPLE_RATE,
                              I2S_BITS_PER_SAMPLE_32BIT,
                              I2S_CHANNEL_MONO));

  return 0;
}


