/*
 * os_i2c.c
 *
 *  Created on: Jul 10, 2020
 *      Author: eddie
 */

/***************************** Include Files *******************************/
#include "drv_i2c.h"
#include "os_i2c.h"
#include "mis_logger.h"

/***************************** Translated APIs *****************************/

int OS_i2c_master_init (void)
{
  return DRV_i2c_master_init();
}

uint8_t OS_i2c_master_slv_addr (uint8_t addr)
{
  return DRV_i2c_master_slv_addr(addr);
}

bool OS_i2c_master_is_slave_present (uint8_t i2c_addr7)
{
  return DRV_i2c_master_is_slave_present(i2c_addr7);
}

uint32_t OS_i2c_master_wr (const uint8_t *data   , uint32_t len)
{
  return DRV_i2c_master_wr(data, len);
}

uint32_t OS_i2c_master_rd (uint8_t *data, uint32_t len)
{
  return DRV_i2c_master_rd(data, len);
}

uint32_t OS_i2c_master_wr_a (const uint8_t *data   , uint32_t len, int addr)
{
  return DRV_i2c_master_wr_a(data, len, addr);
}

uint32_t OS_i2c_master_rd_a (uint8_t *data, uint32_t len, int addr)
{
  return DRV_i2c_master_rd_a(data, len, addr);
}

uint32_t OS_i2c_master_wr_ar (const uint8_t * data,
                              uint32_t len,
                              int addr,
                              const uint8_t * reg)
{
  return DRV_i2c_master_wr_ar(data, len, addr, reg);
}

uint32_t OS_i2c_master_rd_ar (uint8_t * data,
                              uint32_t len,
                              int addr,
                              const uint8_t * reg)
{
  return DRV_i2c_master_rd_ar(data, len, addr, reg);
}

int OS_i2c_master_option_set (uint32_t opt)
{
  return DRV_i2c_master_option_set(opt);
}

int OS_i2c_toggle_repeat_start (void)
{
  return DRV_i2c_toggle_repeat_start();
}

int OS_i2c_repeat_start_get (void)
{
  return DRV_i2c_repeat_start_get();
}

int OS_i2c_clock_set (int clk_speed)
{
  return DRV_i2c_clock_set(clk_speed);
}

int OS_i2c_set_reg_address_len (int len)
{
  return DRV_i2c_set_reg_address_len(len);
}

int OS_i2c_get_reg_address_len (void)
{
  return DRV_i2c_get_reg_address_len();
}


/***********************************************************/
