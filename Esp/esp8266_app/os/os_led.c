/* LEDC (LED Controller) fade
 */

#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "driver/ledc.h"
#include "os_gpio.h"

/*TODO ?*/
//#include "hal/ledc_types.h"

#define LED_RESOLUTION (LEDC_TIMER_13_BIT)
#define LED_MAX        ((1 << LEDC_TIMER_13_BIT) - 1)
#define LEDC_FADE_TIME (5000)
#define UNUSED         (0)

static TimerHandle_t htimer0 = NULL;
static void timer0_handler (TimerHandle_t htimer);

static ledc_channel_config_t ledc_channel =
  {
    .channel = LEDC_CHANNEL_0,
    .duty = 30,
    .gpio_num = 2,
#ifndef ARCH_ESP32C3
    .speed_mode = LEDC_HIGH_SPEED_MODE, // use hw controller
#else
    .speed_mode = LEDC_LOW_SPEED_MODE,
#endif
    .hpoint = 0,
    .timer_sel = LEDC_TIMER_0,
    .intr_type = LEDC_INTR_DISABLE
 };

void OS_led_init (void)
{
  ledc_timer_config_t ledc_timer =
    { .duty_resolution = LEDC_TIMER_13_BIT, // 16 levels
      .freq_hz = 1000, // frequency of PWM signal
#ifndef ARCH_ESP32C3
      .speed_mode = LEDC_HIGH_SPEED_MODE, // use hw controller
#else
    .speed_mode = LEDC_LOW_SPEED_MODE,
#endif
      .timer_num = LEDC_TIMER_0 // timer index
    };

  int ret = ledc_timer_config(&ledc_timer);
  if (ESP_OK == ret)
  {
    ret = ledc_channel_config(&ledc_channel);
    if (ESP_OK == ret)
    {
      printf("%d\n", __LINE__);
      ret = ledc_set_duty(ledc_channel.speed_mode,
                          ledc_channel.channel,
                          LED_RESOLUTION / 2);
      if (ESP_OK == ret)
      {
        ret = ledc_update_duty(ledc_channel.speed_mode, ledc_channel.channel);
//        ret = ledc_fade_func_install(0); // hw fading service.
      }
    }
  }

  printf("err:%d", ret);
}

void OS_led_bright (int channel, int val)
{
  val = val % 16; /* 0..15 */
  val = (val << (LED_RESOLUTION - 4));

  printf("\tledc channel:%d, val:%d\n", channel, val);

  ledc_set_duty(UNUSED, ledc_channel.channel, val);
  ledc_update_duty(UNUSED, ledc_channel.channel);
}

int OS_led_blink_speed (int speed)
{
  if (NULL == htimer0)
  {
    return -1;
  }

  /* convert speed to ms (0..2000) */
  speed = (speed % 17) * 125;
  int ret = xTimerChangePeriod(htimer0, pdMS_TO_TICKS(speed), 2000);

  return ret == pdPASS ? 0 : -1;
}

int OS_led_proc_start (void)
{
  /* if timer already exist, delete it */
  if (NULL != htimer0)
  {
    xTimerDelete(htimer0, 0);
    return 0;
  }

  htimer0 = xTimerCreate("tim0",
                         pdMS_TO_TICKS(2000), // 2sec
                         pdTRUE,
                         (void*) 1,
                         timer0_handler);

  if (NULL == htimer0)
  {
    return -1;
  }

  return pdPASS == xTimerStart(htimer0, portMAX_DELAY) ? 0 : -1;
}

static void timer0_handler (TimerHandle_t htimer)
{
  configASSERT(htimer);

  int lTimerId = (int) pvTimerGetTimerID(htimer);
  static int led_state = 0;

  if (1 == lTimerId)
  {
    OS_gpio_state_set(2, led_state);
    led_state ^= 1;
  }
}

