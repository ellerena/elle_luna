/*
 * os_dht11.c
 *
 *  Created on: Sep 10, 2021
 *      Author: ellerena
 */

/***************************** Include Files *******************************/
#include <xil_printf.h>

#include <os_uart.h>
#include <drv_1w.h>
#include <w1_regs.h>

#include "mis_logger.h"

/***************************** Translated APIs *****************************/

int OS_dht11_data_rd (void * buf)
{
  return DRV_w1_get_data(buf);
}

void OS_dht11_show_result (void)
{
  uint8_t buf[4 * 2]; /* temporary buffer to store the dht11 data */
  int result;

  result = OS_dht11_data_rd((void*) buf);

  if (0 == result)
  {

    float farht;
    uint8_t * dat = buf;

    farht = dat[2] + (float) dat[1] / 100;
    farht = ((farht * 9) / 5) + 32;

    uint32_t status = DRV_1w_reg_rd(REG_W1_STA);

    PRNTHT("stat: ", status, "xh" CRLF);
    PRNTHT("read: ", dat[4], "");
    PRNTHT("", *(uint32_t* ) dat, CRLF);
    PRNTHT("chks: ", dat[0], CRLF);
    PRNF("%d.%02dH %d.%dC\n", dat[4], dat[3], dat[2], dat[1]);
  }
    else
  {
    PRNS("DHT11 read failed\n");
  }
}

void OS_dht11_set_treshold (uint32_t tres)
{
  DRV_1w_reg_wr(REG_W1_TRESH, tres);
}
