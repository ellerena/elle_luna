/*
 * os_gpio.c
 *
 *  Created on: Oct 22, 2021
 *      Author: ellerena
 */

#include "drv_gpio.h"

int OS_gpio_init (void)
{
  return DRV_gpio_init();
}

int OS_gpio_state_set (int gpio, int state)
{
  return DRV_gpio_state_set(gpio, state);
}

int OS_gpio_togle (int led)
{
  return DRV_gpio_togle(led);
}


