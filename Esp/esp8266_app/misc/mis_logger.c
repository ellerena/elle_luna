/*
 * mis_logger.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include "mis_logger.h"

void log_dump_bytes (const void * data, int len)
{
  unsigned char * dat = (unsigned char *) data;

  int i = 0;
  while (len--)
  {
    PRNHEX(*dat++, 2);

    if ((15 == (i++ % 16)) || (0 == len))
    {
      PRNS(CRLF);
    }
    else
    {
      PRNS(" ");
    }
  }
}

