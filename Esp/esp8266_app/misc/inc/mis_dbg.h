/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file mis_dbg.h
 * @date 09.12.2021
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#pragma once

#if __cplusplus
extern "C" {
#endif

/***************************************************************************************************
 * Includes
 **************************************************************************************************/
#include <mis_logger.h>

/***************************************************************************************************
 * Macro Definitions
 **************************************************************************************************/
#define TSTRCO(x, y)                if(x) return y         /* On test, return code */
#define TSTRLN(x)                   if (x) return __LINE__ /* On test return line */
#define TSTRER(x)                   if (x) return -1       /* On test return fail */
#define TSTLOGE(test, name)         if(test) PRNS(name " NG" CRLF) /* test and log if error */
#define TSTPRNS(test, name)         if(test) PRNS(name " NG" CRLF); \
                                    else PRNS(name " Ok" CRLF)

#define st(x)                       do { x } while (__LINE__ == -1)
#define debounce(x, y)              for(x = 0; x < 500; x++) if(y) x = 0

//#define DBGLED                      *(u32*)REG_LED_DATA = dbgled++;
//#define DBGLEDSHOW(x)               *(u32*)REG_LED_DATA = x;

/***************************************************************************************************
 * Type Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Constant Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Variable Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Function Prototypes
 **************************************************************************************************/

#if __cplusplus
}
#endif
