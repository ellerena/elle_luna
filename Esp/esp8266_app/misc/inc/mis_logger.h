/*
 * mis_logger.h
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#ifndef USER_APP_INC_MIS_LOGGER_H_
#define USER_APP_INC_MIS_LOGGER_H_

#include "stdio.h"


#define CRLF          "\n"
#define PRNF          printf
#define PRNS(s)       printf(s)    /* print one string line */
#define PRNS2(s)      printf(s) //
#define PRNT(x, y)    if(x) PRNF(y CRLF)
#define PRNHEX(n, w)  printf("%0" #w "x", n)

/* verbose output that can be toggled off/on for release/debug */
#ifdef VERBOSE
#define LOGES(s) PRNS(s)
#else
#define LOGES(s)
#endif

#define PRNTHT(pre, n8w, post)  printf("%s%lx%s", pre, (uint32_t)n8w, post)
#define PRNTHT2(pre, n8w, post)  PRNTHT(pre, n8w, post)

#define PRINTF(format)
#define PRINTFV(format, ...)

extern void log_dump_bytes (const void * data, int len);

#endif /* USER_APP_INC_MIS_LOGGER_H_ */
