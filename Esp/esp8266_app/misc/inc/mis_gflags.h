/*
 * mis_gflags.h
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#ifndef USER_APP_INC_MIS_GFLAGS_H_
#define USER_APP_INC_MIS_GFLAGS_H_

#define F_IRQB             (1 << 2)  /* IRQB pending request */
#define F_PB               (1 << 1)
#define F_UART             (1 << 20)
#define F_COMMERR          (1 << 14) /* request unknown by command processor */
#define F_ENDCON           (1 << 16) /* close socket connection */
#define F_ENDPROG          (1 << 17) /* end socket task */
#define F_HTTP             (1 << 0)  /* HTTP request answered by socket */
#define F_OTA_PEND         (1 << 15) /* OTA request pending */
#define F_BT_ADVCONF       (1 << 12) /* Bluetooth ADV configured */
#define F_BT_SCANRSPCONF   (1 << 13) /* Bluetooth SCAN RSP configured */
#define F_BT_ENA           (1 << 10) /* Bluetooth is enabled */

#define M_BT_CONF          (F_BT_ADVCONF | F_BT_SCANRSPCONF)
#define M_SOCKET           (F_ENDCON | F_ENDPROG | F_HTTP)
#define M_CNTXT            (F_OTA_PEND) /* mask flags to be saved to NVS context */

#define GFL(x)             (gFlags & (x))

#define GF(x)              (gFlags & (x))                /* get flag state */
#define GFS(x)             do {gFlags |= (x);} while(0)  /* set flag */
#define GFC(x)             do {gFlags &= ~(x);} while(0) /* clear flag */
#define GFCLEAR            do {gFlags = 0;} while(0)     /* clear all flags */

#define FLB(v, f)          ((v)&(1 << (f))) /* true if bit#f is 1 in v */
#define FLM(v, m)          ((v)&(m)) /* true if any bit in mask is 1 isn v */

extern volatile unsigned int gFlags;

#endif /* USER_APP_INC_MIS_GFLAGS_H_ */
