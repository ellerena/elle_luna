/*
 * drv_i2c.h
 *
 *  Created on: Sep 10, 2021
 *      Author: ellerena
 */

#ifndef DRV_DRV_I2C_H_
#define DRV_DRV_I2C_H_

/* required includes */
#include <stdint.h>
#include <stdbool.h>

/*
 * The slave address of some peripherals.
 */
#define OLED_MELIFE       (0x3c)
#define OLED_BLUE         (0x3d)

#define OLED1_I2C_ADR      OLED_MELIFE
#define I2C_FREQ           (465100)    /* i2c SCL frequency - desired 400kHz
                                          but due to esp error we use a
                                          higher value 440kHz */
#define I2C_BUFSZ          (128)       /* i2c buffer size */

#ifdef ARCH_ESP32

#ifdef ARCH_ESP32C3_IMU
#define I2C_SCL_PIN (8)
#define I2C_SDA_PIN (2)
#elif defined(ARCH_ESP32C3)
#define I2C_SCL_PIN (7)
#define I2C_SDA_PIN (6)
#else
#define I2C_SCL_PIN (22)
#define I2C_SDA_PIN (21)
#endif

#else
#define I2C_SCL_PIN (5)
#define I2C_SDA_PIN (4)
#endif

/* main API */
extern int DRV_i2c_master_init (void);
extern uint8_t DRV_i2c_master_slv_addr (uint8_t addr);
extern int DRV_i2c_master_slv_addr_get (void);

/**
 * @brief i2c write raw data to default slave address
 *  ===============================================================
 * | ST | dst_addr_w < ack | dat[0] < ack ... dat[n-1] < ack | STP |
 *  ===============================================================
 *
 * @param data, data to send
 * @param len, data length
 *
 * @return number of bytes sent (<len> on sucess)
 */
extern uint32_t DRV_i2c_master_wr (const uint8_t * data, uint32_t len);

/**
 * @brief i2c read raw data from default slave address
 *  ===============================================================
 * | ST | dst_addr_r < ack | dat[0] > ack ... dat[n-1] > ack | STP |
 *  ===============================================================
 *
 * @param data, data buffer to receive
 * @param len, data length
 *
 * @return number of bytes received (<len> on sucess)
 */
extern uint32_t DRV_i2c_master_rd (uint8_t * data, uint32_t len);

/**
 * @brief i2c write raw data to the specified slave address
 *  ===========================================================
 * | ST | addr_w < ack | dat[0] < ack ... dat[n-1] < ack | STP |
 *  ===========================================================
 *
 * @param data, data to send
 * @param len, data length
 * @param addr, desired address of slave
 *
 * @return number of bytes sent (<len> on sucess)
 */
extern uint32_t DRV_i2c_master_wr_a (const uint8_t * data,
                                     uint32_t len,
                                     int addr);

/**
 * @brief i2c read data from specified slave address
 *  ===========================================================
 * | ST | addr_r < ack | dat[0] > ack ... dat[n-1] > ack | STP |
 *  ===========================================================
 *
 * @param data, data buffer to receive
 * @param len, data length
 * @param addr, desired address of slave
 *
 * @return number of bytes received (<len> on sucess)
 */
extern uint32_t DRV_i2c_master_rd_a (uint8_t * data, uint32_t len, int addr);

/**
 * @brief i2c write data to the slave address starting
 *          at the specified register address
 *  ===================================================================
 * | ST | addr_w < ack | reg < ack | d[0] < ack ... d[n-1] < ack | STP |
 *  ===================================================================
 *
 * @param data, data to send
 * @param len, data length
 * @param addr, slave address (7 bit)
 * @param reg, starting register address
 *
 * @return number of bytes sent (<len> on sucess)
 */
extern uint32_t DRV_i2c_master_wr_ar (const uint8_t * data,
                                      uint32_t len,
                                      int addr,
                                      const uint8_t * reg);

/**
 * @brief i2c read data from the slave address starting
 *          at the specified register address
 *  ===================================================================
 * | ST | addr_r < ack | reg < ack | d[0] > ack ... d[n-1] > ack | STP |
 *  ===================================================================
 *
 * @param data, data to send
 * @param len, data length
 * @param addr, slave address (7 bit)
 * @param reg, starting register address
 *
 * @return number of bytes sent (<len> on sucess)
 */
extern uint32_t DRV_i2c_master_rd_ar (uint8_t * data,
                                      uint32_t len,
                                      int addr,
                                      const uint8_t * reg);
extern int DRV_i2c_master_option_set (uint32_t opt);
extern int DRV_i2c_toggle_repeat_start (void);
extern int DRV_i2c_repeat_start_get (void);

extern bool DRV_i2c_master_is_slave_present(uint8_t slave_addr);

extern int DRV_i2c_clock_set (int clk_speed);

extern int DRV_i2c_set_reg_address_len (int len);
extern int DRV_i2c_get_reg_address_len (void);

#endif /* DRV_DRV_I2C_H_ */
