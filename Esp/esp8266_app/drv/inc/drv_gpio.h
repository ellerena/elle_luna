/*
 * drv_gpio.h
 *
 *  Created on: Oct 14, 2021
 *      Author: ellerena
 */

#ifndef DRV_INC_DRV_GPIO_H_
#define DRV_INC_DRV_GPIO_H_

#define POSEDGE_PIN (1 << POSEDGE_PIN_NUM)
#define NEGEDGE_PIN (1 << NEGEDGE_PIN_NUM)

#ifdef ARCH_ESP32C3_IMU
#define POSEDGE_PIN_NUM GPIO_NUM_4
#define NEGEDGE_PIN_NUM GPIO_NUM_2
#elif defined ARCH_ESP32C3
#define POSEDGE_PIN_NUM GPIO_NUM_3
#define NEGEDGE_PIN_NUM GPIO_NUM_8
#elif defined ARCH_ESP32
#define LED_PIN (1 << LED_PIN_NUM)
#define LED_PIN_NUM GPIO_NUM_2
#define POSEDGE_PIN_NUM GPIO_NUM_26
#define NEGEDGE_PIN_NUM GPIO_NUM_23
#define DBG_PIN (1 << DBG_PIN_NUM)
#define DBG_PIN_NUM GPIO_NUM_0
#else
#define LED_PIN (1 << LED_PIN_NUM)
#define LED_PIN_NUM GPIO_NUM_2
#define POSEDGE_PIN_NUM GPIO_NUM_15
#define NEGEDGE_PIN_NUM GPIO_NUM_13
#endif


int DRV_gpio_init (void);
int DRV_gpio_state_set (int gpio, int state);
int DRV_gpio_togle (int led);

#endif /* DRV_INC_DRV_GPIO_H_ */
