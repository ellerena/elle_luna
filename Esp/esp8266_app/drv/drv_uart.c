/*
 * drv_uart.c
 *
 *  Created on: Sep 13, 2021
 *      Author: ellerena
 */

#include "uart.h"

int drv_uart_init (void)
{
  return uart_init();
}

int drv_uart_init_irq (void)
{
  return uart_init_irq();
}

int drv_uart_putchar (int c)
{
  return uart_sendbyte(c);
}

int drv_uart_puts (const char * s)
{
  while (1)
  {
    char c = *s++;
    if (c < 0x9)
    {
      break;
    }
    drv_uart_putchar(c);
  }

  return 0; // TODO: must return # of chars printed
}

