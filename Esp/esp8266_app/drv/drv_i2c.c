/*
 * drv_i2c.c
 *
 *  Created on: Jul 10, 2020
 *      Author: eddie
 */

/***************************** Include Files *******************************/
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "esp_system.h"
#include "driver/i2c.h"
#include "drv_i2c.h"

#define I2C_PORT I2C_NUM_0
#define I2C_ADDR_WR(a) ((a << 1) | I2C_MASTER_WRITE)
#define I2C_ADDR_RD(a) ((a << 1) | I2C_MASTER_READ)
#define I2C_WAIT_TICKS (10 / portTICK_PERIOD_MS)
#define IS_VALID_DEVICE_ADDRESS(x) ((3 < (x)) && (0x78 > (x)))

static uint8_t dst_addr = OLED1_I2C_ADR;
static int op_stop = 1; // 1: send stop bit, 0: repeat start
static int op_reg_addr_len = 1; // number of bytes in the register address
/***************************** Translated APIs *****************************/

int DRV_i2c_master_init (void)
{
  i2c_config_t i2c_cnf =
    {
      .mode = I2C_MODE_MASTER,
      .sda_io_num = I2C_SDA_PIN,
      .sda_pullup_en = 0,
      .scl_io_num = I2C_SCL_PIN,
      .scl_pullup_en = 0,

#ifdef ARCH_ESP32
      .master.clk_speed = I2C_FREQ };

  int result = i2c_param_config(I2C_PORT, &i2c_cnf);

  /* adjust scl duty cycle if needed */
  DRV_i2c_clock_set(i2c_cnf.master.clk_speed);

#else // esp8266 aprox 100kHz
      .clk_stretch_tick = 170 };

  int result = i2c_driver_install(I2C_PORT, i2c_cnf.mode);
#endif

  if (ESP_OK == result)
  {
#ifdef ARCH_ESP32
    result = i2c_driver_install(I2C_PORT, i2c_cnf.mode, 0, 0, 0);
#else // esp8266 aprox 100kHz
    result = i2c_param_config(I2C_PORT, &i2c_cnf);
#endif
  }

  return result;
}

/* sets the slave address for future transfers */
uint8_t DRV_i2c_master_slv_addr (uint8_t addr)
{
  if(IS_VALID_DEVICE_ADDRESS(addr))
  {
    dst_addr = addr;
  }

  return dst_addr;
}

bool DRV_i2c_master_is_slave_present(uint8_t slave_addr)
{
#ifdef ARCH_ESP32
  /* run only if theaddress is valid as per i2c specs */
  if (!IS_VALID_DEVICE_ADDRESS(slave_addr))
  {
    return false;
  }

  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, I2C_ADDR_WR(slave_addr), true);
  i2c_master_stop(cmd);

  int ret = i2c_master_cmd_begin(I2C_PORT, cmd, I2C_WAIT_TICKS);

  i2c_cmd_link_delete(cmd);
#else
  int ret = DRV_i2c_master_wr(&slave_addr, 1);
#endif

  return (ESP_OK == ret);
}

uint32_t DRV_i2c_master_wr (const uint8_t * data, uint32_t len)
{
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, I2C_ADDR_WR(dst_addr), true);
  i2c_master_write(cmd, (uint8_t*) data, len, true);
  i2c_master_stop(cmd);

  int ret = i2c_master_cmd_begin(I2C_PORT, cmd, I2C_WAIT_TICKS);
  i2c_cmd_link_delete(cmd);

  return ret == ESP_OK ? len : 0;
}

uint32_t DRV_i2c_master_rd (uint8_t * data, uint32_t len)
{
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, I2C_ADDR_RD(dst_addr), true);
  i2c_master_read(cmd, data, len, I2C_MASTER_LAST_NACK); // read, ack-ing each byte
  i2c_master_stop(cmd);

  int ret = i2c_master_cmd_begin(I2C_PORT, cmd, I2C_WAIT_TICKS);
  i2c_cmd_link_delete(cmd);

  return ret == ESP_OK ? len : 0;
}

uint32_t DRV_i2c_master_wr_a (const uint8_t * data, uint32_t len, int addr)
{
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, I2C_ADDR_WR(addr), true);
  i2c_master_write(cmd, (uint8_t*) data, len, true);
  if (op_stop)
  {
    i2c_master_stop(cmd);
  }

  int ret = i2c_master_cmd_begin(I2C_PORT, cmd, 10 / portTICK_PERIOD_MS);
  i2c_cmd_link_delete(cmd);

  return ret == ESP_OK ? len : 0;
}

uint32_t DRV_i2c_master_rd_a (uint8_t * data, uint32_t len, int addr)
{
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, I2C_ADDR_RD(addr), true);
  i2c_master_read(cmd, data, len, I2C_MASTER_ACK);
  i2c_master_stop(cmd);
  int ret = i2c_master_cmd_begin(I2C_PORT, cmd, I2C_WAIT_TICKS);
  i2c_cmd_link_delete(cmd);

  return ret == ESP_OK ? len : 0;
}

uint32_t DRV_i2c_master_wr_ar (const uint8_t * data,
                               uint32_t len,
                               int addr,
                               const uint8_t * reg)
{
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, I2C_ADDR_WR(addr), true);
  i2c_master_write(cmd, reg, op_reg_addr_len, true);
  i2c_master_write(cmd, (uint8_t*) data, len, true);
  if (op_stop)
  {
    i2c_master_stop(cmd);
  }

  int ret = i2c_master_cmd_begin(I2C_PORT, cmd, I2C_WAIT_TICKS);
  i2c_cmd_link_delete(cmd);

  return ret == ESP_OK ? len : 0;
}

uint32_t DRV_i2c_master_rd_ar (uint8_t * data,
                               uint32_t len,
                               int addr,
                               const uint8_t * reg)
{
#ifdef ARCH_ESP32
  int ret = i2c_master_write_read_device(I2C_PORT,
                                         (addr),
                                         reg,
                                         op_reg_addr_len,
                                         data,
                                         len,
                                         I2C_WAIT_TICKS);
#else // ESP8266
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, I2C_ADDR_WR(addr), true);
  i2c_master_write(cmd, reg, op_reg_addr_len, true);
  if (op_stop)
  {
    i2c_master_stop(cmd);
  }

  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, I2C_ADDR_RD(addr), true);
  i2c_master_read(cmd, data, len, I2C_MASTER_LAST_NACK);
  i2c_master_stop(cmd);
  int ret = i2c_master_cmd_begin(I2C_PORT, cmd, I2C_WAIT_TICKS);
  i2c_cmd_link_delete(cmd);
#endif

  return ret == ESP_OK ? len : 0;
}

int DRV_i2c_master_option_set (uint32_t opt)
{
  return 0;
}

int DRV_i2c_toggle_repeat_start (void)
{
  op_stop ^= 1;
  return op_stop;
}

int DRV_i2c_repeat_start_get (void)
{
  return op_stop ^= 1;
}

int DRV_i2c_clock_set (int clk_speed)
{
  int result = ESP_OK;

#ifdef ARCH_ESP32
  int clk_ticks = (APB_CLK_FREQ / clk_speed);

  int clk_ticks_high =
      (clk_speed >= 300000) ? (clk_ticks * 13 / 50) : (clk_ticks >> 1);

  int clk_ticks_low = clk_ticks - clk_ticks_high;

  i2c_set_period(I2C_PORT, clk_ticks_high, clk_ticks_low);
  i2c_get_period(I2C_PORT, &clk_ticks_high, &clk_ticks_low);

  clk_ticks = clk_ticks_high + clk_ticks_low;
  result = APB_CLK_FREQ / clk_ticks;
#endif

  return result;
}

int DRV_i2c_set_reg_address_len (int len)
{
  assert(1 == len || 2 == len || 4 == len);

  op_reg_addr_len = len;
  return 0;
}

int DRV_i2c_get_reg_address_len (void)
{
  return op_reg_addr_len;
}

/***********************************************************/
