/*
 * drv_gpio.c
 *
 *  Created on: Oct 14, 2021
 *      Author: ellerena
 */

#include "driver/gpio.h"
#include "drv_gpio.h"

int DRV_gpio_init (void)
{
  gpio_config_t gpio_cnf;
  int result;

#ifdef LED_PIN
  /* GPIO2 - blue led configuration */
  gpio_cnf.pin_bit_mask = LED_PIN;
  gpio_cnf.mode = GPIO_MODE_OUTPUT;
  gpio_cnf.intr_type = GPIO_INTR_DISABLE;
  gpio_cnf.pull_up_en = GPIO_PULLUP_DISABLE;
  gpio_cnf.pull_down_en = GPIO_PULLDOWN_DISABLE;

  result = gpio_config(&gpio_cnf);
  if (ESP_OK != result)
  {
    return (int) result;
  }
#endif

#ifdef POSEDGE_PIN
  /* (shared) positive edge triggered irq pin configuration */
  gpio_cnf.pin_bit_mask = POSEDGE_PIN;
  gpio_cnf.mode = GPIO_MODE_INPUT;
  gpio_cnf.intr_type = GPIO_INTR_POSEDGE;
  gpio_cnf.pull_up_en = GPIO_PULLUP_DISABLE;
  gpio_cnf.pull_down_en = GPIO_PULLDOWN_DISABLE;

  result = gpio_config(&gpio_cnf);
  if (ESP_OK != result)
  {
    return (int) result;
  }
#endif

#ifdef NEGEDGE_PIN
  /* (shared) positive edge triggered irq pin configuration */
  gpio_cnf.pin_bit_mask = NEGEDGE_PIN;
  gpio_cnf.mode = GPIO_MODE_INPUT;
  gpio_cnf.intr_type = GPIO_INTR_NEGEDGE;
  gpio_cnf.pull_up_en = GPIO_PULLUP_DISABLE;
  gpio_cnf.pull_down_en = GPIO_PULLDOWN_DISABLE;

  result = gpio_config(&gpio_cnf);
  if (ESP_OK != result)
  {
    return (int) result;
  }
#endif

#ifdef DBG_PIN
  /* (shared) general purpose debug pin for in/out, no IRQ */
  gpio_cnf.pin_bit_mask = DBG_PIN;
  gpio_cnf.mode = GPIO_MODE_OUTPUT;
  gpio_cnf.intr_type = GPIO_INTR_DISABLE;
  gpio_cnf.pull_up_en = GPIO_PULLUP_DISABLE;
  gpio_cnf.pull_down_en = GPIO_PULLDOWN_DISABLE;

  result = gpio_config(&gpio_cnf);
  if (ESP_OK != result)
  {
    return (int) result;
  }
#endif

  return (int) result;
}

int DRV_gpio_state_set (int gpio, int state)
{
  /* for led state: note that physical connection is
   * active low */
  return (int) gpio_set_level(gpio, 1 & state);
}

// note: this function works only for input pins
int DRV_gpio_togle (int gpio)
{
  return (int) gpio_set_level(gpio, 1 ^ gpio_get_level(gpio));
}
