#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

## Factory build
COMPONENT_ADD_INCLUDEDIRS += inc ../user_app/inc

COMPONENT_OBJS := drv_gpio.o drv_i2c.o

COMPONENT_SRCDIRS += ./


