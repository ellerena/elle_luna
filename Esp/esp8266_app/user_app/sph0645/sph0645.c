/*
 * sph0645.c
 *
 *  Created on: Nov 21, 2021
 *      Author: ellerena
 */

#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_task_wdt.h"
#include "driver/gpio.h"
#include "drv_gpio.h" // for debugging only TODO
#include "os_i2s.h"
#include "os_gpio.h" // for debugging only TODO
#include "driver/i2s.h"
#include "mis_logger.h"

/* some macros used to convert the data read into
 * meaningful human readable values
 * val: 2's complement value to be converted to int
 * sign: position of the sign bit */
#define TWOS_COMPLEMENT_2INT(val, sign) (((int)(val) ^ (1 << sign)) - (1 << sign))
#define INTE(x) TWOS_COMPLEMENT_2INT(DATA_VALID_BITS(x), 17)

#define AUDIO_LEN_SEC (5)
#define NUM_SAMPLES (I2S_MIC_SAMPLE_RATE * AUDIO_LEN_SEC)
#define NUM_BYTES_MONO (NUM_SAMPLES * I2S_MIC_BYTES_PER_SAMPLE)
#define RXBUFSZ (1024)
#define RXBUFSZ_BYTES (RXBUFSZ * I2S_MIC_BYTES_PER_SAMPLE)
#define DATA_VALID_BITS(x) (x >> 14)

static uint32_t rxbuf[RXBUFSZ_BYTES];

struct
{
  int16_t temp;
  uint32_t timestamp;
} raw;

static SemaphoreHandle_t xSemaphore = NULL;
static TaskHandle_t xTask = NULL;

static void log_dump_data (const void * data, int len)
{
  uint32_t *dat = (uint32_t*) data;

  int i = 0;
  while (len > 0)
  {
    printf("%d", INTE(*dat++));
//    PRNHEX(*dat++, 8);

    if ((15 == (i++ % 16)) || (0 == len))
    {
      PRNS(CRLF);
    }
    else
    {
      PRNS(",");
    }

    len -= I2S_MIC_BYTES_PER_SAMPLE;
  }

}

/*
 * @brief  platform specific delay (platform dependent)
 *
 * @param  ms        delay in ms
 */
static void platform_delay (uint32_t ms)
{
  vTaskDelay(ms / portTICK_PERIOD_MS);
}

/*
 * @brief  platform specific initialization (platform dependent)
 */
static void platform_init (void)
{
  OS_gpio_state_set(DBG_PIN_NUM, 0);
  os_i2s_master_init();
}

static void proc_setup (void)
{
  /* Init test platform */
  platform_init();

  platform_delay(5); // delay to allow reset complete
}

static void proc_check (void)
{
  size_t bytes_read;
  int bytes_left = NUM_BYTES_MONO;

  esp_task_wdt_reset();
  OS_gpio_state_set(DBG_PIN_NUM, 1);

  while (bytes_left > 0)
  {
    i2s_read(I2S_MIC_CHANNEL,
             (char*) rxbuf,
             bytes_left > RXBUFSZ_BYTES ? RXBUFSZ_BYTES : bytes_left,
             &bytes_read,
             128);
    log_dump_data(rxbuf, bytes_read);
    fflush(stdout);
    bytes_left -= bytes_read;
    esp_task_wdt_reset();
  }
  OS_gpio_state_set(DBG_PIN_NUM, 0);
}

/* -------- task process -------- */
static void sph0645_isr_handler (void * arg)
{
  int xHigherPriorityTaskWoken = pdFALSE;

  xSemaphoreGiveFromISR(xSemaphore, &xHigherPriorityTaskWoken);
}

static void sph0645_task_handler (void * arg)
{
#if 0 // disable for now until application is defined
  for (;;)
  {
    if (xSemaphoreTake(xSemaphore, portMAX_DELAY) == pdTRUE)
    {
      proc_check();
    }
  }
#else
  //for (;;)
  {
    proc_check();
  }
  vTaskDelete(xTask);

#endif
}

/**
 * @brief external API to enable/disable the process
 *        every time it is called, it toggles the process on/off
 * */
int sph0645_proc_start (void)
{
  /* if task exist, then end the whole process */
  if (xTask)
  {
    ESP_ERROR_CHECK(i2s_driver_uninstall(I2S_MIC_CHANNEL));
    vTaskDelete(xTask);
    xTask = NULL;

    PRNS("sph0645 ended\n");
    return 0;
  }

  proc_setup();

  if (NULL == xSemaphore)
  {
    xSemaphore = xSemaphoreCreateBinary();
  }

  int result = ESP_FAIL;
  if (NULL != xSemaphore)
  {
    result = xTaskCreate(sph0645_task_handler, "sph0645_task_handler",
                         2048,
                         NULL,
                         tskIDLE_PRIORITY,
                         &xTask);
    result = (result == pdPASS ? 0 : 1);

  }

  PRNTHT("sph0645::", result, "\n");

  return result;
}
