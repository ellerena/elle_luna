/*
 * sph0645.h
 *
 *  Created on: Oct 30, 2021
 *      Author: ellerena
 */

#error "this code should be excluded"

#ifndef USER_APP_SPH0645_SPH0645_H_
#define USER_APP_SPH0645_SPH0645_H_

#ifdef __cplusplus
extern "C"
{
#endif

  extern int sph0645_proc_start (void);

#ifdef __cplusplus
}
#endif

#endif /* USER_APP_SPH0645_SPH0645_H_ */
