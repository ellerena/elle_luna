/*
 * ws2812.c
 *
 *  Created on: Dec 26, 2021
 *      Author: ellerena
 */

#include "freertos/FreeRTOS.h" // TODO: forcely required by esp8266
#include "driver/uart.h"
#ifdef ARCH_ESP32
#include "soc/uart_reg.h"
#else // esp8266
#include "esp8266/uart_register.h"
#endif

#define WS2812_UART_PORT (UART_NUM_1)
#define WS2812_UART_BR  (3000000)
#define WS2812_UART_TX_BUF (512) // 8x8 display = 64 pix = 192 bytes = 512 uart bytes
#define WS2812_UART_PIN_TX (16)
#define WS2812_UART_PIN_RX (UART_PIN_NO_CHANGE)
#define WS2812_PIXELS (64) // 64
#define WS2812_BYTES_PER_PIXEL (3)
#define WS2812_BITS_PER_BYTE (3)
#define WS2812_BYTES (WS2812_PIXELS * WS2812_BYTES_PER_PIXEL) // 192
#define WS2812_BYTES_BUFFER (WS2812_BYTES * WS2812_BITS_PER_BYTE) // 512
#define BYTE_VAL_CLEAN (0xDB)
#define KEY2 (1 << 23)
#define KEY1 (1 << 22)
#define KEY0 (1 << 21)
#define KEY2_VAL (1 << 7)
#define KEY1_VAL (1 << 4)
#define KEY0_VAL (1 << 1)

uint32_t flags = 0;
char tx_buf[WS2812_BYTES_BUFFER] =
  { 0 };

int ws2812_uart_init (void)
{
  /* Configure parameters of an UART driver, communication pins and
   * install the driver. Note that this feature only requires TX pin */
  uart_config_t uart_config =
    {
      .baud_rate = WS2812_UART_BR,
      .data_bits = UART_DATA_8_BITS,
      .parity = UART_PARITY_DISABLE,
      .stop_bits = UART_STOP_BITS_1,
      .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
#ifdef ARCH_ESP32
      .source_clk = UART_SCLK_APB
#endif
      };
  int ret = uart_driver_install(WS2812_UART_PORT,
                                (UART_FIFO_LEN + 1),
                                WS2812_UART_TX_BUF,
                                0,
                                NULL,
                                0);

  if (0 == ret)
  {
    ret = uart_param_config(WS2812_UART_PORT, &uart_config);
#ifdef ARCH_ESP32
    if (0 == ret)
    {
      ret = uart_set_pin(WS2812_UART_PORT,
                         WS2812_UART_PIN_TX,
                         WS2812_UART_PIN_RX,
                         UART_PIN_NO_CHANGE,
                         UART_PIN_NO_CHANGE);
    }

    uint32_t reg_val = *(uint32_t*) UART_CONF0_REG(WS2812_UART_PORT);
    *(uint32_t*) UART_CONF0_REG(WS2812_UART_PORT) = reg_val | UART_TXD_INV; // Inverse TX

#else
    uint32_t reg_val = *(uint32_t*) UART_CONF0(WS2812_UART_PORT);
    *(uint32_t*) UART_CONF0(WS2812_UART_PORT) = reg_val | UART_TXD_INV; // Inverse TX
#endif
  }

  return ret;
}

/**
 * @brief converts pixel data received as 24 bit RGB values to
 *        a byte pattern which, transmitted using the uart,
 *        corresponds to the WS2812B protocol (inverted polarity).
 *        The result is stored in tx_buf from where it can be
 *        transmitted directly via uart.
 *
 * @param pixel_data, pointer to source for 24bit RGB data in ram
 * @param pixel_num, number of pixels (24 bit RGB) to process.
 *
 * */
void ws2812_stuffer (const uint8_t * pixel_data, int pixel_num)
{

  char *ptxbuf = tx_buf;

  while (pixel_num--)
  {
    /* grab 3 bytes and process them as a single 24 bit pixel */
    int pixGRB = (pixel_data[0] << 16) + (pixel_data[1] << 8)
        + (pixel_data[2]);
    pixel_data += WS2812_BYTES_PER_PIXEL;

    /* we process input data in groups of 3 bits, so for 24 RGB
     * bits we'll run the process 24/3 = 8 times */
    for (int i = 0; i < 8; ++i)
    {
      char byte_val = BYTE_VAL_CLEAN;

      if (KEY2 & pixGRB) // process bit # 23
      {
        byte_val &= ~0x01;
      }
      if (KEY1 & pixGRB) // process bit # 22
      {
        byte_val &= ~0x08;
      }
      if (KEY0 & pixGRB) // process bit # 21
      {
        byte_val &= ~0x40;
      }

      *ptxbuf++ = byte_val;
      pixGRB <<= WS2812_BITS_PER_BYTE;
    }
  }
}

/**
 * sample application use
 * */
int ws2812_proc (void * data, int len)
{
  if (0 == flags)
  {
    ws2812_uart_init();
    flags |= 1;
  }

  ws2812_stuffer((uint8_t*) data, len / 3);

  uart_write_bytes(WS2812_UART_PORT, tx_buf, 8 * len / 3);

  return 0;
}

