// (c) Meta Platforms, Inc. and affiliates. Confidential and proprietary.
/**
 * @file sx9200.h
 *
 * @brief Driver for the Semtech SX9200 Smart Sensor
 *
 */
#pragma once

#include <stdint.h>

#define SX9200_DATA_CAPTURE_LEN32 (13)
#define SX9200_RESET_DELAY_MS     (10)
#define SX9200_PROX_INT_LEVEL_ON  0
#define SX9200_PROX_INT_LEVEL_OFF 1
#define REG_ADDR_SZ               sizeof(uint16_t)
#define REG_VAL_SZ                sizeof(uint32_t)

#define SX9200_I2C_ADDRESS (0x28)    // default i2c slave address (7bit)

typedef enum {
    SX9200_PHASE_0,
    SX9200_PHASE_1,
    SX9200_PHASE_2,
    SX9200_PHASE_3,
    SX9200_PHASE_4,
    SX9200_PHASE_5,
    SX9200_PHASE_6,
    SX9200_PHASE_7,
    SX9200_PHASE_COUNT,
} SX9200_PHASE;

/*
 * @brief this function initializes the sensor driver
 *         parameters.
 * @paaram  pointer to an opaque (for now) parameter
 * @return  0 on success, error code otherwise
 */
int sx9200_driver_init(void* param);

/*
 * @brief this function performs a SW reser of the sensor.
 * @return  0 on success, error code otherwise
 */
int sx9200_sensor_reset(void);

/*
 * @brief this function performs the initial low level
 *        configuration of the sensor by batch writing
 *        all required registers.
 * @return  0 on success, error code otherwise
 * */
int sx9200_sensor_config(void);

/*
 * @brief   Retrieves fresh data from the sensor.
 * @param   raw_data, pointer to ram space to receive the data
 *              it is assumed that caller allocates the
 *              proper ram space for the entire capture
 * @return  0: no change in data, same as in previous capture,
 *          1: new data received
 */
int sx9200_get_new_data(uint32_t* new_data);

/*
 *  @brief  reads a number of registers from the sensor.
 *  @param  reg_addr: the 16bit register address
 *  @param  reg_val: a pointer to a ram location to store the
 *                  received data. It is assumed that caller
 *                  allocates enough valid space for the entire
 *                  reading.
 *  @param  num_regs: the number of consecutive registers to read.
 *                      note that each register is 32 bit wide.
 *  @return 0: success, error code otherwise.
 */
int sx9200_sensor_reg_read(uint16_t reg_addr, uint32_t* reg_val, int num_regs);

/*
 *  @brief  write a 32 bit value to one registers in the sensor.
 *  @param  reg_addr: the 16bit register address
 *  @param  reg_val: a 32bit value to be written
 *  @return 0: success, error code otherwise.
 */
int sx9200_sensor_reg_write(uint16_t reg_addr, uint32_t reg_val);
