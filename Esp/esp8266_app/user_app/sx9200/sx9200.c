/*
 * sx9200.c
 *
 *  Created on: Dec 13, 2021
 *      Author: ellerena
 */
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include "string.h"
#include "driver/i2c.h"
#include "drv_gpio.h"
#include "os_i2c.h"
#include "sock.h"
#include "mis_logger.h"
#include "dataout.h"
#include "fwregmap_utils.h"
#include "sx9200_registers.h"
#include "sx9200.h"

/*****/
#define be32toh(x) REV32(x)
#define LOG_INFO(...)
/*****/

#define I2C_ADDR7 (0x28)
#define REV32(x) ( \
                ( (x & 0x000000ff) << 24 ) + \
                ( (x & 0x0000ff00) <<  8 ) + \
                ( (x & 0x00ff0000) >>  8 ) + \
                ( (x & 0xff000000) >> 24 ))

static SemaphoreHandle_t xSemaphore = NULL;
static TaskHandle_t xTask = NULL;

typedef struct
{
  uint16_t address;
  uint32_t data;
} reg_def_t;

/* register configuration sequence, most setting are same as Semtech's
 *   evaluation board application, plus some customizations for our
 *   intended use.
 */
static const reg_def_t cnf_data[] =
  {
    { SX9200_REG_IRQ_MASK_A /* 0x4004 */, 0x8 }, // IRQ on each conversion
      { SX9200_REG_IRQ_MASK_B /* 0x8010 */, 0x8 }, // Sld/Wheel Engine 1 interrupt allowed
      { SX9200_REG_IRQ_SETUP /* 0x4008 */, 0x8 }, // IRQ active low on CS3, no Pause
      { SX9200_REG_PIN_SETUP_A /* 0x42C0 */, 0x800 }, // CS3 output when not sensing, open drain
      { SX9200_REG_PIN_SETUP_B /* 0x42C4 */, 0x0 }, // CSx output low when used as output
      { SX9200_REG_SCAN_PERIOD_SETUP /* 0x8024 */, 0x80B }, // 0x80B desired 50Hz
      { SX9200_REG_GENERAL_SETUP /* 0x8028 */, 0x3E3E }, //
      { SX9200_REG_AFE_PARAMETER_SETUP /* 0x802C */, 0x1095F }, //
      { SX9200_REG_AFE_CONNECTION_SETUP /* 0x8034 */, 0xDB69B6 }, //
      { SX9200_REG_AFE_PARAMETER_SETUP_X1 /* 0x8038 */, 0x1095F }, //
      { SX9200_REG_AFE_PARAMETER_SETUP_X2 /* 0x8044 */, 0x1095F }, //
      { SX9200_REG_AFE_PARAMETER_SETUP_X3 /* 0x8050 */, 0x1095F }, //
      { SX9200_REG_AFE_PARAMETER_SETUP_X4 /* 0x805C */, 0x1095F }, //
      { SX9200_REG_AFE_PARAMETER_SETUP_X5 /* 0x8068 */, 0x1095F }, //
      { SX9200_REG_AFE_PARAMETER_SETUP_X6 /* 0x8074 */, 0x10F5F }, //
      { SX9200_REG_AFE_PARAMETER_SETUP_X7 /* 0x8080 */, 0x10F5F }, //
      { SX9200_REG_OFFSET_READBACK /* 0x8030 */, 0xDA0368 }, //
      { SX9200_REG_AFE_CONNECTION_SETUP_X1 /* 0x8040 */, 0xD769B6 }, //
      { SX9200_REG_AFE_CONNECTION_SETUP_X2 /* 0x804C */, 0xBB69B6 }, //
      { SX9200_REG_AFE_CONNECTION_SETUP_X3 /* 0x8058 */, 0xDB59B6 }, //
      { SX9200_REG_AFE_CONNECTION_SETUP_X4 /* 0x8064 */, 0xDB6976 }, //
      { SX9200_REG_AFE_CONNECTION_SETUP_X5 /* 0x8070 */, 0xDB69AE }, //
      { SX9200_REG_AFE_CONNECTION_SETUP_X6 /* 0x807C */, 0xDB69B6 }, //
      { SX9200_REG_FILTER_SETUP_A /* 0x808C */, 0x100000 }, //
      { SX9200_REG_FILTER_SETUP_A_X1 /* 0x80AC */, 0x5100000 }, //
      { SX9200_REG_FILTER_SETUP_A_X2 /* 0x80CC */, 0x5100000 }, //
      { SX9200_REG_FILTER_SETUP_A_X3 /* 0x80EC */, 0x5100000 }, //
      { SX9200_REG_FILTER_SETUP_A_X4 /* 0x810C */, 0x5100000 }, //
      { SX9200_REG_FILTER_SETUP_A_X5 /* 0x812C */, 0x5100000 }, //
      { SX9200_REG_FILTER_SETUP_A_X6 /* 0x814C */, 0x100000 }, //
      { SX9200_REG_FILTER_SETUP_A_X7 /* 0x816C */, 0x100000 }, //
      { SX9200_REG_FILTER_SETUP_B_X1 /* 0x80B0 */, 0x20601800 }, //
      { SX9200_REG_FILTER_SETUP_B_X2 /* 0x80D0 */, 0x20601800 }, //
      { SX9200_REG_FILTER_SETUP_B_X3 /* 0x80F0 */, 0x20601800 }, //
      { SX9200_REG_FILTER_SETUP_B_X4 /* 0x8110 */, 0x20601800 }, //
      { SX9200_REG_FILTER_SETUP_B_X5 /* 0x8130 */, 0x20601800 }, //
      { SX9200_REG_FILTER_SETUP_B_X6 /* 0x8150 */, 0x20601800 }, //
      { SX9200_REG_FILTER_SETUP_B_X7 /* 0x8170 */, 0x20601800 }, //
      { SX9200_REG_THRESHOLD_SETUP_X1 /* 0x80BC */, 0x78005A }, //
      { SX9200_REG_THRESHOLD_SETUP_X2 /* 0x80DC */, 0x78005A }, //
      { SX9200_REG_THRESHOLD_SETUP_X3 /* 0x80FC */, 0x78005A }, //
      { SX9200_REG_THRESHOLD_SETUP_X4 /* 0x811C */, 0x78005A }, //
      { SX9200_REG_THRESHOLD_SETUP_X5 /* 0x813C */, 0x78005A }, //
      { SX9200_REG_SLIDER_WHEEL_SETUP_A /* 0x818C */, 0x20000108 }, //
      { SX9200_REG_SLIDER_WHEEL_SETUP_A_X1 /* 0x8190 */, 0x1A0 }, //
      { SX9200_REG_SMART_BUTTON_SETUP /* 0x81FC */, 0x414 }, //
      { SX9200_REG_SMART_BUTTON_SETUP_X1 /* 0x8200 */, 0x1 }, //
      { SX9200_REG_AFE_PARAMETER_SETUP /* 0x802C */, 0x1095F }, //
      { SX9200_REG_SPECIALIZED_READBACK_A /* 0x8290 */, 0x2A90000 }, //
      { SX9200_REG_SPECIALIZED_READBACK_B /* 0x829C */, 0x1EC25400 }, //
      { SX9200_REG_COMMAND /* 0x4280 */, 0xF }, //
      { SX9200_REG_COMMAND /* 0x4280 */, 0xE }, //
  };

static const uint16_t read_set[] =
  {
  SX9200_REG_IRQ_SOURCE,
    SX9200_REG_DEVICE_STATUS_A,
    SX9200_REG_DEVICE_STATUS_B,
    SX9200_REG_DEVICE_STATUS_C,
    SX9200_REG_DEVICE_STATUS_D,
    SX9200_REG_DIFF_READBACK,
    SX9200_REG_DIFF_READBACK_X1,
    SX9200_REG_DIFF_READBACK_X2,
    SX9200_REG_DIFF_READBACK_X3,
    SX9200_REG_DIFF_READBACK_X4,
    SX9200_REG_DIFF_READBACK_X5,
    SX9200_REG_DIFF_READBACK_X6,
    SX9200_REG_DIFF_READBACK_X7, };

static fwregmap_handle_t regmap =
  { .context = NULL, .reg_reader_fn = NULL, .reg_writer_fn = NULL };

/*
 * @brief i2c single register write function
 *
 */
static int sx9200_i2c_reg_wr (void * context,
                            int reg_addr,
                              int num_bytes,
                            uint8_t * data)
{
  /* sensor uses big-endian, so we must perform byte re-ordering first */
  uint8_t be_data[] =
    {
      (reg_addr >> 8) & 0xff,
      reg_addr & 0xff,
      data[3],
      data[2],
      data[1],
      data[0] };

  uint32_t ret = OS_i2c_master_wr_a(be_data, sizeof(be_data), I2C_ADDR7);

  return ret == sizeof(be_data) ? 0 : -1;
}

/*
 * @brief i2c single register read function
 *
 */
static int sx9200_i2c_rd (void * context,
                            int reg_addr,
                            int num_bytes,
                            uint8_t * data)
{
  /* sensor uses big-endian, so we must perform byte re-ordering first */
  uint8_t be_data[] =
    { reg_addr >> 8, reg_addr & 0xff };

  return i2c_master_write_read_device(I2C_NUM_0,
                                      (I2C_ADDR7),
                                      be_data,
                                      2,
                                      data,
                                      num_bytes,
                                      0);
}

/*****************************************************************************/

int sx9200_sensor_reset (void)
{
  uint32_t regval = 0;
  sx9200_read_DEVICE_INFO(&regmap, &regval);
  regval = be32toh(regval);

  LOG_INFO("SxSensorId: %lx", regval);
  if (regval != SX9200_ID)
    return -1;

  /* perform a SW reset of the sensor */
  return sx9200_write_DEVICE_RESET(&regmap, SX9200_DEVICE_RESET_VAL);
}

int sx9200_sensor_config (void)
{
  /* first we must read the IRQ source  register, this
   is just so that the sensor releases the NIRQ pin */
  uint32_t regval;
  sx9200_read_IRQ_SOURCE(&regmap, (sx9200_IRQ_SOURCE_t*) &regval);
  regval = be32toh(regval);

  /* configure the device via i2c, batch write registers */
  const reg_def_t *pval = cnf_data;
  int len = sizeof(cnf_data) / sizeof(reg_def_t);

  while (len--)
  {
    if (sx9200_i2c_reg_wr(NULL, pval->address, 4, (uint8_t*) &pval->data))
    {
      return -1;
    }
    pval++;
  }

  return 0;
}

/*****************************************************************************/


/*
 * @brief this function performs the initial low level
 *        configuration of the sensor.
 * */
void sx9200_proc_setup (void)
{
  printf("sx9200 proc\n");
  regmap.reg_reader_fn = sx9200_i2c_rd;
  regmap.reg_writer_fn = sx9200_i2c_reg_wr;

  /* Init test platform */
//  platform_init();
//  platform_puts("Init complete" CRLF);
//  platform_delay(BOOT_TIME); // wait sensor boot time
//  platform_puts("Boot complete" CRLF);
  /* Check device ID */
  uint32_t regval;
  sx9200_read_DEVICE_INFO(&regmap, &regval);
  regval = REV32(regval);

  PRNTHT("I'm ", regval, CRLF);
  if (regval != SX9200_ID)
    return;

  /* perform a SW reset of the sensor */
  sx9200_write_DEVICE_RESET(&regmap, SX9200_DEVICE_RESET_VAL);

  vTaskDelay(1); // wait 1 rtos systick

  /* first we must read the IRQ source  register, this
   is just so that the sensor releases the NIRQ pin */
  sx9200_read_IRQ_SOURCE(&regmap, (sx9200_IRQ_SOURCE_t*) &regval);
  regval = REV32(regval);

  /* configure the device via i2c, batch write registers */
  const reg_def_t *pval = cnf_data;
  int len = sizeof(cnf_data) / sizeof(reg_def_t);

  while (len--)
  {
    if (sx9200_i2c_reg_wr(NULL, pval->address, 4, (uint8_t*) &pval->data))
    {
      return;
    }
    pval++;
  }
}

static void proc_check (void)
{
#define MG(x) (REV32(x))

  uint32_t reg_val;
  uint32_t raw_val[sizeof(read_set) / REG_ADDR_SZ];
  uint32_t *raw_data = raw_val;
  uint16_t *reg_addr = (uint16_t*) read_set;
  int num = sizeof(read_set) / REG_ADDR_SZ;

  while (num--)
  {
    sx9200_i2c_rd(NULL, *reg_addr++, REG_VAL_SZ, (uint8_t*) &reg_val);
    *raw_data++ = REV32(reg_val);
  }

  /* data processing/conversion */
  uint32_t val[sizeof(read_set) / REG_ADDR_SZ] =
    { 0 };

  for (int i = 0; i < (sizeof(read_set) / REG_ADDR_SZ); ++i)
  {
    val[i] = MG(raw_val[i]);
  }

  /* only present data if different than previous reading */
  static uint8_t prev[sizeof(val)] =
    { 0 };

  if (0 != memcmp(prev, val, sizeof(prev)))
  {
    /* present data */
    dataout_proc_queue(val, sizeof(val), F_DOUT_SOC);

    // update previous values for future reference
    memcpy(prev, val, sizeof(prev));
  }
}

/* -------- task process -------- */
static void sx92_isr_handler (void * arg)
{
  int xHigherPriorityTaskWoken = pdFALSE;

  xSemaphoreGiveFromISR(xSemaphore, &xHigherPriorityTaskWoken);
}

static void sx92_task_handler (void * arg)
{
  for (;;)
  {
    if (xSemaphoreTake(xSemaphore, portMAX_DELAY) == pdTRUE)
    {
      proc_check();
    }
  }
}

int sx92_raw_proc_start (void)
{
  /* if task exist, then end the whole process */
  if (xTask)
  {
    gpio_isr_handler_remove(NEGEDGE_PIN_NUM);
    gpio_uninstall_isr_service();
    vTaskDelete(xTask);
    xTask = NULL;

    PRNS("sx9200 ended\n");
    return 0;
  }

  if (NULL == xSemaphore)
  {
    xSemaphore = xSemaphoreCreateBinary();
  }

  int result = ESP_FAIL;
  if (NULL != xSemaphore)
  {
    result = xTaskCreate(sx92_task_handler, "sx92_task_handler", 2048,
    NULL,
                         10, &xTask);

    if (pdPASS == result)
    {
      //install gpio isr service
      gpio_install_isr_service(0);
      result = gpio_isr_handler_add(NEGEDGE_PIN_NUM,
                                    sx92_isr_handler,
                                    (void*) NULL);
    }
  }

  sx9200_proc_setup();

  PRNTHT("sx9200 :: ", result, "\n");

  return result;
}

