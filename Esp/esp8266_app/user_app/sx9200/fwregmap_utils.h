#pragma once

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

struct fwregmap_handle_t;
typedef int (*fwregmap_reg_reader_fn_t)(void* context, int addr, int size, uint8_t* data);
typedef int (*fwregmap_reg_writer_fn_t)(void* context, int addr, int size, uint8_t* data);

/// The fwregmap_handle_t is what tells the generated code how to read/write
/// specific register offsets.
typedef struct fwregmap_handle_t {
    void* context;
    fwregmap_reg_reader_fn_t reg_reader_fn;
    fwregmap_reg_writer_fn_t reg_writer_fn;
} fwregmap_handle_t;

#ifdef __cplusplus
}
#endif
