#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

COMPONENT_ADD_INCLUDEDIRS += inc
COMPONENT_ADD_INCLUDEDIRS += comproc
COMPONENT_ADD_INCLUDEDIRS += ssd1306
COMPONENT_ADD_INCLUDEDIRS += ../misc/inc
COMPONENT_ADD_INCLUDEDIRS += ../../esp32md1_app/rtosOnly
COMPONENT_ADD_INCLUDEDIRS += ../../../../elle_vs/Projects/socket/ltv
