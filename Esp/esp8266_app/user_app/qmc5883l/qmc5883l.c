/*
 * qmc5883l.c Magnetometer
 *
 *  Created on: Nov 18, 2020
 *      Author: ellerena
 */


#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"
//#include "esp_bit_defs.h"
#include "driver/gpio.h"
#include "drv_gpio.h"
#include "os_i2c.h"
#include "mis_logger.h"
#include "oledproc.h"
#include "sock.h"

#define SEN_ADDR7 (0x0d)

/* Eeprom register addresses */
enum MAG_REG_ADDRESSES
{
    R_DATA_X_L = 0,
    R_DATA_X_M, // 1
    R_DATA_Y_L, // 2
    R_DATA_Y_M, // 3
    R_DATA_Z_L, // 4
    R_DATA_Z_M, // 5
    R_STATUS, // 6
    R_TEMPERATURE_L, // 7
    R_TEMPERATURE_M, // 8
    R_CONTROL_1, // 9
    R_CONTROL_2, // 0xa
    R_PERIOD // 0xb
};

#define CTRL1_OSR_512 (0 << 6)
#define CTRL1_OSR_256 (1 << 6)
#define CTRL1_OSR_128 (2 << 6)
#define CTRL1_OSR_64 (3 << 6)
#define CTRL1_RNG_2G (0 << 4)
#define CTRL1_RNG_8G (1 << 4)
#define CTRL1_ODR_10HZ (0 << 2)
#define CTRL1_ODR_50HZ (1 << 2)
#define CTRL1_ODR_100HZ (2 << 2)
#define CTRL1_ODR_200HZ (3 << 2)
#define CTRL1_MODE_STANDBY (0)
#define CTRL1_MODE_CONTINUOUS (1)

#define CTRL2_INT_ENB (1 << 0)
#define CRTL2_ROL_PNT (1 << 6)
#define CTRL2_SOFT_RST (1 << 7)

#define STATUS_DOR (1 << 2)
#define STATUS_LOCK (1 << 1)
#define STATUS_RDY (1 << 0)

struct
{
  int16_t temp;
  int16_t xyz[3];
  uint32_t timestamp;
} raw;

static SemaphoreHandle_t xSemaphore = NULL;
static TaskHandle_t xTask = NULL;

/*
 * @brief  Read generic device register (platform dependent)
 */
static int platform_reg_rd (uint8_t reg, uint8_t * val, uint32_t len)
{
  uint32_t l = OS_i2c_master_rd_ar(val, len, SEN_ADDR7, &reg);

  return l == len ? ESP_OK : ESP_FAIL;
}

/*
 * @brief  Write generic device register (platform dependent)
 */
static int platform_reg_wr (uint8_t reg, uint8_t * val, uint32_t len)
{
  uint32_t l = OS_i2c_master_wr_ar(val, len, SEN_ADDR7, &reg);

  return l == len ? ESP_OK : ESP_FAIL;
}

/*
 * @brief  platform specific delay (platform dependent)
 *
 * @param  ms        delay in ms
 *
 */
static void platform_delay (uint32_t ms)
{
  vTaskDelay(ms / portTICK_PERIOD_MS);
}

/*
 * @brief  platform specific initialization (platform dependent)
 */
static void platform_init (void)
{
  // Enable i2c STOP mode"

  int stop_op;
  do
  {
    stop_op = OS_i2c_toggle_repeat_start();
  }
  while (1 != stop_op);
}

static void proc_setup (void)
{
  /* Init test platform */
  platform_init();

  uint8_t regval = CTRL2_SOFT_RST; // reset device
  platform_reg_wr(R_CONTROL_2, &regval, 1);
  platform_delay(5); // delay to allow reset complete

  regval = 0x01; // define set/reset period
  platform_reg_wr(R_PERIOD, &regval, 1);

  regval = 0; // enable interrupt pin
  platform_reg_wr(R_CONTROL_2, &regval, 1);

  regval = CTRL1_OSR_128 + CTRL1_RNG_8G + CTRL1_ODR_10HZ + CTRL1_MODE_CONTINUOUS;
  platform_reg_wr(R_CONTROL_1, &regval, 1);

}

static void proc_check (void)
{
#define MG(x) ((((int)(x))*800)/32768)

  /* read back data from the sensor */
  platform_reg_rd(R_DATA_X_L, (uint8_t*) raw.xyz, sizeof(raw.xyz));

  /* data processing/conversion */
  int val[3];
  val[0] = MG(raw.xyz[0]); // x axis read value
  val[1] = MG(raw.xyz[1]); // y axis read value
  val[2] = MG(raw.xyz[2]); // z axis read value

  /* only present data if different than previous reading */
  static uint8_t prev[sizeof(val)] =
    { 0 };

  if (0 != memcmp(prev, val, sizeof(val)))
  {
    /* present data */
    char msg[23];
    sprintf(msg, "mag %4d %4d %4d" CRLF, val[0], val[1], val[2]);
    sock_tx_proc_queue(msg, strlen(msg), 0, 1);
    oled_proc_queue((void*) msg);
    PRNS(msg);

    // update previous values for future reference
    memcpy(prev, val, sizeof(val));
  }
}


/* -------- task process -------- */
static void mag_isr_handler (void * arg)
{
  int xHigherPriorityTaskWoken = pdFALSE;

  xSemaphoreGiveFromISR(xSemaphore, &xHigherPriorityTaskWoken);
}

static void mag_task_handler (void * arg)
{
  for (;;)
  {
    if (xSemaphoreTake(xSemaphore, portMAX_DELAY) == pdTRUE)
    {
      proc_check();
    }
  }
}

/**
 * @brief external API to enable/disable tilt detection
 *        every time it is called, it toggles on/off
 *        tilt detection
 * */
int qmc5883l_raw_proc_start (void)
{
  /* if task exist, then end the whole process */
  if (xTask)
  {
    gpio_isr_handler_remove(POSEDGE_PIN_NUM);
    gpio_uninstall_isr_service();
    vTaskDelete(xTask);
    xTask = NULL;
    platform_init();
    uint8_t regval = CTRL2_SOFT_RST; // reset device
    platform_reg_wr(R_CONTROL_2, &regval, 1);

    PRNS("Magnetometer ended\n");
    return 0;
  }

  if (NULL == xSemaphore)
  {
    xSemaphore = xSemaphoreCreateBinary();
  }

  int result = ESP_FAIL;
  if (NULL != xSemaphore)
  {
    result = xTaskCreate(mag_task_handler,
                         "mag_task_handler",
                         2048,
                         NULL,
                         10,
                         &xTask);

    if (pdPASS == result)
    {
      //install gpio isr service
      gpio_install_isr_service(0);
      result = gpio_isr_handler_add(POSEDGE_PIN_NUM,
                                    mag_isr_handler,
                                    (void*) NULL);
    }
  }

  proc_setup();

  PRNTHT("Magnetometer :: ", result, "\n");

  return result;
}
