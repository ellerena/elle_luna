## mlx90614 - driver component build configuration

FILE(GLOB allc *.c)

idf_component_register(SRCS ${allc}
                       INCLUDE_DIRS "."
                       REQUIRES ssd1306 user_app
                       )
