/*
 * hmc5883l.h
 *
 *  Created on: Nov 18, 2020
 *      Author: ellerena
 */

#ifndef USER_APP_HMC5883L_HMC5883L_H_
#define USER_APP_HMC5883L_HMC5883L_H_

extern int qmc5883l_raw_proc_start (void);

#endif /* USER_APP_HMC5883L_HMC5883L_H_ */
