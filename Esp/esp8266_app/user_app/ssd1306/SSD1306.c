/******************************************************************************
 SSD1306.c                                   *
 SSD1306 OLED driver for CCS PIC C compiler                 *
 *
 This driver supports SPI mode and I2C mode (default).                        *
 For the SPI mode and before the include of the driver file, add:             *
 #define SSD1306_SPI_MODE                                                   *
 *
 https://simple-circuit.com/                                                  *
 *
 *******************************************************************************
 *******************************************************************************
 This is a library for our Monochrome OLEDs based on SSD1306 drivers          *
 *
 Pick one up today in the adafruit shop!                                     *
 ------> http://www.adafruit.com/category/63_98                              *
 *
 Adafruit invests time and resources providing this open source code,         *
 please support Adafruit and open-source hardware by purchasing               *
 products from Adafruit!                                                      *
 *
 Written by Limor Fried/Ladyada  for Adafruit Industries.                     *
 BSD license, check license.txt for more information                          *
 All text above, and the splash screen must be included in any redistribution *
 *******************************************************************************/

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "os_i2c.h"
#include "disp_txt_library.h"
#include "SSD1306.h"

#define SWAP(a, b)      { uint8_t t = a; a = b; b = t; }
#define VRAM_SZ         (SSD1306_LCDWIDTH*SSD1306_LCDHEIGHT / 8)
#define SSD1306_CHARS_L 21 //((SSD1306_LCDWIDTH / 6) - 1)
#define SSD1306_WRAP    SSD1306_LINE8 //(SSD1306_LCDHEIGHT - SSD1306_LINES)

extern int16_t curx, cury;

uint8_t rotation = 0; /* Display rotation (0 thru 3) */
uint8_t disp_w; /* we must use variables for the display width */
uint8_t disp_h; /* and height because they change when rotating */
uint8_t startline = SSD1306_START_LINE; /* local variable used for vertical scrolling */

/* our 'video ram'. Stores the pixel content of the entire screen */
static union
{
  uint8_t buf[VRAM_SZ];
  uint8_t page[SSD1306_PAGES][SSD1306_LCDWIDTH];
} vram;

const uint8_t logo[VRAM_SZ] = LOGO;

/**
 * @brief re-assemble command and data into a single string
 *          and transmit it to device
 */
void ssd1306_intro_wr (uint8_t * data, int32_t size, uint8_t command)
{
  uint8_t *p = (uint8_t*) malloc(size + sizeof(uint8_t));

  if (p)
  {
    // assemble command and data into a single continuous string
    p[0] = command;
    memcpy((void* ) (&p[1]), data, size);
    // send the whole thing to the display
    OS_i2c_master_wr(p, size + sizeof(uint8_t));
    free(p);
  }
}

void ssd1306_command (uint8_t c)
{
  uint8_t comm[2] =
    { SSD1306_CO_COMMAND, c };

  OS_i2c_master_wr(comm, 2);
}

/**
 * @brief low level initialization
 *          set/config registers for operation and
 *          initializes the local vram with our logo screen
 * */
void SSD1306_Init (void)
{
  uint8_t comm[] =
    {
    SSD1306_DISPLAYOFF,
      SSD1306_SETDISPLAYCLOCKDIV, 0x80,
      SSD1306_SETMULTIPLEX,
      SSD1305_WDWHEIGHT - 1,
      SSD1306_SETDISPLAYOFFSET,
      0, SSD1306_SETSTARTLINE | 0x0,
      SSD1306_CHARGEPUMP,
      0x14,
      SSD1306_MEMORYMODE,
      0, SSD1306_SEGREMAP | 0x1,
      SSD1306_COMSCANDEC,
#if (SSD1305_WDWHEIGHT==64)
      SSD1306_SETCOMPINS, 0x12,
      SSD1306_SETCONTRAST, 0xCF,
#else
      SSD1306_SETCOMPINS,
      0x02,
      SSD1306_SETCONTRAST,
      0x8F,
#endif
      SSD1306_SETPRECHARGE,
      0xF1,
      SSD1306_SETVCOMDETECT,
      0x40,
      SSD1306_DISPLAYALLON_RESUME,
      SSD1306_NORMALDISP,
      SSD1306_DEACTIVATE_SCROLL,
      SSD1306_COLUMNADDR, 0, SSD1306_LCDWIDTH - 1,
      SSD1306_PAGEADDR,
      0, SSD1306_PAGES - 1,
      SSD1306_DISPLAYON };

  ssd1306_intro_wr(comm, sizeof(comm), SSD1306_CO_COMMAND);

  // reset key parameters
  disp_h = SSD1306_LCDHEIGHT;
  disp_w = SSD1306_LCDWIDTH;
  startline = SSD1306_START_LINE;

  memcpy(vram.buf, logo, sizeof(vram)); // initialize vram with our logo
}

/**
 * @brief transfers entire local buffer to device (full screen)
 */
void display_full_frame (void)
{
  uint8_t comm[] =
    { SSD1306_PAGEADDR, 0, SSD1306_PAGES - 1 };

  // set the device gddram pointer at the new page
  ssd1306_intro_wr(comm, sizeof(comm), SSD1306_CO_COMMAND);

  /* transfer the entire buffer to the device */
  for (int i = 0; i < SSD1306_PAGES; ++i)
  {
    ssd1306_intro_wr(vram.page[i], SSD1306_LCDWIDTH, SSD1306_CO_DATA_FIELD);
  }
}

/**
 * @brief trasnfers a single line to the devie (1 page)
 */
void display_line_nxt (void)
{
  int page = ((cury / 8) - 1) & (SSD1306_PAGES - 1);
  uint8_t comm[] =
    { SSD1306_PAGEADDR, page, SSD1306_PAGES - 1 };

  // set the device gddram pointer at the new page
  ssd1306_intro_wr(comm, sizeof(comm), SSD1306_CO_COMMAND);
  // transmit the content for the new line
  ssd1306_intro_wr(vram.page[page], SSD1306_LCDWIDTH, SSD1306_CO_DATA_FIELD);
}

/**
 * @brief   Allow to invert the display
 * @param   invert, either 0: normal or 1: invert
 * */
void display_invert (int invert)
{
  ssd1306_command(invert ? SSD1306_INVERTDISP : SSD1306_NORMALDISP);
}

// Activate a right handed scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// display_scrollright(0x00, 0x0F)
void display_startScrollRight (uint8_t start, uint8_t stop)
{
  ssd1306_command(SSD1306_RIGHT_HORIZONTAL_SCROLL);
  ssd1306_command(0X00);
  ssd1306_command(start);
  ssd1306_command(0X00);
  ssd1306_command(stop);
  ssd1306_command(0X00);
  ssd1306_command(0XFF);
  ssd1306_command(SSD1306_ACTIVATE_SCROLL);
}

// Activate a right handed scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// display_scrollright(0x00, 0x0F)
void display_startScrollLeft (uint8_t start, uint8_t stop)
{
  ssd1306_command(SSD1306_LEFT_HORIZONTAL_SCROLL);
  ssd1306_command(0X00);
  ssd1306_command(start);
  ssd1306_command(0X00);
  ssd1306_command(stop);
  ssd1306_command(0X00);
  ssd1306_command(0XFF);
  ssd1306_command(SSD1306_ACTIVATE_SCROLL);
}

// Activate a diagonal scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// display_scrollright(0x00, 0x0F)
void display_startScrollDiagRight (uint8_t start, uint8_t stop)
{
  ssd1306_command(SSD1306_SET_VERTICAL_SCROLL_AREA);
  ssd1306_command(0X00);
  ssd1306_command(SSD1306_LCDHEIGHT);
  ssd1306_command(SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL);
  ssd1306_command(0X00);
  ssd1306_command(start);
  ssd1306_command(0X00);
  ssd1306_command(stop);
  ssd1306_command(0X01);
  ssd1306_command(SSD1306_ACTIVATE_SCROLL);
}

// Activate a diagonal scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// display_scrollright(0x00, 0x0F)
void display_startScrollDiagLeft (uint8_t start, uint8_t stop)
{
  ssd1306_command(SSD1306_SET_VERTICAL_SCROLL_AREA);
  ssd1306_command(0X00);
  ssd1306_command(SSD1306_LCDHEIGHT);
  ssd1306_command(SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL);
  ssd1306_command(0X00);
  ssd1306_command(start);
  ssd1306_command(0X00);
  ssd1306_command(stop);
  ssd1306_command(0X01);
  ssd1306_command(SSD1306_ACTIVATE_SCROLL);
}

void display_stopScroll (void)
{
  ssd1306_command(SSD1306_DEACTIVATE_SCROLL);
}

void display_dim (uint8_t level)
{
  // the range of contrast to too small to be really useful
  // it is useful to dim the display
  ssd1306_command(SSD1306_SETCONTRAST);
  ssd1306_command(level ? 0xCF : 0);
}

void display_clear (void)
{
  /* clear video ram */
  memset(vram.buf, 0, VRAM_SZ);

  /* clear display ram */
  display_full_frame();

  /* set cursor position to origin */
  cury = 0;
  curx = 0;

  /* reset scrolling */
  startline = SSD1306_START_LINE;
  ssd1306_command(SSD1306_SETSTARTLINE | 0);
}

void display_puts (const char * str)
{
  /* update scrolling calculator */
  startline += 8;

  /* if we're at bottom, scroll one line up */
  if (startline >= SSD1306_SETSTARTLINE)
  {
    ssd1306_command(startline);
    if (startline >= 0x78)
    {
      startline = SSD1306_LINE8;
    }
  }

  /* print one complete line of text */
  for (int i = 0; i < SSD1306_CHARS_L; ++i)
  {
    /* write char if it is printable, space otherwise */
    display_putc(*str >= ' ' ? *str++ : ' ');

    /* ensure we don't pass beyond right edge */
    if (((disp_w - curx) < 6) || (curx < 6))
      break;
  }

  /* if last line is reached, wrap to the top line */
  if (cury > SSD1306_WRAP)
  {
    cury = 0;
  }

  /* transmit line of text to the device */
  display_line_nxt();
}
/////
void display_fillRect (uint8_t x,
                       uint8_t y,
                       uint8_t w,
                       uint8_t h,
                       uint8_t color)
{
  for (int i = x; i < x + w; i++)
    display_drawVLine(i, y, h, color);
}

void display_fillScreen (void)
{
  display_fillRect(0, 0, SSD1306_LCDWIDTH, SSD1306_LCDHEIGHT, 1);
}

void display_setRotation (uint8_t m)
{
  rotation = (m & 3);
  switch (rotation)
  {
    case 0:
    case 2:
      disp_w = SSD1306_LCDWIDTH;
      disp_h = SSD1306_LCDHEIGHT;
      break;
    case 1:
    case 3:
      disp_w = SSD1306_LCDHEIGHT;
      disp_h = SSD1306_LCDWIDTH;
      break;
  }
}

/*!
 @brief   Get rotation setting for display
 @returns 0 thru 3 corresponding to 4 cardinal rotations
 */
uint8_t display_getRotation (void)
{
  return rotation;
}

// the most basic function, set a single pixel
void display_drawPixel (uint8_t x, uint8_t y, uint8_t color)
{
  uint32_t pos;

  if ((x >= disp_w) || (y >= disp_h))
    return;

  // check rotation, move pixel around if necessary
  switch (rotation)
  {
    case 1:
      SWAP(x, y)
      ;
      x = SSD1306_LCDWIDTH - x - 1;
      break;
    case 2:
      x = SSD1306_LCDWIDTH - x - 1;
      y = SSD1306_LCDHEIGHT - y - 1;
      break;
    case 3:
      SWAP(x, y)
      ;
      y = SSD1306_LCDHEIGHT - y - 1;
      break;
  }

  /* calculate pixel position in video ram, x is the column number */
  pos = x + (uint16_t) (y / 8) * SSD1306_LCDWIDTH;

  switch (color)
  {
    case WHITE:
      vram.buf[pos] |= (1 << (y & 7)); /* set bit */
      break;
    case BLACK:
      vram.buf[pos] &= ~(1 << (y & 7)); /* reset bit */
      break;
    case INVERSE:
      vram.buf[pos] ^= (1 << (y & 7)); /* invert bit */
      break;
  }
}

void display_drawHLine (uint8_t x, uint8_t y, uint8_t w, uint8_t color)
{
  bool bSwap = false;
  switch (rotation)
  {
    case 0:
      // 0 degree rotation, do nothing
      break;
    case 1:
      // 90 degree rotation, swap x & y for rotation, then invert x
      bSwap = true;
      SWAP(x, y)
      ;
      x = SSD1306_LCDWIDTH - x - 1;
      break;
    case 2:
      // 180 degree rotation, invert x and y - then shift y around for disp_h.
      x = SSD1306_LCDWIDTH - x - 1;
      y = SSD1306_LCDHEIGHT - y - 1;
      x -= (w - 1);
      break;
    case 3:
      // 270 degree rotation, swap x & y for rotation, then invert y  and adjust y for w (not to become h)
      bSwap = true;
      SWAP(x, y)
      ;
      y = SSD1306_LCDHEIGHT - y - 1;
      y -= (w - 1);
      break;
  }

  if (bSwap)
  {
    drawFastVLineInternal(x, y, w, color);
  }
  else
  {
    drawFastHLineInternal(x, y, w, color);
  }
}

void display_drawVLine (uint8_t x, uint8_t y, uint8_t h, uint8_t color)
{
  bool bSwap = false;
  switch (rotation)
  {
    case 0:
      break;
    case 1:
      // 90 degree rotation, swap x & y, then invert x and adjust x for h (now to become w)
      bSwap = true;
      SWAP(x, y)
      ;
      x = SSD1306_LCDWIDTH - x - 1;
      x -= (h - 1);
      break;
    case 2:
      // 180 degree rotation, invert x and y - then shift y around for disp_h.
      x = SSD1306_LCDWIDTH - x - 1;
      y = SSD1306_LCDHEIGHT - y - 1;
      y -= (h - 1);
      break;
    case 3:
      // 270 degree rotation, swap x & y for rotation, then invert y
      bSwap = true;
      SWAP(x, y)
      ;
      y = SSD1306_LCDHEIGHT - y - 1;
      break;
  }

  if (bSwap)
  {
    drawFastHLineInternal(x, y, h, color);
  }
  else
  {
    drawFastVLineInternal(x, y, h, color);
  }
}

void drawFastHLineInternal (uint8_t x, uint8_t y, uint8_t w, uint8_t color)
{
  // Do bounds/limit checks
  if (y >= SSD1306_LCDHEIGHT)
  {
    return;
  }

  // make sure we don't go off the edge of the display
  if ((x + w) > SSD1306_LCDWIDTH)
  {
    w = (SSD1306_LCDWIDTH - x);
  }

  // set up the pointer for  movement through the buffer
  register uint8_t *pBuf = vram.buf;
  // adjust the buffer pointer for the current row
  pBuf += ((uint16_t) (y / 8) * SSD1306_LCDWIDTH);
  // and offset x columns in
  pBuf += x;

  register uint8_t mask = 1 << (y & 7);

  switch (color)
  {
    case WHITE:
      while (w--)
      {
        *pBuf++ |= mask;
      }
      ;
      break;
    case BLACK:
      mask = ~mask;
      while (w--)
      {
        *pBuf++ &= mask;
      }
      break;
    case INVERSE:
      while (w--)
      {
        *pBuf++ ^= mask;
      }
      break;
  }
}

void drawFastVLineInternal (uint8_t x, uint8_t __y, uint8_t __h, uint8_t color)
{

  // do nothing if we're off the left or right side of the screen
  if (x >= SSD1306_LCDWIDTH)
  {
    return;
  }

  // make sure we don't go past the disp_h of the display
  if ((__y + __h) > SSD1306_LCDHEIGHT)
  {
    __h = (SSD1306_LCDHEIGHT - __y);
  }

  // this display doesn't need ints for coordinates, use local byte registers for faster juggling
  register uint8_t y = __y;
  register uint8_t h = __h;

  // set up the pointer for fast movement through the buffer
  register uint8_t *pBuf = vram.buf;
  // adjust the buffer pointer for the current row
  pBuf += ((uint16_t) (y / 8) * SSD1306_LCDWIDTH);
  // and offset x columns in
  pBuf += x;

  // do the first partial byte, if necessary - this requires some masking
  register uint8_t mod = (y & 7);
  if (mod)
  {
    // mask off the high n bits we want to set
    mod = 8 - mod;

    // note - lookup table results in a nearly 10% performance improvement in fill* functions
    // register uint8_t mask = ~(0xFF >> (mod));
    static uint8_t premask[8] =
      { 0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE };
    register uint8_t mask = premask[mod];

    // adjust the mask if we're not going to reach the end of this byte
    if (h < mod)
    {
      mask &= (0XFF >> (mod - h));
    }

    switch (color)
    {
      case WHITE:
        *pBuf |= mask;
        break;
      case BLACK:
        *pBuf &= ~mask;
        break;
      case INVERSE:
        *pBuf ^= mask;
        break;
    }

    // fast exit if we're done here!
    if (h < mod)
    {
      return;
    }

    h -= mod;

    pBuf += SSD1306_LCDWIDTH;
  }

  // write solid bytes while we can - effectively doing 8 rows at a time
  if (h >= 8)
  {
    if (color == INVERSE)
    { // separate copy of the code so we don't impact performance of the black/white write version with an extra comparison per loop
      do
      {
        *pBuf = ~(*pBuf);

        // adjust the buffer forward 8 rows worth of data
        pBuf += SSD1306_LCDWIDTH;

        // adjust h & y (there's got to be a faster way for me to do this, but this should still help a fair bit for now)
        h -= 8;
      }
      while (h >= 8);
    }
    else
    {
      // store a local value to work with
      register uint8_t val = (color == WHITE) ? 255 : 0;

      do
      {
        // write our value in
        *pBuf = val;

        // adjust the buffer forward 8 rows worth of data
        pBuf += SSD1306_LCDWIDTH;

        // adjust h & y (there's got to be a faster way for me to do this, but this should still help a fair bit for now)
        h -= 8;
      }
      while (h >= 8);
    }
  }

  // now do the final partial byte, if necessary
  if (h)
  {
    mod = h & 7;
    // this time we want to mask the low bits of the byte, vs the high bits we did above
    // register uint8_t mask = (1 << mod) - 1;
    // note - lookup table results in a nearly 10% performance improvement in fill* functions
    static uint8_t postmask[8] =
      { 0x00, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F };
    register uint8_t mask = postmask[mod];
    switch (color)
    {
      case WHITE:
        *pBuf |= mask;
        break;
      case BLACK:
        *pBuf &= ~mask;
        break;
      case INVERSE:
        *pBuf ^= mask;
        break;
    }
  }
}

// end of driver code.
