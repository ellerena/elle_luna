/*
 * oledproc.c
 *
 *  Created on: Oct 15, 2021
 *      Author: ellerena
 */

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "string.h"
#include "esp_err.h"
#include "SSD1306.h"
#include "disp_txt_library.h"

#define QSZ 8
#define POOL_V 32
#define POOL_H 21

static QueueHandle_t oledproc_Q = NULL;
static TaskHandle_t oledproc_T = NULL;
static char msgpool[POOL_V][POOL_H];
static int msgid = 0;

static void oledproc_task (void * args)
{
  int index;

  display_Init();

  for (;;)
  {
    if (pdTRUE == xQueueReceive(oledproc_Q, &index, portMAX_DELAY))
    {
      display_puts(msgpool[index]);
    }
  }
}

/* external API */
int oled_proc_start (void)
{
  int result = ESP_FAIL;

  /* create our queue - if it doesn't yet exist */
  if (NULL == oledproc_Q)
  {
    oledproc_Q = xQueueCreate(QSZ, sizeof(char));
  }

  if (NULL != oledproc_Q)
  {
    result = xTaskCreate(oledproc_task, "oledproc_task",
                         1024,
                         NULL,
                         6,
                         &oledproc_T);
    if (pdPASS == result)
    {
      result = ESP_OK;
    }
  }

  return result;
}

void oled_proc_end (void)
{
  vTaskDelete(oledproc_T);
}

int oled_proc_queue (void * request)
{
  /* copy the request msg entirely to our local pool */
  memcpy(msgpool[msgid], (char* ) request, POOL_H);

  /* add the msp index to our queue */
  int result = xQueueSend(oledproc_Q, &msgid, portMAX_DELAY);

  if (pdTRUE == result)
  {
    /* move msg pointer to next position in the pool */
    msgid = (msgid + 1) & (POOL_V - 1);
    result = ESP_OK;
  }
  else
  {
    result = ESP_FAIL;
  }

  return result;
}

