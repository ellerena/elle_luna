/*
 * mlx90614.c Infrared Thermometer
 *
 *  Created on: Oct 30, 2021
 *      Author: ellerena
 */

#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "esp_crc.h"
#include "esp_err.h"
#include "esp_bit_defs.h"
#include "os_i2c.h"
#include "mis_logger.h"
#include "oledproc.h"
#include "sock.h"

#define SEN_ADDR7 (0x22) //(0x0 and 0x5a: from factory, 0x22: custom)

#define MAP_RAM (0x0)
#define MAP_EEPROM (0x20)
#define READFLAGS (0xf0)
#define SLEEPMODE (0xff)
#define KELVIN2CELSIUS(k) ((((int)k * 100) - 27315)/100)

/* Eeprom register addresses */
#define TO_MAX EEPROMADDR(0x0)
#define TO_MIN EEPROMADDR(0x1)
#define PWMCTRL EEPROMADDR(0x2)
#define TA_RANGE EEPROMADDR(0x3)
#define EMISSIVITY EEPROMADDR(0x4)
#define CONFIG_REG1 EEPROMADDR(0x5)
#define SMBUS_ADDR EEPROMADDR(0xE)
#define ID_NUM0 EEPROMADDR(0x1c)
#define ID_NUM1 EEPROMADDR(0x1d)
#define ID_NUM2 EEPROMADDR(0x1e)
#define ID_NUM3 EEPROMADDR(0x1f)

/* Ram register addresses */
#define RAW_IR_CH1 RAMADDR(0x4)
#define RAW_IR_CH2 RAMADDR(0x5)
#define TA RAMADDR(0x6)
#define TOBJ1 RAMADDR(0x7)
#define TOBJ2 RAMADDR(0x8)

/* utility macros */
#define I2C_ADDR_WR(a) ((a << 1) | 0)
#define I2C_ADDR_RD(a) ((a << 1) | 1)
#define RAMADDR(x) (MAP_RAM + x)
#define EEPROMADDR(x) (MAP_EEPROM + x)

static void mlx_tmr_handler (TimerHandle_t htimer);
static TimerHandle_t mlx_timer = NULL;

/*
 * @brief  Read generic device register (platform dependent)
 *
 * @param  reg       register to read
 * @param  bufp      pointer to buffer that store the data read
 * @param  len       number of consecutive register to read
 *
 */
static int32_t platform_reg_rd (uint8_t reg, uint16_t * val)
{
  uint8_t pval[6] =
    { I2C_ADDR_WR(SEN_ADDR7), reg, I2C_ADDR_RD(SEN_ADDR7) };

  uint32_t l = OS_i2c_master_rd_ar((uint8_t*) &pval[3], 3, SEN_ADDR7, &reg);

  if (3 == l)
  {
//    uint8_t crc = crc8(pval, 5); // crc8_le(uint8_t crc, uint8_t const *buf, uint32_t len);
//    printf("[%02x], %02x\n", pval[5], crc);
    uint16_t result = pval[4];
    *val = (result << 8) + pval[3];

    return ESP_OK;
  }

  return ESP_FAIL;
}

/*
 * @brief  Write generic device register (platform dependent)
 *
 * @param  reg       register to write
 * @param  bufp      pointer to data to write in register reg
 * @param  len       number of consecutive register to write
 *
 */
static int32_t platform_reg_wr (uint8_t reg, uint16_t val)
{
  uint8_t pval[5] =
    { I2C_ADDR_WR(SEN_ADDR7), reg, val & 0xff, (val >> 8), 0 };

  uint32_t l = OS_i2c_master_wr_ar((uint8_t*) &pval[2], 3, SEN_ADDR7, &reg);

  if (3 == l)
  {
//    uint8_t crc = crc8(pval, 5); // crc8_le(uint8_t crc, uint8_t const *buf, uint32_t len);
//    printf("[%02x], %02x\n", pval[5], crc);
    return ESP_OK;
  }

  return ESP_FAIL;
}

/*
 * @brief  platform specific delay (platform dependent)
 *
 * @param  ms        delay in ms
 *
 */
static void platform_delay (uint32_t ms)
{

}

/*
 * @brief  platform specific initialization (platform dependent)
 */
static void platform_init (void)
{
  // Disable i2c STOP mode: sensor uses "Repeat Start"

  int stop_op;

  do
  {
    stop_op = OS_i2c_toggle_repeat_start();

  }
  while (0 != stop_op);
}

/**
 * @brief switch stop/repeat start for i2c
 * */
static void i2c_stop_mode (int stop)
{
  int stop_op;

  do
  {
    stop_op = OS_i2c_toggle_repeat_start();

  }
  while (stop != stop_op);
}

static void proc_setup (void)
{
  /* Init test platform */
  platform_init();
  PRNS("Init complete" CRLF);
  platform_delay(0);
}

/* -------- task process -------- */
static void mlx_tmr_handler (TimerHandle_t htimer)
{
#define CELSIUS(x) (((x * 2) - 27315) / 100)

  i2c_stop_mode(0); // enable repeat-start for i2c

  int32_t t_reg = 0; // max valid value ix 0x7fff
  int result = platform_reg_rd(TA, (uint16_t*) &t_reg);

  if ((0 == result) && (0 == (t_reg & BIT15))) // bit 15 is error flag
  {
    int32_t val[2];

    val[0] = CELSIUS(t_reg);

    result = platform_reg_rd(TOBJ1, (uint16_t*) &t_reg);
    i2c_stop_mode(1); // disable repeat start now

    if ((0 == result) && (0 == (t_reg & BIT15)))
    {
      val[1] = CELSIUS(t_reg);
      ;

      /* only present data if it has changed */
      static int32_t prev[2] =
        { 0 };

      if (memcmp(prev, val, 2 * sizeof(int32_t)))
      {
        char msg[30];
        sprintf(msg, "Ta %3d To %3d" CRLF, val[0], val[1]);
        sock_tx_proc_queue(msg, strlen(msg), 0, 1);
        oled_proc_queue((void*) msg);
        PRNS(msg);
        memcpy(prev, val, 2 * sizeof(int32_t));
      }

      return;
    }
  }
  else
  {
    i2c_stop_mode(1);
  }

  PRNS("Read error" CRLF);
}

static void mlx90614_raw_proc_end (void)
{
  xTimerStop(mlx_timer, 0);
  xTimerDelete(mlx_timer, 0);
  mlx_timer = NULL;
}

/**
 * @brief external API to enable/disable tilt detection
 *        every time it is called, it toggles on/off
 *        tilt detection
 * */
int mlx90614_raw_proc_start (void)
{
  /* if timer exist, end the whole process */
  if (mlx_timer)
  {
    mlx90614_raw_proc_end();
    PRNS("mlx process ended\n");
    return ESP_OK;
  }

  /* setup/configure the process */
  proc_setup();

  int ret = ESP_FAIL;

  mlx_timer = xTimerCreate("mlx_timer",
                           pdMS_TO_TICKS(1000),
                           pdTRUE,
                           (void*) 1,
                           mlx_tmr_handler);

  if (mlx_timer)
  {
    if (pdPASS == xTimerStart(mlx_timer, 0))
    {
      ret = ESP_OK;
    }
  }

  PRNF("mlx process result: %d\n", ret);
  return ret;
}
