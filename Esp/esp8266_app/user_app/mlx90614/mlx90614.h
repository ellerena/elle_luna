/*
 * mlx90614.h
 *
 *  Created on: Oct 30, 2021
 *      Author: ellerena
 */

#ifndef USER_APP_MLX90614_MLX90614_H_
#define USER_APP_MLX90614_MLX90614_H_

#ifdef __cplusplus
extern "C"
{
#endif

extern int mlx90614_raw_proc_start (void);

#ifdef __cplusplus
}
#endif

#endif /* USER_APP_MLX90614_MLX90614_H_ */
