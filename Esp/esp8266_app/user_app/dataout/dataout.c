/*
 * dataout.c
 *
 *  Created on: Nov 30, 2021
 *      Author: ellerena
 */

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "string.h"
#include "esp_err.h"
#include "mis_logger.h"
#include "sock.h"
#include "dataout.h"

#ifndef ESP_NODISPLAY
#include "oledproc.h"
#endif

#ifdef USE_BLE_GAPPER
#include "ble_gapper.h"
#elif defined(USE_BLE)
#include "ble.h"
#endif

#define QSZ 8
#define POOL_V QSZ
#define POOL_H 128

typedef struct
{
  uint32_t flags;
  int msgid;
  int len;
} msgbox_t;

static msgbox_t msgbox = {0};

static QueueHandle_t datout_Q = NULL;
static TaskHandle_t datout_T = NULL;

static uint8_t msgpool[POOL_V][POOL_H];

static void datout_task (void * args)
{
  msgbox_t msg;

  for (;;)
  {
    /* use peek to retrieve the next item from the queue, we
     * don't want to remove the  item from the queue yet till we've
     * finished processing the associated data array */
    if (pdTRUE == xQueuePeek(datout_Q, &msg, portMAX_DELAY))
    {
      uint32_t flags = msg.flags;

      if (flags & F_DOUT_TXT)
      {
        if (flags & F_DOUT_PRN)
        {
          PRNS((char* )msgpool[msg.msgid]);
        }

#ifndef ESP_NODISPLAY
        if (flags & F_DOUT_OLE)
        {
          oled_proc_queue((void*) msgpool[msg.msgid]);
        }
#endif

        if (flags & F_DOUT_SOC)
        {
#ifndef DONOTUSEWIFI
          sock_tx_proc_queue(msgpool[msg.msgid],
                             msg.len,
                             0,
                             1);
#endif
        }
      }
      else
      {
        if (flags & F_DOUT_PRN)
        {
          log_dump_bytes(msgpool[msg.msgid], msg.len);
        }

        if (flags & F_DOUT_SOC)
        {
#ifndef DONOTUSEWIFI
          sock_tx_proc_queue(msgpool[msg.msgid], msg.len, 0, 0);
#endif
        }

        if (flags & F_DOUT_BLE)
        {
#ifdef USE_BLE_GAPPER
          gapper_update_data(msgpool[msg.msgid], msg.len);
#elif defined(USE_BLE)
          ble_notify_client(msgpool[msg.msgid], msg.len);
#endif
        }
      }

      /* now we can remove the item from the queue and let
       * the data array to be available fro writing new data */
      xQueueReceive(datout_Q, &msg, portMAX_DELAY);
    }
  }
}

/* external API */
int dataout_proc_start (void)
{
  int result = ESP_FAIL;

  /* create our queue - if it doesn't yet exist */
  if (NULL == datout_Q)
  {
    datout_Q = xQueueCreate(QSZ, sizeof(msgbox_t));
  }

  if (NULL != datout_Q)
  {
    result = xTaskCreate(datout_task, "datout_task", 2048, NULL, 6, &datout_T);
    if (pdPASS == result)
    {
      result = ESP_OK;
    }
  }

  return result;
}

void dataout_proc_end (void)
{
  vTaskDelete(datout_T);
}

int dataout_proc_queue (const void * request, int len, uint32_t flags)
{
  static int msgid = 0;

  /* if data represents text, recalculate its length to add null char */
  if (F_DOUT_TXT & flags)
  {
    len = strlen((char*) request);
  }

  /* fill queue element parameters */
  msgbox.flags = flags;
  msgbox.len = len;
  msgbox.msgid = msgid;

  /* add item to our queue */
  int result = xQueueSendToBack(datout_Q, &msgbox, portMAX_DELAY);

  /* copy the request msg entirely to our local pool */
  memcpy(msgpool[msgid], request, len);

  if (F_DOUT_TXT & flags)
  {
    msgpool[msgid][len] = '\0';
  }

    /* move msg pointer to next position in the pool */
  msgid = (msgid + 1) & (POOL_V - 1);

  return result;
}
