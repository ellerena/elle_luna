/*
 * dataout.h
 *
 *  Created on: Nov 30, 2021
 *      Author: ellerena
 */

#ifndef USER_APP_DATAOUT_DATAOUT_H_
#define USER_APP_DATAOUT_DATAOUT_H_

#define F_DOUT_TXT (1 << 0) // data represents text (null ended)
#define F_DOUT_PRN (1 << 10) // present to stdout (print)
#define F_DOUT_OLE (1 << 11) // display in the oled
#define F_DOUT_SOC (1 << 12) // transmit via socket
#define F_DOUT_BLE (1 << 13) // send to BLE gapper

extern int dataout_proc_start (void);
extern int dataout_proc_queue (const void * request, int len, uint32_t flags);

#endif /* USER_APP_DATAOUT_DATAOUT_H_ */
