/*
 * main.c
 *
 *  Created on: May 28, 2020
 *      Author: eddie
 */

#include "freertos/FreeRTOS.h"
#include "esp_err.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "os_gpio.h"
#include "os_i2c.h"
#include "os_led.h"
#include "comproc.h"
#include "dataout.h"
#include "oledproc.h"
#include "http.h"
#include "ble.h"
#include "ble_spp.h"
#include "ble_gapper.h"
#include "ntp.h"
#include "sock.h"

#ifdef ARCH_ESP32
#include "wifi_sta.h"
#else // esp8266
#include "connect.h"
#endif

/* global scope objects */
volatile unsigned int gFlags = 0;

void app_main ()
{
  esp_log_level_set("sock_server_task", ESP_LOG_INFO);
  esp_log_level_set("wifi_event_handler", ESP_LOG_INFO);
  esp_log_level_set("LSM6", ESP_LOG_INFO);
  esp_log_level_set("BLEG", ESP_LOG_INFO); //BLEB
  esp_log_level_set("gatts_profile_event_handler", ESP_LOG_INFO);
  esp_log_level_set("gap_event_handler", ESP_LOG_INFO);

  ESP_ERROR_CHECK(OS_gpio_init()); // initialize gpio (led)
  ESP_ERROR_CHECK(OS_gpio_state_set(2, GPIO_LED_ON)); // turn on led for now

  ESP_ERROR_CHECK(nvs_flash_init());

  ESP_ERROR_CHECK(OS_gpio_state_set(2, GPIO_LED_OFF)); // turn led off
  ESP_ERROR_CHECK(dataout_proc_start()); // start data presentation layer process
  ESP_ERROR_CHECK(OS_led_proc_start()); //start led blinking
  ESP_ERROR_CHECK(OS_led_blink_speed(1)); // 250ms blink
#ifndef DONOTUSEWIFI
#ifdef ARCH_ESP32
  ESP_ERROR_CHECK(wifi_init()); // start wifi socket
#else
  ESP_ERROR_CHECK(live_connect()); // start wifi socket
#endif
  ntp_init(); // start ntp server
  http_start(); // start http server
#endif

  ESP_ERROR_CHECK(OS_led_blink_speed(8)); // 2sec blink

  ESP_ERROR_CHECK(command_proc_start()); // start command processor
#ifndef DONOTUSEWIFI
  ESP_ERROR_CHECK(sock_proc_start()); // start socket process
#endif
  ESP_ERROR_CHECK(OS_i2c_master_init()); // start i2c driver
  ESP_ERROR_CHECK(oled_proc_start()); // start oled display processor
  ESP_ERROR_CHECK(console_proc_start()); // start console

  /* start BLE process (only available in esp32xx)*/
#ifdef ARCH_ESP32
#ifdef USE_BLE_GAPPER
  ble_gapper_proc_start();
#elif defined(USE_BLE)
  ESP_ERROR_CHECK(ble_proc_start());
#endif
  // ESP_ERROR_CHECK(ble_spp_proc_start());
#endif

  // start the lsm6dsx raw data collection
  // char cmd[] = "gr";
  // command_proc_queue(cmd);

  ESP_ERROR_CHECK(OS_led_blink_speed(16)); // 4sec blink
}
