/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file lsm6dsxx_platform.c
 * @date 09.29.2021
 * @brief
 *
 * @details ATTENTION: By default the driver is little endian. If you need switch
 *            to big endian please see "Endianness definitions" in the
 *            header file of the driver (_reg.h).
 *
 **************************************************************************************************/

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_err.h"
#include "drv_gpio.h"
#include "os_i2c.h"
#include "mis_logger.h"
#include "dataout.h"

#include "lsm6dsxx_platform.h"

/************************************ Local Macro Definitions ************************************/
#ifdef __MICROBLAZE__
#define SLEEP_U(us) usleep_MB(us)
#define SLEEP_S(s) sleep_MB(s)
#define OP_STOP 0
#elif defined(ESP8266)
#define SLEEP_U(us)
#define SLEEP_S(s)
#else
#include "xiicps.h"
#define SLEEP_U(us) usleep_A9(us)
#define SLEEP_S(s) sleep_A9(s)
#define OP_STOP XIICPS_7_BIT_ADDR_OPTION
#endif

#define EXT_PROC_FN_CALLS(x) extern void proc_setup_##x(void * data, int len); \
                          extern void proc_check_##x(void)

/************************************ Local Type Definitions  ************************************/
typedef void (*fn_proc_setup_t) (void *, int);
typedef void (*fn_proc_check_t) (void);

typedef struct
{
  char name[8];
  fn_proc_setup_t fn_setup;
  fn_proc_check_t fn_check;
  int stacksz;
} procc_t;

/********************************** Global Variable Declarations *********************************/
/* declare our extern lsm6dsxx process calls here so
 * they are not exposed in the api header */
EXT_PROC_FN_CALLS(6d);
EXT_PROC_FN_CALLS(mlc);
EXT_PROC_FN_CALLS(raw);
EXT_PROC_FN_CALLS(tilt);

stmdev_ctx_t dev_ctx =
  { .write_reg = platform_write, .read_reg = platform_read, .handle = NULL };

/*********************************** Local Constant Definitions **********************************/
const procc_t procc[] =
  {
    { "[6d]", proc_setup_6d, proc_check_6d, 1024 },
    { "[mlc]", proc_setup_mlc, proc_check_mlc, 1024 },
    { "[raw]", proc_setup_raw, proc_check_raw, 3072 },
    { "[tilt]", proc_setup_tilt, proc_check_tilt, 2048 }
  };

/********************************** Local Function Declarations **********************************/
static void isr_handler (void * arg);

static void task_handler (void * arg);

/**
 * @brief external API to enable/disable tilt detection
 *        every time it is called, it toggles on/off
 *        tilt detection
 * */
static int proc_0_start (void * data, int len);

/********************************** Local Variable Declarations **********************************/
static int task_idx                 = 0;
static SemaphoreHandle_t xSemaphore = NULL;
static TaskHandle_t xTask           = NULL;

/********************************** Public Function Declarations *********************************/
int32_t platform_write (void * handle,
                        uint8_t reg,
                        const uint8_t * bufp,
                        uint16_t len)
{
  uint32_t l = OS_i2c_master_wr_ar(bufp, len, SEN_ADDR7, &reg);
  return (l == len) ? ESP_OK : ESP_FAIL;
}

int32_t platform_read (void * handle, uint8_t reg, uint8_t * bufp, uint16_t len)
{
  // assume repeat start is off initially
  uint32_t l = OS_i2c_master_rd_ar(bufp, len, SEN_ADDR7, &reg);
  return (l == len) ? ESP_OK : ESP_FAIL;;
}

void platform_delay (uint32_t ms)
{
  vTaskDelay(ms / portTICK_PERIOD_MS);
}

void platform_puts (const char * str)
{
  dataout_proc_queue(str, 0, F_DOUT_TXT + F_DOUT_PRN + F_DOUT_SOC);
}

void platform_prntht (const char * str1, uint32_t val, const char * str2)
{
  PRNTHT(str1, val, str2);
}

void platform_init (void)
{
  // ensure we are in i2c STOP mode: each transaction ends in stop

  int stop_op;

  do
  {
    stop_op = OS_i2c_toggle_repeat_start();

  }
  while (1 != stop_op);
}

int lsm6dsxx_proc_start (int proc, void * data, int len)
{
  task_idx = proc;

  return proc_0_start(data, len);

}

/*********************************** Local Function Definitions **********************************/
/*  Task Processing */
static void isr_handler (void * arg)
{
  int xHigherPriorityTaskWoken = pdFALSE;

  xSemaphoreGiveFromISR(xSemaphore, &xHigherPriorityTaskWoken);
}

static void task_handler (void * arg)
{
  for (;;)
  {
    if (xSemaphoreTake(xSemaphore, portMAX_DELAY) == pdTRUE)
    {
      assert(task_idx < TASK_NUM);
      procc[task_idx].fn_check();
    }
  }
}

static int proc_0_start (void * data, int len)
{
  /* if task exist, then end the whole process */
  if (xTask)
  {
    gpio_isr_handler_remove(POSEDGE_PIN_NUM);
    gpio_uninstall_isr_service();
    vTaskDelete(xTask);
    xTask = NULL;
    if (xSemaphore)
    {
      vQueueDelete(xSemaphore);
      xSemaphore = NULL;
    }
    dataout_proc_queue("process ended" CRLF, 0, F_DOUT_TXT + F_DOUT_PRN);
    return 0;
  }

  if (NULL == xSemaphore)
  {
    xSemaphore = xSemaphoreCreateBinary();
  }

  int result = ESP_FAIL;
  if (NULL != xSemaphore)
  {
    result = xTaskCreate(task_handler, "task_handler", procc[task_idx].stacksz,
                         NULL,
                         10, &xTask);

    if (pdPASS == result)
    {
      //install gpio isr service
      gpio_install_isr_service(0);
      result = gpio_isr_handler_add(POSEDGE_PIN_NUM, isr_handler, (void*) NULL);
    }
  }

  /* setup/configure the process */
  procc[task_idx].fn_setup(data, len);
  PRNTHT(procc[task_idx].name, result, "\n");
  return result;
}
