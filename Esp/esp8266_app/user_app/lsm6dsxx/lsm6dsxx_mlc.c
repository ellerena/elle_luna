/*
 * lsm6dsxx_mlc.c
 *
 *  Created on: Sep 28, 2021
 *      Author: ellerena
 */

/* ATTENTION: By default the driver is little endian. If you need switch
 *            to big endian please see "Endianness definitions" in the
 *            header file of the driver (_reg.h).
 */

/* Includes ------------------------------------------------------------------*/
#include "string.h"
#include "mis_logger.h"
#include "lsm6dsxx_platform.h"

#include "myucf.h"
#define MLCTST myucf

#define proc_setup proc_setup_mlc
#define proc_check proc_check_mlc

/* Private variables ---------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

void proc_i2c_tst (int loops)
{
#define TSTSZ 4
  struct
  {
    uint8_t add; // register address to test
    uint8_t val; // value to be read/written
    char acc; // access type: 0:r, 1:r/w
    uint8_t pad;
  } data[TSTSZ] =
    {
      { SENUPP(X_OFS_USR), 0, 'w', 0 },
        { SENUPP(Y_OFS_USR), 0, 'w', 0 },
        { SENUPP(Z_OFS_USR), 0, 'w', 0 },
        { SEN_ID_REG, SEN_ID, 'r', 0 }
      };

  platform_puts("Test Start..." CRLF);

  for (int k = 0; k < loops; k++) // main loop iteration
  {
    for (int i = 0; i < TSTSZ; i++) // iterate each element of test package
    {
      uint8_t regval;
      int ret;

      /* check test type and run accordingly */
      if ('r' == data[i].acc) // read test
      {
        ret = platform_read(NULL, data[i].add, &regval, 1);
        if (ret || (regval != data[i].val)) // verify read value is expected
        {
          PRNF("%5u [%02x:%02x] r%d %d" CRLF, k, data[i].add, regval, ret,
          __LINE__); // r: read test failed
        }
      }
      else // write/read test
      {
        // write new value
        ret = platform_write(NULL, data[i].add, (uint8_t*) &k, 1);

        // check write error
        if (0 == ret)
        {
          // read back value
          ret = platform_read(NULL, data[i].add, &regval, 1);

          // check read errors
          if (ret || (regval != (0xff & k)))
          {
            // wr: write ok, but read back failed
            PRNF("%5u [%02x:%02x] wr%d %d" CRLF, k, data[i].add, regval, ret,
            __LINE__);
          }
        }
        else
        { // w: attempt to write returned fail
          PRNF("%5u [%02x:%02x] w%d %d" CRLF, k, data[i].add, k, ret, __LINE__);
        }
      }

      platform_delay(5);
    }
  }

  platform_puts("... Test completed" CRLF);
}

/**
 * @brief Setup the IMU module for our use.
 *         - configure required registers
 * */
void proc_setup_mlc (void * data, int len)
{
  /* Init test platform */
  platform_init();
  platform_puts("Init complete" CRLF);
  platform_delay(LSM6_BOOT_TIME_MS); // wait sensor boot time
  platform_puts("Boot complete" CRLF);

  /* Check device ID */
  uint8_t regval;
  platform_read(NULL, SEN_ID_REG, &regval, 1);
  platform_prntht("I'm ", regval, CRLF);
  if (regval != SEN_ID)
    return;

  // perform a SW reset of the sensor
  regval = 1; // reset bit
  platform_write(NULL, SENUPP(CTRL3_C), &regval, 1);
  do
  {
    platform_read(NULL, SENUPP(CTRL3_C), &regval, 1);
  }
  while (regval & 1);

  // load the initial values (ucf)
  ucf_line_t *pval = (ucf_line_t*) MLCTST;
  int conf_num = sizeof(MLCTST) / sizeof(ucf_line_t);

  while (conf_num--)
  {
    platform_write(NULL, pval->address, (uint8_t*) &pval->data, 1);

    uint8_t readval;
    platform_read(NULL, pval->address, &readval, 1);
    platform_prntht("A:x", pval->address, CRLF);

    pval++;
    platform_delay(5); // settle time
  }

  SENLOW(int_notification_set)(&dev_ctx, SENUPP(ALL_INT_LATCHED));
}

/**
 * @brief This function is called when a tilt is detected
 *        following and irqB (int1) detection event
 *        It reads back gyro and accel from imu
 *        and checks any event flag activated.
 * */
void proc_check (void)
{
  // unlock access to embedded function bank
  uint8_t lock = 0x80; // bit7 locks/unlocks
  int ret = platform_write(NULL, SENUPP(FUNC_CFG_ACCESS), &lock, 1);

  if (0 == ret)
  {
    // read the new register values
    int8_t mlc_val;
    uint8_t st;

    platform_read(NULL, SENUPP(MLC0_SRC), (uint8_t*) &mlc_val, 1);
    platform_read(NULL, SENUPP(MLC_STATUS), &st, 1);

    // reinstate lock access to embedded function bank
    lock = 0;
    platform_write(NULL, SENUPP(FUNC_CFG_ACCESS), &lock, 1);

    // present data
    char msg[21];
    sprintf(msg, "mlc:%02x st:%02x" CRLF, mlc_val, st);
    platform_puts(msg);
  }
  else
  {
    platform_puts("spi error" CRLF);
  }
}
