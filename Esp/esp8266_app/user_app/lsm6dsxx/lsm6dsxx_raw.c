/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file lsm6dsxx_raw.c
 * @date 09.21.2021
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#include "esp_log.h"
#include "string.h"
#include <stdbool.h>
#include "lsm6dsxx_platform.h"
#include "dataout.h"

/************************************ Local Macro Definitions ************************************/
#define proc_setup proc_setup_raw
#define proc_check proc_check_raw

#define WTM_LEV (2)
#define WTM_LEV_L ((WTM_LEV) & 0xff)
#define WTM_LEV_M (((WTM_LEV) >> 8) & 0xff)

#define SECS_TO_SAMPLES(sec, odr) ((odr) * (sec))
#define SAMPLES_TO_FIFO_WORDS(samples) (2 * (samples))
#define FIFO_WORDS_TO_BYTES(fifo_words) (7 * (fifo_words))
#define ACC_RUNNING_SIZE (2*4) // size of the running sampling array
#define ACC_BLIND_SAMPLE_COUNT (12) // samples to ignore following a true event detected
#define ACC_THRESHOLD_DELTA (2000) // threshold level at which sample processing starts
#define FIFO_WORD_SIZE_BYTES (7)
#define RAW_DC_TS_DELTA_BITS (12)
#define RAW_DC_TS_DELTA_MASK ((1 << RAW_DC_TS_DELTA_BITS) - 1)
#define SAMPLES_FOR_CALIBRATION (12) // how may initial samples will be used for calibration
#define TAG "LSM6"
#define DEFAULT_EVAL_AXIS (AXIS_Z)
#define PACKET_RAW_DATA (1)
#define PACKET_EVENT_COUNT (2)

/************************************ Local Type Definitions  ************************************/
typedef enum
{
  IDLE = 0,
  CALIBRATE,
  SEARCH,
  BLIND,
  SAMPLE,
  HIT,
  STATE_COUNT // must not exceed 8 to comply with ble transport protocol
} state_machine_t;

typedef enum
{
  AXIS_X = 0,
  AXIS_Y,
  AXIS_Z,
  NUM_AXIS
} axis_num_t;

typedef struct
 {
  uint8_t axis_x:1; // include this axis in the detection
  uint8_t axis_y:1; // include this axis in the detection
  uint8_t axis_z:1; // include this axis in the detection
  uint8_t reset_count:1; // reset the internal event counter
  uint8_t use_raw_samples:1; // transmit each raw sample, otherwise only count is sent
} operation_mode_flags_t;

typedef union
{
  operation_mode_flags_t flags;
  uint8_t value;
} operation_mode_t;

typedef struct __attribute__((packed))
{
  uint8_t tag;
  union data_u
  {
    int16_t val16[3];
    uint8_t val8[6];
  } data;
} fifo_word_t;

typedef struct __attribute__((packed))
{
  uint8_t tag;
  uint32_t ts;
  uint8_t bdr1;
  uint8_t bdr2;
} fifo_word_ts_t;

typedef struct __attribute__((packed))
{
  uint32_t packet_type: 4; // constant tag to identify the type of data in the packet
  uint32_t event_count: 8;
  uint32_t ts_delta: RAW_DC_TS_DELTA_BITS; // up to 102400us
  uint32_t sample1_state: 3;
  uint32_t sample2_state: 3;
} raw_dc_params_t;

typedef struct __attribute__((packed))
{
  raw_dc_params_t dc_params;
  uint32_t ts;
  int16_t acc_s1[3];
  int16_t acc_s2[3];
} raw_dc_pack_t;

typedef struct
{
  int32_t sum[NUM_AXIS];
  int16_t offset[NUM_AXIS];
  int16_t threshold[NUM_AXIS];
  uint8_t samples;
} sensor_calibration_values_t;

typedef struct
{
  int32_t running_sample[ACC_RUNNING_SIZE];
  sensor_calibration_values_t calibrate;
  uint8_t running_offset;
} acc_params_t;

/*********************************** Local Constant Definitions **********************************/
static const conf_data_t cnf_data[] =
  {
    { SENUPP(CTRL9_XL), 0xe2 }, // Disable I3C interface
      { SENUPP(I3C_BUS_AVB), 0x00 },
      { SENUPP(CTRL10_C), 0x20 }, // Enable timestamp counter
      { SENUPP(CTRL1_XL), 0x32 }, // XL ODR=26Hz, FS=2g, 2nd stage LPF2
      { SENUPP(CTRL2_G), 0x2c }, // Gyro ODR=26Hz, FS=2000dps
      { SENUPP(CTRL8_XL), 0x80 }, // Accelerometer LP filter, ODR div/100

    /* set watermark threshold, this covers 2 registers.
     * for now we don't use compression so 2nd register is mostly
     * unused */
      { SENUPP(FIFO_CTRL1), WTM_LEV_L }, // LSB of watermark
      { SENUPP(FIFO_CTRL2), WTM_LEV_M }, // MSB of watermark

    /* set fifo sample writing rate for acc and gyro */
      { SENUPP(FIFO_CTRL3), 0x03 }, /* 52Hz for ACC, no Gyro */

    /* set timestamp batch data rate and fifo mode== */
      { SENUPP(FIFO_CTRL4), 0x00 }, /* clear fifo */
      { SENUPP(FIFO_CTRL4), 0x46 }, /* ts each sample, fifo continuous */

    /* enable irq on reaching watermark */
      { SENUPP(INT1_CTRL), 0x08 } /* INT1_FIFO_TH */
  };

/********************************** Global Variable Declarations *********************************/

/********************************** Local Variable Declarations **********************************/
static operation_mode_t operation_mode = {0};
static uint8_t event_count = 0;
static axis_num_t eval_axis = DEFAULT_EVAL_AXIS;
static state_machine_t state_machine = IDLE;
static int16_t blind_sample_counter = 0;
static fifo_word_t ffw_buffer[WTM_LEV] = {0}; // we expect only 2 fifo words
static acc_params_t acc_params = {0};
static raw_dc_pack_t raw_dc_pack = {0};
static fifo_word_ts_t * ffw_ts = (fifo_word_ts_t *)&ffw_buffer[0]; // pointer to timestamp fifo word
static fifo_word_t * ffw_acc = (fifo_word_t *)&ffw_buffer[1]; // pointer to accelerometer fifo word

/********************************** Local Function Declarations **********************************/
static void calibrate_running_parameters(fifo_word_t * ffw);
static bool true_event_is_detected(void);
static void sample_storing_and_classification(fifo_word_t * ffw, axis_num_t axis);
static bool is_first_valid_sample(fifo_word_t * ffw, axis_num_t axis);
static void pack_and_ship_raw_dc(void);
static void pack_and_ship_event_count(void);

/********************************** Public Function Declarations *********************************/
/**
 * @brief Setup the IMU module for our use.
 * */
void proc_setup (void * data, int len)
{
  if (len) // if configuration data was received, process it.
  {
    operation_mode.value = *(uint8_t *)data;

    // select the evaluation axis from the received selection.
    eval_axis = operation_mode.flags.axis_x ? AXIS_X
              : operation_mode.flags.axis_y ? AXIS_Y
              : operation_mode.flags.axis_z ? AXIS_Z : DEFAULT_EVAL_AXIS;
  }

  state_machine = IDLE;
  operation_mode.value = 0;
  event_count = 0;
  /* Init test platform */
  platform_init();
  platform_puts(TAG " Init complete\n");
  platform_delay(LSM6_BOOT_TIME_MS); // wait sensor boot time
  platform_puts(TAG "Boot complete\n");

  /* Check device ID */
  uint8_t regval;
  platform_read(NULL, SEN_ID_REG, &regval, 1);
  platform_prntht(TAG " I'm 0x", regval, "\n" );
  if (regval != SEN_ID)
    return;

  /* perform a SW reset of the sensor */
  regval = 1; // reset bit
  platform_write(NULL, SENUPP(CTRL3_C), &regval, 1);
  do
  {
    platform_read(NULL, SENUPP(CTRL3_C), &regval, 1);
  }
  while (regval & 1);

  /* configure IMU module via i2c, write registers */
  const conf_data_t *pval = cnf_data;
  int conf_num = sizeof(cnf_data) / sizeof(conf_data_t);

  while (conf_num--)
  {
    platform_write(NULL, pval->address, (uint8_t*) &pval->data, 1);
    pval++;
  }

  platform_delay(500);

  // initialize the calibratrion samples counter
  memset(&acc_params, 0, sizeof acc_params);
  state_machine = CALIBRATE;
}

/**
 * @brief This function is called when a event is detected
 */
void proc_check (void)
{
  // read the status regs
  uint16_t sta;
  platform_read(NULL, SENUPP(FIFO_STATUS1), (uint8_t*) &sta, 2);

  // reject any intermittent requests that may occur as we pull the data in
  uint16_t len = sta & 0x3ff;
  if (len < WTM_LEV)
  {
    platform_prntht(TAG " unexpected: 0x", sta, "\n");
    return;
  }

  if (len > WTM_LEV)
  {
    len = WTM_LEV;
  }

  /* pull all the samples at once */
  platform_read(NULL, SENUPP(FIFO_DATA_OUT_TAG), (uint8_t *)ffw_buffer, len * FIFO_WORD_SIZE_BYTES);

  switch (state_machine)
  {
    case CALIBRATE:
    {
      /* if the process is starting, the first SAMPLES_FOR_CALIBRATION samples are only
      used for calibration purposes */
      calibrate_running_parameters(ffw_acc);
      if (acc_params.calibrate.samples >= SAMPLES_FOR_CALIBRATION)
      {
        for (int i = 0; i < ACC_RUNNING_SIZE; ++i)
        {
          acc_params.running_sample[i] = acc_params.calibrate.offset[eval_axis];
        }

        platform_prntht(TAG " offset: ", acc_params.calibrate.offset[eval_axis], "\n");

        state_machine = SEARCH;
      }
    }
      break;

    case SEARCH:
    {
      if (is_first_valid_sample(ffw_acc, eval_axis))
      {
        state_machine = SAMPLE;
      }
    }
      break;

    case BLIND:
    {
      /* if a true event has been recently detected, ignore for a reasonable time
      the new samples as a mean to avoid false positives due to bouncing */
      if (--blind_sample_counter <= 0)
      {
        for (int i = 0; i < ACC_RUNNING_SIZE; ++i)
        {
          acc_params.running_sample[i] = acc_params.calibrate.offset[eval_axis];
        }
        state_machine = SEARCH;
      }
    }
      break;

    case SAMPLE:
    {
      sample_storing_and_classification(ffw_acc, eval_axis);

      if (true_event_is_detected())
      {
        SENLOW(fifo_data_out_tag_t) * fifo_tag = (SENLOW(fifo_data_out_tag_t) *)ffw_ts;
        if (SENUPP(TIMESTAMP_TAG) == fifo_tag->tag_sensor)
        {
          uint32_t timestamp = ffw_ts->ts;

          ESP_LOGI(TAG,
                    "%3d %10lu00 %02x %02x %6d %6d %6d",
                    event_count,
                    timestamp/4, // timestamp in usec
                    ((fifo_word_t*)ffw_ts)->tag,
                    ffw_acc->tag,
                    ffw_acc->data.val16[AXIS_X],
                    ffw_acc->data.val16[AXIS_Y],
                    ffw_acc->data.val16[AXIS_Z]
                    );
        }

        // and restart a new whole detection cycle
        blind_sample_counter = ACC_BLIND_SAMPLE_COUNT;
        state_machine = HIT;
      }
    }
      break;

    default:
      return;
  }

  if (HIT == state_machine && !operation_mode.flags.use_raw_samples)
  {
    pack_and_ship_event_count();
  }
  else if (operation_mode.flags.use_raw_samples)
  {
    pack_and_ship_raw_dc();
  }

  if (HIT == state_machine)
  {
    state_machine = BLIND;
  }
}

/*********************************** Local Function Definitions **********************************/
static void calibrate_running_parameters(fifo_word_t * ffw)
{
  acc_params.calibrate.samples++;

  for (int i = 0; i < NUM_AXIS; ++i)
  {
    acc_params.calibrate.sum[i] += ffw->data.val16[i];
    acc_params.calibrate.offset[i] = (int16_t)(acc_params.calibrate.sum[i]/acc_params.calibrate.samples);
  }
}

static void sample_storing_and_classification(fifo_word_t * ffw, axis_num_t axis)
{
  acc_params.running_sample[acc_params.running_offset++] = ffw->data.val16[axis];
  if (acc_params.running_offset >= ACC_RUNNING_SIZE)
  {
    acc_params.running_offset = 0;
  }
}

static bool true_event_is_detected(void)
{
  int i = 0;
  int j = acc_params.running_offset;

  do
  {
    if (acc_params.running_sample[j++] < acc_params.calibrate.offset[eval_axis])
    {
      return false;
    }
    if (j >= ACC_RUNNING_SIZE)
    {
      j = 0;
    }

  } while(++i < (ACC_RUNNING_SIZE/2));

  do
  {
    if (acc_params.running_sample[j++] >= acc_params.calibrate.offset[eval_axis])
    {
      return false;
    }
    if (j >= ACC_RUNNING_SIZE)
    {
      j = 0;
    }
  } while(++i < (ACC_RUNNING_SIZE/2));

  // landing here a true event has been detected
  event_count++;

  return true;
}

static bool is_first_valid_sample(fifo_word_t * ffw, axis_num_t axis)
{
  return ffw->data.val16[axis] > (acc_params.calibrate.offset[axis] + ACC_THRESHOLD_DELTA);
}

static void pack_and_ship_raw_dc(void)
{
  static bool is_pack_complete = false;

  if (!is_pack_complete)
  { // pack the sample 1 data
    memcpy(raw_dc_pack.acc_s1, &ffw_acc->data, sizeof raw_dc_pack.acc_s1);
    raw_dc_pack.ts = ffw_ts->ts;
    raw_dc_pack.dc_params.sample1_state = state_machine;
    is_pack_complete = true;
  }
  else
  { // pack the sample 2 data
    memcpy(raw_dc_pack.acc_s2, &ffw_acc->data, sizeof raw_dc_pack.acc_s2);
    raw_dc_pack.dc_params.ts_delta = (ffw_ts->ts - raw_dc_pack.ts) & RAW_DC_TS_DELTA_MASK;
    raw_dc_pack.dc_params.sample2_state = state_machine;
    raw_dc_pack.dc_params.packet_type = PACKET_RAW_DATA;

    // transmit the packet to BLE central
    dataout_proc_queue(&raw_dc_pack, sizeof raw_dc_pack, F_DOUT_BLE);

    is_pack_complete = false;
  }
}

static void pack_and_ship_event_count(void)
{
  raw_dc_pack.ts = ffw_ts->ts;
  raw_dc_pack.dc_params.event_count = event_count;
  raw_dc_pack.dc_params.packet_type = PACKET_EVENT_COUNT;

  // transmit the packet to BLE central
  dataout_proc_queue(&raw_dc_pack, sizeof raw_dc_pack.dc_params + sizeof raw_dc_pack.ts , F_DOUT_BLE);
}
