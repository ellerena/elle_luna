/*
 * lsm6dsxx.h
 *
 *  Created on: Oct 20, 2021
 *      Author: ellerena
 */

#ifndef USER_APP_LSM6DSXX_LSM6DSXX_H_
#define USER_APP_LSM6DSXX_LSM6DSXX_H_

#ifdef __cplusplus
extern "C"
{
#endif

// #define LSM6DSRX // << select sensor part =================//

#ifdef LSM6DSRX
#warning "*** Using LSM6DSRX ***"
#include "lsm6dsrx_reg.h"
#define SENLOW(x) lsm6dsrx_##x
#define SENUPP(x) LSM6DSRX_##x
#elif defined(LSM6DSR)
#warning "*** Using LSM6DSR ***"
#include "lsm6dsr_reg.h"
#define SENLOW(x) lsm6dsr_##x
#define SENUPP(x) LSM6DSR_##x
#else
#warning "*** Using LSM6DSOX ***"
#include "lsm6dsox_reg.h"
#define SENLOW(x) lsm6dsox_##x
#define SENUPP(x) LSM6DSOX_##x
#endif

#define SEN_ID SENUPP(ID)
#define SEN_ID_REG SENUPP(WHO_AM_I)
#define LSM6_BOOT_TIME_MS (10) // ms

typedef struct
{
  uint8_t address;
  uint8_t data;
} conf_data_t;

#ifdef __cplusplus
}
#endif

#endif /* USER_APP_LSM6DSXX_LSM6DSXX_H_ */
