/*
 * command_handlers.c
 *
 *  Created on: Jun 17, 2020
 *      Author: Eddie Llerena
 */

#include "cli_tools.h"
#include "mis_gflags.h"

void CommandProcessor (char * buf)
{
//   display_puts(buf);

  char task = TASK| 0x20; /* simple conversion to lowercase */
  char mode = MODE | 0x20; /* simple conversion to lowercase */
  int n = ParseCharsToValueHex(&ARGS);

  switch (task)
  {
    CLI_SELECT('w', cli_ws2812); /* ws2812 module */

#ifndef ESP8266_WS2812
    CLI_SELECT('s', cli_sys); /* system operations */

//    CLI_SELECT('m', cli_mem); /* memory access operations */

    CLI_SELECT('i', cli_i2c); /* i2c commands */

    CLI_SELECT('o', cli_ssd1306); /* oled display operations */

    CLI_SELECT('g', cli_lsm6dsxx); /* lsm6dsxx imu sensor */

    CLI_SELECT('x', cli_isl29125); /* isl29125 RGB sensor operations */

    CLI_SELECT('l', cli_led); /* led operations */

    CLI_SELECT('t', cli_mlx90614); /* mlx90614 temperature sensor */

    CLI_SELECT('q', cli_qmc5883l); /* qmc5883l magnetometer sensor */

//      CLI_SELECT('9', cli_sx9200); /* sx9200 capacitive touch sensor */
//
//    CLI_SELECT('1', cli_1w); /* 1-wire and DTH11 operations */
//
//    CLI_SELECT('c', cli_spi); /* communications, spi operations */
//
//    CLI_SELECT('u', cli_ftdi); /* USB (FTDI) operations */
//
//    CLI_SELECT('f', cli_spisniffer); /* SPI Sniffer operations */
#endif

    default:
    {
      GFS(F_COMMERR);
    }
    break;
  }
}

