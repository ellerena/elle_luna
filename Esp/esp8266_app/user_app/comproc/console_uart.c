/*
 * os_uart.c
 *
 *  Created on: Sep 13, 2021
 *      Author: ellerena
 */

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "comproc.h"

#define BUF_SIZE (90)

static QueueHandle_t uart0_queue;

static void uart_event_task (void * pvParameters)
{
  uart_event_t event;
  uint8_t *buf = (uint8_t*) malloc(BUF_SIZE);
  uint8_t *pin = buf;

  for (;;)
  {
    if (xQueueReceive(uart0_queue,
                      (void*) &event,
                      (portTickType) portMAX_DELAY))
    {
      switch (event.type)
      {
        case UART_DATA:
          uart_read_bytes(UART_NUM_0, pin, event.size, portMAX_DELAY);
          if (pin[event.size - 1] == '\r')
          {
            pin[event.size - 1] = '\0';
            pin = buf;
            command_proc_queue((void*) pin);
          }
          else
          {
            pin += event.size;
          }
          break;

        case UART_FIFO_OVF:
        case UART_BUFFER_FULL:
          uart_flush_input(UART_NUM_0);
          xQueueReset(uart0_queue);
          break;

        case UART_PARITY_ERR:
        case UART_FRAME_ERR:
        default:
          break;
      }
    }
  }

  free(buf);
  buf = NULL;
  vTaskDelete(NULL);
}

int console_proc_start (void)
{
  int ret = uart_driver_install(UART_NUM_0,
                                129,
                                0,
                                10,
                                &uart0_queue,
                                0);

  if (ESP_OK == ret)
  {
    ret = xTaskCreate(uart_event_task, "uart_event_task", 1024, NULL, 2, NULL);
    ret = (ret == pdPASS) ? 0 : -1;
  }

  return ret;
}
