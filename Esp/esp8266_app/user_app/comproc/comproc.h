/*
 * comproc.h
 *
 *  Created on: Oct 15, 2021
 *      Author: ellerena
 */

#ifndef USER_APP_INC_COMPROC_H_
#define USER_APP_INC_COMPROC_H_

extern int command_proc_start (void);
extern void command_proc_end (void);
extern int command_proc_queue (void * request);
extern int console_proc_start (void);

#endif /* USER_APP_INC_COMPROC_H_ */
