/*
 * comproc.c
 *
 *  Created on: Oct 15, 2021
 *      Author: ellerena
 */

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_err.h"
#include "command_handlers.h"

#define QSZ 10

static QueueHandle_t comproc_Q = NULL;
static TaskHandle_t comproc_T = NULL;

static void comproc_task (void * args)
{
  void *request;

  for (;;)
  {
    if (pdTRUE == xQueueReceive(comproc_Q, &request, portMAX_DELAY)) //Q_MAXWAITMS))
    {
      CommandProcessor((char*) request);
    }
  }
}

/* external API */
int command_proc_start (void)
{
  int result = ESP_FAIL;

  comproc_Q = xQueueCreate(QSZ, sizeof(void*));

  if (NULL != comproc_Q)
  {
    result = xTaskCreate(comproc_task,
                         "comproc_task",
                         2048,
                         NULL,
                         10,
                         &comproc_T);
    if (pdPASS == result)
    {
      result = ESP_OK;
    }
  }

  return result;
}

void command_proc_end (void)
{
  vTaskDelete(comproc_T);
}

int command_proc_queue (void * request)
{
  int result = xQueueSend(comproc_Q, (void* )&(request), portMAX_DELAY);

  return pdTRUE == result ? ESP_OK : ESP_FAIL;
}

