/*
 * sock.c
 *
 *  Created on: May 28, 2020
 *      Author: eddie
 */

#include "freertos/FreeRTOS.h"
#include <lwip/sockets.h>
#include "esp_log.h"
#include "esp_err.h"
#include "mis_gflags.h"
#include "comproc.h"
#include "sock.h"
#include "ltv.h"

#ifdef ARCH_ESP32
#include "lwip/sys.h"
#endif

#define SSOCK_PRT (5005)
#define SOCK_BUF (512)
#define QSZ 20
#define TAG __FUNCTION__

static QueueHandle_t sock_tx_Q = NULL;
static int scon = -1;

char bux_rx[SOCK_BUF], *pbuf = bux_rx;

static void sock_rx_handler (void)
{
  for (;;)
  {
    // clear some socket related flags
    GFC(M_SOCKET);

    /* receive loop */
    int n;
    while ((n = recv(scon, bux_rx, SOCK_BUF - 1, 0)) > 0)
    {
      if (strstr(bux_rx, "HTTP"))
        GFS(F_HTTP);
      bux_rx[n] = '\0';
      printf("%d>%s\n", n, bux_rx);
      if (bux_rx[n - 1] <= '\n') /* end of msg */
        break;
    }

    /* received data processing */
    if (n)
    {
      if (GF(F_HTTP))
      {
        snprintf(bux_rx, SOCK_BUF, "HTTP/1.0 200 OK\r\n\r\nParis et Libere!");
        send(scon, bux_rx, (int )strlen(bux_rx), 0);
        GFS(F_ENDCON); /* flag to close the connection */
      }
      else if (bux_rx[0] == '.') /* end connection request */
      {
        GFS(F_ENDCON);
      }
      if (bux_rx[1] == '.') /* task end request */
      {
        GFS(F_ENDPROG + F_ENDCON); /* flag to close & exit program */
        break;
      }
      if (GF(F_ENDCON))
      {
        break;
      }

      command_proc_queue((void*) pbuf);
      sock_tx_proc_queue("Ok", 3, 0, 1); // 3rd byte ('\0') needed by protocol
    }
  }
}

static void sock_server_task (void * pvParameters)
{
  int ssrv = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);

  struct sockaddr_in saddr =
    {
      .sin_family = AF_INET,
      .sin_addr.s_addr = htonl(INADDR_ANY),
      .sin_port = htons(SSOCK_PRT) };

  bind(ssrv, (struct sockaddr* )&saddr, sizeof(saddr));
  listen(ssrv, 1);

  for (;;)
  {
    ESP_LOGI(TAG, "Waiting for client @ %d...\n", SSOCK_PRT);

    struct sockaddr_in caddr;
    socklen_t slen = sizeof(caddr);

    scon = accept(ssrv, (struct sockaddr* )&caddr, &slen);
    if (scon < 0)
      continue;

    inet_ntoa_r(caddr.sin_addr.s_addr, bux_rx, SOCK_BUF - 1);
    printf("Connected to %s\n", bux_rx);

    GFC(M_SOCKET);
    sock_rx_handler();
    closesocket(scon);
    scon = -1; // mark the socket as inactive

    if (GF(F_ENDPROG)) /* if program exit requested */
      break;
  }

  vTaskDelete(NULL);
}

static void sock_tx_task (void * pvParameters)
{
  tx_mem_t *pTx;

  for (;;)
  {
    if (pdTRUE == xQueueReceive(sock_tx_Q, (void*) &pTx, portMAX_DELAY))
    {
      if (scon >= 0)
      {
        send(scon, (void* )&pTx->lnk, pTx->len, 0);
        free(pTx);
      }
    }
  }
}

int sock_proc_start (void)
{
  int result = xTaskCreate(sock_server_task, "tcp_server", 2048, NULL, 6, NULL);

  if (pdPASS == result)
  {
    sock_tx_Q = xQueueCreate(QSZ, sizeof(void*));

    if (NULL != sock_tx_Q)
    {
      result = xTaskCreate(sock_tx_task, "sock_tx_task", 1024, NULL, 8, NULL);

      if (pdPASS == result)
      {
        return ESP_OK;
      }
    }
  }

  return ESP_FAIL;
}

int sock_tx_proc_queue (void * request, int len, uint8_t command, uint8_t flag)
{
  /* if queue is full skip and just return */
  if (1 > uxQueueSpacesAvailable(sock_tx_Q))
  {
    return ESP_ERR_NO_MEM;
  }

  tx_mem_t *pmem = (tx_mem_t*) malloc(TX_MEM_LEN(len));

  if (NULL == pmem)
  {
    return ESP_ERR_NO_MEM;
  }

  ltv_pack_response((void*) &pmem->lnk, request, len, command, flag);
  pmem->len = TX_LNK_LEN(len);

  /* add the msp index to our queue */
  return
      pdTRUE == xQueueSend(sock_tx_Q, (void*) &pmem, portMAX_DELAY) ? ESP_OK :
          ESP_FAIL;
}

