/*
 * isl29125_raw.c
 *
 *  Created on: Sep 28, 2021
 *      Author: ellerena
 */

/* ATTENTION: By default the driver is little endian. If you need switch
 *            to big endian please see "Endianness definitions" in the
 *            header file of the driver (_reg.h).
 */

/* Includes ------------------------------------------------------------------*/
#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"
#include "esp_err.h"

#include "drv_gpio.h"
#include "os_i2c.h"
#include "mis_logger.h"
#include "oledproc.h"
#include "sock.h"

#define ESP8266
#define SEN_ID (0x7d)
#define SEN_ADDR7 (0x44)
#define SEN_CONF1_INIT_VAL (SEN_CNF1_RES_16 + SEN_CNF1_RANGE_HIGH + SEN_CNF1_MODE_RGB)
#define SEN_CONF2_INIT_VAL (0)
#define SEN_CONF3_INIT_VAL (SEN_CNF3_CONV_TO_INT_ENA + SEN_CNF3_PERSIST_8)
#define BOOT_TIME 10 //ms

#ifdef __MICROBLAZE__
#define SLEEP_U(us) usleep_MB(us)
#define SLEEP_S(s) sleep_MB(s)
#define OP_STOP 0
#elif defined(ESP8266)
#define SLEEP_U(us)
#define SLEEP_S(s)
#else
#include "xiicps.h"
#define SLEEP_U(us) usleep_A9(us)
#define SLEEP_S(s) sleep_A9(s)
#define OP_STOP XIICPS_7_BIT_ADDR_OPTION
#endif

#define SEN_DEV_RST_REG (0x00)
#define SEN_DEV_RST_VAL (0x46)

/* Configuration-1 register definitions */
#define SEN_CNF1_RANGE_LOW (0)
#define SEN_CNF1_RANGE_HIGH (1 << 3)

#define SEN_CNF1_RES_12 (1 << 4)
#define SEN_CNF1_RES_16 (0)

#define SEN_CNF1_SYNC_0 (0)
#define SEN_CNF1_SYNC_1 (1 << 5)

/* Configuration-2 register definitions */
#define SEN_CNF2_IR_OFFSET_OFF (0)
#define SEN_CNF2_IR_OFFSET_ON (1 << 7)

/* Configuration-3 register definitions */
#define SEN_CNF3_PERSIST_1 (0)
#define SEN_CNF3_PERSIST_2 (1 << 2)
#define SEN_CNF3_PERSIST_4 (2 << 2)
#define SEN_CNF3_PERSIST_8 (3 << 2)

#define SEN_CNF3_CONV_TO_INT_DIS (0)
#define SEN_CNF3_CONV_TO_INT_ENA (1 << 4)

/* Status register definitions */
#define SEN_ST_RGBTHF (1 << 0)
#define SEN_ST_CONVERTED (1 << 1)
#define SEN_ST_BOUTF (1 << 2)
#define SEN_ST_RGBCF (3 <<4)

enum SEN_CNF3_INTSEL_VAL
{
    SEN_CNF3_INTSEL_NONE = 0,
    SEN_CNF3_INTSEL_GREEN, // 1
    SEN_CNF3_INTSEL_RED, // 2
    SEN_CNF3_INTSEL_BLUE // 3
};

enum SEN_CNF1_MODE_VAL
{
    SEN_CNF1_MODE_POW_DOWN = 0,
    SEN_CNF1_MODE_GREEN, // 1
    SEN_CNF1_MODE_RED, // 2
    SEN_CNF1_MODE_BLUE, // 3
    SEN_CNF1_MODE_STANDBY, // 4
    SEN_CNF1_MODE_RGB, // 5
    SEN_CNF1_MODE_RED_GREEN, // 6
    SEN_CNF1_MODE_GREEN_BLUE // 7
};

enum SensAddress
{
    SEN_ID_REG = 0,
    SEN_RESET_REG = 0,
    SEN_CNF1_REG, // 1
    SEN_CNF2_REG, // 2
    SEN_CNF3_REG, // 3
    SEN_LOWTRE_L_REG, // 4
    SEN_LOWTRE_M_REG, // 5
    SEN_HIGTRE_L_REG, // 6
    SEN_HIGTRE_M_REG, //7
    SEN_STATUS, // 8
    SEN_GREEN_L, // 9
    SEN_GREEN_H, // 10
    SEN_RED_L, // 11
    SEN_RED_H, // 12
    SEN_BLUE_L, // 13
    SEN_BLUE_H, // 14
    SEN_REG_COUNT
};

struct
{
  int8_t pad1[3];
  int8_t st;
  uint16_t g;
  uint16_t r;
  uint16_t b;
  int16_t pad2;
  uint32_t timestamp;
} raw;

static SemaphoreHandle_t xSemaphore = NULL;
static TaskHandle_t xTask = NULL;

/* Private variables ---------------------------------------------------------*/


/* Private functions ---------------------------------------------------------*/

/*
 * @brief  Write generic device register (platform dependent)
 *
 * @param  reg       register to write
 * @param  bufp      pointer to data to write in register reg
 * @param  len       number of consecutive register to write
 *
 */
static int32_t platform_write (uint8_t reg,
                               const uint8_t * bufp,
                               uint16_t len)
{
  uint32_t l = OS_i2c_master_wr_ar(bufp, len, SEN_ADDR7, &reg);
  return (l == len) ? 0 : -1;
}

/*
 * @brief  Read generic device register (platform dependent)
 *
 * @param  reg       register to read
 * @param  bufp      pointer to buffer that store the data read
 * @param  len       number of consecutive register to read
 *
 */
static int32_t platform_read (uint8_t reg,
                              uint8_t * bufp,
                              uint16_t len)
{
  // assume repeat start is off initially
  uint32_t l = OS_i2c_master_rd_ar(bufp, len, SEN_ADDR7, &reg);
  return (l == len) ? 0 : -1;;
}

/*
 * @brief  platform specific delay (platform dependent)
 *
 * @param  ms        delay in ms
 *
 */
static void platform_delay (uint32_t ms)
{
  vTaskDelay(ms / portTICK_PERIOD_MS);
}

/*
 * @brief  platform specific initialization (platform dependent)
 */
static void platform_init (void)
{
  // ensure we are in i2c STOP mode: each transaction ends in stop

  int stop_op;

  do
  {
    stop_op = OS_i2c_toggle_repeat_start();

  }
  while (1 != stop_op);
}

/**
 * @brief Setup the IMU module for our use.
 * */
void isl29125_proc_setup (void)
{
  /* Init test platform */
  platform_init();
  platform_delay(BOOT_TIME); // wait sensor boot time
  PRNS("platform initialized" CRLF);

  /* Check device ID */
  uint8_t regval[3] =
    { SEN_CONF1_INIT_VAL, SEN_CONF2_INIT_VAL, SEN_CONF3_INIT_VAL };

  platform_read(SEN_ID_REG, regval, 1);
  PRNTHT("I'm ", regval[0], CRLF);
  if (regval[0] != SEN_ID)
    return;

  // perform a SW reset of the sensor
  regval[0] = SEN_DEV_RST_VAL;
  platform_write(SEN_RESET_REG, regval, 1);
  platform_delay(BOOT_TIME);

  /* configure sensor registers as desired:
   * capture red, green and blue @ 16bits */
  regval[0] = (SEN_CNF1_RES_16 + SEN_CNF1_RANGE_HIGH + SEN_CNF1_MODE_RGB);
  platform_write(SEN_CNF1_REG, regval, 3);
}

/**
 * @brief This function is called when a tilt is detected
 *        following and irqB (int1) tilt detection event
 *        It reads back gyro and accel from imu
 *        and checks if the tilt detected flag
 *        is active.
 * */
/* Check if Tilt event */
static void proc_check (void)
{
  /* read the new register values, read status at last since
   * reading it resumes conversion immediately */
  platform_read(SEN_GREEN_L, (uint8_t*) &raw.g, 3 * sizeof(raw.g));
  platform_read(SEN_STATUS, (uint8_t*) &raw.st, 1);

  /* only present data if different than previous reading */
  static uint8_t prev[3 * sizeof(raw.g)] =
    { 0 };

  if (0 != memcmp(prev, &raw.g, 3 * sizeof(raw.g)))
  {
    // present the data captured
    char msg[24];
    sprintf(msg, "rgb %02x %4x %4x %4x" CRLF, raw.st, raw.r, raw.g, raw.b);
    sock_tx_proc_queue(msg, strlen(msg), 0, 1);
    oled_proc_queue((void*) msg);
    PRNS(msg);

    // update previous values for future reference
    memcpy(prev, &raw.g, 3 * sizeof(raw.g));
  }
}

/* -------- task process -------- */
static void raw_isr_handler (void * arg)
{
  int xHigherPriorityTaskWoken = pdFALSE;

  xSemaphoreGiveFromISR(xSemaphore, &xHigherPriorityTaskWoken);
}

static void raw_task_handler (void * arg)
{
  for (;;)
  {
    if ( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE)
    {
      proc_check();
    }
  }
}

/**
 * @brief external API to enable/disable tilt detection
 *        every time it is called, it toggles on/off
 *        tilt detection
 * */
int isl29125_raw_proc_start (void)
{
  /* if task exist, then end the whole process */
  if (xTask)
  {
    gpio_isr_handler_remove(NEGEDGE_PIN_NUM);
    gpio_uninstall_isr_service();
    vTaskDelete(xTask);
    xTask = NULL;
    PRNS("lux process ended\n");
    return 0;
  }

  /* setup/configure the process */
  // proc_setup();

  if (NULL == xSemaphore)
  {
    xSemaphore = xSemaphoreCreateBinary();
  }

  int result = ESP_FAIL;
  if (NULL != xSemaphore)
  {
    result = xTaskCreate(raw_task_handler,
                         "raw_task_handler",
                         2048,
                         NULL,
                         10,
                         &xTask);

    if (pdPASS == result)
    {
      //install gpio isr service
      gpio_install_isr_service(0);
      result = gpio_isr_handler_add(NEGEDGE_PIN_NUM,
                                    raw_isr_handler,
                                    (void*) NULL);
    }
  }

  PRNTHT("lux process: ", result, "\n");
  return result;
}
