/*
 * isl29125.h
 *
 *  Created on: Nov 14, 2021
 *      Author: ellerena
 */

#ifndef USER_APP_ISL29125_ISL29125_H_
#define USER_APP_ISL29125_ISL29125_H_

#ifdef __cplusplus
extern "C"
{
#endif

extern int isl29125_raw_proc_start (void);
extern void isl29125_proc_setup (void);

#ifdef __cplusplus
}
#endif

#endif /* USER_APP_ISL29125_ISL29125_H_ */
