/*
 * lm75a.c
 *
 *  Created on: Nov 21, 2021
 *      Author: ellerena
 */

#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"
#include "driver/gpio.h"
#include "drv_gpio.h"
#include "os_i2c.h"
#include "mis_logger.h"
#include "oledproc.h"
#include "sock.h"

#define SEN_ADDR7 (0x4b)

/* register addresses */
enum MAG_REG_ADDRESSES
{
    R_TEMPERATURE = 0,
    R_CONFIGURATION, // 1
    R_T_HYST, // 2
    R_T_OS, // 3
    R_PRODUCT_ID = 7, // 7
};

#define CONFIG_SHUTDOWN (1 << 0)
#define CONFIG_IRQ_MODE (1 << 1)
#define CONFIG_ACTIVE_HIGH (1 << 2)
#define CONFIG_FAULT_QUEUE_1 (0 << 3)
#define CONFIG_FAULT_QUEUE_2 (1 << 3)
#define CONFIG_FAULT_QUEUE_4 (2 << 3)
#define CONFIG_FAULT_QUEUE_8 (3 << 3)

#define DATAWR(x) (x << 7)
#define DATARD(x) (x >> 7)

struct
{
  int16_t temp;
  uint32_t timestamp;
} raw;

static SemaphoreHandle_t xSemaphore = NULL;
static TaskHandle_t xTask = NULL;

/*
 * @brief  Read generic device register (platform dependent)
 */
static int platform_reg_rd (uint8_t reg, uint8_t * val, uint32_t len)
{
  uint32_t l = OS_i2c_master_rd_ar(val, len, SEN_ADDR7, &reg);

  return l == len ? ESP_OK : ESP_FAIL;
}

/*
 * @brief  Write generic device register (platform dependent)
 */
static int platform_reg_wr (uint8_t reg, uint8_t * val, uint32_t len)
{
  uint32_t l = OS_i2c_master_wr_ar(val, len, SEN_ADDR7, &reg);

  return l == len ? ESP_OK : ESP_FAIL;
}

/*
 * @brief  platform specific delay (platform dependent)
 *
 * @param  ms        delay in ms
 */
static void platform_delay (uint32_t ms)
{
  vTaskDelay(ms / portTICK_PERIOD_MS);
}

/*
 * @brief  platform specific initialization (platform dependent)
 */
static void platform_init (void)
{
  // Enable i2c STOP mode"

  int stop_op;
  do
  {
    stop_op = OS_i2c_toggle_repeat_start();
  }
  while (1 != stop_op);
}

static void proc_setup (void)
{
  /* Init test platform */
  platform_init();

  uint16_t regval = DATAWR(19); // set Thist
  platform_reg_wr(R_T_HYST, (uint8_t*) &regval, 2);

  regval = DATAWR(32); // set Tos
  platform_reg_wr(R_T_OS, (uint8_t*) &regval, 2);

  regval = CONFIG_IRQ_MODE + CONFIG_ACTIVE_HIGH + CONFIG_FAULT_QUEUE_4; // set configuration
  platform_reg_wr(R_CONFIGURATION, (uint8_t*) &regval, 1);

  platform_delay(5); // delay to allow reset complete
}

static void proc_check (void)
{
#define MG(x) ((((int)DATAWR(x)))/2)

  /* read back data from the sensor */
  uint16_t regval;
  platform_reg_rd(R_TEMPERATURE, (uint8_t*) &regval, sizeof(regval));

  /* data processing/conversion */
  raw.temp = MG(regval); // get converted value

  /* only present data if different than previous reading */
  static int16_t prev = 0;

  if (prev != raw.temp)
  {
    /* present data */
    char msg[23];
    sprintf(msg, "lm75a %4d" CRLF, raw.temp);
    sock_tx_proc_queue(msg, strlen(msg), 0, 1);
    oled_proc_queue((void*) msg);
    PRNS(msg);

    // update previous values for future reference
    prev = raw.temp;
  }
}

/* -------- task process -------- */
static void lm75a_isr_handler (void * arg)
{
  int xHigherPriorityTaskWoken = pdFALSE;

  xSemaphoreGiveFromISR(xSemaphore, &xHigherPriorityTaskWoken);
}

static void lm75a_task_handler (void * arg)
{
  for (;;)
  {
    if (xSemaphoreTake(xSemaphore, portMAX_DELAY) == pdTRUE)
    {
      proc_check();
    }
  }
}

/**
 * @brief external API to enable/disable tilt detection
 *        every time it is called, it toggles on/off
 *        tilt detection
 * */
int lm75a_raw_proc_start (void)
{
  /* if task exist, then end the whole process */
  if (xTask)
  {
    gpio_isr_handler_remove(POSEDGE_PIN_NUM);
    gpio_uninstall_isr_service();
    vTaskDelete(xTask);
    xTask = NULL;
    platform_init();
    uint8_t regval = CONFIG_SHUTDOWN; // shutdown device
    platform_reg_wr(R_CONFIGURATION, &regval, 1);

    PRNS("lm75a ended\n");
    return 0;
  }

  if (NULL == xSemaphore)
  {
    xSemaphore = xSemaphoreCreateBinary();
  }

  int result = ESP_FAIL;
  if (NULL != xSemaphore)
  {
    result = xTaskCreate(lm75a_task_handler,
                         "lm75a_task_handler",
                         1024,
                         NULL,
                         10,
                         &xTask);

    if (pdPASS == result)
    {
      //install gpio isr service
      gpio_install_isr_service(0);
      result = gpio_isr_handler_add(POSEDGE_PIN_NUM,
                                    lm75a_isr_handler,
                                    (void*) NULL);
    }
  }

  proc_setup();

  PRNTHT("lm75a::", result, "\n");

  return result;
}
