/*
 * lm75a.h
 *
 *  Created on: Nov 21, 2021
 *      Author: ellerena
 */

#ifndef USER_APP_LM75A_LM75A_H_
#define USER_APP_LM75A_LM75A_H_


extern int lm75a_raw_proc_start (void);


#endif /* USER_APP_LM75A_LM75A_H_ */
