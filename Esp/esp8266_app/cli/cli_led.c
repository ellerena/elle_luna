/*
 * cli_led.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include "os_led.h"
#include "mis_gflags.h"

void cli_led (char task, void * args, int len)
{
  unsigned char *pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'i': /* li: led initialize) */
    {
      OS_led_init();
    }
      break;
    case 'l': /* ll<xx>: set led level to xx (00..0f) */
    {
      OS_led_bright(0, pArgs[0]);
    }
      break;
    case 'c': /* lc<xx>: led cycle at rate xx */
    {
      OS_led_blink_speed(pArgs[0]);
    }
      break;
    case 'u': /* lu<xx>: led fade up to level xx */
    case 'd': /* ld<xx>: led fade down to level xx */
    case 'x': /* lx<xx><yy>: fade up to xx then down to yy */

    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}

