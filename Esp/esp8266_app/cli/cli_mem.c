/*
 * cli_mem.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include <ll_regs.h>
#include <mis_gflags.h>
#include <mis_logger.h>

void cli_mem (char task, void *args, int len)
{
  unsigned char * pArgs = (unsigned char*) args;

  unsigned temp = REV32(*(u32* )pArgs);

  switch (task)
  {
    case 'w':
    { /* mw<addr:32><val:32> write <val> to memory <addr> */
      BR_OUT32(temp, REV32(*(u32*)&pArgs[4]));
      break;
    }
    case 'r':
    { /* mr<addr:32>[<num_words:8>] read <num_words> starting at <addr> */
      len = (len < 5) ? 1 : pArgs[4]; /* if num_words is not missing use 1 */
      for (int j = 0; j < len; j++, temp += 4)
      {
        PRNTHT("[", temp, "] x");
        PRNTHT("", BR_IN32(temp), CRLF);
      }
      break;
    }
    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}

