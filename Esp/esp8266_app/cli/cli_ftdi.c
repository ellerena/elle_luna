/*
 * cli_ftdi.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include <stdbool.h>

#include <regs.h>

#include "ftdi_245_sync_fifo_if.h"

void cli_ftdi (char task, void *args, int len)
{
  unsigned char * pArgs = (unsigned char*) args;
  unsigned int temp;

  switch (task)
  {
    case 's':
    { /* us: read status register */
      temp = BR_IN32(REG_FT_STATUS);
    PRNTHT("Status: ", temp, "xh" CRLF); /* show entire status register */
    PRNTHT("TX Full: ", (bool)(temp & 0x8000), "h" CRLF);
    PRNTHT("RXEmpty: ", (bool)(temp & 0x80000000), "h" CRLF);
  }
    break;
  case 'r':
  { /* check Rx, print any existing data */
    while (!(BR_IN32(REG_FT_STATUS) & 0x80000000))
    {
      PRNF("%c", BR_IN32(REG_FT_RD) & 0xff);
    }
    PRNS(CRLF);

  }
    break;
  case 't': /* ut<\t><....> : write text string to usb */
    if (pArgs[0] == '\0')
    {
      temp = 1;
      while (pArgs[temp] >= ' ')
      {
        BR_OUT32(REG_FT_WR, pArgs[temp++]);
      }
    }
    break;
  default:
  {
    GFS(F_COMMERR);
  }
    break;
  }
}

