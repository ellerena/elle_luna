/*
 * cli_sys.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include "esp_system.h"
#include "esp_spi_flash.h"
#include "mis_gflags.h"
#include "mis_logger.h"

#ifdef ARCH_ESP32C3_IMU
#define MACHINE_NAME "ESP32C3 IMU DC"
#elif defined(ARCH_ESP32C3)
#define MACHINE_NAME "ESP32C3 Enavili"
#elif defined(ARCH_ESP32)
#define MACHINE_NAME "ESP32 Wemos"
#else
#include "esp8266/eagle_soc.h"
#define MACHINE_NAME "ESP8266 Wemos"
#endif

void cli_sx9200 (char task, void * args, int len)
{
//  unsigned char *pArgs = (unsigned char*) args;

  switch (task)
  {
    /* some features require a fast i2c which is only available on ESP32 */
#if defined(ARCH_ESP32) && !defined(ARCH_ESP32C3)
    case 'x':
    {
      extern int sx92_raw_proc_start (void);
      sx92_raw_proc_start();
    }
      break;
#endif

    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}
