/*
 * cli_spi.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include <mis_gflags.h>

#include "spi.h"

void cli_spi (char task, void *args, int len)
{
  unsigned char * pArgs = (unsigned char*) args;

  switch (task)
  {
    case 't':
    { /* transmit data */
      spi_TxRx(pArgs, NULL, len);
      break;
    }
    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}

