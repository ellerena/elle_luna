/*
 * cli_i2c.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include <mis_gflags.h>
#include <mis_logger.h>

#include <os_i2c.h>

#define ADDR_DEV_LEN (1)
#define GET_AS_UIN16(p) (((p)[0] << 8) + (p)[1])
#define GET_AS_UINT24(p) ((GET_AS_UIN16(p) << 8) + (p)[2])
#define GET_AS_UINT32(p) ((GET_AS_UIN24(p) << 8) + (p)[3])

void cli_i2c (char task, void * args, int len)
{
  unsigned char *pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'a': /* ia<xx : 1 hex> address of slave */
    {
      uint8_t new_addr = len > 0 ? pArgs[0] : 0;
      new_addr = OS_i2c_master_slv_addr(new_addr);

      PRNS("i2c address: ");
      PRNHEX(new_addr, 2);
      PRNS("h [");
      PRNHEX(new_addr << 1, 2);
      PRNS("h/");
      PRNHEX((1 | (new_addr << 1)), 2);
      PRNS("h]" CRLF);
    }
      return;
    case 's': /* scan slaves present in bus */
    {
      for (uint8_t i2c_addr7 = 0; i2c_addr7 < 0x7f; ++i2c_addr7)
      {
        if (OS_i2c_master_is_slave_present(i2c_addr7))
        {
          PRNS("ID: ");
          PRNHEX(i2c_addr7, 2);
          PRNS("h [");
          PRNHEX(i2c_addr7 << 1, 2);
          PRNS("h/");
          PRNHEX((1 | (i2c_addr7 << 1)), 2);
          PRNS("h]" CRLF);
        }
      }
    }
      return;
    case 'w': /* write data bytes to current slave */
    {
      len = OS_i2c_master_wr(pArgs, len);
      PRNTHT("written ", len, CRLF);
    }
      return;
    case 'r': /* ir<nn> reads nn bytes from current slave */
    {
      len = OS_i2c_master_rd(pArgs, pArgs[0]);
      PRNTHT("received: ", len, CRLF);
      log_dump_bytes((void*) pArgs, len);

    }
      return;
    case '0': /* i0<aa><d1...dn> write bytes d1..dn to slave aa */
    {
      len = OS_i2c_master_wr_a(pArgs + 1, len - 1, pArgs[0]);
      PRNTHT("written ", len, CRLF);
    }
      return;
    case '1': /* i1<aa><rr><d0d1...dn> write bytes d1..dn to slave aa
     starting at register address rr */
    {
      int reg_len = OS_i2c_get_reg_address_len();
      if (len > reg_len + ADDR_DEV_LEN)
      {
        len = OS_i2c_master_wr_ar(pArgs + reg_len + ADDR_DEV_LEN,
                                  len - (reg_len + ADDR_DEV_LEN),
                                  pArgs[0],
                                  &pArgs[1]);
        PRNTHT("written ", len, CRLF);
      }
    }
      return;
    case '5': /* i5<aa><ll> read ll bytes from slave aa */
    {
      len = OS_i2c_master_rd_a(pArgs, pArgs[1], pArgs[0]);
      log_dump_bytes(pArgs, len);
    }
      return;
    case '6': /* i6<aa><rr[...]><ll> read ll bytes from slave aa
     starting at register address rr[...] */
    {
      int reg_len = OS_i2c_get_reg_address_len();

      if (len > reg_len + ADDR_DEV_LEN)
      {

        len = OS_i2c_master_rd_ar(pArgs,
                                  pArgs[ADDR_DEV_LEN + reg_len],
                                  pArgs[0],
                                  &pArgs[1]);
        log_dump_bytes(pArgs, len);
      }

    }
      return;
    case 'o': /* io<xx>, set i2c mode options xx */
    {
      OS_i2c_master_option_set(pArgs[0]);
    }
      return;
    case 'f': /* if<nnnnnn: hex 24b>, set clock speed nnnnnn */
    {
      if (len < 3)
      {
        break;
      }

      int clock_speed = GET_AS_UINT24(pArgs);

      clock_speed  = OS_i2c_clock_set(clock_speed);
      PRNTHT("clkSpeed: 0x", clock_speed, CRLF);
    }
      return;
    case 'x': /* ix toggle repeat start mode on/off */
    {
      int rs = OS_i2c_toggle_repeat_start();
      PRNTHT("i2c stop: ", rs, CRLF);
    }
      return;

    case 'l': /* il<xx>, set register address size, xx can be 1, 2 or 4 only */
    {
      if (len > 0)
      {
        OS_i2c_set_reg_address_len(pArgs[0]);
      }
      PRNTHT("reg address length:", OS_i2c_get_reg_address_len(), CRLF);
    }
      return;

    default:
    {
      PRNS("Invalid command or argument\n");
      GFS(F_COMMERR);
    }
      return;
  }
}
