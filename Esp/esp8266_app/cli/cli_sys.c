/*
 * cli_sys.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include "esp_chip_info.h"
#include "esp_system.h"
#include "spi_flash_mmap.h"
#include "esp_flash.h" // V5
#include "mis_gflags.h"
#include "mis_logger.h"

#ifdef ARCH_ESP32C3_IMU
#define MACHINE_NAME "ESP32C3 IMU DC"
#elif defined(ARCH_ESP32C3)
#define MACHINE_NAME "ESP32C3 Enavili"
#elif defined(ARCH_ESP32)
#define MACHINE_NAME "ESP32 Wemos"
#else
#include "esp8266/eagle_soc.h"
#define MACHINE_NAME "ESP8266 Wemos"
#endif

void cli_sys (char task, void * args, int len)
{
//  unsigned char *pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'i': /* fi: firmware id (welcome) */
    {
      esp_chip_info_t chip_info;

      esp_chip_info(&chip_info);

      uint32_t size_flash_chip;
      esp_flash_get_size(NULL, &size_flash_chip);

      PRNS("******************************\n"
           "        ...and ne forhtedon na\n");
      PRNS("build " __DATE__ " " __TIME__ "\n");

      PRNF(MACHINE_NAME " Model %d\n", chip_info.model);
      PRNF("%d CPU cores\n", chip_info.cores);
      PRNF("silicon revision %d\n", chip_info.revision);
      PRNF("%luMB %s flash\n",
           size_flash_chip / (1024 * 1024),
           (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" :
               "external");
      PRNF("Features: %s%s%s\n",
           (chip_info.features & CHIP_FEATURE_WIFI_BGN) ? "/Wifi" : "",
           (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
           (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");
      PRNS("******************************\n");

    }
      break;

    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}
