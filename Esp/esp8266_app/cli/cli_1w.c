/*
 * cli_1w.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include <ll_regs.h>
#include <mis_gflags.h>
#include <mis_logger.h>
#include <os_uart.h>
#include <os_dht11.h>
#include <os_1w.h>
#include <w1.h>

void cli_1w (char task, void *args, int len)
{
  unsigned char * pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'h':
    { /* 1h: set 1-wire pin high */
      OS_1w_pin_set(1);
      break;
    }
    case 'l':
    { /* 1l: set 1-wire pin high */
      OS_1w_pin_set(0);
      break;
    }
    case 's':
    { /* 1s: read 1-wire status register */
      PRNTHT("1w_s: ", W1_STATUS, "xh" CRLF);
      break;
    }
    case 'd':
    { /* 1d: present DHT11 data result */
      OS_dht11_show_result();
      break;
    }
    case 't':
    { /* 1t<val:32> sets the 1-wire 1/0 detector threshold value */
      OS_dht11_set_treshold(REV32(*(unsigned int * )pArgs));
      break;
    }
    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}
