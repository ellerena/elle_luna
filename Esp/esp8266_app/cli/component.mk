#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

## Factory build
COMPONENT_ADD_INCLUDEDIRS += inc
COMPONENT_ADD_INCLUDEDIRS += ../os/inc
COMPONENT_ADD_INCLUDEDIRS += ../misc/inc

COMPONENT_OBJS := cli_tools.o

ifeq '$(PROJECT_NAME)' 'ws2812'
COMPONENT_OBJS += cli_ws2812.o
else
COMPONENT_ADD_INCLUDEDIRS += ../user_app/ssd1306
COMPONENT_ADD_INCLUDEDIRS += ../user_app/isl29125
COMPONENT_ADD_INCLUDEDIRS += ../user_app/lsm6dsxx
COMPONENT_ADD_INCLUDEDIRS += ../user_app/mlx90614
COMPONENT_ADD_INCLUDEDIRS += ../user_app/qmc5883l

COMPONENT_OBJS += cli_sys.o cli_i2c.o cli_ssd1306.o cli_led.o cli_ws2812.o
COMPONENT_OBJS += cli_isl29125.o cli_lsm6dsxx.o cli_mlx90614.o cli_qmc5883l.o
endif

COMPONENT_SRCDIRS += ./

