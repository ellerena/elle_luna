/*
 * cli_spisniff.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include <regs.h>

#include "spisnif.h"

void cli_spisniffer (char task, void *args, int len)
{
//   unsigned char * pArgs = (unsigned char*)args;

  switch (task)
  {
    case 'r':
    { /* SpiSniffer reset */
      BR_OUT32(REG_SPISNIF_R0, 1u);
      BR_OUT32(REG_SPISNIF_R0, 0u);
      snif_k = 0;
      break;
    }
    case 'g':
    { /* SpiSniffer data */
      PRNS(CRLF "No\tMO\tMI\tE CS" CRLF);
      for (int i = 0; i < snif_k; i++)
      {
      PRNF("%04d\tx%02x\tx%02x\t%1d %04d" CRLF, i, SPIF_OIK(snif_dat[i]));
    }
    break;
  }
  case 'v':
  { /* SpiSniffer next value */
    len = SPIFVAL;
    PRNF("x%08x  MO:x%02x  MI:x%02x  E:%1d K:%4d\n>" CRLF, len, SPIF_OIK(len));
    break;
  }
  default:
  {
    GFS(F_COMMERR);
  }
    break;
  }
}

