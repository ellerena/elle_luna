## cli - command line interface component build configuration

FILE(GLOB allc cli_tools.c
               cli_sys.c
               cli_led.c
               cli_i2c.c
               cli_isl29125.c
               cli_mlx90614.c
               cli_ssd1306.c
               cli_lsm6dsxx.c
               cli_qmc5883l.c
               cli_sx9200.c
               cli_ws2812.c
               )

idf_component_register(SRCS ${allc}
                       INCLUDE_DIRS "inc"
                       REQUIRES isl29125 lsm6dsxx mlx90614 qmc5883l
                        sx9200 ws2812 sph0645
                       )
#set_property(TARGET ${COMPONENT_LIB} APPEND PROPERTY LINK_INTERFACE_MULTIPLICITY 3)
