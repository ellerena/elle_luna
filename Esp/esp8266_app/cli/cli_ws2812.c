/*
 * cli_ws2812.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include "mis_gflags.h"

void cli_ws2812 (char task, void * args, int len)
{
  unsigned char *pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'w': // ww<ggrrbb>...
    {
      extern int ws2812_proc (void*, int);
      ws2812_proc(pArgs, len);
    }
      break;

    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}
