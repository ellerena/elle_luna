/*
 * cli_ssd1306.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include <mis_gflags.h>

#include <disp_txt_library.h>
#include <SSD1306.h>
#include <oledproc.h>

void cli_ssd1306 (char task, void *args, int len)
{
  unsigned char * pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'i': /* oi: oled initialization */
    {
      display_Init();
    }
      break;
    case 'c': /* oc<xxyy....: n hex> commands to send to oled */
    {
      while (len--)
      {
        ssd1306_command(*pArgs++);
      }
    }
      break;
    case 't': /* ot<\t><....> : write text string to oled */
    {
      if (pArgs[0] == '\0')
      {
#ifdef MEMALIGNEDREQUIRED
        oled_proc_queue((void*) &pArgs[3]);
//        display_puts((char*) &pArgs[3]);
#else
        oled_proc_queue((void*) &pArgs[1]);
//        display_puts((char*) &pArgs[1]);
#endif
      }
    }
      break;
    case 'p': /* op<x:8><y<:8> : position cursor at lines x, y */
    {
      display_locate(pArgs[0], pArgs[1]);
    }
      break;
    case 'z': /* oz: clear screen */
    {
      display_clear();
    }
      break;
    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}

