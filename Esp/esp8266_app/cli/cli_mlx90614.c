/*
 * cli_mlx90614.c
 *
 *  Created on: Oct 30, 2021
 *      Author: ellerena
 */

#include "mlx90614.h"
#include "mis_gflags.h"

void cli_mlx90614 (char task, void * args, int len)
{
//  unsigned char *pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'p': // mlx90614 start process
    {
      mlx90614_raw_proc_start();
    }
      break;

    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}

