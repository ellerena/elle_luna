/*
 * cli_lsm6dsxx.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include "lsm6dsxx_platform.h"
#include "mis_gflags.h"

void cli_lsm6dsxx (char task, void * args, int len)
{
  unsigned char *pArgs = (unsigned char*) args;

  switch (task)
  {
    case '6': // lsm6dsxx 6d detection
    {
      lsm6dsxx_proc_start(D6D, pArgs, len);
    }
      break;
    case '2': // sw<xxyy> lsm6dsxx i1c stress test
    {
      int loops = (pArgs[0] * 256) + pArgs[1];
      proc_i2c_tst(loops);
    }
      break;
    case 'a': // lsm6dsxx tap detection
    {
      lsm6dsxx_tap_proc_start();
    }
      break;
    case 'm': // lsm6dsxx mlc test
    {
      lsm6dsxx_proc_start(MLC, pArgs, len);
    }
      break;
    case 'r': // lsm6dsxx raw data logger
    {
      lsm6dsxx_proc_start(RAWDAT, pArgs, len);
    }
      break;
    case 't': // lsm6dsxx tilt detection
    {
      lsm6dsxx_proc_start(TILT, pArgs, len);
    }
      break;

    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}
