/*
 * cli_tools.h
 *
 *  Created on: Oct 10, 2021
 *      Author: eddie
 */

#ifndef CLI_INC_CLI_TOOLS_H_
#define CLI_INC_CLI_TOOLS_H_

#define CLI_SELECT(task, fn) \
   case task:\
   {\
      extern void fn (char, void *, int);\
      fn(mode, (void*)&ARGS_BEGIN, n);\
   } break

#define TASK         buf[0] /* variable representing the 'task' */
#define MODE         buf[1] /* variable representing the 'mode' */
#define ARGS         buf[2] /* first element of arguments array */

#ifdef MEMALIGNEDREQUIRED
#define ARGS_BEGIN   buf[0] /* position of the arguments in the buffer */
#else
#define ARGS_BEGIN   buf[2] /* position of the arguments in the buffer */
#endif

extern int ParseCharsToValueHex (char *p);

#endif /* CLI_INC_CLI_TOOLS_H_ */
