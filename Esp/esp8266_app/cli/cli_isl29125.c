/*
 * cli_isl29125.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include "mis_gflags.h"
#include "isl29125.h"

void cli_isl29125 (char task, void * args, int len)
{
//  unsigned char *pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'i': // isl29125 start setup
    {
      isl29125_proc_setup();
    }
      break;

    case 'x': // isl29125 raw data capture
    {
      isl29125_raw_proc_start();
    }
      break;

    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}
