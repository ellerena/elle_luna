/*
 * cli_qmc5883l.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include "stdio.h"
#include "qmc5883l.h"
#include "mis_gflags.h"

void cli_qmc5883l (char task, void * args, int len)
{
//  unsigned char *pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'p': // qmc5883l magnetometer process
    {
      qmc5883l_raw_proc_start();
    }
      break;

    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}
