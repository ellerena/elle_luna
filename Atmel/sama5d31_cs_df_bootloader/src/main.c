/* ----------------------------------------------------------------------------
 *         SAM Software Package License 
 * ----------------------------------------------------------------------------
 * Copyright (c) 2012, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

/**
 * \page spi_serialflash SPI with Serialflash Example
 *
 * \section Purpose
 *
 * \section Requirements
 *
 * This package can be used with SAMA5D3x evaluation kits.
 *
 * \section Description
 *
 * \section References
 * - spi_serialflash/main.c
 * - spi.c
 * - spid_dma.c
 * - at25_spi.c
 * - at25d.c
 */

#include <board.h>
#include <libspiflash.h>
#include <stdio.h>
#include <string.h>

#define POLLING_MODE    0								/** Polling or interrupt mode */
#define SPI_PINS        PINS_SPI0, PIN_SPI0_NPCS0		/** SPI peripheral pins to configure to access the serial flash. */
#define DF_OFFSET		0x40000							/** User Program offset in DataFlash */
#define EXT_SDRAM		(0x20000000)					/** Start address of external SDRAM */
#define	PROGSIZE		(200 * 1024 )


typedef void (*fnc)(void);
const fnc AppEntry = (fnc) EXT_SDRAM;					/** Pointer to start of the External SDRAM where Application is copied */

static sDmad dmad;										/** Global DMA driver instance for all DMA transfers in application. */
static At25 at25;										/** Serial flash driver instance. */
static Spid spid;										/** SPI driver instance. */

#if POLLING_MODE == 0
static void ISR_DMA(void)
{
    DMAD_Handler(&dmad);
}
#endif

int main(void)
{
	uint32_t tmp1, tmp2;

	WDT->WDT_MR = WDT_MR_WDDIS;

	printf( "-- E.Llerena bootloader v1.0.0.0 %s %s --\n\r", __DATE__, __TIME__ );

	BOARD_ConfigureDdram(DDRAM_MT47H128M16RT);
	DMAD_Initialize( &dmad, POLLING_MODE );				/** Initialize DMA driver instance with polling mode */

#if POLLING_MODE == 0									/** Enable interrupts */
	PMC->PMC_PCER1 = (1 << ( ID_IRQ - 32));
	AIC->AIC_SSR  = ID_DMAC0;
	AIC->AIC_IDCR = AIC_IDCR_INTD;						/** Disable the interrupt first */
	AIC->AIC_SMR  = 0;									/** Configure mode */
	AIC->AIC_SVR = (uint32_t) ISR_DMA;					/** Configure handler */
	AIC->AIC_ICCR = AIC_ICCR_INTCLR;					/** Clear interrupt */
	AIC->AIC_IECR = AIC_IECR_INTEN;
#endif
														/** Initialize the SPI */
	tmp1 = (PIO_PD10A_SPI0_MISO | PIO_PD11A_SPI0_MOSI \
			| PIO_PD12A_SPI0_SPCK | PIO_PD13A_SPI0_NPCS0);

	PIOD->PIO_IDR = tmp1;
	PIOD->PIO_PUDR = tmp1;
	tmp2 = PIOD->PIO_ABCDSR[0];
	tmp2 &= ~tmp1;
	PIOD->PIO_ABCDSR[0] = tmp2;
	PIOD->PIO_ABCDSR[1] = tmp2;
	PIOD->PIO_PDR = tmp1;
														/** Initialize the serial flash */
	spid.pSpiHw = SPI0;
	spid.spiId  = ID_SPI0;
	spid.semaphore = 1;
	spid.pCurrentCommand = 0;
	spid.pDmad = &dmad;

	PMC->PMC_PCER0 = (1 << ID_SPI0);					/** Enable the SPI Peripheral ,Execute a software reset of the SPI, Configure SPI in Master Mode*/
	SPI0->SPI_CR = SPI_CR_SPIDIS;
	SPI0->SPI_CR = SPI_CR_SWRST;						/** Execute a software reset of the SPI twice */
	SPI0->SPI_CR = SPI_CR_SWRST;
	SPI0->SPI_MR = SPI_MR_MSTR | SPI_MR_MODFDIS | SPI_MR_PCS_Msk;
	PMC->PMC_PCDR0 = (1 << ID_SPI0);					/* disable the SPI peripheral */

	AT25_Configure(&at25, &spid, 0, POLLING_MODE);

	AT25_SendCommand(&at25, AT25_READ_JEDEC_ID, 1, \
			(unsigned char *) &tmp1, 3, 0, 0, 0);		/** Read the JEDEC ID of the device to identify it*/

	while ((&spid)->semaphore == 0);					/** Wait for transfer to finish */

	if (AT25_FindDevice(&at25, tmp1))
	{
		printf("%s (%08x) serial flash detected\n\r", (&at25)->pDesc->name, tmp1);
	}
	else
	{
		printf("Failed to detect device (JEDEC ID 0x%08X).\n\r", tmp1);
		return 1;
	}

	tmp2 = (&at25)->pDesc->pageSize;
	tmp1 = (&at25)->pDesc->size / tmp2;

	printf("Dataflash %dx%d\n\r", tmp1, tmp2 );

	for (tmp1 = 0; tmp1 < PROGSIZE; tmp1 += tmp2)			/** Copy code from External Flash to External SDRAM */
	{
		AT25D_Read(&at25, (unsigned char *)(EXT_SDRAM+tmp1), tmp2, DF_OFFSET+tmp1);
	}

	printf("\n\rStarting program @0x40000...\n\r");

	AppEntry();

	return 0;
}

