/*
 * debugger.c
 *
 *  Created on: Mar 16, 2014
 *      Author: Eddie.Llerena
 */

#include "SAMPal.h"

/**
 *	@brief	adjusts the SPI1_CR register parameters
 *	@param	bytepos is the bit position where the parameter begins.
 *			it can take these values:
 *			0u:	parameter is in the 8 LSB bits (BCCNC)
 *			8u: paratemer is in the 2nd byte (SCBR)
 *			16u: parameter is in the 3rd bye (DLYBS)
 *			24u: parameter is in the 4th byte (DLBCT)
 *	@param	delta is the increment/decrement value for the parameter
 * */
void AdjustSPI1CR(uint32_t bytepos, uint32_t delta)
{
	uint32_t temp, temp2;
	uint32_t *ptemp;

	ptemp = (uint32_t*)SPI1;						/* point to the SPI1 structure */
	temp = *(ptemp + 0xf);							/* SPI1_CSR: get current reg. value */

	delta = delta << bytepos;
	delta = temp + delta;
	bytepos = (0xffu << bytepos);
	temp &= ~bytepos;
	delta &= bytepos;
	temp |= delta;

	*(ptemp + 0xf) = temp;							/* SPI1_CSR: Upload the new value */
	printf("SPI1_CSR3 new value 0x%08x", temp);		/* print the result */
}


void readstr(char* c)
{
	do
	{
		char temp;
		temp = DBGU_GetChar();
		printf("%c", temp);
		*c = temp;
		c++;
		if (temp ==13)
		{
			break;
		}
	} while (1);
}

extern void SPI_Calibration( void )
{
	uint32_t tmp1 = 0, tmp2;
	char str[30], *p;

	p = (char*)SPI1;
	tmp1 = *((uint32_t*)p + 0xf);

	while(1)
	{
		printf("\r#");
		tmp1 = DBGU_GetChar();
		printf("%c ", (int)tmp1);
		switch(tmp1)
		{
			case 'w':										/* write a byte string via SPI1 */
				printf ("SPI write [reg val]: ");
				readstr(str);								/* read a string of bytes in the format "## ## ##..." */
				tmp2 = 0;
				p = str;
				while (31 < (*p))
				{
					str[tmp2] = (char) strtol(p, &p, 16);	/* convert each ## string (hex) into a number */
					tmp2 += 3;
				}
				tmp2 = *(uint32_t*)str;
				tmp2 &= 0xff0000ff;
				tmp2 |= 0x00000180;
				*(uint32_t*)str = tmp2;
				SpiWriteData(4, (uint8_t*)str);				/* sends the bytes via SPI1 */
				break;
			case 'g':
				printf ("SPI read [reg]: ");
				readstr(str);
				p = str;
				*(uint32_t*)str = strtol(p, &p, 16);
				*(str+1) = 1;
				SpiWriteData(3u, (uint8_t*)str);
				printf("\n\n\t\t0x%02x", SpiSendByte(0));
				break;
			case 't':
			case 'T':
				tmp2 = 24;
				tmp1 = (tmp1 & 0x20) ? -1 : 1;
				AdjustSPI1CR(tmp2, tmp1);
				break;
			case 's':
			case 'S':
				tmp2 = 16;
				tmp1 = (tmp1 & 0x20) ? -1 : 1;
				AdjustSPI1CR(tmp2, tmp1);
				break;
			case 'b':
			case 'B':
				tmp2 = 8;
				tmp1 = (tmp1 & 0x20) ? -1 : 1;
				AdjustSPI1CR(tmp2, tmp1);
				break;
			case 'c':
			case 'C':
				tmp2 = 0;
				tmp1 = (tmp1 & 0x20) ? -1 : 1;
				AdjustSPI1CR(tmp2, tmp1);
				break;

#ifdef ENABLE_SPI_CALIBRATION_MODULE
			case 'u':
				Update_SPI_Param();
				break;
#endif
			case 'p':								/* 0x50 - pin name: 0x0, pin state: 1 */
			case 'P':								/* 0x70 - pin name: 0x0, pin state: 0 */
			case 'r':								/* 0x71 - pin name: 0x1, pin state: 1 */
			case 'R':								/* 0x51 - pin name: 0x1, pin state: 0 */
			case 'a':								/* 0x62 - pin name: 0x2, pin state: 1 */
			case 'A':								/* 0x42 - pin name: 0x2, pin state: 0 */
			case '[':								/* 0x5b - pin name: 0xb, pin state: 1 */
			case '{':								/* 0x7b - pin name: 0xb, pin state: 0 */
			case ']':								/* 0x5d - pin name: 0xb, pin state: 1 */
			case '}':								/* 0x7d - pin name: 0xb, pin state: 0 */

													/*** Extract pin and state info from 'tmp1' ***/
				tmp2 = (tmp1 & 0x20u)>>5;				/* pin state requested */
				tmp1 &= 0xfu;						/* pin name */
				GPIO_Pin_Toggle(tmp1, tmp2);
				break;
//			case '?':
//				printf("0x%lx\r\tmp2>", SAMPal_RNG_Get());
//				break;
//			case 'd':
//				printf("5sec tick: ");
//				while(1) {
//					SlPalTimeDelayMicrosec(0, 5000000);
//					printf(". ");
//				}
//				break;
			case 'q':
				return;
		}
    }
}
