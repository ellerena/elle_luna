/*
 ============================================================================
 Name        : main.c
 Author      : Eddie Llerena
 Version     :
 Copyright   : All rights reserved by the author
 Description : Hello World in C
 ============================================================================
 */

#include "SAMPal.h"

extern int main( void )
{

#if 0
    BOARD_ConfigureDdram(BOARD_DDRAM_TYPE);
#endif

#ifdef ddram
    MMU_Initialize((uint32_t *)0x30C000);
    CP15_EnableMMU();
    CP15_EnableIcache();
    CP15_EnableDcache();
#endif

    SlPalInitialize(SL_NULL, SL_NULL);
	printf( "\r\nSAMA5D3x-EK LOC Sample App Test v.1.0.0.2 %s - %s",  __DATE__, __TIME__) ;
	SPI_Calibration();
	return 0;
}
