/***********************************************************************************

    Filename:     mrfi_link.c

    Description:  MRFI link implementation.

***********************************************************************************/

/***************************** INCLUDES *****************************/
#include "string.h"
#include <stdio.h>
#include "mrfi_link.h"
#include "hal_defs.h"

#define USB_SUSPEND_HOOKS

#ifdef USB_SUSPEND_HOOKS    // Register USB hooks if necessary
#include "usb_suspend.h"
static void linkRestore(void);
#endif

/************************ PRIVATE CONSTANTS ************************/
#define MRFI_LINK_PAN_ID            0x2007      // MRFI address definitions

/************************ PRIVATE VARIABLES ************************/
XDATA mrfiPacket_t pkt_in;
XDATA mrfiPacket_t pkt_out;
XDATA volatile uint8_t mrfiPktRdy;     // TRUE when a valid data packet is ready

/************************ PUBLIC VARIABLES ************************/
mrfiPacket_t * CODE pPktOut = &pkt_out;
mrfiPacket_t * CODE pPktIn = &pkt_in;

/************************ PUBLIC FUNCTIONS ************************/
/**
* @brief      Initialise the MRFI layer. Selects RF channel and addresses.
* @param      src - source address (16 bit)
* @param      dst - destination address (16 bit)
*/
void rfInit(void)
{
#ifdef USB_SUSPEND_HOOKS    // Register USB hooks if necessary
    pFnSuspendEnterHook= MRFI_Sleep;
    pFnSuspendExitHook= linkRestore;
#endif

   /* Initialise the addresses */
   MRFI_P_DST_ADDR(pPktOut)[0] = LO_UINT16(MRFI_LINK_PAN_ID);
   MRFI_P_DST_ADDR(pPktOut)[1] = HI_UINT16(MRFI_LINK_PAN_ID);
   MRFI_P_DST_ADDR(pPktOut)[2] = LO_UINT16(DEV_ADDR_REM);
   MRFI_P_DST_ADDR(pPktOut)[3] = HI_UINT16(DEV_ADDR_REM);

   MRFI_P_SRC_ADDR(pPktOut)[0] = LO_UINT16(MRFI_LINK_PAN_ID);
   MRFI_P_SRC_ADDR(pPktOut)[1] = HI_UINT16(MRFI_LINK_PAN_ID);
   MRFI_P_SRC_ADDR(pPktOut)[2] = LO_UINT16(DEV_ADDR_LOC);
   MRFI_P_SRC_ADDR(pPktOut)[3] = HI_UINT16(DEV_ADDR_LOC);

                                       /* Initialise MRFI link housekeeping data */
   mrfiPktRdy= FALSE;
                                       /* Initialise MRFI */
   MRFI_Init();
   MRFI_WakeUp();
   MRFI_SetLogicalChannel(MRFI_CHANNEL);
#if (TARGET_CONFIG == CLT_TARGET)
   MRFI_RxOn();
   MRFI_SetRxAddrFilter(MRFI_P_SRC_ADDR(pPktOut));
   MRFI_EnableRxAddrFilter();
#else
#ifdef MRFI_CC1111
   rfBroadcastTxt("CC1111\r", 7);
#else
   rfBroadcastTxt("CC1110\r", 7);
#endif
#endif
}

/**
* @brief      Read data from the RX buffer
* @param      pBuf - buffer for storage of received data
* @return     Number of bytes received
*/
uint8_t rfRecv(void)
{
   uint8_t n;

   n= mrfiPktRdy ? MRFI_GET_PAYLOAD_LEN(&pkt_in) : 0;
   mrfiPktRdy= FALSE;

   return n;
}

/**
 * @brief   Simplest broadcasting.
 * */
void rfBroadcast(uint16_t len)
{
   MRFI_SET_PAYLOAD_LEN(pPktOut, len);

   MRFI_Transmit(&pkt_out, MRFI_TX_TYPE_FORCED);
}

void rfBroadcastDataAsTxt(uint8_t* data, uint16_t len)
{
   uint8_t *p = MRFI_P_PAYLOAD(pPktOut);

   *p++ = F_MSG_TXT;
   while (len--)
   {
      sprintf((char*)p, "%02x", *data++);
      p+=2;
   }
   rfBroadcast(p - (uint8_t *)MRFI_P_PAYLOAD(pPktOut));
}

void rfBroadcastTxt(const char* txt, uint16_t len)
{
   uint8_t *p = MRFI_P_PAYLOAD(pPktOut);

   p[0] = F_MSG_TXT;
   memcpy(p+1, txt, len);
   rfBroadcast(len+1);
}

/**
* @brief       Returns true if RF data is ready
* @return      true if data is ready
*/
uint8_t rfDataRdy(void)
{
   return mrfiPktRdy;
}

#ifdef USB_SUSPEND_HOOKS
/**
* @fn          linkRestore
* @brief       Restore the link after exiting from LPM. Used as USB resume hook
*/
static void linkRestore(void)
{
    MRFI_WakeUp();
    MRFI_RxOn();
}
#endif

/**
* @brief       This function is called by the ISR of MRFI at RF receive and MUST
*              be included in all applications. It should be as short as possible
*              to avoid lengthy processing in the ISR.
*/
void MRFI_RxCompleteISR()
{
   MRFI_Receive(&pkt_in);

   if (MRFI_GET_PAYLOAD_LEN(&pkt_in)>1)
   {
      mrfiPktRdy= TRUE;
   }
}



