/***********************************************************************************

  Filename:	    rf_modem.c

  Description:	RF Modem is an application which uses MRFI to implement Over The Air
                streaming between two serial ports. The application implements a
                simple ACK handshake on top of MRFI.

  Operation:    1) Select a device number (1 or 2) by moving the joystick left/right.
                The device number must be distinct between the two nodes in
                ordre to assign each of them unique addresses in the network.
                NB! USB dongles automatically assume device number 2.

                2) Push S1 to confirm the choice.

                3) Configure your terminal emulation program as follows:
                - baud rate 38400 bps
                - 8 data bits no parity, 1 stop bit
                - HW flow control

***********************************************************************************/

/****************************** INCLUDES ******************************/
#include <string.h>
#include <stdio.h>
#include "hal_board.h"
#include "hal_uart.h"
#include "mrfi_link.h"
#include "mrfi.h"

/**************************** CONSTANTS and DEFINITIONS ****************************/
#if (TARGET_CONFIG == SVR_TARGET)
#define ACLK_TICKS_PER_SEC   (32)
#define TIC_ONE_SEC          (ACLK_TICKS_PER_SEC)
#define TIC_TEN_MSEC         (ACLK_TICKS_PER_SEC/100)
#define TIM_LED_CYCLE        (20*TIC_ONE_SEC)
#define TIM_LED_ON           (TIC_TEN_MSEC)
#elif (TARGET_CONFIG == CLT_TARGET)
#define COM_MAX_LEN_IN       (44)
#define TIM_LED_CYCLE        (100)      /* .25 sec */
#define TIM_LED_CYCLE_LONG   (4*TIM_LED_CYCLE)
#define MSG_RCVD_FAIL        (22)
#define TIM4_CONFIG          (T4CTL_DIV_128 | T4CTL_OVFIM | T4CTL_START | T4CTL_MODE_UPDOWN)

IDATA uint16_t gTimLoad = TIM_LED_CYCLE;
IDATA uint16_t gTim = TIM_LED_CYCLE;
#endif


void fSystemReset(void);                       /* System Reset fn */

/****************** Interrupt Service Routine Declarations *******************/
BSP_ISR_FUNCTION(uart1RxISR, URX1_VECTOR);
BSP_ISR_FUNCTION(isr_button, P1INT_VECTOR);
BSP_ISR_FUNCTION( MRFI_RfIsr, RF_VECTOR );
BSP_ISR_FUNCTION(sleep_timer_isr, ST_VECTOR);
BSP_ISR_FUNCTION(mytimer, T4_VECTOR);

/******************************** LOCAL VARIABLES ********************************/
IDATA volatile uint8_t flags;

#define  P1CC111X                (0xf0)
#define  IN_PORT                 (P1CC111X | BIT2 | BIT3)

#define MACRO_GPIO_INIT          /* (P1_0 and P1_1 already in BSP_Init */\
   P1SEL &= ~P1CC111X;           /* Set pins as GPIO */\
   P1DIR &= ~P1CC111X;           /* Set pins as INPUT */\
   P1INP &= ~P1CC111X;           /* Set pins as PU/PD */\
   P2INP &= ~P2INP_PDUP1;        /* Set all P1 inputs as PU */\
   P1IFG &= ~P1CC111X;           /* Clear pending interrupts */\
   P1IEN &= ~P1CC111X;           /* Disable IRQs */

#define MACRO_ADC10_INIT         ADCCON1 = STSEL_ST | BIT1 | BIT0;   \
                                 ADCCON2 = ADCCON2_SREF_AVDD | ADCCON2_SDIV_256 | ADCCON2_SCH_POSVOL;

/****************************** LOCAL FUNCTIONS ******************************/
/**
* @brief       This is the main entry of the RF Modem application. It sets
*              distinct short addresses for the nodes, initalises and runs
*              receiver and sender tasks sequentially in an endless loop.
*/
void main(void)
{
#if (TARGET_CONFIG == CLT_TARGET)
   uint8_t msgLenIn;
   uint8_t *p = MRFI_P_PAYLOAD(pPktIn);
   uint8_t msgRcvdCount = 0;
   uint8_t gCOMmsg[COM_MAX_LEN_IN];
#else
   uint8_t *p = MRFI_P_PAYLOAD(pPktOut);
   uint8_t tmp;
#endif

	BSP_Init();
	halMcuWaitMs(50);
#if (TARGET_CONFIG == CLT_TARGET)
   halUartInit();
// halSpiInit();
#else
   MACRO_GPIO_INIT
   MACRO_ADC10_INIT
#endif

#if (TARGET_CONFIG == CLT_TARGET)
   T4CC0 = 0xff;                                   /* timer 4: count full range */
   T4OVFIF = 0;
   T4IE = 1;
   T4CTL = T4CTL_CLR;
   T4CTL = TIM4_CONFIG;                            /* use MCLK/128, up/down mode, irq enabled */
#else                                              /** setup SLEEP timer **/
   STIF = 0;                                        /* clear Sleep Timer CPU interrupt flag */
   WORIRQ &= ~WORIRQ_EVENT0_FLAG;                   /* clear Sleep Timer peripheral interrupt flag */
   WORIRQ |= WORIRQ_EVENT0_MASK;                    /* Enable interrupt mask for sleep timer */
   WORCTRL = WORCTL_WOR_RESET | WORCTL_WOR_RES_1024;  /* sleep timer: reset and set resolution */
   tmp = WORTIME0;
   while(tmp == WORTIME0);                          /* 1st wait for positive edge */
   tmp = WORTIME0;
   while(tmp == WORTIME0);                          /* 2nd wait for positive edge */
   WOREVT0 = (TIM_LED_CYCLE & 0xff);                /* sleep time low byte*/
   WOREVT1 = (TIM_LED_CYCLE >> 8);                  /* sleep time high byte*/
   SLEEP = (SLEEP & ~SLEEP_MODE) | SLEEP_MODE_PM0;
   STIE = 1;
#endif
   rfInit();                                        /* Init MRFI stack */

	flags = 0;
	BSP_ENABLE_INTERRUPTS();			   				/* Enable interrupts */
	while(1)											   		/* Main processing loop */
	{
#if (TARGET_CONFIG == CLT_TARGET)                  /* CLT_TARGET */
      if (rfDataRdy())                       /* if data has been received... */
      {
         gTimLoad = TIM_LED_CYCLE_LONG;
         msgLenIn = rfRecv();                /* Receive RF data */
         if(msgLenIn)                              /* send packet to UART */
         {
            msgRcvdCount = 0;
            flags = 0;
            if(p[0] == F_MSG_TXT)
            {
               COM_puts((char*)p+1);
            }
            else        /* F_MSG_BIN: <F_MSG_BIN>,<P1>,<P2>,<BATT_16bit>,<P1b>,<P2b> */
            {
               uint32_t t;

               t = (p[3] + (p[4] << 8));
               sprintf(gCOMmsg, "PORT:x%04x, BATT:%lumV\r", p[1], t*3000/511);
               COM_puts(gCOMmsg);
               t = (p[1] | (p[2] << 8));
               if (t ^ IN_PORT) HAL_LED_R_ON;
               else HAL_LED_R_OFF;
            }
         }
      }
      if (gTim < 1)
      {
         msgRcvdCount++;
         if(msgRcvdCount >= MSG_RCVD_FAIL)
         {
            gTimLoad = TIM_LED_CYCLE;
            msgRcvdCount = 0;
         }
         gTim = gTimLoad;
         HAL_LED_G_TGL;
      }
#else                                        /* SVR_TARGET */
      uint16_t t;

      tmp = WORTIME0;
      while(tmp == WORTIME0);                          /* wait for positive edge */
      PCON |= PCON_IDLE;                               /* enter sleep power mode */
      ADCCON1 |= ADCCON1_ST | (BIT1 | BIT0);
      while( !(ADCCON1 & ADCCON1_EOC));
      t = ((ADCH << 8) | (ADCL));
      t >>= 6;
      p[3] = t;
      p[4] = (t>>8);
      t = (P1 & IN_PORT);
      p[1] = t;
      p[5] = 0xff ^ t;
      t >>= 8;
      p[2] = t;
      p[6] = 0xff ^ t;
      p[0] = F_MSG_BIN;

      MRFI_WakeUp();
      rfBroadcast(7);
      MRFI_Sleep();
#endif

//		if(F_COM_RCVD & flags)
//		{
//			tmp = rfSend(N_RETRIES);
//			if( tmp != MRFI_TX_RESULT_SUCCESS)
//			{
//				COM_send("Ter\r",4);
//				HAL_LED_R_ON;
//			}
//			else
//			{
//				HAL_LED_R_OFF;
//			}
//			flags &= ~F_COM_RCVD;
//		}
//
//		if (rfDataRdy())									/* if data has been received... */
//		{
//			tmp = rfRecv();								/* Receive RF data */
//			if(tmp)												/* send packet to UART */
//			{
//				COM_send(PKT_DATA_OFS(pPktIn),tmp);
//				HAL_LED_G_TGL;
//				HAL_LED_R_OFF;
//			}
//			else
//			{													/* if we get here, an error occurred */
//				HAL_LED_R_ON;
//			}
//		}
	}
}

void halMcuWaitMs(uint16_t msec)
{
   while (msec--)
   {
      BSP_Delay(1000);
   }
}

void fSystemReset(void)                       /* System Reset fn */
{
   WDCTL = ((WDCTL & (WDCTL_MODE | WDCTL_EN | WDCTL_CLR)) | WDCTL_INT3_MSEC_2);  // after ~2 ms

   WDCTL &= ~WDCTL_MODE;                     // Select watchdog mode
   WDCTL |= WDCTL_EN;                        // Enable timer
   while(1);                                 // Halt here until reset
}


BSP_ISR_FUNCTION(isr_button, P1INT_VECTOR)
{
	if (P1IFG & BIT2)
	{
//		HAL_LED_G_TGL;								/* force PUC */
//		if(MRFI_GET_PAYLOAD_LEN(pPktOut))
//			flags |= F_COM_RCVD;
	}

	if (P1IFG & BIT3)
	{
	   fSystemReset();								/* force PUC */
	}

	P1IFG = 0;												/* Clear pending interrupts - pin 2 */
	P1IF = 0;												/* clear IRCON2 flag */
}

BSP_ISR_FUNCTION(sleep_timer_isr, ST_VECTOR)
{   /* the order in which the flags are cleared is important.
      For pulse or egde triggered interrupts one has to clear the CPU interrupt
      flag prior to clearing the module interrupt flag. */
   STIF = 0;                      /* clear Sleep Timer CPU interrupt flag */
   WORIRQ &= ~WORIRQ_EVENT0_FLAG; /* clear Sleep Timer peripheral interrupt flag */
}


BSP_ISR_FUNCTION(mytimer, T4_VECTOR)
{
#if (TARGET_CONFIG == SVR_TARGET)
#elif (TARGET_CONFIG == CLT_TARGET)
   if (T4OVFIF)   /* 26MHz period * 128 * 512 = 2.521msec */
   {
      gTim--;
      T4OVFIF = 0;
   }
#endif
}


