/***********************************************************************************

  Filename:	    rf_modem.c

  Description:	RF Modem is an application which uses MRFI to implement Over The Air
                streaming between two serial ports. The application implements a
                simple ACK handshake on top of MRFI.

  Operation:    1) Select a device number (1 or 2) by moving the joystick left/right.
                The device number must be distinct between the two nodes in
                ordre to assign each of them unique addresses in the network.
                NB! USB dongles automatically assume device number 2.

                2) Push S1 to confirm the choice.

                3) Configure your terminal emulation program as follows:
                - baud rate 38400 bps
                - 8 data bits no parity, 1 stop bit
                - HW flow control

***********************************************************************************/

/****************************** INCLUDES ******************************/
#include <string.h>
#include <stdio.h>
#include "hal_board.h"
#include "hal_uart.h"			/* needed just for the F_COM_RCVD definition */
#include "usb_uart.h"
#include "mrfi_link.h"
#include "mrfi.h"

/**************************** CONSTANTS and DEFINITIONS ****************************/
#define COM_MAX_LEN_IN       (40)
#define TIM_LED_CYCLE        (92)      /* .25 sec */
#define TIM_LED_CYCLE_LONG   (4*TIM_LED_CYCLE)
#define MSG_RCVD_FAIL        (22)
#define TIM4_CONFIG          (T4CTL_DIV_128 | T4CTL_OVFIM | T4CTL_START | T4CTL_MODE_UPDOWN)

extern uint8_t  cdcCTS;

/****************** Interrupt Service Routine Declarations *******************/
BSP_ISR_FUNCTION( MRFI_RfIsr, RF_VECTOR );
BSP_ISR_FUNCTION(mytimer, T4_VECTOR);
BSP_ISR_FUNCTION(usbirqHandler, P2INT_VECTOR);
BSP_ISR_FUNCTION(usbirqResumeHandler, P0INT_VECTOR);
#ifdef MRFI_CC1110
BSP_ISR_FUNCTION(uart1RxISR,URX1_VECTOR);
#endif

/******************************** LOCAL VARIABLES ********************************/
XDATA volatile uint8_t flags;
XDATA int16_t gTimLoad = TIM_LED_CYCLE;
XDATA int16_t gTim = TIM_LED_CYCLE;
XDATA uint8_t msgRcvdCount = 0;
XDATA uint8_t gCOMmsg[COM_MAX_LEN_IN];
XDATA char gID[] = "CC1111 - AP - v1.0\r";

static void appRfReceiverTask(void);
static void appRfSenderTask(void);

/****************************** LOCAL FUNCTIONS ******************************/
/**
* @brief       This is the main entry of the RF Modem application. It sets
*              distinct short addresses for the nodes, initalises and runs
*              receiver and sender tasks sequentially in an endless loop.
*/
void main(void)
{
	BSP_Init();
	halMcuWaitMs(50);

   bufInit(&rbRxBuf);
   bufInit(&rbTxBuf);

   T4CC0 = 0xff;                                   /* timer 4: count full range */
   T4OVFIF = 0;
   T4IE = 1;
   T4CTL = T4CTL_CLR;
   T4CTL = TIM4_CONFIG;                            /* use MCLK/128, up/down mode, irq enabled */

	rfInit();  			// Initialize the MRFI RF link layer

	flags = 0;
   usbUartInit(115200);                      /* start USB - UART interface (and enable IRQs) */
   HAL_LED_G_ON;
   while(1)																//  Main processing loop
	{
      if (gTim < 1)
      {
         msgRcvdCount++;
         if(msgRcvdCount >= MSG_RCVD_FAIL)
         {
            gTimLoad = TIM_LED_CYCLE;
            msgRcvdCount = 0;
         }
         gTim = gTimLoad;
//         HAL_LED_G_TGL;
      }
      usbUartProcess();
      appRfSenderTask();
      appRfReceiverTask();
	}
}

void halMcuWaitMs(uint16_t msec)
{
   while (msec--)
   {
      BSP_Delay(1000);
   }
}

static void appRfReceiverTask(void)
{
   uint8_t *p = MRFI_P_PAYLOAD(pPktIn);
   uint8_t msgLenIn;

   if (rfDataRdy())
   {
      cdcCTS = 0;                      // Tell the PC not to send data
      halMcuWaitMs(1);                 // Wait for the PC to respond
      msgLenIn = rfRecv();             // Receive RF data

      switch (p[0])
      {
         case F_MSG_BIN:
         {
            uint32_t t;

            t = (p[3] + (p[4] << 8));
            sprintf(gCOMmsg, "P:x%04x, B:%lumV\r", p[1], t*3000/511);
            bufPut(&rbTxBuf,gCOMmsg,22);
            break;
         }
         case F_MSG_TXT:
         {
            bufPut(&rbTxBuf,p+1,msgLenIn-1);
            break;
         }
         case F_MSG_ID:
         {
            bufPut(&rbTxBuf,gID,19);
            break;
         }
         case F_MSG_COM:
         {
            break;
         }
         default:
         {
            break;
         }
      }

      cdcCTS = 1;        // Signal RX flow on, the PC may send data again
      gTimLoad = TIM_LED_CYCLE_LONG;
      msgRcvdCount = 0;
   }
}

static void appRfSenderTask(void)
{
   uint8_t msgLenIn;
   static uint8_t n = 1, *p = MRFI_P_PAYLOAD(pPktOut) + 1;

   msgLenIn = rbRxBuf.nBytes;

   if (msgLenIn)
   {
      cdcCTS = 0;
      halMcuWaitMs(1);                 // Wait for the PC to respond
      bufGet(&rbRxBuf, p, msgLenIn);
      n += msgLenIn;
      HAL_LED_G_TGL;
      if (p[msgLenIn-1] < 32)
      {
         *MRFI_P_PAYLOAD(pPktOut) = F_MSG_TXT;
         rfBroadcast(n);
         n = 1;
         p = MRFI_P_PAYLOAD(pPktOut) + 1;
      }
      else
      {
         p += msgLenIn;
      }
      cdcCTS = 1;
   }
}

BSP_ISR_FUNCTION(mytimer, T4_VECTOR)
{
#if (TARGET_CONFIG == SVR_TARGET)
#elif (TARGET_CONFIG == CLT_TARGET)
   if (T4OVFIF)   /* 24MHz period * 128 * 512 = 2.73msec */
   {
      gTim--;
      T4OVFIF = 0;
   }
#endif
 //  usbUartProcess();
}

