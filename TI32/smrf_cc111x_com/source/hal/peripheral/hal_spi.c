/*
 * hal_spi.c
 *
 *  Created on: Feb 10, 2017
 *      Author: eddie.llerena
 */

/***************************** INCLUDES *****************************/
#include "hal_board.h"
#include "hal_spi.h"

/******************* LOCAL CONSTANTS and DEFINITIONS *******************/
#define HAL_SPI_P0				(0xf << 2)


/********************** GLOBAL VARIABLES **********************/

/********************** LOCAL VARIABLES **********************/
// Baud rate settings
#if BSP_CONFIG_CLOCK_MHZ==32

#define BAUD_M_38400                 59
#define BAUD_M_57600                216
#define BAUD_M_115200               216
#define BAUD_E_38400                 10
#define BAUD_E_57600                 10
#define BAUD_E_115200                11

#elif BSP_CONFIG_CLOCK_MHZ==26

#define BAUD_M_38400                131
#define BAUD_M_57600                 34
#define BAUD_M_115200                34
#define BAUD_E_38400                 10
#define BAUD_E_57600                 11
#define BAUD_E_115200                12

#elif BSP_CONFIG_CLOCK_MHZ==24

#define BAUD_M_38400                163
#define BAUD_M_57600                 59
#define BAUD_M_115200                59
#define BAUD_E_38400                 10
#define BAUD_E_57600                 11
#define BAUD_E_115200                12

#else
#error "Clock speed not defined!"
#endif

/****************************** LOCAL FUNCTIONS ******************************/
/**
 *	@brief	This function prints a character string to the UART.<br>
 *			It will stop after the first non-printable character -expected to be '\r'.
 *	@param	s is a pointer to the start of the string.
*/
uint16_t halSpiSend(const uint8_t * s, uint16_t len)
{
	uint16_t l;

	l = len;
	do
	{
		U0DBUF = *s++;								/* Send the character */
		while (!(UTX0IF));							/* USCI_A0 TX buffer empty? */
		UTX0IF = 0;									/* clear TX emty flag */
	} while (--l);									/* repeat <len> times */

	return len;
}

/**
@brief	Initalise UART. Supported baudrates are: 38400, 57600 and 115200
@param	uint8_t baudrate
		uint8_t options - this parameter is ignored
*/
void halSpiInit(void)
{
	PERCFG &= ~PERCFG_U0CFG;					/* set USART0 location 1 */
	P0SEL |= HAL_SPI_P0;							/* set P0.2..P0.5 pins as alternate functions */

	U0CSR = U0CSR_RE;								/* SPI Master, Rx enabled */
	U0UCR|= U0UCR_FLUSH;							/* SPI flush */
	U0BAUD = BAUD_M_115200;						/* set baud rate */
	U0GCR = BAUD_E_115200 | U0GCR_ORDER;	/* set baud rate, POL negative, Phase 1st edge, LCD first */

	URX0IF = 0;										/* Prepare for reception */

	halSpiSend(__DATE__ " " __TIME__ "\r", 22);
}



