/*
 * hal_spi.h
 *
 *  Created on: Feb 10, 2017
 *      Author: eddie.llerena
 */

#ifndef SOURCE_HAL_PERIPHERAL_HAL_SPI_H_
#define SOURCE_HAL_PERIPHERAL_HAL_SPI_H_

#ifdef __cplusplus
extern "C" {
#endif

/***************************** INCLUDES *****************************/
#include "bsp.h"

/*********************** CONSTANTS AND DEFINES ***********************/

/*********************** GLOBAL FUNCTIONS ***********************/
void halSpiInit(void);
uint16_t halSpiSend(const uint8_t * s, uint16_t len);

/*********************** GLOBAL VARIABLES ***********************/
//extern uint8_t gSPImsg[];


#ifdef  __cplusplus
}
#endif


#endif /* SOURCE_HAL_PERIPHERAL_HAL_SPI_H_ */
