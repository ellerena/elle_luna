/***********************************************************************************
  Filename:     hal_mcu.h

  Description:  hal mcu library header file

***********************************************************************************/

#ifndef HAL_MCU_H
#define HAL_MCU_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** INCLUDES *****************************/
#include "hal_defs.h"

/***************************** masks *****************************/
// Bit masks to check SLEEP register

/***************************** macros *****************************/
#define IS_HFRC_STABLE()    (SLEEP & SLEEP_HFRC_S)     // Macro for checking status of the high frequency RC oscillator.
#define IS_XOSC_STABLE()    (SLEEP & SLEEP_XOSC_S)     // Macro for checking status of the crystal oscillator

/*********************** CONSTANTS AND DEFINES ***********************/


/*********************** GLOBAL FUNCTIONS ***********************/
void halMcuWaitMs(uint16_t msec);

#ifdef  __cplusplus
}
#endif

#endif
