/***********************************************************************************
    Filename:     usb_interrupt.c
    Description:  USB library interrupt initialisation and ISR.
***********************************************************************************/

#define USBINTERRUPT_C ///< Modifies the behavior of "EXTERN" in usb_interrupt.h

#include "usb_firmware_library_headers.h"
#include "hal_board.h"

/**
 * @brief Initializes the module
 * This function should be called after the @ref module_usb_framework module has been initialized.
 * Use interrupt group priority control (refer to the CC2511/CC1111 datasheet) to adjust the priority of the
 * USB interrupt relative to other interrupts.
 *
 * @param[in]       irqMask
 *     A bit mask containing USBIRQ_EVENT bits for all events that shall be reported
 */
void usbirqInit(void)
{
   usbirqData.eventMask = 0x0000;
   usbirqData.inSuspend = FALSE;
   usbirqData.irqMask = USBIRQ_EVENT_ALL;

   // Select IRQ flags to handle
   USBCIE = (uint8_t)(USBIRQ_EVENT_ALL);        // Common USB Interrupt Enable Mask
   USBIIE = (uint8_t)(USBIRQ_EVENT_ALL >> 4);   // In Endpoints and EP0 Interrupt Enable Mask
   USBOIE = (USBIRQ_EVENT_ALL >> 9) & 0x3E;     // Out Endpoints Interrupt Enable Mask

   // Configure P0 for rising edge detection on P0[7:4], but keep the irq disabled until needed.
   PICTL |= 0x10;
   HAL_USB_RESUME_INT_CLEAR();
   HAL_USB_INT_CLEAR();
   HAL_USB_INT_ENABLE();
}

/**
 * @brief USB interrupt handler
 * Clears the P2 interrupt flag and converts all USB interrupt flags into events.
 * The interrupt also lets @ref usbsuspEnter() break from the suspend loop.
 */
BSP_ISR_FUNCTION(usbirqHandler, P2INT_VECTOR)
{
   uint8_t usbcif;

   while (!IS_XOSC_STABLE());             // First make sure that the crystal oscillator is stable

   usbcif = USBCIF;
   if (usbcif & USBCIF_RSTIF)             // Special handling for reset interrupts
   {   // All interrupts (except suspend) are by default enabled by hardware, so
       // re-initialize the enable bits to avoid unwanted interrupts
       USBCIE = usbirqData.irqMask;
       USBIIE = usbirqData.irqMask >> 4;
       USBOIE = (usbirqData.irqMask >> 9) & 0x3E;

       USBPOW |= USBPOW_SUSPEND_EN;       // Enable suspend mode when suspend signaling is detected on the bus
   }

   // Record events (keeping existing)
   usbirqData.eventMask |= (uint16_t)usbcif;
   usbirqData.eventMask |= (uint16_t)USBIIF << 4;
   usbirqData.eventMask |= (uint16_t)USBOIF << 9;

   // If we get a suspend event, we should always enter suspend mode. We must,
   // however be sure that we exit the suspend loop upon resume or reset signaling.
   if (usbcif & USBCIF_SUSPENDIF)
   {
      usbirqData.inSuspend = TRUE;
   }
   if (usbcif & (USBCIF_RSTIF | USBCIF_RESUMEIF))
   {
      usbirqData.inSuspend = FALSE;
   }

   usbirqHookProcessEvents();                  // Hand them over to the application
   HAL_USB_INT_CLEAR();                        // Clear the P2 interrupt flag
}

/**
 * @brief USB resume interrupt handler
 * This routine clears the USB resume interrupt flag, and makes sure that MCU does not return to power
 * mode 1 again until the the suspend loop has been exited.
 */
BSP_ISR_FUNCTION(usbirqResumeHandler, P0INT_VECTOR)
{
   uint8_t flags;

   while (!IS_XOSC_STABLE());       // First make sure that the crystal oscillator is stable
                                    // Clear and disable the interrupt flag
   flags = P0IFG;
   if (flags & 0x80)                // We have a USB_RESUME interrupt (which could also be a USB reset)
   {
      HAL_USB_RESUME_INT_DISABLE();
      usbsuspStopPm1();
   }

   HAL_USB_RESUME_INT_CLEAR();

}
