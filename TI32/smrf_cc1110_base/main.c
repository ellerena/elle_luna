/*
 * main.c
 *
 *  Created on: Apr 21, 2015
 *      Author: eddie.llerena
 */

#include "main.h"

unsigned i = 0xffff;
FARVAR char t[] = "Ezequiel Edison Llerena Gonzalez";
char *c = t;

int main(void)
{
	MACRO_WDT_INIT											/* Stop Watch Dog timer (default at reset) */
	MACRO_CLK_INIT											/* setup clocks using external xtals */
//	MACRO_TIMER_INIT										/* Reset the timer and connect it to SMCLK */
	MACRO_GPIO_INIT											/* configure GPIO port pins */
	MACRO_COM_INIT											/* configure UART @ 115200-8N1 */
//	MACRO_ADC10_INIT										/* configure ADC10 to read P4.3..4.6 */

	EA = 1;
	COM_TX_t(t, sizeof(t));									/* print welcome msg */

	TURN_OFF_LED(LED_GREEN);

	while(1)
	{
		unsigned k;
		k = i;
		TOGGLE_LED(LED_RED);
		while(k--);
		k = i;
		while(k--);
	}
}

//__bsp_ISR_FUNCTION__(i_user_button, P1INT_VECTOR)
ISR_FUNCTION(isr_button, P1INT_VECTOR)
{
	if (P1IFG & BUTN_S1)
	{
		TOGGLE_LED(LED_GREEN);								/* force PUC */
		COM_TX_t(c, 1);
		if ((c - t)> sizeof(t))
			c = t;
		else
			c++;

	}

	if (P1IFG & BUTN_S2)
	{
		if (i >= 0x8000)
			i = 0x2;
		else
			i <<= 2 ;
	}
	P1IFG = 0;												/* Clear pending interrupts - pin 2 */
	P1IF = 0;												/* clear IRCON2 flag */
}
