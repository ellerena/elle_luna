/***********************************************************************************

    Filename:	  mrfi_link.h

	Description:  MRFI link header file.

***********************************************************************************/

#ifndef MRFI_LINK_H
#define MRFI_LINK_H

#include "mrfi.h"

#define SVR_TARGET      's'
#define CLT_TARGET      'c'

#define TARGET_CONFIG CLT_TARGET

#ifndef TARGET_CONFIG
#define TARGET_CONFIG SVR_TARGET
#endif

/*********************** CONSTANTS AND DEFINES ***********************/
#define DEV_ADDR_LOC                0x25DE
#define DEV_ADDR_REM                DEV_ADDR_LOC    // 0x25EB
#define MRFI_CHANNEL                0

#define  F_MSG_ID                (4)
#define  F_MSG_COM               (2)      /* message conveys executable commands */
#define  F_MSG_TXT               (1)      /* message conveys a text string */
#define  F_MSG_BIN               (0)      /* message conveys binary data values */

#define  MRFI_TX_RESULT_ACK_TIMEOUT    (3)

extern volatile IDATA uint8_t flags;
extern mrfiPacket_t * CODE pPktIn;
extern mrfiPacket_t * CODE pPktOut;

/************************ PUBLIC FUNCTIONS ************************/
void  rfInit(void);
uint8_t rfRecv(void);
void rfBroadcast(uint16_t len);
uint8_t rfDataRdy(void);
void rfBroadcastDataAsTxt(uint8_t* data, uint16_t len);
void rfBroadcastTxt(char* txt, uint16_t len);

#endif
