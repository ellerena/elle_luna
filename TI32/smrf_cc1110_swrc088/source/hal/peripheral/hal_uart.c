/***********************************************************************************

  Filename:     hal_uart.c

  Description:  UART interface to UART0 at P0. Works with HW flow control and
                optionally with buffered TX transmission. Reception is always
                buffered. Works with 8051 based SoCs.

***********************************************************************************/

/***************************** INCLUDES *****************************/
#include "hal_board.h"
#include "hal_uart.h"
#include "mrfi_link.h"

/******************* LOCAL CONSTANTS and DEFINITIONS *******************/
#define WELCOME_MSG              "CC1110-miniDev " __DATE__ " " __TIME__ "\r"
#define HAL_UART_RX_TX	   		(3 << 6)	// Peripheral Pin Select for Rx/Tx.
#define TX_DATA_OFS 			   	((uint8_t *)MRFI_P_PAYLOAD(pPktOut) + 2)

/********************** LOCAL VARIABLES **********************/
// Baud rate settings
#if BSP_CONFIG_CLOCK_MHZ==32
#define BAUD_M_38400                 59
#define BAUD_M_57600                216
#define BAUD_M_115200               216
#define BAUD_E_38400                 10
#define BAUD_E_57600                 10
#define BAUD_E_115200                11

#elif BSP_CONFIG_CLOCK_MHZ==26
#define BAUD_M_38400                131
#define BAUD_M_57600                 34
#define BAUD_M_115200                34
#define BAUD_E_38400                 10
#define BAUD_E_57600                 11
#define BAUD_E_115200                12

#elif BSP_CONFIG_CLOCK_MHZ==24
#define BAUD_M_38400                163
#define BAUD_M_57600                 59
#define BAUD_M_115200                59
#define BAUD_E_38400                 10
#define BAUD_E_57600                 11
#define BAUD_E_115200                12

#else
#error "Clock speed not defined!"
#endif

/****************************** LOCAL FUNCTIONS ******************************/
/**
@brief	Initalise UART. Supported baudrates are: 38400, 57600 and 115200
@param	uint8_t baudrate
		uint8_t options - this parameter is ignored
*/
void halUartInit(void)
{
	PERCFG |= PERCFG_U1CFG;		   				/* set UART1 in alt 2 location */
	P1SEL |= HAL_UART_RX_TX;						/* set P1.6 & P1.7 pins in alternate functions RX and TX */

	U1CSR = (U1CSR_MODE | U1CSR_RE);				/* USART1 in UART mode, Rx enable, Idle */
	U1UCR|= (U1UCR_FLUSH | U1UCR_STOP);			/* USART1 flush, use 1 stop */
	U1BAUD = BAUD_M_115200;							/* set baud rate */
	U1GCR = BAUD_E_115200;							/* set baud rate */

	COM_puts(WELCOME_MSG);

	URX1IF = 0; 										/* Prepare for reception */
}

/**
 *	@brief	This function prints a character string to the UART.<br>
 *			It will stop after the first non-printable character -expected to be '\r'.
 *	@param	s is a pointer to the start of the string.
*/
void COM_send(const uint8_t * s, uint16_t len)
{
   while (len--)                        /* repeat <len> times */
	{
      UTX1IF = 0;                       /* clear TX empty flag */
      U1DBUF = *s++;                    /* Send the character */
	   while (!UTX1IF);                  /* wait for TX buffer empty */
	}
}

/**
 * @brief prints a string of characters to the com port.
 *          it prints all characters in the string until a non
 *          printable is found.
 * @param   *s, is a pointer to the string location.
 */
void COM_puts(const uint8_t * s)
{
   uint8_t c;

   do
   {
      c = *s++;
      if (c < 32) c = '\r';
      UTX1IF = 0;
      U1DBUF = c;
      while (!UTX1IF);             /* USCI_A1 TX buffer empty? */
   } while(c != '\r');
}

/**
 * @brief   This function converts and prints an unsigned char using hex format display.
 * @param   n is the unsigned char number (between o and 255)
 */
void COM_TXHex(uint16_t v)
{
   char msg[] = {"x0000 "}, c;
   unsigned i= 4;

   do
   {
      c = v & 0x0f;                         /* extract less significant nibble for processing */
      msg[i] = c < 0x0a ? c+0x30 : c+0x37;  /* convert the nibble into a printable hex */
      v>>=4;                                /* extract more significant nibble for processing */
   } while(--i);

   COM_send(msg, 6);                        /* send the whole thing for printing */

}

BSP_ISR_FUNCTION(uart1RxISR,URX1_VECTOR)
{
	uint8_t	t;
	static uint8_t *pacCommand = TX_DATA_OFS;		/* pointer used to write characters recvd by UART1 */
	static uint8_t len = 2;

	URX1IF = 0;
	t = U1DBUF;
	*pacCommand++ = t;
	len++;
	if (t == '\r')							/* <return> ends the uart command entry */
	{
		MRFI_SET_PAYLOAD_LEN(pPktOut, len);
		pacCommand = TX_DATA_OFS;
		len = 2;
		flags = F_COM_RCVD;					/* rise a flag to alert main() */
	}
}




