/***********************************************************************************

  Filename:     hal_board.h

  Description:  SmartRF04 board with CC2430EM/CC2510EM/CC1110EM/CC2530EM.

***********************************************************************************/

#ifndef HAL_BOARD_H
#define HAL_BOARD_H

#include "hal_mcu.h"
#if (!defined BSP_NO_BUTTONS)
#include "bsp_buttons.h"
#endif
#include "bsp_leds.h"

/***************************** CONSTANTS *****************************/
// Clock
#if (chip==2430) || (chip==2431) || (chip==2530) || (chip==2531)
#define BSP_CONFIG_CLOCK_MHZ                32
#elif (chip==2510) || (chip==1110) || (chip==1111)
#define BSP_CONFIG_CLOCK_MHZ                (BSP_CLOCK_MHZ)
#endif

// USB
#define HAL_BOARD_IO_USB_ENABLE_PORT   1
#define HAL_BOARD_IO_USB_ENABLE_PIN    1

#define HAL_USB_ENABLE()           ( SLEEP |= SLEEP_USB_EN )

#define HAL_USB_PULLUP_ENABLE()    st( P1SEL &= ~BV(1); P1 |= BV(1); P1DIR |= BV(1);)

#define HAL_USB_INT_ENABLE()       st( IEN2|= 0x02; )
#define HAL_USB_INT_DISABLE()      st( IEN2&= ~0x02; )
#define HAL_USB_INT_CLEAR()        st( P2IFG= 0; P2IF= 0; )

#define HAL_USB_RESUME_INT_ENABLE() st( P0IE= 1;)
#define HAL_USB_RESUME_INT_DISABLE() st( P0IE= 0;)
#define HAL_USB_RESUME_INT_CLEAR()  st(P0IFG= 0; P0IF= 0; )

/******************************** MACROS ********************************/
// LEDs
#define HAL_LED_G_ON	   			BSP_TURN_ON_LED1()
#define HAL_LED_R_ON		   		BSP_TURN_ON_LED2()

#define HAL_LED_G_OFF				BSP_TURN_OFF_LED1()
#define HAL_LED_R_OFF				BSP_TURN_OFF_LED2()

#define HAL_LED_G_TGL				BSP_TOGGLE_LED1()
#define HAL_LED_R_TGL				BSP_TOGGLE_LED2()

// Buttons
#define HAL_BUTTON_1_PUSHED			BSP_BUTTON1()
#define HAL_BUTTON_2_PUSHED			BSP_BUTTON2()

#define MCU_IO_TRISTATE   1             // Used as "func" for the macros below
#define MCU_IO_PULLUP     2
#define MCU_IO_PULLDOWN   3

/************** TODO *****************/
typedef struct
{
    volatile uint8_t pData[BUF_SIZE];
    volatile uint8_t nBytes;
    volatile uint8_t iHead;
    volatile uint8_t iTail;
} ringBuf_t;


extern XDATA ringBuf_t rbRxBuf;
extern XDATA ringBuf_t rbTxBuf;

extern void bufInit(ringBuf_t *pBuf);
extern uint8_t bufPut(ringBuf_t *pBuf, const uint8_t *pData, uint8_t nBytes);

#endif

