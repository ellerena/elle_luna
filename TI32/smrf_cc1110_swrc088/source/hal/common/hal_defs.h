/***********************************************************************************
  Filename:     hal_defs.h

  Description:  HAL defines

***********************************************************************************/

#ifndef HAL_DEFS_H
#define HAL_DEFS_H

/***************************** INCLUDES *****************************/
#include "bsp.h"
#include <stdbool.h>
/******************************** MACROS ********************************/
#define TRUE			true
#define FALSE			false

#define	BIT0		BV(0)
#define	BIT1		BV(1)
#define	BIT2		BV(2)
#define	BIT3		BV(3)
#define	BIT4		BV(4)
#define	BIT5		BV(5)
#define	BIT6		BV(6)
#define	BIT7		BV(7)

/* uint16_t processing */
#define HI_UINT16(a) (((uint16_t)(a) >> 8) & 0xFF)
#define LO_UINT16(a) (a & 0xFF)

/********************** Compiler abstraction **********************/
/*** IAR MSP430 ***/
#ifdef __IAR_SYSTEMS_ICC__

#define _PRAGMA(x) _Pragma(#x)

#if defined __ICC430__

#ifndef CODE
#define CODE
#endif
#ifndef XDATA
#define XDATA
#endif
#define FAR
#define NOP()  asm("NOP")

#define HAL_ISR_FUNC_DECLARATION(f,v)  _PRAGMA(vector=v##_VECTOR) __interrupt void f(void)
#define HAL_ISR_FUNC_PROTOTYPE(f,v)    _PRAGMA(vector=v##_VECTOR) __interrupt void f(void)
#define HAL_ISR_FUNCTION(f,v)          HAL_ISR_FUNC_PROTOTYPE(f,v); HAL_ISR_FUNC_DECLARATION(f,v)

#elif defined __ICC8051__			/*** IAR 8051 ***/

#define FAR
#define NOP()							asm("NOP")

#define HAL_MCU_LITTLE_ENDIAN()			__LITTLE_ENDIAN__
#define HAL_ISR_FUNC_DECLARATION(f,v)   _PRAGMA(vector=v) __near_func __interrupt void f(void)
#define HAL_ISR_FUNC_PROTOTYPE(f,v)     _PRAGMA(vector=v) __near_func __interrupt void f(void)
#define HAL_ISR_FUNCTION(f,v)           HAL_ISR_FUNC_PROTOTYPE(f,v); HAL_ISR_FUNC_DECLARATION(f,v)

#else
#error "Unsupported IAR architecture"
#endif

/*** KEIL 8051 ***/
#elif defined __KEIL__
#include <intrins.h>

#define BIG_ENDIAN
#define __code  code		// Keil workaround
#define __xdata xdata		// Keil workaround
#define CODE   code
#define XDATA  xdata
#define FAR
#define NOP()  _nop_()

#define HAL_ISR_FUNC_DECLARATION(f,v)   void f(void) interrupt v
#define HAL_ISR_FUNC_PROTOTYPE(f,v)     void f(void)
#define HAL_ISR_FUNCTION(f,v)           HAL_ISR_FUNC_PROTOTYPE(f,v); HAL_ISR_FUNC_DECLARATION(f,v)

typedef unsigned short istate_t;

/*** WIN32 ***/
#elif defined WIN32

#define CODE
#define XDATA
#include "windows.h"
#define FAR far
#pragma warning (disable :4761)

/*** SDCC (TODO) ***/
#elif defined SDCC
typedef void (*VFPTR)(void);

/*** Code Composer Essential ***/
#elif defined __TI_COMPILER_VERSION__
#define CODE
#define XDATA
#define FAR

typedef unsigned short istate_t;

/*** Other compilers ***/
#else
#error "Unsupported compiler"
#endif

#endif

