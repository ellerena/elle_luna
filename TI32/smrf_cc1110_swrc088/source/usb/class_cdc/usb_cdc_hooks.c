/***********************************************************************************

    Filename: usb_cdc_hooks.c

    Contains the necessary hook functions for various USB request processing
    that is featured from the USB firmware library. Some
    functions are empty.

***********************************************************************************/


/**********************************************************************************
 * INCLUDES
 */

#include "usb_firmware_library_headers.h"
#include "usb_cdc.h"
#include "usb_uart.h"
#include "hal_board.h"

#define EP0_STATUS_STALL        st(usbfwData.ep0Status = EP_STALL;)     /* set STALL state for ep0 */


/* Global data */
extern XDATA CDC_LINE_CODING_STRUCTURE currentLineCoding;
extern uint16_t cdcRTS;

// *********************************************************************************
// All Hooks and functions required by the USB library.
// *********************************************************************************

// **************** Process USB class requests with OUT data phase *****************
/**
 * @brief   Process USB class requests with OUT data phase, or stall endpoint 0 when unsupported
 * */
void usbcrHookProcessOut(void)
{
   switch(usbSetupHeader.request)
   {
      case CDC_SET_CONTROL_LINE_STATE:// Control line state from host
         if(usbfwData.ep0Status == EP_IDLE)
         {
            cdcRTS = usbSetupHeader.value;
//            if (cdcRTS)           // TODO: temporarily removed (warning 126)
//            {
//               HAL_LED_G_ON;
//            }
//            else
//            {
//               HAL_LED_G_OFF;
//            }
            usbfwData.ep0Status = EP_RX;
         }
         break;

      case CDC_SET_LINE_CODING:
         if(usbfwData.ep0Status == EP_IDLE)
         {
            usbSetupData.pBuffer = (GENERIC uint8_t *) &currentLineCoding;
            usbfwData.ep0Status = EP_RX;
         }
         else if(usbfwData.ep0Status == EP_RX)
         {
         }
         break;

      default:                               // Unknown request?
         EP0_STATUS_STALL;
         break;
   }
}

/**
 * @brief   Process USB class requests with IN data phase, or stall endpoint 0 when unsupported
 *
 * */
void usbcrHookProcessIn(void)
{  // Process USB class requests with IN data phase, or stall endpoint 0 when unsupported
   if (usbSetupHeader.request == CDC_GET_LINE_CODING)
   {  // First the endpoint status is EP_IDLE...
      if (usbfwData.ep0Status == EP_IDLE)
      {
         usbSetupData.pBuffer = (GENERIC uint8_t *) &currentLineCoding;
         usbSetupData.bytesLeft = 7;
         usbfwData.ep0Status = EP_TX;
         // Then the endpoint status is EP_TX (remember: we did that here when setting up the buffer)
      }
      else if (usbfwData.ep0Status == EP_TX)
      {
         // usbfwData.ep0Status is automatically reset to EP_IDLE when returning to usbfwSetupHandler()
      }
   }
   else
   {
      EP0_STATUS_STALL;
   }
}

// ********************************  Unsupported USB hooks *************************
void usbvrHookProcessOut(void)      {EP0_STATUS_STALL;}
void usbvrHookProcessIn(void)       {EP0_STATUS_STALL;}

// ************************  unsupported/unhandled standard requests ***************
void usbsrHookSetDescriptor(void)   {EP0_STATUS_STALL;}
void usbsrHookSynchFrame(void)      {EP0_STATUS_STALL;}
void usbsrHookClearFeature(void)    {EP0_STATUS_STALL;}
void usbsrHookSetFeature(void)      {EP0_STATUS_STALL;}
void usbsrHookModifyGetStatus(uint8_t recipient, uint8_t index, XDATA uint16_t *pStatus) \
      {(void)recipient; (void)index; (void)pStatus;}

// ************************ USB standard request event processing ******************
void usbsrHookProcessEvent(uint8_t event, uint8_t index)
{
   (void)index;

   switch (event)   // Process relevant events, one at a time.
   {
      case USBSR_EVENT_CONFIGURATION_CHANGING : //(the device configuration is about to change)
         break;
      case USBSR_EVENT_CONFIGURATION_CHANGED :// (the device configuration has changed)
         break;
      case USBSR_EVENT_INTERFACE_CHANGING ://(the alternate setting of the given interface is about to change)
         break;
      case USBSR_EVENT_INTERFACE_CHANGED : //(the alternate setting of the given interface has changed)
         break;
      case USBSR_EVENT_REMOTE_WAKEUP_ENABLED ://(remote wakeup has been enabled by the host)
         break;
      case USBSR_EVENT_REMOTE_WAKEUP_DISABLED ://(remote wakeup has been disabled by the host)
         break;
      case USBSR_EVENT_EPIN_STALL_CLEARED ://(the given IN endpoint's stall condition has been cleared the host)
         break;
      case USBSR_EVENT_EPIN_STALL_SET ://(the given IN endpoint has been stalled by the host)
         break;
      case USBSR_EVENT_EPOUT_STALL_CLEARED ://(the given OUT endpoint's stall condition has been cleared the host)
         break;
      case USBSR_EVENT_EPOUT_STALL_SET ://(the given OUT endpoint has been stalled by the PC)
         break;
   }
}

// ************************ USB interrupt event processing *************************
void usbirqHookProcessEvents(void)
{
    // Handle events that require immediate processing here
}

