/*
 * header.h
 *
 *  Created on: Feb 1, 2018
 *      Author: eddie.llerena
 */

#ifndef SRC_HEADER_H_
#define SRC_HEADER_H_

#ifndef __SDCC_PIC12F1822
#define __SDCC_PIC12F1822
#endif

#include <pic16regs.h>

/****************************** Constants ******************************/
#define LOGO            "12F1822, " __DATE__ " " __TIME__ "\r\n"
#define FOSC            (800000)
#define FTIM            (FOSC/4)
#define START_BIT_H     (120)      /* min */
#define STOP_BIT_H      (250)      /* max */
#define BIT_IS_1        (35)       /* max */
#define GCOMINSZ        (12)

#define CONFIG_VAL      (_FCMEN_OFF & _IESO_OFF & _CLKOUTEN_OFF & _BOREN_OFF & _CPD_OFF & _CP_OFF & _MCLRE_OFF & _PWRTE_OFF & _WDTE_OFF & _WDTE_OFF & _FOSC_INTOSC)
#define CONFIG2_VAL     (_LVP_OFF & _BORV_LO & _STVREN_ON & _PLLEN_OFF & _WRT_OFF)
#define OPTION_REG_VAL  (0b11010110)

/******************************* Macros *******************************/
#define IR_IS_HIGH      (PORTAbits.RA2)
#define LED             (PORTAbits.RA4)
#define LED2            (PORTAbits.RA5)

extern void COM_init(void);
extern void COM_puts(const char * s);
extern void COM_TXHex(uint16_t v);


#endif /* SRC_HEADER_H_ */
