#ifndef __DEFINES_FTDI_TEST_APP_H__
#define __DEFINES_FTDI_TEST_APP_H__

#include "ftd2xx.h"

#ifdef __cplusplus
extern "C" {
#endif

#define	USB_BUF_SZ_W		(32*32*256*2)	     	/* USB buffer size for one Big Red data block [bytes] */
#define	USB_DEV_NAME		"Sona_xxxx"

#define	FIFO245_MASK		(0xff)						/* bit mask : 0=input, 1=output */
#define	FIFO245_MODE		(0x40)						/* Synchronous 245 FIFO Mode (FT2232H and FT232H devices only) */
#define	FIFO245_RST             (0x00)						/* reset mode */
#define	FIFO245_LATENCY	(2)							/* FTDI recommendation for best performance */
#define	FIFO245_USB_PARAM	(0x10000)					/* FTDI recommendation for best performance */

/* some convenient FTDI definitions */
#define	FT_PURGE_RX_TX       FT_PURGE_RX | FT_PURGE_TX

#define	FtListDeviceInfo     FT_CreateDeviceInfoList
#define	FtGetDevicesInfo     FT_GetDeviceInfoList
#define	FtGetDevicesDetail   FT_GetDeviceInfoDetail

#define	FtGetQueueSt         FT_GetQueueStatus

#define	FtOpenEx             FT_OpenEx
#define	FtOpen               FT_Open
#define	FtRead               FT_Read
#define	FtWrite              FT_Write
#define	FtClose              FT_Close
#define	FtPurge              FT_Purge

#define	FtGetBitMod          FT_GetBitMode
#define	FtSetBitMod          FT_SetBitMode

#define	FtGetStatus          FT_GetStatus
#define	FtCyclePort          FT_CyclePort
#define	FtResetPort          FT_ResetPort
#define FtReset              FT_ResetDevice

#define	FtEe_Read            FT_EE_Read
#define	FtSetLatencyTimer    FT_SetLatencyTimer
#define	FtSetUsbParam        FT_SetUSBParameters
#define	FtSetFlowCtrl        FT_SetFlowControl

#define	FtLibraryVersion     FT_GetLibraryVersion
#define	FtDriverVersion      FT_GetDriverVersion

#ifdef __cplusplus
}
#endif

#endif
