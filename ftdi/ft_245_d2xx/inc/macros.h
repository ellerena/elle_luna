/*
 * macros.h
 *
 *  Created on: Jul 30, 2020
 *      Author: Eddie Llerena
 */

#ifndef INC_MACROS_H_
#define INC_MACROS_H_

#define st(x)              do{x} while (0)
#define PRNR(...)          st(printf(__VA_ARGS__);return __LINE__;)
#define PRNE(...)          st(printf(__VA_ARGS__);goto exit;)
#define PRNDBGL            printf("%d\n", __LINE__);
#define IFT(x)             if(FT_OK != (x))
#define IFF(x)             if(FT_OK == (x))
#define MALLOC(v,t,s)      do {v = (t)malloc(s);} while (0)
#define MALLOCR(v,t,s,e)   if (NULL == (v = (t)malloc(s))) { \
                           printf("[%d] malloc failed\n", __LINE__); \
                           return e; }
#define MALLOCE(v,t,s)     if (NULL == (v = (t)malloc(s))) { \
                           printf("[%d] malloc failed\n", __LINE__); \
                           retCode = 1; goto exit; }

#endif /* INC_MACROS_H_ */
