/*
   To build use the following gcc statement
   (assuming you have the d2xx library in the /usr/local/lib directory).
   gcc -o read main.c -L. -lftd2xx -Wl,-rpath,/usr/local/lib
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "ftd2xx.h"
#include "macros.h"

void PrintData (FT_DEVICE ftDevice, const FT_PROGRAM_DATA * pData);

int main(int argc, char *argv[])
{
   FT_STATUS	ftStatus;
   FT_HANDLE	ftHandle0;
   int iport;
   static FT_PROGRAM_DATA Data;
   static FT_DEVICE ftDevice;
   DWORD libraryVersion = 0;
   int retCode = 0;

   IFT(ftStatus = FT_GetLibraryVersion(&libraryVersion))
   {
      PRNR("Error reading library version.\n");
   }
   printf("Library version = 0x%x\n", (unsigned int)libraryVersion);

   iport = 0;
   if(argc > 1) {
      sscanf(argv[1], "%d", &iport);
   }

   printf("Opening port %d\n", iport);
   IFT(ftStatus = FT_Open(iport, &ftHandle0))
   {  /* remember to rmmod ftdi_sio and usbserial */
      PRNR("FT_Open(%d) failed\n", iport);
   }

   printf("FT_Open succeeded.  Handle is %p\n", ftHandle0);

   IFT(ftStatus = FT_GetDeviceInfo(ftHandle0, &ftDevice, NULL, NULL, NULL, NULL))
   {
      printf("FT_GetDeviceType FAILED!\n");
      retCode = 1;
      goto exit;
   }

   printf("FT_GetDeviceInfo succeeded.  Device is type %d.\n", (int)ftDevice);

   /* MUST set Signature1 and 2 before calling FT_EE_Read */
   Data.Signature1 = 0x00000000;
   Data.Signature2 = 0xffffffff;
   MALLOCE(Data.Manufacturer, char *, 256); /* E.g "FTDI" */
   MALLOCE(Data.ManufacturerId, char *, 256); /* E.g. "FT" */
   MALLOCE(Data.Description, char *, 256); /* E.g. "USB HS Serial Converter" */
   MALLOCE(Data.SerialNumber, char *, 256); /* E.g. "FT000001" if fixed, or NULL */

   IFT(ftStatus = FT_EE_Read(ftHandle0, &Data))
   {
      printf("FT_EE_Read failed\n");
      retCode = 1;
      goto exit;
   }

   PrintData (ftDevice, &Data);

   exit:
   free(Data.Manufacturer);
   free(Data.ManufacturerId);
   free(Data.Description);
   free(Data.SerialNumber);
   FT_Close(ftHandle0);
   printf("Returning %d\n", retCode);
   return retCode;
}

void PrintData (FT_DEVICE ftDevice, const FT_PROGRAM_DATA * pData)
{
   printf("FT_EE_Read succeeded.\n\n");

   printf("Signature1 = %d\n", (int)pData->Signature1);
   printf("Signature2 = %d\n", (int)pData->Signature2);
   printf("Version = %d\n", (int)pData->Version);

   printf("VendorId = 0x%04X\n", pData->VendorId);
   printf("ProductId = 0x%04X\n", pData->ProductId);
   printf("Manufacturer = %s\n", pData->Manufacturer);
   printf("ManufacturerId = %s\n", pData->ManufacturerId);
   printf("Description = %s\n", pData->Description);
   printf("SerialNumber = %s\n", pData->SerialNumber);
   printf("MaxPower = %d\n", pData->MaxPower);
   printf("PnP = %d\n", pData->PnP) ;
   printf("SelfPowered = %d\n", pData->SelfPowered);
   printf("RemoteWakeup = %d\n", pData->RemoteWakeup);

   if (ftDevice == FT_DEVICE_BM)
   {
      /* Rev4 (FT232B) extensions */
      printf("BM:\n");
      printf("---\n");
      printf("\tRev4 = 0x%X\n", pData->Rev4);
      printf("\tIsoIn = 0x%X\n", pData->IsoIn);
      printf("\tIsoOut = 0x%X\n", pData->IsoOut);
      printf("\tPullDownEnable = 0x%X\n", pData->PullDownEnable);
      printf("\tSerNumEnable = 0x%X\n", pData->SerNumEnable);
      printf("\tUSBVersionEnable = 0x%X\n", pData->USBVersionEnable);
      printf("\tUSBVersion = 0x%X\n", pData->USBVersion);
   }

   if (ftDevice == FT_DEVICE_2232C)
   {
      /* Rev 5 (FT2232C) extensions */
      printf("2232RC:\n");
      printf("-------\n");
      printf("\tRev5 = 0x%X\n", pData->Rev5);
      printf("\tIsoInA = 0x%X\n", pData->IsoInA);
      printf("\tIsoInB = 0x%X\n", pData->IsoInB);
      printf("\tIsoOutA = 0x%X\n", pData->IsoOutA);
      printf("\tIsoOutB = 0x%X\n", pData->IsoOutB);
      printf("\tPullDownEnable5 = 0x%X\n", pData->PullDownEnable5);
      printf("\tSerNumEnable5 = 0x%X\n", pData->SerNumEnable5);
      printf("\tUSBVersionEnable5 = 0x%X\n", pData->USBVersionEnable5);
      printf("\tUSBVersion5 = 0x%X\n", pData->USBVersion5);
      printf("\tAIsHighCurrent = 0x%X\n", pData->AIsHighCurrent);
      printf("\tBIsHighCurrent = 0x%X\n", pData->BIsHighCurrent);
      printf("\tIFAIsFifo = 0x%X\n", pData->IFAIsFifo);
      printf("\tIFAIsFifoTar = 0x%X\n", pData->IFAIsFifoTar);
      printf("\tIFAIsFastSer = 0x%X\n", pData->IFAIsFastSer);
      printf("\tAIsVCP = 0x%X\n", pData->AIsVCP);
      printf("\tIFBIsFifo = 0x%X\n", pData->IFBIsFifo);
      printf("\tIFBIsFifoTar = 0x%X\n", pData->IFBIsFifoTar);
      printf("\tIFBIsFastSer = 0x%X\n", pData->IFBIsFastSer);
      printf("\tBIsVCP = 0x%X\n", pData->BIsVCP);
   }

   if (ftDevice == FT_DEVICE_232R)
   {
      /* Rev 6 (FT232R) extensions */
      printf("232R:\n");
      printf("-----\n");
      printf("\tUseExtOsc = 0x%X\n", pData->UseExtOsc);       // Use External Oscillator
      printf("\tHighDriveIOs = 0x%X\n", pData->HighDriveIOs);       // High Drive I/Os
      printf("\tEndpointSize = 0x%X\n", pData->EndpointSize);       // Endpoint size

      printf("\tPullDownEnableR = 0x%X\n", pData->PullDownEnableR);    // non-zero if pull down enabled
      printf("\tSerNumEnableR = 0x%X\n", pData->SerNumEnableR);     // non-zero if serial number to be used

      printf("\tInvertTXD = 0x%X\n", pData->InvertTXD);       // non-zero if invert TXD
      printf("\tInvertRXD = 0x%X\n", pData->InvertRXD);       // non-zero if invert RXD
      printf("\tInvertRTS = 0x%X\n", pData->InvertRTS);       // non-zero if invert RTS
      printf("\tInvertCTS = 0x%X\n", pData->InvertCTS);       // non-zero if invert CTS
      printf("\tInvertDTR = 0x%X\n", pData->InvertDTR);       // non-zero if invert DTR
      printf("\tInvertDSR = 0x%X\n", pData->InvertDSR);       // non-zero if invert DSR
      printf("\tInvertDCD = 0x%X\n", pData->InvertDCD);       // non-zero if invert DCD
      printf("\tInvertRI = 0x%X\n", pData->InvertRI);            // non-zero if invert RI

      printf("\tCbus0 = 0x%X\n", pData->Cbus0);            // Cbus Mux control
      printf("\tCbus1 = 0x%X\n", pData->Cbus1);            // Cbus Mux control
      printf("\tCbus2 = 0x%X\n", pData->Cbus2);            // Cbus Mux control
      printf("\tCbus3 = 0x%X\n", pData->Cbus3);            // Cbus Mux control
      printf("\tCbus4 = 0x%X\n", pData->Cbus4);            // Cbus Mux control

      printf("\tRIsD2XX = 0x%X\n", pData->RIsD2XX); // non-zero if using D2XX
   }
}



