// ftdi_test_app.c
//

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef _MSC_VER				/* below code only for use in Visual Studio */
#include <curses.h>
#include <sys/time.h>
#include <unistd.h>
#define SLEEPUS         usleep
#else
#include <windows.h>
#define SLEEPUS         Sleep
#endif
#include "macros.h"
#include "ftdi_test_app.h"

#define VER_PRODUCT     0
#define VER_DESIGN      0
#ifdef _DEBUG
#define VER_RELEASE     1					/* release number (odd = debug) */
#else
#define VER_RELEASE     0					/* release number (even = release) */
#endif
#define VER_BUILD       0
#define BUILD_DAT_TIM   "Enavili FTDI Test App v." STR(VER_PRODUCT) "." STR(VER_DESIGN) "." STR(VER_RELEASE) "." STR(VER_BUILD) " " __DATE__ " " __TIME__

#define MAX_STR_LEN     256

#define TASK         USB_buf[0]
#define MODE         USB_buf[1]
#define DBGL         printf("%u\n", __LINE__);

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

FT_HANDLE hUsb;       /* this will be our main handler */
uint8_t *USB_buf = NULL;    /* pointer to our USB data buffer */

/**
 This function performs the low level USB
 initialization and configuration.
 return: 0 (FT_OK) on success otherwise an FTDI error code
 */
static FT_STATUS USB_Init(void) {
   FT_STATUS ftStatus;
   int NumDev, i;
   FT_DEVICE_LIST_INFO_NODE *devInfo;

   hUsb = NULL;                                                /* start with a NULL pointer */
   if (!USB_buf)                                               /* if 1st time, allocate USB buffer */
   {                                                           /* 1 block = 32x32x256x2bytes = 512kB */
      MALLOCR(USB_buf, uint8_t*, USB_BUF_SZ_W, FT_INSUFFICIENT_RESOURCES)
   }

   memset(USB_buf, 0, sizeof(FT_DEVICE_LIST_INFO_NODE));       /* zero a few bytes only */

   IFF(ftStatus = FtListDeviceInfo((LPDWORD) &NumDev))         /* list all attached FTDI devices */
   { /* list created ok */
      ftStatus = FT_DEVICE_NOT_FOUND;                          /* assume it will fail */
      if (NumDev) {                                            /* is at least one FTDI is present... */

         MALLOCR(devInfo, FT_DEVICE_LIST_INFO_NODE*,
               sizeof(FT_DEVICE_LIST_INFO_NODE) * NumDev, -1); /* allocate space for all info */

         FtGetDevicesInfo(devInfo, (LPDWORD) &NumDev);         /* get the device information list */
         for (i = 0; i < NumDev; i++) {
            if (!strncmp(USB_DEV_NAME, devInfo[i].SerialNumber, 4)) /* first device with name alike */
            {
               if (devInfo[i].ftHandle) {                      /* if device already open, use its handle */
                  hUsb = devInfo[i].ftHandle;
                  ftStatus = FT_OK;
               } else {                                        /* otherwise, open it and get handle */
                  ftStatus = FtOpen(i, &hUsb);
                  devInfo[i].ftHandle = hUsb;
               }
                                                               /* copy device's info to our buffer */
               memcpy(USB_buf, &devInfo[i], sizeof(FT_DEVICE_LIST_INFO_NODE));
               break;
            }
         }
         free(devInfo);
      }
   }

   if (ftStatus) /* if no device found so far exit */
   {
      return ftStatus;
   }

   /*    Configure the fifo 245 Sync mode
    * 	 Mode values:
    0x0= Reset
    0x1= Asynchronous Bit Bang
    0x2= MPSSE (FT2232, FT2232H, FT4232H and FT232H devices only)
    0x4= Synchronous Bit Bang (FT232R, FT245R, FT2232, FT2232H, FT4232H and FT232H devices only)
    0x8= MCU Host Bus Emulation Mode (FT2232, FT2232H, FT4232H and FT232H devices only)
    0x10 = Fast Opto-Isolated Serial Mode (FT2232, FT2232H, FT4232H and FT232H devices only)
    0x20 = CBUS Bit Bang Mode (FT232R and FT232H devices only)
    0x40 = Single Channel Synchronous 245 FIFO Mode (FT2232H and FT232H devices only)
    */
   FtSetBitMod(hUsb, FIFO245_MASK, FIFO245_RST); /* reset synchronous FIFO-245 */
   SLEEPUS(10000);
   FtSetBitMod(hUsb, FIFO245_MASK, FIFO245_MODE); /* use synchronous FIFO-245 */
   FtSetUsbParam(hUsb, FIFO245_USB_PARAM, FIFO245_USB_PARAM); /* In, Out; (OUT - not supported) ?? */

   /*
    timeout is programmable and can be set at 1ms intervals between 2ms and 255ms.
    This allows the device to be optimized for protocols requiring faster response times
    */
   FtSetLatencyTimer(hUsb, FIFO245_LATENCY); /* 2-255; 16 default 2=best performance ?? */
   ftStatus = FtSetFlowCtrl(hUsb, FT_FLOW_RTS_CTS, 0, 0); /* 0x11, 0x13); */

   return ftStatus;
}

DWORD ftreaddata(void) {
   DWORD USB_data_cnt = 0;

   FtGetQueueSt(hUsb, &USB_data_cnt); /* checks for data available at RX */

   if (USB_data_cnt) { /* data available for pick up */

      FtRead(hUsb, (void*) USB_buf, USB_data_cnt, &USB_data_cnt); /* read data */

   }

   return USB_data_cnt;
}

void printBuf(uint32_t len) {
   uint32_t i;
   printf("Reading %d\n", len);

   if (len > 200)
      len = 200;

   for (i = 0; i < len; ++i) {
      // if (!(i % 16))
      //    printf("\n");
      printf("%c", USB_buf[i]);
   }
   printf("\n");
}

/**
 @brief   converts a string of chars representing a vector of hex numbers into a vector of bytes equivalent.
 Note: if first value is lower than 1fh (e.g. tab) then next bytes remain intact.
 @param   p contains the address of the first char in the string.
 @retval  the count of numbers comverted.
 @verbatim
 Text entered by a user via the keyboard is in the form of a string of printable chars that represent hex numbers.
 This functions takes that string of chars and extracts the meaningful hex numbers represented in it, then
 it places those numbers (bytes) in an array at the same ram location. Returning the number of numbers
 converted.
 @endverbatim
 */
//static int ParseCharsToValueHex(char *p)
//{
//   char *q, d;
//   int i, k;
//   uint8_t c;
//
//   q = p;
//   k = 0;
//
//   while (*p > 0x1f)                         /* Will process until a non printable character is found */
//   {
//      d = 0;                                 /* initialize the 'number equivalent value' */
//      i = 0;                                 /* initialize the digit counter (assume 2 digits max per number)*/
//      do
//      {
//         d <<= 4;                            /* each digit uses the first 4 bits, so on each loop we must push the others */
//         c = *p++;                           /* read the new char */
//         if ((c > 0x29) && (c < 0x3a))       /* char is between 0 and 9 */
//         {
//            d += (c - 0x30);                 /* add the char equivalen value to our total */
//            i++;                             /* increment our digit counter */
//         }
//         if ((c > 0x60 ) && (c < 0x67))      /* char is between 0xa and 0xf */
//         {
//            d += (c - 0x60 + 0x9);           /* add the char equivalen value to our total */
//            i++;                             /* increment our digit counter */
//         }
//      } while (( i == 0 ) || ((*p > 0x2f) && (i < 2)));  /* repeat: if no char found or: if found a space and less than 2 digits */
//
//      if(i)                                  /* if the digit counter is non-zero then a value has been found */
//      {
//         *q++ = d;                           /* write the number found to memory */
//         k++;                                /* increment 'detected numbers counter' */
//      }
//   }
//
//   *q = 0;                                   /* write 0 at the end */
//
//   return k;
//}
int main(void) {
   FT_STATUS ftStatus;
   DWORD USB_data_cnt; /** general use data counter **/
   uint8_t *acArgs;

   printf(BUILD_DAT_TIM "\n");

   ftStatus = USB_Init(); /* initialize USB interface */
   IFF (ftStatus) { /* if FT_OK */
      FT_DEVICE_LIST_INFO_NODE *pInfo;

      pInfo = (FT_DEVICE_LIST_INFO_NODE*) USB_buf;
      printf("USB Device: %s - %s\n", pInfo->Description, pInfo->SerialNumber);
   } else {
      PRNR("USB not found - Error %d\n", ftStatus);
   }

   acArgs = &USB_buf[2];
   FtPurge(hUsb, FT_PURGE_RX_TX);

   while (1) {
      printf("\n#");

      fgets((char*) USB_buf, MAX_STR_LEN, stdin); /* read a string of chars from stdin */

      if (TASK== '.')
      break;

      switch (TASK) {
         case 'l': /* waits for USB data, prints it and sends it back to sender */
         {
            printf("loop back (Rx->Tx)\n");

            if (( USB_data_cnt = ftreaddata())) {
               printBuf(USB_data_cnt); /* print read data */
               ftStatus = FtWrite(hUsb, (void*)USB_buf, /* transmit data back to sender */
                     USB_data_cnt, &USB_data_cnt);
               printf("\nTx %d bytes, code %d\n",
                     USB_data_cnt, ftStatus);
               break;
            }
         }
         break;
         case 't': /* reads data from user, sends it via USB and prints any data already received */
         {
            ftStatus = FtWrite(hUsb, (void*) acArgs,
                  strlen((char*) acArgs) - 1, &USB_data_cnt);
            if (ftStatus)
            printf("Tx failed, code %d\n", ftStatus);

            if ((USB_data_cnt = ftreaddata())) {
               printBuf(USB_data_cnt); /* print read data */
            }
         }
         break;
         case 'p': /* transmits a sequence from 0 to 255 via USB */
         {
            for (USB_data_cnt= 0; USB_data_cnt < 0x100; USB_data_cnt++) {
               USB_buf[USB_data_cnt] = USB_data_cnt;
            }
            USB_buf[USB_data_cnt] = 0;
            ftStatus = FtWrite(hUsb, (void*) USB_buf,
                  USB_data_cnt, &USB_data_cnt);
            if (( USB_data_cnt = ftreaddata())) {
               printBuf(USB_data_cnt); /* print read data */
            }
         }
         break;
         case 'r': /* read USB data */
         {
            if (( USB_data_cnt = ftreaddata())) {
               printBuf(USB_data_cnt); /* print read data */
            }
         }
         break;
         case 'f': /* purge/flush ftdi internal buffers */
         {
            ftStatus = FtPurge(hUsb, FT_PURGE_RX | FT_PURGE_TX); // Purge both Rx and Tx buffers
            if (ftStatus != FT_OK) {
               printf ("Purge failed");
            }
         }
         break;
         case '@': /* reset ftdi */
         {
            ftStatus = FtReset(hUsb); // Purge both Rx and Tx buffers
            if (ftStatus != FT_OK) {
               printf ("Reset failed");
            }
         }
         break;
         default:
         {
            DWORD RxBytes = 0, TxBytes = 0, EventDWord = 0;

            FtGetStatus(hUsb, &RxBytes, &TxBytes, &EventDWord);
            printf ("Events : %08x\n", EventDWord);
            printf ("RxBytes: %u\n", RxBytes);
            printf ("TxBytes: %u\n", TxBytes);
         }
         break;
      }
   }

   free(USB_buf);

   return FtClose(hUsb);				// this will close FT232H;
}

