/*
 * main2.c
 *
 *  Created on: Dec 7, 2021
 *      Author: ellerena
 */

/*
 * Copyright (c) 2019 STMicroelectronics
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <drivers/sensor.h>
#include <stdio.h>
#include <sys/util.h>

#ifdef CONFIG_LSM6DSR_TRIGGER
static int lsm6dsr_acc_trig_cnt;
static int lsm6dsr_gyr_trig_cnt;
static int lsm6dsr_temp_trig_cnt;

static void lsm6dsr_acc_trig_handler(const struct device *dev,
             struct sensor_trigger *trig)
{
  sensor_sample_fetch_chan(dev, SENSOR_CHAN_ACCEL_XYZ);
  lsm6dsr_acc_trig_cnt++;
}

static void lsm6dsr_gyr_trig_handler(const struct device *dev,
             struct sensor_trigger *trig)
{
  sensor_sample_fetch_chan(dev, SENSOR_CHAN_GYRO_XYZ);
  lsm6dsr_gyr_trig_cnt++;
}

static void lsm6dsr_temp_trig_handler(const struct device *dev,
              struct sensor_trigger *trig)
{
  sensor_sample_fetch_chan(dev, SENSOR_CHAN_DIE_TEMP);
  lsm6dsr_temp_trig_cnt++;
}
#endif

static void lsm6dsr_config (const struct device * lsm6dsr)
{
  struct sensor_value odr_attr, fs_attr;

  /* set LSM6DSR accel sampling frequency to 208 Hz */
  odr_attr.val1 = 208;
  odr_attr.val2 = 0;

  if (sensor_attr_set(lsm6dsr,
                      SENSOR_CHAN_ACCEL_XYZ,
                      SENSOR_ATTR_SAMPLING_FREQUENCY,
                      &odr_attr) < 0)
  {
    printk("Cannot set sampling frequency for LSM6DSR accel\n");
    return;
  }

  sensor_g_to_ms2(16, &fs_attr);

  if (sensor_attr_set(lsm6dsr,
                      SENSOR_CHAN_ACCEL_XYZ,
                      SENSOR_ATTR_FULL_SCALE,
                      &fs_attr) < 0)
  {
    printk("Cannot set fs for LSM6DSR accel\n");
    return;
  }

  /* set LSM6DSR gyro sampling frequency to 208 Hz */
  odr_attr.val1 = 208;
  odr_attr.val2 = 0;

  if (sensor_attr_set(lsm6dsr,
                      SENSOR_CHAN_GYRO_XYZ,
                      SENSOR_ATTR_SAMPLING_FREQUENCY,
                      &odr_attr) < 0)
  {
    printk("Cannot set sampling frequency for LSM6DSR gyro\n");
    return;
  }

  sensor_degrees_to_rad(250, &fs_attr);

  if (sensor_attr_set(lsm6dsr,
                      SENSOR_CHAN_GYRO_XYZ,
                      SENSOR_ATTR_FULL_SCALE,
                      &fs_attr) < 0)
  {
    printk("Cannot set fs for LSM6DSR gyro\n");
    return;
  }

#ifdef CONFIG_LSM6DSR_TRIGGER
  struct sensor_trigger trig;

  trig.type = SENSOR_TRIG_DATA_READY;
  trig.chan = SENSOR_CHAN_ACCEL_XYZ;
  sensor_trigger_set(lsm6dsr, &trig, lsm6dsr_acc_trig_handler);

  trig.type = SENSOR_TRIG_DATA_READY;
  trig.chan = SENSOR_CHAN_GYRO_XYZ;
  sensor_trigger_set(lsm6dsr, &trig, lsm6dsr_gyr_trig_handler);

  trig.type = SENSOR_TRIG_DATA_READY;
  trig.chan = SENSOR_CHAN_DIE_TEMP;
  sensor_trigger_set(lsm6dsr, &trig, lsm6dsr_temp_trig_handler);
#endif
}

void lsm6dsxx_run (void)
{

#ifdef CONFIG_LSM6DSR_ENABLE_TEMP
  struct sensor_value die_temp;
#endif

  struct sensor_value accel1[3];
  struct sensor_value gyro[3];
  const struct device *lsm6dsr = device_get_binding(DT_LABEL(DT_INST(0,
                                                                     st_lsm6dso)));
  int cnt = 1;

  if (lsm6dsr == NULL)
  {
    printf("Could not get LSM6DSR device\n");
    return;
  }

  lsm6dsr_config(lsm6dsr);

  while (1)
  {
    /* Get sensor samples */

#ifndef CONFIG_LSM6DSR_TRIGGER
    if (sensor_sample_fetch(lsm6dsr) < 0)
    {
      printf("LSM6DSR Sensor sample update error\n");
      return;
    }
#endif

    /* Get sensor data */

    sensor_channel_get(lsm6dsr, SENSOR_CHAN_ACCEL_XYZ, accel1);
    sensor_channel_get(lsm6dsr, SENSOR_CHAN_GYRO_XYZ, gyro);
#ifdef CONFIG_LSM6DSR_ENABLE_TEMP
    sensor_channel_get(lsm6dsr, SENSOR_CHAN_DIE_TEMP, &die_temp);
#endif

    /* Display sensor data */

    /* Erase previous */
    printf("\0033\014");

    printf("X-NUCLEO-IKS01A3 sensor dashboard\n\n");

    /* temperature */
    printf("LSM6DSR: Accel (m.s-2): x: %.3f, y: %.3f, z: %.3f\n",
           sensor_value_to_double(&accel1[0]),
           sensor_value_to_double(&accel1[1]),
           sensor_value_to_double(&accel1[2]));

    printf("LSM6DSR: GYro (dps): x: %.3f, y: %.3f, z: %.3f\n",
           sensor_value_to_double(&gyro[0]),
           sensor_value_to_double(&gyro[1]),
           sensor_value_to_double(&gyro[2]));

#ifdef CONFIG_LSM6DSR_ENABLE_TEMP
    /* temperature */
    printf("LSM6DSR: Temperature: %.1f C\n",
           sensor_value_to_double(&die_temp));
#endif

    cnt++;
    k_sleep(K_MSEC(2000));
  }
}

