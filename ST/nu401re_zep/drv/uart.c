/*
 * uart.c
 *
 *  Created on: Nov 12, 2020
 *      Author: ellerena
 */

#include <zephyr.h>
#include <string.h>
#include <sys/printk.h>

#include "uart.h"

K_FIFO_DEFINE(printk_fifo_q);

void uart_msg_enqueue_val (uint32_t item, uint32_t value)
{
  uint32_t size = sizeof(log_uart_val_t);

  log_uart_val_t temp =
    { .type = FIFO_UART_VAL, .item = item, .value = value };

  uint8_t *mem_ptr = k_malloc(size);
  __ASSERT_NO_MSG(mem_ptr != 0);

  memcpy(mem_ptr, &temp, size);
  k_fifo_put(&printk_fifo_q, mem_ptr);
}

void uart_msg_enqueue_str (const char * data, size_t size)
{
  /* place the string in the heap */
  char *pstr = k_malloc(size + 1);
  __ASSERT_NO_MSG(pstr);
  memcpy(pstr, data, size);
  pstr[size] = '\0';

  /* create and fill the log structure in the heap */
  log_uart_str_t *ptemp = k_malloc(sizeof(log_uart_str_t));
  __ASSERT_NO_MSG(ptemp);
  ptemp->type = FIFO_UART_STR;
  ptemp->pstr = pstr;

  k_fifo_put(&printk_fifo_q, ptemp);
}

void uart_out (void)
{
  printk("Paris est libere!\r\n" CONFIG_BOARD "\r\n" \
         __DATE__ " " __TIME__"\r\n");

  while (1)
  {
    log_uart_val_t *rx_data = k_fifo_get(&printk_fifo_q, K_FOREVER);
    __ASSERT_NO_MSG(rx_data);

    if (rx_data->type == FIFO_UART_VAL)
    {
      printk("[%8x] %u\n", rx_data->item, rx_data->value);
    }
    else if (rx_data->type == FIFO_UART_STR)
    {
      log_uart_str_t *p = (log_uart_str_t*) rx_data;
      printk("%s\n", p->pstr);
      k_free(p->pstr);
    }

    k_free(rx_data);
  }
}

