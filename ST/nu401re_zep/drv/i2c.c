/*
 * i2c.c
 *
 *  Created on: Dec 4, 2021
 *      Author: ellerena
 */

/*
 * Copyright (c) 2019 STMicroelectronics
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <errno.h>
#include <sys/printk.h>
#include <device.h>
#include <drivers/i2c.h>
#include <drivers/sensor.h>
#include <stdio.h>
#include <sys/util.h>

//#define I2C_DEV  DT_LABEL(DT_INST(0, st_stm32-i2c-v1))     //DT_LABEL(DT_ALIAS(i2c1))

#define I2C_ADDR7 0x6b // lsm6dso i2s 7bit address

const struct device *i2c_dev;

static int write_bytes (uint8_t addr, uint8_t * data, uint32_t num_bytes)
{
  struct i2c_msg msgs[2];

  /* Setup I2C messages */

  /* Send the address to write to */
  msgs[0].buf = &addr;
  msgs[0].len = 1U;
  msgs[0].flags = I2C_MSG_WRITE;

  /* Data to be written, and STOP after this. */
  msgs[1].buf = data;
  msgs[1].len = num_bytes;
  msgs[1].flags = I2C_MSG_WRITE | I2C_MSG_STOP;

  return i2c_transfer(i2c_dev, &msgs[0], 2, I2C_ADDR7);
}

static int read_bytes (uint8_t addr, uint8_t * data, uint32_t num_bytes)
{
  struct i2c_msg msgs[2];

  /* Setup I2C messages */

  /* Send the address to read from */
  msgs[0].buf = &addr;
  msgs[0].len = 1U;
  msgs[0].flags = I2C_MSG_WRITE;

  /* Read from device. STOP after this. */
  msgs[1].buf = data;
  msgs[1].len = num_bytes;
  msgs[1].flags = I2C_MSG_READ | I2C_MSG_STOP;

  return i2c_transfer(i2c_dev, &msgs[0], 2, I2C_ADDR7);
}

void i2c_init (void)
{
  i2c_dev = device_get_binding(DT_NODELABEL(arduino_i2c));
  if (!i2c_dev)
  {
    printk("I2C: Device driver not found.\n");
    return;
  }
}

//#ifdef CONFIG_LSM6DSO_TRIGGER
//static int lsm6dso_acc_trig_cnt;
//static int lsm6dso_gyr_trig_cnt;
//static int lsm6dso_temp_trig_cnt;
//
//static void lsm6dso_acc_trig_handler(const struct device *dev,
//             struct sensor_trigger *trig)
//{
//  sensor_sample_fetch_chan(dev, SENSOR_CHAN_ACCEL_XYZ);
//  lsm6dso_acc_trig_cnt++;
//}
//
//static void lsm6dso_gyr_trig_handler(const struct device *dev,
//             struct sensor_trigger *trig)
//{
//  sensor_sample_fetch_chan(dev, SENSOR_CHAN_GYRO_XYZ);
//  lsm6dso_gyr_trig_cnt++;
//}
//
//static void lsm6dso_temp_trig_handler(const struct device *dev,
//              struct sensor_trigger *trig)
//{
//  sensor_sample_fetch_chan(dev, SENSOR_CHAN_DIE_TEMP);
//  lsm6dso_temp_trig_cnt++;
//}
//#endif
//
//static void lsm6dso_config (const struct device * lsm6dso)
//{
//  struct sensor_value odr_attr, fs_attr;
//
//  /* set LSM6DSO accel sampling frequency to 208 Hz */
//  odr_attr.val1 = 208;
//  odr_attr.val2 = 0;
//
//  if (sensor_attr_set(lsm6dso,
//                      SENSOR_CHAN_ACCEL_XYZ,
//                      SENSOR_ATTR_SAMPLING_FREQUENCY,
//                      &odr_attr) < 0)
//  {
//    printk("Cannot set sampling frequency for LSM6DSO accel\n");
//    return;
//  }
//
//  sensor_g_to_ms2(16, &fs_attr);
//
//  if (sensor_attr_set(lsm6dso,
//                      SENSOR_CHAN_ACCEL_XYZ,
//                      SENSOR_ATTR_FULL_SCALE,
//                      &fs_attr) < 0)
//  {
//    printk("Cannot set fs for LSM6DSO accel\n");
//    return;
//  }
//
//  /* set LSM6DSO gyro sampling frequency to 208 Hz */
//  odr_attr.val1 = 208;
//  odr_attr.val2 = 0;
//
//  if (sensor_attr_set(lsm6dso,
//                      SENSOR_CHAN_GYRO_XYZ,
//                      SENSOR_ATTR_SAMPLING_FREQUENCY,
//                      &odr_attr) < 0)
//  {
//    printk("Cannot set sampling frequency for LSM6DSO gyro\n");
//    return;
//  }
//
//  sensor_degrees_to_rad(250, &fs_attr);
//
//  if (sensor_attr_set(lsm6dso,
//                      SENSOR_CHAN_GYRO_XYZ,
//                      SENSOR_ATTR_FULL_SCALE,
//                      &fs_attr) < 0)
//  {
//    printk("Cannot set fs for LSM6DSO gyro\n");
//    return;
//  }
//
//#ifdef CONFIG_LSM6DSO_TRIGGER
//  struct sensor_trigger trig;
//
//  trig.type = SENSOR_TRIG_DATA_READY;
//  trig.chan = SENSOR_CHAN_ACCEL_XYZ;
//  sensor_trigger_set(lsm6dso, &trig, lsm6dso_acc_trig_handler);
//
//  trig.type = SENSOR_TRIG_DATA_READY;
//  trig.chan = SENSOR_CHAN_GYRO_XYZ;
//  sensor_trigger_set(lsm6dso, &trig, lsm6dso_gyr_trig_handler);
//
//  trig.type = SENSOR_TRIG_DATA_READY;
//  trig.chan = SENSOR_CHAN_DIE_TEMP;
//  sensor_trigger_set(lsm6dso, &trig, lsm6dso_temp_trig_handler);
//#endif
//}
//
//void main (void)
//{
//  struct sensor_value temp1, temp2, temp3, hum, press;
//#ifdef CONFIG_LSM6DSO_ENABLE_TEMP
//  struct sensor_value die_temp;
//#endif
//  struct sensor_value die_temp2;
//  struct sensor_value accel1[3], accel2[3];
//  struct sensor_value gyro[3];
//  struct sensor_value magn[3];
//
//  const struct device *lsm6dso = device_get_binding(DT_LABEL(DT_INST(0, st_lsm6dso)));
//  int cnt = 1;
//
//  if (lsm6dso == NULL)
//  {
//    printf("Could not get LSM6DSO device\n");
//    return;
//  }
//
//  lsm6dso_config(lsm6dso);
//
//  while (1)
//  {
//
//    /* Get sensor samples */
//#ifndef CONFIG_LSM6DSO_TRIGGER
//    if (sensor_sample_fetch(lsm6dso) < 0)
//    {
//      printf("LSM6DSO Sensor sample update error\n");
//      return;
//    }
//#endif
//
//    /* Get sensor data */
//    sensor_channel_get(lsm6dso, SENSOR_CHAN_ACCEL_XYZ, accel1);
//    sensor_channel_get(lsm6dso, SENSOR_CHAN_GYRO_XYZ, gyro);
//#ifdef CONFIG_LSM6DSO_ENABLE_TEMP
//    sensor_channel_get(lsm6dso, SENSOR_CHAN_DIE_TEMP, &die_temp);
//#endif
//
//    /* Display sensor data */
//
//    /* Erase previous */
//    printf("\0033\014");
//
//    printf("X-NUCLEO-IKS01A3 sensor dashboard\n\n");
//
//    printf("LSM6DSO: Accel (m.s-2): x: %.3f, y: %.3f, z: %.3f\n",
//           sensor_value_to_double(&accel1[0]),
//           sensor_value_to_double(&accel1[1]),
//           sensor_value_to_double(&accel1[2]));
//
//    printf("LSM6DSO: GYro (dps): x: %.3f, y: %.3f, z: %.3f\n",
//           sensor_value_to_double(&gyro[0]),
//           sensor_value_to_double(&gyro[1]),
//           sensor_value_to_double(&gyro[2]));
//
//#ifdef CONFIG_LSM6DSO_ENABLE_TEMP
//    /* temperature */
//    printf("LSM6DSO: Temperature: %.1f C\n",
//           sensor_value_to_double(&die_temp));
//#endif
//
//#ifdef CONFIG_LSM6DSO_TRIGGER
//    printk("%d:: lsm6dso acc trig %d\n", cnt, lsm6dso_acc_trig_cnt);
//    printk("%d:: lsm6dso gyr trig %d\n", cnt, lsm6dso_gyr_trig_cnt);
//    printk("%d:: lsm6dso temp trig %d\n", cnt,
//      lsm6dso_temp_trig_cnt);
//#endif
//
//    cnt++;
//    k_sleep(K_MSEC(2000));
//  }
//}
