/*
 * gpio.c
 *
 *  Created on: Dec 11, 2021
 *      Author: ellerena
 */

#include <zephyr.h>
#include <device.h>
#include <drivers/gpio.h>
#include <sys/util.h>
#include <sys/printk.h>
#include <inttypes.h>

#define SW0_NODE DT_ALIAS(sw0)
#define LSM_INT1 DT_ALIAS(lsm6int1)

#define SW0_GPIO_LABEL  DT_GPIO_LABEL(SW0_NODE, gpios)
#define SW0_GPIO_PIN  DT_GPIO_PIN(SW0_NODE, gpios)
#define SW0_GPIO_FLAGS  (GPIO_INPUT | DT_GPIO_FLAGS(SW0_NODE, gpios))

#define LSM6INT1_GPIO_LABEL  DT_GPIO_LABEL(LSM_INT1, gpios)
#define LSM6INT1_GPIO_PIN  DT_GPIO_PIN(LSM_INT1, gpios)
#define LSM6INT1_GPIO_FLAGS  (GPIO_OUTPUT_LOW)

static struct gpio_callback button_cb_data;

static void button_pressed (const struct device * dev,
                            struct gpio_callback * cb,
                            uint32_t pins)
{
  printk("B1 @ %" PRIu32 "\n", k_cycle_get_32());
}

static int b1_gpio_init (void)
{
  const struct device *button;
  int ret = -1;

  button = device_get_binding(SW0_GPIO_LABEL);
  if (NULL != button)
  {
    ret = gpio_pin_configure(button, SW0_GPIO_PIN, SW0_GPIO_FLAGS);
    if (0 == ret)
    {
      ret = gpio_pin_interrupt_configure(
                                         button,
                                         SW0_GPIO_PIN,
                                         GPIO_INT_EDGE_TO_ACTIVE);
      if (0 == ret)
      {
        gpio_init_callback(&button_cb_data, button_pressed, BIT(SW0_GPIO_PIN));
        gpio_add_callback(button, &button_cb_data);
      }
    }
  }

  return ret;
}

static int lsm6int1_gpio_init (void)
{
  const struct device *int1;
  int ret = -1;

  int1 = device_get_binding(LSM6INT1_GPIO_LABEL);
  if (NULL != int1)
  {
    ret = gpio_pin_configure(int1, LSM6INT1_GPIO_PIN, LSM6INT1_GPIO_FLAGS);
  }

  return ret;
}

void gpio_tst (void)
{
  printk("B1 setup: %d\n", b1_gpio_init());

  printk("LSM6_INT1 setup %d\n", lsm6int1_gpio_init());

}

