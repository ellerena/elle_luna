/*
 *
 */

#include <zephyr.h>
#include <device.h>
#include <kernel.h>
#include <devicetree.h>
//#include <lvgl.h>
#include <stdio.h>
#include <drivers/display.h>
#include <drivers/gpio.h>
#include <drivers/sensor.h>
#include <sys/printk.h>
#include <sys/__assert.h>
#include <string.h>
#include <logging/log.h>

#include "uart.h"
#include "leds.h"

LOG_MODULE_REGISTER(app);

#define STACKSIZE 1024                       /* thread size of stack area */
#define PRIORITY 7                           /* thread scheduling priority */

//void ssd1306(void)
//{
//   uint32_t count = 0U;
//   char count_str[11] = {0};
//   const struct device *display_dev;
//   lv_obj_t *hello_world_label;
//   lv_obj_t *count_label;
//
//   display_dev = device_get_binding(CONFIG_LVGL_DISPLAY_DEV_NAME);
//
//   if (display_dev == NULL) {
//      LOG_ERR("display not found, skipping its test.");
//      return;
//   }
//
//   if (IS_ENABLED(CONFIG_LVGL_POINTER_KSCAN)) {
//      lv_obj_t *hello_world_button;
//
//      hello_world_button = lv_btn_create(lv_scr_act(), NULL);
//      lv_obj_align(hello_world_button, NULL, LV_ALIGN_CENTER, 0, 0);
//      lv_btn_set_fit(hello_world_button, LV_FIT_TIGHT);
//      hello_world_label = lv_label_create(hello_world_button, NULL);
//   } else {
//      hello_world_label = lv_label_create(lv_scr_act(), NULL);
//   }
//
//   lv_label_set_text(hello_world_label, __TIME__);
//   lv_obj_align(hello_world_label, NULL, LV_ALIGN_CENTER, 0, 0);
//
//   count_label = lv_label_create(lv_scr_act(), NULL);
//   lv_obj_align(count_label, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);
//
//   lv_task_handler();
//   display_blanking_off(display_dev);
//
//   for(;;) {
//      if ((count % 100) == 0U) {
//         sprintf(count_str, "%d", count/100U);
//         lv_label_set_text(count_label, count_str);
//      }
//      lv_task_handler();
//      k_sleep(K_MSEC(10));
//      ++count;
//   }
//}

void main(void)
{
  extern void lsm6dsxx_run(void);
  extern void gpio_tst (void);

  gpio_tst();
  lsm6dsxx_run();

}

/**************** Extra Threads ****************/
/*** Led blinkers ***/
K_THREAD_DEFINE(blink0_id, 512, blink0, NULL, NULL, NULL, PRIORITY, 0, 0);

/*** SSD1306 OLED display ***/
//K_THREAD_DEFINE(ssd1306_id, STACKSIZE, ssd1306, NULL, NULL, NULL, PRIORITY, 0, 0);

/*** Uart thread ***/
K_THREAD_DEFINE(uart_out_id, 512, uart_out, NULL, NULL, NULL, PRIORITY, 0, 0);

