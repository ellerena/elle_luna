/*
 * lsm6dsxx_tilt.c
 *
 *  Created on: Sep 28, 2021
 *      Author: ellerena
 */

/* ATTENTION: By default the driver is little endian. If you need switch
 *            to big endian please see "Endianness definitions" in the
 *            header file of the driver (_reg.h).
 */

#define SENSOR_BUS hi2c1

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include <stdio.h>

#include "lsm6dsxx.h"

#include "stm32f4xx_hal.h"
#include "usart.h"
#include "gpio.h"
#include "i2c.h"

/* Private macro -------------------------------------------------------------*/
#define BOOT_TIME 10 //ms
#define LSM6DSX_I2C_ADD8 LSM6DSR_I2C_ADD_L
#define HORZL "===================="
#define I2C_ADR(x) addr[x][0]
#define I2C_NAM(x) addr[x][1]
#define I2C_REG(x) addr[x][2]
#define I2C_RES(x) addr[x][3]
#define I2C_RD(x) addr[x][4]

/* Private variables ---------------------------------------------------------*/
typedef struct
{
  uint8_t address;
  uint8_t data;
} conf_data_t;

static uint8_t whoamI, rst;
static char tx_buf[25] = ">";
static uint32_t flags = 0;
static uint32_t ik = 0;

static const conf_data_t cnf_data[] =
  {
    { SENUPP(CTRL9_XL), 0xe2 }, // Disable I3C interface
      { SENUPP(I3C_BUS_AVB), 0x00 },

    /* enable timestamp counter */
      { SENUPP(CTRL10_C), 0x20 }, // Enable timestamp counter

    /* Set XL Output Data Rate: The tilt function requires ODR >= 26 Hz */
      { SENUPP(CTRL1_XL), 0x22 }, // XL ODR=26Hz, FS=2g, 2nd stage LPF2

    /* Enable Tilt in embedded function. */
      { SENUPP(FUNC_CFG_ACCESS), 0x80 }, // select embedded function register map
      { SENUPP(EMB_FUNC_EN_A), 0x10 }, // tilt enable
      { SENUPP(EMB_FUNC_INT1), 0x10 }, // Routing of tilt event on INT1
      { SENUPP(PAGE_RW), 0x80 }, // Enable latched mode (lir) for embedded functions
      { SENUPP(FUNC_CFG_ACCESS), 0x00 }, // de-select embedded function register map

    /* Interrupt generation on Tilt INT1 pin */
      { SENUPP(MD1_CFG), 0x02 }, // Routing of embedded functions event on INT1
  };

static uint8_t addr[][5] =
  { // {8bit i2c addr, whoami val, whoami red}
    { 0x32 + 1, 0x44, 0xf }, // LIS2DW12 accelerometer
    { 0xd6 + 1, 0x6c, 0xf }, // LSM6DSO acc + gyro
    { 0xbe + 1, 0xbc, 0xf }, // HTS221 temp + humidity
    { 0xba + 1, 0xb3, 0xf }, // LPS22HH pressure
    { 0x94 + 1, 0x53, 0xfe }, // STTS751 temperature
    { 0x3c + 1, 0x40, 0x4f }, // LIS2MDL magnetometer
    { LSM6DSR_I2C_ADD_L, 0x6b, 0xf }, // 0xd5/0xd4 (0x6a)
    { LSM6DSR_I2C_ADD_H, 0x6c, 0xf }, // 0xd7/0xd6 (0x6b)
  };

/* Extern variables ----------------------------------------------------------*/
int process = 0;

/* Private functions ---------------------------------------------------------*/

static int32_t platform_write (void * handle,
                               uint8_t reg,
                               const uint8_t * bufp,
                               uint16_t len);
static int32_t platform_read (void * handle,
                              uint8_t reg,
                              uint8_t * bufp,
                              uint16_t len);
static void platform_delay (uint32_t ms);
static void platform_init (void);
static void sensor_scan (void);

/* Initialize mems driver interface */
static stmdev_ctx_t dev_ctx =
  { .write_reg = platform_write,
    .read_reg = platform_read,
    .handle = &SENSOR_BUS };

/*
 * @brief  Write generic device register (platform dependent)
 *
 * @param  handle    customizable argument. In this examples is used in
 *                   order to select the correct sensor bus handler.
 * @param  reg       register to write
 * @param  bufp      pointer to data to write in register reg
 * @param  len       number of consecutive register to write
 *
 */
static int32_t platform_write (void * handle,
                               uint8_t reg,
                               const uint8_t * bufp,
                               uint16_t len)
{
  HAL_I2C_Mem_Write(&SENSOR_BUS,
                    LSM6DSX_I2C_ADD8,
                    reg,
                    I2C_MEMADD_SIZE_8BIT,
                    (uint8_t*) bufp, len, 1000);

  return 0;
}

/*
 * @brief  Read generic device register (platform dependent)
 *
 * @param  handle    customizable argument. In this examples is used in
 *                   order to select the correct sensor bus handler.
 * @param  reg       register to read
 * @param  bufp      pointer to buffer that store the data read
 * @param  len       number of consecutive register to read
 *
 */
static int32_t platform_read (void * handle,
                              uint8_t reg,
                              uint8_t * bufp,
                              uint16_t len)
{
  HAL_I2C_Mem_Read(&SENSOR_BUS,
                   LSM6DSX_I2C_ADD8,
                   reg,
                   I2C_MEMADD_SIZE_8BIT,
                   bufp, len, 1000);
  return 0;
}

/*
 * @brief  platform specific delay (platform dependent)
 *
 * @param  ms        delay in ms
 *
 */
static void platform_delay (uint32_t ms)
{
  HAL_Delay(ms);
}

/*
 * @brief  platform specific initialization (platform dependent)
 */
static void platform_init (void)
{
  sensor_scan();
}

static void platform_byte_wr (uint8_t i2c_addr8, uint8_t reg, uint8_t val)
{
  HAL_I2C_Mem_Write(&SENSOR_BUS, i2c_addr8, reg,
                    I2C_MEMADD_SIZE_8BIT,
                    &val, 1, 1000);
}

static void platform_byte_rd (uint8_t i2c_addr8, uint8_t reg, uint8_t * val)
{
  HAL_I2C_Mem_Read(&SENSOR_BUS, i2c_addr8, reg,
                   I2C_MEMADD_SIZE_8BIT,
                   val, 1, 1000);
}

static void platform_batch_wr (const conf_data_t * conf, int num, int i2c_addr8)
{
  while (num--)
  {
    platform_byte_wr(i2c_addr8, conf->address, conf->data);
    conf++;
  }
}

static void platform_batch_rd (const conf_data_t * conf, int num, int i2c_addr8)
{
  sprintf(&tx_buf[1], HORZL "\n");
  HAL_UART_Transmit(&huart2, (uint8_t*) tx_buf, strlen(tx_buf), 1000);

  uint8_t val;
  while (num--)
  {
    platform_byte_rd(i2c_addr8, conf->address, &val);
    conf++;

    sprintf(&tx_buf[1], "%02x>%02x %02x\n", i2c_addr8, i2c_addr8 >> 1, val);
    HAL_UART_Transmit(&huart2, (uint8_t*) tx_buf, strlen(tx_buf), 1000);
  }

  sprintf(&tx_buf[1], HORZL "\n\n");
  HAL_UART_Transmit(&huart2, (uint8_t*) tx_buf, strlen(tx_buf), 1000);
}

static void sensor_scan (void)
{
  for (int i = 0; i < 8; ++i)
  {
    I2C_RES(i)= HAL_I2C_Mem_Read(&SENSOR_BUS, I2C_ADR(i), I2C_REG(i), I2C_MEMADD_SIZE_8BIT, &I2C_RD(i), 1, 1000);
  }

  sprintf(&tx_buf[1], HORZL "\n");
  HAL_UART_Transmit(&huart2, (uint8_t*)tx_buf, strlen(tx_buf), 1000);
  for (int i = 0; i < 8; ++i)
  {
    sprintf(&tx_buf[1], "%02x>%02xh %02x:%02xh ",
        I2C_ADR(i) & 0xfe,
        I2C_ADR(i) >> 1,
        I2C_NAM(i),
        I2C_RD(i));

    sprintf(&tx_buf[15], ( 0 == I2C_RES(i)) ? "OK\n":"NG\n");
    HAL_UART_Transmit(&huart2, (uint8_t*)tx_buf, strlen(tx_buf), 1000);
  }
  sprintf(&tx_buf[1], HORZL "\n\n");
  HAL_UART_Transmit(&huart2, (uint8_t*)tx_buf, strlen(tx_buf), 1000);

}

void lsm6dsxx_tilt (void)
{
  /* Init test platform */
  platform_init();
  platform_delay(BOOT_TIME); // wait sensor boot time

  /* Check device ID */
  SENLOW(read_reg)(&dev_ctx, SENUPP(WHO_AM_I), &whoamI, 1);
  sprintf(&tx_buf[1], " %d %02xh %02xh\n", __LINE__, LSM6DSX_I2C_ADD8, whoamI);
  HAL_UART_Transmit(&huart2, (uint8_t*) tx_buf, strlen(tx_buf), 1000);
  if (whoamI != SENUPP(ID))
  {
    while (1)
    {
      if (process == 2)
      {
        sensor_scan();
        process = 0;
      }
    }
  }

  /* sw reset the sensor */
  SENLOW(reset_set)(&dev_ctx, PROPERTY_ENABLE); // Restore default configuration

  do
  {
    SENLOW(reset_get)(&dev_ctx, &rst);
  }
  while (rst);

  /* batch load all registers required to setup the sensor */
  platform_batch_wr(cnf_data,
                    sizeof(cnf_data) / sizeof(conf_data_t),
                    LSM6DSX_I2C_ADD8);

  /* configure INT1 on the MCU side as IRQ pint */
  USR_LSM6_INT_Enable();

  PIN_L(LD2_GPIO_Port, LD2_Pin);

  while (1)
  {
    if (flags)
    { /* Check if Tilt event */
      uint8_t is_tilt;
      SENLOW(tilt_flag_data_ready_get)(&dev_ctx, &is_tilt);

      if (is_tilt)
      {
        PIN_T(LD2_GPIO_Port, LD2_Pin);
        sprintf(&tx_buf[1], "%lu\n", ik);
        HAL_UART_Transmit(&huart2, (uint8_t*) tx_buf, strlen(tx_buf), 1000);
      }
      flags = 0;
    }

    if (process == 2)
    {
      sensor_scan();
      process = 0;
    }
  }
}

void HAL_GPIO_EXTI_Callback (uint16_t GPIO_Pin)
{
  if (GPIO_PIN_0 == GPIO_Pin)
  {
    ik++;
    flags = 1;
  }

  if (GPIO_PIN_13 == GPIO_Pin)
  {
    PIN_T(LD2_GPIO_Port, LD2_Pin);
    process = 2;
  }

}

