/*
 * cli_i2c.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include <i2c.h>
#include <stdio.h>

#define DEFAULT_REG_ADDRESS_LEN I2C_MEMADD_SIZE_8BIT
#define DEFAULT_REG_ADDRESS_LEN_VAL (1)
#define DEFAULT_REG_DATA_LEN_BYTES (1)
#define DEFAULT_SLAVE_ADDRESS (0x3a)

typedef struct
{
  uint8_t dev_addr;
  uint8_t word_sz_bytes;
  uint8_t reg_addr_sz;
  uint8_t reg_addr_sz_val;
  bool use_stop_bit;
} dev_comm_params_t;

dev_comm_params_t dev_comm = {.dev_addr = DEFAULT_SLAVE_ADDRESS,
                              .word_sz_bytes = DEFAULT_REG_DATA_LEN_BYTES,
                              .reg_addr_sz = DEFAULT_REG_ADDRESS_LEN_VAL,
                              .reg_addr_sz_val = DEFAULT_REG_ADDRESS_LEN,
                              .use_stop_bit = false};

static void print_buffer_bytes(void * buf, int len)
{
  uint8_t * data = (uint8_t *)buf;

  for(int i = 0; i < len; ++i)
  {
    printf("%02x ", data[i]);

    if ((0xf == (i&0xf)) || (i == (len - 1)))
    {
      printf("\n");
    }
  }

  printf("%d bytes in buffer\n", len);
}

void cli_i2c (char task, void * args, int len)
{
  unsigned char *pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'a': /* ia<xx : 1 byte/hex> set slave's address */
    {
      if(IS_VALID_DEVICE_ADDRESS(pArgs[0]))
      {
        dev_comm.dev_addr = pArgs[0];
      }

      printf("slave address: %02xh\n", dev_comm.dev_addr);

      break;
    }

    case  'b': /* ib<nn> set a fix register size of n bytes */
    {
      if (pArgs[0])
      {
        dev_comm.word_sz_bytes = pArgs[0];
      }

      printf("register size: %d bytes\n", dev_comm.word_sz_bytes);

      break;
    }

    case 'l': /* il<xx>, set register address size, xx can be 1 or 2 only */
    {
      if (1 == pArgs[0])
      {
        dev_comm.reg_addr_sz = 1;
        dev_comm.reg_addr_sz_val = I2C_MEMADD_SIZE_8BIT;
      }
      else if (2 == pArgs[0])
      {
        dev_comm.reg_addr_sz = 2;
        dev_comm.reg_addr_sz_val = I2C_MEMADD_SIZE_16BIT;
      }

      printf("register address size: %d bytes\n", dev_comm.reg_addr_sz);

      break;
    }

    case 'p': /* enable/disable using stop instead of repeat start */
    {
      dev_comm.use_stop_bit = pArgs[0];

      printf("repeated start: %s\n", dev_comm.use_stop_bit ? "disabled" : "enabled");

      break;
    }

    case 's': /* is, scan slaves present in bus */
    {
      for (int i = 1; i < 0x80; ++i)
      {
        if (!IS_VALID_DEVICE_ADDRESS(i) ||
            HAL_I2C_IsDeviceReady(&hi2c1, TO_8BIT_ADDRESS_W(i), 1, 10) != HAL_OK)
        {
          continue;
        }

        printf("Found device @ %02xh [%02xh %02xh]\n", i, TO_8BIT_ADDRESS_W(i), TO_8BIT_ADDRESS_R(i));
      }

      break;
    }

    case 't': /* it<dev_addr:8b><...>, transmits raw bytes (without register address) */
    {
      HAL_StatusTypeDef status = HAL_ERROR;

      if (len > 1)
      {
        status = HAL_I2C_Master_Transmit(&hi2c1,
                                          TO_8BIT_ADDRESS_W(pArgs[0]),
                                          &pArgs[1],
                                          --len,
                                          HAL_MAX_DELAY);
      }

      printf("%d bytes transferred\n", status ? 0 : len);

      break;
    }

    case 'g': /* ig<dev_addr:8b><nn>, gets nn raw bytes (without register address) */
    {
      if (len < 2)
      {
        printf("invalid parameters\n");
        break;
      }

      len = pArgs[1];

      HAL_StatusTypeDef status = HAL_I2C_Master_Receive(&hi2c1,
                                                        TO_8BIT_ADDRESS_W(pArgs[0]),
                                                        pArgs,
                                                        len,
                                                        HAL_MAX_DELAY);
      if (status)
      {
        len = 0;
      }

      print_buffer_bytes(pArgs, len);

      break;
    }

    case 'w': /* iw<reg_addr><...>, write bytes <...> to current slave @ reg_addr> */
    {
      if (len < dev_comm.reg_addr_sz + dev_comm.word_sz_bytes)
      {
        break;
      }

      uint16_t reg_addr = 2 == dev_comm.reg_addr_sz
                          ? __builtin_bswap16((*(uint16_t*)&pArgs[0]))
                          : pArgs[0];

      uint16_t buf_size = ((len - dev_comm.reg_addr_sz) & ~(dev_comm.word_sz_bytes - 1));

      HAL_StatusTypeDef status = HAL_I2C_Mem_Write(&hi2c1,
                                                    TO_8BIT_ADDRESS_W(dev_comm.dev_addr),
                                                    reg_addr,
                                                    dev_comm.reg_addr_sz_val,
                                                    &pArgs[dev_comm.reg_addr_sz],
                                                    buf_size,
                                                    HAL_MAX_DELAY);

      printf("%d bytes written\n", status ? 0 : buf_size);

      break;
    }

    case 'r': /* ir<reg-addr><nn> reads nn words from current slave */
    {
      if (len <= dev_comm.reg_addr_sz)
      {
        break;
      }

      uint16_t reg_addr = 2 == dev_comm.reg_addr_sz
                          ? __builtin_bswap16((*(uint16_t*)&pArgs[0]))
                          : pArgs[0];

      len = pArgs[dev_comm.reg_addr_sz];

      HAL_StatusTypeDef status = HAL_I2C_Mem_Read(&hi2c1,
                                              TO_8BIT_ADDRESS_W(dev_comm.dev_addr),
                                              reg_addr,
                                              dev_comm.reg_addr_sz_val,
                                              pArgs,
                                              len,
                                              HAL_MAX_DELAY);

      if (status)
      {
        len = 0;
      }

      print_buffer_bytes(pArgs, len);

      break;
    }

    default:
    {
      printf("unknown i2c command\n");
    }
      break;
  }
}
