
#include <zephyr.h>
#include <device.h>
#include <kernel.h>
#include <stdio.h>
#include <drivers/sensor.h>
#include <string.h>

#include "uart.h"

#define DHT_ID             "[DHT22]"
#define DHT_MISSING_MSG    "Couldn't find sensor"
#define DHT_FETCH_ERR      "Failed to fetch the sensor"
#define DHT_GET_ERR        "Failed to read the sensor"
#define DHT_ID_LEN         sizeof(DHT_ID)
#define DHT_MISSING_LEN    sizeof(DHT_MISSING_MSG)
#define DHT_FETCH_ERR_LEN  sizeof(DHT_FETCH_ERR)
#define DHT_GET_ERR_LEN    sizeof(DHT_GET_ERR)
#define DHT_MSG_MAX_SZ     (70)
#define DHT_DATA_MSG_SZ    (DHT_MSG_MAX_SZ - sizeof(DHT_ID))

static const char *now_str(void)
{
   static char buf[16]; /* ...HH:MM:SS.MMM */
   unsigned int now = k_uptime_get_32();
   unsigned int ms = now % MSEC_PER_SEC;
   unsigned int s;
   unsigned int min;
   unsigned int h;

   now /= MSEC_PER_SEC;
   s = now % 60U;
   now /= 60U;
   min = now % 60U;
   now /= 60U;
   h = now;

   snprintf(buf, sizeof(buf), "%u:%02u:%02u.%03u",
       h, min, s, ms);
   return buf;
}

static char msg[DHT_MSG_MAX_SZ] = DHT_ID;

static void dht22_msg_enqueue(const char * message)
{
   char * pmsg = msg + DHT_ID_LEN;
   size_t msg_len = DHT_ID_LEN;

   msg_len += sprintf(pmsg, "%s", message);
   uart_msg_enqueue_str(msg, msg_len);
}

void run_dht22_sensing(void)
{
   const char *const label = DT_LABEL(DT_INST(0, aosong_dht));
   const struct device *dht22 = device_get_binding(label);

   if (!dht22) {
      dht22_msg_enqueue(DHT_MISSING_MSG);
      return;
   }

   for(;;) {
      int rc = sensor_sample_fetch(dht22);

      if (rc) {
         dht22_msg_enqueue(DHT_FETCH_ERR);
         return;
      }

      struct sensor_value temperature;
      struct sensor_value humidity;

      rc = sensor_channel_get(dht22, SENSOR_CHAN_AMBIENT_TEMP,
               &temperature);
      if (rc == 0) {
         rc = sensor_channel_get(dht22, SENSOR_CHAN_HUMIDITY,
                  &humidity);
      }

      if (rc) {
         dht22_msg_enqueue(DHT_GET_ERR);
      }
      else {
         char msg[DHT_DATA_MSG_SZ];

         sprintf(msg, "[%s]: %.1f Cel ; %.1f %%RH\n",
                      now_str(),
                      sensor_value_to_double(&temperature),
                      sensor_value_to_double(&humidity));
         dht22_msg_enqueue(msg);
      }

      k_sleep(K_SECONDS(3));

   }
}
