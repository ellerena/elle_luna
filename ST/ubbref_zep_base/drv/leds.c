/*
 *
 */

#include <zephyr.h>
#include <drivers/gpio.h>
#include <sys/printk.h>
#include <string.h>

#include "uart.h"

#define LED0_NODE DT_ALIAS(led0)
#define LED1_NODE DT_ALIAS(led1)
#define LED2_NODE DT_ALIAS(led2)
#define LED3_NODE DT_ALIAS(led3)

struct led {
   const char *gpio_dev_name;
   const char *gpio_pin_name;
   unsigned int gpio_pin;
   unsigned int gpio_flags;
};

static void blink(const struct led *led, uint32_t sleep_ms, uint32_t id)
{
   const struct device *gpio_dev;
   int cnt = 0;
   int ret;

   gpio_dev = device_get_binding(led->gpio_dev_name);
   if (gpio_dev == NULL) {
      printk("Error: %s missing\n", led->gpio_dev_name);
      return;
   }

   ret = gpio_pin_configure(gpio_dev, led->gpio_pin, led->gpio_flags);
   if (ret != 0) {
      printk("Error %d: pin %d '%s'\n", ret, led->gpio_pin, led->gpio_pin_name);
      return;
   }

   while (1) {
      gpio_pin_set(gpio_dev, led->gpio_pin, cnt & 1);

      uart_msg_enqueue_val(id, cnt);

      k_msleep(sleep_ms);
      cnt++;
   }
}

void blink0(void)
{
   const struct led led0 = {
      .gpio_dev_name = DT_GPIO_LABEL(LED0_NODE, gpios),
      .gpio_pin_name = DT_LABEL(LED0_NODE),
      .gpio_pin = DT_GPIO_PIN(LED0_NODE, gpios),
      .gpio_flags = GPIO_OUTPUT | DT_GPIO_FLAGS(LED0_NODE, gpios),
   };
   blink(&led0, 1000, 0);
}

void blink1(void)
{
   const struct led led1 = {
      .gpio_dev_name = DT_GPIO_LABEL(LED1_NODE, gpios),
      .gpio_pin_name = DT_LABEL(LED1_NODE),
      .gpio_pin = DT_GPIO_PIN(LED1_NODE, gpios),
      .gpio_flags = GPIO_OUTPUT | DT_GPIO_FLAGS(LED1_NODE, gpios),
   };
   blink(&led1, 500, 1);
}

void blink2(void)
{
   const struct led led2 = {
      .gpio_dev_name = DT_GPIO_LABEL(LED2_NODE, gpios),
      .gpio_pin_name = DT_LABEL(LED2_NODE),
      .gpio_pin = DT_GPIO_PIN(LED2_NODE, gpios),
      .gpio_flags = GPIO_OUTPUT | DT_GPIO_FLAGS(LED2_NODE, gpios),
   };
   blink(&led2, 250, 2);
}

void blink3(void)
{
   const struct led led3 = {
      .gpio_dev_name = DT_GPIO_LABEL(LED3_NODE, gpios),
      .gpio_pin_name = DT_LABEL(LED3_NODE),
      .gpio_pin = DT_GPIO_PIN(LED3_NODE, gpios),
      .gpio_flags = GPIO_OUTPUT | DT_GPIO_FLAGS(LED3_NODE, gpios),
   };
   blink(&led3, 100, 3);
}


