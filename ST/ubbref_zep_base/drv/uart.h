/*
 * uart.h
 *
 *  Created on: Jan 13, 2021
 *      Author: ellerena
 */

#ifndef SRC_UART_H_
#define SRC_UART_H_

#define FIFO_UART_VAL   (0)
#define FIFO_UART_STR   (1)

typedef struct {

   void *fifo_reserved;
   uint32_t type;
   uint32_t item;
   uint32_t value;

} log_uart_val_t;

typedef struct {

   void *fifo_reserved;
   uint32_t type;
   char * pstr;
} log_uart_str_t;

extern struct k_fifo printk_fifo_q;

extern void uart_msg_enqueue_val(uint32_t item, uint32_t value);
void uart_msg_enqueue_str(const char * data, size_t size);
extern void uart_out(void);

#endif /* SRC_UART_H_ */
