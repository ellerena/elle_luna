/*
 *
 */

#include <zephyr.h>
#include <device.h>
#include <kernel.h>
#include <devicetree.h>
#include <lvgl.h>
#include <stdio.h>
#include <drivers/display.h>
#include <drivers/gpio.h>
#include <drivers/sensor.h>
#include <sys/printk.h>
#include <sys/__assert.h>
#include <string.h>
#include <logging/log.h>

#include "uart.h"
#include "wire1.h"
#include "leds.h"

LOG_MODULE_REGISTER(app);

#define STACKSIZE 1024                       /* thread size of stack area */
#define PRIORITY 7                           /* thread scheduling priority */

void ssd1306(void)
{
   uint32_t count = 0U;
   char count_str[11] = {0};
   const struct device *display_dev;
   lv_obj_t *hello_world_label;
   lv_obj_t *count_label;

   display_dev = device_get_binding(CONFIG_LVGL_DISPLAY_DEV_NAME);

   if (display_dev == NULL) {
      LOG_ERR("display not found, skipping its test.");
      return;
   }

   if (IS_ENABLED(CONFIG_LVGL_POINTER_KSCAN)) {
      lv_obj_t *hello_world_button;

      hello_world_button = lv_btn_create(lv_scr_act(), NULL);
      lv_obj_align(hello_world_button, NULL, LV_ALIGN_CENTER, 0, 0);
      lv_btn_set_fit(hello_world_button, LV_FIT_TIGHT);
      hello_world_label = lv_label_create(hello_world_button, NULL);
   } else {
      hello_world_label = lv_label_create(lv_scr_act(), NULL);
   }

   lv_label_set_text(hello_world_label, __TIME__);
   lv_obj_align(hello_world_label, NULL, LV_ALIGN_CENTER, 0, 0);

   count_label = lv_label_create(lv_scr_act(), NULL);
   lv_obj_align(count_label, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);

   lv_task_handler();
   display_blanking_off(display_dev);

   for(;;) {
      if ((count % 100) == 0U) {
         sprintf(count_str, "%d", count/100U);
         lv_label_set_text(count_label, count_str);
      }
      lv_task_handler();
      k_sleep(K_MSEC(10));
      ++count;
   }
}

void main(void)
{
   /* run the dht22 thread
    * note that we could use the K_THREAD_DEFINE macro
    * too, but we want to show alternative ways to run threads */
   run_dht22_sensing();
}

/**************** Extra Threads ****************/
/*** Led blinkers ***/
K_THREAD_DEFINE(blink0_id, STACKSIZE, blink0, NULL, NULL, NULL, PRIORITY, 0, 0);
K_THREAD_DEFINE(blink1_id, STACKSIZE, blink1, NULL, NULL, NULL, PRIORITY, 0, 0);
K_THREAD_DEFINE(blink2_id, STACKSIZE, blink2, NULL, NULL, NULL, PRIORITY, 0, 0);
K_THREAD_DEFINE(blink3_id, STACKSIZE, blink3, NULL, NULL, NULL, PRIORITY, 0, 0);

/*** SSD1306 OLED display ***/
//K_THREAD_DEFINE(ssd1306_id, STACKSIZE, ssd1306, NULL, NULL, NULL, PRIORITY, 0, 0);

/*** Uart thread ***/
K_THREAD_DEFINE(uart_out_id, STACKSIZE, uart_out, NULL, NULL, NULL, PRIORITY, 0, 0);

