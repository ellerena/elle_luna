/*
 * slider_gesture_interface.c
 *
 *  Created on: Aug 16, 2021
 *      Author: ellerena
 */

#include "CAPT_BSP.h"
#include <sensors.h>
#include "virtual_com_cmds.h"

/*
 * Add components to allow communications with host MCU.
 * We are slave, to send data to host we will activate the IRQ.
 * The packet has 4 bytes as follows:
 *      [position_MSB, position_LSB, gesture_byte, status_byte]
 */

/* tx_buffer:
 *   slider cycle (27) + slider sensor (11) = 38 bytes
 *   capture button (15) + capture sensor (11) = 26 bytes
 *   face proximity cycle (15) + face proximity sensor (11) = 26 bytes
 *   Total = 90 bytes
 */
#define TX_BUFFER_SIZE (92)

#define FIRST_TOUCH(p) (((p)->bSensorTouch == true) && ((p)->bSensorPrevTouch == false))
#define FINGER_LEAVE(p) (((p)->bSensorTouch == false) && ((p)->bSensorPrevTouch == true))

static uint8_t tx_buffer[TX_BUFFER_SIZE] = {0xc0, 0xca, 0xed, 0xd1, 0xc0, 0x10, 0x50};

static void slider_callback(tSensor *pSensor);


void cb_bta(tSensor* pSensor)
{
    if(FIRST_TOUCH(pSensor))
    {
        LED1_ON;
    }
    else if (FINGER_LEAVE(pSensor))
    {
        LED1_OFF;
    }
}

void cb_pra(tSensor* pSensor)
{
    if (pSensor->bSensorProx)
    {
        LED2_ON;
    }
    else
    {
        LED2_OFF;
    }
}

/*
 * Registers the slider callback
 */
void slider_gesture_init(void)
{
    /* Register the slider's callback */
	MAP_CAPT_registerCallback(&BTA, &cb_bta);
	MAP_CAPT_registerCallback(&PRA, &cb_pra);
    MAP_CAPT_registerCallback(&SLD03, &slider_callback);

    /* Tell the slave I2C driver which buffer is used for tranmit. */
    I2CSlave_setTransmitBuffer(tx_buffer, sizeof tx_buffer);
}

static void slider_callback(tSensor *pSensor)
{
	CHK_POINTER_OR_RETURN(pSensor);

	if(!pSensor->bSensorTouch)
		return;

    COM_DumpBytes(tx_buffer, sizeof tx_buffer);
    I2CSlave_setRequestFlag();
}
