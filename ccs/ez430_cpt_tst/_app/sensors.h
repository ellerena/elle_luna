/*
 * slider_gesture_interface.h
 *
 *  Created on: Aug 16, 2021
 *      Author: ellerena
 */

#ifndef APP_SENSORS_H_
#define APP_SENSORS_H_

#include <captivate.h>
#include <I2CSlave.h>

#include "code_platform.h"

//*****************************************************************************
//
//! Prototypes for external functions
//
//*****************************************************************************
extern void cb_bta(tSensor* pSensor);
extern void cb_pra(tSensor* pSensor);
extern void slider_gesture_init(void);


#endif /* APP_SENSORS_H_ */
