/******************************************************************************/
// Virtual Com Port Communicatio
/******************************************************************************/

#ifndef VIRTUAL_COM_CMDS
#define VIRTUAL_COM_CMDS

#include "msp430.h"
#include "stdint.h"
#include "code_platform.h"

#define SMCLK_9600      0
#define SMCLK_115200    1
#define SMCLK_230400    2

#define UART_MODE       SMCLK_230400

#if UART_MODE == SMCLK_115200
#define UCA0BRWVAL 8
#define UCA0MCTLWVAL UCOS16 | UCBRF_10 | 0xF700  /* 0xF700 is UCBRSx = 0xF7 ---- 14384 */
#elif UART_MODE == SMCLK_9600
#define UCA0BRWVAL 104;
#define UCA0MCTLWVAL UCOS16 | UCBRF_2 | 0xD600   /* 0xD600 is UCBRSx = 0xD6 */
#elif UART_MODE == SMCLK_230400
#define UCA0BRWVAL 17;
#define UCA0MCTLWVAL /*UCOS16 |*/ UCBRF_0 | 0x4A00
#endif

#define MACRO_COM_INIT                                                      	\
    UCA0CTLW0 |= UCSWRST;                     /* Put eUSCI in reset */          \
    UCA0CTLW0 |= UCSSEL__SMCLK;               /* CLK = SMCLK */                 \
    UCA0BRW = UCA0BRWVAL;                                                       \
    UCA0MCTLW |= UCA0MCTLWVAL;                                                  \
    UCA0CTLW0 &= ~UCSWRST;                    /* Initialize eUSCI */            \
    UCA0IE |= UCRXIE;                         /* Enable USCI_A0 RX interrupt */ \
    P1SEL1 &= ~(BIT4 | BIT5);                 /* USCI_A0 UART: P1.4, P1.5 */    \
    P1SEL0 |= BIT4 | BIT5;                                                      \
    PM5CTL0 &= ~LOCKLPM5;                     /* unlock to activate */

void COM_print(const char*, uint8_t);
void COM_puts(const char * s);
void COM_TXHex(uint16_t);
void COM_DumpBytes(uint8_t *data, int len);

#endif
