#include <_drv/virtual_com_cmds.h>

/**
 *	@brief	This function prints a character string to the UART.<br>
 *			It will stop after the first non-printable character -expected to be '\r'.
 *	@param	s is a pointer to the start of the string.
 * */
void COM_print(const char * s, uint8_t len)
{
	do
	{
	    UART_TX_WAIT_BUF_EMPTY;				/* USCI_A0 TX buffer empty? */
	    UART_TX_BUFFER_CHAR;				/* Send the character */
	} while (--len);						/* repeat <len> times */
}

/**
 *	@brief	This function converts and prints an unsigned char using hex format display.
 *	@param	n is the unsigned char number (between o and 255)
 */
void COM_TXHex(uint16_t v)
{
	char msg[] = {"x0000 "}, c;
	unsigned i= 4;

	do
	{
		c = v & 0x0f;								/* extract less significant nibble for processing */
		msg[i] = c < 0x0a ? c+0x30 : c+0x57;		/* convert the nibble into a printable hex */
		v>>=4;										/* extract more significant nibble for processing */
	} while(--i);

	COM_print(msg, 6);								/* send the whole thing for printing */

}

void COM_puts(const char * s)
{
	while (*s)
	{
	    UART_TX_WAIT_BUF_EMPTY;				/* USCI_A0 TX buffer empty? */
	    UART_TX_BUFFER_CHAR;				/* Send the character */
	}
}

#define DUMP_LINE_SZ	(16)

void COM_DumpBytes(uint8_t *data, int len)
{
	uint8_t str[4] = {0, 0, '\n', '\r'};
	int count = 1;

	while (len--)
	{
		uint8_t c = *data++;
		uint8_t d = c >> 4;

		c &= 0xf;
		str[0] = d < 0x0a ? d+0x30 : d+0x57;
		str[1] = c < 0x0a ? c+0x30 : c+0x57;

		if ((0 == ((count++)%DUMP_LINE_SZ)) || (0 == len))
		{
			str[2] = '\n';
			COM_print((char*)str, 4);
		}
		else
		{
			str[2] = ' ';
			COM_print((char*)str, 3);
		}
	}
}

