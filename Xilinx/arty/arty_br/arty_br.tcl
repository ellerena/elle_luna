#
# Vivado (TM) v2015.2 (64-bit)
#
# arty_br.tcl: Tcl script for re-creating project 'arty_br'
#
# Generated by Vivado on Sat Mar 04 19:23:27 -0500 2017
# IP Build 1264090 on Wed Jun 24 14:22:01 MDT 2015
#
# This file contains the Vivado Tcl commands for re-creating the project to the state*
# when this script was generated. In order to re-create the project, please source this
# file in the Vivado Tcl Shell.
#
# * Note that the runs in the created project will be configured the same way as the
#   original project, however they will not be launched automatically. To regenerate the
#   run results please launch the synthesis/implementation runs as needed.
#
#*****************************************************************************************
# NOTE: In order to use this script for source control purposes, please make sure that the
#       following files are added to the source control system:-
#
# 1. This project restoration tcl script (arty_br.tcl) that was generated.
#
# 2. The following source(s) files that were local or imported into the original project.
#    (Please see the '$orig_proj_dir' and '$origin_dir' variable setting below at the start of the script)
#
#    <none>
#
# 3. The following remote source files that were added to the original project:-
#
#    "D:/Projects/Git/elle_luna/Xilinx/arty/arty_br/arty_br.srcs/sources_1/bd/system/system.bd"
#    "D:/Projects/Git/elle_luna/Xilinx/arty/arty_br/arty_br.srcs/sources_1/bd/system/hdl/system_wrapper.v"
#    "D:/Projects/Git/elle_luna/Xilinx/arty/arty_br/arty_br.srcs/sources_1/bd/system/ip/system_mig_7series_0_0/board.prj"
#    "D:/Projects/Git/elle_luna/Xilinx/arty/arty_br/src/cnst/arty_br.xdc"
#
#*****************************************************************************************

# set origin directory path location variable
if { [info exists ::origin_dir_loc] } {
  set origin_dir $::origin_dir_loc
} else {
  set origin_dir [file dirname [info script]]
}

set proj_name [file rootname [file tail [info script]]]
set git_dir $origin_dir/../../../..
set ip_loc_dir $origin_dir/../../ip

puts "--> Creating new project $proj_name in [file normalize $origin_dir]"

# Create project
create_project arty_br $origin_dir

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Set project properties
set obj [get_projects arty_br]
set_property "board_part" "digilentinc.com:arty:part0:1.1" $obj
set_property "default_lib" "xil_defaultlib" $obj
set_property "simulator_language" "Mixed" $obj

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# Set IP repository paths
set obj [get_filesets sources_1]
set_property "ip_repo_paths" "[file normalize "$git_dir/vivado/ip"]" $obj

# Rebuild user ip_repo's index before adding any source files
puts "---> Updating IP catalogs"
update_ip_catalog -rebuild

#################################################
# Create block design
puts "---> Creating block design from board tcl"
source $origin_dir/src/bd/system.tcl -notrace

# Generate the wrapper
puts "---> Adding top wrapper"
set design_name [get_bd_designs]
make_wrapper -files [get_files $design_name.bd] -top -import
#################################################

# Set 'sources_1' fileset object
puts "---> Adding source files"
##set obj [get_filesets sources_1]
##set files [list \
## "[file normalize "$origin_dir/arty_br.srcs/sources_1/bd/system/system.bd"]"\
## "[file normalize "$origin_dir/arty_br.srcs/sources_1/bd/system/hdl/system_wrapper.v"]"\
## "[file normalize "$origin_dir/arty_br.srcs/sources_1/bd/system/ip/system_mig_7series_0_0/board.prj"]"\
##]
##add_files -norecurse -fileset $obj $files

# Set 'sources_1' fileset file properties for remote files
puts "---> Set properties for sources_1 content"
set file "$origin_dir/arty_br.srcs/sources_1/bd/system/system.bd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
if { ![get_property "is_locked" $file_obj] } {
  set_property "generate_synth_checkpoint" "0" $file_obj
}


# Set 'sources_1' fileset file properties for local files
# None

# Set 'sources_1' fileset properties
set obj [get_filesets sources_1]
set_property "top" "system_wrapper" $obj

# Create 'constrs_1' fileset (if not found)
if {[string equal [get_filesets -quiet constrs_1] ""]} {
  create_fileset -constrset constrs_1
}

# Set 'constrs_1' fileset object
puts "---> Adding constrainst file"
set obj [get_filesets constrs_1]

# Add/Import constrs file and set constrs file properties
set file "[file normalize "$origin_dir/src/const/arty_br.xdc"]"
set file_added [add_files -norecurse -fileset $obj $file]
set file "$origin_dir/src/const/arty_br.xdc"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$file"]]
set_property "file_type" "XDC" $file_obj

# Set 'constrs_1' fileset properties
set obj [get_filesets constrs_1]
set_property "target_constrs_file" "[file normalize "$origin_dir/src/const/arty_br.xdc"]" $obj

# Create 'sim_1' fileset (if not found)
if {[string equal [get_filesets -quiet sim_1] ""]} {
  create_fileset -simset sim_1
}

# Set 'sim_1' fileset object
set obj [get_filesets sim_1]
# Empty (no sources present)

# Set 'sim_1' fileset properties
set obj [get_filesets sim_1]
set_property "top" "system_wrapper" $obj
set_property "xsim.compile.xvhdl.nosort" "1" $obj
set_property "xelab.unifast" "" $obj

# Create 'synth_1' run (if not found)
if {[string equal [get_runs -quiet synth_1] ""]} {
  create_run -name synth_1 -part xc7a35ticsg324-1L -flow {Vivado Synthesis 2015} -strategy "Vivado Synthesis Defaults" -constrset constrs_1
} else {
  set_property strategy "Vivado Synthesis Defaults" [get_runs synth_1]
  set_property flow "Vivado Synthesis 2015" [get_runs synth_1]
}
set obj [get_runs synth_1]

# set the current synth run
current_run -synthesis [get_runs synth_1]

# Create 'impl_1' run (if not found)
if {[string equal [get_runs -quiet impl_1] ""]} {
  create_run -name impl_1 -part xc7a35ticsg324-1L -flow {Vivado Implementation 2015} -strategy "Vivado Implementation Defaults" -constrset constrs_1 -parent_run synth_1
} else {
  set_property strategy "Vivado Implementation Defaults" [get_runs impl_1]
  set_property flow "Vivado Implementation 2015" [get_runs impl_1]
}
set obj [get_runs impl_1]
set_property "steps.write_bitstream.args.readback_file" "0" $obj
set_property "steps.write_bitstream.args.verbose" "0" $obj

# set the current impl run
current_run -implementation [get_runs impl_1]

regenerate_bd_layout
puts "INFO: Project created:arty_br"
