/*
 * main.c
 *
 *  Created on: Jan 17, 2019
 *      Author: Eddie.Llerena
 */

#include <stdio.h>
#include <stdint.h>
#include "platform.h"
#include "xil_printf.h"
#include "xiomodule.h"

#define CRLF            "\r\n"
#define ADDR_SEL(x)     XIOModule_DiscreteWrite(&io, 1, (x))
#define DATA_RD         XIOModule_DiscreteRead(&io, 1)
#define	UART_O(x)      XIOModule_DiscreteWrite(&io, 2, (x))
#define UART_I          XIOModule_DiscreteRead(&io, 2)
#define LED(x)          XIOModule_DiscreteWrite(&io, 3, (x))
#define CTRL_WR(x)      XIOModule_DiscreteWrite(&io, 4, (x))
#define UART_TX_BUSY    (UART_I & (1 << 9))
#define UART_RX_RDY     (UART_I & (1 << 8))
#define UART_TX(x)      do{UART_O(((x)&0xff)|(1 << 8)); UART_O(0);} while(0)

XIOModule io;
unsigned char c2b[] = { 0, 1, 2, 5, 11, 6, 13, 10, 4, 9, 3, 7, 15, 14, 12, 8 };

static unsigned int cod2bin(unsigned int in) {
   return (c2b[in & 0xf] + (c2b[(in >> 4) & 0xf] << 4)
         + (c2b[(in >> 8) & 0xf] << 8));
}

void SendWord(unsigned int data) {
   UART_TX(data);
   while (UART_TX_BUSY)
      ;
   UART_TX(data >> 8);
   while (UART_TX_BUSY)
      ;
   UART_TX(data >> 16);
   while (UART_TX_BUSY)
      ;
   UART_TX(data >> 24);
   while (UART_TX_BUSY)
      ;
}

void SendByte(unsigned int data) {
   UART_TX(data);
   while (UART_TX_BUSY)
      ;
}

void COM_puts(const char *s) {
   while (*s > 9) {
      UART_TX(*s++);
      while (UART_TX_BUSY)
         ;
   }
}

unsigned char COM_getc(void) {
   unsigned char data;

   while (!UART_RX_RDY)
      ;
   data = (UART_I & 0xff);
   UART_O(1 << 10);

   return data;
}

void timerTick(void* ref) {
}

/**
 @brief	prints a 32 bit number in hexadecimal format.
 @param	n is the number to print
 @param	w is the minimal number of chars to print minus 1, can be [0..7].
 @verbatim
 e.g.	n = 1, w = 3 -> 0001<br>
 n = 0, w = 2 -> 00
 @endverbatim
 */
void put_hex(unsigned int n) {
   char c;
   unsigned int f = 0x7u;

   do /* loop through each printable nible in n */
   {
      n = (n << 4) | (n >> 28); /* move nible to the 4 LSB */
      c = n & 0xf; /* extract nible only */
      c += ((c <= 9) ? 0x30 : 0x57); /* convert number to printable char */
      SendByte(c); /* print the nible in text format */
   } while (f--); /* loop for al nibles */
}

/**
 @brief	this is the external function to print text and numbers (as hex)
 @param	pre, post are pointers to text we want before/after the number
 @param	n, is the number we want to print
 */
void COM_hex(const char *pre, unsigned int n, const char *post) {
   COM_puts(pre);
   put_hex(n);
   COM_puts(post);
}

int main() {
   char s[] = CRLF "Mojo V3 v.00.00 - " __DATE__ " " __TIME__ CRLF ">", tsk =
         0x18u;
   unsigned int i;

   init_platform();
   XIOModule_Initialize(&io, XPAR_IOMODULE_0_DEVICE_ID); // Initialize the io module
//	microblaze_register_handler(XIOModule_DeviceInterruptHandler, XPAR_IOMODULE_0_DEVICE_ID); // register the interrupt handler
   XIOModule_Start(&io); // start the io module
//	XIOModule_Connect(&io, XIN_IOMODULE_FIT_1_INTERRUPT_INTR, timerTick, NULL); // register timerTick() as our interrupt handler
//	XIOModule_Enable(&io, XIN_IOMODULE_FIT_1_INTERRUPT_INTR); // enable the interrupt
//	microblaze_enable_interrupts(); // enable global interrupts

   CTRL_WR(127 << 24);
   LED(0xff);
   COM_puts(s);

   while (1) {
      tsk = COM_getc();
      LED(tsk);
      if (tsk == '.')
         break;
      switch (tsk) {
      case 'r':                    // reset/init
         ADDR_SEL(0);
         i = DATA_RD;
         CTRL_WR(i | 0xf);
         CTRL_WR(i & ~0xf);

      case 'k':                    // Dummy response just to serve request
         SendByte(0);
         break;

      case 'l':                    // read latch time interval
         ADDR_SEL(3);
         i = cod2bin(DATA_RD);
         SendWord(i);
         break;

      case 'i':
         COM_puts(s);
         break;

      case '0':
      case '1':
      case 'g':
         ADDR_SEL(0);
         i = DATA_RD;
         i &= ~(1 << 8);
         if (tsk == '1')
            i |= (1 << 8);
         if (tsk == 'g')
            CTRL_WR(i | (1 << 4));
         CTRL_WR(i);
         COM_hex("", i, CRLF ">");
         break;

      default:
         ADDR_SEL(0);
         SendWord(DATA_RD);
         ADDR_SEL(1);
         SendWord(DATA_RD);
         ADDR_SEL(2);
         SendWord(DATA_RD);
         break;
      }
   }

   CTRL_WR(0xfu);
   CTRL_WR(0);
   LED(0xff);
   cleanup_platform();

   return 0;
}
