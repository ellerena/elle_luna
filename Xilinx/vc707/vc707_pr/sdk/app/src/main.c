/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

#include <stdio.h>
#include "platform.h"
#include "BR_gpio.h"
#include "BR_timer.h"
#include "BR_lcd.h"
#include "br_regs.h"
#include "br_int.h"
#include "device/BR_iic.h"
#include "ftdi_fmc_if.h"

#define FW_VERSION			"0.0.0"
#define	HW_VERSION			"0.0.0"

#define PRINTF				xil_printf
#define POINTER_TO_ARGS		(2)							/* index of the location of the 'arguments' in the buffer array */
#define BUFFER_SIZE			(20)						/* max number of bytes that can be received in a command entry */
#define TASK				acCommand[0]				/* variable to hold the requested 'task' */
#define	MODE				acCommand[1]				/* variable to hold the 'mode' for the TASK*/

u32 iic_reg[] = {
		REG_IIC_DGIER,
		REG_IIC_IISR,
		REG_IIC_IIER,
		REG_IIC_RESETR,
		REG_IIC_CR_REG,
		REG_IIC_SR_REG,
		REG_IIC_DTR_REG,
		REG_IIC_DRR_REG,
		REG_IIC_ADR_REG,
		REG_IIC_TFO_REG,
		REG_IIC_RFO_REG,
		REG_IIC_TBA_REG,
		REG_IIC_RFD_REG,
		REG_IIC_GPO_REG
};

char iic_reg_t[][14] = {
		"GIE         ",
		"ISR         ",
		"IER         ",
		"SOFTR       ",
		"CR          ",
		"SR          ",
		"TX_FIFO     ",
		"RX_FIFO     ",
		"ADR         ",
		"TX_FIFO_OCY ",
		"RX_FIFO_OCY ",
		"TEN_ADR     ",
		"RX_FIFO_PIRQ",
		"GPO         ",
		"TSUSTA      ",
		"TSUSTO      ",
		"THDSTA      ",
		"TSUDAT      ",
		"TBUF        ",
		"THIGH       ",
		"TLOW        ",
		"THDDAT      "
};



int ParseCharsToValueHex(char *p);

u8 acCommand[BUFFER_SIZE];								/* buffer used to move commands and arguments around */
u8 *acArgs = &acCommand[POINTER_TO_ARGS];				/* pointer to the arguments section in the buffer */
u32 flags = 0;

static void BR_PB_Processor(u32 pb)
{
	switch(pb)
	{
		case PB_CENTER:
		case PB_NORTH:
		case PB_SOUTH:
		case PB_EAST:
		case PB_WEST:
		default:
			PRINTF("PB 0x%08X\n", pb);
			break;
	}
	while((pb = PUSH_RD_DATA32));										/* de-bounce: wait for button release */
}


int main()
{

	u32 tmp;
	u8 * s;

	init_platform();

	BR_TIM_initialize();							/* Initialize Timers */
	BR_GPIO_init();							/* Initialize GPIO connections for LCD, LED, PB and DIP switches */
	BR_Intc_Initialize();							// commented out: we don't use the interrupt controller for now
	BR_LCD_init();									/* Initialize the LCD display module */

	PRINTF("\r\nPhysical Layer Initialized\r\n"\
				"H:" HW_VERSION " F:" FW_VERSION \
				"\r\nb:" __TIME__ "\r\n");

	BR_LCD_goto(0,0);
	BR_LCD_puts("H:" HW_VERSION " F:" FW_VERSION);
	BR_delay_MS(1000);
	BR_LCD_goto(1,0);
	BR_LCD_puts("b:" __TIME__);
	BR_delay_MS(1000);
	while(1)
	{
		if((tmp = PUSH_RD_DATA32))					/* if push button pressed... */
		{
			BR_PB_Processor(tmp);
		}
		if(flags)
		{
			tmp = ParseCharsToValueHex((char*)acArgs);					/* convert text arguments into numbers, number of arguments stored in n */
			switch (TASK)
			{
				case 'w':
					PRINTF("%08x:%08x\n", iic_reg[acArgs[0]], acArgs[1]);
					BR_OUT32(iic_reg[acArgs[0]], acArgs[1]);
					break;
				case 'e':
					s = &acArgs[1];
					switch (MODE)
					{
						case 'w':
							while (--tmp) BR_OUT32(iic_reg[acArgs[0]], *s++);
							break;
						case 't':
							br_iic_eeprom_write(&acArgs[1], tmp-1, acArgs[0]);
							break;
						case 'r':										/* er<mem_addr><len> */
							tmp = acArgs[1];
							s = acArgs;
							tmp -= br_iic_eeprom_read(acArgs, tmp, acArgs[0]);
							//tmp = EepromReadByte(acArgs[0], acArgs, tmp);

							while(tmp--) PRINTF("%02x ", *s++);
							PRINTF("\n", *s++);
							break;
						case 'a':
							BR_OUT32(iic_reg[acArgs[0]], (acArgs[1] << 8) | acArgs[2]);
							break;
						case 'u':
							br_iic_mux_select(acArgs[0]);
							break;
						case 'i':
						    BR_IIC_initialize();
							break;
						case 'g':
							BR_OUT32(REG_IIC_GPO_REG, BR_IN32(REG_IIC_SR_REG));
							break;
						case 's':
						{
						    u8 p[256];
						    u32 i;
						    for (i = 0; i < 256; i++) p[i] = i;
						    br_iic_eeprom_write(p, 256, 0);
						    PRINTF("Done");
						    break;
						}
						default:
							break;
					}
					break;


//				case 'l':
//					LEDS_ON(1 << *acArgs);
//					break;
			}
			for(tmp = 0; tmp < 14; tmp+=2)
				PRINTF("\t%x %04x:%s  %04x\t%x %04x:%s %04x\r", tmp, BR_IN32(iic_reg[tmp]), iic_reg_t[tmp], (u16)iic_reg[tmp],
						tmp+1, BR_IN32(iic_reg[tmp+1]), iic_reg_t[tmp+1], (u16)iic_reg[tmp+1]);

			PRINTF ("#%08x : #%08x : #%08x : #%08x\r>", BR_IN32(REG_FT_STATUS), BR_IN32(REG_FT_RD), BR_IN32(REG_FT_WR), BR_IN32(REG_FT_CTRL));
			flags = 0;
		}
	}

    cleanup_platform();
    return 0;
}





/**
	@brief	converts a string of chars representing a vector of hex numbers into a vector of bytes equivalent.
	@param	p contains the address of the first char in the string.
	@retval	the count of numbers comverted.
	@verbatim
			Text entered by a user via the keyboard is in the form of a string of printable chars that represent hex numbers.
			This functions takes that string of chars and extracts the meaningful hex numbers represented in it, then
			it places those numbers (bytes) in an array at the same ram location. Returning the number of numbers
			converted.
	@endverbatim
 */
int ParseCharsToValueHex(char *p)
{
	char *q, d;
	int i, k;
	uint8_t c;

	q = p;
	k = 0;

	while (*p > 0x1f)										/* Will process until a non printable character is found */
	{
		d = 0;												/* initialize the 'number equivalent value' */
		i = 0;												/* initialize the digit counter (assume 2 digits max per number)*/
		do
		{
			d <<= 4;										/* each digit uses the first 4 bits, so on each loop we must push the others */
			c = *p++;										/* read the new char */
			if ((c > 0x29) && (c < 0x3a))					/* char is between 0 and 9 */
			{
				d += (c - 0x30);							/* add the char equivalen value to our total */
				i++;										/* increment our digit counter */
			}
			if ((c > 0x60 ) && (c < 0x67))					/* char is between 0xa and 0xf */
			{
				d += (c - 0x60 + 0x9);						/* add the char equivalen value to our total */
				i++;										/* increment our digit counter */
			}
		} while (( i == 0 ) || ((*p > 0x2f) && (i < 2)));	/* repeat: if no char found or: if found a space and less than 2 digits */

		if(i)												/* if the digit counter is non-zero then a value has been found */
		{
			*q++ = d;										/* write the number found to memory */
			k++;											/* increment 'detected numbers counter' */
		}
	}

	return k;
}
