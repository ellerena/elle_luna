/*
 * BR_iic.c
 *
 *  Created on: Aug 15, 2016
 *      Author: Eddie.Llerena
 */

#ifndef BR_IIC_C_
#define BR_IIC_C_

#ifndef UBLAZE_FW
#include "BR_regs.h"
#else

#include "xparameters.h"
#include "xil_types.h"
#include "xil_io.h"

/* I2C register base */
#define IIC_BASE_ADDR                   (XPAR_AXI_IIC_0_BASEADDR)

/* I2C register offsets from base */
#define IIC_DGIER_OFFSET                (XIIC_DGIER_OFFSET)
#define IIC_IISR_OFFSET                 (XIIC_IISR_OFFSET)
#define IIC_IIER_OFFSET                 (XIIC_IIER_OFFSET)
#define IIC_RESETR_OFFSET               (XIIC_RESETR_OFFSET)
#define IIC_CR_OFFSET                   (XIIC_CR_REG_OFFSET)
#define IIC_SR_OFFSET                   (XIIC_SR_REG_OFFSET)
#define IIC_DTR_OFFSET                  (XIIC_DTR_REG_OFFSET)
#define IIC_DRR_OFFSET                  (XIIC_DRR_REG_OFFSET)
#define IIC_ADR_OFFSET                  (XIIC_ADR_REG_OFFSET)
#define IIC_TFO_OFFSET                  (XIIC_TFO_REG_OFFSET)
#define IIC_RFO_OFFSET                  (XIIC_RFO_REG_OFFSET)
#define IIC_TBA_OFFSET                  (XIIC_TBA_REG_OFFSET)
#define IIC_RFD_OFFSET                  (XIIC_RFD_REG_OFFSET)
#define IIC_GPO_OFFSET                  (XIIC_GPO_REG_OFFSET)

/* I2C register direct access */
#define REG_IIC_DGIER                   (IIC_BASE_ADDR + XIIC_DGIER_OFFSET)
#define REG_IIC_IISR                    (IIC_BASE_ADDR + XIIC_IISR_OFFSET)
#define REG_IIC_IIER                    (IIC_BASE_ADDR + XIIC_IIER_OFFSET)
#define REG_IIC_RESETR                  (IIC_BASE_ADDR + XIIC_RESETR_OFFSET)
#define REG_IIC_CR_REG                  (IIC_BASE_ADDR + XIIC_CR_REG_OFFSET)
#define REG_IIC_SR_REG                  (IIC_BASE_ADDR + XIIC_SR_REG_OFFSET)
#define REG_IIC_DTR_REG                 (IIC_BASE_ADDR + XIIC_DTR_REG_OFFSET)
#define REG_IIC_DRR_REG                 (IIC_BASE_ADDR + XIIC_DRR_REG_OFFSET)
#define REG_IIC_ADR_REG                 (IIC_BASE_ADDR + XIIC_ADR_REG_OFFSET)
#define REG_IIC_TFO_REG                 (IIC_BASE_ADDR + XIIC_TFO_REG_OFFSET)
#define REG_IIC_RFO_REG                 (IIC_BASE_ADDR + XIIC_RFO_REG_OFFSET)
#define REG_IIC_TBA_REG                 (IIC_BASE_ADDR + XIIC_TBA_REG_OFFSET)
#define REG_IIC_RFD_REG                 (IIC_BASE_ADDR + XIIC_RFD_REG_OFFSET)
#define REG_IIC_GPO_REG                 (IIC_BASE_ADDR + XIIC_GPO_REG_OFFSET)

#endif /* UBLAZE_FW */

/*====================================== masks =======================================*/
#define IIC_M_ISR_TXE                   (XIIC_INTR_TX_ERROR_MASK)
#define IIC_M_ISR_RXFULL                (XIIC_INTR_RX_FULL_MASK)
#define IIC_M_ISR_TXERROR               (XIIC_INTR_TX_ERROR_MASK)
#define IIC_M_ISR_IDLE                  (XIIC_INTR_BNB_MASK)
#define IIC_M_ISR_ARB                   (XIIC_INTR_ARB_LOST_MASK)
#define IIC_M_SR_BUSY                   (XIIC_SR_BUS_BUSY_MASK)
#define IIC_M_SR_TXE                    (XIIC_SR_TX_FIFO_EMPTY_MASK)
#define IIC_ENABLE                      (XIIC_CR_ENABLE_DEVICE_MASK)
#define IIC_OFF                         (0)

#define IIC_M_FIFO_RST                  (XIIC_CR_TX_FIFO_RESET_MASK)
#define IIC_M_RX                        (XIIC_CR_MSMS_MASK | XIIC_CR_ENABLE_DEVICE_MASK)
#define IIC_M_RX_NACK                   (IIC_M_RX | XIIC_CR_NO_ACK_MASK)
#define IIC_M_TX_START                  (IIC_M_RX | XIIC_CR_DIR_IS_TX_MASK)
#define IIC_M_READ                      (IIC_M_ISR_RXFULL | IIC_M_ISR_TXERROR | IIC_M_ISR_ARB)

/*================================= bit definitions ==================================*/
#define IIC_DEV_USER_CLK                (1 << 0)
#define IIC_DEV_FMC1                    (1 << 1)
#define IIC_DEV_FMC2                    (1 << 2)
#define IIC_DEV_EEPROM                  (1 << 3)
#define IIC_DEV_SFP                     (1 << 4)
#define IIC_DEV_HDMI                    (1 << 5)
#define IIC_DEV_DDR3                    (1 << 6)
#define IIC_DEV_5324_CLK                (1 << 7)

#define IIC_B_START                     (1 << 8)
#define IIC_B_STOP                      (1 << 9)

/*==================================== constants =====================================*/
#define IIC_EEP_DIG_ADR                (0x00)

#define IIC_SOFTR_RKEY                  (0x0a)
#define IIC_MASTER_ADDR                 (0x74)
#define IIC_EEP_ADDR                    (0x54)

#define EEPROM_SIZE                     (1024)
#define EEPROM_PAGE_SZ                  (16)

/*====================================== macros ======================================*/
#define IIC_ADR_RD(x)                   (((x) << 1) | XIIC_READ_OPERATION)
#define IIC_ADR_WR(x)                   (((x) << 1) | XIIC_WRITE_OPERATION)
#define IIC_BYTE_S(x)                   ((x) | IIC_B_START)
#define IIC_BYTE_P(x)                   ((x) | IIC_B_STOP)
#define IIC_EEP_SEL_W(a)                IIC_ADR_WR((((a>>8)&3)|IIC_EEP_ADDR))        /* EEPROM select address for writes */
#define IIC_EEP_SEL_R(a)                 IIC_ADR_RD((((a>>8)&3)|IIC_EEP_ADDR))        /* EEPROM select address for reads */
#define IIC_EEP_ADR(a)                  (a&0xff)                                        /* EEPROM address byte */


void BR_IIC_initialize(void);
void br_iic_mux_select(u32 device);
u32 br_iic_eeprom_read(u8 * data, u32 len, u32 address);
u32 br_iic_eeprom_write(u8 * data, u32 len, u32 address);


#define IIC_BASE_ADDRESS    XPAR_IIC_0_BASEADDR

/*
 * The following constant defines the address of the IIC Slave device on the
 * IIC bus. Note that since the address is only 7 bits, this constant is the
 * address divided by 2.
 * The 7 bit IIC Slave address of the IIC EEPROM on the ML300/ML310/ML403/ML410/
 * ML501/ML505/ML507/ML510 boards is 0x50. The 7 bit IIC Slave address of the
 * IIC EEPROM on the ML605/SP601/SP605 boards is 0x54.
 * Please refer the User Guide's of the respective boards for further
 * information about the IIC slave address of IIC EEPROM's.
 */
#define EEPROM_ADDRESS  0x54     /* 0xA0 as an 8 bit number */

/*
 * The page size determines how much data should be written at a time.
 * The ML300 board supports a page size of 32 and 16
 * The write function should be called with this as a maximum byte count.
 */
#define PAGE_SIZE   16

/*
 * The Starting address in the IIC EEPROM on which this test is performed
 */
#define EEPROM_TEST_START_ADDRESS   128


/**************************** Type Definitions *******************************/

/*
 * The AddressType for ML300/ML310/ML510 boards should be u16 as the address
 * pointer in the on board EEPROM is 2 bytes.
 * The AddressType for ML403/ML501/ML505/ML507/ML605/SP601/SP605 boards should
 * be u8 as the address pointer in the on board EEPROM is 1 bytes.
 */
typedef u8 AddressType;
unsigned EepromReadByte(AddressType Address, u8 *BufferPtr, u16 ByteCount);

#endif /* BR_IIC_C_ */
