/**
	@file		br_iic.c
	@author		Sonavation Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		I2C controller

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Sonavation, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#include "xiic_l.h"
#include "BR_iic.h"
#include "xil_printf.h"

#define st(x)				do { x; } while (__LINE__ == -1)
#define GPO_TOG(y)			st(xx^=y ; WR_OFF32(IIC_GPO_OFFSET, xx))
#define SHOW_ME_DATA(x)		st(WR_OFF32(IIC_GPO_OFFSET, RD_OFF32(x)))
#define SHOW_POINT			st(xil_printf("%d\n", __LINE__))

static u32 xx = 0x0;

/********************* local prototypes *********************/
/**
 * @brief   Select a device for i2c communication. This function
 *          is needed because the I2C module on the VC707 board
 *          is connected to 8 different devices, hence this
 *          functions allows us to select which one will be used.
 * @param   device, a constant that defines which device to select.
 * */
void br_iic_mux_select(u32 device);

/**
 * @brief   reads a stream of bytes from the eeprom via i2c
 * @param   data, pointer to a buffer where read bytes are placed
 * @param   len, number of bytes to read
 * @param   address, eeprom address to start reading from
 * @retun   0 on success, otherwise the number of bytes not read
 * */
u32 br_iic_eeprom_read(u8 * data, u32 len, u32 address);

/**
 * @brief   writes a stream of bytes to the eeprom via i2c
 * @param   data, pointer to a buffer where source data is located
 * @param   len, number of bytes to write
 * @param   address, eeprom address to start the write
 * @retun   0 on success, otherwise the number of bytes not written
 * */
u32 br_iic_eeprom_write(u8 * data, u32 len, u32 address);

/********************* function definitions *********************/
/**
 * @brief   This function performs a low level initialization of the AXI IIC module.
 * */
void BR_IIC_initialize(void)
{
    u32 *ptemp;

    ptemp = (u32*)IIC_BASE_ADDR;

    WR_OFF32(IIC_RESETR_OFFSET, IIC_SOFTR_RKEY);        /* reset AXI i2c bus controller */
    WR_OFF32(IIC_CR_OFFSET, IIC_M_FIFO_RST);            /* reset AXI TX fifo */
    WR_OFF32(IIC_CR_OFFSET, IIC_OFF);                   /* turn AXI i2c controller off */
}

/**
 * @brief   Select a device for i2c communication. This function
 *          is needed because the I2C module on the VC707 board
 *          is connected to 8 different devices, hence this
 *          functions allows us to select which one will be used.
 * */
void br_iic_mux_select(u32 device)
{
    u32 *ptemp;

    ptemp = (u32*)IIC_BASE_ADDR;

    WR_OFF32(IIC_DTR_OFFSET, IIC_ADR_WR(IIC_MASTER_ADDR));      /* select the PCA9548 i2c mux */
    WR_OFF32(IIC_DTR_OFFSET, IIC_BYTE_P(device));               /* write desired address in TX fifo */
    WR_OFF32(IIC_CR_OFFSET, IIC_M_TX_START);                    /* transmit what's in the fifo */

    while (!IIC_SR_IS_ON(IIC_M_SR_BUSY));                       /* wait TX to start */
    while (IIC_SR_IS_ON(IIC_M_SR_BUSY));                        /* wait TX to stop */
    WR_OFF32(IIC_CR_OFFSET, IIC_OFF);                           /* turn the driver off */
}

/******************** M24C08 EEPROM 1k x 8 Api ********************/
u32 br_iic_eeprom_write(u8 * data, u32 len, u32 address)
{
    volatile u32 *ptemp, temp;

    ptemp = (u32*)IIC_BASE_ADDR;

    while (len)
    {
        WR_OFF32(IIC_DTR_OFFSET, IIC_EEP_SEL_W(address));                     /* write EEPROM address in TX fifo */
        WR_OFF32(IIC_CR_OFFSET, IIC_M_TX_START);                              /* start transmission from TX fifo */
        while (!IIC_SR_IS_ON(IIC_M_SR_BUSY));                                 /* wait TX to start */
        WR_OFF32(IIC_DTR_OFFSET, IIC_EEP_ADR(address));                       /* transmit address (low byte) */

        temp = ((address | 0xf) - address + 1);                               /* calculate how many bytes will */
        temp = (len >= temp) ? temp : len;                                    /* be transmitted (we transmit */
        address += temp;                                                      /* in groups of EEPROM_PAGE_SZ max) */
        len -= temp;
        while (temp)                                                          /* copy the data group to TX fifo */
        {
            while (!IIC_SR_IS_ON(IIC_M_SR_TXE));                              /* wait TX fifo empty */
            WR_OFF32(IIC_DTR_OFFSET, (temp-- > 1) ?                           /* copy next data value, if */
                    *data++ : IIC_BYTE_P(*data++));                           /* is the last one we add the stop bit */
        }
        while (!IIC_SR_IS_ON(IIC_M_SR_BUSY));                                 /* wait TX to start */
        while (IIC_SR_IS_ON( IIC_M_SR_BUSY));                                 /* wait TX to stop */

        while(1)                                                              /* wait for eeprom's digestion */
        {
            WR_OFF32(IIC_DTR_OFFSET, IIC_BYTE_P(IIC_EEP_SEL_W(address)));     /* write EEPROM address in TX fifo */
            WR_OFF32(IIC_CR_OFFSET, IIC_M_TX_START);                          /* transmit data in TX fifo */
            while (!IIC_SR_IS_ON(IIC_M_SR_BUSY));                             /* wait TX to start */
            WR_OFF32(IIC_IISR_OFFSET, IIC_ISR_IS_ON(IIC_M_ISR_TXE));          /* clear TX error flag */
            while (IIC_SR_IS_ON(IIC_M_SR_BUSY));                              /* wait TX to complete */
            if(!IIC_ISR_IS_ON(IIC_M_ISR_TXE)) break;                          /* exit loop when digestion is complete */
            WR_OFF32(IIC_CR_OFFSET, XIIC_CR_TX_FIFO_RESET_MASK);              /* clear TX fifo */
            WR_OFF32(IIC_CR_OFFSET, IIC_OFF);                                 /* turn AXI transmitter off */
        }
    }

    WR_OFF32(IIC_CR_OFFSET, IIC_OFF);                                         /* turn everything off while we change gears */
    return len;
}

/**
 * @brief   reads a stream of bytes from the eeprom via i2c
 * */
u32 br_iic_eeprom_read(u8 * data, u32 len, u32 address)
{
    volatile u32 *ptemp;

    if (!len) return len;
    ptemp = (u32*)IIC_BASE_ADDR;
    WR_OFF32(IIC_DTR_OFFSET, IIC_EEP_SEL_W(address));                   /* send eeprom address to TX fifo */
    WR_OFF32(IIC_DTR_OFFSET, IIC_BYTE_P(IIC_EEP_ADR(address)));         /* send data address to TX fifo */
    WR_OFF32(IIC_CR_OFFSET, IIC_M_TX_START);                            /* start transmission */
    while (!(RD_OFF32(IIC_SR_OFFSET) & IIC_M_SR_BUSY));                 /* wait TX to start */
    while ((RD_OFF32(IIC_SR_OFFSET) & IIC_M_SR_BUSY));                  /* wait TX to stop */

    WR_OFF32(IIC_IISR_OFFSET, IIC_ISR_IS_ON(IIC_M_READ));               /* clear read and error flags */
    WR_OFF32(IIC_RFD_OFFSET, 0);                                        /* set RX fifo size: we'll read 1-by-1 */
    WR_OFF32(IIC_DTR_OFFSET, IIC_EEP_SEL_R(address));                   /* send eeprom address for reads */
    WR_OFF32(IIC_CR_OFFSET, (len == 1) ? IIC_M_RX_NACK : IIC_M_RX);     /* start Rx, if it is last one add a NACK */
    while (!IIC_SR_IS_ON(IIC_M_SR_BUSY));                               /* wait TX to start */
    WR_OFF32(IIC_IISR_OFFSET, IIC_M_ISR_IDLE);                          /* clear ISR: BNB flag */

    while (len)                                                         /* loop to read byte by byte */
    {
        while(1)                                                        /* wait while RX fifo is filled */
        {
            u32 temp = RD_OFF32(IIC_IISR_OFFSET);
            if((len > 1) & (temp & IIC_M_ISR_TXERROR)) return len;      /* look for errors, if any, return */
            if (temp & IIC_M_ISR_RXFULL) break;
        }
        if(len == 1)                                                    /* once last byte is ready, disable RX */
        {
            WR_OFF32(IIC_CR_OFFSET, IIC_ENABLE);
        }
        if(len-- == 2)                                                  /* previous to last byte, request to send a NACK */
        {
            WR_OFF32(IIC_CR_OFFSET, IIC_M_RX_NACK);                     /* send NACK after next read */
        }

        *data++ = RD_OFF32(IIC_DRR_OFFSET);                             /* read data in RX fifo */
        WR_OFF32(IIC_IISR_OFFSET, IIC_ISR_IS_ON(IIC_M_READ));           /* clear flags */
    }
    while (!IIC_ISR_IS_ON(IIC_M_ISR_IDLE));                             /* wait for completion */
    WR_OFF32(IIC_CR_OFFSET, IIC_OFF);                                   /* turn everything off while we change gears */

    return len;
}

/**
 * @brief   loads digital gains stored in the eeprom into the IQ block's buffer
 * */
//u32 BR_IIC_load_DIG_from_eeprom(void)
//{
//    u32 tmp;
//
//    br_iic_mux_select(IIC_DEV_EEPROM);
//    tmp = br_iic_eeprom_read((u8*)dig_buffer, IQ_DIG_BUF_BYTES, IIC_EEP_DIG_ADR);
//    xil_printf("IQread %d\n", tmp);
//    return tmp = tmp ? 0 : IQ_DIG_BUF_BYTES;
//}

/**
 * @brief   stores digital gains from the IQ block buffer to the eeprom
 * */
//u32 BR_IIC_store_DIG_in_eeprom(void)
//{
//    u32 tmp;
//
//    br_iic_mux_select(IIC_DEV_EEPROM);
//    tmp = br_iic_eeprom_write((u8*)dig_buffer, IQ_DIG_BUF_BYTES, IIC_EEP_DIG_ADR);
//    tmp = tmp ? 0 : IQ_DIG_BUF_BYTES;
//    return tmp;
//}












/*****************************************************************************/
/**
* This function reads a number of bytes from the IIC serial EEPROM into a
* specified buffer.
*
* @param    Address contains the address in the EEPROM to read from.
* @param    BufferPtr contains the address of the data buffer to be filled.
* @param    ByteCount contains the number of bytes in the buffer to be read.
*       This value is not constrained by the page size of the device
*       such that up to 64K may be read in one call.
*
* @return   The number of bytes read. A value less than the specified input
*       value indicates an error.
*
* @note     None.
*
****************************************************************************/
unsigned EepromReadByte(AddressType Address, u8 *BufferPtr, u16 ByteCount)
{
    volatile unsigned ReceivedByteCount;
    u16 StatusReg;
    u8 EepromIicAddr;
    EepromIicAddr = EEPROM_ADDRESS;
    /*
     * Set the address register to the specified address by writing
     * the address to the device, this must be tried until it succeeds
     * because a previous write to the device could be pending and it
     * will not ack until that write is complete.
     */
    do {
        StatusReg = XIic_ReadReg(IIC_BASE_ADDRESS, XIIC_SR_REG_OFFSET);
        if(!(StatusReg & XIIC_SR_BUS_BUSY_MASK)) {
            ReceivedByteCount = XIic_Send(IIC_BASE_ADDRESS,
                            EepromIicAddr,
                            (u8 *)&Address,
                            sizeof(Address),
                            XIIC_STOP);

            if (ReceivedByteCount != sizeof(Address)) {

                /* Send is aborted so reset Tx FIFO */
                XIic_WriteReg(IIC_BASE_ADDRESS,
                        XIIC_CR_REG_OFFSET,
                        XIIC_CR_TX_FIFO_RESET_MASK);
                XIic_WriteReg(IIC_BASE_ADDRESS,
                        XIIC_CR_REG_OFFSET,
                        XIIC_CR_ENABLE_DEVICE_MASK);
            }
        }

    } while (ReceivedByteCount != sizeof(Address));

    /*
     * Read the number of bytes at the specified address from the EEPROM.
     */
    ReceivedByteCount = XIic_Recv(IIC_BASE_ADDRESS, EepromIicAddr,
                    BufferPtr, ByteCount, XIIC_STOP);

    /*
     * Return the number of bytes read from the EEPROM.
     */
    return ReceivedByteCount;
}



