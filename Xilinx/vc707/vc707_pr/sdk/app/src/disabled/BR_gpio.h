/**
	@file		BR_gpio.h
	@author		Sonavation Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		STM communication module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Sonavation, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#ifndef BR_GPIO_H_
#define BR_GPIO_H_

#include "xgpio_l.h"
#include "xparameters.h"

#define	IO_0_WR_REG32(x, y)			(*(u32*)(XPAR_GPIO_0_BASEADDR + (x)) = (y))
#define	IO_0_RD_REG32(x)			(*(u32*)(XPAR_GPIO_0_BASEADDR + (x)))
#define	IO_0_RD_REG8(x)				(*(u8*)(XPAR_GPIO_0_BASEADDR + (x)))

#define	IO_1_WR_REG32(x, y)			(*(u32*)(XPAR_GPIO_1_BASEADDR + (x)) = (y))
#define	IO_1_RD_REG32(x)			(*(u32*)(XPAR_GPIO_1_BASEADDR + (x)))
#define	IO_1_RD_REG8(x)				(*(u8*)(XPAR_GPIO_1_BASEADDR + (x)))

#define	LCD_BASE_ADDR				(XPAR_GPIO_0_BASEADDR)
#define	LED_BASE_ADDR				(XPAR_GPIO_0_BASEADDR)
#define	DIPS_BASE_ADDR				(XPAR_GPIO_1_BASEADDR)
#define	PUSH_BASE_ADDR				(XPAR_GPIO_1_BASEADDR)

#define	LED_DATA_OFFSET				(XGPIO_DATA_OFFSET)
#define	LED_DIR_OFFSET				(XGPIO_TRI_OFFSET)
#define	LCD_DATA_OFFSET				(XGPIO_DATA2_OFFSET)
#define	LCD_DIR_OFFSET				(XGPIO_TRI2_OFFSET)

#define	DIPS_DATA_OFFSET			(XGPIO_DATA_OFFSET)
#define	DIPS_DIR_OFFSET				(XGPIO_TRI_OFFSET)
#define	PUSH_DATA_OFFSET			(XGPIO_DATA2_OFFSET)
#define	PUSH_DIR_OFFSET				(XGPIO_TRI2_OFFSET)

// Masks to the pins on the GPIO port
//#define LCD_BIT_TEST	0x80		//bit 7
#define LCD_BIT_DB4		0x40		//bit 6
#define LCD_BIT_DB5		0x20		//bit 5
#define LCD_BIT_DB6		0x10		//bit 4
#define LCD_BIT_DB7		0x08		//bit 3
#define LCD_BIT_RW		0x04		//bit 2
#define LCD_BIT_RS		0x02		//bit 1
#define LCD_BIT_E		0x01		//bit 0

#define	DIPS_SET_DIR(x)				IO_1_WR_REG32(DIPS_DIR_OFFSET, x)
#define	PUSH_SET_DIR(x)				IO_1_WR_REG32(PUSH_DIR_OFFSET, x)
#define	LCD_SET_DIR(x)				IO_0_WR_REG32(LCD_DIR_OFFSET, x)
#define	LED_SET_DIR(x)				IO_0_WR_REG32(LED_DIR_OFFSET, x)
#define	LCD_WR_DATA32(x)			IO_0_WR_REG32(LCD_DATA_OFFSET, x)

/*==================================== External API ====================================*/

/* Push buttons masks */
#define	PB_ALL_MASK					(0x1F)
#define	PB_NORTH					(0x10)
#define	PB_EAST						(0x08)
#define	PB_SOUTH					(0x04)
#define	PB_WEST						(0x02)
#define	PB_CENTER					(0x01)

#define	LED_WR_DATA32(x)			IO_0_WR_REG32(LED_DATA_OFFSET, x)	/* present data on the LED (8 LSB used only) */
#define	LCD_WR_DATA8(x)				LCD_WR_DATA32((x) * 0xff)			/* write data to the LCD output register */
#define	DIPS_RD_DATA32				IO_1_RD_REG32(DIPS_DATA_OFFSET)		/* returns content of the DIP switches (only 8 LSB are valid) */
#define	PUSH_RD_DATA32				(PB_ALL_MASK & IO_1_RD_REG32(PUSH_DATA_OFFSET))	/* returns state of push buttons (bit mask defined above) */

/**
 *	@brief	Routine to initialize the devices that are controlled via GPIO: LCD, LED, Push buttons, DIP switches.
 *			Since on the FPGA side these devices are all handled from the memory space, we
 *			perform a single initialization for them all together.
 */
extern void BR_GPIO_initialize(void);

#endif /* BR_GPIO_H_ */
