/**
	@file		BR_stm.c
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		STM communication module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/
#include "device/BR_stm.h"
#include "protocol/rfdata.h"
#include "protocol/txbf.h"

#define	DIVISOR				(STM_UART_CLOCK_FREQ / (STM_UART_BAUD * 16))
#define	BAUD_LSB			(DIVISOR & XUN_DIVISOR_BYTE_MASK)
#define	BAUD_MSB			((DIVISOR >> 8) & XUN_DIVISOR_BYTE_MASK)
#define	LCR_REG_VAL			(XUN_LCR_8_DATA_BITS)						/* 8 bits/1 stop/No Par */
#define MULTIPLIER          1000
#define AD_REFERENCE        (4.73 * MULTIPLIER)

#define TOSTM               do{comm[0] = j - 1; \
                               for(int r = 0; r < j; r++) \
                               { BR_STM_send_single(&comm[r]);\
                               PRINTFV("%x ",comm[r]); } \
                               PRINTF("\n"); \
                            } while(0)
#define NXTC            	comm[j++]
#define BIT31               (1 << 31)
#define REVOLVE32(x, y)     (((x) << (y)) | ((x) >> (32-(y))))

#define MMSK                (0)
#define MPAIR               (1)
#define MSIDE               (2)

/*================================ global variables  =================================*/
const uint8_t STM_comm[] = STM_COMMANDS;
uint8_t gSTMParam[8];

/**
 * @brief	This routine sends 1 or more 1-byte commands to the STM via UART.
 * 			For each command send, it will use the STM BUSY pin to wait until
 * 			the command is executed and completed before sending the next command.
 * @param	data, is a pointer to an array in memory containing the commands to be sent in sequence.
 * @param	len, is the number of commands in the array
 */
int BR_STM_send_command(const uint8_t * data, uint32_t len)
{
#ifndef STM_DUMMY_BACK_END
	volatile uint32_t *ptemp;

	ptemp = (uint32_t*)STM_BASE_ADDR;
	while(len--)
	{
		while(!(XUN_MODEM_CTS_MASK & RD_OFF32(XUN_MSR_OFFSET)));		/* wait for STM to be ready */
		WR_OFF32(XUN_THR_OFFSET, *data++);								/* send 1 byte */
		while((XUN_MODEM_CTS_MASK & RD_OFF32(XUN_MSR_OFFSET)));			/* wait for STM to be busy */
		while(!(XUN_MODEM_CTS_MASK & RD_OFF32(XUN_MSR_OFFSET)));		/* wait for STM to be ready */
	}
#endif

	return 0;
}

/**
 * @brief	This routine performs initialization of the UART port in preparation to
 * 			interfacing with the STM32 located in the BigRed board.
 */
int BR_STM_initialize(void)
{
	volatile uint32_t * ptemp;
	int i;

	ptemp = (uint32_t*)STM_BASE_ADDR;
	WR_OFF32(XUN_FCR_OFFSET, 0);							/* don't use FIFO */// XUN_FIFO_ENABLE | XUN_FIFO_RX_TRIG_MSB);
	WR_OFF32(XUN_LCR_OFFSET, XUN_LCR_DLAB);					/* allow access to DLR */
	WR_OFF32(XUN_DRLS_OFFSET, 0xFFu);						/* prevent possible divide by 0 */
	WR_OFF32(XUN_DRLM_OFFSET, BAUD_MSB);
	WR_OFF32(XUN_DRLS_OFFSET, BAUD_LSB);
	WR_OFF32(XUN_LCR_OFFSET, LCR_REG_VAL);					/* set UART parameters */
	WR_OFF32(XUN_IER_OFFSET, 0);							/* disable all interrupts */
	WR_OFF32(XUN_MCR_OFFSET, XUN_OPTION_ASSERT_OUT1);		/* keep RTS low */

    for(i = 500; i > 0; i--)                                /* wait for STM to be ready (not busy) */
    {
#ifndef STM_DUMMY_BACK_END

    	if (!(XUN_MODEM_CTS_MASK & RD_OFF32(XUN_MSR_OFFSET)))
            i = 500;
#endif
    }

    return BR_STM_send_command(STM_comm, STM_COM_RX_INIT+1);   /* STM: setup US mode */
}

/**
 *	@brief	Instructs the STM to repeat execute the last commands previously completed.
 */
void BR_STM_Repeat_Last_Command(void)
{
#ifndef STM_DUMMY_BACK_END
	volatile uint32_t *ptemp;

	ptemp = (uint32_t*)STM_BASE_ADDR;
	while(!(XUN_MODEM_CTS_MASK & RD_OFF32(XUN_MSR_OFFSET)));		/* wait for STM to be ready */
	WR_OFF32(XUN_MCR_OFFSET, 0);									/* Toggle RTS high */
	while((XUN_MODEM_CTS_MASK & RD_OFF32(XUN_MSR_OFFSET)));			/* wait for STM to be busy */
	WR_OFF32(XUN_MCR_OFFSET, XUN_OPTION_ASSERT_OUT1);				/* Toggle CTS low */
	while(!(XUN_MODEM_CTS_MASK & RD_OFF32(XUN_MSR_OFFSET)));		/* wait for STM to be ready */
#endif
}

/**
 * @brief   This routine sends a 1-byte command to the STM via UART.
 *          For each command send, it will use the STM BUSY pin to wait until
 *          the command is executed and completed before sending the next command.
 *          Ready = 0, Busy = -1
 * @param   data, pointer to the single command to be sent.
 */
int BR_STM_send_single(const uint8_t * data)
{
#ifndef STM_DUMMY_BACK_END
    volatile uint32_t *ptemp;

    ptemp = (uint32_t*)STM_BASE_ADDR;
    while(!(XUN_MODEM_CTS_MASK & RD_OFF32(XUN_MSR_OFFSET)));        /* wait for STM to be ready */
    WR_OFF32(XUN_THR_OFFSET, *data);                                /* send 1 byte */
    while((XUN_MODEM_CTS_MASK & RD_OFF32(XUN_MSR_OFFSET)));         /* wait for STM to be busy */
    while(!(XUN_MODEM_CTS_MASK & RD_OFF32(XUN_MSR_OFFSET)));        /* wait for STM to be ready */
#endif
    return 0;
}
////////////////////////////////
/**
 * @brief   Initialize STM running parameters
 * */
void BR_STM_initParams(void)
{
    gSTMParam[TX_ARM_EVE] = POL_COM_TX_ARM_EVEN;                         /* initialize STM scan paramaters */
    gSTMParam[TX_ARM_ODD] = POL_COM_TX_ARM_ODD;                          /* initialize STM scan paramaters */
    gSTMParam[TX_SHIFT] = POL_COM_TX_SHIFT_1;                            /* initialize STM scan paramaters */
    gSTMParam[TX_CLEAN] = POL_COM_TX_SHIFT_64;                           /* initialize STM scan paramaters */
    gSTMParam[RX_ARM_EVE] = POL_COM_RX_ARM_EVEN;                         /* initialize STM scan paramaters */
    gSTMParam[RX_ARM_ODD] = POL_COM_RX_ARM_ODD;                          /* initialize STM scan paramaters */
    gSTMParam[RX_SHIFT] = POL_COM_RX_SHIFT_32;                           /* initialize STM scan paramaters */
    gSTMParam[RX_CLEAN] = POL_COM_RX_INIT;                               /* initialize STM scan paramaters */
}

/**
 * @brief   This routine implements the POL_COM_STM_REG_READ.
 * @param   data, is a pointer to a byte in ram: red_address.
 * @param   len, unused.
 * @return  A byte value returned by the Big Red board (via STM)
 */
int BR_STM_Read_Register(const uint8_t* data, uint32_t len)
{
    uint8_t command[2];

    command[0] = POL_COM_STM_REG_READ;
    command[1] = *data;
    if (-1 ==BR_STM_send_command(command, 2))
        ERRORPRINTF("STM Read Register -- STM Busy -- Timer Expired...\n");

    return BR_IN32(STM_BASE_ADDR + XUN_RBR_OFFSET);
}

static int BR_STM_store_arm_pattern(BF_TXENTRY_T* plist, uint8_t * comm)
{
    uint32_t i, j, l, k,cn;
    uint8_t c[BF_MAXDELAYSJSON], bytepat, *pc;
    float *pf;

    l = plist->valueLength;
    if (!((l | plist->txCenter)&1)) l--;

    i = ((l+8)&0x78);               // 8, 16, 24, 32...
    pf = plist->delayValues;
    pc = c;
    if (((2&l) && !(1&plist->txCenter)) || (!(2&l) && (1&plist->txCenter)))
    {
        *pc++ = 0;
        i--;
        cn = (l - (l/2))/2;
    }
    else
    {
        cn = (l - (l/2) - 1)/2;
    }
    while(i--)
    {
        *pc++ = (*pf++ == BF_DLYT_Z) ? 0 : 1;   /* mark TX line as active (1) or inactive (0) */
    }

    i = ((l+8)&0x78);               // 8, 16, 24, 32...
    pc = &c[i-1];
    k = i >> 3;

    for(i = 0; i < k; i++)
    {
        bytepat = 0;
        for (j = 0; j < 8; j++)
        {
            bytepat <<= 1;
            if (*pc--) bytepat |= 0x1;
        }
        *comm++ = POL_COM_STM_REG_WRITE;
        *comm++ = POL_WR_TX_LOAD_BYTE;
        *comm++ = ~bytepat;
    }

    *comm++ = POL_COM_STM_REG_WRITE;
    *comm++ = POL_WR_TX_SHIFT;
    *comm++ = (TX_SENSOR_SHIFT - cn);        /* TX beamforming shift */;

    return (3*(k+1));
}

/**
 * @brief   detects configuration (half/full, odd/even) for each channel
 * */
static void BR_STM_store_mask_build(BF_TXENTRY_T* plist, uint32_t * msk)
{
    uint32_t i, dlyinx, pl_side, pl_pair, pl_mask;
    uint8_t c[BF_MAXDELAYSJSON];
    float a, b;

    i = plist->valueLength;
    if (!((i | plist->txCenter)&1)) i--;
    i = 0x3f & (plist->txCenter - (i/2));

    for (dlyinx = 0; dlyinx < BF_MAXDELAYSJSON; dlyinx++)
    {
        c[i] = (plist->delayValues[dlyinx] == BF_DLYT_Z) ? 0 : 1;   /* mark TX line as active (1) or inactive (0) */
        i = (i+1) & 0x3f;
    }

    pl_mask = pl_pair = pl_side = 0;
    for (i = 0; i < BF_MAXDELAYSJSON; i+=2)
    {
        pl_side >>= 1;
        pl_pair >>= 1;
        pl_mask >>= 1;
        a = c[i];
        b = c[i+1];

        if(a && b)
        {
            pl_pair |= BIT31;
            pl_mask |= BIT31;
        } else if (a)
        {
            pl_mask |= BIT31;
        } else if (b)
        {
            pl_side |= BIT31;
            pl_mask |= BIT31;
        }
    }
    msk[MMSK] = pl_mask;
    msk[MPAIR] = pl_pair;
    msk[MSIDE] = pl_side;
}

/**
 * @brief   detects continuous patterns between two scans
 * */
static int BR_STM_store_mask_compare(uint32_t * pold, uint32_t * pnew)
{
    uint32_t old, new;
    int i;

    old = pold[0];
    new = pnew[0];

    i = 0;
    do
    {
        if (new == old)                                        /* new value is just a revolve of old */
        {
            if (pnew[1] == REVOLVE32(pold[1], i))
                if (pnew[2] == REVOLVE32(pold[2], i)) break;  /* new mask is same as old but revolved */
        }
        old = REVOLVE_DOWN(old);
    } while (++i < 32);

    return i;
}

static int BR_STM_store_arm(uint32_t *msk, uint8_t * comm)
{
    uint32_t i, n, m;

    m = msk[MMSK];
    n = 0;
    for (i = 0; i < 32; i++)
    {
        if(m&1) n++;
        m >>= 1;
    }
    i = n >> 1;
    comm[0] = comm[3] = POL_COM_STM_REG_WRITE;
    comm[1] = POL_WR_TX_ARM_SIZE;
    comm[4] = POL_WR_TX_ARM_SHIFT;
    comm[2] = n;                          /* TX beamforming size */
    comm[5] = (TX_SENSOR_SHIFT-i);        /* TX beamforming shift */

    return 6;
}

static uint32_t BR_STM_store_line_pre(uint8_t *com, uint32_t line)
{
    uint32_t j = 0;
    int tmp = 0;

    if(gFlags & GST_BFLATCH)
    {
        if(line > 1)                               /* clean previous load */
        {
            tmp = (gTX_Scan_Parameters >> 24) - (line >> 1);
            if(tmp > 0)
            {
                com[j++] = POL_COM_STM_REG_WRITE;
                com[j++] = POL_WR_TX_SHIFT;
                com[j++] = tmp;
            }
        }
        com[j++] = POL_COM_STM_REG_WRITE;          /* set beam size */
        com[j++] = POL_WR_TX_ARM_SIZE;
        com[j++] = gTX_Scan_Parameters;
        com[j++] = POL_COM_STM_REG_WRITE;          /* set initial shift */
        com[j++] = POL_WR_TX_ARM_SHIFT;
        com[j++] = (gTX_Scan_Parameters >> 8) + (line >> 1);
        com[j++] = POL_COM_STM_REG_WRITE;          /* set TX normal operation */
        com[j++] = POL_STM_DBG_FLAGS;
        com[j++] = 0;
        com[j++] = gSTMParam[line & 1];            /* select and latch TX */
    }
    else
    {
        if(line > 1)
        {
            com[j++] = POL_COM_TX_SHIFT_1;         /* simple shift one */
        }
        else
        {
            com[j++] = POL_COM_STM_REG_WRITE;      /* set beam size */
            com[j++] = POL_WR_TX_ARM_SIZE;
            com[j++] = gTX_Scan_Parameters;
            com[j++] = POL_COM_STM_REG_WRITE;      /* set initial shift */
            com[j++] = POL_WR_TX_ARM_SHIFT;
            com[j++] = gTX_Scan_Parameters >> 8;
            com[j++] = gSTMParam[line & 1];        /* enable 1st sensor TX */
        }
    }
    if(gFlags & GST_BFLATCH)
    {
        tmp = ((gTX_Scan_Parameters & 0xff) >> 1) - (line >> 1);
        if(tmp > 0)                               /* clean previous load */
        {
            com[j++] = POL_COM_STM_REG_WRITE;
            com[j++] = POL_WR_TX_SHIFT;
            com[j++] = tmp;
        }
        com[j++] = POL_COM_STM_REG_WRITE;          /* set beam size */
        com[j++] = POL_WR_TX_ARM_SIZE;
        com[j++] = gTX_Scan_Parameters >> 16;
        com[j++] = POL_COM_STM_REG_WRITE;          /* set initial shift */
        com[j++] = POL_WR_TX_ARM_SHIFT;
        com[j++] = (64 - (gTX_Scan_Parameters >> 24) + (line >> 1));
        com[j++] = POL_COM_STM_REG_WRITE;          /* disable latching */
        com[j++] = POL_STM_DBG_FLAGS;
        com[j++] = F_DBG_LATCH_TX;
        com[j++] = gSTMParam[line & 1];            /* select without latch TX */
    }

    return j;
}

/**
 * @brief   build the STM_store using legacy scan method (TX BF independent)
 * */
void BR_STM_store_init_legacy_scan(void)
{
    uint32_t txl, j, r, t, g;                                /* RX group, TX line counters */
    uint8_t comm[40] = {POL_COM_STM_SCAN_ARM, POL_COM_STM_BYTES_WRITE};

    BR_STM_send_command(comm, 2);                            /* STM point to begin of command table */

    j = 1;

    for(r = RX_ARM_ODD;  r >= RX_ARM_EVE; r--)
    {
        t = TX_ARM_ODD;
        do
        {
            g = r;                                           /* g = either RX_ARM_ODD or RX_ARM_EVEN */
            do
            {
                comm[j++] = gSTMParam[g];                    /* arm to rx group */
                for (txl = t; txl < BF_MAXENTRIES; txl += 2)
                {
                    j += BR_STM_store_line_pre(&comm[j], txl);
                    PRINTFV("%3d: ", txl);
                    TOSTM; j = 1;
                }
                g = (g | 1) + 1;                             /* g = RX_SHIFT */
            } while (g <= RX_SHIFT);
        } while (t-- != TX_ARM_EVE);
    }

    comm[1] = gSTMParam[TX_CLEAN];                           /* clean up commands */
    comm[2] = gSTMParam[RX_CLEAN];
    comm[3] = POL_COM_STM_REG_WRITE;                         /* set TX normal operation */
    comm[4] = POL_STM_DBG_FLAGS;
    comm[5] = 0;
    j = 6; TOSTM;
    comm[0] = 0;                                             /* table end mark */
    for(txl = 0; txl < 5; txl++)
    {
        BR_STM_send_single(comm);
    }
    ERRORPRINTFV("%d\n", __LINE__);
}

void BR_STM_store_init(void)
{
    BF_TXENTRY_T* plist;
    uint32_t txl = 0, rxg = 0, txl_, rxg_, rxs, rxs_, i, j, proc; /* RX group, TX line counters */
    uint8_t comm[40] = {POL_COM_STM_SCAN_ARM, POL_COM_STM_BYTES_WRITE};
    uint8_t rxgtab[] = {3, 5, 2, 4};
    uint32_t msk[3] = {0, 0, 0}, msk_[3] = {0, 0, 0};           /* mask, pairs, side */
    int pf;

    BR_STM_send_command(comm, 2);                               /* STM point to begin of table */
                                                                /* preloads & force 1st run */
    plist = &(gBfStore.playList[0]);                            /* start of playlist */
    rxg_ = 1 ^ rxgtab[plist->groupID[0]];
    txl_ = 0;
    rxs_ = 1 ^ plist->groupType;

    i = gBfStore.playListIndex;
    while (i--)
    {
        j = 1;                                                  /* start command counter */
        BR_STM_store_mask_build(plist, msk);                    /* calculate masks */
        pf = BR_STM_store_mask_compare(msk_, msk);              /* check if delay pattern changed */
        rxg = rxgtab[plist->groupID[0]];
        txl = plist->txCenter;
        rxs = plist->groupType;
        proc = 0;                                               /* reset all process flags */

        if((rxg != rxg_) || (rxs != rxs_))                              // distinct rx group
        {
            if (rxs != rxs_)
            {
                proc |= F_STO_RX_RESTART;
            }
            else if((rxg & 1) == (rxg_ & 1))                    // same side
            {
                if(rxg > rxg_)                                  // shift fwd 32, 64 or 96
                {
                    proc |= F_STO_RX_SHIFT;
                }
                else if(rxg == (rxg_ - 2))                      // shift back
                {
                    proc |= F_STO_RX_RESTART;
                }
            }
            else                                                // phase change
            {
                proc |= F_STO_RX_RESTART;
            }
        }
        if(31 < pf)                                             /* distinct array masks */
        {
            proc |= (F_STO_TX_RESIZE | F_STO_TX_RESTART |
                    F_STO_TX_CUSTOM | F_STO_TX_CLEAN);         /* force re-start TX */
        }
        else if(txl != txl_)                                  // distinct tx line
        {
            if((txl & 1) == (txl_ & 1))                         // shift same side
            {
                if(txl == (txl_ + 2))                           // consecutive line
                {
                    proc |= F_STO_TX_SHIFT1;
                }
                else if(txl > txl_)                           // shift forward
                {
                    proc |= F_STO_TX_SHIFT_LNG;
                }
                else                                            // shift backwards
                {
                    proc |= (F_STO_TX_RESTART | F_STO_TX_RESIZE | F_STO_TX_CLEAN | F_STO_TX_CUSTOM);  /*TODO*/
                }
            }
        }
        else
        {
            proc |= F_STO_TX_NOP;
        }

        if (proc & F_STO_TX_NOP)
        {
            NXTC = POL_COM_NOP;
        }
        if (proc & F_STO_TX_CLEAN)
        {
            if((txl>>1) < (192-(txl_>>1)))               // clean tx pipe
            {
                NXTC = POL_COM_STM_REG_WRITE;
                NXTC = POL_WR_TX_SHIFT;
                NXTC = (192-(txl_>>1)) - (txl >> 1);
            }
        }
        if (proc & F_STO_TX_RESIZE)                     /* re-build TX pattern */
        {
            j += (proc & F_STO_TX_CUSTOM) ? BR_STM_store_arm_pattern(plist, &comm[j]):
                                            BR_STM_store_arm(msk, &comm[j]);
        }
        if (proc & F_STO_TX_SHIFT1)                     /* Shift 1 */
        {
            NXTC = gSTMParam[TX_SHIFT];
        }
        if (proc & F_STO_TX_RESTART)
        {
            if (!(proc & F_STO_TX_CUSTOM))
            {
                NXTC = msk[MPAIR] ? POL_COM_TX_ARM_PAIR :
                        gSTMParam[(txl & 1) ? TX_ARM_ODD : TX_ARM_EVE];
            }
            if(txl > 129)                                // plus tx offset
            {
                NXTC = POL_COM_STM_REG_WRITE;
                NXTC = POL_WR_TX_SHIFT;
                NXTC = (txl - 128) >> 1;
            }
        }
        if (proc & F_STO_TX_SHIFT_LNG)                  /* shift to far position */
        {
            NXTC = POL_COM_STM_REG_WRITE;
            NXTC = POL_WR_TX_SHIFT;
            NXTC = ((txl - txl_) >> 1);
        }
        if (proc & F_STO_RX_SHIFT)
        {
            NXTC = (POL_COM_RX_SHIFT_32 + ((rxg - rxg_) >> 1) - 1);
        }
        if (proc & F_STO_RX_RESTART)
        {
            NXTC = POL_COM_RX_INIT;
            NXTC = (rxs & 1) ? POL_COM_RX_ARM_PAIR :
                    gSTMParam[(rxg & 1) ? RX_ARM_ODD : RX_ARM_EVE];
            if(rxg > 3) NXTC = (POL_COM_RX_SHIFT_32 + ((rxg >> 1) - 2));
        }

        /*ERRORPRINTFV("%d, %d ", rxg, txl);*/
        PRINTFV("%3d.%02d %08x/%08x/%08x %2d p%08x ",
            plist->txCenter, 0x3f & plist->txCenter, msk[MMSK], msk[MPAIR], msk[MSIDE], pf, proc);
        TOSTM;

        rxg_ = rxg;
        txl_ = txl;
        rxs_ = rxs;
        memcpy(msk_, msk, sizeof(msk));
        plist++;
    }
    comm[1] = gSTMParam[TX_CLEAN];
    comm[2] = gSTMParam[RX_CLEAN];
    j = 3;
    TOSTM;

    comm[0] = 0;
    BR_STM_send_single(comm);                               /* Table end mark */
    ERRORPRINTFV("%d\n", __LINE__);
}

