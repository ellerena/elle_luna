/**
	@file		main.c
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		STM communication module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#include <stdlib.h>
#include "BR_ft232.h"
#include "BR_gpio.h"
#include "BR_lcd.h"
#include "device/BR_dma.h"
#include "device/BR_ofo.h"
#include "device/BR_timer.h"
#include "device/BR_stm.h"
#include "device/BR_tx.h"
#include "device/BR_msm.h"
#include "device/BR_des.h"
#include "device/BR_dco.h"
#include "device/BR_iic.h"
#include "device/BR_iq.h"
#include "device/BR_int.h"
#include "protocol/profile.h"
#include "protocol/rfdata.h"
#include "protocol/array3D.h"
#include "protocol/txbf.h"
#include "xil_cache.h"

/* external dependencies for macros and definitions */
#include "sona_usb.h"

#ifdef BR_RELEASE
#define FW_VERSION			"1.0.i"
#else
#define FW_VERSION			"1.0_i"
#endif

uint32_t gGlobals[] = {
        (uint32_t)&gTX_Scan_Parameters,     /* 0 */
        (uint32_t)&gTxBfMask,               /* 1 */
        (uint32_t)&gTX_Trigger,             /* 2 */
        (uint32_t)&gFlags,                  /* 3 */
        (uint32_t)gSTMParam,                /* 4 */
        (uint32_t)&gDDR3HeapUsed,           /* 5 */
        (uint32_t)gRfSubBlocks,             /* 6 -- in ddram -- */
        (uint32_t)gFT_RX_buffer,            /* 7 */
        (uint32_t)gFT_TX_buffer,            /* 8 */
        (uint32_t)gTxBfStripes,             /* 9 */
        (uint32_t)&gBfStore,                /* 10 */
        (uint32_t)gTxBfTable,               /* 11 */
};


// <xx xx [ONELINE_1][FULL_1][b][TLINE_5] [RSIDE_1][RPOS_3][TSIDE_1][TPOS_3]>
#define	BLK_RPOS(x)		(((x) >> 4) & 0x7)
#define	BLK_RSIDE(x)	(((x) >> 7) & 0x1)
#define	BLK_TPOS(x)		((x) & 0x7)
#define	BLK_TSIDE(x)	(((x) >> 3) & 0x1)
#define	BLK_TCHAN(x)	(((x) >> 8) & 0x1f)
#define	BLK_FULL(x)		((x) & 0x4000)
#define	BLK_ONELINE(x)	((x) & 0x8000)
#define BLK_TSHIFT(x)	((BLK_TPOS(x) << 5) + BLK_TCHAN(x))

#define	PRINTVER(x,y)		PRNF(x "%d.%d.%d.%d\n", (y>>24), 0xff&(y>>16), 0xff&(y>>8), 0xff&y)

#define	VERSIZE			((8 + 1 + 1 + 1)*sizeof(int)) /* regs + timer + error flags + gGlobals */

void BR_init_platform		(void);
int BR_Get_Ver				(void);
void BR_cleanup_platform	(void);
int BR_DES_Calibrate		(void);
int BR_GetRFData			(void);
int BR_GetRFDataBF			(void);
int BR_GetRFData_4x			(void);
int BR_Read_Register		(u32 *reg, u32 len);
int write_registers			(u32 *reg, u32 len);
int BR_Position				(void *args, u32 len);
int BR_Get_Stripe       	(void *args, u32 len);
int BR_Get_RF_Samples		(void *args, u32 len);
int Test_Function			(void *args, u32 len);
int BR_Json_load			(void *args, u32 len);
int BR_Config_Multi         (void *args, u32 len);
int BR_GetRFBFData          (void);

typedef int (*BR_pargfn_t)( void* , u32);
typedef void (*BR_pvoidfn_t)( void );
typedef int (*BR_pvoidfbfn_t)( void );

u8 loc_command[4];
u32 gLastTimer;

BR_pvoidfn_t BR_fnptr[] = {
			(BR_pvoidfn_t) Test_Function,               /* 0 */
			(BR_pvoidfn_t) BR_GetRFData,                /* 1 */
			(BR_pvoidfn_t) BR_Get_Stripe,               /* 2 */
			(BR_pvoidfn_t) BR_Position,                 /* 3 */
			(BR_pvoidfn_t) BR_GetRFBFData,	            /* 4 */
			(BR_pvoidfn_t) BR_Json_load,                /* 5 */
			(BR_pvoidfn_t) BR_STM_send_command,         /* 6 */
			(BR_pvoidfn_t) BR_STM_Read_Register,        /* 7 */
			(BR_pvoidfn_t) BR_DES_Calibrate,            /* 8 */
			(BR_pvoidfn_t) BR_TX_Burst_Length,          /* 9 */
			(BR_pvoidfn_t) BR_GetRFData_4x,             /* 10 - variable */
			(BR_pvoidfn_t) BR_MSM_Arm,                  /* 11 */
			(BR_pvoidfn_t) BR_DCO_Calibrate,            /* 12 */
			(BR_pvoidfn_t) BR_TX_fill_lut,              /* 13 */
			(BR_pvoidfn_t) BR_Get_Ver,                  /* 14 */
			(BR_pvoidfn_t) BR_Read_Register,            /* 15 */
			(BR_pvoidfn_t) write_registers,             /* 16 */
			(BR_pvoidfn_t) BR_Get_RF_Samples,           /* 17 */
			(BR_pvoidfn_t) BR_Config_Multi,             /* 18 */
			(BR_pvoidfn_t) NULL                         /* 19 */
	};

static void user_timer_init(void)
{
    u32 * ptemp;

    ptemp = (u32*)TIM2_BASE_ADDR;
    WR_OFF32(TIM2_TLR0_OFFSET, 0);            /* set reset value */
    WR_OFF32(TIM2_TCSR0_OFFSET, 0);           /* reset and stop the timer */
}

int BR_Read_Register(u32 *reg, u32 len)
{
	u32 lenbytes, *dst;
	u32 flag = 0;

	dst = (u32*)gFT_TX_buffer;
	reg++;				/* skip 1st value which is profile name */

	switch (*reg++)
	{

		case DCO_PROG:
			reg = (u32*)REG_DCO_PROG_START;
			len = DCO_PROG_LEN;
			break;

		case DCO_CAL:
			reg = (u32*)REG_DCO_CAL_START;
			len = DCO_PROG_LEN;
			break;

		case DIG_PROG:
			reg = dig_buffer;
			len = IQ_DIG_BUF_WORDS;
			break;

        case TX_TRIG:
        	reg = &gTX_Trigger;
        	len = 1;
            break;

		case USER_TST:
		{
		    switch (*reg)
		    {
		        case 0:
		            reg = (u32*)REG_IQ_BP_FIR_COEF_x;
		            len = IQ_BP_COE_LEN_W;
		            break;

		        case 1:
		            reg = (u32*)REG_IQ_LP_FIR_COEF_x;
		            len = IQ_BP_COE_LEN_W;
		            break;

		        case 2:					/* USB peek data */
		        	reg++;
		        	len = (*(reg+1)) >> 2;
		        	flag = 1;
		        	reg = (u32*)*reg;
		        	break;

		        default:
		            len = 0;
		            break;
		    }
		    break;
		}

		default:
			BR_FT_Send_Tag(ERROR_INVALID_REQ, 0, 0);						/* transmit TAG header to client */
			return -1;
	}

	lenbytes = (4 * len);

	while(len--)
	{
		if(!flag) PRNF("%d %08x x%08x\n", len, reg, *reg);
		*dst++ = *reg++;
	}

	BR_FT_Send_Tag(CMD_RFDATA_ATTRIB, 0, lenbytes);						/* transmit TAG header to client */
	BR_FT_Send_Data((FT_WR_TYPE*) gFT_TX_buffer, lenbytes);

	return 0;
}

void BR_init_platform(void)
{
	char text[20];

#if (XPAR_MICROBLAZE_USE_ICACHE)
    Xil_ICacheEnable();
#endif
#if ( XPAR_MICROBLAZE_USE_DCACHE)
    Xil_DCacheEnable();
#endif

    PRNF("\r\nStarting Physical Layer...\r\n");
    PRNF("SDK:%s\n", stringify(SDKVER));
	BR_TIMER_init();									/* Initialize Timers */
	BR_GPIO_init();		            					/* Initialize GPIO connections for LCD, LED, PB and DIP switches */
	BR_LCD_init();										/* Initialize the LCD display module */
    BR_IIC_initialize();                                /* Initialize i2c master controller */
	BR_Intc_Initialize();								// commented out: we don't use the interrupt controller for now
    BR_STM_initialize();								/* Initialize the STM32 UART and set Ultrasound mode */
    BR_STM_initParams();                            	/* Initialize STM running parameters */
	text[0] = POL_STM_VER_BYTE3;
	text[0] = BR_STM_Read_Register((u8*)&text[0], 0);
	text[1] = POL_STM_VER_BYTE2;
	text[1] = BR_STM_Read_Register((u8*)&text[1], 0);
	text[2] = POL_STM_VER_BYTE1;
	text[2] = BR_STM_Read_Register((u8*)&text[2], 0);
	text[3] = POL_STM_VER_BYTE0;
	text[3] = BR_STM_Read_Register((u8*)&text[3], 0);

	PRNF("STM Ready %d.%d.%d.%d\n", text[0], text[1], text[2], text[3]);
    BR_OUT32((REG_BIG_SW_RST), 1);                      /* Main Software Reset of the VC707 Modules */
    while(BR_IN32(REG_BIG_SW_RST)&1);
	PRNF("Starting State Machines, Waiting for source clock...");
#ifndef STM_DUMMY_BACK_END
	while(!(BR_IN32(REG_BIG_CLK_STATUS)&0x1));
    BR_OUT32(REG_BIG_CLK_SELECT, 1);                    /* Select CSM clock as source */
#else

#endif
    PRNF("%02x:%02x\n", BR_IN32(REG_BIG_CLK_SELECT),
			BR_IN32(REG_BIG_CLK_STATUS));				/* Show clock source and status */
	BR_OUT32(REG_UBLAZE_CONTROL, 0);					/* LED_mux_sel: show clock status on 2 upper leds */
	BR_TIMER_delay_ms(500);

	BR_FT_Initialize();									/* Initialize FT232 USB module */
	PRINTVER("FTD_VER:", BR_IN32(REG_FT_VERSION));
#ifdef BR_USE_DMA
	BR_DMA_Initialize();								/* Initialize the CDMA module */
	PRNF("DMA Initialized\n");
#endif
	BR_OFO_Initialize(PAG_B_PATTERN_OFF);				/* Initialize the output fifo and pattern generator */
	PRINTVER("OFO_VER:", BR_IN32(REG_OFO_VERSION));
	PRINTVER("PAG_VER:", BR_IN32(REG_PAT_VERSION));
	BR_DES_Initialize();								/* initialize ser/des */
	PRINTVER("DES_VER:", BR_IN32(REG_BIG_DES_VER));
	BR_DCO_Initialize();								/* initialize DCO module */
	PRINTVER("DCO_VER:", BR_IN32(REG_BIG_DCO_VER));
	PRNF("IQ INIT:%d\n", BR_IQ_Initialize());         /* Initialize IQ module */
    BR_TX_Initialize();                                 /* Initialize TXT module */
	PRINTVER("TXT_VER:", BR_IN32(REG_BIG_TXT_VER));
	PRNF("MSM INIT:%d\n", BR_MSM_Initialize());		/* Initialize master state machine */
	PRINTVER("BR__VER:", BR_IN32(REG_BIG_VERSION));
	PRINTVER("REG_VER:", BR_IN32(REG_BIG_REG_VER));

	PRNF("\nGlobals\t[%08x] size:%d\n", (u32)gGlobals, sizeof(gGlobals)>>2);

	PRNF("FW:" FW_VERSION "\r\nb:" __DATE__ " " __TIME__ "\r\n\n");
	sprintf(text, "F:" FW_VERSION " %08lx", BR_IN32(REG_BIG_VERSION));
	BR_LCD_goto(0,0);
	BR_LCD_puts(text);
	txbf_initialize();
	initDDR3(0);
	user_timer_init();                                  /* customize timer 1 for this app */
}

int BR_Get_Ver(void)
{
    uint32_t i, *src, *dst;

    dst = (uint32_t*)gFT_TX_buffer;
    src = (uint32_t*)REG_BIG_VERSION;
    for(i = 0; i < 8; i++)                                  /* transmit device versions */
    {
        *dst++ = *src++;
    }
    *dst++ = gLastTimer;                                    /* transmit last timer */
    *dst++ = *(uint32_t*)REG_BIG_ERR_STATUS;                /* transmit error status register */
    *dst++ = (uint32_t)gGlobals;                            /* transmit gGlobal array address */
	BR_FT_Send_Tag(CMD_RESPONSE, TAG_F_ALL_OFF, VERSIZE);   /* send tag for VERSIZE words to follow */
	BR_FT_Send_Data((FT_WR_TYPE*)gFT_TX_buffer, VERSIZE);
	return 0;
}

void BR_cleanup_platform(void)
{
    Xil_DCacheDisable();
    Xil_ICacheDisable();
}

static void BR_Tag_Processor(void)
{
	TAG_NUM_TYPE	tag_num;
	TAG_FLG_TYPE	tag_flag;
	TAG_LEN_TYPE	tag_len;
	u8 * tag;

	BR_FT_Fetch_Bytes(TAG_HEADER_SIZE);
	tag = gFT_RX_buffer;
	tag_num = *(TAG_NUM_TYPE*) (tag + TAG_NUM_OFFSET);			/* retrieve TAG number */
	tag_flag = *(TAG_FLG_TYPE*) (tag + TAG_FLG_OFFSET);			/* retrieve TAG flags */
	tag_len = *(TAG_LEN_TYPE*) (tag + TAG_LEN_OFFSET);			/* retrieve TAG len */

	DBGPRINTF("[i%02x-n%02x-f%04x-l%d] - Ft:%08x\n", *tag, tag_num, tag_flag, tag_len, BR_IN32(REG_FT_STATUS));
	if(*tag != CMD_BIGRED)                                      /* invalid command received */
	{
		PRNF("[i%02x-n%02x-f%04x-l%d] - Invalid - Ft:%08x\n", *tag, tag_num, tag_flag, tag_len, BR_IN32(REG_FT_STATUS));
		/* TODO: clean FTDI pipes */
		return;
	}

    BR_OUT32(REG_TIM2_TCSR0, XTC_CSR_ENABLE_TMR_MASK); 	   		/* start timer */

    tag = FT_TX_buffer;
	if (!tag_len)												/* command only (argument-less functions) */
	{
	    if(tag_flag & TAG_F_FB_REQST)
	    {
	        *(int*)tag = ((BR_pvoidfbfn_t)BR_fnptr[tag_num])(); /* execute "int fn()" type function */
	        tag_len = sizeof(int);
	        tag += tag_len;
	    }
	    else
	    {
	        BR_fnptr[tag_num]();                                /* execute "fn()" type function */
	    }
	}
	else														/* data field is present */
	{
		BR_FT_Fetch_Bytes(tag_len);								/* move TAG data from FIFO to ram */
		*(int*)tag = ((BR_pargfn_t)BR_fnptr[tag_num])((void*)gFT_RX_buffer, tag_len);	/* execute argument type function */
        tag_len = sizeof(int);
		tag += tag_len;
	}

	gLastTimer = BR_IN32(REG_TIM2_TCR0);
	BR_OUT32(REG_TIM2_TCSR0, XTC_CSR_LOAD_MASK);    	        /* stop timer and reset count */

    if(tag_flag & TAG_F_FB_REQST)
    {
        BR_FT_Send_Tag(CMD_RESPONSE, TAG_F_ALL_OFF, tag_len);
        if(tag_len)
        {
            BR_FT_Send_Data((FT_WR_TYPE*)FT_TX_buffer, tag_len);
        }
    }
}

static void BR_PB_Processor(u32 pb)
{
	LED_WR_DATA32(LED_ALL_OFF);
	PRNF("FT.St:%08x DIPS: %08x PB:%08x\n", BR_IN32(REG_FT_STATUS), DIPS_RD_DATA32, pb);

	switch(pb)
	{
		case PB_CENTER:							/* Genera; debugging tasks */
        {
            pb = DIPS_RD_DATA32;
            switch(pb)
            {
                case 0:                                                     /* perform DES calibration */
                {
                    BR_DES_Calibrate();
                    break;
                }
                case 1:                                                     /* perform MSM acquisition */
                {
                    SET_TXT_MASK(TXT_MSK_ALL_ON);
                    BR_MSM_Start_Acquisition(MSM_ACQ);
                    PRNF("Acquired\n");
                    break;
                }
                case 2:                                                     /* STM: setup US mode */
                {
                    BR_STM_send_command(STM_comm + STM_COM_POW_MODE3, 6);
                    PRNF("STM Initialized\n");
                    break;
                }
                case 3:                                                     /* switch clock source */
                {
                    BR_OUT32(REG_BIG_CLK_SELECT, BR_IN32(REG_BIG_CLK_SELECT)^1);
                    PRNF("Clk#%d St:%x\n\r", BR_IN32(REG_BIG_CLK_SELECT), BR_IN32(REG_BIG_CLK_STATUS));
                    break;
                }
                case 4:                                                     /* TX LUT filling */
                {
                    u32 i;
                    for(i = TXT_LUT_START_OFFSET; i<TXT_LUT_END_OFFSET; i+=16)                              /* fill burst LUT with default pattern */
                    {
                        BR_OUT32(TXT_BASE_ADDR+i, TXT_MSK_ALL_ON);
                        BR_OUT32(TXT_BASE_ADDR+i+4, TXT_MSK_ALL_ON);
                        BR_OUT32(TXT_BASE_ADDR+i+8, TXT_MSK_ALL_OFF);
                        BR_OUT32(TXT_BASE_ADDR+i+12, TXT_MSK_ALL_OFF);
                    }
                    break;
                }
                case 5:                                                     /* select external clock */
                {
                    pb = BR_IN32(REG_BIG_CLK_SELECT) ^ 1;
                    BR_OUT32(REG_BIG_CLK_SELECT, pb);                       /* Select CSM clock as source */
                    while(!BR_IN32(REG_BIG_CLK_STATUS));
                    PRNF("Clock Select %d\n", pb);
                    break;
                }
                case 6:                                                     /* FT232H USB receive test */
                {
                    u32 tmp;
                    u8 *ptemp = (u8*)gFT_RX_buffer;
                    PRNF("Waiting for USB data...");
                    do
                    {
                        tmp = (FT_M_RX_COUNT & BR_IN32(REG_FT_STATUS));
                    } while (!tmp);                                         /* if data received via USB... */
                    pb = BR_FT_PickUp_Data();
                    PRNF("%d bytes in queue", pb);
                    tmp = 0;
                    while(pb--)
                    {
                        if(0==tmp%32) PRNF("\n");
                        PRNF("%02x ", *ptemp++);
                        tmp++;
                    }
                    PRNF("\n");
                    break;
                }
                case 7:                                                     /* FIR/IQ Test */
                {
//                    u32 dummy[IQ_BP_COE_LEN + IQ_BP_COE_LEN];
//
//                    BR_IQ_COE_Update(NULL);
//                    BR_IQ_COE_read(dummy);
//                    PRNF("COEFF:\n");
//                    for(pb = 0; pb < (IQ_BP_COE_LEN + IQ_BP_COE_LEN); pb++)
//                        PRNF("%08x ", dummy[pb]);
//                    PRNF("\n");
                    break;
                }
                default:                                                    /* test TX module */
                {
                    SET_TXT_MASK(TXT_MSK_ALL_ON);
                    BR_OUT32(TXT_BASE_ADDR+TXT_START_BURST_OFFSET, TXT_START_BURST_START);
                    BR_TIMER_delay_us(1);
                    BR_OUT32(TXT_BASE_ADDR+TXT_START_BURST_OFFSET, 0);
                    PRNF("TX started\n");
                    break;
                }
            }
            break;
        }
		case PB_SOUTH:							/* DMA test: enables a pattern from PAG and transmits it using DMA */
        {
            pb = DIPS_RD_DATA32;                /* dip switches: [7..2] determine # of bytes to transfers, [1,0]: PAG pattern # */
            BR_OFO_Initialize(pb & 0x3);        /* enable the test pattern requested */
            BR_FT_Send_from_OFO(pb*4);
            BR_OFO_Initialize(PAG_B_PATTERN_OFF);
            PRNF("Test pattern %d, USB sent %d bytes, DMA st:%08x\n", \
                    pb&0x3, pb*4, BR_IN32(REG_DMA_SR));
            break;
        }
		case PB_NORTH:
		case PB_EAST:
		case PB_WEST:
		default:
			break;
	}

	for (pb = 0; pb < 500; pb++)				/* de-bounce: wait for button release */
	    if(PUSH_RD_DATA32) pb = 0;
}

int BR_Position(void *args, u32 len)
{
	u32 y, block, pos;
	u8 gSTM_TX_Buffer[7];
	u8 *pSTM = gSTM_TX_Buffer;

	block = *(u32*)args;

	pos = BLK_RSIDE(block);										/* read the desired RX side */
	y = BLK_FULL(block) ?
			STM_COM_RX_ARM_PAIR : (STM_COM_RX_ARM_EVEN + pos);	/* scan RX by pair or single line */
	*pSTM++ = STM_comm[y]; 										/* STM: enable desired side on RX grp A */

	pos = (BLK_RPOS(block) - RX_B);								/* read block Rx position */
	if(pos) *pSTM++ = STM_comm[STM_COM_RX_SHIFT_1+pos];			/* STM: shift to desired RX grp */

	pos = BLK_TSIDE(block);										/* read the desired TX side */
	y = BLK_FULL(block) ?
			STM_COM_TX_ARM_PAIR : (STM_COM_TX_ARM_EVEN + pos);	/* scan TX by pair or single line */
	*pSTM++ = STM_comm[y];										/* STM: enable desired TX Grp A 1st line */

	pos = (((BLK_TPOS(block) - TX_C)*32) + BLK_TCHAN(block));	/* read Tx line position */
	if(pos)														/* STM: shift to desired TX grp */
	{
		*pSTM++ = POL_COM_STM_REG_WRITE;
		*pSTM++ = POL_WR_TX_SHIFT;
		*pSTM++ = pos;
	}

	return BR_STM_send_command(gSTM_TX_Buffer, (u32)(pSTM - gSTM_TX_Buffer));
}

int BR_DES_Calibrate(void)
{		/**** runs DES calibration and then transmits 1 byte from the argument to the STM ****/
    int rc;
    int loops = DES_LOOP_MAX;

    do
    {
       BR_STM_send_command(STM_comm+STM_COM_US_TEST_3, 1);    /* STM: Set AD9272 to A33 Pattern */
       if (BR_IN32(XPAR_M07_AXI_BASEADDR + 4)&0x1)            /* if clock is good... */
       {
           rc = BR_DES_Start_Cal(DES_B_SERDES_RISING_EDGE);   /* DES: run calibration */
           if (!rc)                                           /* DES calibration Ok */
           {
               break;
           }
       }
       else
       {
           rc = -3;
       }
       PRNF("DES:%08x %d %d\n", BR_IN32(REG_DES_STATUS), loops, rc); /* expected 0xbc3c3c3c */
    } while(--loops);

    BR_STM_send_command(STM_comm+STM_COM_US_TEST_0, 1);       /* STM: Remove AD9272 test pattern */

    return rc;
}

int BR_GetRFData_4x(void)
{
	u32 y, t, r, T_side, R_side, data, stripe, line;
	u32 x_start, y_start, x_end, y_end;
	u32 RxGroup = 0;
	u32 mask = gTxBfMask;

	x_start = RX_B;									/* for now we just scan 4 blocks */
	x_end = RX_C;
	y_start = TX_C;
	y_end = TX_D;

    MSM_RANGE_RESET;                                /* resets the min/max range engine */
    MSM_RANGE_CAPTURE;                              /* remove reset and start min/max range machine */

    stripe = MSM_SAMPLE_WDW * RF_BLK_RX * RF_BLK_BPS;
    if(BR_IQ_MODE_IS_ACTIVE) stripe *= 2;

    BR_STM_send_command(STM_comm+STM_COM_STM_SCAN_ARM, 2);  /* reset STM store pointer and run 1st step */

	R_side = 1;
	do
	{
		T_side = 1;
		do
		{
			r = x_start;
			while(1)										/* process each RX groups */
			{
			    BR_IQ_DIG_use(RxGroup);
				t = y_start;
				line = T_side;
				do
				{
					data = ((R_side<<7) | (r<<4) | (T_side <<3) | t);
					BR_FT_Send_Tag(CMD_RFDATA_BLOCK, data, stripe * RF_BLK_TX);	/* transmit TAG header to client */

					y = 0;
					do										/* transmit 32 TX (whole block) :: 4*16*128*32 = 256kB */
					{
                        BR_TX_Upload_BF_Delays(line);
						SET_TXT_MASK(mask);                 /* TXT: set mask channel */
						BR_MSM_Start_Acquisition( (gTX_Trigger == line) ? MSM_ACQ_WITH_TRIGGER : MSM_ACQ);   /* perform data acquisition */
                        line += 2;
                        BR_STM_Repeat_Last_Command();		/* request STM to position Rx/Tx */
						BR_FT_Send_from_OFO(stripe);		/*send 1 TX grp: e.g. 128 samples x 32RX x 2 bytes = 8192 bytes */
						REVOLVE_MASK(mask);                 /* TXT: shift mask */
					} while (++y < 32);
				} while (++t <= y_end);						/* loop through all desired TX groups */

				if (++r > x_end) break;
				else RxGroup++;
			}
			RxGroup-= (x_end-x_start);
		} while (T_side--);									/* loop twice (once for each T_side) */
		RxGroup = (x_end-x_start+1);
	} while (R_side--);

	return MSM_RANGE_VALUES;
}

int BR_GetRFBFData(void)
{
    u32 data, strip_sz, RxGroup = 0;
    u32 mask = gTxBfMask;
    const BF_TXENTRY_T* pl = gBfStore.playList;
    uint32_t plCount;

    strip_sz = MSM_SAMPLE_WDW * RF_BLK_RX * RF_BLK_BPS;
    if(BR_IQ_MODE_IS_ACTIVE) strip_sz *= 2;

    MSM_RANGE_RESET;                                        /* resets the min/max range engine */
    MSM_RANGE_CAPTURE;                                      /* remove reset and start min/max range machine */
    BR_STM_send_command(STM_comm+STM_COM_STM_SCAN_ARM, 2);  /* reset STM store pointer and run 1st step */

    RxGroup = 255;											/* force to run once */
    plCount = gBfStore.playListIndex;
	while(plCount--)                                        /* process each RX groups */
	{
        if (RxGroup != pl->groupID[0])                      /* check if RX group change */
        {
            RxGroup = pl->groupID[0];
            BR_IQ_DIG_use(RxGroup);                         /* load dig-gain values */
        }

        data = (RxGroup | (pl->dispY[0] << 4));
		BR_FT_Send_Tag(CMD_RFDATA_STRIP, data, strip_sz);   /* transmit TAG header to client */

		BR_TX_Upload_BF_DelaysCF(pl->delayCoarse, pl->delayFine);
		SET_TXT_MASK(mask);                                 /* TXT: set mask channel */
		BR_MSM_Start_Acquisition( (gTX_Trigger == pl->dispY[0]) ? MSM_ACQ_WITH_TRIGGER : MSM_ACQ);   /* perform data acquisition */

		BR_STM_Repeat_Last_Command();       /* request STM to position Rx/Tx */
		BR_FT_Send_from_OFO(strip_sz);      /*send 1 TX grp: samples x 32RX x 2 bytes */
		REVOLVE_MASK(mask);                 /* TXT: shift mask */

		pl++;
	}

    return MSM_RANGE_VALUES;
}

int BR_GetRFData(void)
{
	u32 y, t, r, stripe;
	u32 x_start, y_start, x_end, y_end;
//	u32 mask;

	x_start = RX_B;									/* for now we just scan 4 blocks */
	x_end = RX_C;
	y_start = TX_C;
	y_end = TX_D;

    MSM_RANGE_RESET;                                /* resets the min/max range engine */
    MSM_RANGE_CAPTURE;                              /* remove reset and start min/max range machine */

    stripe = MSM_SAMPLE_WDW * RF_BLK_RX * RF_BLK_BPS;

	BR_STM_send_command(STM_comm+STM_COM_RX_ARM_PAIR, 1);				/* STM: select desired RX Group (32) */
	r = x_start;
	while(1)															/* process each RX groups */
	{
		BR_STM_send_command(STM_comm+STM_COM_TX_ARM_PAIR, 1);
		t = y_start;
		do
		{
			SET_TXT_MASK(TXT_MSK_ALL_ON);								/* TXT: set mask channel */
			BR_FT_Send_Tag(CMD_RFDATA_BLOCK, (r<<4 | t), stripe * RF_BLK_TX);	/* transmit TAG header to client */
			y = 0;
			do															/* transmit 32 TX (whole block) :: 4*16*128*32 = 256kB */
			{
				BR_MSM_Start_Acquisition(MSM_ACQ);	            		/* perform data acquisition */
                if(y) BR_STM_Repeat_Last_Command();
                else BR_STM_send_command(STM_comm+STM_COM_TX_SHIFT_1, 1);
				BR_FT_Send_from_OFO(stripe);							/*send data of 1 tx: e.g. 128 samples x 32RX x 2 bytes = 8192 bytes */
//				SET_TXT_MASK(mask<<=1);									/* TXT: shift mask */
			} while(++y < 32);
		} while (++t <= y_end);

		t = TXEND - y_end;
		if(t > y_start)
		{
			BR_STM_send_command(STM_comm+STM_COM_TX_SHIFT_1+t, 1);		/* STM: shift to desired TX grp */
		}

		if (++r > x_end) break;
		else BR_STM_send_command(STM_comm+STM_COM_RX_SHIFT_32, 1);
	}
    BR_STM_send_command(STM_comm+STM_COM_RX_INIT, 1);                   /* disable all RX */

    return MSM_RANGE_VALUES;
}

/**
 * @brief   Get 1 single block (32 x 32 x samples) of sampled data
 * @param   args, pointer to channel identification parameter:
 *          <4:Tx channel><1:Rx side><3:Rx block><1:Tx side><3:Tx block (LSB)>
 * @param   len: unused
 * @return  MIN_MAX_VALUE register content
 * */
int BR_Get_RF_Samples(void * args, u32 len)
{
	u32 y, block_def, stripe, pos;

	BR_IQ_CTRL_MODE(IQ_MODE_INI);                                       /* Force RF - TEST ONLY */
    MSM_RANGE_RESET;                                                    /* resets the min/max range engine */
    MSM_RANGE_CAPTURE;                                                  /* remove reset and start min/max range machine */
//	BR_OFO_Initialize(PAG_B_PATTERN_B);                                 /* TODO: TEST ONLY */

    block_def = *(u16*)args;											/* read block location */
	pos = BLK_RSIDE(block_def);											/* read the desired RX side */
	y = BLK_FULL(block_def) ?
	        STM_COM_RX_ENA_GRP_A:(STM_COM_RX_ENA_A_EVEN + pos);			/* scan RX by pair or single line */
	BR_STM_send_single(STM_comm + y); 									/* STM: enable desired side on RX grp A */

	pos = BLK_RPOS(block_def);											/* read block Rx position */
	if(pos) BR_STM_send_single(STM_comm+STM_COM_RX_SHIFT_1+pos);		/* STM: shift to desired RX grp */

	pos = BLK_TSIDE(block_def);											/* read the desired TX side */
	y = BLK_FULL(block_def)?STM_COM_TX_SEL_1:(STM_COM_TX_ENA_1 + pos);	/* scan TX by pair or single line */
	BR_STM_send_single(STM_comm + y);									/* STM: enable desired TX Grp A 1st line */

	pos = BLK_TPOS(block_def);											/* read block Tx position */
	if(pos) BR_STM_send_single(STM_comm+STM_COM_TX_SHIFT_1+pos);		/* STM: shift to desired TX grp */

	SET_TXT_MASK(TXT_MSK_ALL_ON);										/* TXT: set mask channel */
	stripe = MSM_SAMPLE_WDW * RF_BLK_RX * RF_BLK_BPS;					/* each slice transferred is 1 TX row of samples (128x32x2 = 8192) */
	y = stripe * 32 ;													/* we'll transmit 32 slices to complete 1 block */
	BR_FT_Send_Tag(CMD_RFDATA_BLOCK, block_def, y);						/* transmit TAG header to client */

	y = 0;
	do												/* transmit remaining 31 TX (whole block) :: 128x32x2x31 = 248kB */
	{
		BR_MSM_Start_Acquisition(MSM_ACQ);	            				/* perform data acquisition */
        if(y) BR_STM_Repeat_Last_Command();
        else BR_STM_send_single(STM_comm+STM_COM_TX_SHIFT_1);
		BR_FT_Send_from_OFO(stripe);									/*send 1 TX grp: e.g. 128 samples x 32RX x 2 bytes = 8192 bytes */
	} while (++y < 32);

	y = TXEND - pos;
	if(y) BR_STM_send_single(STM_comm+STM_COM_TX_SHIFT_1+y);			/* STM: shift to desired TX grp */

	BR_STM_send_single(STM_comm+STM_COM_RX_INIT);						/* STM: Reset/Initialize RX */

//	BR_OFO_Initialize(PAG_B_PATTERN_OFF); /* TEST ONLY */
    return MSM_RANGE_VALUES;
}

#define F_NODLY    (1 << 31)        /* don't load BF delays */
#define F_NOSMA    (1 << 30)        /* don't trigger SMA port */

int BR_Get_Stripe(void * args, u32 len)
{
    u32 stripe;
    BF_TXENTRY_T* pl;
    STRIPE_HEADER_T sh = {0, 0, 0, 0};

	BR_IQ_CTRL_MODE(IQ_MODE_INI);                           /* Force RF - TEST ONLY */
    MSM_RANGE_RESET;                                        /* resets the min/max range engine */
    MSM_RANGE_CAPTURE;                                      /* remove reset and start min/max range machine */

    len = *(u32*)args;										/* get desired delay <x:16><fine:8><coarse:8LSB> */
    pl = &gBfStore.playList[len&0xfff];                     /* select playlist (up to 8*384=3072 */

    stripe = (MSM_SAMPLE_WDW * RF_BLK_RX * RF_BLK_BPS);     /* each stripe is (samples x 32rx x 2bytes) */
    sh.length = stripe; sh.pad = MSM_SAMPLE_WDW;
    sh.dispX = pl->groupID[0]; sh.dispY = pl->dispY[0];

    BR_FT_Send_Tag(CMD_RFDATA_STRIP, len, stripe + sizeof(sh) + 4); /* transmit TAG header */
    BR_FT_Send_Data((FT_WR_TYPE*)&sh, sizeof(STRIPE_HEADER_T));     /* transmit sh header */

    BR_IQ_DIG_use(pl->groupID[0]);                          /* load digital gain values */
    if (!(len & F_NODLY))
    {
        if(gFlags & GST_BFACTIVE)
        {
            BR_TX_Upload_BF_DelaysCF(pl->delayCoarse, pl->delayFine);   /* load BF delays */
        }
        else
        {
            BR_TX_Upload_BF_Delays(len&0xff);
        }

    }
    SET_TXT_MASK(TXT_MSK_ALL_ON); 			  				/* TXT: set mask channel */
    BR_MSM_Start_Acquisition((len & F_NOSMA) ?
			MSM_ACQ : MSM_ACQ_WITH_TRIGGER);				/* perform data acquisition */
	BR_FT_Send_from_OFO(stripe);							/* transmit stripe data */
	BR_FT_Send_Data((FT_WR_TYPE*)REG_MSM_RANGE_VALUE, 4);   /* transmit min/max values */

	BR_TX_Dbg_BF_Ram_Dlys((u32*)gFT_TX_buffer);             /* capture delays */

	return REG_MSM_RANGE_VALUE;
}

int Test_Function(void * args, u32 len)
{
    u32 plCount, RxGroup = 0, mask = gTxBfMask;
    u32 *dbgdata;
    const BF_TXENTRY_T *pl = gBfStore.playList;

    MSM_RANGE_RESET;                                        /* resets the min/max range engine */
    MSM_RANGE_CAPTURE;                                      /* remove reset and start min/max range machine */
    BR_STM_send_command(STM_comm+STM_COM_STM_SCAN_ARM, 2);  /* reset STM store pointer and run 1st step */

    dbgdata = (u32*)gFT_TX_buffer;
    RxGroup = 255;                                          /* force to run once */
    plCount = gBfStore.playListIndex;
    while(plCount--)                                        /* process each RX groups */
    {
        if (RxGroup != pl->groupID[0])                      /* check if RX group change */
        {
            RxGroup = pl->groupID[0];
            BR_IQ_DIG_use(RxGroup);                         /* load dig-gain values */
        }

        BR_TX_Upload_BF_DelaysCF(pl->delayCoarse, pl->delayFine);
        SET_TXT_MASK(mask);                                 /* TXT: set mask channel */
        BR_MSM_Start_Acquisition(MSM_ACQ_WITH_TRIGGER);     /* perform data acquisition */
        BR_OFO_Initialize(PAG_B_PATTERN_OFF);               /* reset OFO */

        *dbgdata++ = mask;
        dbgdata += BR_TX_Dbg_BF_Ram_Dlys(dbgdata);          /* capture delays */

        BR_STM_Repeat_Last_Command();       /* request STM to position Rx/Tx */
        REVOLVE_MASK(mask);                 /* TXT: shift mask */

        pl++;
    }

    return ((u32)dbgdata - (u32)gFT_TX_buffer);
}

int BR_Json_load(void *args, u32 len)
{
    int rc;
    char errorbuf[64];

    errorbuf[0] = 0;
    gBfStore.status &= ~(STATUS_RECALC | STATUS_BFVALID);  //mark not valid until JSON decoded

    rc = decode_dtd_ex(args, &gBfStore, len, errorbuf);

    if (!rc)				//successful decode
    {
        ERRORPRINTFV("BF Version:%s\n", gBfStore.version);
        gBfStore.status |= (STATUS_RECALC | STATUS_BFVALID);
        rc = gBfStore.playListIndex;
    }
    else
    {
        ERRORPRINTFV("TX BeamForm Errors, argsPtr%p, size:%d, err:%s\n", args, len, errorbuf);
    }

    return rc;
}

/**
 * @brief  Main configurator. Performs up to 32 tasks.
 * @return  system status flags.
 * */
int BR_Config_Multi(void *args, u32 len)
{
    u32 tsk = *(u32*)args;
    int result;

    if(tsk & BR_CFG_DESCAL)
    {
        if((result = BR_DES_Calibrate()) < 0)
            return result;
    }
    if(tsk & BR_CFG_DCOCAL)
    {
        args++; len -= 4;
        if((result = BR_DCO_Calibrate(args, len)) < 0)
            return result;
    }
    if(tsk & BR_CFG_SCAN_TXBF)      /* set TX BF */
    {
        if(!(gBfStore.status & STATUS_BFVALID))
        {
            ERRORPRINTF("Json data missing\n");
            return -1;
        }
        if(gBfStore.status & STATUS_RECALC)
        {
            int freq = 0x80;

            freq = BR_STM_Read_Register((const uint8_t*)&freq, 0);
            freq = (freq * 50000) + 15000000;
            ERRORPRINTFV("TXBF calculated @ %dHz\n", freq);
            txbf_normalize(freq);
            gBfStore.status &= ~STATUS_RECALC;
            BR_STM_store_init();
        }
        if(!(gFlags & GST_BFACTIVE))
        {
            BR_fnptr[FN_BR_GETRFDATA_4X] = (BR_pvoidfn_t)BR_GetRFBFData;
            gFlags |= GST_BFACTIVE;
        }
    }
    if(tsk & BR_CFG_SCAN_LEGACY)    /* set TX legacy scan */
    {
        BR_fnptr[FN_BR_GETRFDATA_4X] = (BR_pvoidfn_t)BR_GetRFData_4x;
        gTX_Scan_Parameters = TX_ARM_INI;
        BR_TX_Config(NULL, TX_CFGBFSTOREINI | TX_CFGBFTABLE | TX_CFGBFLATCH); /* reset TXBF params (disable it) */
    }

    return gFlags;
}

int main (void)
{
	volatile u32 *ptemp;
	u32 tmp;

	BR_init_platform();									/* low level initialization of all FPGA HW */

	while(1)
	{
		ptemp = (u32*)PB_SW_BASE_ADDR;
		tmp = RD_OFF32(PUSH_DATA_OFFSET);
		if(tmp & PB_ALL_MASK)						/* if push button pressed... */
		{
			BR_PB_Processor(tmp);
		}
		else
		{
			ptemp = (u32*)FT_BASE_ADDR;
			tmp = RD_OFF32(FT_STATUS_OFFSET);
			if (FT_M_RX_COUNT & tmp)			/* if data received via USB... */
			{
				BR_Tag_Processor();
			}
		}
	}

	BR_cleanup_platform();
	return 0;
}



