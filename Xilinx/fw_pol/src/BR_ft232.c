/**
	@file		BR_ft232.c
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		FT232H processing module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#include "BR_ft232.h"
#include "device/BR_ofo.h"
#include "device/BR_timer.h"
#ifdef BR_USE_DMA
#include "device/BR_dma.h"
#endif

u8 FT_TX_buffer[FT_TX_BUFFER_SZ] = "My test data";

/**
 * @brief	Initializes the FTDI 232H module and FIFO
 * */
void BR_FT_Initialize(void)
{
	u32 *ptemp;

	ptemp = (u32*)FT_BASE_ADDR;
	WR_OFF32(FT_CONTROL_OFFSET, FT_B_RESET);
	BR_TIMER_delay_us(10);
	WR_OFF32(FT_CONTROL_OFFSET, FT_CONTROL_INIT);
}

/**
 * @brief	When called, picks up data present in the FT232 FIFO, copies it
 * 			into out local buffer gFT_RX_buffer[] and returns the number of bytes
 * 			read.
 *
 * @return	Number of bytes read from the FTDI Fifo.
 */
u32 BR_FT_PickUp_Data(void)
{
	volatile u32 *ptemp;
	u32 temp, datacount;
	u8 * newdata;

	newdata = gFT_RX_buffer;
	datacount = 0;
	ptemp = (u32*)FT_BASE_ADDR;
	temp = (RD_OFF32(FT_STATUS_OFFSET) & FT_M_RX_COUNT);
	while(temp)
	{
		datacount += temp;
		while(temp--)
		{
			BR_OUT8(newdata++, RD_OFF8(FT_READ_RX_OFFSET));
		}
		temp = (RD_OFF32(FT_STATUS_OFFSET) & FT_M_RX_COUNT);
	}

	return datacount;
}

/**
 * @brief	When called, picks up the request amount of bytes in the FT232 FIFO, copies it
 * 			into out local buffer gFT_RX_buffer[] and returns the number of bytes
 * 			read.
 * @param	len is the number of bytes requested to be read, must be less/equal to FT_RX_BLOCK_SZ
 * @return	0: success, otherwise it means error.
 */
int BR_FT_Fetch_Bytes(u32 len)
{
	volatile u32 *ptemp;
	u8 * newdata;

	ptemp = (u32*)FT_BASE_ADDR;
	newdata = gFT_RX_buffer;									/* point to our local ram buffer */
	while(len)												/* count and copy */
	{
		if(RD_OFF32(FT_STATUS_OFFSET) & FT_M_RX_COUNT)
		{
			BR_OUT8(newdata++, RD_OFF8(FT_READ_RX_OFFSET));
			len--;
		}
	}

	return 0;
}

/**
 * @brief	Transmits data via USB to the external application.
 * 			This function can transmit in either 32bit or 8bit format.
 * 	@param	data, pointer to data location.
 * 	@param	len, number of BYTES to transfer, MUST be multiple of 4.
 */
void BR_FT_Send_Data(FT_WR_TYPE* data, u32 len)
{
	volatile u32 *ptemp;

	ptemp = (u32*) FT_BASE_ADDR;

	while(len)
	{
		WR_OFF32(FT_WRITE_TX_OFFSET , *data++);
		len -= FT_WR_SZ;
	}
}

/**
 * 	@brief	Transmits a TAG formatted packet to the client.
 * 			NOTE: Tag_Header must be defined as multiple of 4 in size.
 * 	@param	com: command code (union of category and number)
 * 	@param	flags: processing/description flags
 * 	@param	data_len: size of the data to follow later.
 * */
void BR_FT_Send_Tag(u32 com, u32 flags, u32 data_len)
{
	tTagHeader Tag_Header;

	Tag_Header.command.comm = (TAG_COM_TYPE)com;
	Tag_Header.flags = (TAG_FLG_TYPE)flags;
	Tag_Header.length = data_len;

	BR_FT_Send_Data((FT_WR_TYPE*)&Tag_Header, sizeof(Tag_Header));
}

/**
 * @brief	Transmits a block of data from the OFO, straight to the
 * 			FTDI fifo.
 * @param	len, number of BYTES to transmit. Must be multiple of 4.
 */
void BR_FT_Send_from_OFO(s32 len)
{
#ifdef BR_USE_DMA
		BR_DMA_Start_Transfer(len);
#else
	volatile u32 *src, *dst;

	src = (u32*)(REG_OFO_READ_DATA);
	dst = (u32*)(REG_FT_WRITE_TX);

	while (len > 0)
	{
#ifndef BR_USE_32BIT_WRITE
		u32 temp;

		temp = BR_IN32(src);
		BR_OUT32(dst, temp);
		temp >>= 8;
		BR_OUT32(dst, temp);
		temp >>= 8;
		BR_OUT32(dst, temp);
		temp >>= 8;
		BR_OUT32(dst, temp);
#else
		BR_OUT32(dst, *src);

#endif
		len -= 4;
	}
#endif
}
