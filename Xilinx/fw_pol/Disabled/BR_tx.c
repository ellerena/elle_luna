/*
 * BR_tx.c
 *
 *  Created on: Sep 7, 2016
 */

#include "BR_tx.h"

extern void BR_TX_Initialize()
{
	int i;

	SET_TXT_BURST_LENGTH(TXT_DEFAULT_BURST_LENGTH);
	SET_TXT_MASK(TXT_DEFAULT_MASK);

	for(i = 0; i<TXT_LOOKUP_TABLE_SIZE; i+=4)			/* test pattern for now */
	{
		SET_TXT_LOOKUP_TABLE(i, TXT_MSK_ALL_ON);
		SET_TXT_LOOKUP_TABLE(i+1, TXT_MSK_ALL_ON);
		SET_TXT_LOOKUP_TABLE(i+2, TXT_MSK_ALL_OFF);
		SET_TXT_LOOKUP_TABLE(i+3, TXT_MSK_ALL_OFF);
	}
}


/**
 * @brief	Configures the TXT burst length.
 * @param	burst_length, is a pointer to a 1 byte word where  burst_length = (desired burst length - 1).
 * 			hence burst_length is in [0..255]. Note: currently we support a maximum desired length of 128.
 * */
extern int BR_TX_Burst_Length(u8 * burst_length, u32 len_)
{
	SET_TXT_BURST_LENGTH(*(u32*)burst_length);
	return (int)TXT_RD_REG32(REG_TXT_BURST_LENGTH);
}

extern void BR_TX_fill_lut(u32 *lut)
{
	int i;

	for(i = 0; i<TXT_LOOKUP_TABLE_SIZE; i+=16)			/* test pattern for now */
	{
		SET_TXT_LOOKUP_TABLE(i, *lut++);
		SET_TXT_LOOKUP_TABLE(i+1, *lut++);
		SET_TXT_LOOKUP_TABLE(i+2, *lut++);
		SET_TXT_LOOKUP_TABLE(i+3, *lut++);
	}
}



