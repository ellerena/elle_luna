/*
 * BR_ppa.h
 *
 *  Created on: Nov 29, 2016
 *      Author: Eddie.Llerena
 */

#ifndef BR_PPA_H_
#define BR_PPA_H_

#include "device/BR_ofo.h"

/*================================= bit definitions ==================================*/
extern u32 gpTransmitWindow;

extern int BR_PPA_Configure(u8 * args, u32 len);


#endif /* BR_PPA_H_ */
