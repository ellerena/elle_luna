/*
 * BR_ppa.c
 *
 *  Created on: Nov 29, 2016
 *      Author: Eddie.Llerena
 */

#include "BR_ppa.h"

u32 gpTransmitWindow = REG_MSM_WDW;
u32 gTransmitWindow = 0x10000u;             /* represents a window of starts 0 and length 1 */

int BR_PPA_Configure(u8 * args, u32 len)
{
	if (*args)
	{
		BR_OUT32(REG_PPA_CONTROL, PPA_CONTROL_ENA);			/* Enable PPA */
		gpTransmitWindow = (u32)&gTransmitWindow;
	}
	else
	{
		BR_OUT32(REG_PPA_CONTROL,  PPA_CONTROL_DIS);		/* disable PPA */
		gpTransmitWindow = REG_MSM_WDW;
	}
	return 0;
}
