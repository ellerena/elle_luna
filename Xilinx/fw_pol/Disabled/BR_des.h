/*
 * BR_msm.h
 *
 *  Created on: Sep 11, 2016
 */

#ifndef BR_DES_H_
#define BR_DES_H_

#include "xil_types.h"
#include "xparameters.h"

#define DES_BASE_ADDR					(XPAR_M07_AXI_BASEADDR + 0x2000)		/* De-serializer (des) base address */

/*================================= register offsets =================================*/
#define	DES_CONTROL_OFFSET				(0x0)									/* DES control offset */
#define	DES_STATUS_OFFSET				(0x4)									/* DES status offset */
#define	DES_VERSION_OFFSET				(0x18)

/*=============================== register definitions ===============================*/
#define	REG_DES_CONTROL					(DES_BASE_ADDR + DES_CONTROL_OFFSET)
#define	REG_DES_STATUS					(DES_BASE_ADDR + DES_STATUS_OFFSET)
#define	REG_DES_VERSION					(XPAR_M07_AXI_BASEADDR + DES_VERSION_OFFSET)

/*================================= bit definitions ==================================*/
#define	DES_B_START						(1 << 0)								/* DES start bit */
#define DES_B_NORMAL_MODE				(0 << 1)								/* DES bit clock normal operation mode */
#define	DES_B_RUN_ALL_DELAYS			(1 << 1)								/* DES bit clock test mode */
#define	DES_B_SERDES_RISING_EDGE		(0 << 2)								/* DES serialize/deserializer bit order */
#define	DES_B_SERDES_FALLING_EDGE		(1 << 2)								/* DES serialize/deserializer bit order */

/*====================================== masks  ======================================*/
#define	DES_M_ALIGN_LEVEL_ERROR_0		(1 << 0)								/* error aligning level */
#define	DES_M_ALIGN_EDGE_ERROR_0		(1 << 1)								/* error aligning edge */
#define	DES_M_FRAME_ALIGNED_0			(1 << 2)								/* Frame has been successfully aligned */
#define	DES_M_DCO_ALIGNED_0				(1 << 3)								/* DCO has been successfully aligned */
#define	DES_M_ALIGN_LEVEL_ERROR_1		(1 << 8)								/* error aligning level */
#define	DES_M_ALIGN_EDGE_ERROR_1		(1 << 9)								/* error aligning edge */
#define	DES_M_FRAME_ALIGNED_1			(1 << 10)								/* Frame has been successfully aligned */
#define	DES_M_DCO_ALIGNED_1				(1 << 11)								/* DCO has been successfully aligned */
#define	DES_M_ALIGN_LEVEL_ERROR_2		(1 << 16)								/* error aligning level */
#define	DES_M_ALIGN_EDGE_ERROR_2		(1 << 17)								/* error aligning edge */
#define	DES_M_FRAME_ALIGNED_2			(1 << 18)								/* Frame has been successfully aligned */
#define	DES_M_DCO_ALIGNED_2				(1 << 19)								/* DCO has been successfully aligned */
#define	DES_M_ALIGN_LEVEL_ERROR_3		(1 << 24)								/* error aligning level */
#define	DES_M_ALIGN_EDGE_ERROR_3		(1 << 25)								/* error aligning edge */
#define	DES_M_FRAME_ALIGNED_3			(1 << 26)								/* Frame has been successfully aligned */
#define	DES_M_DCO_ALIGNED_3				(1 << 27)								/* DCO has been successfully aligned */
#define	DES_M_BIT_ALIGN_DONE_0			(1 << 4)								/* Bit Alignment completed */
#define	DES_M_FRAME_ALIGN_DONE_0		(1 << 5)								/* Frame Alignment completed */
#define	DES_M_BIT_ALIGN_DONE_1			(1 << 12)								/* Bit Alignment completed */
#define	DES_M_FRAME_ALIGN_DONE_1		(1 << 13)								/* Frame Alignment completed */
#define	DES_M_BIT_ALIGN_DONE_2			(1 << 20)								/* Bit Alignment completed */
#define	DES_M_FRAME_ALIGN_DONE_2		(1 << 21)								/* Frame Alignment completed */
#define	DES_M_BIT_ALIGN_DONE_3			(1 << 28)								/* Bit Alignment completed */
#define	DES_M_FRAME_ALIGN_DONE_3		(1 << 29)								/* Frame Alignment completed */
#define	DES_M_ALL_ALIGN_DONE			(1 << 31)								/* Calibration process complete */
#define DES_M_ALIGN_EDGE_ERROR			(DES_M_ALIGN_EDGE_ERROR_0 \
										| DES_M_ALIGN_EDGE_ERROR_1 \
										| DES_M_ALIGN_EDGE_ERROR_2 \
										| DES_M_ALIGN_EDGE_ERROR_3)
#define DES_M_ALIGN_LEVEL_ERROR			(DES_M_ALIGN_LEVEL_ERROR_0 \
										| DES_M_ALIGN_LEVEL_ERROR_1 \
										| DES_M_ALIGN_LEVEL_ERROR_2 \
										| DES_M_ALIGN_LEVEL_ERROR_3)
#define DES_M_FRAME_ALIGNED				(DES_M_FRAME_ALIGNED_0 \
										| DES_M_FRAME_ALIGNED_1 \
										| DES_M_FRAME_ALIGNED_2 \
										| DES_M_FRAME_ALIGNED_3)
#define DES_M_DCO_ALIGNED				(DES_M_DCO_ALIGNED_0 \
										| DES_M_DCO_ALIGNED_1 \
										| DES_M_DCO_ALIGNED_2 \
										| DES_M_DCO_ALIGNED_3)
#define	DES_M_ALIGN_DONE				(DES_M_FRAME_ALIGNED \
										| DES_M_DCO_ALIGNED)
#define	DES_M_ALIGN_ERROR				(DES_M_ALIGN_EDGE_ERROR \
										| DES_M_ALIGN_LEVEL_ERROR)

/*====================================== macros ======================================*/
#define	SET_DES_REG(x, y)				(*(u32*)(x) = (y))
#define	GET_DES_REG(x)					(*(u32*)(x))
#define	DES_RESET_VALUE					(0)
#define	DES_RESET						SET_DES_REG(REG_DES_CONTROL, DES_RESET_VALUE)
#define	DES_STATUS						GET_DES_REG(REG_DES_STATUS)
#define	DES_ALIGN_STATUS				(DES_M_ALIGN_DONE & DES_STATUS)
#define	DES_ERROR_STATUS				(DES_M_ALIGN_ERROR & DES_STATUS)

/*==================================== External API ====================================*/
/**
 * 	@brief	Initializes the DES by resetting its logic and setting the default
 * 	 collection depth value
 * */
extern void BR_DES_Initialize(void);

/**
 * 	@brief	Starts the ser/des calibration by resetting its logic and setting the desired
 * 			configuration bits
 * 	@param	flags: configuration bits
 * */
extern void BR_DES_Start_Cal(u32 flags);

#endif /* BR_DES_H_ */
