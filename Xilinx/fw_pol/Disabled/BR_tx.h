/*
 * BR_tx.h
 *
 *  Created on: Sep 7, 2016
 */

#ifndef BR_TX_H_
#define BR_TX_H_

#include "xil_types.h"
#include "xparameters.h"

#define	TXT_BASE_ADDR						(XPAR_M07_AXI_BASEADDR)

#define	TXT_VERSION						(0x14)
#define	TXT_START_BURST_OFFSET			(0x1000)
#define	TXT_MASK_OFFSET					(0x1004)
#define	TXT_BURST_LENGTH_OFFSET			(0x1008)
#define	SCRATCH_OFFSET					(0x100C)
#define	TXT_LOOKUP_TABLE_START_OFFSET	(0x1100)
#define	TXT_LOOKUP_TABLE_END_OFFSET		(0x12FC)

#define	TXT_START_BURST_START			(0x1u)
#define	TXT_LOOKUP_TABLE_SIZE			(TXT_LOOKUP_TABLE_END_OFFSET - TXT_LOOKUP_TABLE_START_OFFSET + 4)

#define	REG_TXT_VERSION					(TXT_BASE_ADDR + TXT_VERSION)
#define	REG_TXT_START_BURST				(TXT_BASE_ADDR + TXT_START_BURST_OFFSET)
#define	REG_TXT_MASK					(TXT_BASE_ADDR + TXT_MASK_OFFSET)
#define	REG_TXT_BURST_LENGTH			(TXT_BASE_ADDR + TXT_BURST_LENGTH_OFFSET)
#define	REG_SCRATCH_REG					(TXT_BASE_ADDR + SCRATCH_OFFSET)
#define	REG_TXT_LOOKUP_TABLE_START		(TXT_BASE_ADDR + TXT_LOOKUP_TABLE_START_OFFSET)
#define	REG_TXT_LOOKUP_TABLE_END		(TXT_BASE_ADDR + TXT_LOOKUP_TABLE_END_OFFSET)

#define	TXT_MSK_ALL_ON					(0xffffffff)
#define	TXT_MSK_ALL_OFF					(0x0)
#define	TXT_DEFAULT_BURST_LENGTH		(128)
#define	TXT_DEFAULT_MASK				TXT_MSK_ALL_OFF

#define	TXT_RD_REG32(x)					(*(u32*)(x))

/*==================================== External API ====================================*/
#define	SET_TXT_BURST_LENGTH(x)			(*(u32*)REG_TXT_BURST_LENGTH = (x))
#define	SET_TXT_MASK(x)					(*(u32*)REG_TXT_MASK = (x))
#define	SET_TXT_LOOKUP_TABLE(x, y)		(*(u32*)(REG_TXT_LOOKUP_TABLE_START + 4*(x)) = (y))
#define	TXT_START_BURST					(*(u32*)REG_TXT_MASK = TXT_START_BURST_START)
#define	TXT_STOP_BURST					(*(u32*)REG_TXT_MASK = 0)
#define	GET_TXT_LOOKUP_TABLE(x)			(*(u32*)(REG_TXT_LOOKUP_TABLE_START + 4*(x)))		/* x = LUT ordinal (0 based) */

extern void BR_TX_Initialize(void);
extern int BR_TX_Burst_Length(u8 * burst_length, u32 len_);
extern void BR_TX_fill_lut(u32 *lut);

#endif /* BR_TX_H_ */
