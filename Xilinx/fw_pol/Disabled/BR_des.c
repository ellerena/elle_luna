/*
 * BR_des.c
 *
 *  Created on: Sep 11, 2016
 */

#include "BR_des.h"

/**
 * 	@brief	Initializes the DES by resetting its logic and setting the default values
 * */
extern void BR_DES_Initialize(void)
{
	DES_RESET;										/* reset the DES logic */
}

/**
 * 	@brief	Starts the ser/des calibration resetting its logic and setting the desired
 * 			configuration bits
 * 	@param	flags: configuration bits
 * */
extern void BR_DES_Start_Cal(u32 flags)
{
	u32 i = 0xfffff;

	flags |= DES_B_START;
	SET_DES_REG(REG_DES_CONTROL, flags);						/* start the DES with selected settings */
	flags &= ~DES_B_START;
	SET_DES_REG(REG_DES_CONTROL, flags);						/* clear 'start' flag */
	while (i--)													/* wait for process to be done */
	{
		flags = DES_STATUS;
		if((DES_M_FRAME_ALIGNED == (DES_M_FRAME_ALIGNED&flags)) || \
				((DES_M_ALIGN_ERROR & flags)))
			break;
	}

}



