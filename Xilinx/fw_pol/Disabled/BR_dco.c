/*
 * BR_dco.c
 *
 *  Created on: Oct 11, 2016
 */

#include "BR_dco.h"
#include "BR_timer.h"												/* required for delays */

/**
 * 	@brief	Initializes the DCO.
 * */
void BR_DCO_Initialize(void)
{
	u32 *ptemp;

	ptemp = (u32*)DCO_BASE_ADDR;
	WR_OFF32(DCO_START_OFFSET, DCO_V_RESET_LOGIC);					/* reset DCO logic */
	BR_TIMER_delay_us(DCO_CAL_TIMEOUT);									/* wait for reset to complete */
	WR_OFF32(DCO_CONTROL_OFFSET, DCO_CTRL_INIT);					/* set default control value */
}

/**
 * 	@brief	This function fills the DCO calibration data. Calibration data may be either
 * 			provided by the user (32 words user_data) or automatically generated.
 * 	@param	user_data, is a pointer to the location of the user provided data.
 * 			if user data is provided then it must be as an array of 32 words (u32).
 * 	@param	len, if len is 32*4 then the user data is programmed and the mode set to "PROG_SEL"
 * 				if len is any other value, then automatic calibration is performed.
 * 	@return	0 if everything goes well, otherwise -1
 * */
int BR_DCO_Calibrate(u32* user_data, u32 len)
{
	volatile u32 *ptemp;
	int temp = 0;

	ptemp = (u32*)DCO_BASE_ADDR;
	WR_OFF32(DCO_START_OFFSET, DCO_V_RESET_LOGIC);					/* reset DCO logic */
	BR_TIMER_delay_us(DCO_CAL_TIMEOUT);									/* wait for reset to complete */

	if (len == (4*32))												/* program user data and select programmed data mode */
	{
		len = RD_OFF32(DCO_CONTROL_OFFSET);							/* reuse var len to get control register */
		len |= DCO_B_PROG_SEL;										/* enable PROG_SEL mode */
		WR_OFF32(DCO_CONTROL_OFFSET, len);							/* upload to control register */
		ptemp = (u32*)REG_DCO_PROG_START;							/* point to REG_DCO_PROG_START address */
		do
		{
			BR_OUT32(ptemp++, *user_data++);					/* write user calibration data */
		} while (ptemp <= (u32*)REG_DCO_PROG_END);					/* loop from REG_DCO_PROG_START to REG_DCO_PROG_END */
	}
	else
	{
		len = RD_OFF32(DCO_CONTROL_OFFSET);							/* reuse var len to get control register */
		len &= ~DCO_B_PROG_SEL;										/* disable PROG_SEL mode */
		WR_OFF32(DCO_CONTROL_OFFSET, len);							/* upload to control register */
		WR_OFF32(DCO_START_OFFSET, DCO_B_START);					/* start auto calibration */
		temp = DCO_CAL_TIMEOUT;
		do															/* wait for calibration to complete */
		{
			if(RD_OFF32(DCO_STATUS_OFFET) & DCO_M_CAL_RESULT)
			{
				break;
			}
		} while(temp--);
	}

	if(temp > 0)
	{
		temp = 0;
	}
	return temp;
}



