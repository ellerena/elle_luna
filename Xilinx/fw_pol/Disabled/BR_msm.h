/*
 * BR_msm.h
 *
 *  Created on: Sep 11, 2016
 */

#ifndef BR_MSM_H_
#define BR_MSM_H_

#include "BR_regs.h"

/*================================= bit definitions ==================================*/
#define	MSM_B_START_ACQ					(1 << 0)								/* MSM start acquisition bit */
#define	MSM_V_RESET_LOGIC				(0)										/* Value to reset the MSM */
#define	MSM_B_FULL_CAPTURE				(1 << 0)								/* Data Acquisition runs until complete collection depth */

/*====================================== masks  ======================================*/
#define	MSM_M_IDLE						(1 << 0)								/* MSM is currently idle */
#define MSM_M_RUNNING					(1 << 1)								/* MSM is running, work in progress */
#define	MSM_M_ENDED						(1 << 2)								/* MSM capture completed */
#define	MSM_M_COL_DEPTH					(0x1ff)									/* MSM collection depth value mask [1..511] */
#define	MSM_M_WDW_START					(0x000000ff)							/* MSM sampling window start [0..255]*/
#define	MSM_M_WDW_LENGTH				(0x01FF0000)							/* MSM sampling window length [1..511] << 16 */
#define	MSM_O_WDW_LENGTH				(16)									/* MSM sampling window length offset bit field */

/*==================================== constants =====================================*/
#define	MSM_COL_DEPTH_DEFAULT			(128)									/* default collection depth is 128 samples */
#define	MSM_SAM_WDW_0_127				((128 << MSM_O_WDW_LENGTH) + 0)			/* sample window [0..127] */
#define	MSM_SAMPLE_CAPTURE_FULL			(MSM_B_FULL_CAPTURE)					/* sample capture the full collection depth */
#define	MSM_SAMPLE_CAPTURE_PARTIAL		(0)										/* sample capture just the window */
#define	MSM_RESET_TIMEOUT				(10)									/* used to determine reset hangs */

/*===================================== macros =======================================*/
#define	SET_MSM_COL_DEPTH(x)			BR_OUT32(REG_MSM_COL_DEPTH, x)
#define	MSM_RESET						BR_OUT32(REG_MSM_START_ACQ, MSM_V_RESET_LOGIC)
#define	MSM_START_ACQUISITION			BR_OUT32(REG_MSM_START_ACQ, MSM_B_START_ACQ)
#define	MSM_IS_IDLE						(BR_IN32(REG_MSM_STATUS) & MSM_M_IDLE)
#define	MSM_IS_RUNNING					(BR_IN32(REG_MSM_STATUS) & MSM_M_RUNNING)
#define	MSM_CAPTURE_COMPLETED			(BR_IN32(REG_MSM_STATUS) & MSM_M_COMPLETED)
#define	MSM_SAMPLE_WDW					(BR_IN32(REG_MSM_WDW) >> MSM_O_WDW_LENGTH)


typedef struct sMSM_params
{
	u32 collection_depth;					/* MSM collection depth value mask [1..511] */
	u32 full_depth_capture;					/* keep sampling till full depth (1) or stop at window end (0) */
	u16 sample_window_start;				/* defines the start sample [0..255] */
	u16 sample_window_length;				/* defines the sampling window length [1..511] */
} tMSM_params;


/*==================================== External API ====================================*/
/**
 * 	@brief	Initializes the MSM by resetting its logic and setting the default
 * 	 collection parameters.
 * 	@return	negative: initialization failed to reset the MSM
 * 			0		: initialization Ok.
 * */
extern int BR_MSM_Initialize(void);

/**
 * 	@brief	Configures and arms the MSM with a desired configuration
 * 	@param	args, a pointer to an structure of type tMSM_params (casted as u32*) containing the parameters required:
 * 					{Collection Depth, Full/Partial sample capture, Sample Window definition}
 * 			len: unused
 * 	@return	negative: process failed to reset the MSM
 * 			0		: process ended ok.
 * */
extern int BR_MSM_Arm(u32 * args, u32 len);

/**
 * 	@brief	Performs data acquisition
 * */
extern __inline__ void BR_MSM_Start_Acquisition(void);

#endif /* BR_MSM_H_ */
