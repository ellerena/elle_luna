/**
	@file		BR_stm.h
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		STM communication module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#ifndef BR_STM_H_
#define BR_STM_H_

#include "xuartns550.h"
#include "xuartns550_l.h"

#ifdef UBLAZE_FW
#include "BR_regs.h"
#else
#include "xparameters.h"

#define	STM_BASE_ADDR					(XPAR_UARTNS550_0_BASEADDR)

#ifndef WR_OFF32
#define	WR_OFF32(x, y)					Xil_Out32(((u32)ptemp + (x)),(y))
#define RD_OFF32(x)						Xil_In32((u32)ptemp + (x))
#define	WR_OFF8(x, y)					Xil_Out8(((u32)ptemp + (x)),(y))
#define RD_OFF8(x)						Xil_In8((u32)ptemp + (x))
#define	BR_OUT32(x, y)					Xil_Out32((u32)(x),(y))
#define	BR_IN32(x)						Xil_In32((u32)(x))
#endif
#endif
/*********/
/*================================= bit definitions ==================================*/

/*====================================== masks  ======================================*/

/*==================================== constants =====================================*/
#define	STM_UART_BAUD					(1500000)
#define	STM_UART_CLOCK_FREQ				XPAR_UARTNS550_0_CLOCK_FREQ_HZ
#define STM_DEVICE_ID   		        XPAR_UARTNS550_0_DEVICE_ID

/*==================================== Command Sets ===================================*/

/*==================================== External API ====================================*/

/**
 * @brief	This routine sends 1 or more 1-byte commands to the STM via UART.
 * 			For each command send, it will use the STM BUSY pin to wait until
 * 			the command is executed and completed before sending the next command.
 * @param	data, is a pointer to an array in memory containing the commands to be sent in sequence.
 * @param	len, is the number of commands in the array
 */
extern void BR_STM_send_command(u8 * data, u32 len);

/**
 * @brief	This routine implements the POL_COM_STM_REG_READ.
 * @param	data, is a pointer to a byte in ram: red_address.
 * @param	len, unused.
 * @return	A byte value returned by the Big Red board (via STM)
 */
int BR_STM_Read_Register(u8 * data, u32 len);

/**
 * @brief	This routine performs initialization of the UART port in preparation to
 * 			interfacing with the STM32 located in the BigRed board.
 */
extern void BR_STM_initialize(void);

/**
 *	@brief	Instructs the STM to repeat execute the last commands previously completed.
 */
extern void BR_STM_Repeat_Last_Command(void);

#endif /* BR_STM_H_ */
