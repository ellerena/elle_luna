/*
 * BR_ofo.c
 *
 *  Created on: Sep 11, 2016
 */

#include "BR_ofo.h"
#include "device/BR_timer.h"

void BR_OFO_Initialize(u32 pattern)
{
	u32 * ptemp;

	ptemp = (u32*)PAG_BASE_ADDR;							/* point to pattern generator register */
	WR_OFF32(PAG_CONTROL_OFFSET, PAG_B_RESET_LOGIC);		/* reset and stop PAG */

	ptemp = (u32*)OFO_BASE_ADDR;							/* point to OFO base */
	WR_OFF32(OFO_CONTROL_OFFSET, OFO_B_RESET_LOGIC);		/* reset OFO, pattern generator bypassed */
	BR_TIMER_delay_us(10);									/* Wait for reset to take effect */
	WR_OFF32(OFO_CONTROL_OFFSET, OFO_B_SIGN_EXT_ENA);		/* signal extension enabled */

	ptemp = (u32*)PAG_BASE_ADDR;							/* point to pattern generator register */
	WR_OFF32(PAG_CONTROL_OFFSET, pattern);
}




