/*
 * BR_dco.h
 *
 *  Created on: Oct 11, 2016
 */

#ifndef BR_DCO_H_
#define BR_DCO_H_

#include "BR_regs.h"

/*================================= bit definitions ==================================*/
#define	DCO_B_START						(1 << 0)								/* DCO start calibration bit */
#define	DCO_B_PROG_SEL					(1 << 16)								/* Use DCO user programmed value */
#define	DCO_B_CAL_SEL					(0)										/* Use DCO calculated calibration value */
#define	DCO_V_RESET_LOGIC				(0)										/* Value to reset the DCO */

/*====================================== masks  ======================================*/
#define	DCO_M_DONE						(1 << 0)								/* DCO calibration completed */
#define DCO_M_OVERFLOW					(1 << 1)								/* DCO calibration overflowed in at least one channel */
#define	DCO_M_UNDERFLOW					(1 << 2)								/* DCO calibration underflowed in at least one channel */
#define	DCO_M_CAL_RESULT				(DCO_M_DONE + DCO_M_OVERFLOW + DCO_M_UNDERFLOW)
#define	DCO_M_AVG_CNT					(0xff)									/* DCO number of samples to average (8, 16, 32 or 64) */
#define	DCO_M_CAL_VAL					(0xfff)									/* DCO mask to get the calibrated DCO value */
#define	DCO_M_PROG_VAL					(0xff)

/*==================================== constants =====================================*/
#define	DCO_AVG_CNT_08					(0x08)									/* DCO uses 8 samples average */
#define	DCO_AVG_CNT_16					(0x10)									/* DCO uses 16 samples average */
#define	DCO_AVG_CNT_32					(0x20)									/* DCO uses 32 samples average */
#define	DCO_AVG_CNT_64					(0x40)									/* DCO uses 64 samples average */
#define	DCO_PROG_LEN					(32)									/* size of DCO_PROG register set in words (u32) */
#define	DCO_CAL_TIMEOUT					(10)
#define	DCO_CTRL_INIT					(DCO_AVG_CNT_16 + DCO_B_CAL_SEL)		/* DCO initial control value */

/*==================================== External API ====================================*/
/**
 * 	@brief	Initializes the DCO by resetting its logic and setting the default
 * 	 parameters.
 * */
extern void BR_DCO_Initialize(void);

/**
 * 	@brief	This function fills the DCO calibration data. Calibration data may be either
 * 			provided by the user (32 words received at user_data) or automatically
 * 			generated.
 * 	@param	user_data, is a pointer to the location of the user provided calibration data.
 * 			if this data is provided then it must follow this sequence:
 * 				word 0: one of the DCO_AVG_CNT_xx values
 * 				word 1 to 32: DC offset calibration data provided by the user.
 * 	@param	len, if len is 4 then only the DCO ctrl. register is programmed (DCO avg. cnt and prog sel).
 * 				if len is 33*4 then the DCO ctrl. register and user data are programmed
 * 				if len is any other value, then automatic calibration is performed.
 * 	@return	0 if everything goes well, otherwise -1
 * */
int BR_DCO_Calibrate(u32* user_data, u32 len);

#endif /* BR_DCO_H_ */
