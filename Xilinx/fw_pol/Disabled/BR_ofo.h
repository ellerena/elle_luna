/*
 * BR_ofo.h
 *
 *  Created on: Sep 11, 2016
 */

#ifndef BR_OFO_H_
#define BR_OFO_H_

#include "BR_regs.h"

/*================================= bit definitions ==================================*/
#define	OFO_B_FIFO_EMPTY				(0x10000)				/* OFO fifo is empty */
#define	OFO_B_FIFO_FULL					(0x20000)				/* OFO fifo is full */
#define OFO_B_INTERNAL_PAG				(1 << 0)				/* disconnects input and uses internal PAG */
#define	OFO_B_SIGN_EXT_ENA				(1 << 1)				/* signal extension enable */
#define	OFO_B_RESET_LOGIC				(1 << 31)				/* resets OGO logic */
#define PAG_B_PATTERN_OFF				(0x0)					/* stop and bypass PAG*/
#define PAG_B_PATTERN_A					(0x1)					/* starts PAG w/pattern A */
#define PAG_B_PATTERN_B					(0x2)					/* starts PAG w/pattern B */
#define	PAG_B_RESET_LOGIC				(1 << 31)				/* resets PAG logic */

/*====================================== masks  ======================================*/
#define	OFO_M_READ_DATA_COUNT			(0x3fff)				/* 14 bit field (16kb) */
#define	OFO_M_FIFO_EMPTY				OFO_B_FIFO_EMPTY
#define	OFO_M_FIFO_FULL					OFO_B_FIFO_FULL
#define OFO_M_INTERNAL_PAG_STATE		OFO_B_PAG_ENABLE
#define PAG_M_STATE						(PAG_B_PATTERN_A | PAG_B_PATTERN_B)

/*====================================== macros ======================================*/
#define	OFO_IS_EMPTY					(OFO_M_FIFO_EMPTY & BR_IN32(REG_OFO_STATUS))
#define	OFO_IS_FULL						(OFO_M_FIFO_FULL & BR_IN32(REG_OFO_STATUS))
#define	OFO_DATA_AVAILABLE				(OFO_M_READ_DATA_COUNT & & BR_IN32(REG_OFO_STATUS))
#define	OFO_RESET						BR_OUT32(REG_OFO_CONTROL, OFO_B_RESET_LOGIC)
#define	OFO_USE_INTERNAL_PAG			BR_OUT32(REG_OFO_CONTROL, OFO_B_INTERNAL_PAG)
#define	OFO_STOP_INTERNAL_PAG			BR_OUT32(REG_OFO_CONTROL, 0)
#define	OFO_SIGN_EXTEND_ENABLE			BR_OUT32(REG_OFO_CONTROL, OFO_B_SIGN_EXT_ENA)
#define PAG_PATTERN_OFF					BR_OUT32(REG_PAT_CONTROL, PAG_B_PATTERN_OFF)
#define PAG_SEL_PATTERN_A				BR_OUT32(REG_PAT_CONTROL, PAG_B_PATTERN_A)
#define PAG_SEL_PATTERN_B				BR_OUT32(REG_PAT_CONTROL, PAG_B_PATTERN_B)
#define	PAG_RESET_AND_STOP				BR_OUT32(REG_PAT_CONTROL, PAG_B_RESET_LOGIC)

/*==================================== External API ====================================*/
/**
 * @brief	Initializes the output fifo (OFO) as well as the pattern generator (PAG).
 * @param	pattern, is either PAG_B_PATTERN_A, PAG_B_PATTERN_B or PAG_B_PATTERN_OFF
 */
extern void BR_OFO_Initialize(u32 pattern);

#endif /* BR_OFO_H_ */
