/*
 * BR_ofo.c
 *
 *  Created on: Sep 11, 2016
 */

#include "BR_msm.h"

/**
 * 	@brief	Initializes the MSM by resetting its logic and setting the default
 * 	 collection parameters.
 * 	@return	negative: initialization failed to reset the MSM
 * 			0		: initialization Ok.
 * */
int BR_MSM_Initialize(void)
{
	volatile u32 *ptemp;
	int temp;

	ptemp = (u32*)MSM_BASE_ADDR;
	WR_OFF32(MSM_START_ACQ_OFFSET, MSM_V_RESET_LOGIC);		/* reset MSM logic */
	WR_OFF32(MSM_COL_DEPTH_OFFSET, MSM_COL_DEPTH_DEFAULT);	/* set collection depth to 128 */
	WR_OFF32(MSM_CONTROL_OFFSET, MSM_B_FULL_CAPTURE);		/* set full window sample capture */
	WR_OFF32(MSM_WINDOW_OFFSET, MSM_SAM_WDW_0_127);			/* set sample window [0..127] */
	temp = MSM_RESET_TIMEOUT;
	do
	{
		if(RD_OFF32(MSM_STATUS_OFFSET) & MSM_M_IDLE)
		{
			break;												/* if MSM becomes idle, exit */
		}
	}
	while(temp--);												/* count down loop */

	if(temp > 0)
	{
		temp = 0;
	}
	return temp;
}

/**
 * 	@brief	Configures and arms the MSM with a desired configuration
 * 	@param	args, a pointer to an structure of type tMSM_params (casted as u32*) containing the parameters required:
 * 					{Collection Depth, Full/Partial sample capture, Sample Window definition}
 * 			len: unused
 * 	@return	negative: process failed to reset the MSM
 * 			0		: process ended ok.
 * */
int BR_MSM_Arm(u32 * args, u32 len)
{
	volatile u32 *ptemp;
	int temp;

	ptemp = (u32*)MSM_BASE_ADDR;
	WR_OFF32(MSM_START_ACQ_OFFSET, MSM_V_RESET_LOGIC);		/* reset MSM logic */
	WR_OFF32(MSM_COL_DEPTH_OFFSET, *(args + 0));			/* set collection depth to 128 */
	WR_OFF32(MSM_CONTROL_OFFSET, *(args + 1));				/* set full window sample capture */
	WR_OFF32(MSM_WINDOW_OFFSET, *(args + 2));				/* set sample window [0..127] */
	temp = MSM_RESET_TIMEOUT;
	do
	{
		if(RD_OFF32(MSM_STATUS_OFFSET) & MSM_M_IDLE)
		{
			break;												/* if MSM becomes idle, exit */
		}
	}
	while(temp--);												/* count down loop */

	if(temp > 0)
	{
		temp = 0;
	}

	return temp;
}

/**
 * 	@brief	Performs data acquisition
 * */
void BR_MSM_Start_Acquisition(void)
{
	volatile u32 *ptemp;

	ptemp = (u32*)MSM_BASE_ADDR;
	WR_OFF32(MSM_START_ACQ_OFFSET, MSM_B_START_ACQ);		/* MSM: start capture */
	while(!(RD_OFF32(MSM_STATUS_OFFSET) & MSM_M_ENDED));	/* Capture done ? */
	WR_OFF32(MSM_START_ACQ_OFFSET, MSM_V_RESET_LOGIC);		/* required by design */
}



