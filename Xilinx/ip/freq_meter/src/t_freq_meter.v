`timescale 1ns / 1ps
module t_freq_meter;
    reg t_ck, s_ck;
    wire [15:0] val;

	freq_meter fm (t_ck, s_ck, val);

	initial begin
		t_ck=0;
		s_ck=0;
	end

	always #10 t_ck = ~t_ck;
	always #220 s_ck = ~s_ck;
	
endmodule

