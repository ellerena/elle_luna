`timescale 1ns / 1ps

/*	Frequency meter circuit
	tst_clk: clock we want to measure
	sam_clk: local clock. It is required that sam_clk be "much slower" than tst_clk
	freq_val: result of the process
 */

module freq_meter(
	input tst_clk, sam_clk,
	output reg [15:0] freq_val
	);

	reg [3:0] cnt0;
	reg [15:4] cnt1;

	wire sam_tck;

	always @ (posedge tst_clk or posedge sam_tck)
	begin
		cnt0 <= sam_tck ? 0 : cnt0 + 1;
		cnt1 <= sam_tck ? 0 : cnt1 + (cnt0 == 4'hf);
	end

	always @ (posedge sam_clk)
		freq_val <= {cnt1, cnt0};

	Level2Pulse l2p (tst_clk, sam_clk, sam_tck);

endmodule

module Level2Pulse(
	input c, i,
	output o );

	reg x = 0;
	assign o = ~x & i;
	always @ (posedge c) x <= i;

endmodule