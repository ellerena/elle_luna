`timescale 1ns / 1ps

module t_ftdi_controller(output wire rd_n, wr_n, oe_n);

reg cin = 1'b1, cout = 1'b1, rxf_n = 1'b1, rst = 0, read = 0;
reg [7:0] ftin = 8'h34;

ftdi_wr_if #(8, 2048) ftdi(
	.cin(cin),
	.cout(cout), .read(read), .txe_n(1'b1),
	.rst(rst),
	.rxf_n(rxf_n),
	.ftin(ftin),
	.wr_n(wr_n),
	.rd_n(rd_n),
	.oe_n(oe_n)
	);

always #8.3 cin = ~cin;
always #20 cout = ~cout;
initial begin
	#2 rst = 1'b1;
	#10 rst = 0;
	#7 rxf_n = 0;
	#100 rxf_n = 1'b1;
	#20 read = 1'b1;
	#80 read = 1'b0;
end

endmodule

