set OS [lindex $tcl_platform(os) 0]
if { $OS == "Windows" } {
  set MOJOLOAD "C:/Program Files/Mojo Loader/mojo-loader.exe"
  set MOJOPORT COM121
} else {
  set MOJOLOAD /home/eddie/engineer/mojo/mojo-loader
  set MOJOPORT /dev/ttyACM0
}

set BINFILE [file normalize ./nosvc/mojo_top.bin]
exec $MOJOLOAD -p $MOJOPORT -b $BINFILE -t #-f -v

