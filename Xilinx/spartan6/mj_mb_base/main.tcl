# 
# Project automation script for mj_mb 
# 
# Created for ISE version 14.7
# 
# This file contains several Tcl procedures (procs) that you can use to automate
# your project by running from xtclsh or the Project Navigator Tcl console.
# If you load this file (using the Tcl command: source /mnt/data/projects/git/elle_luna/Xilinx/spartan6/mj_mb_base/main.tcl), then you can
# run any of the procs included here.
# 
# This script is generated assuming your project has HDL sources.
# Several of the defined procs won't apply to an EDIF or NGC based project.
# If that is the case, simply remove them from this script.
# 
# You may also edit any of these procs to customize them. See comments in each
# proc for more instructions.
# 
# This file contains the following procedures:
# 
# Top Level procs (meant to be called directly by the user):
#    run_process: you can use this top-level procedure to run any processes
#        that you choose to by adding and removing comments, or by
#        adding new entries.
#    rebuild_project: you can alternatively use this top-level procedure
#        to recreate your entire project, and the run selected processes.
# 
# Lower Level (helper) procs (called under in various cases by the top level procs):
#    show_help: print some basic information describing how this script works
#    add_source_files: adds the listed source files to your project.
#    set_project_props: sets the project properties that were in effect when this
#        script was generated.
#    create_libraries: creates and adds file to VHDL libraries that were defined when
#        this script was generated.
#    set_process_props: set the process properties as they were set for your project
#        when this script was generated.
# 

set myProject "mj_mb"
set origin_dir [file normalize [file dirname [info script]]]
set origin_file [file normalize [info script]]
set GIT_FOLDER [file normalize $origin_dir/../../../../]
set myScript "$origin_file"

# 
# Main (top-level) routines
# 
# run_process
# This procedure is used to run processes on an existing project. You may comment or
# uncomment lines to control which processes are run. This routine is set up to run
# the Implement Design and Generate Programming File processes by default. This proc
# also sets process properties as specified in the "set_process_props" proc. Only
# those properties which have values different from their current settings in the project
# file will be modified in the project.
# 
proc run_process {} {

   ## put out a 'heartbeat' - so we know something's happening.
   puts "\n>>run_process...\n"

   set_process_props
   #
   # process run "Synthesize"
   # process run "Translate"
   # process run "Map"
   # process run "Place & Route"
   #
   set task "Implement Design"
   if { ! [run_task $task] } {
      puts ">>$task run failed, check run output for details."
      project close
      return
   }

   set task "Generate Programming File"
   if { ! [run_task $task] } {
      puts ">>$task run failed, check run output for details."
      project close
      return
   }

   puts ">>Run completed (successfully)."
   project close

}

# 
# rebuild_project
# 
# This procedure renames the project file (if it exists) and recreates the project.
# It then sets project properties and adds project sources as specified by the
# set_project_props and add_source_files support procs. It recreates VHDL Libraries
# as they existed at the time this script was generated.
# 
# It then calls run_process to set process properties and run selected processes.
# 
proc rebuild_project {} {

   global myProject
   global origin_dir

   ## put out a 'heartbeat' - so we know something's happening.
   puts "\n>>Rebuilding ($myProject)\n"

   set proj_exts [ list ise xise gise ]
   foreach ext $proj_exts {
      set proj_name "$origin_dir/workdir/${myProject}.$ext"
      if { [ file exists $proj_name ] } { 
         file rename $origin_dir/workdir $origin_dir/workdir_nosvc 
      }
   }

   file mkdir "$origin_dir/workdir"
   cd "$origin_dir/workdir"
   project new $myProject
   set_project_props
   add_source_files
#   create_libraries
   puts ">>project rebuild completed."

#   run_process

}

# 
# Support Routines
# 

# 
proc run_task { task } {

   # helper proc for run_process

   puts ">>Running '$task'"
   set result [ process run "$task" ]
   #
   # check process status (and result)
   set status [ process get $task status ]
   if { ( ( $status != "up_to_date" ) && \
            ( $status != "warnings" ) ) || \
         ! $result } {
      return false
   }
   return true
}

proc open_project {} {

   global myProject

   if { ! [ file exists ${myProject}.xise ] } { 
      ## project file isn't there, rebuild it.
      puts "Project $myProject not found. Use project_rebuild to recreate it."
      return false
   }

   project open $myProject

   return true

}
# 
# set_project_props
# 
# This procedure sets the project properties as they were set in the project
# at the time this script was generated.
# 
proc set_project_props {} {

   puts ">>Setting project properties..."

   project set family "Spartan6"
   project set device "xc6slx9"
   project set package "tqg144"
   project set speed "-2"
   project set top_level_module_type "HDL"
   project set synthesis_tool "XST (VHDL/Verilog)"
   project set simulator "ISim (VHDL/Verilog)"
   project set "Preferred Language" "Verilog"
   project set "Enable Message Filtering" "false"

}


# 
# add_source_files
# 
# This procedure add the source files that were known to the project at the
# time this script was generated.
# 
proc add_source_files {} {

   global myScript
   global GIT_FOLDER
   global origin_dir

   if { ! [ open_project ] } {
      return false
   }

   puts ">> Adding sources to project..."

   file mkdir "$origin_dir/workdir/ipcore_dir"
   file copy "$origin_dir/ipcore_dir/mb_mcs.xco" "ipcore_dir/mb_mcs.xco"

   xfile add "$GIT_FOLDER/vivado/common/verilog/fast_counter.v"
   xfile add "$GIT_FOLDER/vivado/ip/time_interval_1.0/hdl/TimeInterval.v"
   xfile add "$origin_dir/hdl/GlobalMacros.h" -include_global
   xfile add "$origin_dir/hdl/avr_interface.v"
   xfile add "$origin_dir/hdl/cclk_detector.v"
   xfile add "$origin_dir/hdl/clamp_mod.v"
   xfile add "$origin_dir/hdl/mojo.ucf"
   xfile add "$origin_dir/hdl/mojo_top.v"
   xfile add "$origin_dir/hdl/serial_rx.v"
   xfile add "$origin_dir/hdl/serial_tx.v"
   xfile add "$origin_dir/hdl/spi_slave.v"
   xfile add "ipcore_dir/mb_mcs.xco"
   puts ""
   puts "WARNING: project contains IP cores, synthesis will fail if any of the cores require regenerating."
   puts ""

   # Set the Top Module as well...
   project set top "mojo_top"

   puts ">> project sources reloaded."

} ; # end add_source_files

# 
# create_libraries
# 
# This procedure defines VHDL libraries and associates files with those libraries.
# It is expected to be used when recreating the project. Any libraries defined
# when this script was generated are recreated by this procedure.
# 
proc create_libraries {} {

   global myScript

   if { ! [ open_project ] } {
      return false
   }

   puts ">> Creating libraries..."


   # must close the project or library definitions aren't saved.
   project save

} ; # end create_libraries

# 
# set_process_props
# 
# This procedure sets properties as requested during script generation (either
# all of the properties, or only those modified from their defaults).
# 
proc set_process_props {} {

   global myScript

   if { ! [ open_project ] } {
      return false
   }

   puts ">> setting process properties..."

   project set "Other Bitgen Command Line Options" "-bd \"/mnt/data/projects/git/elle_luna/Xilinx/sdk/mj_mb_base_app/r_nosvc/mj_mb_base_app.elf\" tag mb_mcs" -process "Generate Programming File"
   project set "Other Ngdbuild Command Line Options" "-bm \"ipcore_dir/mb_mcs.bmm\"" -process "Translate"
   project set "Enable Internal Done Pipe" "true" -process "Generate Programming File"

   puts ">> project property values set."

} ; # end set_process_props

proc main {} {

   if { [llength $::argv] == 0 } {
      puts $origin_file
      rebuild_project
      return true
   }
}

if { $tcl_interactive } {
   rebuild_project
} else {
   if {[catch {main} result]} {
      puts ">> failed: $result."
   }
}

