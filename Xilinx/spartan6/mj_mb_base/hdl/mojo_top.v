module mojo_top( input mclk, rst_n, cclk /* cclk from AVR, 1: AVR is ready */,
            spi_ss, spi_mosi, spi_sck, avr_tx, avr_rx_busy, sma, ltch, titrg,
            output spi_miso, avr_rx, smaO, claO,
            output [3:0] spi_channel, output[7:0]led
            );

reg [7:0] leds;
reg [8:0] uart_rx;
reg [31:0] i_data;
wire [31:0] io_addr, ctrl_reg, uart_i, uart_o;
wire [14:0] adc_o;
wire [15:0] tim;
wire [7:0] rx_data, led_;
wire rx_ready, fit1_toggle;
assign led = leds;
assign rst = ~rst_n;
assign uart_o[31:10] = {22'h0, avr_rx_busy};
assign uart_o[8:0] = uart_rx;

DCM_SP #(
   .FACTORY_JF(16'hC080),               // FACTORY JF values
   .CLKIN_PERIOD(`CLKIN_PERIOD),         // period of input clock
   .CLKDV_DIVIDE(`CLKDV_DIVIDE),         // Divide by: 1.5,2.0... 7.5,8,9... 16
   .CLKFX_DIVIDE(`CLKFX_DIVIDE),         // integer from 1 to 32
   .CLKFX_MULTIPLY(`CLKFX_MULTIPLY)    // integer from 2 to 32
   )
   DCM_SP_inst (
      .CLKIN(mclk),                     // Clock input (from IBUFG, BUFG or DCM)
      .RST(1'b0),                        // DCM asynchronous reset input
      .PSEN(1'b0),                     // Phase shift enable
      .CLK0(clk),                        // 0 degree DCM CLK output /* 50MHz for uBlaze */
//      .CLK2X(clk2x),                     // 2X DCM CLK output /* 100MHz */
      .CLKDV(clkdv),                     // Divided DCM CLK out (CLKIN/CLKDV_DIVIDE) /* 1/2 = 25MHz */
      .CLKFX(clkfx),                     // DCM CLK synthesis out (CLKIN*CLKFX_MULTIPLY/CLKFX_DIVIDE) /* 9/1 450MHz */
      .CLKFB(clk)                     // DCM clock feedback
      );

// AVR interface *******************************************
always @ (posedge clk)
   uart_rx <= uart_i[10] ? 9'h0 : rx_ready ? {rx_ready, rx_data} : uart_rx;

avr_interface _avr(
   .clk (clk), .rst (rst),
   .cclk (cclk),

   .spi_mosi (spi_mosi), .spi_sck (spi_sck), .spi_ss (spi_ss),
   .spi_miso (spi_miso),
   .spi_channel (spi_channel),

   // ADC Interface Signals. output: <[new_sample:1],[channel:4],[value:10]>
   .channel (4'hf), //input [3:0], channel 0xf disables ADC
   .new_sample (adc_o[14]),
   .sample (adc_o[9:0]),
   .sample_channel(adc_o[13:10]),

   // AVR Serial Signals
   .rx (avr_tx),
   .tx (avr_rx),

   // Serial TX User Interface
   .tx_data (uart_i[7:0]),
   .new_tx_data (uart_i[8]), .tx_block (avr_rx_busy),
   .tx_busy (uart_o[9]),

   // Serial Rx User Interface
   .new_rx_data (rx_ready),
   .rx_data (rx_data)
  );

// Time Interval Module ************************************
wire cla_trg = (titrg | ctrl_reg[4]);
TimeInterval _ti(clkfx, ctrl_reg[1], sma, ltch, , tim);
clamp_gen _cl(clkdv, cla_trg, ctrl_reg[31:24], smaO, claO);

// Microblaze MCS Module ***********************************
mb_mcs mcs_0 (
      .Clk(clk), // input Clk
      .Reset(rst), // input Reset
      .FIT1_Toggle(fit1_toggle), // output FIT1_Toggle
      .GPI1(i_data), // input [31 : 0] GPI1
      .GPI2(uart_o), // input [31 : 0] GPI2
      .GPO1(io_addr), // output [7 : 0] GPO1
      .GPO2(uart_i), // output [31 : 0] GPO2
      .GPO3(led_), // output [31 : 0] GPO3
      .GPO4(ctrl_reg), // output [31 : 0] GPO4
      .GPI1_Interrupt(), // output GPI1_Interrupt
      .GPI2_Interrupt() // output GPI2_Interrupt
      );

// Miscelaneous ********************************************
always @ *
   case (io_addr[1:0])
      2'h0:   i_data <= ctrl_reg;
      2'h1:   i_data <= `B_DATE;
      2'h2:   i_data <= `B_TIME;
      2'h3:   i_data <= {adc_o, 1'b0, tim};
   endcase

always @ *
   case (ctrl_reg[8])
      1'h0:   leds <= {cclk, ~avr_rx_busy, uart_o[9], 1'b0, cla_trg, smaO, claO, fit1_toggle};
      1'h1:   leds <= led_;
   endcase

endmodule
