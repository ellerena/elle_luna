`include "info.v"
//`include "..\common\verilog\ft232h\ft232fifo.h"

`define SPARTAN_3_DCM
`define CLKIN_PERIOD		(20)     	/* 50MHz crystal */
`define CLKDV_DIVIDE		(2)			/* 2..16 */
`define CLKFX_MULTIPLY	(6)			/* 2..32 */
`define CLKFX_DIVIDE		(1)			/* 1..32  CLKFX = CLKIN * CLOCK_FX_MULTIPLY / CLOCK_FX_DIVIDE_BY */

