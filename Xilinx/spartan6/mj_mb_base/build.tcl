set origin_dir [file normalize [file dirname [info script]]]
set GIT_FOLDER [file normalize $origin_dir/../../../../]
set ELF [file normalize $GIT_FOLDER/elle_luna/Xilinx/sdk/mj_mb_base_app/r_nosvc/mj_mb_base_app.elf]
source ipcore_dir/microblaze_mcs_setup.tcl
puts "******* MB MCS Setup process complete"
source $GIT_FOLDER/vivado/ip/ip_user_files/info.tcl
puts "******* info.v file creation complete"
microblaze_mcs_data2mem $ELF
puts "******* ELF data2mem process complete"
project set "Other Ngdbuild Command Line Options" "-bm \"ipcore_dir/mb_mcs.bmm\"" -process "Translate"
puts "******* Ngdbuild update process complete"
