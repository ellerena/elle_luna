/*
 * Copyright (c) 2015-2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <zephyr/sys/printk.h>
#include <zephyr/sys/util.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/hci.h>

#define ERR_EXIT(r) do \
	{	int ret = (r); \
		if(ret) \
		{ printk("bt_err l:%d err:%d\n", __LINE__, ret); \
		  return; } \
	} while (0)

static uint8_t mfg_dat[] = { 0xff, 0xff, 0x00 };

static const struct bt_data ad[] = { BT_DATA(BT_DATA_MANUFACTURER_DATA, mfg_dat, 3)};

static void ble_gapper_task(void)
{
	printk("Starting Broadcaster\n");
	ERR_EXIT(bt_enable(NULL)); // Initialize the Bluetooth Subsystem
	printk("Bluetooth initialized\n");

	do {

		printk("Sending advertising data: 0x%02X\n", mfg_dat[2]);
		ERR_EXIT(bt_le_adv_start(BT_LE_ADV_NCONN, ad, ARRAY_SIZE(ad), NULL, 0));

		k_msleep(3000);

		ERR_EXIT(bt_le_adv_stop());

		mfg_dat[2]++;

		k_msleep(3000);

	} while (1);
}

K_THREAD_DEFINE(ble_gapper_id, 512, ble_gapper_task, NULL, NULL, NULL, 7, 0, 0);
