/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file task_handler.h
 * @date 06.02.2023
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#pragma once

#if __cplusplus
extern "C" {
#endif

/***************************************************************************************************
 * Includes
 **************************************************************************************************/
#include <stdint.h>
/***************************************************************************************************
 * Macro Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Type Definitions
 **************************************************************************************************/
typedef enum
{
    TASK_LSM6DS3 = 0,
    TASK_NUM
} task_id_t;

typedef enum
{
    TASK_IDLE = 0,
    TASK_START,
    TASK_RUNPROC,
} task_state_t;

/***************************************************************************************************
 * Constant Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Variable Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Function Prototypes
 **************************************************************************************************/
extern int task_handler_new_state(task_id_t task_id, task_state_t task_state);

#if __cplusplus
}
#endif