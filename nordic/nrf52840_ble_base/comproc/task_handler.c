/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file task_handler.c
 * @date 06.02.2023
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#include <zephyr/kernel.h>

#include <task_handler.h>

/************************************ Local Macro Definitions ************************************/
#define MQUEUE_SIZE (10)

/************************************ Local Type Definitions  ************************************/
typedef void (*task_fn)(void);

typedef struct
{
    task_id_t task_id;
    task_state_t new_state;
} task_q_item_t;

typedef struct
{
    task_state_t task_state;
    task_fn task_start_fn;
    task_fn task_runproc_fn;
} task_details_t;

/*********************************** Local Constant Definitions **********************************/
/********************************** Global Variable Declarations *********************************/
/********************************** Local Function Declarations **********************************/
/********************************** Local Variable Declarations **********************************/
task_details_t task[TASK_NUM] = {0};

K_MSGQ_DEFINE(mqueue, sizeof (task_q_item_t), MQUEUE_SIZE, 2);

/********************************** Public Function Declarations *********************************/
/*********************************** Local Function Definitions **********************************/
int task_handler_new_state(task_id_t task_id, task_state_t task_state)
{
    task_q_item_t task_q_item = {.task_id = task_id, .new_state = task_state};
    return k_msgq_put(&mqueue, &task_q_item, K_NO_WAIT);
}

void task_handler_task_handler(void)
{
    task_q_item_t task_q_item;

    for (;;)
    {
        int ret = k_msgq_get(&mqueue, &task_q_item, K_FOREVER);
		// TODO: handle possible error in ret
        task[task_q_item.task_id].task_state = task_q_item.new_state;

        switch (task_q_item.new_state)
        {
            case TASK_START:
            {
                task[task_q_item.task_id].task_start_fn();

                break;
            }

            case TASK_RUNPROC:
            {
                task[task_q_item.task_id].task_runproc_fn();
                break;
            }

            default:
        }

    }
}
