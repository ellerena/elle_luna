/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file command_handlers.h
 * @date 06.12.2020
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#pragma once

#if __cplusplus
extern "C" {
#endif

/***************************************************************************************************
 * Includes
 **************************************************************************************************/

/***************************************************************************************************
 * Macro Definitions
 **************************************************************************************************/
#define CLI_SELECT(task, fn) \
   case task:\
   {\
      extern void fn (char, void *, int);\
      fn(mode, (void*)&ARGS_BEGIN, n);\
   } break

#define TASK         buf[0] /* variable representing the 'task' */
#define MODE         buf[1] /* variable representing the 'mode' */
#define ARGS         buf[2] /* first element of arguments array */

#ifdef MEMALIGNEDREQUIRED
#define ARGS_BEGIN   buf[0] /* position of the arguments in the buffer */
#else
#define ARGS_BEGIN   buf[2] /* position of the arguments in the buffer */
#endif

/***************************************************************************************************
 * Type Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Constant Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Variable Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Function Prototypes
 **************************************************************************************************/
void CommandProcessor(char * buf);
extern int ParseCharsToValueHex (char *p);

#if __cplusplus
}
#endif
