/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file command_parser.c
 * @date 10.10.2021
 * @brief
 *
 * @details
 *
 **************************************************************************************************/


/************************************ Local Macro Definitions ************************************/
/************************************ Local Type Definitions  ************************************/
/*********************************** Local Constant Definitions **********************************/
/********************************** Global Variable Declarations *********************************/
/********************************** Local Function Declarations **********************************/
/********************************** Local Variable Declarations **********************************/
/********************************** Public Function Declarations *********************************/
/**
 @brief   converts a string of chars representing a vector
 of hex numbers into a vector of bytes equivalent.
 Note: if first value is lower than '!' (e.g. tab or space)
 then next bytes remain intact.
 @param   p contains the address of the first char in the string.
 @retval  the count of numbers converted.
 @verbatim
 Text entered by a user via the keyboard is in the form of a string
 of printable chars that represent hex numbers.
 This functions takes that string of chars and extracts the meaningful
 hex numbers represented in it, then it places those numbers (bytes)
 in an array at the same ram location. Returning the number of numbers
 converted.
 @endverbatim
 */
int ParseCharsToValueHex (char * p)
{
#ifdef MEMALIGNEDREQUIRED
  char *q = p - 2;
#else
  char *q = p;
#endif

  int k = 0;

  while (*p > ' ') /* Will process until a non printable character is found */
  {
    char d = 0; /* initialize the 'number equivalent value' */
    int i = 0; /* initialize the digit counter (assume 2 digits max per number)*/

    do
    {
      d <<= 4; /* each digit uses the first 4 bits, so on each loop we must push the others */
      unsigned char c = *p++; /* read the new char */

      if ((c >= '0') && (c <= '9')) /* char is between 0 and 9 */
      {
        d += (c - '0'); /* add the char equivalent value to our total */
        i++; /* increment our digit counter */
      }

      if ((c >= 'a') && (c <= 'f')) /* char is between 0xa and 0xf */
      {
        d += (c - 0x60 + 0x9); /* add the char equivalent value to our total */
        i++; /* increment our digit counter */
      }
    }
    while ((i == 0) || ((*p >= '0') && (i < 2))); /* repeat: if no char found or: if found a space and less than 2 digits */

    if (i) /* if the digit counter is non-zero then a value has been found */
    {
      *q++ = d; /* write the number found to memory */
      k++; /* increment 'detected numbers counter' */
    }
  }

  *q = 0; /* write 0 at the end */

  return k;
}

/*********************************** Local Function Definitions **********************************/
