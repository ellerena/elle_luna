/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file console.c
 * @date 05.25.2023
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#include <zephyr/kernel.h>
#include <zephyr/console/console.h>
#include <command_handlers.h>

/************************************ Local Macro Definitions ************************************/
#define BUF_SIZE (90)

/************************************ Local Type Definitions  ************************************/
/*********************************** Local Constant Definitions **********************************/
/********************************** Global Variable Declarations *********************************/
/********************************** Local Function Declarations **********************************/
static void console_input_event_task (void);

/********************************** Local Variable Declarations **********************************/
/********************************** Public Function Declarations *********************************/
void comproc_uart_start (void)
{
    console_input_event_task();
}

/*********************************** Local Function Definitions **********************************/
static void console_input_event_task (void)
{
    console_getline_init();

	while (1) {
		printk("\n>");
		char *s = console_getline();

		CommandProcessor(s);
	}
}

K_THREAD_DEFINE(console_id, 512, console_input_event_task, NULL, NULL, NULL, 7, 0, 0);
