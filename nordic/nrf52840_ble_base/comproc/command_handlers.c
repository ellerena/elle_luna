/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file command_handlers.c
 * @date 06.17.2020
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#include "command_handlers.h"
#include "mis_gflags.h"

/************************************ Local Macro Definitions ************************************/
/************************************ Local Type Definitions  ************************************/
/*********************************** Local Constant Definitions **********************************/
/********************************** Global Variable Declarations *********************************/
/********************************** Local Function Declarations **********************************/
/********************************** Local Variable Declarations **********************************/
/********************************** Public Function Declarations *********************************/
void CommandProcessor (char * buf)
{
//   display_puts(buf);

  char task = TASK | 0x20; /* to lowercase */
  char mode = MODE | 0x20; /* to lowercase */
  int n = ParseCharsToValueHex(&ARGS);

  switch (task)
  {
    CLI_SELECT('s', cli_sys); /* system operations */

    // CLI_SELECT('m', cli_mem); /* memory access operations */

    CLI_SELECT('i', cli_i2c); /* i2c commands */

    CLI_SELECT('o', cli_ssd1306); /* oled display operations */

    // CLI_SELECT('g', cli_lsm6dsxx); /* lsm6dsxx imu sensor */

    // CLI_SELECT('x', cli_isl29125); /* isl29125 RGB sensor operations */

    // CLI_SELECT('l', cli_led); /* led operations */

    // CLI_SELECT('t', cli_mlx90614); /* mlx90614 temperature sensor */

    // CLI_SELECT('q', cli_qmc5883l); /* qmc5883l magnetometer sensor */

    // CLI_SELECT('9', cli_sx9200); /* sx9200 capacitive touch sensor */

    // CLI_SELECT('1', cli_1w); /* 1-wire and DTH11 operations */

    // CLI_SELECT('c', cli_spi); /* communications, spi operations */

    // CLI_SELECT('u', cli_ftdi); /* USB (FTDI) operations */

    // CLI_SELECT('f', cli_spisniffer); /* SPI Sniffer operations */

    // CLI_SELECT('w', cli_ws2812); /* ws2812 module */

    default:
    {
      // GFS(F_COMMERR);
    }
    break;
  }
}

/*********************************** Local Function Definitions **********************************/
