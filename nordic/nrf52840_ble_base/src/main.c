/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>

#define BUILD_DATETIME __DATE__ " " __TIME__

/* global scope objects */
volatile unsigned int gFlags = 0;

void main(void)
{
	printk("Molon Labe " BUILD_DATETIME "\n");
}
