/*
 * mis_dbg.h
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#ifndef MISC_INC_MIS_DBG_H_
#define MISC_INC_MIS_DBG_H_

#include <mis_logger.h>

#define TSTRCO(x, y)                if(x) return y         /* On test, return code */
#define TSTRLN(x)                   if (x) return __LINE__ /* On test return line */
#define TSTRER(x)                   if (x) return -1       /* On test return fail */
#define TSTLOGE(test, name)         if(test) PRNS(name " NG" CRLF) /* test and log if error */
#define TSTPRNS(test, name)         if(test) PRNS(name " NG" CRLF); \
                                    else PRNS(name " Ok" CRLF)

#define st(x)                       do { x } while (__LINE__ == -1)
#define debounce(x, y)              for(x = 0; x < 500; x++) if(y) x = 0

//#define DBGLED                      *(u32*)REG_LED_DATA = dbgled++;
//#define DBGLEDSHOW(x)               *(u32*)REG_LED_DATA = x;

#endif /* MISC_INC_MIS_DBG_H_ */
