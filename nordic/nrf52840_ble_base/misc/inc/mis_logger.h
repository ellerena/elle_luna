/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file mis_logger.h
 * @date 05.26.2023
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#pragma once

#if __cplusplus
extern "C" {
#endif

/***************************************************************************************************
 * Includes
 **************************************************************************************************/
#include <zephyr/kernel.h>

/***************************************************************************************************
 * Macro Definitions
 **************************************************************************************************/
#define CRLF          "\n"
#define PRNF          printk
#define PRNS(s)       printk(s)    /* print one string line */
#define PRNS2(s)      printk(s) //
#define PRNT(x, y)    if(x) PRNF(y CRLF)
#define PRNHEX(n, w)  printk("%0" #w "x", n)

/* verbose output that can be toggled off/on for release/debug */
#ifdef VERBOSE
#define LOGES(s) PRNS(s)
#else
#define LOGES(s)
#endif

#define PRNTHT(pre, n8w, post)  printk("%s%x%s", pre, n8w, post)
#define PRNTHT2(pre, n8w, post)  PRNTHT(pre, n8w, post)

#define PRINTF(format)
#define PRINTFV(format, ...)

/***************************************************************************************************
 * Type Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Constant Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Variable Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Function Prototypes
 **************************************************************************************************/
extern void log_dump_bytes (const void * data, int len);

#if __cplusplus
}
#endif
