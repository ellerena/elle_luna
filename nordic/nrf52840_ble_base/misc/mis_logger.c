/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file mis_logger.c
 * @date 09.12.2021
 * @brief
 *
 * @details
 *
 **************************************************************************************************/
#include "mis_logger.h"

/************************************ Local Macro Definitions ************************************/
/************************************ Local Type Definitions  ************************************/
/*********************************** Local Constant Definitions **********************************/
/********************************** Global Variable Declarations *********************************/
/********************************** Local Function Declarations **********************************/
/********************************** Local Variable Declarations **********************************/
/********************************** Public Function Declarations *********************************/
void log_dump_bytes (const void * data, int len)
{
  unsigned char * dat = (unsigned char *) data;

  int i = 0;
  while (0 < len--)
  {
    PRNHEX(*dat++, 2);

    if ((15 == (i++ % 16)) || (0 == len))
    {
      PRNS(CRLF);
    }
    else
    {
      PRNS(" ");
    }
  }
}

/*********************************** Local Function Definitions **********************************/
