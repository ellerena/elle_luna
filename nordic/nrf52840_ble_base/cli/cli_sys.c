/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file cli_sys.c
 * @date 09.12.2021
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#include "mis_gflags.h"
#include "mis_logger.h"

/************************************ Local Macro Definitions ************************************/
#define MACHINE_NAME CONFIG_BOARD

/************************************ Local Type Definitions  ************************************/
/*********************************** Local Constant Definitions **********************************/
/********************************** Global Variable Declarations *********************************/
/********************************** Local Function Declarations **********************************/
/********************************** Local Variable Declarations **********************************/
/********************************** Public Function Declarations *********************************/
void cli_sys (char task, void * args, int len)
{
//  unsigned char *pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'i': /* fi: firmware id (welcome) */
    {
      PRNS("******************************\n"
           "        ...and ne forhtedon na\n");
      PRNS("build " __DATE__ " " __TIME__ "\n");

      PRNS("machine: " MACHINE_NAME "\n");
      PRNS("******************************\n");
    }
      break;

    default:
    {
      // GFS(F_COMMERR);
    }
      break;
  }
}

/*********************************** Local Function Definitions **********************************/
