/*
 * cli_i2c.c
 *
 *  Created on: Sep 12, 2021
 *      Author: ellerena
 */

#include <mis_gflags.h>
#include <mis_logger.h>
#include <drv_i2c.h>

#define I2C_DEV_LEN (1)
#define GET_AS_UIN16(p) (((p)[0] << 8) + (p)[1])
#define GET_AS_UINT24(p) ((GET_AS_UIN16(p) << 8) + (p)[2])
#define GET_AS_UINT32(p) ((GET_AS_UIN24(p) << 8) + (p)[3])

void cli_i2c (char task, void * args, int len)
{
  unsigned char *pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'c': /* ic<00/01> select i2c controller to use */
    {
      len = DRV_i2c_set_controller(0 < len ? pArgs[0] : 0xff);
      PRNTHT("i2c: ", len, "\n");

      return;
    }

    case 'i':
    {
      len = DRV_i2c_master_init();

      PRNTHT("ret: ", len, "\n");

      return;
    }

    case 'a': /* ia<xx : 1 hex> address of slave */
    {
      uint8_t new_addr = len > 0 ? pArgs[0] : 0;
      new_addr = DRV_i2c_master_slv_addr(new_addr);

      PRNS("i2c address: ");
      PRNHEX(new_addr, 2);
      PRNS("h [");
      PRNHEX(new_addr << 1, 2);
      PRNS("h/");
      PRNHEX((1 | (new_addr << 1)), 2);
      PRNS("h]" CRLF);

      return;
    }

    case 'l': /* il<xx>, set register address size, xx can be 1, 2 or 4 only */
    {
      len = DRV_i2c_set_reg_address_len(pArgs[0]);

      PRNTHT("reg address length:", len, CRLF);

      return;
    }

    case 'p': /* ip<xx> use stop bit instead of repeated start when reading */
    {
      len = DRV_i2c_set_use_stop(pArgs[0]);
      PRNTHT("i2c use_stop: ", len, CRLF);

      return;
    }

    case 's': /* scan slaves present in bus */
    {
      for (uint8_t i2c_addr7 = 0; i2c_addr7 < 0x7f; ++i2c_addr7)
      {
        if (DRV_i2c_master_is_slave_present(i2c_addr7))
        {
          PRNS("ID: ");
          PRNHEX(i2c_addr7, 2);
          PRNS("h [");
          PRNHEX(i2c_addr7 << 1, 2);
          PRNS("h/");
          PRNHEX((1 | (i2c_addr7 << 1)), 2);
          PRNS("h]" CRLF);
        }
      }

      return;
    }

    case 't': /* transmit raw bytes to the current slave */
    {
      len = DRV_i2c_master_tx(pArgs, len);
      PRNTHT("written ", len, CRLF);

      return;
    }

    case 'g': /* ir<nn> receives nn raw bytes from the current slave */
    {
      len = DRV_i2c_master_rx(pArgs, pArgs[0]);
      PRNTHT("received: ", len, CRLF);
      log_dump_bytes((void*) pArgs, len);

      return;
    }

    case 'w': /* iw<aa><rr[...]><d1...dn> write d1..dn to slave aa @ rr */
    {
      int dat_offset = I2C_DEV_LEN + DRV_i2c_set_reg_address_len(0 /* just read */);

      if (len > dat_offset)
      {
        len = DRV_i2c_master_wr_ar(&pArgs[dat_offset],
                                  len - dat_offset,
                                  pArgs[0],
                                  &pArgs[I2C_DEV_LEN]);
        PRNTHT("written ", len, CRLF);
      }

      return;
    }

    case 'r': /* ir<aa><rr[...]><ll> read ll bytes from slave aa @ rr */
    {
      int len_offset = I2C_DEV_LEN + DRV_i2c_set_reg_address_len(0 /* just read */);

      if (len > len_offset)
      {

        len = DRV_i2c_master_rd_ar(pArgs,
                                  pArgs[len_offset],
                                  pArgs[0],
                                  &pArgs[I2C_DEV_LEN]);
        PRNTHT("received: ", len, CRLF);

        log_dump_bytes(pArgs, len);
      }

      return;
    }

    // case 'f': /* if<nnnnnn: hex 24b>, set clock speed nnnnnn */
    // {
    //   if (len < 3)
    //   {
    //     break;
    //   }
    //   int clock_speed = GET_AS_UINT24(pArgs);
    //   clock_speed  = DRV_i2c_clock_set(clock_speed);
    //   PRNTHT("clkSpeed: 0x", clock_speed, CRLF);
    //   return;
    // }

    default:
    {
      PRNS("Invalid command or argument\n");
      GFS(F_COMMERR);
    }
      return;
  }
}
