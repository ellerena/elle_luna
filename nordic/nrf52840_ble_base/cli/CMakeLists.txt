## cli - command line interface component build configuration

file(GLOB allc "*.c")

zephyr_interface_library_named(cli)
#zephyr_include_directories(inc)
zephyr_library_sources(${allc})
