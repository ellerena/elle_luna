/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file cli_ssd1306.c
 * @date 09.12.2021
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#include <mis_gflags.h>

#include <disp_txt_library.h>
#include <SSD1306.h>
#include <oledproc.h>

/************************************ Local Macro Definitions ************************************/
/************************************ Local Type Definitions  ************************************/
/*********************************** Local Constant Definitions **********************************/
/********************************** Global Variable Declarations *********************************/
/********************************** Local Function Declarations **********************************/
/********************************** Local Variable Declarations **********************************/
/********************************** Public Function Declarations *********************************/
void cli_ssd1306 (char task, void *args, int len)
{
  unsigned char * pArgs = (unsigned char*) args;

  switch (task)
  {
    case 'i': /* oi: oled initialization */
    {
      display_Init();
    }
      break;
    case 'c': /* oc<xxyy....: n hex> commands to send to oled */
    {
      while (len--)
      {
        ssd1306_command(*pArgs++);
      }
    }
      break;
    case 't': /* ot<\t><....> : write text string to oled */
    {
      if (pArgs[0] == '\0')
      {
#ifdef MEMALIGNEDREQUIRED
        oled_proc_queue((void*) &pArgs[3]);
//        display_puts((char*) &pArgs[3]);
#else
//        oled_proc_queue((void*) &pArgs[1]);
        display_puts((char*) &pArgs[1]);
#endif
      }
    }
      break;
    case 'p': /* op<x:8><y<:8> : position cursor at lines x, y */
    {
      display_locate(pArgs[0], pArgs[1]);
    }
      break;
    case 'z': /* oz: clear screen */
    {
      display_clear();
    }
      break;
    default:
    {
      GFS(F_COMMERR);
    }
      break;
  }
}

/*********************************** Local Function Definitions **********************************/
