/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file drv_gpio.c
 * @date 05.25.2023
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#include <zephyr/kernel.h>
#include <zephyr/drivers/gpio.h>
#include <assert.h>
#include "drv_gpio.h"

/************************************ Local Macro Definitions ************************************/
#define PIN_HIGH (1)
#define PIN_LOW (0)
#define LED_R_PIN DT_GPIO_PIN(LED_R_NODE, gpios)
#define LED_G_PIN DT_GPIO_PIN(LED_G_NODE, gpios)
#define LED_B_PIN DT_GPIO_PIN(LED_B_NODE, gpios)
#define LED_COLOR_MASK ((1 << LED_B_PIN) + (1 << LED_G_PIN))

/************************************ Local Type Definitions  ************************************/
/*********************************** Local Constant Definitions **********************************/
static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(LED_NODE, gpios);
static const struct gpio_dt_spec imu_int = GPIO_DT_SPEC_GET(DT_ALIAS(imuirq), gpios);
static const struct gpio_dt_spec imu_pwr = GPIO_DT_SPEC_GET(DT_ALIAS(imupwr), gpios);

/********************************** Global Variable Declarations *********************************/
/********************************** Local Function Declarations **********************************/
static void led_toggle_task(void);
static void imu_int_handler(const struct device *dev, struct gpio_callback *cb,
		    uint32_t pins);

/********************************** Local Variable Declarations **********************************/
static struct gpio_callback imu_int_cb_data;

/********************************** Public Function Declarations *********************************/
void drv_gpio_init(void)
{
    assert(device_is_ready(led.port));
    gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);

    gpio_pin_configure(led.port, LED_G_PIN, GPIO_OUTPUT_ACTIVE);

    assert(device_is_ready(imu_pwr.port));
    gpio_pin_configure_dt(&imu_pwr, GPIO_OUTPUT_ACTIVE);

    assert(device_is_ready(imu_int));
    gpio_pin_configure_dt(&imu_int, GPIO_INPUT);
    gpio_pin_interrupt_configure_dt(&imu_int,
					      GPIO_INT_EDGE_TO_ACTIVE);
    gpio_init_callback(&imu_int_cb_data, imu_int_handler, BIT(imu_int.pin));
	gpio_add_callback(imu_int.port, &imu_int_cb_data);

    led_toggle_task();
}

/*********************************** Local Function Definitions **********************************/
static void led_toggle_task(void)
{
    for(;;)
    {
        gpio_port_clear_bits_raw(led.port, LED_COLOR_MASK);
        k_msleep(LED_ON_TIME_MS);

        gpio_port_toggle_bits(led.port, LED_COLOR_MASK);
        k_msleep(LED_OFF_TIME_MS);
    }
}

static void imu_int_handler(const struct device *dev, struct gpio_callback *cb,
		    uint32_t pins)
{
    printk("pressed\n");


}

K_THREAD_DEFINE(led_id, 512, drv_gpio_init, NULL, NULL, NULL, 7, 0, 0);
