/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file drv_i2c.c
 * @date 05.27.2023
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#include "drv_i2c.h"

/************************************ Local Macro Definitions ************************************/
#define I2C_PORT I2C_NUM_0
#define I2C_ADDR_WR(a) ((a << 1) | I2C_MASTER_WRITE)
#define I2C_ADDR_RD(a) ((a << 1) | I2C_MASTER_READ)
#define I2C_WAIT_TICKS (10 / portTICK_RATE_MS)
#define IS_VALID_DEVICE_ADDRESS(x) ((3 < (x)) && (0x78 > (x)))
#define IS_VALID_REG_ADDRESS_LEN(x) (1 == (x) || 2 == (x))

/************************************ Local Type Definitions  ************************************/
typedef struct
{
    uint8_t reg_addr_size:2; // reg address can be 16 bits wide (2) or 8 bits wide (1)
    uint8_t use_stop:1; // <dev_addr_WR><reg_addr><stop(1)/repeat start(0)><dev_addr_RD><data>
    uint8_t unused:5;
} common_params_t;

typedef struct
{
  struct device * i2c_dev;
  common_params_t params;
  uint8_t slv_addr;
} local_ctxt_t;

/*********************************** Local Constant Definitions **********************************/
const struct device *const i2c_dev0 = DEVICE_DT_GET(DT_NODELABEL(i2c0));
const struct device *const i2c_dev1 = DEVICE_DT_GET(DT_NODELABEL(i2c1));

/********************************** Global Variable Declarations *********************************/
local_ctxt_t ctxt = {
  .params = {.reg_addr_size = REG_ADDR_8_BITS, .use_stop = STOP_BEFORE_READ},
  .slv_addr = OLED1_I2C_ADR,
  .i2c_dev  = (struct device *)i2c_dev1,
};

/********************************** Local Function Declarations **********************************/
/********************************** Local Variable Declarations **********************************/
/********************************** Public Function Declarations *********************************/
int DRV_i2c_master_init (void)
{
  ctxt.params.reg_addr_size = REG_ADDR_8_BITS;
  ctxt.params.use_stop      = STOP_BEFORE_READ;
  ctxt.slv_addr             = OLED1_I2C_ADR;

  if (!device_is_ready(i2c_dev0)) {
		return -1;
	}

  if (!device_is_ready(i2c_dev1)) {
		return -1;
	}

  ctxt.i2c_dev = (struct device *)i2c_dev1;

  int ret = i2c_configure(ctxt.i2c_dev, I2C_SPEED_SET(I2C_SPEED_FAST) | I2C_MODE_MASTER);

  return ret;
}

/* sets the slave address for future transfers */
uint8_t DRV_i2c_master_slv_addr (uint8_t addr)
{
  if(IS_VALID_DEVICE_ADDRESS(addr))
  {
    ctxt.slv_addr = addr;
  }

  return ctxt.slv_addr;
}

bool DRV_i2c_master_is_slave_present(uint8_t slave_addr)
{
  /* run only if theaddress is valid as per i2c specs */
  if (!IS_VALID_DEVICE_ADDRESS(slave_addr))
  {
    return false;
  }

  struct i2c_msg header;
  header.buf = &slave_addr;
  header.len = 0u;
  header.flags = I2C_MSG_WRITE | I2C_MSG_STOP;

  int ret = i2c_transfer(ctxt.i2c_dev, &header, 1, slave_addr);

  return (0 == ret);
}

int32_t DRV_i2c_master_tx (const uint8_t * data, uint32_t len)
{
  struct i2c_msg msg = {
    .buf = (uint8_t *)data,
    .len = len,
    .flags = I2C_MSG_WRITE | I2C_MSG_STOP
  };

  int ret = i2c_transfer(ctxt.i2c_dev, &msg, 1, ctxt.slv_addr);

  return 0 == ret ? len : ret;
}

int32_t DRV_i2c_master_rx (uint8_t * data, uint32_t len)
{
  struct i2c_msg msg = {
    .buf = data,
    .len = len,
    .flags = I2C_MSG_READ | I2C_MSG_STOP};

  int ret = i2c_transfer(ctxt.i2c_dev, &msg, 1, ctxt.slv_addr);

  return 0 == ret ? len : ret;
}

int32_t DRV_i2c_master_wr_ar (const uint8_t * data,
                               uint32_t len,
                               int addr,
                               const uint8_t * reg)
{
  struct i2c_msg msg[2] = {
    {
      .buf   = (uint8_t*)reg,
      .len   = ctxt.params.reg_addr_size,
      .flags = I2C_MSG_WRITE,
    },
    {
      .buf   = (uint8_t*) data,
      .len   = len,
      .flags = I2C_MSG_WRITE | I2C_MSG_STOP,
    }
  };

  int ret = i2c_transfer(ctxt.i2c_dev, &msg[0], 2, addr);

  return 0 == ret ? len : ret;
}

int32_t DRV_i2c_master_rd_ar (uint8_t * data,
                               uint32_t len,
                               int addr,
                               const uint8_t * reg)
{
    struct i2c_msg msg[2] = {
    {
      .buf   = (uint8_t*)reg,
      .len   = ctxt.params.reg_addr_size,
      .flags = I2C_MSG_WRITE + (ctxt.params.use_stop ? I2C_MSG_STOP : 0)
    },
    {
      .buf   = data,
      .len   = len,
      .flags = I2C_MSG_READ | I2C_MSG_STOP
    }
  };

  int ret = i2c_transfer(ctxt.i2c_dev, &msg[0], 2, addr);


  return 0 == ret ? len : ret;
}

int DRV_i2c_set_use_stop (int use_stop)
{
  ctxt.params.use_stop = (use_stop & 1);

  return ctxt.params.use_stop;
}

int DRV_i2c_set_reg_address_len (int len)
{
  if(IS_VALID_REG_ADDRESS_LEN(len))
  {
    ctxt.params.reg_addr_size = len;
  }

  return ctxt.params.reg_addr_size;
}

int DRV_i2c_set_controller(int controller)
{
  if (0 == controller || 1 == controller)
  {
    ctxt.i2c_dev = (1 == controller) ?
                   (struct device *)i2c_dev1 :
                   (struct device *)i2c_dev0;
  }

  if (ctxt.i2c_dev == i2c_dev0)
  {
    return 0;
  }
  else if (ctxt.i2c_dev == i2c_dev1)
  {
    return 1;
  }

  return -1;
}

// int DRV_i2c_clock_set (int clk_speed)
// {
//   int result = ESP_OK;

// #ifdef ARCH_ESP32
//   int clk_ticks = (I2C_APB_CLK_FREQ / clk_speed);

//   int clk_ticks_high =
//       (clk_speed >= 300000) ? (clk_ticks * 13 / 50) : (clk_ticks >> 1);

//   int clk_ticks_low = clk_ticks - clk_ticks_high;

//   i2c_set_period(I2C_PORT, clk_ticks_high, clk_ticks_low);
//   i2c_get_period(I2C_PORT, &clk_ticks_high, &clk_ticks_low);

//   clk_ticks = clk_ticks_high + clk_ticks_low;
//   result = I2C_APB_CLK_FREQ / clk_ticks;
// #endif

//   return result;
// }

/*********************************** Local Function Definitions **********************************/
