/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file drv_i2c.h
 * @date 09.10.2021
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#pragma once

#if __cplusplus
extern "C" {
#endif

/***************************************************************************************************
 * Includes
 **************************************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/i2c.h>

/***************************************************************************************************
 * Macro Definitions
 **************************************************************************************************/
/*
 * The slave address of some peripherals.
 */
#define OLED_MELIFE       (0x3c)
#define OLED_BLUE         (0x3d)

#define OLED1_I2C_ADR      OLED_MELIFE
#define I2C_FREQ           (465100)    /* i2c SCL frequency - desired 400kHz
                                          but due to esp error we use a
                                          higher value 440kHz */
#define I2C_BUFSZ          (128)       /* i2c buffer size */

#define I2C_SCL_PIN (5)
#define I2C_SDA_PIN (4)

#define REG_ADDR_16_BITS (2)
#define REG_ADDR_8_BITS (1)
#define STOP_BEFORE_READ (1)
#define NO_STOP_BEFORE_READ (0)

/***************************************************************************************************
 * Type Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Constant Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Variable Declarations
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Function Prototypes
 **************************************************************************************************/
/**
 * @brief initializes the i2c master controller
 *
 * @return 0 on success, error code otherwise
 */
extern int DRV_i2c_master_init (void);

/**
 * @brief i2c write raw data to default slave address
 *  ===============================================================
 * | ST | dst_addr_w < ack | dat[0] < ack ... dat[n-1] < ack | STP |
 *  ===============================================================
 *
 * @param data, data to send
 * @param len, data length
 *
 * @return number of bytes sent (<len> on sucess)
 */
extern int32_t DRV_i2c_master_tx (const uint8_t * data, uint32_t len);

/**
 * @brief i2c read raw data from default slave address
 *  ===============================================================
 * | ST | dst_addr_r < ack | dat[0] > ack ... dat[n-1] > ack | STP |
 *  ===============================================================
 *
 * @param data, data buffer to receive
 * @param len, data length
 *
 * @return number of bytes received (<len> on sucess)
 */
extern int32_t DRV_i2c_master_rx (uint8_t * data, uint32_t len);

/**
 * @brief i2c write data to the slave address starting
 *          at the specified register address
 *  ===================================================================
 * | ST | addr_w < ack | reg < ack | d[0] < ack ... d[n-1] < ack | STP |
 *  ===================================================================
 *
 * @param data, data to send
 * @param len, data length
 * @param addr, slave address (7 bit)
 * @param reg, starting register address
 *
 * @return number of bytes sent (<len> on sucess)
 */
extern int32_t DRV_i2c_master_wr_ar (const uint8_t * data,
                                      uint32_t len,
                                      int addr,
                                      const uint8_t * reg);

/**
 * @brief i2c read data from the slave address starting
 *          at the specified register address
 *  ===================================================================
 * | ST | addr_r < ack | reg < ack | d[0] > ack ... d[n-1] > ack | STP |
 *  ===================================================================
 *
 * @param data, data to send
 * @param len, data length
 * @param addr, slave address (7 bit)
 * @param reg, starting register address
 *
 * @return number of bytes sent (<len> on sucess)
 */
extern int32_t DRV_i2c_master_rd_ar (uint8_t * data,
                                      uint32_t len,
                                      int addr,
                                      const uint8_t * reg);

extern uint8_t DRV_i2c_master_slv_addr (uint8_t addr);
extern bool DRV_i2c_master_is_slave_present(uint8_t slave_addr);
extern int DRV_i2c_set_use_stop (int use_stop);
extern int DRV_i2c_set_reg_address_len (int len);
extern int DRV_i2c_set_controller(int controller);

extern int DRV_i2c_clock_set (int clk_speed);

#if __cplusplus
}
#endif
