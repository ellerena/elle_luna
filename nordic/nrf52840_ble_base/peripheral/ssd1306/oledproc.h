/*
 * oledproc.h
 *
 *  Created on: Oct 15, 2021
 *      Author: ellerena
 */

#ifndef USER_APP_INC_OLEDPROC_H_
#define USER_APP_INC_OLEDPROC_H_

#ifdef __cplusplus
extern "C"
{
#endif

int oled_proc_start (void);
void oled_proc_end (void);
int oled_proc_queue (void * request);

#ifdef __cplusplus
}
#endif

#endif /* USER_APP_INC_OLEDPROC_H_ */
