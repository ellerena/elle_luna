/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file lsm6dsxx_platform.h
 * @date 11.1.2021
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#pragma once

#if __cplusplus
extern "C" {
#endif

/***************************************************************************************************
 * Includes
 **************************************************************************************************/
#include "lsm6dsxx.h"

/***************************************************************************************************
 * Macro Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Type Definitions
 **************************************************************************************************/
typedef enum
{
    d6d = 0,
    mlc,
    rawdat,
    tilt,
} tasks_e;

/***************************************************************************************************
 * Constant Definitions
 **************************************************************************************************/

/***************************************************************************************************
 * Exported Variable Declarations
 **************************************************************************************************/
extern stmdev_ctx_t dev_ctx;

/***************************************************************************************************
 * Exported Function Prototypes
 **************************************************************************************************/
/*
 * @brief  Write generic device register (platform dependent)
 *
 * @param  handle    customizable argument. In this examples is used in
 *                   order to select the correct sensor bus handler.
 * @param  reg       register to write
 * @param  bufp      pointer to data to write in register reg
 * @param  len       number of consecutive register to write
 *
 */
extern int32_t platform_write (void * handle,
                               uint8_t reg,
                               const uint8_t * bufp,
                               uint16_t len);

/*
 * @brief  Read generic device register (platform dependent)
 *
 * @param  handle    customizable argument. In this examples is used in
 *                   order to select the correct sensor bus handler.
 * @param  reg       register to read
 * @param  bufp      pointer to buffer that store the data read
 * @param  len       number of consecutive register to read
 *
 */
extern int32_t platform_read (void * handle,
                              uint8_t reg,
                              uint8_t * bufp,
                              uint16_t len);

/*
 * @brief  platform specific delay (platform dependent)
 *
 * @param  ms, delay in ms
 *
 */
extern void platform_delay (uint32_t ms);

/*
 * @brief  platform specific initialization (platform dependent)
 */
extern void platform_init (void);

extern void platform_puts (const char * str);
extern void platform_prntht (const char * str1,
                              uint32_t val,
                              const char * str2);
extern int lsm6dsxx_proc_start (int proc);
extern int lsm6dsxx_tap_proc_start (void);
extern void proc_i2c_tst (int loops);

#if __cplusplus
}
#endif
