/*
 * lsm66dsxx_6d.c
 *
 *  Created on: Sep 28, 2021
 *      Author: ellerena
 */

/* Includes ------------------------------------------------------------------*/
#include "string.h"
#include "mis_logger.h" // for CRLF
#include "mis_gflags.h"
#include "lsm6dsxx_platform.h"

#define proc_setup proc_setup_6d
#define proc_check proc_check_6d

struct
{
  int16_t temp;
  int16_t gyr[3];
  int16_t acc[3];
  uint32_t timestamp;
} raw;

static const conf_data_t cnf_data[] =
  {
    { SENUPP(CTRL9_XL), 0xe2 }, // Disable I3C interface
      { SENUPP(I3C_BUS_AVB), 0x00 },

    /* Set XL ODR, FS and LPF2 */
      { SENUPP(CTRL1_XL), 0x60 }, // XL ODR=416Hz, FS=2g, 2nd stage LPF2

    /* Set interrupt as latched and clear on read */
      { SENUPP(TAP_CFG0), 0x41 }, // Enable Block Data Update + Reg. auto inc

    /* ... Also set threshold in degrees. */
      { SENUPP(TAP_THS_6D), 0x60 }, // 0x60: 50 degrees

    /* LPF2 on 6D/4D function selection. */
      { SENUPP(CTRL8_XL), 0x01 }, // low_pass_on_6d

    /* Interrupt generation on INT1 pin */
      { SENUPP(TAP_CFG2), 0x80 }, // INTERRUPTS_ENABLE
      { SENUPP(MD1_CFG), 0x04 }, // NT1_6D
  };

/* Private variables ---------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/**
 * @brief Setup the IMU module for our use.
 * */
void proc_setup_6d (void)
{
  /* Init test platform */
  platform_init();
  platform_puts("Init complete" CRLF);
  platform_delay(LSM6_BOOT_TIME_MS); // wait sensor boot time
  platform_puts("Boot complete" CRLF);

  /* Check device ID */
  uint8_t regval;
  platform_read(NULL, SEN_ID_REG, &regval, 1);
  platform_prntht("I'm ", regval, CRLF);
  if (regval != SEN_ID)
    return;

  /* perform a SW reset of the sensor */
  regval = 1; // reset bit
  platform_write(NULL, SENUPP(CTRL3_C), &regval, 1);
  do
  {
    platform_read(NULL, SENUPP(CTRL3_C), &regval, 1);
  }
  while (regval & 1);

  /* configure IMU module via i2c, write registers */
  const conf_data_t *pval = cnf_data;
  int len = sizeof(cnf_data) / sizeof(conf_data_t);

  while (len--)
  {
    platform_write(NULL, pval->address, (uint8_t*) &pval->data, 1);
    pval++;
  }
}

/**
 * @brief This function is called when a tilt is detected
 *        following and irqB (int1) tilt detection event
 *        It reads back gyro and accel from imu
 *        and checks if the tilt detected flag
 *        is active.
 * */
/* Check if Tilt event */
void proc_check (void)
{
#define REGSSZ 6
#define MG(x) ((((int)(x))*200)/32768)

  // read the new register values for 6d source and accel. values
  uint8_t d6d;
  platform_read(NULL, SENUPP(D6D_SRC), &d6d, 1);
  platform_read(NULL,
                SENUPP(OUTX_L_A),
                (uint8_t*) &raw.acc,
                3 * sizeof(int16_t));

  // convert raw data to mg values
  int val[3];
  val[0] = MG(raw.acc[0]);
  val[1] = MG(raw.acc[1]);
  val[2] = MG(raw.acc[2]);

  char msg[30] =
    { '6' };
  if (FLB(d6d, 6))
  {
    if (FLB(d6d, 0))
    {
      msg[1] = 'x';
    }
    if (FLB(d6d, 1))
    {
      msg[1] = 'X';
    }
    if (FLB(d6d, 2))
    {
      msg[1] = 'y';
    }
    if (FLB(d6d, 3))
    {
      msg[1] = 'Y';
    }
    if (FLB(d6d, 4))
    {
      msg[1] = 'z';
    }
    if (FLB(d6d, 5))
    {
      msg[1] = 'Z';
    }

    sprintf(msg + 2, "%02x %3d %3d %3d" CRLF, d6d, val[0], val[1], val[2]);
    platform_puts(msg);
  }
  else
  {
    int v = 0x6d6d0000 | d6d;
    platform_prntht("irq missed x", v, "h" CRLF);
  }
}
