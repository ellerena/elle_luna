/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file lsm6dsxx_raw.c
 * @date 09.21.2021
 * @brief
 *
 * @details
 *
 **************************************************************************************************/
#include "string.h"
#include "lsm6dsxx_platform.h"
#include "mis_logger.h"
#include "dataout.h"

/************************************ Local Macro Definitions ************************************/
#define proc_setup proc_setup_raw
#define proc_check proc_check_raw

#define WTM_LEV (2) //((((int)SAMPLES_QTY/SAMPLES_IN_PACKET)*FIFO_WORDS_IN_PACKET) +
                //  ((SAMPLES_QTY%SAMPLES_IN_PACKET)*FIFO_WORDS_IN_SUBPACKET))

#define WTM_LEV_L ((WTM_LEV) & 0xff)
#define WTM_LEV_M (((WTM_LEV) >> 8) & 0xff)

#define SECS_TO_SAMPLES(sec, odr) ((odr) * (sec))
#define SAMPLES_TO_FIFO_WORDS(samples) (3 * (samples))
#define FIFO_WORDS_TO_BYTES(fifo_words) (7 * (fifo_words))

#define SAMPLES_IN_PACKET (32)
#define FIFO_WORDS_IN_PACKET (SAMPLES_IN_PACKET /* gyr */ + \
                              SAMPLES_IN_PACKET /* acc */ + \
                              1 /* ts */) // 65
#define FIFO_WORDS_IN_SUBPACKET (1 /* gyr */ + \
                                 1 /* acc */)
#define PACKET_PERIOD (SAMPLES_IN_PACKET/ODR_HZ)

#define ACC_RUNNING_SIZE (2*4) // size of the running sampling array
#define ACC_BLIND_SAMPLE_COUNT (12) // samples to ignore following a true event detected
#define ACC_THRESHOLD_DELTA (2000) // threshold level at which sample processing starts
#define FIFO_WORD_SIZE_BYTES (7)
#define FIFO_WORD_PER_SAMPLE (3)
#define ODR_HZ (26)
#define SAMPLES_LOG_PERIOD_MS (3000)
#define SAMPLES_QTY SECS_TO_SAMPLES( SAMPLES_LOG_PERIOD_MS/1000, ODR_HZ) // 78
#define SAMPLES_FIFO_WORDS SAMPLES_TO_FIFO_WORDS(SAMPLES_QTY) // 234
#define SAMPLES_SIZE_BYTES FIFO_WORDS_TO_BYTES(SAMPLES_FIFO_WORDS) // 1638
#define SAMPLES_FOR_CALIBRATION (12) // how may initial samples will be used for calibration

#define MG(x) ((((int)(x))*2000)/32768) // 16bits for a FS=+/-2g mapped to +/-1000 TIMESTAMP0
#define TAG_CYCLE(x) (3&((x)>>1))
#define TAG "LSM6"

/************************************ Local Type Definitions  ************************************/
typedef enum
{
  IDLE = 0,
  CALIBRATE,
  SEARCH,
  BLIND,
  SAMPLE,
} state_machine_t;

typedef enum
{
  AXIS_X = 0,
  AXIS_Y,
  AXIS_Z,
  NUM_AXIS
} axis_num_t;

typedef struct __attribute__((packed))
{
  uint8_t tag;
  union data_u
  {
    int16_t val16[3];
    uint8_t val8[6];
  } data;
} fifo_word_t;

typedef struct __attribute__((packed))
{
  // fifo word 1: gyroscope data
  uint8_t tag_gyr; // gyro tag sequence: 09h, 0ah, 0ch, 0fh
  int16_t gyr[3]; // gyro values [x, y, z]

  // fifo word 2: accelerometer data
  uint8_t tag_acc; // accel tag sequence: 11h, 12h, 14h, 17h
  int16_t acc[3]; // accel values [x, y, z]
} fifo_sample_sub_t;

typedef struct __attribute__((packed))
{
  // fifo word 0: time stamp data
  uint8_t tag_ts; // timestamp tag sequence: 21h, 22h, 24h, 27h
  uint32_t timestamp; // 32bit timestamp
  uint16_t unused0;

  // sub data: gyro + accelerometer
  fifo_sample_sub_t subdata;
} fifo_sample_t;

typedef struct
{
  int32_t sum[NUM_AXIS];
  int16_t offset[NUM_AXIS];
  int16_t threshold[NUM_AXIS];
  uint8_t samples;
} sensor_calibration_values_t ;


typedef struct
{
  int32_t running_sample[ACC_RUNNING_SIZE];
  sensor_calibration_values_t calibrate;
  uint8_t running_offset;
} acc_params_t;

/*********************************** Local Constant Definitions **********************************/
/**
 * the fifo holds up to 3kB or 512 fifo words (1 fifo word = 7 bytes).
 * we'll set a watermark at a lower value to avoid over-running.
 * data packet is composed of:
 * 1st 3 7byte words: TS, GY, ACC.
 * Next 31 2x7byte words contain only: GY and ACC data.
 * So a complete 'batch packet' contains one timestamp + 32 samples
 * of GY and ACC.
 * A full fifo data set (32 samples) is then:
 * <1 fifo_sample_t:21> <31 fifo_sample_sub_t:14> = <21> + <14*31> = 195 bytes
 * using a fast i2c transfer (400kHz) this would take aprox. 31.24ms
 * using a sensor sampling of 26Hz (38.46ms) means we can transfer a full
 * fifo data set from the sensor to MCU in aprox. 1 sample time.
 */
static const conf_data_t cnf_data[] =
  {
    { SENUPP(CTRL9_XL), 0xe2 }, // Disable I3C interface
      { SENUPP(I3C_BUS_AVB), 0x00 },
      { SENUPP(CTRL10_C), 0x20 }, // Enable timestamp counter
      { SENUPP(CTRL1_XL), 0x22 }, // XL ODR=26Hz, FS=2g, 2nd stage LPF2
      { SENUPP(CTRL2_G), 0x2c }, // Gyro ODR=26Hz, FS=2000dps
      { SENUPP(CTRL8_XL), 0x80 }, // Accelerometer LP filter, ODR div/100

    /* set watermark threshold, this covers 2 registers.
     * for now we don't use compression so 2nd register is mostly
     * unused */
      { SENUPP(FIFO_CTRL1), WTM_LEV_L }, // LSB of watermark
      { SENUPP(FIFO_CTRL2), WTM_LEV_M }, // MSB of watermark

    /* set fifo sample writing rate for acc and gyro */
      { SENUPP(FIFO_CTRL3), 0x02 }, /* 26Hz for ACC, no Gyro */

    /* set timestamp batch data rate and fifo mode== */
      { SENUPP(FIFO_CTRL4), 0x00 }, /* clear fifo */
      { SENUPP(FIFO_CTRL4), 0x46 }, /* ts each sample, fifo continuous */

    /* enable irq on reaching watermark */
      { SENUPP(INT1_CTRL), 0x08 } /* INT1_FIFO_TH */
  };

/********************************** Global Variable Declarations *********************************/

/********************************** Local Variable Declarations **********************************/
static state_machine_t state_machine = CALIBRATE;
static int16_t blind_sample_counter = 0;
static uint8_t buffer[2048] = {0};
static acc_params_t acc_params = {0};

/********************************** Local Function Declarations **********************************/
static void calibrate_running_parameters(int16_t * sensor_val);
static bool true_event_is_detected(void);
static void sample_storing_and_classification(int16_t * sensor_val, axis_num_t axis);
static bool is_first_valid_sample(int16_t * sensor_val, axis_num_t axis);

/********************************** Public Function Declarations *********************************/
/**
 * @brief Setup the IMU module for our use.
 * */
void proc_setup (void)
{
  state_machine = IDLE;
  /* Init test platform */
  platform_init();
  PRNS(TAG, "Init complete");
  platform_delay(LSM6_BOOT_TIME_MS); // wait sensor boot time
  PRNS(TAG, "Boot complete");

  /* Check device ID */
  uint8_t regval;
  platform_read(NULL, SEN_ID_REG, &regval, 1);
  PRNTHT(TAG " I'm 0x", regval, "\n");
  if (regval != SEN_ID)
    return;

  /* perform a SW reset of the sensor */
  regval = 1; // reset bit
  platform_write(NULL, SENUPP(CTRL3_C), &regval, 1);
  do
  {
    platform_read(NULL, SENUPP(CTRL3_C), &regval, 1);
  }
  while (regval & 1);

  /* configure IMU module via i2c, write registers */
  const conf_data_t *pval = cnf_data;
  int len = sizeof(cnf_data) / sizeof(conf_data_t);

  while (len--)
  {
    platform_write(NULL, pval->address, (uint8_t*) &pval->data, 1);
    pval++;
  }

  platform_delay(500);

  // initialize the calibratrion samples counter
  memset(&acc_params, 0, sizeof acc_params);
  state_machine = CALIBRATE;
}

/**
 * @brief This function is called when a event is detected
 */
void proc_check (void)
{
  // read the status regs
  uint16_t sta;
  platform_read(NULL, SENUPP(FIFO_STATUS1), (uint8_t*) &sta, 2);

  // reject any intermittent requests that may occur as we pull the data in
  uint16_t len = sta & 0x3ff;
  if (len < WTM_LEV)
  {
    PRNTHT(TAG " unexpected: 0x", sta, "\n");
    return;
  }

  /* pull all the samples at once */
  platform_read(NULL, SENUPP(FIFO_DATA_OUT_TAG), buffer, len * FIFO_WORD_SIZE_BYTES);

  fifo_word_t *pbuffer = (fifo_word_t*) buffer;

  switch (state_machine)
  {
    case CALIBRATE:
    {
      /* if the process is starting, the first SAMPLES_FOR_CALIBRATION samples are only
      used for calibration purposes */
      calibrate_running_parameters(pbuffer[1].data.val16);
      if (acc_params.calibrate.samples >= SAMPLES_FOR_CALIBRATION)
      {
        for (int i = 0; i < ACC_RUNNING_SIZE; ++i)
        {
          acc_params.running_sample[i] = acc_params.calibrate.offset[AXIS_Y];
        }

        PRNF(TAG " samples: %d, offset: %d",
                  acc_params.calibrate.samples,
                  acc_params.calibrate.offset[AXIS_Y]);

        state_machine = SEARCH;
      }
    }
      return;

    case SEARCH:
    {
      if (is_first_valid_sample(pbuffer[1].data.val16, AXIS_Y))
      {
        state_machine = SAMPLE;
      }
    }
      return;

    case BLIND:
    {
      /* if a true event has been recently detected, ignore for a reasonable time
      the new samples as a mean to avoid false positives due to bouncing */
      if (--blind_sample_counter <= 0)
      {
        for (int i = 0; i < ACC_RUNNING_SIZE; ++i)
        {
          acc_params.running_sample[i] = acc_params.calibrate.offset[AXIS_Y];
        }
        state_machine = SEARCH;
      }
    }
      return;

    case SAMPLE:
    {
      sample_storing_and_classification(pbuffer[1].data.val16, AXIS_Y);

      if (!true_event_is_detected())
      {
        return;
      }
    }
      break;

    default:
      return;
  }

  // landing here we've got a true event, let's celebrate
  dataout_proc_queue(pbuffer, FIFO_WORD_SIZE_BYTES * WTM_LEV, F_DOUT_BLE);

  SENLOW(fifo_data_out_tag_t) * fifo_tag = (SENLOW(fifo_data_out_tag_t) *)pbuffer;
  if (SENUPP(TIMESTAMP_TAG) == fifo_tag->tag_sensor)
  {
    uint32_t timestamp;
    memcpy(&timestamp, &pbuffer->data.val16[0], sizeof timestamp);

    PRNF(TAG " %10u00 %02x %02x %6d %6d %6d",
              timestamp/4, // timestamp in usec
              pbuffer[0].tag,
              pbuffer[1].tag,
              pbuffer[1].data.val16[AXIS_X],
              pbuffer[1].data.val16[AXIS_Y],
              pbuffer[1].data.val16[AXIS_Z]
              );
  }

  // and restart a new whole detection cycle
  blind_sample_counter = ACC_BLIND_SAMPLE_COUNT;
  state_machine = BLIND;
}

/*********************************** Local Function Definitions **********************************/
static void calibrate_running_parameters(int16_t * sensor_val)
{
  acc_params.calibrate.samples++;

  for (int i = 0; i < NUM_AXIS; ++i)
  {
    acc_params.calibrate.sum[i] += sensor_val[i];
    acc_params.calibrate.offset[i] = (int16_t)(acc_params.calibrate.sum[i]/acc_params.calibrate.samples);
  }
}

static void sample_storing_and_classification(int16_t * sensor_val, axis_num_t axis)
{
  acc_params.running_sample[acc_params.running_offset++] = sensor_val[axis];
  if (acc_params.running_offset >= ACC_RUNNING_SIZE)
  {
    acc_params.running_offset = 0;
  }
}

static bool true_event_is_detected(void)
{
  int i = 0;
  int j = acc_params.running_offset;

  do
  {
    if (acc_params.running_sample[j++] < acc_params.calibrate.offset[AXIS_Y])
    {
      return false;
    }
    if (j >= ACC_RUNNING_SIZE)
    {
      j = 0;
    }

  } while(++i < (ACC_RUNNING_SIZE/2));

  do
  {
    if (acc_params.running_sample[j++] >= acc_params.calibrate.offset[AXIS_Y])
    {
      return false;
    }
    if (j >= ACC_RUNNING_SIZE)
    {
      j = 0;
    }
  } while(++i < (ACC_RUNNING_SIZE/2));

  return true;
}

static bool is_first_valid_sample(int16_t * sensor_val, axis_num_t axis)
{
  return sensor_val[axis] > (acc_params.calibrate.offset[axis] + ACC_THRESHOLD_DELTA);
}
