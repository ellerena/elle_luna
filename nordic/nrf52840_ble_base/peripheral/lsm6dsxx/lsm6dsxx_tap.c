/*
 * lsm6dsxx_tap.c
 *
 *  Created on: Sep 28, 2021
 *      Author: ellerena
 */

/* Includes ------------------------------------------------------------------*/
#include "string.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"
#include "esp_err.h"

#include "drv_gpio.h"
#include "mis_logger.h"
#include "mis_gflags.h"
#include "dataout.h"
#include "lsm6dsxx_platform.h"

static const conf_data_t cnf_data[] =
  {
    { SENUPP(CTRL9_XL), 0xe2 }, // Disable I3C interface
      { SENUPP(I3C_BUS_AVB), 0x00 },
      { SENUPP(CTRL1_XL), 0x50 }, // XL 0x60 ODR=416Hz, FS=2g, 1st stage LPF2

    /* latched interrupt w/ clear on read and enable tap detect */
      { SENUPP(TAP_CFG0), 0x0e }, // 0x4f INT_CLR_ON_READ + LIR + TAP_X/Y/Z_EN

    /* Set Tap threshold to 01000b, 500 mg (= 8 * FS_XL / 32 ) */
      { SENUPP(TAP_CFG1), 0x08 }, // tap_ths_x = 8
      { SENUPP(TAP_CFG2), 0x88 }, // tap_ths_y = 8, INTERRUPTS_ENABLE = 1

    /* ... Also set threshold in degrees. */
      { SENUPP(TAP_THS_6D), 0x08 }, // 0x60: 50 degrees // tap_ths_z = 8

    /* tap parameters
     * set DUR @ INT_DUR2 to 0111b, Duration = 538.5ms (7 * 32 * ODR_XL)
     * set SHOCK @ INT_DUR2 to 11b, Shock = 57.36ms (3 * 8 * ODR_XL)
     * set QUIET @ INT_DUR2 to 11b, Quiet = 28.68ms (3 * 4 * ODR_XL)
     */
      { SENUPP(INT_DUR2), 0x06 }, // 0x4f dur = 0x7, quiet = 0x3, shock = 0x3

    /* select single only or single+double tap detection */
      { SENUPP(WAKE_UP_THS), 0x00 }, // 0x80 bit7: SINGLE_DOUBLE_TAP

    /* Interrupt generation on INT1 pin */
      { SENUPP(MD1_CFG), 0x40 }, // 0x48 INT1_SINGLE_TAP + INT1_DOUBLE_TAP + INT1_6D
  };

struct
{
  int16_t temp;
  int16_t gyr[3];
  int16_t acc[3];
  uint32_t timestamp;
} raw;

typedef struct
{
  TimerHandle_t timer;
  uint8_t reg;
  uint8_t count;
} tap_t;

static tap_t tap =
  { .timer = NULL, .count = 0 };

static SemaphoreHandle_t xSemaphore = NULL;
static TaskHandle_t xTask = NULL;

static void tap_tmr_handler (TimerHandle_t htimer);


/* Private variables ---------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/**
 * @brief Setup the IMU module for our use.
 * */
static void proc_setup (void)
{
  /* Init test platform */
  platform_init();
  dataout_proc_queue("Init complete" CRLF, 0, F_DOUT_TXT + F_DOUT_PRN);
  platform_delay(LSM6_BOOT_TIME_MS); // wait sensor boot time
  dataout_proc_queue("Boot complete" CRLF, 0, F_DOUT_TXT + F_DOUT_PRN);

  /* Check device ID */
  uint8_t regval;
  platform_read(NULL, SEN_ID_REG, &regval, 1);
  PRNTHT("I'm ", regval, CRLF);
  if (regval != SEN_ID)
    return;

  // perform a SW reset of the sensor
  regval = 1; // reset bit
  platform_write(NULL, SENUPP(CTRL3_C), &regval, 1);
  do
  {
    platform_read(NULL, SENUPP(CTRL3_C), &regval, 1);
  }
  while (regval & 1);

  /* configure IMU module via i2c, write registers */
  const conf_data_t *pval = cnf_data;
  int len = sizeof(cnf_data) / sizeof(conf_data_t);

  while (len--)
  {
    platform_write(NULL, pval->address, (uint8_t*) &pval->data, 1);
    pval++;
  }
}

static void tap_timer_run (void)
{
  uint8_t tap_cnt = tap.count;
  tap.count = tap_cnt + 1;

  if (0 == tap_cnt)
  {
    xTimerStart(tap.timer, 0); // start timer for multi-tap detection
  }
}

/**
 * @brief this function checks the event and acts accordingly.
 */
static void proc_check (void)
{
  // read the status register
  uint8_t tap_src;
  platform_read(NULL, SENUPP(TAP_SRC), &tap_src, 1);

  if (FLB(tap_src, 5)) // single, double tap
  {
    tap_timer_run();
    tap.reg = tap_src; // store last received tap_src
  }
}

/* -------- task process -------- */
static void six_isr_handler (void * arg)
{
  int xHigherPriorityTaskWoken = pdFALSE;

  xSemaphoreGiveFromISR(xSemaphore, &xHigherPriorityTaskWoken);
}

static void six_task_handler (void * arg)
{
  for (;;)
  {
    if ( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE)
    {
      proc_check();
    }
  }
}

static void tap_tmr_handler (TimerHandle_t htimer)
{
  uint8_t tap_cnt = tap.count;
  tap.count = 0;

  char msg[23] =
    { 'T', 'p', ' ', ' ' };

  if (0 < tap_cnt)
  {
    sprintf(msg + 3, "%02x %02x" CRLF, tap_cnt, tap.reg);
  }
  else
  {
    sprintf(msg + 3, "Err %02x %02x", tap_cnt, tap.reg);
  }

  dataout_proc_queue(msg, 0, F_DOUT_TXT + F_DOUT_PRN);
}

/**
 * @brief external API to enable/disable the event detection process
 */
int lsm6dsxx_tap_proc_start (void)
{
  /* if task exist, then end the whole process */
  if (xTask)
  {
    gpio_isr_handler_remove(POSEDGE_PIN_NUM);
    gpio_uninstall_isr_service();
    vTaskDelete(xTask);
    xTask = NULL;
    xTimerStop(tap.timer, 0);
    xTimerDelete(tap.timer, 0);
    tap.timer = NULL;
    dataout_proc_queue("tap MLC process ended" CRLF,
                       0,
                       F_DOUT_TXT + F_DOUT_PRN);
    return 0;
  }

  /* setup/configure the process */
  proc_setup();

  if (NULL == xSemaphore)
  {
    xSemaphore = xSemaphoreCreateBinary();
  }

  int result = ESP_FAIL;
  if (NULL != xSemaphore)
  {
    result = xTaskCreate(six_task_handler,
                         "six_task_handler",
                         1024,
                         NULL,
                         10,
                         &xTask);

    if (pdPASS == result)
    {
      //install gpio isr service
      gpio_install_isr_service(0);
      result = gpio_isr_handler_add(POSEDGE_PIN_NUM,
                                    six_isr_handler,
                                    (void*) NULL);
    }

    if (ESP_OK == result)
    {
      tap.timer = xTimerCreate("tap_timer",
                               pdMS_TO_TICKS(500),
                               pdFALSE,
                               (void*) 1,
                               tap_tmr_handler);
      if (NULL == tap.timer)
      {
        result = ESP_FAIL;
      }
    }
  }

  PRNTHT("tap process start ", result, "\n");
  return result;
}
