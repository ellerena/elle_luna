/*
 * lsm6dsxx_tilt.c
 *
 *  Created on: Sep 28, 2021
 *      Author: ellerena
 */

/* ATTENTION: By default the driver is little endian. If you need switch
 *            to big endian please see "Endianness definitions" in the
 *            header file of the driver (_reg.h).
 */

/* Includes ------------------------------------------------------------------*/
#include "string.h"
#include "mis_logger.h"
#include "mis_gflags.h"
#include "lsm6dsxx_platform.h"

#define proc_setup proc_setup_tilt
#define proc_check proc_check_tilt

struct
{
  int16_t temp;
  int16_t gyr[3];
  int16_t acc[3];
  uint32_t timestamp;
} raw;

static const conf_data_t cnf_data[] =
  {
    { SENUPP(CTRL9_XL), 0xe2 }, // Disable I3C interface
      { SENUPP(I3C_BUS_AVB), 0x00 },

    /* enable timestamp counter */
      { SENUPP(CTRL10_C), 0x20 }, // Enable timestamp counter

    /* Set XL Output Data Rate: The tilt function requires ODR >= 26 Hz */
      { SENUPP(CTRL1_XL), 0x22 }, // XL ODR=26Hz, FS=2g, 2nd stage LPF2

    /* Enable Tilt in embedded function. */
      { SENUPP(FUNC_CFG_ACCESS), 0x80 }, // select embedded function register map
      { SENUPP(EMB_FUNC_EN_A), 0x10 }, // tilt enable
      { SENUPP(EMB_FUNC_INT1), 0x10 }, // Routing of tilt event on INT1
      { SENUPP(PAGE_RW), 0x80 }, // Enable latched mode (lir) for embedded functions
      { SENUPP(FUNC_CFG_ACCESS), 0x00 }, // de-select embedded function register map

    /* Interrupt generation on Tilt INT1 pin */
      { SENUPP(MD1_CFG), 0x02 }, // Routing of embedded functions event on INT1
  };

/* Private variables ---------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/**
 * @brief Setup the IMU module for our use.
 *         - configure required registers
 * */
void proc_setup (void)
{
  /* Init test platform */
  platform_init();
  platform_puts("Init complete" CRLF);
  platform_delay(LSM6_BOOT_TIME_MS); // wait sensor boot time
  platform_puts("Boot complete" CRLF);

  /* Check device ID */
  uint8_t regval;
  SENLOW(read_reg)(&dev_ctx, SEN_ID_REG, &regval, 1);
  platform_prntht("I'm ", regval, CRLF);
  if (regval != SEN_ID)
    return;

  /* perform a SW reset of the sensor */
  SENLOW(reset_set)(&dev_ctx, PROPERTY_ENABLE);
  do
  {
    SENLOW(reset_get)(&dev_ctx, &regval);
  }
  while (regval);

  /* configure IMU module via i2c, write registers */
  const conf_data_t *pval = cnf_data;
  int len = sizeof(cnf_data) / sizeof(conf_data_t);

  while (len--)
  {
    platform_write(NULL, pval->address, (uint8_t*) &pval->data, 1);
    pval++;
  }
}

/**
 * @brief This function is called when a tilt is detected
 *        following and irqB (int1) detection event
 *        It reads back gyro and accel from imu
 *        and checks any event flag activated.
 * */
void proc_check (void)
{
#define REGSSZ 14
#define MG(x) ((((int)(x))*200)/32768)

  uint8_t emb_func_st;
  uint8_t regval = 0x80; // select embedded function register map
  platform_write(NULL, SENUPP(FUNC_CFG_ACCESS), &regval, 1);
  platform_read(NULL, SENUPP(EMB_FUNC_STATUS), &emb_func_st, 1);
  regval = 0x00; // de-select embedded function register map
  platform_write(NULL, SENUPP(FUNC_CFG_ACCESS), &regval, 1);

  if (FLB(emb_func_st, 4))
  {
    /* read back data from the sensor */
    platform_read(NULL, SENUPP(OUT_TEMP_L), (uint8_t*) &raw, REGSSZ);

    // data processing/conversion
    int val[3];
    val[0] = MG(raw.acc[0]);
    val[1] = MG(raw.acc[1]);
    val[2] = MG(raw.acc[2]);

    // present data
    char msg[23];
    sprintf(msg, "t %4d %4d %4d" CRLF, val[0], val[1], val[2]);
    platform_puts(msg);
  }
  else
  {
    platform_puts("tilt missed" CRLF);
  }
}
