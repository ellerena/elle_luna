/*
 * Copyright Eddie Llerena.
 *
 * NOTICE OF CONFIDENTIAL AND PROPRIETARY INFORMATION & TECHNOLOGY:
 * The information and technology contained herein (including the accompanying binary code)
 * is the confidential information of Eddie Llerena. It is protected by applicable copyright
 * and trade secret law, and may be claimed in one or more U.S. or foreign patents or pending
 * patent applications. Eddie Llerena retains all right, title and interest (including all
 * intellectual property rights) in such information and technology, and no licenses are
 * hereby granted by Meta. Unauthorized use, reproduction, or dissemination is a violation
 * of Eddie Llerena's rights and is strictly prohibited.
 */

/***************************************************************************************************
 *
 * @file main.c
 * @date 05.25.2023
 * @brief
 *
 * @details
 *
 **************************************************************************************************/

#include <zephyr/kernel.h>
#include "drv_gpio.h"

/************************************ Local Macro Definitions ************************************/
#define BUILD_DATETIME __DATE__ " " __TIME__

/************************************ Local Type Definitions  ************************************/
/*********************************** Local Constant Definitions **********************************/
/********************************** Global Variable Declarations *********************************/
/********************************** Local Function Declarations **********************************/
/********************************** Local Variable Declarations **********************************/
/********************************** Public Function Declarations *********************************/
void main(void)
{
	printk("Molon Labe " BUILD_DATETIME "\n");

	drv_gpio_init();
}

/*********************************** Local Function Definitions **********************************/
