/**************************************************************************************************
  Revised:        $Date: 2009-03-10 15:21:32 -0700 (Tue, 10 Mar 2009) $
  Revision:       $Revision: 19366 $

  Copyright 2007 Texas Instruments Incorporated.  All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights granted under
  the terms of a software license agreement between the user who downloaded the software,
  his/her employer (which must be your employer) and Texas Instruments Incorporated (the
  "License"). You may not use this Software unless you agree to abide by the terms of the
  License. The License limits your use, and you acknowledge, that the Software may not be
  modified, copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio frequency
  transceiver, which is integrated into your product. Other than for the foregoing purpose,
  you may not use, reproduce, copy, prepare derivative works of, modify, distribute,
  perform, display or sell this Software and/or its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS�
  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY
  WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
  IN NO EVENT SHALL TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE
  THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY
  INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST
  DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY
  THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 *   BSP (Board Support Package)
 *   Target : Chronos Access Point with CC1111
 *   Top-level board code file.
 * =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 */


/* ------------------------------------------------------------------------------------------------
 *                                            Local Variables
 * ------------------------------------------------------------------------------------------------
 */
#if defined(SW_TIMER)
static uint8_t sIterationsPerUsec = 0;
#endif

/**************************************************************************************************
 * @fn          BSP_InitBoard
 *
 * @brief       Initialize the board.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
void BSP_InitBoard(void)
{
#if defined(SW_TIMER)
#define MHZ_CLOCKS_PER_USEC      BSP_CLOCK_MHZ
#define MHZ_CLOCKS_PER_ITERATION 35

  sIterationsPerUsec = (uint8_t)(((MHZ_CLOCKS_PER_USEC)/(MHZ_CLOCKS_PER_ITERATION))+.5);

  if (!sIterationsPerUsec) 
  {
    sIterationsPerUsec = 1;
  }
#endif   /* SW_TIMER */

  SLEEP &= ~SLEEP_OSC_PD;             /* power up both oscillators */
  while(!(SLEEP & (SLEEP_XOSC_S | SLEEP_HFRC_S)));
  CLKCON = CLKCON_OSC32;
  while(!(SLEEP & (SLEEP_XOSC_S | SLEEP_HFRC_S)));
  SLEEP |= SLEEP_OSC_PD;              /* power down unused oscillator */

}
/**************************************************************************************************
 * @fn          BSP_Delay
 *
 * @brief       Sleep for the requested amount of time.
 *
 * @param       # of microseconds to sleep.
 *
 * @return      none
 **************************************************************************************************
 */
void BSP_Delay(uint16_t usec)
#if !defined(SW_TIMER)
{
#if (defined MRFI_CC2511 || defined MRFI_CC1111)
#define  TICK_CAL    (1)
#define  TICK_PAGE   (255 / 3)                     // 85
#define  TICK_LONG   (uint8_t)((TICK_PAGE * 3) - TICK_CAL)  // ((85 * 3) - 1) = 254
#define  T3_CFG      T3CTL_START | T3CTL_MODE_MODULO | T3CTL_DIV_8

  if (usec >= TICK_PAGE)
  {
     T3CC0 = TICK_LONG;
     do
     {
        usec -= TICK_PAGE;
        T3OVFIF = 0;                       /* Clear the interrupt flag. */
        T3CTL  = T3_CFG;                   /* start timer in down mode */
        while( !T3OVFIF );                 /* Wait till interrupt flag is set. */
     } while (usec >= TICK_PAGE);
  }

  if (usec)
  {
     T3CC0 = (usec * (BSP_CLOCK_MHZ/8));
     T3OVFIF = 0;                          /* Clear the interrupt flag. */
     T3CTL  = T3_CFG;                      /* start timer in down mode */
     while( !T3OVFIF );                    /* Wait till interrupt flag is set. */
  }

  T3OVFIF = 0;                             /* Clear the interrupt flag. */
#else
#error ERROR: No BSP_Delay() defined for HW Timer
#endif
  return;
}


#else  /* !SW_TIMER */

{
  /* Declared 'volatile' in case User optimizes for speed. This will
   * prevent the optimizer from eliminating the loop completely. But
   * it also generates more code...
   */
  volatile uint16_t repeatCount = sIterationsPerUsec*usec;

  while (repeatCount--) ;

  return;
}

#endif  /* !SW_TIMER */
