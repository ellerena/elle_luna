/***************************************************************
 *  SmartRF Studio(tm) Export
 *
 *  Radio register settings specifed with C-code
 *  compatible #define statements.
 *
 ***************************************************************/

#ifndef SMARTRF_CC1111_H
#define SMARTRF_CC1111_H

#define SMARTRF_RADIO_CC1111

#define SMARTRF_SETTING_FSCTRL1     0x12///0x09 //0x12
#define SMARTRF_SETTING_FSCTRL0     0x00
#ifdef ISM_EU
  #define SMARTRF_SETTING_FREQ1       0x3A
  #define SMARTRF_SETTING_FREQ0       0xEE
#else
  #ifdef ISM_US
  // 902MHz (CHANNR=20 --> 906MHz)
  #define SMARTRF_SETTING_FREQ2       0x25 //
  #define SMARTRF_SETTING_FREQ1       0x95 //
  #define SMARTRF_SETTING_FREQ0       0x55 //
    // 912MHz (CHANNR=0)
//    #define SMARTRF_SETTING_FREQ2       0x26
//    #define SMARTRF_SETTING_FREQ1       0x00
//    #define SMARTRF_SETTING_FREQ0       0x00
  #else
    #ifdef ISM_LF
      // 433.30MHz
      #define SMARTRF_SETTING_FREQ2       0x12
      #define SMARTRF_SETTING_FREQ1       0x14
      #define SMARTRF_SETTING_FREQ0       0x7A
    #else
      #error "Wrong ISM band specified (valid are ISM_LF, ISM_EU and ISM_US)"
    #endif // ISM_LF
  #endif // ISM_US
#endif // ISM_EU
#define SMARTRF_SETTING_MDMCFG4     0x1D///0x6B //0x1D
#define SMARTRF_SETTING_MDMCFG3     0x55///0xA3 //0x55
#define SMARTRF_SETTING_MDMCFG2     0x13
#define SMARTRF_SETTING_MDMCFG1     0x23
#define SMARTRF_SETTING_MDMCFG0     0x11
#define SMARTRF_SETTING_CHANNR      0x14///0x00 //0x14
#define SMARTRF_SETTING_DEVIATN     0x63///0x43 //0x63
#define SMARTRF_SETTING_FREND1      0xB6
#define SMARTRF_SETTING_FREND0      0x10
#define SMARTRF_SETTING_MCSM0       0x18
#define SMARTRF_SETTING_FOCCFG      0x1D
#define SMARTRF_SETTING_BSCFG       0x1C
#define SMARTRF_SETTING_AGCCTRL2    0xC7
#define SMARTRF_SETTING_AGCCTRL1    0x00
#define SMARTRF_SETTING_AGCCTRL0    0xB0
#define SMARTRF_SETTING_FSCAL3      0xEA
#define SMARTRF_SETTING_FSCAL2      0x2A
#define SMARTRF_SETTING_FSCAL1      0x00
#define SMARTRF_SETTING_FSCAL0      0x1F
#define SMARTRF_SETTING_TEST2       0x88
#define SMARTRF_SETTING_TEST1       0x31
#define SMARTRF_SETTING_TEST0       0x09
#define SMARTRF_SETTING_PA_TABLE0   0x8E///0x50 //0x8E
#define SMARTRF_SETTING_PKTCTRL1    0x04
#define SMARTRF_SETTING_PKTCTRL0    0x05
#define SMARTRF_SETTING_ADDR        0x00
#define SMARTRF_SETTING_PKTLEN      0xFF

#endif

