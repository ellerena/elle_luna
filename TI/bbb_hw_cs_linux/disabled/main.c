/*
 ============================================================================
 Name        : main.c
 Author      : elle
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C
 ============================================================================
 */

#include <gpio.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define xstr(s) str(s)
#define str(s) #s

/*
 *
 * Print a greeting message on standard output and exit.
 *
 * On embedded platforms this might require semi-hosting or similar.
 *
 * For example, for toolchains derived from GNU Tools for Embedded,
 * to enable semi-hosting, the following was added to the linker:
 *
 * --specs=rdimon.specs -Wl,--start-group -lgcc -lc -lc -lm -lrdimon -Wl,--end-group
 *
 * Adjust it for other toolchains.
 *
 */

int main(int argc, char **argv)
{
	char hostname[20], *query, *host;
	char v;

	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	gethostname(hostname, 19);
	host = getenv("HTTP_HOST");
	query = getenv("QUERY_STRING");
	v = query ? query[8] : ((argc > 1) ? (*argv[1]) : 'x');

	printf("Content-type: text/html\n\n");
	printf("<html>\n");
	printf("<header><title>Hello C/C++\n</title></header>");
	printf("<body><p>\n");
/***************************** BODY BEGIN *****************************/
	printf( "==================================<br>\n"\
			"Intel Debian - v1.0 Linux Test<br>\n"
			xstr(PROJNAME) " - " xstr(CONFIGNAME) "<br>\n"\
			"build " __DATE__ " " __TIME__ "<br>\n"\
			"==================================<br><br>\n");
//	while (v)
//	{
//		printf("Enter a number:");
//		scanf("%d", &v);
//		printf("You entered: %dd [0x%x] {8x%o}\n", v, v, v);
//	}

	printf("Soy %s<br>\n\n", hostname);
	printf("Host: %s<br>\n", host);
	printf("Query: %s<br><br>\n", query);
	printf("Last run: %d-%d-%d %d:%d:%d<br><br>\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	printf("Current LED status must be %s<br><br>\n", v == '1' ? "ON":"OFF");

	printf("Export: %d<br>\n", gpio_export(60, EXPORT));
	printf("Direction: %d<br>\n", gpio_set_dir(60, OUTPUT_PIN));
	gpio_set_value(60, v == '1' ? PIN_HIGH : PIN_LOW);
	if(v == 'x')
	{
		gpio_export(60, UNEXPORT);
	}
	printf("<form><div>LED state:\n"
			"<input type='radio' name='command' value='1'/>On\n"
			"<input type='radio' name='command' value='0'/>Off\n"
			"<input type='radio' name='command' value='x'/>Stop</div>\n");
	printf("<div>Command: <input type='text' name='texto'\n"
			"size='40' value=''><input type='submit'\n"
			"value='Submit' /></div></form><br><br>\n");
	printf("Gracias.\n");
/***************************** BODY END *****************************/
	printf("</p></body>");
	printf("</html>\n");

  return 0;
}

