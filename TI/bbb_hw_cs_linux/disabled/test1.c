/*
 * test2.c
 *
 *  Created on: Mar 21, 2020
 *      Author: eddie
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define ALGNMT       8
#define OFF_T        unsigned short
#define OFF_S        sizeof(OFF_T)
#define TOTSZ(x)     (x + OFF_S + (ALGNMT -1))
#define OFF_V(x,y)   ((y)-(x))
#define READLED		//fread(a, sizeof(char), 1, f); // printf("Read: %d", a[0]);
#define BUFS		   (200)
#define CLEANMEM	   do { memset(mem, 0, BUFS); } while(0);
#define NEWMALLOC(x) malloc(x); // memf(x);
#define NEWFREE(x)   free(x);

uint8_t mem[BUFS];

void * memf(size_t s)
{
   static int r = 0;
   void * p;

   r &= 0x1f;
   printf("r %2d ", r);
   r++;
   p = (void*)&mem[r];

   return p;
}

void * align8_up (void * addr)
{
   return (void*) (((size_t)addr + 7u) & ~((size_t)7u));
}

void * malloc8(size_t size)
{
   void * p, * pm = NULL;
   OFF_T offset;

   if (size) {

      pm = (void*)NEWMALLOC(TOTSZ(size));
      if (pm != NULL) {
         p = (void*)((size_t)pm + OFF_S);
         p = align8_up(p);
         offset = (OFF_T)OFF_V((size_t)pm, (size_t)p);
         ((OFF_T*)p)[-1] = offset;
         printf("p:%p, pm:%p ", p, pm);
         pm = p;
      }

   }

   return pm;
}

void free8(void * p) {
   int offset;
   char * pm;

   pm = (char*)p;
   offset = ((OFF_T*)pm)[-1];
   pm -= offset;

   printf("Free: p:%p, pm:%p, offset:%d\n", p, pm, offset);

   NEWFREE (pm);
}

int main (void)
{
   uint32_t i;
   void * p;
   FILE *f = NULL;
   char a[3] = "";

   printf("My test app b." __DATE__ " " __TIME__ "\n");
   for (i = 10; i < 30; i++)
   {
      p = malloc8(i);
      if (p) {
         free8(p);
      }
   }

   printf("Led test start, memf:%p\n", mem);
   srand(time(NULL));

   for (i = 0; i < 1; i++) {
	   READLED
	   f = fopen("/sys/class/leds/beaglebone:green:usr0/brightness", "r+");
	   fwrite("1", sizeof(char), 1, f);
	   fclose(f);
//	   printf("Led On ");
	   sleep(1);
	   READLED
	   f = fopen("/sys/class/leds/beaglebone:green:usr0/brightness", "r+");
	   fwrite("0", sizeof(char), 1, f);
	   fclose(f);
//	   printf("Led Off\n");
	   sleep(1);
   }

   printf("Led test ended\n");

   printf("bye\n");
   return 0;
}

