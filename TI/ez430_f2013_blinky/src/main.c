/*
  MSP430F20xx Demo - Software Toggle P1.0

  Description;
  Generates a 10ms pulse which is repeated every 1sec.

                MSP430F20xx
             -----------------
         /|\|              XIN|-
          | |                 |
          --|RST          XOUT|-
            |                 |
            |             P1.0|-->LED

*/

#include <msp430.h>

#define		TIC_ONE_SEC			12860
#define		TIC_TEN_MSEC		128

int main(void)
{
  WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
  P1DIR |= 0x01;                            // P1.0 output

  /* configure clocks DCO: 16MHz ACLK: VLO */
  DCOCTL = 0;                               // Select lowest DCOx and MODx settings
  BCSCTL1 = CALBC1_16MHZ | XT2OFF | DIVA_0;	// configure ACLK and DCO (calibrated)
  DCOCTL = CALDCO_16MHZ;					// Load calibrated value
  BCSCTL3 = LFXT1S_2;

  /* configure timer TA */
  CCR0 = 0;									// Stop timer
  TACCTL1 = CCIE;							// CCR1 interrupt enabled
  TACTL = TASSEL_1 + MC_1 + ID_0 + TAIE;	// Use ACLK/1, up to CCR0, use IRQ

  CCR1 = TIC_TEN_MSEC;						// define pulse width
  CCR0 = TIC_ONE_SEC;						// define pulse period and start timer

  __bis_SR_register(LPM0_bits + GIE);       // Enter LPM0 w/ interrupt

  return 0;
}

//#pragma vector=TIMERA1_VECTOR
//__interrupt void Timer_(void)
void __attribute__((interrupt (TIMERA1_VECTOR))) MyTimer_(void)
{
	switch (TAIV)
	{
		case TAIV_TACCR1:
			P1OUT = 0x00;                   // P1.0 low
			break;
		case TAIV_TAIFG:
			P1OUT = 0x01;                   // P1.0 high
			break;
		default:
			break;
	}
}



