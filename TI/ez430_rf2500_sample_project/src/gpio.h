/*
 * gpio.h
 *
 *  Created on: Oct 5, 2014
 *      Author: Edison
 */

#ifndef GPIO_H_
#define GPIO_H_

#include "msp430.h"
#include "stdint.h"

#define POWERDOWN			BIT0
#define	RESETb				BIT1
#define ADDR0				BIT2
#define STINT				BIT3
#define SPICS				BIT4

#define MACRO_GPIO_INIT 	P1REN	= BIT2;									/* P1_2 resistor enable */\
							P1OUT	= BIT0 | BIT1 | BIT2;					/* P1_0/1 OUTPUT, P1_2 PU */\
							P1IFG	= 0;									/* Clear pending interrupts */\
							P1IE	= BIT2;									/* P1_2 Interrupt Enabled */\
							P1IES	= BIT2;									/* P1_2 Interrupt on H-to-L transition */\
							P3DIR	= BIT0;									/* P3_0 output */\
							P3OUT	= BIT0;									/* P3_0 high */\
							P2REN	= STINT;								/* P2_3 resistor enable */\
							P2OUT	= STINT;								/* P2_3 Pull Up */\
							P2DIR	= POWERDOWN | RESETb | ADDR0 | SPICS;	/* P2_3 input, P2_0/1/2 output */




void GPIO_Init(void);
__interrupt void Port_1(void);

#endif /* GPIO_H_ */
