#include "virtual_com_cmds.h"

/**
 *	@brief	This function prints a character string to the UART.<br>
 *			It will stop after the first non-printable character -expected to be '\r'.
 *	@param	s is a pointer to the start of the string.
 * */
void COM_print(char * s)
{
	do
	{
		while (!(IFG2 & UCA0TXIFG));				// USCI_A0 TX buffer empty?
		UCA0TXBUF = *s;
	} while (*s++ > 31);

	while (!(IFG2 & UCA0TXIFG));
	UCA0TXBUF = '#';
}


/**
 *	@brief	This function converts and prints an unsigned char using hex format display.
 *	@param	n is the unsigned char number (between o and 255)
 */
void COM_TXHex(uint8_t *n, unsigned l)
{
	char msg[] = {"x00\r"}, c;

	while(l--)
	{
		c = (*n) & 0x0f;								/* extract less significant nibble for processing */
		msg[2] = c < 0x0a ? c+0x30 : c+0x57;		/* convert the nibble into a printable hex */
		c = (*n)>>4;								/* extract more significant nibble for processing */
		msg[1] = c < 0x0a ? c+0x30 : c+0x57;		/* convert the nibble into a printable hex */
		COM_print(msg);							/* send the whole thing for printing */
		n++;
	}
}

