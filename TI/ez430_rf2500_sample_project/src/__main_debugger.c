/* --COPYRIGHT--,BSD_EX
 * Copyright (c) 2012, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *******************************************************************************
 *
 *                       MSP430 CODE EXAMPLE DISCLAIMER
 *
 * MSP430 code examples are self-contained low-level programs that typically
 * demonstrate a single peripheral function or device feature in a highly
 * concise manner. For this the code may rely on the device's power-on default
 * register values and settings such as the clock configuration and care must
 * be taken when combining code from several examples to avoid potential side
 * effects. Also see www.ti.com/grace for a GUI- and www.ti.com/msp430ware
 * for an API functional library-approach to peripheral configuration.
 *
 * --/COPYRIGHT--*/
/******************************************************************************
		MSP430F22x4 Demo - USCI_A0, 9600 UART Echo ISR, DCO SMCLK

		Description: Echo a received character, RX ISR used. Normal mode is LPM0.
		USCI_A0 RX interrupt triggers TX Echo.
		Baud rate divider with 1MHz = 1MHz/9600 = ~104.2
		ACLK = n/a, MCLK = SMCLK = CALxxx_1MHZ = 1MHz

					MSP430F22x4
				 -----------------
			 /|\|              XIN|-
			  | |                 |
			  --|RST          XOUT|-
				|                 |
				|     P3.4/UCA0TXD|------------>
				|                 | 9600 - 8N1
				|     P3.5/UCA0RXD|<------------

		A. Dannenberg
		Texas Instruments Inc.
		April 2006
		Built with CCE Version: 3.2.0 and IAR Embedded Workbench Version: 3.41A
******************************************************************************/
#include "virtual_com_cmds.h"
#include "gpio.h"
#include "spi.h"


#define		FLAG_UART_COMMAND_RCVD			0x01u
#define		LED_RED							BIT0
#define		LED_GREEN						BIT1
#define		NULL							0

char COM_buffer[21] = "STS3050 SPI 0.0.0.1\r";
uint16_t flag = 0;

/**
 *	@brief	converts a string of chars representing a vector of hex numbers into a vector of bytes equivalent.
 *	@retval	the count of numbers comverted.
 *	@verbatim
 *			Text entered by a user via the keyboard is in the form of a string of printable chars that represent hex numbers.
 *			This functions takes that string of chars and extracts the meaningful hex numbers represented in it, then
 *			it places those numbers (bytes) in an array at the same ram location. Returning the number of numbers
 *			converted.
 *	@endverbatim
 */
unsigned strtohex(void)
{
	unsigned i, k;
	uint8_t c;

	char *p, *q, d;

	p = &COM_buffer[1];
	q = p;
	k = 0;

	while (*p > 0x1f)
	{
		d = 0;
		i = 0;

		do
		{
			d <<= 4;
			c = *p++;
			if ((c > 0x29) && (c < 0x3a))
			{
				d += (c - 0x30);
				i++;
			}
			if ((c > 0x60 ) && (c < 0x67))
			{
				d += (c - 0x60 + 0x9);
				i++;
			}

		} while (( i == 0 ) || ((*p > 0x2f) && (i != 2)));


		if(i)
		{
			*q++ = d;
			k++;
		}
	}

	return k;
}

int main(void)
{
	WDTCTL = WDTPW + WDTHOLD;							/* Stop Watch Dog timer */
	DCOCTL = CALDCO_16MHZ;
	BCSCTL1 = CALBC1_16MHZ;

	MACRO_GPIO_INIT
	MACRO_COM_INIT
	MACRO_SPI_INIT

	IE2 = UCA0RXIE;										/* Enable COM RX interrupt */

	COM_print(COM_buffer);								/* print welcome msg */

	while (1)											/* forever loop */
	{
		__bis_SR_register(LPM0_bits + GIE);				/* Enter LPM0, interrupts enabled */
		if (flag == FLAG_UART_COMMAND_RCVD)
		{
			unsigned	k;

			k = strtohex();

			switch(*COM_buffer)
			{
				case 'w':
					SpiWriteData((uint8_t*)COM_buffer, k);
					break;
				case 'g':
					SpiGetData((uint8_t*)COM_buffer);
					break;
				case 's':
					SpiSendData((uint8_t*)COM_buffer+1, k);
					break;
				case 'p':
					P2OUT |= POWERDOWN;
					P1DIR &= ~LED_RED;
					break;
				case 'P':
					P2OUT &= ~POWERDOWN;
					P1DIR |= LED_RED;
					break;
				case 'r':
					P2OUT &= ~RESETb;
					P1DIR &= ~LED_GREEN;
					break;
				case 'R':
					P2OUT |= RESETb;
					P1DIR |= LED_GREEN;
					break;
				case 'a':
					P2OUT &= ~ADDR0;
					break;
				case 'A':
					P2OUT |= ADDR0;
					break;
				default:
					COM_print("SPI write: w <reg> <B0>..<B8>\r");
					COM_print("SPI read: g <reg> <count>\r");
					COM_print("SPI send: s <B0>...<B9>\r");
			}
			COM_print("#Done\r");
			flag = 0;
		}
	}

	return 0;
}

/**
 *	@brief	Interrupt handler for data received on via UART
 *	@verbatim
 *		Receives byte on the RXD pin of the victual com port. Each
 *		byte received is added to our global buffer.
 *		When the byte received is a return ('\r') a flag is set to the
 *		main program to request processing of the received command.
 */
#pragma vector=USCIAB0RX_VECTOR
__interrupt void UART_SPI_RX_handler(void)
{
	char a;
	static char *buffer = COM_buffer;

	a = UCA0RXBUF;
	*buffer++ = a;							/* store rfcvd char in ram */

	if (a == '\r')
	{
		buffer = COM_buffer;
		flag = FLAG_UART_COMMAND_RCVD;
	}

	__bic_SR_register_on_exit(LPM0_bits);
}



