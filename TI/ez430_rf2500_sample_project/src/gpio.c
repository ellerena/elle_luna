/*
 * gpio.c
 *
 *  Created on: Oct 5, 2014
 *      Author: Edison
 */

#include "gpio.h"

#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
	P1IFG &= ~BIT2;											// Clear pending interrupts - pin 2
	WDTCTL = 0;												/* force PUC */
}


