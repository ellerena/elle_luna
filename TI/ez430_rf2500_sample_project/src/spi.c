/*
 * spi.c
 *
 *  Created on: Oct 5, 2014
 *      Author: Eddie.Llerena
 */

#include "spi.h"
#include "virtual_com_cmds.h"


void SpiWriteData(uint8_t * d, unsigned l)
{
	unsigned i = l;

	do
	{
		d[i+1] = d[i];
	} while (--i > 1);

	*d = *(d+1) | 0x80;
	*(d+1) = (uint8_t)(l-1);
	*(d+2) = 0;
	SpiSendData(d, l+2);
}

void SpiGetData(uint8_t * d)
{
	uint8_t i;

	i = *(d+2);
	*d = *(d+1);
	*(d+1) = i;

	i += 2;
	do
	{
		d[i--] = 0;
	} while (i > 1);
	i = *(d+1);
	SpiSendData(d, (unsigned)i+3u);
	COM_TXHex(d+3, (unsigned)i);
}


void SpiSendData(uint8_t *d, unsigned l)
{
	P2OUT &= ~BIT4;						/* SPI_CS Select */

	do
	{
		UCB0TXBUF = *d;					/* Send 1 byte */
		while (!(IFG2 & UCB0RXIFG));	/* Wait to receive 1 byte */
		*d++ = UCB0RXBUF;				/* Receive 1 byte */
	} while(--l);

	P2OUT |= BIT4;							/* SPI_CS DeSelect */
}
