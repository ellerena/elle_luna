/*
 * spi.h
 *
 *  Created on: Oct 5, 2014
 *      Author: Eddie.Llerena
 */

#ifndef SPI_H_
#define SPI_H_

#include <msp430.h>
#include "stdint.h"

#define MACRO_SPI_INIT		UCB0CTL1 = UCSWRST | UCSSEL1;				/* Use SMCLK and keep RESET for now */				\
							UCB0CTL0 = UCCKPH | UCMSB | UCMST | UCSYNC;	/* rising edge capture, MSB first, Master, SPI */	\
							UCB0BR0 = 4;								/* LSB, baud rate = SMCLK / UCB0BR */				\
							UCB0BR1 = 0;								/* MSB, baud rate = SMCLK / UCB0BR */				\
							P3SEL |= BIT1 | BIT2 | BIT3;				/* Enable MISO, MOSI amd SMCLK functions */			\
							P3REN = BIT0;								/*  */\
							UCB0CTL1 &= ~UCSWRST;						/* release RESET */

void SPI_init(void);
void SpiWriteData(uint8_t*, unsigned);
void SpiGetData(uint8_t*);
void SpiSendData(uint8_t *, unsigned);

#endif /* SPI_H_ */
