/***********************************************************************************
  Filename:     hal_uart.h

  Description:  hal UART library header file

***********************************************************************************/

#ifndef HAL_UART_H
#define HAL_UART_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** INCLUDES *****************************/
#include "bsp.h"

/*********************** CONSTANTS AND DEFINES ***********************/
/* Serial Port Baudrate Settings */
/* Stop Bits */
#define	F_COM_RCVD						( 1 << 0 )

#define CRLF							"\n\r"

/*********************** GLOBAL FUNCTIONS ***********************/
void halUartInit(void);
void COM_send(const uint8_t * s, uint16_t len);
void COM_puts(const uint8_t * s);
void COM_TXHex(uint16_t v);

/*********************** GLOBAL VARIABLES ***********************/
extern volatile XDATA uint8_t flags;


#ifdef  __cplusplus
}
#endif

#endif


