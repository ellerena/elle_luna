/***********************************************************************************

    Filename:     usb_uart.h

    Description:  USB Virtual UART implementation.

***********************************************************************************/

/****************** INCLUDES ******************/
#include "hal_board.h"

#include "usb_cdc.h"
#include "usb_firmware_library_config.h"
#include "usb_firmware_library_headers.h"

#include "usb_uart.h"

XDATA ringBuf_t rbRxBuf;
XDATA ringBuf_t rbTxBuf;

/*********************************************************************/
/********* MACROS and DEFINITIONS *********/

/************ GLOBAL VARIABLES ************/
XDATA CDC_LINE_CODING_STRUCTURE currentLineCoding;
uint16_t cdcRTS;              // Request-To-Send modem control line
uint8_t  cdcCTS;              // Clear-To-Send   modem control line

/*************** LOCAL DATA ************/
static XDATA uint8_t buffer[USB_MAX_PACKET_SIZE];
static uint8_t oldEndpoint;

/*************** LOCAL FUNCTIONS ***********/
/**
* @brief   Extract bytes from the buffer.
* @param   pBuf   - pointer to the ringbuffer
*          pData  - pointer to data to be extracted
*          nBytes - number of bytes
* @return  Bytes actually returned
*/
uint8_t bufGet(ringBuf_t *pBuf, uint8_t *pData, uint8_t nBytes)
{
	uint8_t i, h;
	uint16_t s;

	BSP_ENTER_CRITICAL_SECTION(s);    // Critical section start

	h = pBuf->iHead;
	nBytes = min(nBytes, pBuf->nBytes);
	i= 0;
	while(i < nBytes)
	{
		*pData++ = pBuf->pData[h++];
		if (h == BUF_SIZE) h = 0;
		i++;
	}
	pBuf->nBytes-= i;
	pBuf->iHead = h;

	__bsp_RESTORE_ISTATE__(s);    // Critical section end
	return i;
}

/**
* @brief   Add bytes to the buffer.
* @param   pBuf - pointer to the ringbuffer
*          pData - pointer to data to be appended to the buffer
*          nBytes - number of bytes
* @return  Number of bytes copied to the buffer
*/
uint8_t bufPut(ringBuf_t *pBuf, const uint8_t *pData, uint8_t nBytes)
{
   uint8_t i, t;
   uint16_t s;

   BSP_ENTER_CRITICAL_SECTION(s);    // Critical section start

   i= 0;
   if (pBuf->nBytes+nBytes < BUF_SIZE)
   {
      t = pBuf->iTail;
      while(i < nBytes)
      {
         pBuf->pData[t++]= *pData++;
         if (t == BUF_SIZE) t = 0;
         i++;
      }
      pBuf->nBytes += i;
      pBuf->iTail = t;
   }

   __bsp_RESTORE_ISTATE__(s);    // Critical section end

   return i;
}

/**
* @brief   Initialise a ringbuffer. The buffer must be allocated by the
*          application.
* @param   pBuf - pointer to the ringbuffer
*/
void bufInit(ringBuf_t *pBuf)
{
    uint16_t s;

    BSP_ENTER_CRITICAL_SECTION(s);    // Critical section start

    pBuf->nBytes= 0;
    pBuf->iHead= 0;
    pBuf->iTail= 0;

    __bsp_RESTORE_ISTATE__(s);    // Critical section end
}

/**
* @brief        Handle the USB events which are not directly related to the UART
*/
static void usbEventProcess(void)
{
	if (USBIRQ_GET_EVENT_MASK() & USBIRQ_EVENT_RESET)    // Handle reset signaling on the bus
	{
		USBIRQ_CLEAR_EVENTS(USBIRQ_EVENT_RESET);
		usbfwResetHandler();
	}

	if (USBIRQ_GET_EVENT_MASK() & USBIRQ_EVENT_SETUP)    // Handle packets on EP0
	{
		USBIRQ_CLEAR_EVENTS(USBIRQ_EVENT_SETUP);
		usbfwSetupHandler();
	}

	if (USBIRQ_GET_EVENT_MASK() & USBIRQ_EVENT_SUSPEND)  // Handle USB suspend
	{
		USBIRQ_CLEAR_EVENTS(USBIRQ_EVENT_SUSPEND);       // Clear USB suspend interrupt
		usbsuspEnter();                                  // Take the chip into PM1 until a USB resume is deteceted.
		USBIRQ_CLEAR_EVENTS(USBIRQ_EVENT_RESUME);        // Running again; first clear RESUME interrupt
	}
}

/**
* @brief        Handle traffic flow from RF to USB.
*/
static void usbInProcess(void)
{
    uint8_t length;

    BSP_DISABLE_INTERRUPTS();    // USB ready to accept new IN packet
    oldEndpoint = USB_GET_ENDP;
    USB_USE_ENDP(4);

    if ( USB_IN_ENDP_DISARMED )    // The IN endpoint is ready to accept data
    {
        length = rbTxBuf.nBytes;        // Number of bytes present in RF buffer

        if (length)
        {
            if (length > USB_MAX_PACKET_SIZE)            // Limit the size
            {
                length = USB_MAX_PACKET_SIZE;
            }

            bufGet(&rbTxBuf, buffer, length);            // Read from UART TX buffer
            usbfwWriteFifo(&USBF4, length, (GENERIC void *)buffer);  // Write to USB FIFO
            USB_USE_ENDP(4);            // Flag USB IN buffer as not ready (disarming EP4)
            USB_ARM_IN_ENDP();   // Send data to the host
        }
    }

    USB_USE_ENDP(oldEndpoint);
    BSP_ENABLE_INTERRUPTS();
}

/**
* @brief        Handle traffic flow from USB to RF.
*/
static void usbOutProcess(void)
{
   uint16_t length, nToSend;

   BSP_DISABLE_INTERRUPTS();    // If new packet is ready in USB FIFO

   oldEndpoint = USB_GET_ENDP;
   USB_USE_ENDP(4);

   if (USB_OUT_ENDP_DISARMED)
   {   // Get length of USB packet, this operation must not be interrupted.
      length = USB_ENDPOINT_COUNT_HIGH;
      length <<= 8;
      length |= USB_ENDPOINT_COUNT_LOW;

      // Calculate number of bytes available in RF buffer; and the number
      // of bytes we may transfer in this operation.
      nToSend= min(BUF_SIZE - (rbRxBuf.nBytes), length);

      if (nToSend)        // Space available in UART RX buffer ?
      {
         usbfwReadFifo(&USBF4, nToSend, (GENERIC void *)buffer);  // Read from USB FIFO
         bufPut(&rbRxBuf,buffer,nToSend);            // Write to radio TX buffer
         if (length == nToSend)            // If entire USB packet is read from buffer
         {
            USB_USE_ENDP(4);
            USB_ARM_OUT_ENDPOINT;
         }
      }
   }

   USB_USE_ENDP(oldEndpoint);
   BSP_ENABLE_INTERRUPTS();
}

/*************** FUNCTIONS *************/
/**
* @brief        USB UART init function.
*               - Set initial line decoding to 8/NONE/1.
*               - Initialise the USB Firmware Library and the USB controller.
*/
void usbUartInit(uint32_t baudrate)
{   // Set default line coding.
    currentLineCoding.dteRate = baudrate;
    currentLineCoding.charFormat = CDC_CHAR_FORMAT_1_STOP_BIT;
    currentLineCoding.parityType = CDC_PARITY_TYPE_NONE;
    currentLineCoding.dataBits = 8;

    // Initialise hardware flow control
    cdcRTS= 0;      // TRUE when DCE connected
    cdcCTS= 1;      // Indicate CTS to DCE (internal mimic as CDC does not directly support CTS).

    usbfwInit();    // Init USB library
    usbirqInit();    // Initialize the USB interrupt handler with bit mask containing all processed USBIRQ events
    HAL_USB_PULLUP_ENABLE();    // Enable pullup on D+
    BSP_ENABLE_INTERRUPTS();    // Enable global interrupts TODO: redundant
}

/**
* @brief        The USB UART main task function. Must be called from the
*               applications main loop.
*/
void usbUartProcess(void)
{
	usbEventProcess();          // Process USB events

	if (cdcCTS && cdcRTS)
	{
		usbOutProcess();        // Process USB OUT data (USB -> RF)
		usbInProcess();         // Process USB IN data (RF -> USB)
	}
}



