#ifndef USB_CDC_H
#define USB_CDC_H

/* Device Class Code */
#define CDC_DEVICE                  0x02

/* Communication Interface Class Code */
#define COMM_INTF                   0x02

/* Communication Interface Class SubClass Codes */
#define ABSTRACT_CONTROL_MODEL      0x02

/* Communication Interface Class Control Protocol Codes */
#define V25TER                      0x01    // Common AT commands ("Hayes(TM)")


/* Data Interface Class Codes */
#define DATA_INTF                   0x0A

/* Data Interface Class Protocol Codes */
#define NO_PROTOCOL                 0x00    // No class specific protocol required


/* Communication Feature Selector Codes */
#define ABSTRACT_STATE              0x01
#define COUNTRY_SETTING             0x02

/* Functional Descriptors */
/* Type Values for the bDescType Field */
#define CS_INTERFACE                0x24
#define CS_ENDPOINT                 0x25

/* bDescSubType in Functional Descriptors */
#define DSC_FN_HEADER               0x00
#define DSC_FN_CALL_MGT             0x01
#define DSC_FN_ACM                  0x02    // ACM - Abstract Control Management
#define DSC_FN_DLM                  0x03    // DLM - Direct Line Managment
#define DSC_FN_TELEPHONE_RINGER     0x04
#define DSC_FN_RPT_CAPABILITIES     0x05
#define DSC_FN_UNION                0x06
#define DSC_FN_COUNTRY_SELECTION    0x07
#define DSC_FN_TEL_OP_MODES         0x08
#define DSC_FN_USB_TERMINAL         0x09
/* more.... see Table 25 in USB CDC Specification 1.1 */

#define CDC_COMM_INTF_ID            0x00
#define CDC_DATA_INTF_ID            0x01

// CLASS REQUESTS
#define CDC_SEND_ENCAPSULATED_COMMAND      0x00
#define CDC_GET_ENCAPSULATED_RESPONSE      0x01
#define CDC_SET_COMM_FEATURE               0x02     //optional
#define CDC_GET_COMM_FEATURE               0x03     //optional
#define CDC_CLEAR_COMM_FEATURE             0x04     //optional
#define CDC_SET_LINE_CODING                0x20     //optional
#define CDC_GET_LINE_CODING                0x21     //optional
#define CDC_SET_CONTROL_LINE_STATE         0x22     //optional
#define CDC_SEND_BREAK                     0x23     //optional

#define CDC_CHAR_FORMAT_1_STOP_BIT     0
#define CDC_CHAR_FORMAT_1_5_STOP_BIT   1
#define CDC_CHAR_FORMAT_2_STOP_BIT     2

#define CDC_PARITY_TYPE_NONE           0
#define CDC_PARITY_TYPE_ODD            1
#define CDC_PARITY_TYPE_EVEN           2
#define CDC_PARITY_TYPE_MARK           3
#define CDC_PARITY_TYPE_SPACE          4

#endif
