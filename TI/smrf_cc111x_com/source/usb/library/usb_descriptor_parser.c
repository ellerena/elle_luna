/***********************************************************************************

    Filename:     usb_descriptor_parser.c

    Description:  Parser for USB descriptor structures.

***********************************************************************************/

#define USBDESCRIPTORPARSER_C ///< Modifies the behavior of "EXTERN" in usb_descriptor_parser.h
#include "usb_firmware_library_headers.h"
#include "mrfi_link.h"

//-------------------------------------------------------------------------------------------------------
// USBDP internal module data
static USBDP_DATA usbdpData; ///< USBDP internal module data

/**
 * @brief	Initializes a search.
 * This function must be called before each new search to reset USBDP_DATA.pDesc.
*/
void usbdpInit(void)
{
   usbdpData.pDesc = (const uint8_t *) usbDescriptorMarker.pUsbDescStart;
}

/**
 * @brief	Locates the descriptor of the wanted type
*
* This function parses through the USB descriptors until:
* It hits one with bDescriptorType = wantedType, in which case it returns a pointer to
*     that descriptor, and exits. USBDP_DATA.pDesc will then point to the next descriptor.
* It hits one with bDescriptorType = haltAtType, in which case it returns a NULL-pointer,
*     and exits. USBDP_DATA.pDesc will then point to that descriptor.
* USBDP_DATA.pDesc = usbDescEnd, in which case it returns a NULL-pointer, and exits.
*     USBDP_DATA.pDesc will continue to point to usbDescEnd.
*
* To begin a search with this function, usbdpInit should be called first. It should not be
*       called when continuing a search - for instance after a call to usbdpGetConfigurationDesc().
*
* @param[in]      The wanted descriptor type (e.g. DESC_TYPE_CONFIG)
* @param[in]       haltAtType
*     The parser halts when it reaches this descriptor type, unless haltAtType is 0 (which in any
*     case is an invalid bDescriptorType value).
*
* @return
*     A pointer to the wanted descriptor type, or NULL if it was not found.
*/
void CODE* usbdpFindNext(uint8_t wantedType, uint8_t haltAtType)
{
   void CODE*pResult;

    pResult = NULL;
    while (usbdpData.pDesc != (void CODE*) usbDescriptorMarker.pUsbDescEnd)   // As long as we haven't reached the end...
    {
        if (usbdpData.pDesc[DESC_TYPE_IDX] == wantedType)				   	   // If we have a match on wantedType...
        {
            pResult = (void CODE*) usbdpData.pDesc;
            usbdpData.pDesc += usbdpData.pDesc[DESC_LENGTH_IDX];
            break;
        }
        else if (usbdpData.pDesc[DESC_TYPE_IDX] == haltAtType)                // If we have a match on haltAtType...
        {
            if (haltAtType) break;
        }

        usbdpData.pDesc += usbdpData.pDesc[DESC_LENGTH_IDX];                        // Move on to the next descriptor
    }

    return pResult;
}

/**
 * @brief	Locates the (one and only) device descriptor
*  @note It is not necessary to call usbdpInit() before this function.
*  @return A pointer to the USB_DEVICE_DESCRIPTOR, or NULL if it was not found.
*/
USB_DEVICE_DESCRIPTOR CODE* usbdpGetDeviceDesc(void)
{
	usbdpInit();
	return usbdpFindNext(DESC_TYPE_DEVICE, 0);
}

/**
 * @brief	Locates a configuration descriptor
* The search will either look for a descriptor with a specific
* USB_CONFIGURATION_DESCRIPTOR.bConfigurationValue, or simply take the n'th descriptor (by "index")
*
* @note It is not necessary to call usbdpInit() before this function.
*
* @param[in]       cfgValue
*     The configuration value to search for (USB_CONFIGURATION_DESCRIPTOR.bConfigurationValue), or
*     0 to find descriptor by index
* @param[in]       cfgIndex
*     A zero-based index for the configuration descriptor to find.
*     This value is ignored unless cfgValue is 0.
* @return
*     A pointer to the USB_DEVICE_DESCRIPTOR, or NULL if it was not found.
*/
USB_CONFIGURATION_DESCRIPTOR CODE* usbdpGetConfigurationDesc(uint8_t cfgValue, uint8_t cfgIndex)
{
   USB_CONFIGURATION_DESCRIPTOR CODE*pConfigurationDesc;
   usbdpInit();

   // As long as there are more configuration descriptors...
   while (pConfigurationDesc = usbdpFindNext(DESC_TYPE_CONFIG, 0))
   {  // Search by value?
      if (cfgValue)
      {
         if (cfgValue == pConfigurationDesc->bConfigurationValue) break;
      // Search by index? (search cfgIndex+1 times)
      }
      else if (!cfgIndex--)
      {
         break;
      }
   }

   return pConfigurationDesc;
}

/**
 * @brief	Locates an interface descriptor
*
* The function will first go to the configuration descriptor that matches the supplied configuration
* value, and then locate the interface descriptor that matches the given interface number and alternate
* setting.
*
* @note It is not necessary to call usbdpInit() before this function.
*
* @param[in]       cfgValue
*     The configuration value (USB_CONFIGURATION_DESCRIPTOR.bConfigurationValue)
* @param[in]       intNumber
*     The interface number (USB_INTERFACE_DESCRIPTOR.bInterfaceNumber)
* @param[in]       altSetting
*     The alternate setting (USB_INTERFACE_DESCRIPTOR.bAlternateSetting)
*
* @return
*     A pointer to the USB_INTERFACE_DESCRIPTOR, or NULL if it was not found.
*/
USB_INTERFACE_DESCRIPTOR CODE* usbdpGetInterfaceDesc(uint8_t cfgValue, uint8_t intNumber, uint8_t altSetting)
{
    USB_INTERFACE_DESCRIPTOR CODE*pInterfaceDesc;

    // First get to the correct configuration
    usbdpGetConfigurationDesc(cfgValue, 0);

    // Then find a match on the interface
    while (pInterfaceDesc = usbdpFindNext(DESC_TYPE_INTERFACE, DESC_TYPE_CONFIG))
    {
       if ((pInterfaceDesc->bInterfaceNumber == intNumber) && (pInterfaceDesc->bAlternateSetting == altSetting))
       {
          break;
       }
    }

    return pInterfaceDesc;
}

/**
 * @brief	Locates a string descriptor
* @note It is not necessary to call usbdpInit() before this function.
* @param[in] strIndex, a zero-based index that matches the "iXxxxxxxxxx" string indexes in the other descriptors
* @return A pointer to the USB_INTERFACE_DESCRIPTOR, or NULL if it was not found.
*/
USB_STRING_DESCRIPTOR CODE* usbdpGetStringDesc(uint8_t strIndex)
{
    USB_STRING_DESCRIPTOR CODE* pResult;
    usbdpInit();

#ifdef MS_EXT_C_ID
    if (strIndex == 0xEE)
    {
        do
        {        // Find the Microsoft OS String Descriptor
            pResult = usbdpFindNext(DESC_TYPE_STRING, 0);
        }while (pResult != NULL && pResult->bLength != 18);
    } else
#endif
    {
        do        // Search strIndex+1 times
        {
            pResult = usbdpFindNext(DESC_TYPE_STRING, 0);
        } while (strIndex--);
    }
    return pResult;
}


