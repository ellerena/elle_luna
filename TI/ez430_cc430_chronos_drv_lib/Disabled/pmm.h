/*
	File: PMM.h

	This file is used in conjunction with PMM.c to set the core
	voltage level of a device. To set a core voltage level, call
	SetVCore(level). See RF project(s) for example usage.

	Version 1.0 first
	07/14/07

*/

#ifndef __PMM
#define __PMM

#define	DEFAULT_VCORELEV		(3)

/**
 * @brief	Set the VCore to a new level
 * @param	level			PMM level ID
 */
void SetVCore(unsigned char level);


/************************ Macro definitions ************************/
#define	PMM_INIT	\
	SetVCore(DEFAULT_VCORELEV);									/* prepare core voltage */\
	PMMCTL0_H = 0xA5;											/* Set global high power request enable */\
	PMMCTL0_L |= PMMHPMRE;										\
	PMMCTL0_H = 0x00;

#endif                          /* __PMM */
