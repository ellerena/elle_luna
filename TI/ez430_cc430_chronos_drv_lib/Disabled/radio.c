/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

// system */
#include <stdint.h>
#include <msp430.h>

// driver */
#include "rf1a.h"
#include "timer.h"

// logic */
#include "rfsimpliciti.h"
//#include "bluerobin.h" */

#define	is_rf(x)	1
#define MRFI_RadioIsr(x)

/**************************************** Extern section ******************************************/
//extern void MRFI_RadioIsr(void);			/* SimpliciTI CC430 radio ISR - located in SimpliciTi library */
/*extern void BlueRobin_RadioISR_v(void);	BlueRobin CC430 radio ISR - located in BlueRobin library */

/**
 * @brief	Reset radio core.
 */
void radio_reset(void)
{
	volatile uint16_t i;
	uint8_t x;

	Strobe(RF_SRES);						/* Reset radio core */
	for (i = 0; i < 100; i++) ;				/* Wait before checking IDLE */
	do
	{
		x = Strobe(RF_SIDLE);
	}
	while ((x & 0x70) != 0x00);

	RF1AIFERR = 0;							/* Clear radio error register */
}

/**
 * @brief	Put radio to SLEEP mode.
 *
 * @verbatim
 * Chip bug: Radio does not come out of this SLEEP when put to sleep using the SPWD cmd.
 * However, it does wakes up if SXOFF was used to put it to sleep.
 * @endverbatim
 */
void radio_powerdown(void)
{
	Strobe(RF_SIDLE);
	Strobe(RF_SPWD);
}

/**
 * @brief	Put radio to SLEEP mode (XTAL off only).
 *
 * @verbatim
 * Chip bug: Radio does not come out of this SLEEP when put to sleep using the SPWD cmd.
 * However, it does wakes up if SXOFF was used to put it to sleep.
 * @endverbatim
 */
void radio_sxoff(void)
{
	Strobe(RF_SIDLE);
	Strobe(RF_SXOFF);
}

/**
 * @brief		Prepare radio for RF communication.
 */
void open_radio(void)
{
	radio_reset();							/* Reset radio core */
											/* Enable radio IRQ */
	RF1AIFG &= ~BIT4;						/* Clear a pending interrupt */
	RF1AIE |= BIT4;							/* Enable the interrupt */
}

/**
 * @brief	Shutdown radio for RF communication.
 */
void close_radio(void)
{
	RF1AIFG = 0;							/* Disable radio IRQ */
	RF1AIE = 0;

	radio_reset();							/* Reset radio core */
	radio_powerdown();						/* Put radio to sleep */
}

/**
 * @brief	GDO0/2 ISR to detect received packet.
 * 			In BlueRobin mode:capture packet end time and decode received packet
 * 			In SimpliciTI mode: go to SimpliciTI handler
 */
#pragma vector=CC1101_VECTOR
__interrupt void radio_ISR(void)
{
	uint8_t rf1aivec = RF1AIV;

	if (is_rf())								/* Forward to SimpliciTI interrupt service routine */
	{
		MRFI_RadioIsr();
	}
	else										/* BlueRobin packet end interrupt service routine */
	{
		if (rf1aivec == RF1AIV_RFIFG9)
		{
/*			if ((sBlueRobin.state == BLUEROBIN_SEARCHING) ||
				(sBlueRobin.state == BLUEROBIN_CONNECTED))
			{
				BlueRobin_RadioISR_v();
			}*/
		}
		else if (rf1aivec == RF1AIV_NONE)		/* RF1A interface interrupt (error etc.) */
		{
			__asm__ ("	nop");					/* break here */
		}
	}
}



