/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

#ifndef BUZZER_H_
#define BUZZER_H_

/**************************************** Include section ****************************************/

/**************************************** Prototypes section ****************************************/
extern void reset_buzzer(void);
extern void start_buzzer(uint16_t, uint16_t, uint16_t);
extern void stop_buzzer(void);
extern void toggle_buzzer(void);

/****************************************  Defines section ****************************************/

#define BUZZER_OFF					(0u)
#define BUZZ_ON_OUTPUT_DIS			(1u)
#define BUZZ_ON_OUTPUT_ENA			(2u)

#define	BUZZERPIN					BIT7

#define BUZZER_TIMER_STEPS			(5u)					/* Buzzer frequency = 32768/(BUZZER_TIMER_STEPS+1)/2 = 2.7kHz */
#define BUZZER_ON_TICKS				(MS_TO_TICK(20))		/* Buzzer on time */
#define BUZZER_OFF_TICKS			(MS_TO_TICK(200))		/* Buzzer off time */

/* Macro definitions */
#define BUZZER_INIT											\
		P2MAP7 = PM_TA1CCR0A;								/* connect P2.7 to TA0CCR1A or TA1CCR0A output */\
		P2OUT &= ~BUZZERPIN;								/* pull P2.7 output pin to LOW */\
		P2DIR |= BUZZERPIN;									/* set P2.7 direction as output */


/**************************************** Global Variable section ****************************************/


/**************************************** Extern section ****************************************/

#endif								/*BUZZER_H_ */
