/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

#ifndef __DISPLAY_H
#define __DISPLAY_H

/*************************************** Include section ***************************************/

/*************************************** Extern section  ***************************************/

/*************************************** Defines section ***************************************/
#ifndef BIT
#define	BIT(x)							(1 << x)
#endif

/* below macros must correspond with s_message_flags union */
#define	MSG_PREPARE						BIT(0)
#define	MSG_SHOW						BIT(1)
#define	MSG_ERASE						BIT(2)			/* erase full screen */
#define	MSG_LOCK						BIT(3)
#define	MSG_UNLOCK						BIT(4)
#define	MSG_LOBATT						BIT(5)
#define	MSG_ALARM_ON					BIT(6)
#define	MSG_ALARM_OFF					BIT(7)

/* below macros must correspond with s_display_flags union */
#define	DISP_UPD_FULL					BIT(0)
#define	DISP_UPD_LINE1					BIT(1)
#define	DISP_UPD_LINE2					BIT(2)
#define	DISP_UPD_TIME					BIT(3)
#define	DISP_UPD_STOPWATCH				BIT(4)
#define	DISP_UPD_TEMPERATURE			BIT(5)
#define	DISP_UPD_BATT_VOLT				BIT(6)
#define	DISP_UPD_DATE					BIT(7)
#define	DISP_UPD_ACCEL					BIT(8)
#define	DISP_UPD_ALARM					BIT(9)

#define	DISP_UPD_NONE					(0)

#define	DISPLAY_CLEAR_FLAGS				(0x3ffu)

/* Display function modes */
#define DISPLAY_LINE_UPD_FULL			BIT(0)
#define DISPLAY_LINE_UPD_PARTIAL		BIT(1)
#define DISPLAY_ICONS_OFF				BIT(2)

/* LCD blinking constants */
#define	LCDBLKCTL_DIV_8					LCDBLKDIV2 | LCDBLKDIV1 | LCDBLKDIV0
#define	LCDBLKCTL_DIV_4					LCDBLKDIV1 | LCDBLKDIV0

/* Definitions for line view style */
#define SHOW_1ST_PART					(0u)			/* LINE1 shows hours and minutes, LINE2: day and month */
#define SHOW_2ND_PART					(1u)			/* LINE1 displays seconds, LINE2: year */

/* Definitions for line access */
#define LINE1							(1u)
#define LINE2							(0u)

/* LCD display modes */
#define SEG_OFF							(0u)
#define SEG_ON							(1u)
#define SEG_ON_BLINK_ON					(2u)
#define SEG_ON_BLINK_OFF				(3u)
#define SEG_OFF_BLINK_OFF				(4u)

/* 7-segment character bit assignments */
#define SEG_A							(BIT4)
#define SEG_B							(BIT5)
#define SEG_C							(BIT6)
#define SEG_D							(BIT7)
#define SEG_E							(BIT2)
#define SEG_F							(BIT0)
#define SEG_G							(BIT1)

/* ------------------------------------------ */
/* LCD symbols for easier access */
//
/* xxx_SEG_xxx		= Seven-segment character (sequence 5-4-3-2-1-0) */
/* xxx_SYMB_xxx		= Display symbol, e.g. "AM" for ante meridiem */
/* xxx_UNIT_xxx		= Display unit, e.g. "km/h" for kilometers per hour */
/* xxx_ICON_xxx		= Display icon, e.g. heart to indicate reception of heart rate data */
/* xxx_L1_xxx			= Item is part of Line1 information */
/* xxx_L2_xxx			= Item is part of Line2 information */

/* Symbols for Line1 */
#define LCD_AM							0
#define LCD_PM							1
#define LCD_ARROW_UP					2
#define LCD_ARROW_DOWN					3
#define LCD_PERCENT						4

/* Symbols for Line2 */
#define LCD_TOTAL						5
#define LCD_AVERAGE						6
#define LCD_MAX							7
#define LCD_BATTERY						8

/* Units for Line1 */
#define LCD_L1_FT						9
#define LCD_L1_K						10
#define LCD_L1_M						11
#define LCD_L1_I						12
#define LCD_L1_PER_S					13
#define LCD_L1_PER_H					14
#define LCD_L1_DEGREE					15

/* Units for Line2 */
#define LCD_L2_KCAL						16
#define LCD_L2_KM						17
#define LCD_L2_MI						18

/* Icons */
#define LCD_ICON_HEART					19
#define LCD_ICON_STOPWATCH				20
#define LCD_ICON_RECORD					21
#define LCD_ICON_ALARM					22
#define LCD_ICON_BEEPER1				23
#define LCD_ICON_BEEPER2				24
#define LCD_ICON_BEEPER3				25

/* Line1 7-segments */
#define LCD_SEG_L1_3					26
#define LCD_SEG_L1_2					27
#define LCD_SEG_L1_1					28
#define LCD_SEG_L1_0					29
#define LCD_SEG_L1_COL					30
#define LCD_SEG_L1_DP1					31
#define LCD_SEG_L1_DP0					32

/* Line2 7-segments */
#define LCD_SEG_L2_5					33
#define LCD_SEG_L2_4					34
#define LCD_SEG_L2_3					35
#define LCD_SEG_L2_2					36
#define LCD_SEG_L2_1					37
#define LCD_SEG_L2_0					38
#define LCD_SEG_L2_COL1					39
#define LCD_SEG_L2_COL0					40
#define LCD_SEG_L2_DP					41

/* Line1 7-segment arrays */
#define LCD_SEG_L1_3_0					70
#define LCD_SEG_L1_2_0					71
#define LCD_SEG_L1_1_0					72
#define LCD_SEG_L1_3_1					73
#define LCD_SEG_L1_3_2					74

/* Line2 7-segment arrays */
#define LCD_SEG_L2_5_0					90
#define LCD_SEG_L2_4_0					91
#define LCD_SEG_L2_3_0					92
#define LCD_SEG_L2_2_0					93
#define LCD_SEG_L2_1_0					94
#define LCD_SEG_L2_5_2					95
#define LCD_SEG_L2_3_2					96
#define LCD_SEG_L2_5_4					97
#define LCD_SEG_L2_4_2					98

/* LCD controller memory map */
#define LCD_MEM_1						((uint8_t*)0x0A20)
#define LCD_MEM_2						((uint8_t*)0x0A21)
#define LCD_MEM_3						((uint8_t*)0x0A22)
#define LCD_MEM_4						((uint8_t*)0x0A23)
#define LCD_MEM_5						((uint8_t*)0x0A24)
#define LCD_MEM_6						((uint8_t*)0x0A25)
#define LCD_MEM_7						((uint8_t*)0x0A26)
#define LCD_MEM_8						((uint8_t*)0x0A27)
#define LCD_MEM_9						((uint8_t*)0x0A28)
#define LCD_MEM_10						((uint8_t*)0x0A29)
#define LCD_MEM_11						((uint8_t*)0x0A2A)
#define LCD_MEM_12						((uint8_t*)0x0A2B)

/* Memory assignment */
#define LCD_SEG_L1_0_MEM				(LCD_MEM_6)
#define LCD_SEG_L1_1_MEM				(LCD_MEM_4)
#define LCD_SEG_L1_2_MEM				(LCD_MEM_3)
#define LCD_SEG_L1_3_MEM				(LCD_MEM_2)
#define LCD_SEG_L1_COL_MEM				(LCD_MEM_1)
#define LCD_SEG_L1_DP1_MEM				(LCD_MEM_1)
#define LCD_SEG_L1_DP0_MEM				(LCD_MEM_5)
#define LCD_SEG_L2_0_MEM				(LCD_MEM_8)
#define LCD_SEG_L2_1_MEM				(LCD_MEM_9)
#define LCD_SEG_L2_2_MEM				(LCD_MEM_10)
#define LCD_SEG_L2_3_MEM				(LCD_MEM_11)
#define LCD_SEG_L2_4_MEM				(LCD_MEM_12)
#define LCD_SEG_L2_5_MEM				(LCD_MEM_12)
#define LCD_SEG_L2_COL1_MEM				(LCD_MEM_1)
#define LCD_SEG_L2_COL0_MEM				(LCD_MEM_5)
#define LCD_SEG_L2_DP_MEM				(LCD_MEM_9)
#define LCD_AM_MEM						(LCD_MEM_1)
#define LCD_PM_MEM						(LCD_MEM_1)
#define LCD_ARROW_UP_MEM				(LCD_MEM_1)
#define LCD_ARROW_DOWN_MEM				(LCD_MEM_1)
#define LCD_PERCENT_MEM					(LCD_MEM_5)
#define LCD_TOTAL_MEM					(LCD_MEM_11)
#define LCD_AVERAGE_MEM					(LCD_MEM_10)
#define LCD_MAX_MEM						(LCD_MEM_8)
#define LCD_BATTERY_MEM					(LCD_MEM_7)
#define LCD_L1_FT_MEM					(LCD_MEM_5)
#define LCD_L1_K_MEM					(LCD_MEM_5)
#define LCD_L1_M_MEM					(LCD_MEM_7)
#define LCD_L1_I_MEM					(LCD_MEM_7)
#define LCD_L1_PER_S_MEM				(LCD_MEM_5)
#define LCD_L1_PER_H_MEM				(LCD_MEM_7)
#define LCD_L1_DEGREE_MEM				(LCD_MEM_5)
#define LCD_L2_KCAL_MEM					(LCD_MEM_7)
#define LCD_L2_KM_MEM					(LCD_MEM_7)
#define LCD_L2_MI_MEM					(LCD_MEM_7)
#define LCD_ICON_HEART_MEM				(LCD_MEM_2)
#define LCD_ICON_STOPWATCH_MEM			(LCD_MEM_3)
#define LCD_ICON_RECORD_MEM				(LCD_MEM_1)
#define LCD_ICON_ALARM_MEM				(LCD_MEM_4)
#define LCD_ICON_BEEPER1_MEM			(LCD_MEM_5)
#define LCD_ICON_BEEPER2_MEM			(LCD_MEM_6)
#define LCD_ICON_BEEPER3_MEM			(LCD_MEM_7)

/* Bit masks for write access */
#define LCD_SEG_L1_0_MASK				(BIT2 + BIT1 + BIT0 + BIT7 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L1_1_MASK				(BIT2 + BIT1 + BIT0 + BIT7 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L1_2_MASK				(BIT2 + BIT1 + BIT0 + BIT7 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L1_3_MASK				(BIT2 + BIT1 + BIT0 + BIT7 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L1_COL_MASK				(BIT5)
#define LCD_SEG_L1_DP1_MASK				(BIT6)
#define LCD_SEG_L1_DP0_MASK				(BIT2)
#define LCD_SEG_L2_0_MASK				(BIT3 + BIT2 + BIT1 + BIT0 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L2_1_MASK				(BIT3 + BIT2 + BIT1 + BIT0 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L2_2_MASK				(BIT3 + BIT2 + BIT1 + BIT0 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L2_3_MASK				(BIT3 + BIT2 + BIT1 + BIT0 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L2_4_MASK				(BIT3 + BIT2 + BIT1 + BIT0 + BIT6 + BIT5 + BIT4)
#define LCD_SEG_L2_5_MASK				(BIT7)
#define LCD_SEG_L2_COL1_MASK			(BIT4)
#define LCD_SEG_L2_COL0_MASK			(BIT0)
#define LCD_SEG_L2_DP_MASK				(BIT7)
#define LCD_AM_MASK						(BIT1 + BIT0)
#define LCD_PM_MASK						(BIT0)
#define LCD_ARROW_UP_MASK				(BIT2)
#define LCD_ARROW_DOWN_MASK				(BIT3)
#define LCD_PERCENT_MASK				(BIT4)
#define LCD_TOTAL_MASK					(BIT7)
#define LCD_AVERAGE_MASK				(BIT7)
#define LCD_MAX_MASK					(BIT7)
#define LCD_BATTERY_MASK				(BIT7)
#define LCD_L1_FT_MASK					(BIT5)
#define LCD_L1_K_MASK					(BIT6)
#define LCD_L1_M_MASK					(BIT1)
#define LCD_L1_I_MASK					(BIT0)
#define LCD_L1_PER_S_MASK				(BIT7)
#define LCD_L1_PER_H_MASK				(BIT2)
#define LCD_L1_DEGREE_MASK				(BIT1)
#define LCD_L2_KCAL_MASK				(BIT4)
#define LCD_L2_KM_MASK					(BIT5)
#define LCD_L2_MI_MASK					(BIT6)
#define LCD_ICON_HEART_MASK				(BIT3)
#define LCD_ICON_STOPWATCH_MASK			(BIT3)
#define LCD_ICON_RECORD_MASK			(BIT7)
#define LCD_ICON_ALARM_MASK				(BIT3)
#define LCD_ICON_BEEPER1_MASK			(BIT3)
#define LCD_ICON_BEEPER2_MASK			(BIT3)
#define LCD_ICON_BEEPER3_MASK			(BIT3)

/*********************************** Global Variable section ***********************************/
typedef enum
{
   MENU_ITEM_NOT_VISIBLE = 0,				   /* Menu item is not visible */
   MENU_ITEM_VISIBLE 				           /* Menu item is visible */
} menu_t;

typedef union
{
	struct
	{											/* ***** Line1 + Line2 + Icons */
		uint16_t full_update			: 1;	/* 1 = Redraw all content */
												/* ***** Line only */
		uint16_t line1_full_update		: 1;	/* 1 = Redraw Line1 content */
		uint16_t line2_full_update		: 1;	/* 1 = Redraw Line2 content */
												/* ***** Logic module data update flags */
		uint16_t update_time			: 1;	/* 1 = Time was updated */
		uint16_t update_stopwatch		: 1;	/* 1 = Stopwatch was updated */
		uint16_t update_temperature		: 1;	/* 1 = Temperature was updated */
		uint16_t update_battery_voltage	: 1;	/* 1 = Battery voltage was updated */
		uint16_t update_date			: 1;	/* 1 = Date was updated */
		uint16_t update_acceleration	: 1;	/* 1 = Acceleration data was updated */
		uint16_t update_alarm			: 1;	/* 1 = Alarm time was updated */
												/* ***** miscelaneous (Eddie) */
/*		uint16_t use_metric_units		: 1;	 0: USA (12h, F, ft) , 1: METRIC SYSTEM (24h, C, m) */
	} flag;
	uint16_t flags;								/* Shortcut to all display flags (for reset) */
} s_display_flags;

typedef union
{
	struct
	{
		uint16_t prepare				: 1;	/* 1 = Wait for clock tick, then set flag.show */
		uint16_t show					: 1;	/* 1 = Display message now */
		uint16_t erase					: 1;	/* 1 = Erase message */
		uint16_t type_locked			: 1;	/* 1 = Show "buttons are locked" in Line2 */
		uint16_t type_unlocked			: 1;	/* 1 = Show "buttons are unlocked" in Line2 */
		uint16_t type_lobatt			: 1;	/* 1 = Show "lobatt" text in Line2 */
		uint16_t type_alarm_on			: 1;	/* 1 = Show "  on" text in Line1 */
		uint16_t type_alarm_off			: 1;	/* 1 = Show " off" text in Line1 */
		uint16_t type_error				: 1;	/* general error */
	} flag;
	uint16_t flags;								/* Shortcut to all message flags (for reset) */
} s_message_flags;

extern volatile s_message_flags message;
extern volatile s_display_flags display;		/* link variable for other modules */


/*************************************** API section ***************************************/
/* Macro definitions */
#define LCD_INIT0												/*Erase LCD memory. Init LCD peripheral.*/\
	LCDBMEMCTL |= LCDCLRBM + LCDCLRM;							/* Clear entire display memory */\
	LCDBCTL0 = (LCDDIV0 + LCDDIV1 + LCDDIV2 + LCDDIV3)			/* LCD_FREQ = ACLK/16/8 = 256Hz */\
				| (LCDPRE0 + LCDPRE1) | LCD4MUX | LCDON;		/* Frame frequency = 256Hz/4 = 64Hz, LCD mux 4, LCD on */\
	LCDBBLKCTL = LCDBLKPRE0 | LCDBLKPRE1 | LCDBLKDIV0			\
				| LCDBLKDIV1 | LCDBLKDIV2 | LCDBLKMOD0;			/* LCB_BLK_FREQ = ACLK/8/4096 = 1Hz */\
	P5SEL |= (BIT5 | BIT6 | BIT7);								/* I/O to COM outputs */\
	P5DIR |= (BIT5 | BIT6 | BIT7);								/* I/O to COM outputs */\
	LCDBPCTL0 = 0xFFFF;											/* Activate LCD output segments S0-S15 */\
	LCDBPCTL1 = 0x00FF;											/* Activate LCD output segments S16-S22 */\
	display.flags = 0;										/* reset/initialize our global variable */

#ifdef USE_LCD_CHARGE_PUMP
#define LCD_INIT LCD_INIT0										\
	LCDBVCTL = LCDCPEN | VLCD_2_72;								/* Charge pump voltage generated internally, internal bias (V2-V4) generation*/
#else
#define LCD_INIT LCD_INIT0
#endif

/* Display init / clear */
/*extern void lcd_init(void);*/
extern void clear_display(void);
extern void clear_display_all(void);
extern void clear_line(uint16_t);

/* Blinking function */
extern void start_blink(void);
extern void stop_blink(void);
extern void clear_blink_mem(void);
extern void set_blink_rate(uint16_t);

/* Character / symbol draw functions */
extern void display_char(uint16_t, uint16_t, uint16_t);
extern void display_chars(uint16_t, uint8_t *, uint16_t);
extern void display_symbol(uint16_t, uint16_t);

/* Time display function */
extern void display_am_pm_symbol(uint16_t);

/* Set_value display functions */
extern void display_value(uint16_t, uint32_t, uint16_t, uint16_t);

/* Integer to string conversion */
extern uint8_t *int_to_array(uint32_t, uint16_t, uint16_t);

/* Segment index helper function */
extern uint16_t switch_seg(uint16_t, uint16_t, uint16_t);

/*************************************** Extern section ***************************************/
extern void (*fptr_lcd_line1)(uint16_t, uint16_t);
extern void (*fptr_lcd_line2)(uint16_t, uint16_t);

#endif	/*DISPLAY_H_ */
