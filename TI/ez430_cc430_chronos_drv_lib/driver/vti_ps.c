/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

		VTI SCP1000-D0x pressure sensor driver functions
*************************************************************************************************/

/***************************************** Include section ****************************************/
// system
#include <stdint.h>
#include <msp430.h>
#include "bsp.h"

// driver
#include "vti_ps.h"

/*************************************** Prototypes section ***************************************/
uint16_t ps_read_reg(uint16_t, uint16_t);
uint16_t ps_write_reg(uint16_t, uint16_t);
static uint16_t ps_twi_rd(uint16_t);
static void twi_delay(void);

/***************************************** Defines section ****************************************/
#define	PS_R_REVID					(0x00)		/* ASIC revision mnumber */
#define	PS_R_DATAWR					(0x01)		/* indirect register access data */
#define	PS_R_ADDPTR					(0x02)		/* indirect register access pointer */
#define	PS_R_OPERATION				(0x03)		/* operation register */
#define	PS_R_OPSTATUS				(0x04)		/* operation status */
#define	PS_R_RSTR					(0x06)		/* ASIC software reset */
#define	PS_R_STATUS					(0x07)		/* ASIC top level status */
#define	PS_R_DATARD8				(0x7F)		/* pressure data MSB or 8bit data from eeprom (TWI)*/
#define	PS_R_DATARD16				(0x80)		/* pressure data LSB or 8bit data from indirect register (TWI) */
#define	PS_R_TEMPOUT				(0x81)		/* 14 bit temperature data (TWI) */
#define	PS_R_DATARD8_SPI			(0x1F)		/* pressure data MSB or 8bit data from eeprom (SPI) */
#define	PS_R_DATARD16_SPI			(0x20)		/* pressure data LSB or 8bit data from indirect register (SPI) */
#define	PS_R_TEMPOUT_SPI			(0x21)		/* 14 bit temperature data (SPI) */
#define	PS_R_CFG					(0x00)		/* configuration register */
#define	PS_R_TWIADD					(0x05)		/* TWI address */
#define	PS_R_USERDATA1				(0x29)		/* user data */
#define	PS_R_USERDATA2				(0x2A)		/* user data */
#define	PS_R_USERDATA3				(0x2B)		/* user data */
#define	PS_R_USERDATA4				(0x2C)		/* user data */

#define	PS_OP_NOP					(0x00)		/* no operation, cancel (standby mode).*/
#define	PS_OP_ADDPTR_R				(0x01)		/* read indirect access register pointed by ADDPTR */
#define	PS_OP_ADDPTR_W				(0x02)		/* wWrite indirect access register pointed by ADDPTR */
#define	PS_OP_ADDPTR_EEPROM_R		(0x05)		/* read EEPROM register pointed by ADDPTR */
#define	PS_OP_ADDPTR_EEPROM_W		(0x06)		/* write EEPROM register pointed by ADDPTR */
#define	PS_OP_INIT					(0x07)		/* perform INIT sequence */
#define	PS_OP_MM_HS					(0x09)		/* high speed acquisition start (continuous measurement). */
#define	PS_OP_MM_HR					(0x0A)		/* high resolution acquisition start (continuous measurement). */
#define	PS_OP_MM_ULP				(0x0B)		/* ultra low power acquisition start (continuous measurement). */
#define	PS_OP_MM_LP					(0x0C)		/* low power acquisition start (single Temp. and Press. measurement). */
#define	PS_OP_TEST					(0x0F)		/* asic self test */

#define	PS_RS_RESET					(0x01)		/* RSTR register RESET command */

/************************************ Global Variable section *************************************/
// VTI pressure (hPa) to altitude (m) conversion tables
const int16_t h0[17] = {\
						-153, 0, 111, 540, 989,\
						1457, 1949, 2466, 3012, 3591,\
						4206, 4865, 5574, 6344, 7185,\
						8117, 9164\
						};
const uint16_t p0[17] = {\
						1031, 1013, 1000, 950, 900,\
						850, 800, 750, 700, 650,\
						600, 550, 500, 450, 400,\
						350, 300\
						};

float p[17];

uint8_t ps_ok;										/* Global flag for proper pressure sensor operation */

/**************************************** Extern section ******************************************/

/**
 * @brief	Init pressure sensor I/O
 */
void ps_init(void)
{
	PS_INT_DIR &= ~PS_INT_PIN;										/* DRDY is input */
	PS_INT_IES &= ~PS_INT_PIN;										/* Interrupt on DRDY rising edge */
	PS_TWI_OUT |= (PS_SCL_PIN | PS_SDA_PIN);						/* SCL and SDA start at HIGH */
	PS_TWI_DIR |= (PS_SCL_PIN | PS_SDA_PIN);						/* SCL and SDA are outputs */

	ps_write_reg(PS_R_RSTR, PS_RS_RESET);							/* reset pressure sensor -> powerdown sensor */
	BSP_DELAY_USECS(50000u);
	BSP_DELAY_USECS(50000u);										/* aprox. 100msec wait here to allow reset completion */

	ps_ok = BIT0 & ps_read_reg(PS_R_STATUS, PS_TWI_8BIT);			/* read sensor's STATUS register LSB */

	if (ps_ok == 0)													/* Check: STATUS register LSB must be 0 */
	{		ps_ok = BIT0 &  ps_read_reg(PS_R_DATARD8, PS_TWI_8BIT);		/* read EEPROM checksum (DATARD8): LSB must be 1 */
	}
	else
	{
		ps_ok = 0;
	}
}

/**
 * @brief	Init pressure sensor registers and start sampling
 * @return	1=Sensor started, 0=Sensor did not start
 */
void ps_start(void)
{
	ps_write_reg(PS_R_OPERATION, PS_OP_MM_ULP);			/* Start sampling data in ultra low power mode */
}

/**
 * @brief	Power down pressure sensor
 */
void ps_stop(void)
{
	ps_write_reg(PS_R_OPERATION, PS_OP_NOP);			/* Put sensor to standby */
}

/**
 * @brief	low level control SDA & SCL to send START, RESTART or STOP bits and check ACK
 * @param	task: PS_TWI_SND_START, PS_TWI_SND_RESTART, PS_TWI_SND_STOP, PS_TWI_CHK_ACK
 * @return	1=ACK, 0=NACK
 */
static uint8_t ps_twi_sda(uint16_t task)
{
	uint8_t ack = 0;

	if (task == PS_TWI_SND_START)			/* START (high to low transition on SDA while SCL is high) */
	{
		PS_TWI_SDA_OUT						/* SDA is output */
		PS_TWI_SCL_HI						/* SCL HIGH */
		twi_delay();
		PS_TWI_SDA_LO						/* SDA LOW */
		twi_delay();
		PS_TWI_SCL_LO						/* SCL LOW */
		twi_delay();
	}
	else if (task == PS_TWI_SND_RESTART)
	{
		PS_TWI_SDA_OUT						/* SDA is output */
		PS_TWI_SCL_LO						/* SCL LOW */
		PS_TWI_SDA_HI						/* SDA HIGH */
		twi_delay();
		PS_TWI_SCL_HI						/* SCL HIGH */
		twi_delay();
		PS_TWI_SDA_LO						/* SDA LOW */
		twi_delay();
		PS_TWI_SCL_LO						/* SCL LOW */
		twi_delay();
	}
	else if (task == PS_TWI_SND_STOP)		/* STOP low to high transition on SDA, while SCL is high */
	{
		PS_TWI_SDA_OUT						/* SDA is output */
		PS_TWI_SDA_LO						/* SDA LOW */
		twi_delay();
		PS_TWI_SCL_LO						/* SCL LOW */
		twi_delay();
		PS_TWI_SCL_HI						/* SCL HIGH */
		twi_delay();
		PS_TWI_SDA_HI						/* SDA HIGH */
		twi_delay();
	}
	else if (task == PS_TWI_CHK_ACK)
	{
		PS_TWI_SDA_IN						/* SDA is input */
		PS_TWI_SCL_LO
		twi_delay();
		PS_TWI_SCL_HI						/* SCL HIGH */
		twi_delay();
		ack = PS_TWI_IN & PS_SDA_PIN;
		PS_TWI_SCL_LO
	}

	return (ack ^ PS_SDA_PIN);				/* SDA = 0, ack = any; SDA = 1, ack = 0*/
}

/**
 * @brief	Delay between TWI signal edges.
 */
static void twi_delay(void)
{
	int i;
	for (i = 0; i < 3; i++)
	{
		__asm__ ("nop");
	}
}

/**
 * @brief	Clock out bits through SDA.
 * @param	data		Byte to send
 */
static void ps_twi_wr(uint16_t data)
{
	uint16_t mask;

	mask = BIT7;							/* Set MSB bit in mask (10000000b) */
	PS_TWI_SDA_OUT;							/* SDA is output */

	do										/* clock out from MSB down to LSB */
	{
		PS_TWI_SCL_LO						/* SCL=0 */

		if (data & mask)
			PS_TWI_SDA_HI					/* SDA=1 */
		else
			PS_TWI_SDA_LO					/* SDA=0 */

		twi_delay();						/* force delay to fit proper rate */
		PS_TWI_SCL_HI						/* SCL=1 */
		twi_delay();						/* force delay to fit proper rate */

		mask >>= 1;

	} while (mask);							/* until 8 bits transfer completed */

	PS_TWI_SCL_LO							/* SCL=0 */
	PS_TWI_SDA_IN							/* SDA is input */
}

/**
 * @brief	Read 1 byte from SDA
 * @param	ack		1=Send ACK after read, 0=Send NACK after read
 * @return	byte read (returned as 16 bits but only 8LSB are valid)
 */
static uint16_t ps_twi_rd(uint16_t ack)
{
	uint16_t i, data = 0;

	PS_TWI_SDA_IN							/* SDA is input */

	for (i = 0; i < 8; i++)					/* loop to read 8 bits consecutively */
	{
		PS_TWI_SCL_LO						/* SCL=0 */
		twi_delay();
		PS_TWI_SCL_HI						/* SCL=1 */
		twi_delay();
		data <<= 1;							/* Shift previously captured bits to left */

		if (PS_TWI_IN & PS_SDA_PIN)			/* check SDA input value */
		{
			data |= BIT0;					/* Capture new bit */
		}
	}
											/* 1 aditional clock phase to generate master ACK */
	PS_TWI_SDA_OUT							/* SDA is output */
	PS_TWI_SCL_LO							/* SCL=0 */

	if (ack)
		PS_TWI_SDA_LO						/* Send ack -> continue read */
	else
		PS_TWI_SDA_HI						/* Send nack -> stop read */

	twi_delay();
	PS_TWI_SCL_HI							/* SCL=1 */
	twi_delay();
	PS_TWI_SCL_LO							/* SCL=0 */

	return data;
}

/**
 * @brief	Write a byte to the pressure sensor
 * @param	address	Register address
 * 			data	Data to write
 * 	@return	ACK(1)/NACK(0) returned by sensor
 */
uint16_t ps_write_reg(uint16_t address, uint16_t data)
{
	uint16_t ack;

	ps_twi_sda(PS_TWI_SND_START);					/* Generate start condition */
	ps_twi_wr(PS_TWI_ADR | PS_TWI_WR);				/* Send 7bit device address + write bit */

	ack = ps_twi_sda(PS_TWI_CHK_ACK);				/* Check ACK from device */
	if (ack)
	{
		ps_twi_wr(address);							/* Send 8bit register address */

		ack = ps_twi_sda(PS_TWI_CHK_ACK);			/* Check ACK from device */
		if (ack)
		{
			ps_twi_wr(data);						/* Send 8bit data to register */

			ack = ps_twi_sda(PS_TWI_CHK_ACK);		/* Check ACK from device */
			ps_twi_sda(PS_TWI_SND_STOP);			/* Generate stop condition */
		}
	}
	return ack;
}

/**
 * @brief	Read a byte from the pressure sensor
 * @param	address	Register address
 * 			mode	PS_TWI_8BIT, PS_TWI_16BIT
 * @return	Register content
 */
uint16_t ps_read_reg(uint16_t address, uint16_t mode)
{
	uint16_t ack;

	ps_twi_sda(PS_TWI_SND_START);					/* Generate start condition */
	ps_twi_wr(PS_TWI_ADR | PS_TWI_WR);				/* Send ps device address + write bit */

	ack = ps_twi_sda(PS_TWI_CHK_ACK);				/* Check ACK from device */
	if (ack)
	{
		ps_twi_wr(address);							/* Send 8bit register address */

		ack = ps_twi_sda(PS_TWI_CHK_ACK);			/* Check ACK from device */
		if (ack)
		{
			ps_twi_sda(PS_TWI_SND_RESTART);			/* Generate restart condition */
			ps_twi_wr(PS_TWI_ADR| PS_TWI_RD);		/* Send ps device address + read bit */

			ack = ps_twi_sda(PS_TWI_CHK_ACK);		/* Check ACK from device */
			if (ack)								/* if we get here safely, get the cash! (re-use variable ack) */
			{
				if (mode)							/* mode = PS_TWI_16BIT */
				{
					ack = (ps_twi_rd(1) << 8);		/* Read MSB 8bit data from register (and send ACK) */
				}
				else								/* mode = PS_TWI_8BIT */
				{
					ack = 0;
				}

				ack |= ps_twi_rd(0);				/* Read LSB 8bit data from register (don't send ACK) */
				ps_twi_sda(PS_TWI_SND_STOP);		/* Generate stop condition */
			}
		}
	}

	return ack;
}

/**
 * @brief	Read out pressure. Format is Pa. Range is 30000 .. 120000 Pa.
 * @return	17-bit pressure sensor value (Pa)
 */
uint32_t ps_get_pa(void)
{
	uint32_t data = 0;
														/* Get 3 MSB from DATARD8 register */
	data = ps_read_reg(PS_R_DATARD8, PS_TWI_8BIT);		/* read 8bit data from sensor */
	data = ((data & 0x07) << 16);						/* take only 3LSB and push them to high word */
	data |= ps_read_reg(PS_R_DATARD16, PS_TWI_16BIT);	/* read 16 bit and load them to low word */
	data >>= 2;											/* convert decimal value to Pa (divide by 4)*/

	return data;
}

/**
 * @brief	Read out temperature.
 * @return	14-bit temperature value in +/-xx.x K format
 */
uint16_t ps_get_temp(void)
{
	int16_t data;

	data = ps_read_reg(PS_R_TEMPOUT, PS_TWI_16BIT);		/* read 14 bit from TEMPOUT register */
	data >>= 1;											/* divide by 2 */

	if (data & BITC)									/* if data is negative*/
	{
		data |= 0xE000;									/* sign extend temperature */
	}

	data += 2732;

	return (uint16_t)data;
}

/**
 * @brief	Init pressure table with constants
 * @param	p	Pressure (Pa)
 * @return	Altitude (m)
 */
void init_pressure_table(void)
{
	uint16_t i;

	for (i = 0; i < 17; i++)
	{
		p[i] = p0[i];
	}
}

/**
 * @brief	Calculate pressure table for reference altitude. Implemented straight from VTI reference code.
 * @param	href	Reference height
 * 			p_measPressure (Pa)
 * 			t_measTemperature (10*K)
 */
void update_pressure_table(int16_t href, uint32_t p_meas, uint16_t t_meas)
{
	const float Invt00 = 0.003470415, coefp = 0.00006;
	volatile float t0, p_fact, p_noll, hnoll, h_low = 0;
	uint16_t i;
															/* Typecast arguments */
	volatile float fl_href = href;
	volatile float fl_p_meas = (float)p_meas / 100;			/* Convert from Pa to hPa */
	volatile float fl_t_meas = (float)t_meas / 10;			/* Convert from 10 K to 1 K */

	t0 = fl_t_meas + (0.0065 * fl_href);
	hnoll = fl_href / (t0 * Invt00);

	for (i = 0; i <= 15; i++)
	{
		if (h0[i] > hnoll)
		{
			break;
		}
		h_low = h0[i];
	}

	p_noll = (float)(hnoll - h_low) * (1 - (hnoll - (float)h0[i]) * coefp) *
		((float)p0[i] - (float)p0[i - 1]) / ((float)h0[i] - h_low) + (float)p0[i - 1];

	p_fact = fl_p_meas / p_noll;					/* Calculate multiplicator */

	for (i = 0; i <= 16; i++)						/* Apply correction factor to pressure table */
	{
		p[i] = p0[i] * p_fact;
	}
}

/**
 * @brief	Convert pressure (Pa) to altitude (m) using a conversion table implemented straight from VTI reference code.
 * @param	p_measPressure (Pa)
 * 			t_measTemperature (10*K)
 * 	@return	Altitude (m)
 */
int16_t conv_pa_to_meter(uint32_t p_meas, uint16_t t_meas)
{
	const float coef2 = 0.0007, Invt00 = 0.003470415;
	volatile float hnoll, t0, p_low, fl_h;
	volatile int16_t h;
	uint16_t i;
													/* Typecast arguments */
	volatile float fl_p_meas = (float)p_meas / 100;	/* Convert from Pa to hPa */
	volatile float fl_t_meas = (float)t_meas / 10;	/* Convert from 10 K to 1 K */

	for (i = 0; i <= 16; i++)
	{
		if (p[i] < fl_p_meas)
			break;
		p_low = p[i];
	}

	if (i == 0)
	{
		hnoll = (float)(fl_p_meas - p[0]) / (p[1] - p[0]) * ((float)(h0[1] - h0[0]));
	}
	else if (i < 15)
	{
		hnoll = (float)(fl_p_meas - p_low) *
			(1 - (fl_p_meas - p[i]) * coef2) / (p[i] - p_low) * ((float)(h0[i] - h0[i - 1])) + h0[i - 1];
	}
	else if (i == 15)
	{
		hnoll = (float)(fl_p_meas - p_low) / (p[i] - p_low) * ((float)(h0[i] - h0[i - 1])) + h0[i - 1];
	}
	else											/* i==16 */
	{
		hnoll = (float)(fl_p_meas - p[16]) / (p[16] - p[15]) * ((float)(h0[16] - h0[15])) + h0[16];
	}
													/* Compensate temperature error */
	t0 = fl_t_meas / (1 - hnoll * Invt00 * 0.0065);
	fl_h = Invt00 * t0 * hnoll;
	h = (uint16_t) fl_h;

	return (h);
}

