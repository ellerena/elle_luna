/*************************************************************************************************

	Actual revision: $Revision: $
	Revision label:$Name: $
	Revision state:$State: $

*************************************************************************************************
	Radio core access functions. Taken from TI reference code for CC430.
*************************************************************************************************/

/*************************************** Prototypes section ***************************************/
uint8_t Strobe(uint8_t);
unsigned char ReadSingleReg(uint8_t);
void WriteSingleReg(uint8_t, uint8_t);
void ReadBurstReg(uint8_t, uint8_t *, uint8_t);
void WriteBurstReg(uint8_t, uint8_t *, uint8_t);
void ResetRadioCore(void);
void WritePATable(uint8_t);
void WaitForXT2(void);
