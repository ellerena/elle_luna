/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

		VTI CMA3000-D0x acceleration sensor driver functions
 *************************************************************************************************/

/*=================================================================================================
							CMA3000-D0x acceleration sensor configuration

 The command byte of SPI bus is :
	A	A	A	A	A	A	RW	0(LSB)
	|					|	|	*---- always zero
	|					|	*-------- Read / Write Bit
	<	-	-	-	-	>------------ Register address

	so the real byte sent to SPI bus must be calculated:	BYTE = (Address * 4) + RW*2;

=================================================================================================*/


/***************************************** Include section ****************************************/
/* system */
#include <stdint.h>
#include "bsp.h"

/* driver */
#include "vti_as.h"

/*************************************** Prototypes section ***************************************/

/***************************************** Defines section ****************************************/
#ifndef BIT
	#define	BIT(x)					(1 << x)
#endif
														/***** GENERAL DEFINITIONS *****/
#define	ACC_XYZ_SIZE				(1)					/* size in bytes of x, y, z data in ram */
														/***** REGISTER DEFINITIONS *****/
#define	ACC_REG_WHO_AM_I			(0x00)				/* identification register (r) */
#define	ACC_REG_REVID				(0x01)				/* asic revision ID, fixed in metal */
#define	ACC_REG_CTRL				(0x02)				/* configuration register */
#define	ACC_REG_STATUS				(0x03)				/* status (por, eeprom parity) */
#define	ACC_REG_RSTR				(0x04)				/* reset register */
#define	ACC_REG_INT_STATUS			(0x05)				/* interrupt status register */
#define	ACC_REG_DOUTX				(0X06)				/* x channel output data register */
#define	ACC_REG_DOUTY				(0X07)				/* y channel output data register */
#define	ACC_REG_DOUTZ				(0X08)				/* z channel output data register */
#define	ACC_REG_MDTHR				(0X09)				/* motion detection threshold value register */
#define	ACC_REG_MDFFTMR				(0X0A)				/* free fall and motion detection time register */
#define	ACC_REG_FFTHR				(0X0B)				/* free fall threshold value register */
#define	ACC_REG_I2C_ADDR			(0X0C)				/* I2C device address */

														/***** BIT FIELD DEFINITIONS *****/
#define	ACC_CTRL_G_RANGE_8G			(0x00)
#define	ACC_CTRL_G_RANGE_2G			BIT(7)
#define	ACC_CTRL_INT_LEVEL_HIGH		(0x00)
#define	ACC_CTRL_INT_LEVEL_LOW		BIT(6)
#define	ACC_CTRL_MDET_EXIT_MM		(0x00)
#define	ACC_CTRL_MDET_EXIT_MD		BIT(5)
#define	ACC_CTRL_I2C_ENABLE			(0x00)
#define	ACC_CTRL_I2C_DISABLE		BIT(4)
#define	ACC_CTRL_MODE_POW_DOWN		(0 << 1)
#define	ACC_CTRL_MODE_MM100			(1 << 1)			/* measurement mode @ 100Hz ODR */
#define	ACC_CTRL_MODE_MM400			(2 << 1)			/* measurement mode @ 400Hz ODR */
#define	ACC_CTRL_MODE_MM40			(3 << 1)			/* measurement mode @ 40Hz ODR */
#define	ACC_CTRL_MODE_MDET10		(4 << 1)			/* motion detection mode @ 10Hz ODR */
#define	ACC_CTRL_MODE_FF100			(5 << 1)			/* free fall detection mode @ 100Hz ODR */
#define	ACC_CTRL_MODE_FF400			(6 << 1)			/* free fall detection mode @ 400Hz ODR */
#define	ACC_CTRL_MODE_POW_DOWN2		(7 << 1)
#define	ACC_CTRL_INT_ENABLE			(0x00)
#define	ACC_CTRL_INT_DISABLE		BIT(0)
														/* DCO frequency division factor for SPI speed */
#define AS_BR_DIVIDER				(24u)				/* speed = 12MHz / AS_BR_DIVIDER (max. 500kHz) */

														/** AS configuration to load into ACC_REG_CTRL **/
														/* valid sample rates @ 2g range: 100, 400 */
														/* valid sample rates @ 8g range: 40, 100, 400 */
#define AS_CONFIG_INIT		(	ACC_CTRL_INT_ENABLE		/* enable AS interrupts to master */\
							|	ACC_CTRL_MODE_MM100		/* measurement mode sample rate */\
							|	ACC_CTRL_I2C_DISABLE	/* disable I2C interface */\
							|	ACC_CTRL_MDET_EXIT_MM	/* motion detection exit: go to MM */\
							|	ACC_CTRL_INT_LEVEL_HIGH	/* interrupt pin will toggle high when triggered */\
							|	ACC_CTRL_G_RANGE_2G		/* acceleration range */\
							)
#define RbW_BIT						BIT(1)				/* read (low) or write (high) bit */

														/******************* MACROS ******************/
#define	COMMAND_RD(x)		(x <<= 2)					/*RW bit set to zero & shift address by 2*/
#define	COMMAND_WR(x)		(x = ((x << 2) | RbW_BIT))	/*RW bit set to one & shift address by 2*/

/************************************ Global Variable section *************************************/
uint8_t as_ok;											/* global flag for proper acc. sensor operation */

/**************************************** Extern section ******************************************/

/***************************************** Local section ****************************************/
/*
 * @brief	low level transmits/receives data to AS via SPI
 * @param	tempA, is the transmitted command (register address + r/w bit)
 * 			tempB, is the data to be transmitted
 * @return	1 byte received from AS via SPI
 */
static uint8_t as_transceive(uint16_t tempA, uint16_t tempB)
{
	bspIState_t v;

	AS_SPI_REN &= ~AS_SDI_PIN;							/* remove SPI MISO pull down resistor */
	AS_CSN_OUT &= ~AS_CSN_PIN;							/* pull down SPI_CSB pin - this selects the sensor */

	BSP_ENTER_CRITICAL_SECTION(v);

	AS_TX_BUFFER = tempA;								/* send address + r/w command to AS */

	while(!(AS_IRQ_REG & AS_TX_IFG));					/* wait for TX to be ready for new data */

	AS_TX_BUFFER = tempB;								/* transmit 2nd byte */

	while(AS_SPI_STA & AS_SPI_BSY);						/* wait for SPI transmission to complete */

	__bsp_RESTORE_ISTATE__(v);

	AS_CSN_OUT |= AS_CSN_PIN;							/* de-select acceleration sensor */
	AS_SPI_REN |= AS_SDI_PIN;							/* re-enable SPI_CSB pull down resistor */

	return AS_RX_BUFFER;								/* Return RX buffer (also clears OVR flag) */
}

/**
 * @brief	Read a byte from the acceleration sensor
 * @param	bAddress	Register address
 * @return	bResult		Register content or 0 if there was an error.
 */
static uint8_t as_read_register(uint16_t bAddress)
{
	COMMAND_RD(bAddress);								/* build the command to read from an address */
	return as_transceive(bAddress, bAddress);			/* Return new data from RX buffer */
}

/**
 * @brief	Write a byte to the acceleration sensor
 * @param	bAddress		Register address
 * 			bData			Data to write
 * @return	0 or bResult	Register content.
 * 							If the returned value is 0, there was an error.
 */
static uint8_t as_write_register(uint16_t bAddress, uint16_t bData)
{
	COMMAND_WR(bAddress);								/* build the command to write to an address */
	return as_transceive(bAddress, bData);				/* Return new data from RX buffer */
}

#ifdef AS_OFF_WHEN_NOT_IN_USE
/*
 * @brief	small routine to power off and disconnect the AS
 */
static void as_poweroff(void)
{
	AS_PWR_OUT &= ~(AS_PWR_PIN | AS_CSN_PIN);				/* VDD = 0, CSN = 1 */
	AS_PWR_DIR |= (AS_PWR_PIN | AS_CSN_PIN);				/* set as output to avoid floating pins */
	AS_INT_OUT &= ~AS_INT_PIN;								/* bring low to avoid floating pins */
	AS_INT_DIR |= AS_INT_PIN;								/* set as output to avoid floating pins */
	AS_SPI_OUT &= ~(AS_SDO_PIN + AS_SDI_PIN + AS_SCK_PIN);	/* bring low to avoid floating pins */
	AS_SPI_DIR |= AS_SDO_PIN + AS_SDI_PIN + AS_SCK_PIN;		/* set as output to avoid floating pins */
}
#else
static void as_poweron(void)
{
	AS_INT_DIR &= ~AS_INT_PIN;								/* Input */
	AS_SPI_DIR &= ~AS_SDI_PIN;								/* Input */
	AS_SPI_DIR |= AS_SDO_PIN + AS_SCK_PIN;					/* Output */
	AS_SPI_SEL |= AS_SDO_PIN + AS_SDI_PIN + AS_SCK_PIN;		/* Port pins to SDO, SDI and SCK function */
	PJOUT |= (AS_CSN_PIN | AS_PWR_PIN);						/* CSN=1  VDD=1 */
	PJDIR |= (AS_CSN_PIN | AS_PWR_PIN);
}
#endif

/****************************************** Main section ******************************************/

/**
 * @brief	Setup acceleration sensor connection, do not power up yet
 */
void as_init(void)
{
#ifdef AS_OFF_WHEN_NOT_IN_USE							/* power off and disconnect sensor */

	as_poweroff();
														/***** initialize SPI parameters *****/
	AS_SPI_CTL0 = (UCSYNC | UCMST | UCMSB | UCCKPH);	/* SPI master, 8bits, MSB 1st, clk idle low, data output on falling edge */
	AS_SPI_CTL1 = (UCSSEL1 | UCSWRST);					/* clock source SMCLK, keep SPI peripheral in reset for now */
	AS_SPI_BR0 = AS_BR_DIVIDER;							/* Low byte of division factor for baud rate */
	AS_SPI_BR1 = 0x00;									/* High byte of division factor for baud rate */
	AS_INT_IES &= ~AS_INT_PIN;							/* interrupt pin detection trigger on rising edge */

#else
	as_poweron();
#endif

	as_ok = AS_ST_OK;									/* Reset global sensor flag */
}

/**
 * @brief	Power-up and initialize acceleration sensor
 */
void as_start(void)
{
/*	uint16_t bConfig, bStatus; */

#ifdef AS_OFF_WHEN_NOT_IN_USE							/* activate our SPI interface */
	AS_INT_DIR &= ~AS_INT_PIN;							/* switch INT pin to input */
	AS_SPI_DIR &= ~AS_SDI_PIN;							/* switch SDI pin to input */
	AS_SPI_REN |= AS_SDI_PIN;							/* pulldown on SDI pin */
	AS_SPI_SEL |= AS_SDO_PIN + AS_SDI_PIN + AS_SCK_PIN;	/* connect port pins to SDO, SDI and SCK function */
	PJOUT |= (AS_CSN_PIN | AS_PWR_PIN);					/* deselect acceleration sensor, Power on active high */
#endif
	AS_SPI_CTL1 &= ~UCSWRST;							/* release SPI hardware reset state */

	BSP_DELAY_USECS(10*1000);							/* delay >5ms required before configuring AS after a power on */

	AS_INT_IFG &= ~AS_INT_PIN;							/* reset SPI interrupt flag */
	AS_INT_IE |= AS_INT_PIN;							/* enable SPI interrupt detection */

	as_write_register(ACC_REG_RSTR, 0x02);				/* reset sensor sequence part 1 */
	as_write_register(ACC_REG_RSTR, 0x0A);				/* reset sensor sequence part 2 */
	as_write_register(ACC_REG_RSTR, 0x04);				/* reset sensor sequence part 3 */

	BSP_DELAY_USECS(5*1000u);							/* wait 5 ms for AS to internally reset */

	as_write_register(ACC_REG_CTRL, AS_CONFIG_INIT);	/* send the configuration value to the AS to take effect */
}

/**
 * @brief	Power down acceleration sensor
 */
void as_stop(void)
{
	AS_INT_IE &= ~AS_INT_PIN;								/* disable interrupt detection on our side */

#ifdef AS_OFF_WHEN_NOT_IN_USE								/* Power-down sensor */
	as_poweroff();
	AS_SPI_SEL &= ~(AS_SDO_PIN + AS_SDI_PIN + AS_SCK_PIN);	/* set SPI port pins to common GPIO I/O function */
#else														/* Reset sensor -> sensor to powerdown */
	as_write_register(ACC_REG_RSTR, 0x02);					/* reset sensor sequence part 1 */
	as_write_register(ACC_REG_RSTR, 0x0A);					/* reset sensor sequence part 2 */
	as_write_register(ACC_REG_RSTR, 0x04);					/* reset sensor sequence part 3 */
#endif
}

/**
 * @brief	Service routine to read acceleration values.
 */
void as_get_data(AS_DAT_TYPE * data)
{
	*data++ = as_read_register(ACC_REG_DOUTX);	/* Store X acceleration data in buffer */
	*data++ = as_read_register(ACC_REG_DOUTY);	/* Store Y acceleration data in buffer */
	*data = as_read_register(ACC_REG_DOUTZ);	/* Store Z acceleration data in buffer */
}





