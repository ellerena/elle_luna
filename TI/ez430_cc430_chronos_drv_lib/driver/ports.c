/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/***************************** Include section *****************************/
// system
#include <msp430.h>
#include <stdint.h>

// driver
#include "ports.h"
#include "timer.h"
#include "display.h"

/***************************** Defines section *****************************/


/***************************** Prototypes section *****************************/
void button_repeat_on(void);
void button_repeat_off(void);
void button_repeat_function(void);

/***************************** Global Variable section *****************************/
volatile s_button_flags button;
volatile struct struct_button sButton;

/**
 * @brief	Starts button auto repeat timer.
*/
void button_repeat_on(void)
{
	fptr_Timer0_A3 = button_repeat_function;		/* Set Timer0_A3 to run button repeat function */
	Timer0_A3_Start(MS_TO_TICK(BUTTONS_REPEAT_MS));	/* Start Timer0_A3 IRQ to loop button repeat function */
}

/**
 * @brief	Stop button auto repeat timer.
*/
void button_repeat_off(void)
{
    Timer0_A3_Stop();									/* Disable Timer0_A3 IRQ */
}

/**
 * @brief	counts how many times up/down buttons remain continuously pressed.
 *			Everytime it's called, it checks if UP/DOWN are pressed
 *			and it'll rise a button flag after several positive checks
*/
void button_repeat_function(void)
{
	static uint8_t start_delay = BUTTON_2S;				/* Confirm BUTTON_2S repetitions before flagging button UP/DOWN */
	uint8_t temp;

	temp = (BUTTONS_IN & BUTTON_UP_DOWN);				/* capture UP, DOWN buttons status */

	if (temp)											/* if a button (UP or DOWN) was found pressed */
	{
		if (start_delay == 0)							/* number of tries completed: long pressed confirmed!! */
		{
			button.flags |= temp;						/* rise the proper flag for the pressed buttons */
		}
		else											/* threshold not yet met */
		{
			start_delay--;								/* just decrease counter to log this iteration */
		}
	}
	else												/* if we land here either button was released or never pressed */
	{
		sButton.repeats = 0;							/* Reset repeat counter */
		start_delay = BUTTON_2S;						/* Reset delay control */
		start_blink();									/* Enable blinking on the display */
	}

	if (start_delay == 0)								/* button long press confirmed */
	{
		sButton.repeats++;								/* Increase repeat counter */
		sTime.last = sTime.system;						/* Reset inactivity detection counter */
		stop_blink();									/* Disable blinking the display */
	}
}
