/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/************************************* Include section *************************************/
/* system */
#include <stdint.h>
#include <string.h>
#include <msp430.h>

/* driver */
#include "display.h"

/************************************* Prototypes section *************************************/

/************************************* Defines section *************************************/
#define	MAX_INT_ARRAY		(180)
/************************************* Local Variable section *************************************/

/* Table with memory bit assignment for digits "0" to "9" and characters "A" to "Z" */
const uint8_t lcd_font[] =
{
	SEG_A + SEG_B + SEG_C + SEG_D + SEG_E + SEG_F,				/* [0] displays character '0' */
	SEG_B + SEG_C,												/* [1] displays character '1' */
	SEG_A + SEG_B + SEG_D + SEG_E + SEG_G,						/* [2] displays character '2' */
	SEG_A + SEG_B + SEG_C + SEG_D + SEG_G,						/* [3] displays character '3' */
	SEG_B + SEG_C + SEG_F + SEG_G,								/* [4] displays character '4' */
	SEG_A + SEG_C + SEG_D + SEG_F + SEG_G,						/* [5] displays character '5' */
	SEG_A + SEG_C + SEG_D + SEG_E + SEG_F + SEG_G,				/* [6] displays character '6' */
	SEG_A + SEG_B + SEG_C,										/* [7] displays character '7' */
	SEG_A + SEG_B + SEG_C + SEG_D + SEG_E + SEG_F + SEG_G,		/* [8] displays character '8' */
	SEG_A + SEG_B + SEG_C + SEG_D + SEG_F + SEG_G,				/* [9] displays character '9' */
	SEG_A,														/* [10] displays character '~' (:) */
	SEG_D,														/* [11] displays character '_' (;)*/
	SEG_C + SEG_E + SEG_F + SEG_G,								/* [12] displays character 'h' (<)*/
	SEG_D + SEG_G,												/* [13] displays character '=' */
	0,															/* [14] displays character ' ' (>)*/
	SEG_A + SEG_B + SEG_E + SEG_G,								/* [15] displays character '?' */
	SEG_D + SEG_E + SEG_G,										/* [16] displays character 'c' (@)*/
	SEG_A + SEG_B + SEG_C + SEG_E + SEG_F + SEG_G,				/* [17] displays character 'A' */
	SEG_C + SEG_D + SEG_E + SEG_F + SEG_G,						/* [18] displays character 'B' */
	SEG_A + SEG_D + SEG_E + SEG_F,								/* [19] displays character 'C' */
	SEG_B + SEG_C + SEG_D + SEG_E + SEG_G,						/* [20] displays character 'D' */
	SEG_A + +SEG_D + SEG_E + SEG_F + SEG_G,						/* [21] displays character 'E' */
	SEG_A + SEG_E + SEG_F + SEG_G,								/* [22] displays character 'F' */
	// SEG_A + SEG_C + SEG_D + SEG_E + SEG_F + SEG_G,/* [23] displays character 'G' */
	SEG_A + SEG_B + SEG_C + SEG_D + SEG_F + SEG_G,				/* [24] displays character 'g' */
	SEG_B + SEG_C + SEG_E + SEG_F + SEG_G,						/* [25] displays character 'H' */
	SEG_E + SEG_F,												/* [26] displays character 'I' */
	SEG_A + SEG_B + SEG_C + SEG_D,								/* [27] displays character 'J' */
	// SEG_B + SEG_C + SEG_E + SEG_F + SEG_G,					/* [28] displays character 'k' */
	SEG_D + SEG_E + SEG_F + SEG_G,								/* [29] displays character 'K' */
	SEG_D + SEG_E + SEG_F,										/* [30] displays character 'L' */
	SEG_A + SEG_B + SEG_C + SEG_E + SEG_F,						/* [31] displays character 'M' */
	SEG_C + SEG_E + SEG_G,										/* [32] displays character 'N' */
	SEG_C + SEG_D + SEG_E + SEG_G,								/* [33] displays character 'O' */
	SEG_A + SEG_B + SEG_E + SEG_F + SEG_G,						/* [34] displays character 'P' */
	SEG_A + SEG_B + SEG_C + SEG_D + SEG_E + SEG_F,				/* [35] displays character 'Q' */
	SEG_E + SEG_G,												/* [36] displays character 'R' */
	SEG_A + SEG_C + SEG_D + SEG_F + SEG_G,						/* [37] displays character 'S' */
	SEG_D + SEG_E + SEG_F + SEG_G,								/* [38] displays character 'T' */
	SEG_C + SEG_D + SEG_E,										/* [39] displays character 'U' */
	SEG_C + SEG_D + SEG_E,										/* [40] displays character 'V' */
	SEG_G,														/* [41] displays character '-' (W)*/
	SEG_B + SEG_C + +SEG_E + SEG_F + SEG_G,						/* [42] displays character 'X' */
	SEG_B + SEG_C + SEG_D + SEG_F + SEG_G,						/* [43] displays character 'Y' */
	SEG_A + SEG_B + SEG_D + SEG_E + SEG_G						/* [44] displays character 'Z' */
};

/* Table with memory address for each display element */
const uint8_t *segments_lcdmem[] =
{
	LCD_AM_MEM,	/* 0 */\
	LCD_PM_MEM,	/* 1 */\
	LCD_ARROW_UP_MEM,/* 2 */\
	LCD_ARROW_DOWN_MEM,/* 3 */\
	LCD_PERCENT_MEM,/* 4 */\
	LCD_TOTAL_MEM,	/* 5 */\
	LCD_AVERAGE_MEM,/* 6 */\
	LCD_MAX_MEM,	/* 7 */\
	LCD_BATTERY_MEM,/* 8 */\
	LCD_L1_FT_MEM,	/* 9 */\
	LCD_L1_K_MEM,	/* 10 */\
	LCD_L1_M_MEM,	/* 11 */\
	LCD_L1_I_MEM,	/* 12 */\
	LCD_L1_PER_S_MEM,/* 13 */\
	LCD_L1_PER_H_MEM,/* 14 */\
	LCD_L1_DEGREE_MEM,/* 15 */\
	LCD_L2_KCAL_MEM,/* 16 */\
	LCD_L2_KM_MEM,	/* 17 */\
	LCD_L2_MI_MEM,	/* 18 */\
	LCD_ICON_HEART_MEM,	/* 19 */\
	LCD_ICON_STOPWATCH_MEM,/* 20 */\
	LCD_ICON_RECORD_MEM,/* 21 */\
	LCD_ICON_ALARM_MEM,	/* 22 */\
	LCD_ICON_BEEPER1_MEM,/* 23 */\
	LCD_ICON_BEEPER2_MEM,/* 24 */\
	LCD_ICON_BEEPER3_MEM,/* 25 */\
	LCD_SEG_L1_3_MEM,	/* 26 */\
	LCD_SEG_L1_2_MEM,	/* 27 */\
	LCD_SEG_L1_1_MEM,	/* 28 */\
	LCD_SEG_L1_0_MEM,	/* 29 */\
	LCD_SEG_L1_COL_MEM,	/* 30 */\
	LCD_SEG_L1_DP1_MEM,	/* 31 */\
	LCD_SEG_L1_DP0_MEM,	/* 32 */\
	LCD_SEG_L2_5_MEM,	/* 33 */\
	LCD_SEG_L2_4_MEM,	/* 34 */\
	LCD_SEG_L2_3_MEM,	/* 35 */\
	LCD_SEG_L2_2_MEM,	/* 36 */\
	LCD_SEG_L2_1_MEM,	/* 37 */\
	LCD_SEG_L2_0_MEM,	/* 38 */\
	LCD_SEG_L2_COL1_MEM,/* 39 */\
	LCD_SEG_L2_COL0_MEM,/* 40 */\
	LCD_SEG_L2_DP_MEM	/* 41 */
};

/* Table with bit mask for each display element */
const uint8_t segments_bitmask[] =
{
	LCD_AM_MASK,	/* 0 */\
	LCD_PM_MASK,	/* 1 */\
	LCD_ARROW_UP_MASK,/* 2 */\
	LCD_ARROW_DOWN_MASK,/* 3 */\
	LCD_PERCENT_MASK,/* 4 */\
	LCD_TOTAL_MASK,/* 5 */\
	LCD_AVERAGE_MASK,/* 6 */\
	LCD_MAX_MASK,	/* 7 */\
	LCD_BATTERY_MASK,/* 8 */\
	LCD_L1_FT_MASK,/* 9 */\
	LCD_L1_K_MASK,	/* 10 */\
	LCD_L1_M_MASK,	/* 11 */\
	LCD_L1_I_MASK,	/* 12 */\
	LCD_L1_PER_S_MASK,/* 13 */\
	LCD_L1_PER_H_MASK,/* 14 */\
	LCD_L1_DEGREE_MASK,/* 15 */\
	LCD_L2_KCAL_MASK,/* 16 */\
	LCD_L2_KM_MASK,/* 17 */\
	LCD_L2_MI_MASK,/* 18 */\
	LCD_ICON_HEART_MASK,/* 19 */\
	LCD_ICON_STOPWATCH_MASK,/* 20 */\
	LCD_ICON_RECORD_MASK,/* 21 */\
	LCD_ICON_ALARM_MASK,/* 22 */\
	LCD_ICON_BEEPER1_MASK,/* 23 */\
	LCD_ICON_BEEPER2_MASK,/* 24 */\
	LCD_ICON_BEEPER3_MASK,/* 25 */\
	LCD_SEG_L1_3_MASK,	/* 26 */\
	LCD_SEG_L1_2_MASK,	/* 27 */\
	LCD_SEG_L1_1_MASK,	/* 28 */\
	LCD_SEG_L1_0_MASK,	/* 29 */\
	LCD_SEG_L1_COL_MASK,/* 30 */\
	LCD_SEG_L1_DP1_MASK,/* 31 */\
	LCD_SEG_L1_DP0_MASK,/* 32 */\
	LCD_SEG_L2_5_MASK,	/* 33 */\
	LCD_SEG_L2_4_MASK,	/* 34 */\
	LCD_SEG_L2_3_MASK,	/* 35 */\
	LCD_SEG_L2_2_MASK,	/* 36 */\
	LCD_SEG_L2_1_MASK,	/* 37 */\
	LCD_SEG_L2_0_MASK,	/* 38 */\
	LCD_SEG_L2_COL1_MASK,/* 39 */\
	LCD_SEG_L2_COL0_MASK,/* 40 */\
	LCD_SEG_L2_DP_MASK	/* 41 */
};

/* Quick integer to array conversion table for most common integer values */
const uint8_t itoa_table[][3] =
{
	"000", "001", "002", "003", "004", "005", "006", "007", "008", "009",
	"010", "011", "012", "013", "014", "015", "016", "017", "018", "019",
	"020", "021", "022", "023", "024", "025", "026", "027", "028", "029",
	"030", "031", "032", "033", "034", "035", "036", "037", "038", "039",
	"040", "041", "042", "043", "044", "045", "046", "047",	"048", "049",
	"050", "051", "052", "053", "054", "055", "056", "057", "058", "059",
	"060", "061", "062", "063",	"064", "065", "066", "067", "068", "069",
	"070", "071", "072", "073", "074", "075", "076", "077", "078", "079",
	"080", "081", "082", "083", "084", "085", "086", "087",	"088", "089",
	"090", "091", "092", "093", "094", "095", "096", "097", "098", "099",
	"100", "101", "102", "103", "104", "105", "106", "107", "108", "109",
	"110", "111", "112", "113", "114", "115", "116", "117", "118", "119",
	"120", "121", "122", "123", "124", "125", "126", "127",	"128", "129",
	"130", "131", "132", "133", "134", "135", "136", "137", "138", "139",
	"140", "141", "142", "143",	"144", "145", "146", "147", "148", "149",
	"150", "151", "152", "153", "154", "155", "156", "157", "158", "159",
	"160", "161", "162", "163", "164", "165", "166", "167",	"168", "169",
	"170", "171", "172", "173", "174", "175", "176", "177", "178", "179", "180",
};

/************************************* Global variables *************************************/
volatile s_display_flags display;
uint8_t itoa_str[8];								/* Global return string for int_to_array function */

/************************************* Extern section *************************************/

/************************************* Main section *************************************/

/**
 * @brief	Returns index of 7-segment character. Required for display routines that can draw information on both lines.
 * @param	line	LINE1 or LINE2
 * 			index1	Index of LINE1
 * 			index2	Index of LINE2
 * @return	index2 or index2
*/
uint16_t switch_seg(uint16_t line, uint16_t index1, uint16_t index2)
{
	return (line == LINE1) ? index1 : index2;
}

/**
 * @brief	Generic integer to array routine. Converts integer n to string.
 * 				Default conversion result has leading zeros, e.g. "00123"
 * 				Option to convert leading '0' into whitespace (blanks)
 * @param	n		integer to convert
 * 			digits	number of digits
 * 			blanks	fill up result string with number of white spaces instead of leading zeros
 * @return	pointer to string
*/
uint8_t *int_to_array(uint32_t n, uint16_t digits, uint16_t blanks)
{
	uint8_t digits1 = digits;

	memset(itoa_str, '0', 7);						/* Preset result string */

	if (n <= MAX_INT_ARRAY)									/* numbers 0 .. 180 come from itoa_table */
	{
		if (digits >= 3)									/* if more than 3 digits, write after leading 0s */
		{
			memcpy(itoa_str + (digits - 3), itoa_table[n], 3);
		}
		else												/* if 1 or 2 digits, write from start of string */
		{
			memcpy(itoa_str, itoa_table[n] + (3 - digits), digits);
		}
	}
	else													/* For n > 180 calculate string content */
	{
		do													/* get digits from least to most significant number */
		{
			itoa_str[digits - 1] = n % 10 + '0';
			n /= 10;
		}
		while (--digits > 0);
	}

	digits = 0;
	while ((itoa_str[digits] == '0') && (digits < digits1 - 1))/* Remove specified number of leading '0', always keep last one */
	{
		if (blanks)
		{													/* Convert only specified number of leading '0' */
			itoa_str[digits] = ' ';
			blanks--;
		}
		digits++;
	}

	return (itoa_str);
}

/**
 * @brief	Write to one or multiple LCD segments
 * @param	lcdmem		Pointer to LCD byte memory
 * 			bits		Segments to address
 * 			bitmask		Bitmask for particular display item
 * 			mode		On, off or blink segments
*/
static void write_lcd_mem(uint8_t * lcdmem, uint8_t bits, uint8_t bitmask, uint8_t mode)
{
	if (mode == SEG_OFF)					/* if mode = (0) */
	{
		mode = *lcdmem;
		mode &= ~bitmask;					/* clear segments */
	}
	else if (mode == SEG_ON)				/* if mode = (1)*/
	{
		mode = *lcdmem;
		mode &= ~bitmask;					/* clear segments before writing */
		mode |= bits;						/* set visible segments */
	}
	else if (mode == SEG_ON_BLINK_ON)		/* if mode = (2) */
	{
		mode = *(lcdmem + 0x20);
		mode &= ~bitmask;					/* clear blink segments before writing */
		mode |= bits;						/* set blink segments */
		*(lcdmem + 0x20) = mode;

		mode = *lcdmem;
		mode &= ~bitmask;					/* clear segments before writing */
		mode |= bits;						/* set visible segments */
	}
	else if (mode == SEG_ON_BLINK_OFF)		/* if mode = (3) */
	{
		mode = *(lcdmem + 0x20);
		mode &= ~bitmask;					/* clear blink segments */
		*(lcdmem + 0x20) = mode;

		mode = *lcdmem;
		mode &= ~bitmask;					/* clear segments before writing */
		mode |= bits;						/* set visible segments */
	}
	else									/* if mode = (4) SEG_OFF_BLINK_OFF */
	{
		mode = *(lcdmem + 0x20);
		mode &= ~bitmask;					/* clear blink segments */
		*(lcdmem + 0x20) = mode;

		mode = *lcdmem;
		mode &= ~bitmask;					/* clear segments */
	}

	*lcdmem = mode;
}

/**
 * @fn		display_char
 * @brief	Write to 7-segment characters.
 * @param	uint8_t segment		A valid LCD segment
 * 			uint8_t chr			Character to display
 * 			uint8_t mode		SEG_ON, SEG_OFF, SEG_BLINK
*/
void display_char(uint16_t segment, uint16_t chr, uint16_t mode)
{
	uint8_t *lcdmem;										/* Pointer to LCD memory */
	uint8_t bitmask;										/* Bitmask for character */
	uint8_t bits;											/* Bits to write */

	if ((segment >= LCD_SEG_L1_3) && (segment <= LCD_SEG_L2_DP))
	{
		lcdmem = (uint8_t *) segments_lcdmem[segment];		/* Get LCD memory address for segment from table */
		bitmask = segments_bitmask[segment];				/* Get bitmask for character from table */

		if ((chr >= 0x30) && (chr <= 0x5A))					/* Get bits from font set */
		{
			bits = lcd_font[chr - 0x30];					/* Use font set */
		}
		else if (chr == 0x2D)
		{
			bits = SEG_G;									/* '-' not in font set */
		}
		else
		{
			bits = 0;										/* Other characters map to ' ' (blank) */
		}

		if (segment >= LCD_SEG_L2_5)						/* LINE2 7-segment characters need to swap high- and low-nibble, */
		{
			bits = ((bits << 4) & 0xF0) | ((bits >> 4) & 0x0F);

			if (segment == LCD_SEG_L2_5)					/* LCD_SEG_L2_5 need to convert ASCII '1' and 'L' to 1 bit, */
			{
				if ((chr == '1') || (chr == 'L'))
					bits = BIT7;
			}
		}

		write_lcd_mem(lcdmem, bits, bitmask, mode);			/* Physically write to LCD memory */
	}
}

/**
 * @brief	Write to consecutive 7-segment characters.
 * @param	uint8_t segments	LCD segment array
 * 			uint8_t * str		Pointer to a string
 * 			uint8_t mode		SEG_ON, SEG_OFF, SEG_BLINK
*/
void display_chars(uint16_t segments, uint8_t * str, uint16_t mode)
{
	uint8_t length;											/* Write length */
	uint8_t char_start;										/* Starting point for consecutive write */

	switch (segments)
	{														/* LINE1 */
		case LCD_SEG_L1_3_0:
			length = 4;
			char_start = LCD_SEG_L1_3;
			break;
		case LCD_SEG_L1_3_1:
			length = 3;
			char_start = LCD_SEG_L1_3;
			break;
		case LCD_SEG_L1_2_0:
			length = 3;
			char_start = LCD_SEG_L1_2;
			break;
		case LCD_SEG_L1_3_2:
			length = 2;
			char_start = LCD_SEG_L1_3;
			break;
		case LCD_SEG_L1_1_0:
			length = 2;
			char_start = LCD_SEG_L1_1;
			break;
															/* LINE2 */
		case LCD_SEG_L2_5_0:
			length = 6;
			char_start = LCD_SEG_L2_5;
			break;
		case LCD_SEG_L2_4_0:
			length = 5;
			char_start = LCD_SEG_L2_4;
			break;
		case LCD_SEG_L2_3_0:
			length = 4;
			char_start = LCD_SEG_L2_3;
			break;
		case LCD_SEG_L2_2_0:
			length = 3;
			char_start = LCD_SEG_L2_2;
			break;
		case LCD_SEG_L2_1_0:
			length = 2;
			char_start = LCD_SEG_L2_1;
			break;
		case LCD_SEG_L2_5_4:
			length = 2;
			char_start = LCD_SEG_L2_5;
			break;
		case LCD_SEG_L2_5_2:
			length = 4;
			char_start = LCD_SEG_L2_5;
			break;
		case LCD_SEG_L2_3_2:
			length = 2;
			char_start = LCD_SEG_L2_3;
			break;
		case LCD_SEG_L2_4_2:
			length = 3;
			char_start = LCD_SEG_L2_4;
			break;
		default:
			length = 0;
			char_start = 0;
	}

	for (segments = 0; segments < length; segments++)		/* Write to consecutive digits (reuse variable segments) */
	{
		display_char(char_start + segments,
						*(str + segments), mode);			/* Use single character routine to write display memory */
	}
}

/**
 * @fn		display_symbol
 * @brief	Switch symbol on or off on LCD.
 * @param	uint8_t symbol			A valid LCD symbol (index 0..42)
 * 			uint8_t state			SEG_ON, SEG_OFF, SEG_BLINK
*/
void display_symbol(uint16_t symbol, uint16_t mode)
{
	uint8_t *lcdmem, bits;

	if (symbol <= LCD_SEG_L2_DP)
	{
		lcdmem = (uint8_t *) segments_lcdmem[symbol];		/* Get LCD memory address for symbol from table */
		bits = segments_bitmask[symbol];					/* Get bits for symbol from table */
		write_lcd_mem(lcdmem, bits, bits, mode);			/* Write LCD memory */
	}
}

/**
 * @brief	Erase text line in a given line.
 * @param	line, can be LINE1, LINE2
*/
void clear_line(uint16_t line)
{
	display_chars(switch_seg(line, LCD_SEG_L1_3_0, LCD_SEG_L2_5_0), NULL, SEG_OFF);

	line = (line == LINE1) ? LCD_SEG_L1_COL : LCD_SEG_L2_COL1;
	display_symbol(line++, SEG_OFF);	/* clear LCD_SEG_L1_COL or LCD_SEG_L2_COL1 */
	display_symbol(line++, SEG_OFF);	/* clear LCD_SEG_L1_DP1 or LCD_SEG_L2_COL0 */
	display_symbol(line, SEG_OFF);		/* clear LCD_SEG_L1_DP0 or LCD_SEG_L2_DP */
}

/**
 * @brief	Erase text on LINE1 and LINE2 segments. Keep icons.
*/
void clear_display(void)
{
	clear_line(LINE1);
	clear_line(LINE2);
}

/**
 * @brief	Erase LINE1 and LINE2 segments. Clear also function-specific content.
*/
void clear_display_all(void)
{
	clear_display();							/* clear text on LINE1 and LINE2. Keep icons. */

	fptr_lcd_line1(LINE1, DISPLAY_ICONS_OFF);	/* clear icons (and maybe text) related to LINE1 menu */
	fptr_lcd_line2(LINE2, DISPLAY_ICONS_OFF);	/* clear icons (and maybe text) related to LINE2 menu */
}

/**
 * @brief	Generic decimal display routine. Used exclusively by set_value function.
 * @param	segments				LCD segments where value is displayed
 * 			value					Integer value to be displayed
 * 			digits					Number of digits to convert
 * 			blanks					Number of leadings blanks in int_to_array result string
*/
void display_value(uint16_t segments, uint32_t value, uint16_t digits, uint16_t blanks)
{
	uint8_t *str;

	str = int_to_array(value, digits, blanks);
	display_chars(segments, str, SEG_ON_BLINK_ON);			/* Display string in blink mode */
}

/**
 * @fn		display_am_pm_symbol
 * @brief	Display AM or PM symbol. If AM displays nothing.
 * @param	uint8_t hour		24H internal time format
*/
void display_am_pm_symbol(uint16_t hour)
{
	hour = (hour < 12) ? SEG_OFF : SEG_ON;
	display_symbol(LCD_PM, hour);
}

/**
 * @fn		start_blink
 * @brief	Start blinking.
*/
void start_blink(void)
{
	LCDBBLKCTL |= LCDBLKMOD0;
}

/**
 * @brief	Stop blinking.
*/
void stop_blink(void)
{
	LCDBBLKCTL &= ~LCDBLKMOD0;
}

/**
 * @brief	Clear blinking memory.
*/
void clear_blink_mem(void)
{
	LCDBMEMCTL |= LCDCLRBM;
}

/**
 * @fn		set_blink_rate
 * @brief	Set blink rate register bits.
*/
void set_blink_rate(uint16_t bits)
{
	LCDBBLKCTL &= ~(LCDBLKCTL_DIV_8);
	LCDBBLKCTL |= bits;
}

