/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/**************************************** Include section ****************************************/
/* system */
#include <stdint.h>
#include <msp430.h>

/* driver */
#include "buzzer.h"
#include "timer.h"											/* required to access Timer0_A3 resources */

/*******************************   Prototypes section    *******************************/

/*******************************     Defines section     *******************************/

/******************************* Global Variable section *******************************/

/******************************* Local Variable section *******************************/
struct buzzer
{
	uint16_t on_time;										/* On/off duty */
	uint16_t off_time;										/* On/off duty */
	uint8_t state;											/* current buzzer output state */
	uint8_t cycle;											/* keeps control of cycling buzzer on/off */
};

static struct buzzer sBuzzer;

/**************************************** Extern section ****************************************/

/**************************************** Main section ****************************************/

/**
 * @brief		Init buzzer variables
*/
void reset_buzzer(void)
{
	sBuzzer.cycle = 0;
	sBuzzer.state = BUZZER_OFF;
}

/**
 * @brief		Start buzzer output for a number of cycles
 * @param		cycles		Keep buzzer output for number of cycles
 * 				on_time		Output buzzer for "on_time" ACLK ticks
 * 				off_time	Do not output buzzer for "off_time" ACLK ticks
*/
void start_buzzer(uint16_t cycles, uint16_t on_time, uint16_t off_time)
{
	if (sBuzzer.cycle == 0)								/* run only if buzzer is off */
	{
		sBuzzer.cycle = cycles;
		sBuzzer.on_time = on_time;
		sBuzzer.off_time = off_time;
														/* must init every time cause SimpliciTI uses same timer */
		TA1CTL = TACLR | MC_1 | TASSEL__ACLK;			/* reset TA1R, run up to CCR0, source 32768Hz ACLK */
		TA1CCR0 = BUZZER_TIMER_STEPS;					/* this sets PWM frequency */
		TA1CCTL0 = OUTMOD_4;							/* disable IRQ, set output mode "toggle" */
		P2SEL |= BUZZERPIN;								/* alternate function PWM output on P2.7 */

		fptr_Timer0_A3 = toggle_buzzer;					/* TA0_CCR3 will shoot this function from now on */
		Timer0_A3_Start(sBuzzer.on_time);				/* Start Timer0_A3 to ring after <on_time> */
		timer0_A3_ticks = sBuzzer.off_time;				/* load timer with next <off-time> */
		sBuzzer.state = BUZZ_ON_OUTPUT_ENA;				/* mark buzzer state as enabled with output on */
	}
}

/**
 * @brief		buzzer on/off duty cycle. This function is called
 * 				on each TA0CCR3 cycle to enable/disable buzzer output
*/
void toggle_buzzer(void)
{
	if (sBuzzer.state & BUZZ_ON_OUTPUT_ENA)				/* if buzzing, shut up buzzer */
	{
		TA1CTL &= ~MC_3;								/* stop PWM timer */
		P2OUT &= ~BUZZERPIN;							/* buzzer PWM output pin set low */
		P2SEL &= ~BUZZERPIN;							/* buzzer PWM output pin as GPIO */
		sBuzzer.state = BUZZ_ON_OUTPUT_DIS;				/* Update buzzer state */
		timer0_A3_ticks = sBuzzer.on_time;				/* reload timer with next <on-time> */
	}
	else												/* if shut up, buzz out */
	{
		if (--sBuzzer.cycle == 0)						/* decrement cycle counter until 0 */
		{
			stop_buzzer();								/* cycles complete, buzz job done! */
		}

		if (sBuzzer.state)								/* if buzzer status is BUZZ_ON_OUTPUT_DIS */
		{
			TA1R = 0;									/* reset timer TA1 */
			TA1CTL = MC_1 | TASSEL__ACLK;				/* re-start timer TA1 counting */
			P2SEL |= BUZZERPIN;							/* connect buzzer PWM output pin */
			sBuzzer.state = BUZZ_ON_OUTPUT_ENA;			/* update buzzer state */
			timer0_A3_ticks = sBuzzer.off_time;			/* reload Timer0_A4 IRQ with time-off value */
		}
	}
}

/**
 * @brief		Stop buzzer output
*/
void stop_buzzer(void)
{
	TA1CTL &= ~MC_3;									/* Stop PWM timer */
	P2OUT &= ~BUZZERPIN;								/* set buzzer PWM output pin low */
	P2SEL &= ~BUZZERPIN;								/* set buzzer PWM output pin as GPIO */
//	TA1CCTL0 &= ~CCIE;									/* clear PWM timer interrupt */

	Timer0_A3_Stop();									/* Disable periodic start/stop interrupts */
	reset_buzzer();										/* Clear variables */
}




