/*************************************************************************************************

	Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions
	are met:

	Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the
	distribution.

	Neither the name of Texas Instruments Incorporated nor the names of
	its contributors may be used to endorse or promote products derived
	from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
	"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
	LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
	A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
	OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
	LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
	THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef PORTS_H_
#define PORTS_H_

/*********************** Defines section ***********************/
#define FLAG_SET(flags, bit)			((flags & bit) == bit)

#define BUTTONS_IN						(P2IN)
#define BUTTONS_OUT						(P2OUT)
#define BUTTONS_DIR						(P2DIR)
#define BUTTONS_REN						(P2REN)
#define BUTTONS_IE						(P2IE)
#define BUTTONS_IES						(P2IES)
#define BUTTONS_IFG						(P2IFG)
#define BUTTONS_IRQ_VECT2				(PORT2_VECTOR)

#define BUTTON_DOWN_PIN					(1 << 0)
#define BUTTON_NUM_PIN					(1 << 1)
#define BUTTON_STAR_PIN					(1 << 2)
#define BUTTON_BACKLIGHT_PIN			(1 << 3)
#define BUTTON_UP_PIN					(1 << 4)
#define	BUTTON_STAR_LONG				(1 << 5)
#define	BUTTON_NUM_LONG					(1 << 6)
#define	BUTTON_DUMMY8					(1 << 7)				/* this is dummy/non used bit, make sure is not used */

#define ALL_BUTTONS						(BUTTON_STAR_PIN + BUTTON_NUM_PIN + BUTTON_UP_PIN + \
											BUTTON_DOWN_PIN + BUTTON_BACKLIGHT_PIN)

#define	BUTTON_STAR_NUM					(BUTTON_NUM_PIN | BUTTON_STAR_PIN)
#define	BUTTON_NUM_DOWN					(BUTTON_NUM_PIN | BUTTON_DOWN_PIN)
#define	BUTTON_STAR_NUM_LONG			(BUTTON_STAR_LONG | BUTTON_NUM_LONG)
#define	BUTTON_UP_DOWN					(BUTTON_UP_PIN | BUTTON_DOWN_PIN)

#define	BUTTON_NUM_AND_DOWN_PRESSED		FLAG_SET(BUTTONS_IN, BUTTON_NUM_DOWN)
#define BUTTON_STAR_IS_PRESSED			(BUTTONS_IN & BUTTON_STAR_PIN)
#define BUTTON_NUM_IS_PRESSED			(BUTTONS_IN & BUTTON_NUM_PIN)
#define BUTTON_UP_IS_PRESSED			(BUTTONS_IN & BUTTON_UP_PIN)
#define BUTTON_DOWN_IS_PRESSED			(BUTTONS_IN & BUTTON_DOWN_PIN)
#define BUTTON_BACKLIGHT_IS_PRESSED		(BUTTONS_IN & BUTTON_BACKLIGHT_PIN)
#define NO_BUTTON_IS_PRESSED			((BUTTONS_IN & ALL_BUTTONS) == 0)

#define BUTTON_STAR_IS_RELEASED			(BUTTONS_IES & BUTTON_STAR_PIN)
#define BUTTON_NUM_IS_RELEASED			(BUTTONS_IES & BUTTON_NUM_PIN)
#define BUTTON_UP_IS_RELEASED			(BUTTONS_IES & BUTTON_UP_PIN)
#define BUTTON_DOWN_IS_RELEASED			(BUTTONS_IES & BUTTON_DOWN_PIN)
#define BUTTON_BACKLIGHT_IS_RELEASED	(BUTTONS_IES & BUTTON_BACKLIGHT_PIN)

#define BUTTONS_DEBOUNCE_TIME_I			(5u)
#define BUTTONS_DEBOUNCE_TIME_O			(250u)
#define BUTTONS_DEBOUNCE_TIME_X			(50u)

#define	BUTTONS_REPEAT_MS				(200)

#define BUTTON_LONG_PRESS				(2u)	/* Detect if STAR / NUM button is held low continuously */
#define BACKLIGHT_LIMIT					(5u)	/* Backlight time  (sec) */
#define INACTIVITY_TIME					(30u)	/* Leave set_value() function after some seconds of inactivity */

#define BUTTON_2S						(10u)	/* value used as delay to measure 2 seconds */

#define BUTTON_FLAGS					(0x1ff)	/* corresponds to the 9 LSB in s_button_flags */


/***************************** Structured Variables *****************************/
typedef union
{
	struct
	{	/* Manual button events - NOTE: must follow same order as buttons in PORT2 */
		uint16_t down					: 1;	/* Short DOWN button press */
		uint16_t num					: 1;	/* Short NUM button press */
		uint16_t star					: 1;	/* Short STAR button press */
		uint16_t backlight				: 1;	/* Short BACKLIGHT button press */
		uint16_t up						: 1;	/* Short UP button press */
		uint16_t star_long				: 1;	/* Long STAR button press */
		uint16_t num_long				: 1;	/* Long NUM button press */
/*		uint16_t star_not_long			: 1;	 Between short and long STAR button press
		uint16_t num_not_long			: 1;	 Between short and long NUM button press */
	} flag;
	uint16_t flags;								/* Shortcut to all display flags (for reset) */
} s_button_flags;

struct struct_button
{
/*	uint8_t port2_ie;*/
/*	uint8_t port2_ifg;*/

	uint8_t star_timeout;						/* this variable is incremented each second while STAR button is pressed */
	uint8_t num_timeout;						/* this variable is incremented each second while NUM button is pressed */
	uint8_t backlight_time;						/* controls the timeout for the backlight */
/*	uint8_t backlight_status;					 1 case backlight is on */
	int16_t repeats;							/* counts the number of cycles a key is kept pressed */
};

/***************************** Extern section *****************************/
extern volatile s_button_flags button;			/* link variable for other modules */
extern volatile struct struct_button sButton;	/* link variable for other modules */

extern void button_repeat_on(void);
extern void button_repeat_off(void);
extern void button_repeat_function(void);
/*extern void init_buttons(void); */

/****************************** MACROS *****************************/

/**
 * @brief		Init and enable button interrupts.
*/
#define	BUTTONS_INIT		\
		BUTTONS_DIR &= ~ALL_BUTTONS;			/* Set button ports to input (0) */\
		BUTTONS_OUT &= ~ALL_BUTTONS;			/* Set button ports to input (0) */\
		BUTTONS_OUT |= BUTTON_BACKLIGHT_PIN;	/* pre-load BL pin with 1 */\
		BUTTONS_REN |= ALL_BUTTONS;				/* Enable internal pull-downs (1) */\
		BUTTONS_IES &= ~ALL_BUTTONS;			/* IRQ triggers on rising edge (0) */\
		BUTTONS_IFG &= ~ALL_BUTTONS;			/* Reset IRQ flags (0) */\
		BUTTONS_IE |= ALL_BUTTONS;				/* Enable button interrupts (1) */
#endif


