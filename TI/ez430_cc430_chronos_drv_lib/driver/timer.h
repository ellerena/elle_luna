/*
      Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions
        are met:

          Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.

          Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in the
          documentation and/or other materials provided with the
          distribution.

          Neither the name of Texas Instruments Incorporated nor the names of
          its contributors may be used to endorse or promote products derived
          from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
        "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
        LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
        A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
        OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
        SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
        OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef TIMER_H_
#define TIMER_H_

/*********************** Include section  ***********************/

/********************** Prototypes section **********************/
extern void Timer0_Stop(void);
extern void Timer0_A3_Start(uint16_t ticks);
extern void Timer0_A4_Delay(uint16_t ticks);


/*********************** Variabless section ***********************/
struct time
{
	uint32_t system;							/* Global system time. Used to calculate last activity */
	uint32_t last;								/* Inactivity detection (exits set_value() function) */
	uint8_t drawFlag;							/* Flag to minimize display updates */
	uint8_t display;							/* Viewing style */
	uint8_t hour;								/* Time hours data */
	uint8_t minute;								/* Time minutes data */
	uint8_t second;								/* Time seconds data */
};

extern struct time sTime;
extern unsigned timer0_A3_ticks;				/* Timer0_A3 periodic delay, externally accessible */

/*********************** MACROS *********************/
#define	XTAL_Y1				(32768)				/* Y1 Xtal speed 32.768kHz (from datasheet) */
#define	XTAL_Y2				(26000000)			/* Y2 Xtal speed 26MHz (from datasheet) */
#define	BUTTON_RESET_SEC	(3u)				/* Trigger reset when all buttons are pressed */

#define US_TO_TICK(usec)	\
	(((usec) * 32768) / 1000000)				/* Conversion usec -> ACLK timer ticks */
#define MS_TO_TICK(msec)	\
	(((msec) * 32768) / 1000)					/* Conversion msec -> ACLK timer ticks */

/**
 * @brief		Initializes and starts Timer0:
 * 				Timer0 will cycle count up to 0xffff.
 * 				TACCR0 will trigger once (only) after 1sec (32768 ticks).
*/
#define	Timer0_Init								\
	TA0CCR0 = XTAL_Y1 - 1;						/* set timer load value to 32768 ticks */\
	TA0CCTL0 = CCIE;							/* Enable TACCR0 interrupt */\
	TA0CTL = TASSEL0 | MC_2 | TACLR;			/* source ACLK, continuous mode (up to 0xffff), clear now */

/**
 * @brief		Start Timer0 in continuous mode (up to 0xffff).
*/
#define	Timer0_Start(x)	\
	TA0CTL |= MC_2;								/* Start Timer0 in continuous mode */

/**
 * @brief		Stops Timer0_A3.
*/
#define Timer0_A3_Stop(x)	\
	TA0CCTL3 = 0;								/* clear (disable) TA0CCR3 interrupt */

/************************ Global Variable section ************************/

/************************ Extern section ************************/
extern void (*fptr_Timer0_A3)(void);


#endif                          /*TIMER_H_ */
