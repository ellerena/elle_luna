/*************************************************************************************************

	Actual revision: $Revision: $
	Revision label:$Name: $
	Revision state:$State: $

*************************************************************************************************
	Radio core access functions. Taken from TI reference code for CC430.
*************************************************************************************************/

/***************************************** Include section ****************************************/
/* system */
#include <stdint.h>
#include <msp430.h>

/* driver */
#include "rf1a.h"

/************************************ Global Variable section *************************************/

/***************************************** Defines section ****************************************/
/* ------------------------------------------------------------------------------------------------
 *                                            Macros
 * ------------------------------------------------------------------------------------------------
 */
#define MRFI_RADIO_STATUS_READ_CLEAR()  RF1AIFCTL1 &= ~(RFSTATIFG);

#define MRFI_RADIO_STATUS_READ_WAIT()  while( !(RF1AIFCTL1 & RFSTATIFG) );
#define MRFI_RADIO_INST_WRITE_WAIT()   while( !(RF1AIFCTL1 & RFINSTRIFG));
#define MRFI_RADIO_DATA_WRITE_WAIT()   while( !(RF1AIFCTL1 & RFDINIFG)  );
#define MRFI_RADIO_DATA_READ_WAIT()    while( !(RF1AIFCTL1 & RFDOUTIFG) );

#define ST(x)										/* do-while loop code generator */\
	do\
	{\
		x\
	} while (__LINE__ == -1)

#define ENTER_CRITICAL_SECTION(x)	ST(				/* capture GIE status and disable all IRQ */\
	x = __get_interrupt_state();\
		__disable_interrupt();\
	)

#define EXIT_CRITICAL_SECTION(x)	__set_interrupt_state(x)	/* set CPU status */

/**
 * @brief	Send command to radio.
 * @param	unsigned char strobe	Command to radio
 * @return	statusByte		Radio core status
 */
uint8_t Strobe(uint8_t addr)
{
	uint8_t statusByte, gdo_state;
	uint16_t s;

	statusByte = 0;
	if ((addr == 0xBD) || ((addr > RF_SRES) && (addr < RF_SNOP)))	/* Check for valid addr command */
	{
		ENTER_CRITICAL_SECTION(s);						/* capture CPU status and disable IRQs */
		MRFI_RADIO_STATUS_READ_CLEAR()					/* Clear the Status read flag */

		MRFI_RADIO_INST_WRITE_WAIT()					/* Wait for radio to be ready for next instruction */

		if ((addr > RF_SRES) && (addr < RF_SNOP))	/* Write the addr instruction */
		{
			gdo_state = ReadSingleReg(IOCFG2);			/* buffer IOCFG2 state */
			WriteSingleReg(IOCFG2, 0x29);				/* c-ready to GDO2 */
			RF1AINSTRB = addr;
			if ((RF1AIN & 0x04) == 0x04)				/* chip at sleep mode */
			{
				if ((addr == RF_SXOFF) || (addr == RF_SPWD) || (addr == RF_SWOR))
				{
				}
				else
				{
					while ((RF1AIN & 0x04) == 0x04) ;	/* c-ready ? */
					__delay_cycles(9800);				/* Delay for ~810usec at 12MHz CPU clock */
				}
			}
			WriteSingleReg(IOCFG2, gdo_state);			/* restore IOCFG2 setting */
		}
		else											/* chip active mode */
		{
			RF1AINSTRB = addr;
		}

		statusByte = RF1ASTAT0B;
//*//		MRFI_RADIO_STATUS_READ_WAIT()
		EXIT_CRITICAL_SECTION(s);
	}
	return statusByte;
}

/**
 * @brief	Software reset radio core.
 */
void ResetRadioCore(void)
{
	Strobe(RF_SRES);									/* Reset the Radio Core */
	Strobe(RF_SNOP);									/* Reset Radio Pointer */
}

/**
 * @brief	Read byte from register.
 */
uint8_t ReadSingleReg(uint8_t addr)
{
	uint16_t s;
	uint8_t regValue;

	ENTER_CRITICAL_SECTION(s);

	MRFI_RADIO_INST_WRITE_WAIT()		/* Wait for radio to be ready for next instruction */
	RF1AINSTR1B = (addr | RF_REGRD);
	regValue = RF1ADOUT1B;

	EXIT_CRITICAL_SECTION(s);

	return regValue;
}

/**
 * @brief	Write byte to register.
 */
void WriteSingleReg(uint8_t addr, uint8_t value)
{
	uint16_t s;

	ENTER_CRITICAL_SECTION(s);

	MRFI_RADIO_INST_WRITE_WAIT()						/* Wait for the Radio to be ready for the next instruction */

	RF1AINSTRW = ((addr | RF_REGWR) << 8) + value;		/* Send address + Instruction */
	MRFI_RADIO_DATA_WRITE_WAIT()

	EXIT_CRITICAL_SECTION(s);
}

/**
 * @brief	Read sequence of bytes from register.
 */
void ReadBurstReg(uint8_t addr, uint8_t *pData, uint8_t len)
{
	uint16_t s;

	ENTER_CRITICAL_SECTION(s);

	MRFI_RADIO_INST_WRITE_WAIT()						/* Wait for the Radio to be ready for next instruction */
	RF1AINSTR1B = (addr | RF_REGRD);					/* Send address + Instruction */

	while(len--)
	{
		MRFI_RADIO_DATA_READ_WAIT() 					/* Wait for the Radio Core to update the RF1ADOUTB reg */
		*pData++ = RF1ADOUT1B;							/* Read DOUT from Radio Core + clears RFDOUTIFG */
	}

	EXIT_CRITICAL_SECTION(s);
}

/**
 * @brief	Write sequence of bytes to register.
 */
void WriteBurstReg(uint8_t addr, uint8_t *pData, uint8_t len)
{														/* Write Burst works wordwise not bytewise - bug known already */
	uint16_t s;

	ENTER_CRITICAL_SECTION(s);

	MRFI_RADIO_INST_WRITE_WAIT()						/* Wait for the Radio to be ready for next */
														/* instruction */
	RF1AINSTRW = ((addr | RF_REGWR) << 8) + *pData++;	/* Send address + Instruction */

	while(--len)
	{
		MRFI_RADIO_DATA_WRITE_WAIT()					/* Wait for TX to finish */
		RF1ADINB = *pData++;							/* Send data */
	}
	len = RF1ADOUTB;									/* Reset RFDOUTIFG flag which contains status byte */

	EXIT_CRITICAL_SECTION(s);
}

/**
 * @brief	Write data to power table
 * @param	value	Value to write
 */
void WritePATable(uint8_t value)
{
	uint16_t s;
	uint8_t readbackPATableValue = 0;

	ENTER_CRITICAL_SECTION(s);

	while (readbackPATableValue != value)
	{
		MRFI_RADIO_INST_WRITE_WAIT()
		RF1AINSTRW = 0x7E00 + value;					/* PA Table write (burst) */

		MRFI_RADIO_INST_WRITE_WAIT()
		RF1AINSTRB = RF_SNOP;							/* reset pointer */

		MRFI_RADIO_INST_WRITE_WAIT()
		RF1AINSTRB = 0xFE;								/* PA Table read (burst) */

		MRFI_RADIO_DATA_WRITE_WAIT()
		RF1ADINB = 0x00;								/* dummy write */

		MRFI_RADIO_DATA_READ_WAIT()
		readbackPATableValue = RF1ADOUT0B;

		MRFI_RADIO_INST_WRITE_WAIT()
		RF1AINSTRB = RF_SNOP;
	}

	EXIT_CRITICAL_SECTION(s);
}



