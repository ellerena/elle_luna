/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/******************************* ADC12 functions. *******************************/

/******************************* Include section *******************************/
/* system */
#include <stdint.h>
#include <msp430.h>
#include "bsp.h"

/* driver */
#include "adc12.h"

/*******************************   Prototypes section    *******************************/

/*******************************     Defines section     *******************************/

/******************************* Global Variable section *******************************/

/******************************* Local Variable section *******************************/

/******************************* Extern section *******************************/

/******************************* Main section *******************************/
/**
 * @brief		Init ADC12.				Do single conversion. Turn off ADC12.
 * @param		uint16_t ref			Select reference
 * 				uint16_t sht			Sample-and-hold time
 * 				uint16_t channel		Channel of the conversion
 * 	@return		uint16_t adc12_result	Return ADC result
*/
uint16_t adc12_single_conversion(uint16_t ref, uint16_t sht, uint16_t channel)
{
	uint16_t adc12_result;

	REFCTL0 |= REFMSTR + ref + REFON;			/* Enable internal reference (1.5V or 2.5V) */

	ADC12CTL0 = sht + ADC12ON;					/* Set sample time */
	ADC12CTL1 = ADC12SHP;						/* Enable sample timer */
	ADC12MCTL0 = ADC12SREF_1 + channel;			/* ADC input channel */
	ADC12IE = ADC12IE0;							/* ADC_IFG upon conv result-ADCMEMO */

	BSP_DELAY_USECS(66);						/* Idle wait 2 ticks (66us) to settle internal references */
	ADC12CTL0 |= ADC12ENC;						/* Start ADC12 */
	ADC12CTL0 |= ADC12SC;						/* Sampling and conversion start */

	while (ADC12IE)								/* Wait here until ADC Irq returns */
	{
		_BIS_SR(LPM3_bits + GIE);				/* Go sleep until ADC is done */
	}

	adc12_result = ADC12MEM0;					/* read ADC result, IFG is cleared */
	ADC12CTL0 &= ~(ADC12ENC | ADC12SC | sht);	/* Clear settings of ADC12 */
	ADC12CTL0 &= ~ADC12ON;						/* Shut down ADC12 */

	REFCTL0 &= ~(REFMSTR + ref + REFON);		/* Shut down reference voltage */

	return (adc12_result);
}

/**
 * @brief		Store ADC12 conversion result. Set flag to indicate data ready.
*/
BSP_ISR_FUNCTION(ADC12_ISR, ADC12_VECTOR)
{
	if (ADC12IV == ADC12IV_ADC12IFG0)
	{
			ADC12IE = 0;						/* Disable ADC IRQ */
			_BIC_SR_IRQ(LPM3_bits);				/* Exit active CPU */
	}
	/* **** UNUSED FOR NOW **** */
	/* Vector  0:  No interrupt */
	/* Vector  2:  ADC overflow */
	/* Vector  4:  ADC timing overflow */
	/* Vector  8:  ADC12IFG1 */
	/* Vector 10:  ADC12IFG2 */
	/* Vector 12:  ADC12IFG3 */
	/* Vector 14:  ADC12IFG4 */
	/* Vector 16:  ADC12IFG5 */
	/* Vector 18:  ADC12IFG6 */
	/* Vector 20:  ADC12IFG7 */
	/* Vector 22:  ADC12IFG8 */
	/* Vector 24:  ADC12IFG9 */
	/* Vector 26:  ADC12IFG10 */
	/* Vector 28:  ADC12IFG11 */
	/* Vector 30:  ADC12IFG12 */
	/* Vector 32:  ADC12IFG13 */
	/* Vector 34:  ADC12IFG14 */
}

