/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/*************************************** Include section ***************************************/
/* system */
#include <stdint.h>
#include <msp430.h>

/* driver */
#include "timer.h"
#include "ports.h"
#include "display.h"


/***************************************** Prototypes section *****************************************/
void Timer0_Stop(void);
void Timer0_A3_Start(uint16_t ticks);
void Timer0_A4_Delay(uint16_t ticks);

void (*fptr_Timer0_A3)(void);

/******************************************* Defines section *******************************************/

/*************************************** Global Variable section ***************************************/
unsigned timer0_A3_ticks;
struct time sTime;

/*************************************** Local Variable section ***************************************/

/*************************************** Extern section ***************************************/
//extern void BRRX_TimerTask_v(void); */

/*************************************** Main section ***************************************/

/**
	@brief		Stop and reset Timer0.
*/
void Timer0_Stop(void)
{
	TA0CTL &= ~MC_3;							/* Stop Timer0 */
	TA0R = 0;									/* Set Timer0 count register to 0x0000 */
}

/**
 * @brief		starts a loop that will run a predefined function every <ticks> equivalent seconds.
 * @param		ticks (1 tick = 1/32768 sec)
*/
void Timer0_A3_Start(uint16_t ticks)
{
	uint16_t value = 0;

	timer0_A3_ticks = ticks;					/* Store timer ticks in global variable */

	while (value != TA0R) value = TA0R;			/* Documentation: read TA0R twice to be safe */
	TA0CCR3 = value + ticks;					/* Update CCR with next timer stop value */
	TA0CCTL3 = CCIE;							/* Clear flag and enable interrupt */
}

/**
 * @brief		Go to sleep but wake up every <tick> ticks.
 * 				Once started, it can be externally disabled by setting TA0CCR4 = 0
 * @param		ticks (1 tick = 1/32768 sec)
*/
void Timer0_A4_Delay(uint16_t ticks)
{
	uint16_t value = 0;

	if (!(TA0CTL & MC_3))						/* if Timer0 not running - avoid getting stuck here */
	{
		return;
	}

	TA0CCTL4 = 0;								/* Disable timer interrupt and reset flag */

	while (value != TA0R) value = TA0R;			/* Documentation: read TA0R twice to be safe */

	TA0CCR4 = value + ticks;					/* Update CCR with next timer stop value */
	TA0CCTL4 = CCIE;							/* Re-enable timer interrupt */

	while (TA0CCTL4)							/* wait here until IRQ is triggered */
	{
		_BIS_SR(LPM3_bits + GIE);				/* enter low power mode */

#ifdef USE_WATCHDOG								/* Service watchdog */
		WDTCTL = WDTPW + WDTIS__512K + WDTSSEL__ACLK + WDTCNTCL;
#endif

/*		if (is_stopwatch())						 Redraw stopwatch display
			display_stopwatch(LINE2, DISPLAY_LINE_UPD_PARTIAL);*/

		/*__disable_interrupt();				 disable interrupt to prevent flag's change caused by others */
	}
	/*__enable_interrupt();*/
}

