/*
 * app_src.c
 *
 *  Created on: Nov 7, 2021
 *      Author: ellerena
 */

#include "captivate.h"                   // CapTIvate Touch Software Library
#include "CAPT_BSP.h"                    // CapTIvate EVM Board Support Package
#include "CAPT_Protocol.h"
#include "I2CSlave.h"

#include "_inc/virtual_com_cmds.h"
#include "_inc/sensors.h"

#define EXTENDED_STREAMING_COUNT (1)
#define FIRST_TOUCH(p) (((p)->bSensorTouch == true) && ((p)->bSensorPrevTouch == false))
#define FINGER_LEAVE(p) (((p)->bSensorTouch == false) && ((p)->bSensorPrevTouch == true))

enum
{
	BTA_ID = 0,
	PRA_ID,
	SLD03_ID
} sensor_ids;

/* tx_buffer:
 *   slider cycle (27) + slider sensor (11) = 38 bytes
 *   capture button (15) + capture sensor (11) = 26 bytes
 *   face proximity cycle (15) + face proximity sensor (11) = 26 bytes
 *   Total = 90 bytes
 */
#define TX_BUFFER_SIZE (100)
#define EXTENDED_STREAMING_COUNT (1)

static uint8_t tx_buffer[TX_BUFFER_SIZE] = {0};

const char msg_check[] = "Molon labe\0";

extern void cb_bta(tSensor* pSensor);
extern void cb_pra(tSensor* pSensor);
extern void cb_sld03(tSensor* pSensor);

/**
 * @brief	Button A Callback Function.
 * 			For now just do some LED playing
 * */
void cb_bta(tSensor* pSensor)
{
    if(FIRST_TOUCH(pSensor))
    {
        LED1_ON;
        COM_puts("cb_bta\n");
    }
    else if (FINGER_LEAVE(pSensor))
    {
        LED1_OFF;
    }
}

/**
 * @brief	Proximity sensor Callback Function.
 * 			For now just do some LED playing
 * */
void cb_pra(tSensor* pSensor)
{
    if (pSensor->bSensorProx)
    {
        LED2_ON;
        COM_puts("cb_pra\n");
    }
    else
    {
        LED2_OFF;
    }
}

/**
 * @brief	Slider Callback Function.s
 * */
void cb_sld03(tSensor* pSensor)
{
    if ((pSensor == NULL) || (!pSensor->bSensorTouch))
    {
        return;
    }

    static uint8_t extended_streaming = 0;

    // Define slider elements based on the PCB slider sensor electrodes order
//    static const tElement *ptrElement[4] = {&SLD00_E00, &SLD00_E01, &SLD00_E02, &SLD00_E03};

    // Call custom slider algorithm function
//    slider_algorithm(pSensor, &ptrElement[0], 4);

    /* Assign pointer to transmit buffer */
    uint8_t *buf_ptr = &tx_buffer[0];

    /* When slider gesture deteced, usually finger already left, except tap-hold */
    memset(buf_ptr, 0, TX_BUFFER_SIZE);

    /* fill slider data packages */
    buf_ptr += MAP_CAPT_getCyclePacket(g_pCaptivateSensorArray, BTA_ID, 0, buf_ptr);
    uint8_t bytes = MAP_CAPT_getSensorPacket(g_pCaptivateSensorArray, BTA_ID, buf_ptr);
    uint8_t slider_status = buf_ptr[SENSOR_PACKET_STATUS_BYTE_OFFSET];
    buf_ptr += bytes;

    /* fill button data packages */
    buf_ptr += MAP_CAPT_getCyclePacket(g_pCaptivateSensorArray, SLD03_ID, 0, buf_ptr);
    bytes = MAP_CAPT_getSensorPacket(g_pCaptivateSensorArray, SLD03_ID, buf_ptr);
    uint8_t capture_btn_status = buf_ptr[SENSOR_PACKET_STATUS_BYTE_OFFSET];
    buf_ptr += bytes;

    /* fill with a polite string for debug */
    buf_ptr += MAP_CAPT_getStringPacket(msg_check, buf_ptr);

    /* calculate the total payload size */
    bytes = (buf_ptr - tx_buffer);

    COM_DumpBytes(tx_buffer, sizeof tx_buffer);
    COM_TXHex(slider_status);
    COM_puts("\n");
    COM_TXHex(capture_btn_status);
    COM_puts("\n");

    /* Activate IRQ for the host MCU to come read the data if any prox or touch */
    if (slider_status || capture_btn_status || extended_streaming ) //|| force_streaming)
    {
        I2CSlave_setRequestFlag();
    }

    /* down counter used for big packages of data requiring more than one transfer */
    if (slider_status || capture_btn_status)
    {
        extended_streaming = EXTENDED_STREAMING_COUNT;
    }
    else if (extended_streaming > 0)
    {
        extended_streaming--;
    }
}

int SEN_register_callbacks(void)
{
	MAP_CAPT_registerCallback(&BTA, &cb_bta);
	MAP_CAPT_registerCallback(&PRA, &cb_pra);
	MAP_CAPT_registerCallback(&SLD03, &cb_sld03);

	I2CSlave_setTransmitBuffer(tx_buffer, sizeof tx_buffer);

	return 0;
}

////static uint8_t slider_sensor_gain[4] = {10, 11, 13, 15};

//static uint16_t prev_position = 0xFFFF;
//static void slider_algorithm(tSensor *pSensor, const tElement *pArray[], uint8_t totalElement)
//{
//    uint32_t ui32Num = 0;
//    uint32_t ui32Den = 0;
//    uint8_t ui8Element;
//    uint16_t ui16Count, ui16LTA;
//    uint16_t ui16Delta;
//    IQ16_t iq16temp_position, iq16trimTemp;
//    uint16_t ui16Raw                  = 0;
//    tSliderSensorParams *SensorParams = (tSliderSensorParams *)(pSensor->pSensorParams);
//
//    iq16temp_position.ui32Full = 0;
//    iq16trimTemp.ui32Full      = 0;
//
//    // Dont process if sensor is not being touched
//    if (pSensor->bSensorTouch == false)
//    {
//        SensorParams->SliderPosition.ui16Natural = 0xFFFF;
//        return;
//    }
//    // Calculate the Delta of each slider elements
//    for (ui8Element = 0; ui8Element < totalElement; ui8Element++)
//    {
//        ui16Count = *(pArray[ui8Element]->pRawCount);
//        ui16LTA   = pArray[ui8Element]->LTA.ui16Natural;
//        ui16Delta = 0;
//        switch (pSensor->SensingMethod)
//        {
//            case eSelf:
//            {
//                if (ui16LTA > ui16Count)
//                {
//                    ui16Delta = ui16LTA - ui16Count;
//                }
//                break;
//            }
//            case eProjected:
//            {
//                if (ui16Count > ui16LTA)
//                {
//                    ui16Delta = ui16Count - ui16LTA;
//                }
//                break;
//            }
//            default:
//            {
//                break;
//            }
//        }
//        ui16Delta *= slider_sensor_gain[ui8Element];
//        ui32Num += ui8Element * ui16Delta;
//        ui32Den += ui16Delta;
//    }
//
//    if (ui32Den == 0 || ui32Num == 0)
//    {
//        SensorParams->SliderPosition.ui16Natural = prev_position;
//        return;
//    }
//
//    // Calculate the raw position based on the delta value and the desired resolution
//    ui32Num *= SensorParams->ui16Resolution / (SensorParams->ui8TotalElements - 1);
//    ui16Raw                       = (ui32Num / ui32Den) & 0xFFFF;
//    iq16temp_position.ui16Natural = ui16Raw;
//    iq16temp_position.ui16Decimal = 0;
//
//    // Filter the slider position if filter enabled and not the new touch
//    if ((SensorParams->SliderFilterEnable == 1) && (pSensor->bSensorPrevTouch == true))
//    {
//        SensorParams->SliderPosition = MAP_CAPT_computeIIRFilter(
//            &iq16temp_position, &SensorParams->SliderPosition, SensorParams->SliderBeta);
//    }
//    else
//    { // Use unfiltered value if filter disabled or for a new touch
//        SensorParams->SliderPosition.ui16Natural = iq16temp_position.ui16Natural;
//    }
//
//    // Adjust with calibration values: lower trim and upper trim
//    if (SensorParams->SliderPosition.ui16Natural > SensorParams->SliderLower)
//    {
//        SensorParams->SliderPosition.ui16Natural -= SensorParams->SliderLower;
//    }
//    else
//    {
//        SensorParams->SliderPosition.ui16Natural = 0;
//    }
//
//    // Calculate the fixed point scale factor based on current resolution and trim values
//    SensorParams->SliderPosition.ui32Full >>= 16;
//    iq16trimTemp.ui32Full = ((uint32_t)(SensorParams->ui16Resolution) << 16) /
//                            (SensorParams->SliderUpper - SensorParams->SliderLower);
//    SensorParams->SliderPosition.ui32Full =
//        SensorParams->SliderPosition.ui32Full * iq16trimTemp.ui32Full;
//
//    // Restrict slider positions to [0 .. Resolution-1]
//    if ((SensorParams->SliderPosition.ui16Natural >= SensorParams->ui16Resolution) &&
//        (SensorParams->SliderPosition.ui16Natural < 0xFFFF))
//    {
//        SensorParams->SliderPosition.ui16Natural = SensorParams->ui16Resolution - 1;
//    }
//
//    prev_position = SensorParams->SliderPosition.ui16Natural;
//}
//
//void report_calib_data(void)
//{
//    uint8_t *buf_ptr         = &tx_buffer[0];
//    uint8_t size             = sizeof(tCalibrationValues) * (SLD03.pCycle[0]->ui8NrOfElements);
//    tCalibrationValues *data = get_calib_data_ptr();
//
//    // 0x8E, error
//    *buf_ptr++ = 1; ////TL_PARAM_CMD_FORCERECALIBRATE;
//    *buf_ptr++ =
//        (SLD03.bCalibrationError || BTA.bCalibrationError || PRA.bCalibrationError)
//            ? 1
//            : 0;
//
//    // Slider data
//    memcpy(buf_ptr, (void *)data, size);
//////    I2CSlave_setRequestFlag();
//}
//
//static uint8_t get_ref_cap(uint8_t ref_cap)
//{
//    switch (ref_cap)
//    {
//        case CAPT_REFERENCE_CAP__SELF_1P0PF:
//            return 10;
//        case CAPT_REFERENCE_CAP__SELF_1P1PF:
//            return 11;
//        case CAPT_REFERENCE_CAP__SELF_1P5PF:
//            return 15;
//        case CAPT_REFERENCE_CAP__SELF_5P0PF:
//            return 50;
//        case CAPT_REFERENCE_CAP__SELF_5P1PF:
//            return 51;
//        case CAPT_REFERENCE_CAP__SELF_5P5PF:
//            return 55;
//        default:
//            return 0;
//    }
//}
//
//// Report short and open status of the sensors
//// Data packet:
////      |cmd(0x31)|RefCapx10|Slider shorts|S1 base count|S1 ref count|S2 base|S2 ref|S3 base|S3
////      ref|S4 base|S4 ref| CapBtn short| CapBtn base | CapBtn ref | FaceProx short| FaceProx base |
////      FaceProx ref|
//void report_refcap_test_data(void)
//{
//    uint8_t num_of_elements = SLD03.pCycle[0]->ui8NrOfElements;
//
//    uint8_t *buf_ptr = &tx_buffer[0];
//    *buf_ptr++       = 2; ////CMD_SELF_TEST;
//    *buf_ptr++       = get_ref_cap(sliderTestIO.ui8RefCap);
//    *buf_ptr++       = sliderTestIO.Shorts;
//
//    uint8_t i = 0;
//    for (i = 0; i < num_of_elements; i++)
//    {
//        *buf_ptr++ = (uint8_t)(sliderTestIO.pMeasurements->BaseCount[i] & 0xFF);
//        *buf_ptr++ = (uint8_t)(sliderTestIO.pMeasurements->BaseCount[i] >> 8);
//        *buf_ptr++ = (uint8_t)(sliderTestIO.pMeasurements->RefCount[i] & 0xFF);
//        *buf_ptr++ = (uint8_t)(sliderTestIO.pMeasurements->RefCount[i] >> 8);
//    }
//
//    /* Capture Button */
//    *buf_ptr++ = keypadTestIO.Shorts;
//    *buf_ptr++ = (uint8_t)(keypadTestIO.pMeasurements->BaseCount[0] & 0xFF);
//    *buf_ptr++ = (uint8_t)(keypadTestIO.pMeasurements->BaseCount[0] >> 8);
//    *buf_ptr++ = (uint8_t)(keypadTestIO.pMeasurements->RefCount[0] & 0xFF);
//    *buf_ptr++ = (uint8_t)(keypadTestIO.pMeasurements->RefCount[0] >> 8);
//
//    /* Face Prox */
//    *buf_ptr++ = proxTestIO.Shorts;
//    *buf_ptr++ = (uint8_t)(proxTestIO.pMeasurements->BaseCount[0] & 0xFF);
//    *buf_ptr++ = (uint8_t)(proxTestIO.pMeasurements->BaseCount[0] >> 8);
//    *buf_ptr++ = (uint8_t)(proxTestIO.pMeasurements->RefCount[0] & 0xFF);
//    *buf_ptr++ = (uint8_t)(proxTestIO.pMeasurements->RefCount[0] >> 8);
//
//    /* Activate IRQ for the host MCU to come read the data */
//////    I2CSlave_setRequestFlag();
//}
//
//void report_version(uint16_t major, uint16_t minor)
//{
//    uint8_t *buf_ptr = tx_buffer;
//    *buf_ptr++       = 0x5a; ////CMD_VERSION;
//    memcpy(buf_ptr, (void *)&major, sizeof(major));
//    buf_ptr += sizeof(major);
//    memcpy(buf_ptr, (void *)&minor, sizeof(minor));
//////    I2CSlave_setRequestFlag();
//}





