/*
 * snesors.h
 *
 *  Created on: Nov 7, 2021
 *      Author: ellerena
 */

#ifndef INC_SENSORS_H_
#define INC_SENSORS_H_

#define SENSOR_PACKET_DATA_LEN (5*16 + 2)
#define SENSOR_PACKET_STATUS_BYTE_OFFSET (8)

extern int SEN_register_callbacks(void);


#endif /* INC_SENSORS_H_ */
