/*
 * code_platform.h
 *
 *  Created on: Aug 16, 2021
 *      Author: ellerena
 */

#ifndef DRV_CODE_PLATFORM_H_
#define DRV_CODE_PLATFORM_H_

#define CHK_POINTER_OR_RETURN(p)	if(!p) return

#define UART_TX_WAIT_BUF_EMPTY  while (!(UCTXIFG & UCA0IFG))
#define UART_TX_BUFFER_CHAR     UCA0TXBUF = *s++



#endif /* DRV_CODE_PLATFORM_H_ */
