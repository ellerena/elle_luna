#ifndef VERSION_H_
#define VERSION_H_

#define XSTR(s) STR(s)
#define STR(s) #s

#define __APPLE__

#ifdef __APPLE__
#define SYSTEM_VER "mbp"
#else
#warning MSG_WELCOME: undefined system
#endif


#define VERSION_MAJOR 0 // use 99 for testing only. Should be v1.5
#define VERSION_MINOR 1
#define BUILD_VERSION XSTR(VERSION_MAJOR) "."  XSTR(VERSION_MINOR)

#define MSG_WELCOME "Paris est libere!\n\r" \
	SYSTEM_VER " - v." BUILD_VERSION "\n\r" \
	__DATE__ " " __TIME__ "\n\r\0"

#endif // VERSION_H_
