/*************************************************************************************************
		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/

		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************************************/

#ifndef PROJECT_H_
#define PROJECT_H_

/**************************************** Include section ****************************************/

/**************************************** Defines section ****************************************/
//#define USE_LCD_CHARGE_PUMP					/* Comment this to not use the LCD charge pump */
//#define USE_WATCHDOG							/* Comment this to disable watchdog support */

/***************************************** Macro section *****************************************/

/**************************************** Global variables ****************************************/
//typedef union
//{
//	struct
//	{
//		uint16_t idle_timeout				:1;	/* Timeout after inactivity */
//		uint16_t idle_timeout_enabled		:1;	/* When in set mode, timeout after a given period */
//		uint16_t lock_buttons				:1;	/* Lock buttons */
//		uint16_t mask_buzzer				:1;	/* Do not output buzz for next button event */
///*		uint16_t up_down_repeat_enabled		:1;	 While in set_value(), create virtual UP/DOWN button events */
///*		uint16_t low_battery				:1;	  1 = Battery is low */
///*		uint16_t use_metric_units			:1;	 1 = Use metric units, 0 = use English units */
//		uint16_t delay_over					:1;	/* 1 = Timer delay over */
//	} flag;
//	uint16_t all_flags;							/* Shortcut to all display flags (for reset) */
//} s_system_flags;
//extern volatile s_system_flags sys;

//typedef union
//{
//	struct
//	{
//		uint16_t temperature_measurement	:1;	/* 1 = Measure temperature */
//		uint16_t voltage_measurement		:1;	/* 1 = Measure voltage */
//		uint16_t altitude_measurement		:1;	/* 1 = Measure air pressure */
//		uint16_t acceleration_measurement	:1;	/* 1 = Measure acceleration */
//		uint16_t buzzer						:1;	/* 1 = Output buzzer */
//	} flag;
//	uint16_t all_flags;							/* Shortcut to all display flags (for reset) */
//} s_request_flags;
//extern volatile s_request_flags request;


#endif								/*PROJECT_H_ */
