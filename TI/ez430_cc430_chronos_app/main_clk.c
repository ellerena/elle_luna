/* eZ430-Chronos */

#include <string.h>
#include <msp430.h>
#include <stdint.h>
/* #include <bm.h> */

#include "clock.h"
#include "vti_as.h"
#include "vti_ps.h"
/* #include "radio.h" */
#include "buzzer.h"
#include "ports.h"
#include "timer.h"
#include "rf1a.h"

#include "display.h"
#include "menu.h"
#include "date.h"
#include "alarm.h"
#include "stopwatch.h"
#include "battery.h"
#include "temperature.h"
#include "altitude.h"
#include "battery.h"
#include "user.h"
#include "acceleration.h"
/* #include "bluerobin.h" */
/* #include "rfsimpliciti.h" */
/* #include "simpliciti.h" */
/* #include "rfbsl.h" */
/* #include "test.h" */
#include "mrfi_lnk.h"

/*************************************** Local function prototypes ***************************************/
void wakeup_event(void);
void process_requests(void);
void display_update(void);
void idle_loop(void);
void read_calibration_values(void);

/*************************************** Global Function pointers ***************************************/

/*************************************** Defines ***************************************/
#define CALIBRATION_DATA_LENGTH				(13u)			/* Number of calibration data bytes in INFOA memory */

/*************************************** Global variables ***************************************/
volatile s_system_flags sys;								/* Variable holding system internal flags */
volatile s_request_flags request;							/* Variable holding flags set by logic modules */
volatile s_message_flags message;							/* Variable holding message flags */
/*uint8_t rf_frequoffset;										Global radio frequency offset taken from calibration memory. Compensates crystal deviation from 26MHz nominal value */

/**
 * @brief	Initialize the microcontroller.
 */
static inline void init_application(void)
{
	BSP_Init();
																/* **** Port Mapping **** */
	__disable_interrupt();
	PMAPPWD = 0x02D52;											/* Enable write-access to port mapping registers */
	PMAPCTL = PMAPRECFG;										/* Allow reconfiguration during runtime */
	BUZZER_INIT													/* Configure the buzzer output */
	ACM_INIT													/* Configure the CMA3000 (accelerator) spi */
	__enable_interrupt();
/*	radio_reset(); */
/*	radio_powerdown(); */
	as_init();													/* Init acceleration sensor */
	LCD_INIT													/* initialize the LCD display driver */
	BUTTONS_INIT
	Timer0_Init													/* Configure Timer0 for use by the clock and delay functions */
	ps_init();													/* Init pressure sensor */
	mrfiLinkInit(DEV_ADDR_LOC,DEV_ADDR_REM,MRFI_CHANNEL);  		/* Initialize the MRFI RF link layer */
}

/**
 * @brief	Initializes/resets main global variables
 */
static inline void init_global_variables(void)
{
	fptr_lcd_line1 = ptrMenu_L1->display_function;
	fptr_lcd_line2 = ptrMenu_L2->display_function;

	button.flags = 0;
	sys.flags = 0;
	request.flags = 0;
	message.flags = 0;
	display.flags = DISP_UPD_FULL;

#ifndef ISM_US
	display.flags |= SYS_UNITS;					/* Use metric units when displaying values */
#endif

    read_calibration_values();
    reset_clock();
    reset_date();
    reset_alarm();
    reset_buzzer();
    reset_stopwatch();
    reset_altitude_measurement();
    reset_acceleration();
/*    reset_bluerobin();
    reset_rf();*/
    reset_temp_measurement();
    reset_batt_measurement();
    battery_measurement();
}


int main(void)
{
	init_application();										/* Init MCU */
	init_global_variables();								/* Assign initial value to global variables */

	while (1)												/* Main control loop */
	{
		idle_loop();										/* go to sleep (LPM3), let IRQs wake us up when needed */

		if (button.flags | sys.flags)						/* if any process flags rose, run wake-up tasks */
			wakeup_event();

		if (request.flags)									/* Process actions requested by logic modules */
			process_requests();

		if (display.flags)									/* Before going to LPM3, update display */
			display_update();
	}
}


/**
 * @brief	after wake up from sleep, this routine checks the system
 * 			status, updates several variables so that next routines can
 * 			proceed accordingly
 */
void wakeup_event(void)										/* Process external / internal wakeup events. */
{
	uint16_t temp;

	sys.flags |= SYS_IDLE_TIMEOUT_ENA;						/* Enable idle timeout detection */

	if (sButton.backlight_time)								/************* BACKLIGHT EVENT *************/
	{
		if (--sButton.backlight_time == 0)					/* decrement backlight timer until zero, then turn off */
		{
			P2DIR &= ~BUTTON_BACKLIGHT_PIN;
		}
	}
															/*************  BUTTON EVENTS  *************/
	temp = button.flags;

	if (sys.flags & SYS_LOCK_BUTTONS)						/* If buttons are locked */
	{
		if (0 == (temp & BUTTON_STAR_NUM))					/* Check that STAR-NUM are NOT pressed */
		{
			message.flags |= (MSG_PREPARE | MSG_LOCK);		/* request "lock" message to show on next second tick */
		}
	}
	else if (temp & BUTTON_NUM_LONG)						/* Long press NUM runs MX (BOTTOM) function */
	{
		ptrMenu_L2->mx_function(LINE2);						/* Call sub menu function */
		display.flags |= DISP_UPD_FULL;						/* Set display update flag */
	}
	else if (temp & BUTTON_STAR_LONG)						/* Long press STAR runs MX (TOP) function */
	{
		ptrMenu_L1->mx_function(LINE1);						/* Call sub menu function */
		display.flags |= DISP_UPD_FULL;						/* Set display update flag */
	}
	else if (temp & BUTTON_STAR_PIN)						/* Short STAR advances to next (TOP) menu item */
	{
		fptr_lcd_line1(LINE1, DISPLAY_ICONS_OFF);			/* clean function specific symbols */
		ptrMenu_L1 = ptrMenu_L1->next;						/* move to next menu entry */
		fptr_lcd_line1 = ptrMenu_L1->display_function;		/* assign new display function */
		display.flags |= DISP_UPD_LINE1;					/* set Line1 display update flag */
	}
	else if (temp & BUTTON_NUM_PIN)							/* Short STAR advances to next (TOP) menu item */
	{
/*			rfBSL_button_confirmation = 0;					 Clear rfBSL confirmation flag */
		fptr_lcd_line2(LINE2, DISPLAY_ICONS_OFF);			/*  clean function specific symbols */
		ptrMenu_L2 = ptrMenu_L2->next;						/* Go to next menu entry */
		fptr_lcd_line2 = ptrMenu_L2->display_function;		/* Assign new display function */
		display.flags |= DISP_UPD_LINE2;					/* Set Line2 display update flag */
	}
	else if (temp & BUTTON_UP_PIN)							/* Short press UP runs SX (TOP) function */
	{
		ptrMenu_L1->sx_function(LINE1);						/* Call direct function */
		display.flags |= DISP_UPD_LINE1;					/* Set Line1 display update flag */
	}
	else if (temp & BUTTON_DOWN_PIN)						/* Short press DOWN runs SX (DOWN) function */
	{
		ptrMenu_L2->sx_function(LINE2);						/* Call direct function */
		display.flags |= DISP_UPD_LINE2;					/* Set Line1 display update flag */
	}

	button.flags = 0;										/* mark all flags as processed */
															/************* INTERNAL (NO BUTTON) EVENTS *************/
	if (sys.flags & SYS_IDLE_TIMEOUT)						/* check idle timeout */
	{
		sys.flags &= ~SYS_IDLE_TIMEOUT;						/* Clear timeout flag */
		display.flags |= DISP_UPD_FULL;						/* request full display update */
	}

	sys.flags &= ~SYS_IDLE_TIMEOUT_ENA;						/* Disable idle timeout detection */
}

/**
 * @brief	Process requested actions outside ISR context.
 */
void process_requests(void)
{
	uint16_t flags;

	flags = request.flags;

	if (flags & REQ_MEASURE_TEMP)				/* Do temperature measurement */
	{
		temperature_measurement(STEPPED);
	}

	if (flags & REQ_MEASURE_ALT)					/* Do pressure measurement */
	{
		do_altitude_measurement(STEPPED);
	}

	if (flags & REQ_MEASURE_ACC)						/* Do acceleration measurement */
	{
		do_acceleration_measurement();
	}

	if (flags & REQ_MEASURE_VOLT)					/* Do voltage measurement */
	{
		battery_measurement();
	}

	if (flags & REQ_BUZZ)								/* Generate alarm (two beeps) */
	{
		start_buzzer(2, BUZZER_ON_TICKS, BUZZER_OFF_TICKS);
	}

	request.flags = 0;										/* all done here, reset flags */
}

/**
 * @brief	Process display flags and call LCD update routines.
 */
void display_update(void)
{
	uint16_t temp, line;
	const uint8_t msg[] =	{/*0*/"  LO@T" /*6*/"  OPEN" /*12*/"LOBATT" /*18*/"ALON" /*22*/" OFF"};
	uint8_t *string = (uint8_t *)msg;

	temp = message.flags;
	if (temp & MSG_SHOW)											/* message flags take precedence over LINE2 requests */
	{
		line = LINE2;												/* MSG_LOCK@LINE2 is default */

		if (temp & MSG_UNLOCK)										/* MSG_UNLOCK@LINE2 */
		{
			string += 6;
		}
		else if (temp & MSG_LOBATT)									/* MSG_LOBATT@LINE2 */
		{
			string += 12;
		}
		else if (temp & MSG_ALARM_ON)								/* MSG_ALARM_ON@LINE1 */
		{
			string += 18;
			line = LINE1;
		}
		else if (temp & MSG_ALARM_OFF)								/* MSG_ALARM_OFF@LINE1 */
		{
			string += 22;
			line = LINE1;
		}

		clear_line(line);											/* clear line text */
		line = (line == LINE2) ? LCD_SEG_L2_5_0 : LCD_SEG_L1_3_0;
		display_chars(line, string, SEG_ON);						/* print message on LINE1 or LINE2 */
		message.flags = MSG_ERASE;									/* clear flags & request to erase message on next tick */
	}
	else
	{
		temp = display.flags;

		if (temp & (DISP_UPD_LINE1 | DISP_UPD_FULL))				/* for full screen or LINE1 updates do this */
		{
			clear_line(LINE1);										/* erase whole text line (leave icons) */
			fptr_lcd_line1(LINE1, DISPLAY_LINE_UPD_FULL);
		}
		else if (temp & ptrMenu_L1->update_flag)					/* for LINE1 function specific display requests */
		{
			fptr_lcd_line1(LINE1, DISPLAY_LINE_UPD_PARTIAL);
		}

		if (temp & (DISP_UPD_LINE2 | DISP_UPD_FULL))				/* for full screen or LINE2 updates do this */
		{
			clear_line(LINE2);										/* erase whole text line (leave icons) */
			fptr_lcd_line2(LINE2, DISPLAY_LINE_UPD_FULL);
		}
		else if (temp & ptrMenu_L2->update_flag)					/* for LINE2 function specific display requests */
		{
			fptr_lcd_line2(LINE2, DISPLAY_LINE_UPD_PARTIAL);
		}
	}
/*	if (display.flags |= DISP_UPD_FULL)								 Restore blinking icons cleared by set_value
	{
			if (is_bluerobin() == BLUEROBIN_CONNECTED)
			{
					display_symbol(LCD_ICON_BEEPER1, SEG_ON_BLINK_OFF);		 Turn on beeper icon to show activity
					display_symbol(LCD_ICON_BEEPER2, SEG_ON_BLINK_OFF);		 Turn on beeper icon to show activity
					display_symbol(LCD_ICON_BEEPER3, SEG_ON_BLINK_OFF);		 Turn on beeper icon to show activity
			}
	}*/

	display.flags &= ~DISPLAY_CLEAR_FLAGS;							/* display update completed, clear display flags */
}

/**
 * @brief	Go to LPM. Service watchdog timer when waking up.
 */
void idle_loop(void)
{
	_BIS_SR(LPM3_bits + GIE);										/* To low power mode */
	__no_operation();
#ifdef USE_WATCHDOG													/* Service watchdog */
		WDTCTL = WDTPW + WDTIS__512K + WDTSSEL__ACLK + WDTCNTCL;
#endif
}

/**
 * @brief	Read calibration values for temperature measurement, voltage measurement and radio from INFO memory.
 */
void read_calibration_values(void)
{
#define	CALVAL(x)			(*(flash_mem + x))

	uint8_t *flash_mem;										/* Memory pointer */

	flash_mem = (uint8_t *) 0x1800;							/* point to Info D memory */

	if (CALVAL(0) == 0xFF)									/* if user erased INFO D memory, assign our own values */
	{
/*		rf_frequoffset = 4;*/
		sTemp.offset = -250;
		sBatt.offset = -10;
		sAlt.altitude_offset = 0;
/*		simpliciti_ed_address[0] = 0x79;
		simpliciti_ed_address[1] = 0x56;
		simpliciti_ed_address[2] = 0x34;
		simpliciti_ed_address[3] = 0x12;*/
	}
	else
	{
/*		rf_frequoffset = CALVAL(1);							 Assign calibration data to global variables

		if ((rf_frequoffset > 20) &&
			(rf_frequoffset < (256 - 20)))					 Range check for calibrated FREQEST value (-20 .. + 20 is ok)
		{
			rf_frequoffset = 0;
		}*/
		sTemp.offset = (int16_t) ((CALVAL(2) << 8) + CALVAL(3));
		sBatt.offset = (int16_t) ((CALVAL(4) << 8) + CALVAL(5));
/*		simpliciti_ed_address[0] = CALVAL(6);
		simpliciti_ed_address[1] = CALVAL(7);
		simpliciti_ed_address[2] = CALVAL(8);
		simpliciti_ed_address[3] = CALVAL(9);*/

		if (CALVAL(12) != 0xFF)								/* S/W version byte set during calibration? */
		{
			sAlt.altitude_offset = (int16_t) ((CALVAL(10) << 8) + CALVAL(11));
		}
		else
		{
			sAlt.altitude_offset = 0;
		}
	}
#undef CALVAL
}

/**
 * @brief		Generic value setting routine
 * @param		value		Pointer to value to set
 * 				digits		Number of digits
 * 				blanks		Number of whitespaces before first valid digit
 * 				limitLow	Lower limit of value
 * 				limitHigh	Upper limit of value
 * 				mode
 * 				segments	Segments where value should be drawn
 * 				fptr_SETVAL_display_function1			Value-specific display routine
*/
void set_value(int16_t * value, uint16_t digits, uint16_t blanks, int16_t limitLow,
				int16_t limitHigh, uint16_t mode, uint16_t segments,
				SET_VAL_FUNCTION (fptr_SETVAL_display_function1))
{
	uint8_t update, stopwatch_state;
	int16_t stepValue = 1;
	int32_t val;

	button.flags = 0;										/* clear button flags, start fresh! */
	sButton.repeats = 0;									/* Init step size and repeat counter */
	update = 1;												/* Initial display update */

	clear_blink_mem();										/* clear blink memory */
	stop_buzzer();											/* for safety only - buzzer on/off and button_repeat share same IRQ */
	stopwatch_state = sStopwatch.state;
	sStopwatch.state = STOPWATCH_HIDE;						/* disable stopwatch display update while function is active */

	button_repeat_on();										/* Turn on button repeat counting function every 200ms */
	sys.flag.up_down_repeat_enabled = 1;					/* this flag enables 'tick' sound for u/d buttons */
	set_blink_rate(LCDBLKCTL_DIV_4);						/* start blinking with with 2Hz */

	while ((!sys.flag.idle_timeout) && (!button.flag.star))	/* loop until value is set or timeout happens */
	{
		if (button.flags & BUTTON_NUM_PIN)					/* NUM button processing */
		{
			if  (mode & SETVAL_NEXT) break;			/* only exit if SETVAL_NEXT was enabled */
		}

		val = *value;										/* capture value in a local variable for faster use */

		if (button.flags & BUTTON_UP_PIN)					/* UP button: increase value */
		{
			val = val + stepValue;							/* Increase value */

			if (val > limitHigh)							/* Check value limits */
			{
				val = (mode & SETVAL_ROLLOVER) ? limitLow : limitHigh;
				stepValue = 1;								/* Reset step size to default */
			}

			update = 1;										/* Trigger display update */
			button.flags &= ~BUTTON_UP_PIN;					/* Clear button flag */
		}

		if (button.flags & BUTTON_DOWN_PIN)					/* DOWN button: decrease value */
		{
			val = val - stepValue;							/* Decrease value */

			if (val < limitLow)								/* Check value limits */
			{												/* Check if value can roll over, else stick to limit */
				val = (mode & SETVAL_ROLLOVER) ? limitHigh : limitLow;
				stepValue = 1;								/* Reset step size to default */
			}

			update = 1;										/* Trigger display update */
			button.flags &= ~BUTTON_DOWN_PIN;				/* Clear button flag */
		}

		if (mode & SETVAL_FAST)						/* When fast mode is enabled, increase step size if Sx button is continuously */
		{
			uint8_t doRound = 0;

			switch (sButton.repeats)
			{
				case 0:
					stepValue = 1;
					break;
				case 10:
					stepValue = 10;
					doRound = 1;
					break;
				case 20:
					stepValue = 100;
					doRound = 1;
					break;
				case 30:
					stepValue = 1000;
					doRound = 1;
					break;
			}

			if (doRound)									/* Round value to avoid odd numbers on display */
			{
				val -= val % stepValue;
			}
		}

		*value = val;

		if (update)											/* Update display when there is new data */
		{
			if (mode & SETVAL_SIGNED)				/* handle negative value display if requested */
			{
				if (val >= 0)
				{
					display_symbol(LCD_ARROW_DOWN, SEG_OFF);
				}
				else
				{
					display_symbol(LCD_ARROW_DOWN, SEG_ON);
					val = val * (-1);						/* make value positive so printer can handle it */
				}
			}
															/* Display function can either display value directly, modify value before displaying */
															/* or display a string referenced by the value */
			fptr_SETVAL_display_function1(segments, val, digits, blanks);

			update = 0;										/* Clear update flag */
		}

		idle_loop();										/* go to sleep while waiting for a key pressed */

	}

	button_repeat_off();									/* Turn off button repeat function */
	sys.flag.up_down_repeat_enabled = 0;					/* celaring this flag stops 'tick' sound for u/d buttons */

	sStopwatch.state = stopwatch_state;						/* Enable stopwatch display updates again */

	display_symbol(LCD_ARROW_DOWN, SEG_OFF);				/* Clear down arrow */

	set_blink_rate(LCDBLKCTL_DIV_8);						/* Set blinking rate to 1Hz and stop */
	clear_blink_mem();
}

/**
 * @brief		IRQ handler for TIMERA0_0 IRQ
 * 				TimerA0_0				1/1sec clock tick
*/
BSP_ISR_FUNCTION(TIMER0_A0_ISR, TIMER0_A0_VECTOR)
{
	static uint16_t temp = 0;

	TA0CCR0 += XTAL_Y1;											/* Add 1 sec to TACCR0 (IRQ will be asserted at 0x7FFF and 0xFFFF) */
	TA0CCTL0 &= ~(CCIFG);										/* Reset IRQ flag */

	add_second();												/* Add 1 second to global time */
	display.flags |= DISP_UPD_TIME;								/* rise flag to indicate time has changed */

/*	if (is_rf())										While SimpliciTI stack operates or BlueRobin searches, freeze system state
	{
		if (sRFsmpl.timeout == 0)						SimpliciTI automatic timeout
		{
			simpliciti_flag |= SIMPLICITI_TRIGGER_STOP;
		}
		else
		{
			sRFsmpl.timeout--;
		}

		if (sRFsmpl.mode == SIMPLICITI_SYNC)			switch message after received packet
		{
			if (sRFsmpl.display_sync_done == 0)
			{
				display_chars(LCD_SEG_L2_5_0, (u8 *) "  SYNC", SEG_ON);
			}
			else
			{
				sRFsmpl.display_sync_done--;
			}
		}

		_BIC_SR_IRQ(LPM3_bits);							Exit from LPM3 on RETI
		return;
	}*/

																/********** Services that require 1/min processing **********/
	if (sTime.drawFlag >= 2)
	{
		request.flags |= REQ_MEASURE_VOLT;					/* flag on request for battery voltage check */
		check_alarm();											/* Check if alarm needs to be turned on */
	}

																/********** Services that must run in background **********/
	if (sAlarm.duration)										/**** ALARM: buzz it or stop it if timeout ****/
	{
		if (--sAlarm.duration)									/* reduce alarm time counter while buzzing */
		{
			request.flags |= REQ_BUZZ;							/* keep the buzzer buzzing */
		}
		else													/* if timeout reached stop buzzing */
		{
			stop_buzzer();
		}
	}

	if (sTemp.state & MENU_ITEM_VISIBLE)						/**** TEMPERATURE MODE: request temperature measurement ****/
	{
		request.flags |= REQ_MEASURE_TEMP;						/* request next stage to make temperature measurement */
	}
	else if (sAlt.state & MENU_ITEM_VISIBLE)					/**** ALTITUDE MODE: request temperature measurement ****/
	{
		if (sAlt.timeout)										/* count down to stop measurement when timeout has elapsed */
		{
			if (--sAlt.timeout == 0)
			{
			stop_altitude_measurement();						/* set pressure sensor in standby and idle the IRQ */
			display_chars(LCD_SEG_L1_3_0, (uint8_t*) "ALT;",
							SEG_ON);							/* show ---- m/ft */
			display_symbol(LCD_ARROW_DOWN, SEG_OFF);			/* clear down arrow */
			}
		}
	}
	else if (sAccel.state & MENU_ITEM_VISIBLE)					/**** ACCELERATION MODE: request acceleration measurement ****/
	{
		if (sAccel.timeout)										/* count down to stop measurement when timeout has elapsed */
		{
			if(--sAccel.timeout==0)
			{
				as_stop();											/* stop and power down acceleration sensor */
				display_chars(LCD_SEG_L1_3_0, (uint8_t *) "ACC;", SEG_ON);
				display_symbol(LCD_ARROW_DOWN, SEG_OFF);			/* clear up down */
				display_symbol(LCD_SEG_L1_DP1, SEG_OFF);			/* clear dot point */
			}
			else
			{
				display.flags |= DISP_UPD_ACCEL;
			}
		}

//		if (AS_INT_IN & AS_INT_PIN)								/* If DRDY irq is (still) high, request data again */
//			request.flags |= REQ_MEASURE_ACC;
	}

	if (sBatt.lobatt_count)										/* check if battery low batt message is active */
	{
		sBatt.lobatt_count--;									/* decrement display counter each second, until exhausted */
		message.flags |= (MSG_SHOW | MSG_LOBATT);				/* flag messaging process to show low batt on next tick */
	}

	if (message.flags & MSG_PREPARE)							/* trick to delay one tick before calling message process */
	{
		message.flags &= ~MSG_PREPARE;
		message.flags |= MSG_SHOW;
	}
	else if (message.flags & MSG_ERASE)							/* message cycle is over, so request to repaint screen */
	{
		message.flags &= ~MSG_ERASE;
		display.flags |= DISP_UPD_FULL;
	}

	if (sys.flags & SYS_IDLE_TIMEOUT_ENA)						/* check idle timeout */
	{
		if (sTime.system - sTime.last > INACTIVITY_TIME)
		{
			sys.flags |= SYS_IDLE_TIMEOUT;						/* set timeout flag */
		}
	}

	if (BUTTON_NUM_AND_DOWN_PRESSED)							/******* LONG PRESS [NUM-DOWN] DETECTION *******/
	{
		if (temp++ > BUTTON_LONG_PRESS)							/* count several iterations before calling 'long' press */
		{
			sys.flags ^= SYS_LOCK_BUTTONS;						/* Toggle lock / unlock buttons flag */
			message.flags |= (sys.flag.lock_buttons) ?			/* select and mark proper message to display */
						(MSG_PREPARE | MSG_LOCK)
					:	(MSG_PREPARE | MSG_UNLOCK);

			temp = 0;											/* Reset button lock counter */
		}
	}
	else														/******* LONG PRESS STAR OR NUM DETECTION *******/
	{
		temp = 0;												/* Reset button lock counter */

		if (BUTTON_STAR_IS_PRESSED)								/******* LONG PRESS STAR DETECTION *******/
		{
			sButton.star_timeout++;

			if (sButton.star_timeout > BUTTON_LONG_PRESS)		/* Check if button was held low for some seconds */
			{
				sButton.star_timeout = 0;
				button.flags |= BUTTON_STAR_LONG;
				BUTTONS_IES &= ~BUTTON_STAR_PIN;				/* Return interrupt edge to normal value */
			}
		}
		else													/* there was a button press not long enough */
		{
			sButton.star_timeout = 0;
		}

		if (BUTTON_NUM_IS_PRESSED)								/******* LONG PRESS NUM DETECTION *******/
		{
			sButton.num_timeout++;
			if (sButton.num_timeout > BUTTON_LONG_PRESS)		/* Check if button was held low for some seconds */
			{
				button.flags |= BUTTON_NUM_LONG;
				sButton.num_timeout = 0;
				BUTTONS_IES &= ~BUTTON_NUM_PIN;					/* Return interrupt edge to normal value */
			}
		}
		else													/* there was a button press not long enough */
		{
			sButton.num_timeout = 0;
		}
	}

	_BIC_SR_IRQ(LPM3_bits);										/* Exit from LPM3 on RETI */
}

/**
 * @brief		IRQ handler for timer IRQ.
 * 				TimerA0_1				BlueRobin timer
 * 				TimerA0_2				1/100 sec Stopwatch
 * 				TimerA0_3				Configurable periodic IRQ (used by button_repeat and buzzer)
 * 				TimerA0_4				One-time delay
*/
BSP_ISR_FUNCTION(TIMER0_A1_5_ISR, TIMER0_A1_VECTOR)
{
	switch (TA0IV)
	{
/*		case TA0IV_TA0IFG:
			break;*/
/*		case TA0IV_TA0CCR1:						 TimerA0_1 handler - BlueRobin timer
			BRRX_TimerTask_v();
			break;*/

		case TA0IV_TA0CCR2:						/* TimerA0_2 - 1/1 or 1/100 sec Stopwatch */
			TA0CCTL2 = STW_CCTL2_STOP;			/* Disable IE and Reset IRQ flag */
			stopwatch_update_timer();			/* Load CCR register with next capture point */
			TA0CCTL2 = STW_CCTL2_START;			/* Enable timer interrupt */
			stopwatch_tick();					/* Increase stopwatch counter */
			break;

		case TA0IV_TA0CCR3: 					/* TimerA0_3 heads up call (used by button_repeat and buzzer) */
		{
			TA0CCTL3 = 0;						/* Disable IE and Reset IRQ flag */
			TA0CCR3 += timer0_A3_ticks;			/* Load CCR register with next capture point */
			TA0CCTL3 = CCIE;					/* Re-enable timer interrupt to iterate */
			fptr_Timer0_A3();					/* Call function handler */
			break;
		}

		case TA0IV_TA0CCR4:						/* TimerA0_4 wake up from sleep */
			TA0CCTL4 = 0;						/* Disable IRQ and reset IRQ flag */
			break;
	}

	_BIC_SR_IRQ(LPM3_bits);						/* Exit from LPM3 on RETI */
}

/**
 * @brief	Interrupt service routine for buttons, acceleration sensor CMA_INT
 * 			and pressure sensor DRDY
 *
 * @verbatim
 * 			When SimpliciTI stack is active the button behavior is different:
 * 			- Store button events in SimpliciTI packet data
 * 			- Exit SimpliciTI when button DOWN was pressed
 *
 *
 * 			Erase previous button press after a number of resends (increase number if link quality is low)
 * 			This will create a series of packets containing the same button press, necessary because we have no acknowledge
 * 			Filtering (edge detection) will be done by receiver software
 *
 * @endverbatim
*/
BSP_ISR_FUNCTION(PORT2_ISR, PORT2_VECTOR)
{
	uint8_t	int_enable; /* , simpliciti_button_event = 0; */
/*	static uint8_t simpliciti_button_repeat = 0; */

	int_enable = BUTTONS_IE;											/* grab IRQ enable vector so we can restore later */

	if (0 == (button.flags & BUTTON_STAR_NUM_LONG))						/***** Don't run this if long press already detected *****/
	{
		uint16_t tmp_flag, temp;

		temp = 0;														/* start with all flags low */
		tmp_flag = BUTTONS_IFG & int_enable;							/* photograph IRQ flags current state */

																		/********** CREATE NON BUTTON IRQ EVENTS **********/
		request.flags |= (tmp_flag & (AS_INT_PIN | PS_INT_PIN));		/* if IRQ came by ACC/ALT rise request flags */

		if (0/*is_rf()*/)
		{
/*			if (simpliciti_button_repeat++ > 6)
			{
				simpliciti_data[0] &= ~0xF0;
				simpliciti_button_repeat = 0;
			}

			if (tmp_flag & BUTTON_STAR_PIN)
			{
				simpliciti_data[0] |= SIMPLICITI_BUTTON_STAR;
				simpliciti_button_event = 1;
			}
			else if (tmp_flag & BUTTON_NUM_PIN)
			{
				simpliciti_data[0] |= SIMPLICITI_BUTTON_NUM;
				simpliciti_button_event = 1;
			}
			else if (tmp_flag & BUTTON_UP_PIN)
			{
				simpliciti_data[0] |= SIMPLICITI_BUTTON_UP;
				simpliciti_button_event = 1;
			}
			else if (tmp_flag & BUTTON_DOWN_PIN)
			{
				simpliciti_flag |= SIMPLICITI_TRIGGER_STOP;
			}

			if (simpliciti_button_event)									Trigger packet sending inside SimpliciTI stack
				simpliciti_flag |= SIMPLICITI_TRIGGER_SEND_DATA;*/
		}
		else if (tmp_flag & ALL_BUTTONS)								/******* CREATE BUTTON IRQ EVENTS *******/
		{
			__disable_interrupt();										/* temporarily disable all irqs */
			P2IE = 0;													/* don't allow more PORT2 IRQs for now */
			__enable_interrupt();										/* re-enable irqs */

			Timer0_A4_Delay(MS_TO_TICK(BUTTONS_DEBOUNCE_TIME_I));		/* go to sleep and wake up im 5ms (debounce 1) */
			sTime.last = sTime.system;							/* Reset inactivity detection */
			temp = tmp_flag & BUTTONS_IN;								/* sample pressed buttons again */
			temp |= (BUTTON_STAR_NUM & BUTTONS_IES);					/* sample if STAR and NUM are released now */
			BUTTONS_IES &= ~BUTTONS_IES;								/* re-instate buttons to trigger at press */

																		/****** PROCESS FLAGS REQUIRING IMMEDIATE/ASYNC ATTENTION ******/
			if(temp & BUTTON_BACKLIGHT_PIN)								/*** turn on back light (no need to clear flag later) ***/
			{
				P2DIR |= BUTTON_BACKLIGHT_PIN;							/* turn on backlight */
				sButton.backlight_time = BACKLIGHT_LIMIT;				/* start backlight-on timer */
			}

			if (FLAG_SET(temp, BUTTON_NUM_DOWN))						/*** lock/unlock buttons? that's done somewhere else ***/
			{
				temp = 0;												/* no buzzer and clear flags */
			}

			if (temp)													/*** if flags remain alive complete debounce and click ***/
			{
				if (sAlarm.duration)									/* if alarm is running, any key stops it */
				{
					stop_alarm();
					temp = 0;											/* don't process more flags */
				}
				else if (!sys.flag.up_down_repeat_enabled)				/* if not in a value setup process */
				{
					start_buzzer(1, MS_TO_TICK(20), MS_TO_TICK(150));	/* generate a 'tick' sound */
				}

				Timer0_A4_Delay(MS_TO_TICK(BUTTONS_DEBOUNCE_TIME_O));	/* go to sleep and wake up in 250ms (debounce 2) */
			}

			if (temp & BUTTON_STAR_NUM)									/*** if pressed, START or NUM must be released too ***/
			{
				Timer0_A4_Delay(MS_TO_TICK(BUTTONS_DEBOUNCE_TIME_X));	/* extra debounce delay 50ms */

				tmp_flag = (BUTTONS_IN & BUTTON_STAR_NUM);				/* get current status for NUM and STAR (reuse buzzer var) */
				BUTTONS_IES |= tmp_flag;								/* change IRQ trigger direction to falling edge (1) */
				temp &= ~tmp_flag;										/* lower processed flags */
			}

		}
		button.flags = temp;										/*** upload remaining flags to global variable ***/
	}

	__disable_interrupt();												/* temporarily disable all irqs */
	P2IFG = 0;															/* clear all PORT2 irq flags */
	P2IE = int_enable;													/* reinstate PORT2 IRQ detection */
	__enable_interrupt();												/* re-enable irqs */

	__bic_SR_register_on_exit(LPM4_bits);								/* Exit LPM3/LPM4 on RETI */
}



