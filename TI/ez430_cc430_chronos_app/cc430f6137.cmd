/* ============================================================================ */
/* Copyright (c) 2013, Texas Instruments Incorporated									*/
/*	All rights reserved.														*/
/*																					*/
/*	Redistribution and use in source and binary forms, with or without				*/
/*	modification, are permitted provided that the following conditions				*/
/*	are met:																	*/
/*																					*/
/*	*	Redistributions of source code must retain the above copyright					*/
/*		notice, this list of conditions and the following disclaimer.			*/
/*																					*/
/*	*	Redistributions in binary form must reproduce the above copyright		*/
/*		notice, this list of conditions and the following disclaimer in the			*/
/*		documentation and/or other materials provided with the distribution.		*/
/*																					*/
/*	*	Neither the name of Texas Instruments Incorporated nor the names of			*/
/*		its contributors may be used to endorse or promote products derived			*/
/*		from this software without specific prior written permission.			*/
/*																					*/
/*	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" */
/*	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,				*/
/*	THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR			*/
/*	PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR			*/
/*	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,				*/
/*	EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,			*/
/*	PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; */
/*	OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,	*/
/*	WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR		*/
/*	OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,					*/
/*	EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.								*/
/* ============================================================================ */

/******************************************************************************/
/* lnk_cc430f6137.cmd - LINKER COMMAND FILE FOR LINKING CC430F6137 PROGRAMS		*/
/*																			*/
/*	Usage:	lnk430 <obj files...>	-o <out file> -m <map file> lnk.cmd		*/
/*					cl430	<src files...> -z -o <out file> -m <map file> lnk.cmd		*/
/*																			*/
/*----------------------------------------------------------------------------*/
/* These linker options are for command line linking only.	For IDE linking,	*/
/* you should set your linker options in Project Properties							*/
/* -c														LINK USING C CONVENTIONS	*/
/* -stack	0x0100											SOFTWARE STACK SIZE				*/
/* -heap	0x0100											HEAP AREA SIZE			*/
/*																			*/
/*----------------------------------------------------------------------------*/


/****************************************************************************/
/* SPECIFY THE SYSTEM MEMORY MAP											*/
/****************************************************************************/
MEMORY {
	SFR								: ORIGIN = 0x0000, LENGTH = 0x0010
	PERIPHERALS_8BIT				: ORIGIN = 0x0010, LENGTH = 0x00F0
	PERIPHERALS_16BIT				: ORIGIN = 0x0100, LENGTH = 0x0100
	RAM								: ORIGIN = 0x1C00, LENGTH = 0x0FFE
	INFOA							: ORIGIN = 0x1980, LENGTH = 0x0080
	INFOB							: ORIGIN = 0x1900, LENGTH = 0x0080
	INFOC							: ORIGIN = 0x1880, LENGTH = 0x0080
	INFOD							: ORIGIN = 0x1800, LENGTH = 0x0080
	FLASH							: ORIGIN = 0x8000, LENGTH = 0x7F80
	INT00							: ORIGIN = 0xFF80, LENGTH = 0x0002
	INT01							: ORIGIN = 0xFF82, LENGTH = 0x0002
	INT02							: ORIGIN = 0xFF84, LENGTH = 0x0002
	INT03							: ORIGIN = 0xFF86, LENGTH = 0x0002
	INT04							: ORIGIN = 0xFF88, LENGTH = 0x0002
	INT05							: ORIGIN = 0xFF8A, LENGTH = 0x0002
	INT06							: ORIGIN = 0xFF8C, LENGTH = 0x0002
	INT07							: ORIGIN = 0xFF8E, LENGTH = 0x0002
	INT08							: ORIGIN = 0xFF90, LENGTH = 0x0002
	INT09							: ORIGIN = 0xFF92, LENGTH = 0x0002
	INT10							: ORIGIN = 0xFF94, LENGTH = 0x0002
	INT11							: ORIGIN = 0xFF96, LENGTH = 0x0002
	INT12							: ORIGIN = 0xFF98, LENGTH = 0x0002
	INT13							: ORIGIN = 0xFF9A, LENGTH = 0x0002
	INT14							: ORIGIN = 0xFF9C, LENGTH = 0x0002
	INT15							: ORIGIN = 0xFF9E, LENGTH = 0x0002
	INT16							: ORIGIN = 0xFFA0, LENGTH = 0x0002
	INT17							: ORIGIN = 0xFFA2, LENGTH = 0x0002
	INT18							: ORIGIN = 0xFFA4, LENGTH = 0x0002
	INT19							: ORIGIN = 0xFFA6, LENGTH = 0x0002
	INT20							: ORIGIN = 0xFFA8, LENGTH = 0x0002
	INT21							: ORIGIN = 0xFFAA, LENGTH = 0x0002
	INT22							: ORIGIN = 0xFFAC, LENGTH = 0x0002
	INT23							: ORIGIN = 0xFFAE, LENGTH = 0x0002
	INT24							: ORIGIN = 0xFFB0, LENGTH = 0x0002
	INT25							: ORIGIN = 0xFFB2, LENGTH = 0x0002
	INT26							: ORIGIN = 0xFFB4, LENGTH = 0x0002
	INT27							: ORIGIN = 0xFFB6, LENGTH = 0x0002
	INT28							: ORIGIN = 0xFFB8, LENGTH = 0x0002
	INT29							: ORIGIN = 0xFFBA, LENGTH = 0x0002
	INT30							: ORIGIN = 0xFFBC, LENGTH = 0x0002
	INT31							: ORIGIN = 0xFFBE, LENGTH = 0x0002
	INT32							: ORIGIN = 0xFFC0, LENGTH = 0x0002
	INT33							: ORIGIN = 0xFFC2, LENGTH = 0x0002
	INT34							: ORIGIN = 0xFFC4, LENGTH = 0x0002
	INT35							: ORIGIN = 0xFFC6, LENGTH = 0x0002
	INT36							: ORIGIN = 0xFFC8, LENGTH = 0x0002
	INT37							: ORIGIN = 0xFFCA, LENGTH = 0x0002
	INT38							: ORIGIN = 0xFFCC, LENGTH = 0x0002
	INT39							: ORIGIN = 0xFFCE, LENGTH = 0x0002
	INT40							: ORIGIN = 0xFFD0, LENGTH = 0x0002
	INT41							: ORIGIN = 0xFFD2, LENGTH = 0x0002
	INT42							: ORIGIN = 0xFFD4, LENGTH = 0x0002
	INT43							: ORIGIN = 0xFFD6, LENGTH = 0x0002
	INT44							: ORIGIN = 0xFFD8, LENGTH = 0x0002
	INT45							: ORIGIN = 0xFFDA, LENGTH = 0x0002
	INT46							: ORIGIN = 0xFFDC, LENGTH = 0x0002
	INT47							: ORIGIN = 0xFFDE, LENGTH = 0x0002
	INT48							: ORIGIN = 0xFFE0, LENGTH = 0x0002
	INT49							: ORIGIN = 0xFFE2, LENGTH = 0x0002
	INT50							: ORIGIN = 0xFFE4, LENGTH = 0x0002
	INT51							: ORIGIN = 0xFFE6, LENGTH = 0x0002
	INT52							: ORIGIN = 0xFFE8, LENGTH = 0x0002
	INT53							: ORIGIN = 0xFFEA, LENGTH = 0x0002
	INT54							: ORIGIN = 0xFFEC, LENGTH = 0x0002
	INT55							: ORIGIN = 0xFFEE, LENGTH = 0x0002
	INT56							: ORIGIN = 0xFFF0, LENGTH = 0x0002
	INT57							: ORIGIN = 0xFFF2, LENGTH = 0x0002
	INT58							: ORIGIN = 0xFFF4, LENGTH = 0x0002
	INT59							: ORIGIN = 0xFFF6, LENGTH = 0x0002
	INT60							: ORIGIN = 0xFFF8, LENGTH = 0x0002
	INT61							: ORIGIN = 0xFFFA, LENGTH = 0x0002
	INT62							: ORIGIN = 0xFFFC, LENGTH = 0x0002
	RESET							: ORIGIN = 0xFFFE, LENGTH = 0x0002
}

/****************************************************************************/
/* SPECIFY THE SECTIONS ALLOCATION INTO MEMORY								*/
/****************************************************************************/

SECTIONS
{
	.bss			: {} > RAM					/* GLOBAL & STATIC VARS				*/
	.data			: {} > RAM					/* GLOBAL & STATIC VARS				*/
	.sysmem			: {} > RAM					/* DYNAMIC MEMORY ALLOCATION AREA	*/
	.stack			: {} > RAM					/* SOFTWARE SYSTEM STACK			*/

	.text			: {} > FLASH				/* CODE								*/
	.cinit			: {} > FLASH				/* INITIALIZATION TABLES			*/
	.const			: {} > FLASH				/* CONSTANT DATA					*/
	.cio			: {} > RAM					/* C I/O BUFFER						*/

	.pinit			: {} > FLASH				/* C++ CONSTRUCTOR TABLES			*/
	.init_array		: {} > FLASH				/* C++ CONSTRUCTOR TABLES			*/
	.mspabi.exidx	: {} > FLASH				/* C++ CONSTRUCTOR TABLES			*/
	.mspabi.extab	: {} > FLASH				/* C++ CONSTRUCTOR TABLES			*/

	.infoA			: {} > INFOA				/* MSP430 INFO FLASH MEMORY SEGMENTS */
	.infoB			: {} > INFOB
	.infoC			: {} > INFOC
	.infoD			: {} > INFOD

/* MSP430 INTERRUPT VECTORS				*/
	.int00				: {}		> INT00
	.int01				: {}		> INT01
	.int02				: {}		> INT02
	.int03				: {}		> INT03
	.int04				: {}		> INT04
	.int05				: {}		> INT05
	.int06				: {}		> INT06
	.int07				: {}		> INT07
	.int08				: {}		> INT08
	.int09				: {}		> INT09
	.int10				: {}		> INT10
	.int11				: {}		> INT11
	.int12				: {}		> INT12
	.int13				: {}		> INT13
	.int14				: {}		> INT14
	.int15				: {}		> INT15
	.int16				: {}		> INT16
	.int17				: {}		> INT17
	.int18				: {}		> INT18
	.int19				: {}		> INT19
	.int20				: {}		> INT20
	.int21				: {}		> INT21
	.int22				: {}		> INT22
	.int23				: {}		> INT23
	.int24				: {}		> INT24
	.int25				: {}		> INT25
	.int26				: {}		> INT26
	.int27				: {}		> INT27
	.int28				: {}		> INT28
	.int29				: {}		> INT29
	.int30				: {}		> INT30
	.int31				: {}		> INT31
	.int32				: {}		> INT32
	.int33				: {}		> INT33
	.int34				: {}		> INT34
	.int35				: {}		> INT35
	.int36				: {}		> INT36
	.int37				: {}		> INT37
	.int38				: {}		> INT38
	.int39				: {}		> INT39
	.int40				: {}		> INT40
	.int41				: {}		> INT41
	.int42				: {}		> INT42
	.int43				: {}		> INT43
	.int44				: {}		> INT44
	AES					: { * ( .int45 ) } > INT45 type = VECT_INIT
	RTC					: { * ( .int46 ) } > INT46 type = VECT_INIT
	LCD_B				: { * ( .int47 ) } > INT47 type = VECT_INIT
	PORT2				: { * ( .int48 ) } > INT48 type = VECT_INIT
	PORT1				: { * ( .int49 ) } > INT49 type = VECT_INIT
	TIMER1_A1			: { * ( .int50 ) } > INT50 type = VECT_INIT
	TIMER1_A0			: { * ( .int51 ) } > INT51 type = VECT_INIT
	DMA					: { * ( .int52 ) } > INT52 type = VECT_INIT
	CC1101				: { * ( .int53 ) } > INT53 type = VECT_INIT
	TIMER0_A1			: { * ( .int54 ) } > INT54 type = VECT_INIT
	TIMER0_A0			: { * ( .int55 ) } > INT55 type = VECT_INIT
	ADC12				: { * ( .int56 ) } > INT56 type = VECT_INIT
	USCI_B0				: { * ( .int57 ) } > INT57 type = VECT_INIT
	USCI_A0				: { * ( .int58 ) } > INT58 type = VECT_INIT
	WDT					: { * ( .int59 ) } > INT59 type = VECT_INIT
	COMP_B				: { * ( .int60 ) } > INT60 type = VECT_INIT
	UNMI				: { * ( .int61 ) } > INT61 type = VECT_INIT
	SYSNMI				: { * ( .int62 ) } > INT62 type = VECT_INIT
	.reset				: {}	> RESET	/* MSP430 RESET VECTOR			*/ 
}

/****************************************************************************/
/* INCLUDE PERIPHERALS MEMORY MAP													*/
/****************************************************************************/

-l cc430f6137.cmd



