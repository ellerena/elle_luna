/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/***************************************** Include section ****************************************/
// system
#include <stdint.h>
#include <msp430.h>

// driver
#include "display.h"
#include "altitude.h"
#include "vti_ps.h"
#include "ports.h"
#include "timer.h"

// logic
#include "user.h"

/*************************************** Prototypes section ***************************************/

/***************************************** Defines section ****************************************/
#define INSTANT									(0u)
#define STEPPED									(1u)	/* increase value in small steps */

/************************************ Global Variable section *************************************/
struct alt sAlt;

/**************************************** Extern section ******************************************/
extern uint8_t ps_ok;						/* Global flag for pressure sensor initialisation status */

/**
 * @brief	Reset altitude measurement.
 */
void reset_altitude_measurement(void)
{
	sAlt.altitude = 0;						/* Set default altitude value */
	sAlt.timeout = 0;						/* Clear timeout counter */
	sAlt.state = MENU_ITEM_NOT_VISIBLE;		/* Menu item is not visible */

	if (ps_ok)								/* Pressure sensor ok? */
	{
		init_pressure_table();				/* Initialise pressure table */
		start_altitude_measurement();
		stop_altitude_measurement();

		if (sAlt.altitude_offset)			/* Apply calibration offset and recalculate pressure table */
		{
			sAlt.altitude += sAlt.altitude_offset;
			update_pressure_table(sAlt.altitude, sAlt.pressure, sAlt.temperature);
		}
	}
}

/**
 * @brief	Convert meters to feet
 * @param	m	Meters
 * @return	Feet
 */
int16_t convert_m_to_ft(int16_t m)
{
	return (((int32_t) 328 * m) / 100);
}

/**
 * @brief	Convert feet to meters
 * @param	ft		Feet
 * @return	Meters
 */
int16_t convert_ft_to_m(int16_t ft)
{
	return (((int32_t) ft * 61) / 200);
}

/**
 * @brief	Start altitude measurement
 */
void start_altitude_measurement(void)
{
	if (!ps_ok)												/* Show warning if pressure sensor was not initialised properly */
	{
		display_chars(LCD_SEG_L1_1_0, (uint8_t *) "PS", SEG_ON);
	}
	else if (sAlt.timeout == 0)								/* Start altitude measurement if timeout has elapsed */
	{
		PS_INT_IFG &= ~PS_INT_PIN;							/* clear any pending IFG requests from PS */
		PS_INT_IE |= PS_INT_PIN;

		ps_start();											/* start pressure sensor */
		sAlt.timeout = ALTITUDE_MEASUREMENT_TIMEOUT;		/* start timeout control to avoid draining the battery */

		while (!(PS_INT_IN & PS_INT_PIN));					/* sit here to wait for PS IRQ */
		do_altitude_measurement(INSTANT);					/* grab the new altitude data from PS */
	}
}

/**
 * @brief	Stop altitude measurement
 */
void stop_altitude_measurement(void)
{
	if (ps_ok)												/* don't run this if PS not properly initialized */
	{
		ps_stop();											/* stop pressure sensor */

		PS_INT_IE &= ~PS_INT_PIN;							/* disable DRDY IRQ detection */
		PS_INT_IFG &= ~PS_INT_PIN;							/* clear DRDY IRQ flag (if pending) */

		sAlt.timeout = 0;									/* clear timeout counter */
	}
}

/**
 * @brief	Perform single altitude measurement
 * @param	step	Filter option
 */
void do_altitude_measurement(uint16_t step)
{
	uint32_t pressure;

	sAlt.temperature = ps_get_temp();				/* Get temperature (format is 10*K) from sensor (clears DRDY) */
	pressure = ps_get_pa();							/* Get pressure (format is 1Pa) from sensor */

	if (step)										/* if required, adjust the new value in small steps */
	{
		pressure = (uint32_t) ((pressure * 0.2)
					+ (sAlt.pressure * 0.8));		/* change pressure in small steps */
	}

	sAlt.pressure = pressure;						/* Store measured pressure value */
	sAlt.altitude = conv_pa_to_meter(pressure, sAlt.temperature);	/* Convert pressure (Pa) and temperature (K) to altitude (m) */
}

/**
 * @brief	Altitude direct function.
 * @param	line LINE1, LINE2
 */
void sx_altitude(uint16_t line)
{
	/* Function can be empty */
	/* Restarting of altitude measurement will be done by subsequent full display update */
}

/**
 * @brief	Mx button handler to set the altitude offset. ****Use feet units only****
 * @param	tmp, don't care since line is fixed to LINE1
 */
void mx_altitude(uint16_t tmp)
{
	clear_display_all();
	tmp = sAlt.altitude;							/* get global altitude (m) in local variable */
	tmp = convert_m_to_ft(tmp);						/* get altitude in feet */

	display_symbol(LCD_L1_FT, SEG_ON);				/* lid "ft" symbol */

	while (!(sys.flags & SYS_IDLE_TIMEOUT))			/* Loop values until all are set or user breaks set */
	{
		if (button.flags & BUTTON_STAR_PIN)			/* Button STAR (short): save, then exit */
		{
			tmp = convert_ft_to_m(tmp);				/* convert ft back to m before updating pressure table */
			update_pressure_table((int16_t)tmp, sAlt.pressure, sAlt.temperature);	/* Update pressure table */
			break;
		}

		set_value((int16_t*)&tmp, 4, 3, -300, 9999, SETVAL_FAST
					+ SETVAL_SIGNED, LCD_SEG_L1_3_0, display_value);	/* Set current altitude - offset is set when leaving function */
	}
	display_symbol(LCD_L1_FT, SEG_OFF);
}

/**
 * @brief		Display routine. Supports display in meters and feet.
 * @param	line	LINE1
 * 			update	DISPLAY_LINE_UPD_FULL, DISPLAY_LINE_UPD_PARTIAL, DISPLAY_ICONS_OFF
 */
void display_altitude(uint16_t temp, uint16_t update)
{
	uint8_t *str;
	int16_t	tmp1;

	if (update == DISPLAY_LINE_UPD_FULL)							/* redraw whole screen */
	{
		sAlt.state = MENU_ITEM_VISIBLE;								/* Enable pressure measurement */
		start_altitude_measurement();								/* Start measurement */

		temp = (sys.flags & SYS_UNITS) ? LCD_L1_M : LCD_L1_FT;		/* select 'm' or 'ft' icon */
		display_symbol(temp, SEG_ON);								/* lid the unit symbol icon */
		display_altitude(LINE1, DISPLAY_LINE_UPD_PARTIAL);			/* recursively display rest of altitude */
	}
	else if (update == DISPLAY_LINE_UPD_PARTIAL)
	{
		if (sAlt.timeout)											/* ensure no timeout happened */
		{
			tmp1 = sAlt.altitude;									/* capture global var in a local register */
			if (!(sys.flags & SYS_UNITS))							/* ENGLISH system: do conversion to ft */
			{
				tmp1 = convert_m_to_ft(tmp1);						// Convert from meters to feet */
				if (tmp1 > 9999) tmp1 = 9999;						// Limit to 9999m */
			}
			if (tmp1 >= 0)											// leave altitude in xxxx ft format. Use 3 leading blank digits
			{
				temp = SEG_OFF;
			}
			else													/* if value is negative, make it positive */
			{
				tmp1 = -tmp1;
				temp = SEG_ON;
			}
			str = int_to_array(tmp1, 4, 3);
			display_chars(LCD_SEG_L1_3_0, str, SEG_ON);
			display_symbol(LCD_ARROW_DOWN, temp);
		}
	}
	else															/* if DISPLAY_ICONS_OFF */
	{
		sAlt.state = MENU_ITEM_NOT_VISIBLE;							/* Disable pressure measurement */
		stop_altitude_measurement();								/* Stop measurement */

		display_symbol(LCD_L1_M, SEG_OFF);
		display_symbol(LCD_L1_FT, SEG_OFF);
		display_symbol(LCD_ARROW_DOWN, SEG_OFF);
	}
}

