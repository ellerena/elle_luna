/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

#ifndef STOPWATCH_H_
#define STOPWATCH_H_

/***************************************** Include section ****************************************/


/*************************************** Prototypes section ***************************************/
extern void start_stopwatch(void);
extern void stop_stopwatch(void);
extern void reset_stopwatch(void);
extern void stopwatch_tick(void);
extern void stopwatch_update_timer(void);
extern void mx_stopwatch(uint16_t line);
extern void sx_stopwatch(uint16_t line);
extern void display_stopwatch(uint16_t, uint16_t);

/***************************************** Defines section ****************************************/
#define STOPWATCH_1HZ_TICK				(XTAL_Y1 / 1)		/* timer step to get 1Hz IRQ */
#define STOPWATCH_100HZ_TICK			(XTAL_Y1 / 100)		/* timer step to get 100Hz IRQ */

#define STOPWATCH_STOP					(0u)				/* stopwatch state: stopped */
#define STOPWATCH_RUN					(1u)				/* stopwatch state: running */
#define STOPWATCH_HIDE					(2u)				/* stopwatch state: running in background */

#define	RESOLUTION_1HZ					(0u)				/* display time at 1Hz speed */
#define	RESOLUTION_100HZ				(1u)				/* display time at 100Hz speed */

#define	STOPWATCH_ADJ1					(1u)
#define	STOPWATCH_ADJ10					(2u)

#define	STW_F100						sStopwatch.time[7]
#define	STW_F010						sStopwatch.time[6]
#define	STW_SECL						sStopwatch.time[5]
#define	STW_SECH						sStopwatch.time[4]
#define	STW_MINL						sStopwatch.time[3]
#define	STW_MINH						sStopwatch.time[2]
#define	STW_HOUL						sStopwatch.time[1]
#define	STW_HOUH						sStopwatch.time[0]

#define	STW_CCTL2_STOP					(0)
#define	STW_CCTL2_START					(CCIE)

/************************************ Global Variable section *************************************/
struct stopwatch
{
	uint8_t state;			/* STOPWATCH_STOP, STOPWATCH_RUN, STOPWATCH_HIDE */
	uint8_t drawFlag;		/* 1:hourH, 2:hourL, 3:MinH, 4:MinL, 5:SecH, 6:SecL, 7:1/10, 8:1/100 */
	uint8_t time[8];		/* time[0]:hour HL, [2]min HL, [4]sec HL, [6] 1/10sec, [7] 1/100sec */
	uint8_t	adjust;			/* flags used to control timer rounding down needed */
	uint8_t viewResol;		/* Display resolution (RESOLUTION_1HZ or RESOLUTION_100HZ) */
};
extern struct stopwatch sStopwatch;

/**************************************** Extern section ******************************************/


#endif							/*STOPWATCH_H_ */
