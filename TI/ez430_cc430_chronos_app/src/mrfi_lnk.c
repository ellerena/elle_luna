/***********************************************************************************

    Filename:     mrfi_link.c

    Description:  MRFI link implementation.

***********************************************************************************/

/***************************** INCLUDES *****************************/
#include "string.h"
#include "mrfi_lnk.h"

#define TRUE			1
#define FALSE			0
#define HI_UINT16(a) (((uint16_t)(a) >> 8) & 0xFF)
#define LO_UINT16(a) ((uint16_t)(a) & 0xFF)

/************************ PRIVATE CONSTANTS ************************/
#define MRFI_LINK_PAN_ID				0x2007		// MRFI address definitions

// MRFI doesn't inherently support ACK - adding it at link level
#define MRFI_LINK_DATA					0x7E
#define MRFI_LINK_ACK					0x7F

/************************ PRIVATE VARIABLES ************************/
XDATA mrfiPacket_t pkt_in;
XDATA mrfiPacket_t pkt_out;
XDATA uint8_t dest_addr[4];            // Destination address
XDATA uint8_t src_addr[4];             // Source address
XDATA volatile uint8_t mrfiPktRdy;     // TRUE when a valid data packet is ready
XDATA volatile uint8_t fAckRdy;        // TRUE when a valid ACK has been received
XDATA uint8_t seqSend, seqRecv;        // Sequence numbers

/************************ PUBLIC VARIABLES ************************/
mrfiPacket_t * CODE pPktOut = &pkt_out;
mrfiPacket_t * CODE pPktIn = &pkt_in;

/************************ PRIVATE FUNCTION PROTOYPES ************************/
/**
* @brief       Send an acknowledge packet (no payload)
*/
static void sendAck(void)
{
	__bsp_ISTATE_T__ v;

	v = __bsp_GET_ISTATE__();
	MRFI_SET_PAYLOAD_LEN(&pkt_out, 2);
	MRFI_P_PAYLOAD(&pkt_out)[0]= seqSend;
	MRFI_P_PAYLOAD(&pkt_out)[1]= MRFI_LINK_ACK;
	__bsp_RESTORE_ISTATE__(v);
	MRFI_Transmit(&pkt_out, MRFI_TX_TYPE_FORCED);
}

/**
* @brief       Wait for acknowledge
* @param       timeout in milliseconds
* @return      TRUE if the ACK has been received
*/
static uint8_t waitAck(uint8_t timeout)
{
	uint16_t t;

	t = timeout * 1000;
	while (t--)
	{
//		BSP_Delay(1000);		// wait 1ms
		if(fAckRdy) break;
	}
	timeout = fAckRdy;
	fAckRdy= FALSE;
	return timeout;
}

/************************ PUBLIC FUNCTIONS ************************/
/**
* @brief      Initialise the MRFI layer. Selects RF channel and addresses.
* @param      src - source address (16 bit)
* @param      dst - destination address (16 bit)
*/
void mrfiLinkInit(uint16_t src, uint16_t dst, uint8_t mrfiChannel)
{
#ifdef USB_SUSPEND_HOOKS    // Register USB hooks if necessary
    pFnSuspendEnterHook= MRFI_Sleep;
    pFnSuspendExitHook= linkRestore;
#endif

   /* Initialise the addresses */
	src_addr[0]=  LO_UINT16(MRFI_LINK_PAN_ID);
	src_addr[1]=  HI_UINT16(MRFI_LINK_PAN_ID);
	src_addr[2]=  LO_UINT16(src);
	src_addr[3]=  HI_UINT16(src);

	MRFI_P_DST_ADDR(pPktOut)[0] = LO_UINT16(MRFI_LINK_PAN_ID);
	MRFI_P_DST_ADDR(pPktOut)[1] = HI_UINT16(MRFI_LINK_PAN_ID);
	MRFI_P_DST_ADDR(pPktOut)[2] = LO_UINT16(dst);
	MRFI_P_DST_ADDR(pPktOut)[3] = HI_UINT16(dst);

	MRFI_P_SRC_ADDR(pPktOut)[0] = LO_UINT16(MRFI_LINK_PAN_ID);
	MRFI_P_SRC_ADDR(pPktOut)[1] = HI_UINT16(MRFI_LINK_PAN_ID);
	MRFI_P_SRC_ADDR(pPktOut)[2] = LO_UINT16(src);
	MRFI_P_SRC_ADDR(pPktOut)[3] = HI_UINT16(src);

	MRFI_P_PAYLOAD(pPktOut)[2] = 'C';
	MRFI_P_PAYLOAD(pPktOut)[3] = '4';
	MRFI_P_PAYLOAD(pPktOut)[4] = '3';
	MRFI_P_PAYLOAD(pPktOut)[5] = '0';
	MRFI_P_PAYLOAD(pPktOut)[6] = '\r';

	MRFI_SET_PAYLOAD_LEN(pPktOut, 0);

													/* Initialise MRFI link housekeeping data */
	mrfiPktRdy= FALSE;
	fAckRdy= FALSE;
	seqSend= 0;
	seqRecv= 0;
													/* Initialise MRFI */
	MRFI_Init();
	MRFI_WakeUp();
	MRFI_SetLogicalChannel(mrfiChannel);
	MRFI_RxOn();
	MRFI_SetRxAddrFilter(src_addr);
	MRFI_EnableRxAddrFilter();

	MRFI_SET_PAYLOAD_LEN(pPktOut, 7);
	mrfiLinkBroadcast();
}

/**
* @brief      Read data from the RX buffer
* @param      pBuf - buffer for storage of received data
* @return     Number of bytes received
*/
uint8_t mrfiLinkRecv(void)
{
	uint8_t n;

	if (mrfiPktRdy)
	{
		uint8_t pktType;
		__bsp_ISTATE_T__ v;
													/* Process the packet */
		v = __bsp_GET_ISTATE__();

		n= MRFI_GET_PAYLOAD_LEN(&pkt_in)-2;
		pktType= MRFI_P_PAYLOAD(&pkt_in)[1];
		if ( pktType==MRFI_LINK_DATA && n)
		{
			sendAck();
		}
		mrfiPktRdy= FALSE;

		__bsp_RESTORE_ISTATE__(v);
	}
	else
	{
		n= 0;
	}

	return n;
}

/**
* @brief      Send data on the RX link.
* @param      pBuf - buffer to be transmitted
* @param      len -  number of bytes to be transmitted
* @return     Return code indicates success or failure of transmit:
*                  MRFI_TX_RESULT_SUCCESS - transmit succeeded
*                  MRFI_TX_RESULT_FAILED  - transmit failed because CCA or ACK failed
*/
uint8_t mrfiLinkSend(uint8_t nRetrans)
{
	uint8_t status = 0;

	MRFI_P_PAYLOAD(pPktOut)[0]= seqSend;
	MRFI_P_PAYLOAD(pPktOut)[1]= MRFI_LINK_DATA;

	while (nRetrans--)
	{
		status= MRFI_Transmit(&pkt_out, MRFI_TX_TYPE_CCA);
		if (status==MRFI_TX_RESULT_SUCCESS)
		{
			if (waitAck(20))					/* wait for acknowledge, if received... */
			{
				seqSend++;
				break;
			}
			else								/* timed out without acknowledge */
			{
				status= MRFI_TX_RESULT_ACK_TIMEOUT;
			}
		}
	}

	return status;
}

/**
 *	@brief	Simplest broadcasting.
 * */
void mrfiLinkBroadcast(void)
{
	MRFI_P_PAYLOAD(&pkt_out)[0]= seqSend++;
	MRFI_P_PAYLOAD(&pkt_out)[1]= MRFI_LINK_DATA;

	MRFI_Transmit(&pkt_out, MRFI_TX_TYPE_FORCED);
}


/**
* @brief       Returns true if RF data is ready
* @return      true if data is ready
*/
uint8_t mrfiLinkDataRdy(void)
{
	return mrfiPktRdy;
}

/**
* @brief       This function is called by the ISR of MRFI at RF receive and MUST
*              be included in all applications. It should be as short as possible
*              to avoid lengthy processing in the ISR.
*/
void MRFI_RxCompleteISR()
{
	uint8_t pktType;

	MRFI_Receive(&pkt_in);
	pktType= MRFI_P_PAYLOAD(&pkt_in)[1];

	if (MRFI_GET_PAYLOAD_LEN(&pkt_in)==2 && pktType==MRFI_LINK_ACK)
	{
		fAckRdy= TRUE;
	}
	else if (pktType==MRFI_LINK_DATA)
	{
		mrfiPktRdy= TRUE;
	}
}


