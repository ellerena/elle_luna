/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/


/***************************************** Include section ****************************************/
/* system */
#include <stdint.h>
#include <string.h>
#include <msp430.h>

/* driver */
#include "display.h"
#include "battery.h"

/* logic */
#include "rfbsl.h"
#include "user.h"
#include "mrfi_lnk.h"
/*#include "bluerobin.h"*/
/*#include "rfsimpliciti.h"*/

/************************************ Global Variable section *************************************/
uint8_t rfBSL_button_confirmation = 0;
const uint8_t vers[] = APP_VER;

/**
 * @brief	This functions starts the RFBSL
 * @param	tmp,	don't care
 */
void sx_rfbsl(uint16_t tmp)
{
	if (sBatt.voltage < BATT_LOW_LIMIT)
		return;										/* exit if battery voltage is too low for radio operation */

/*	if (is_bluerobin()) return;						Exit if BlueRobin stack is active */
/*	if (is_rf()) return;							Exit if SimpliciTI stack is active */

	if (++rfBSL_button_confirmation == 3)
	{
		display_chars(LCD_SEG_L2_5_0, (uint8_t *) " RFBSL", SEG_ON);
		CALL_RFSBL();								/* Call RFBSL */
	}
}

/**
 * @brief	RFBSL display routine.
 * @param	line	don't care, fixed to LINE2
 * 			update	DISPLAY_LINE_UPD_FULL
 */
void display_rfbsl(uint16_t tmp, uint16_t update)
{
	if (update & DISPLAY_LINE_UPD_FULL)
	{
		display_chars(LCD_SEG_L2_5_0, (uint8_t*)vers, SEG_ON);
	}
	else if (update == DISPLAY_LINE_UPD_PARTIAL)
	{
		static uint8_t k = 0;
		uint8_t *str;
		str = int_to_array(k++, 4, 0);											/* Display acceleration in x.xx format */
		memcpy(((uint8_t *)MRFI_P_PAYLOAD(pPktOut) + 2), str, 4);
		mrfiLinkBroadcast();
	}
/*	else
	{
	}*/
}



