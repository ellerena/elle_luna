/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/***************************************** Include section ****************************************/
/* system */
#include <stdint.h>
#include <msp430.h>

/* driver */
#include "display.h"
#include "vti_as.h"

/* logic */
#include "acceleration.h"

/***************************************** Defines section ****************************************/
/* Macro to generate a bit mask according to a given bit number. Ex. BIT(3) expands to 8 (== 0x08) */
#ifndef BIT
	#define	BIT(x)									(1 << x)
#endif


/************************************ Global Variable section *************************************/
struct accel sAccel;
AS_DAT_TYPE in[3] = {0, 0, 0};
uint16_t	k = 0;

/* Conversion vector from data to mgrav taken from CMA3000-D0x datasheet (rev 0.4, table 4)
G_RANGE Output sample   B0              B1  B2  B3  B4      B5      B6      B7
2g – 1  400 Hz, 100 Hz  1/56 = 18 mg    36  71  143 286     571     1142    s
2g – 1  40 Hz, 10 Hz    1/14 = 71 mg    143 286 571 1142    2286    4571    s
8g – 0  400 Hz, 100 Hz  1/14 = 71 mg    143 286 571 1142    2286    4571    s
8g – 0  40 Hz, 10 Hz    1/14 = 71 mg    143 286 571 1142    2286    4571    s
*/
uint16_t mgrav_per_bit[7] = { 18, 36, 71, 143, 286, 571, 1142 };

/**************************************** Extern section ******************************************/

extern uint8_t as_ok;				/* Global flag for proper acceleration sensor operation */

/***************************************** Main section *******************************************/
/**
 * @brief	Reset acceleration variables.
 */
void reset_acceleration(void)
{
	sAccel.state = MENU_ITEM_NOT_VISIBLE;			/* Default mode is off */
	sAccel.show_axis = ACCEL_AXIS_Y;				/* Start with Y-axis display */
	sAccel.timeout = 0;								/* Clear timeout counter */
}

/**
 * @brief		Acceleration direct function. Button UP switches between X/Y/Z values.
 * @param		line		LINE2
 */
void sx_acceleration(uint16_t line)
{
	as_get_data(in);								/* get x, y and z data from sensor */
}

/**
 * @brief	Converts measured value to mgrav units
 * @param	value	g data from sensor
 * @return	Acceleration (mgrav)
 */
uint16_t acc_value_to_mgrav(int8_t value)
{
	uint16_t result, *ptemp;

	if (value < 0)									/* if value is negative get its absolute value */
	{
		value = -value;								/* convert 2's complement to positive number */
	}

	result = 0;
	ptemp = mgrav_per_bit;

	while(value)									/* this loop will iterate throught each bit of value */
	{
		if (value & 1u)								/* if bit position is set */
			result += *ptemp++;						/* accumulate correspondent gravity constant */
		value >>= 1;								/* shift to point to next bit position */
	}

	return (result);
}

/**
 * @brief	Get sensor data and store in sAccel struct
 */
void do_acceleration_measurement(void)
{
	k++;
	as_get_data(in);								/* get x, y, z data from sensor */
//	display.flags |= DISP_UPD_ACCEL;				/* flag out to request partial display update*/
}

/**
 * @brief	Display routine.
 * @param	line	LINE1
 * 			update	DISPLAY_LINE_UPD_FULL, DISPLAY_ICONS_OFF
 */
void display_acceleration(uint16_t temp, uint16_t update)
{
	uint8_t *str;
	uint8_t raw_data;
	static uint8_t dp = 1, km=1;

	if (as_ok == AS_ST_FAIL)													/* check if last use of AS ended ok */
	{
		display_chars(LCD_SEG_L1_1_0, (uint8_t *) "AC", SEG_ON);				/* show error message */
	}
	else if (update & DISPLAY_LINE_UPD_FULL)									/* redraw whole LINE1 */
	{
		if (!(sAccel.state && sAccel.timeout))									/* don't run if acceleration is already on */
		{
			as_start();															/* start the AS */
			sAccel.state = MENU_ITEM_VISIBLE;									/* flag acceleration mode ON */
			sAccel.timeout = ACCEL_MEASUREMENT_TIMEOUT;							/* set major timeout counter */
			sAccel.data = 0;													/* clear previous acceleration data */
		}

		display_symbol(LCD_L2_KM, km);										/* Display decimal point */
		km ^= BIT0;
	}
	else if (update & DISPLAY_LINE_UPD_PARTIAL)
	{
		str = int_to_array(k, 4, 0);											/* Display acceleration in x.xx format */
		display_chars(LCD_SEG_L1_3_0, str, SEG_ON);
		display_symbol(LCD_SEG_L1_DP1, dp);
		k = 0;
		dp ^= BIT0;
//
//		raw_data = *str;														/* get raw acceleration value */
//		temp = acc_value_to_mgrav((int8_t)raw_data) / 10;						/* convert raw acceleration to (+)mgrav */
//		temp = (uint16_t) ((temp * 0.2) + (sAccel.data * 0.8));					/* slowly change acceleration value */
//		sAccel.data = temp;														/* store new acceleration into global variable */
//		str = int_to_array(temp, 3, 0);											/* Display acceleration in x.xx format */
//		*(str + 3) = update;													/* insert axis label into the output string */
//		display_chars(LCD_SEG_L1_3_0, str, SEG_ON);
	}
	else																		/* if DISPLAY_ICONS_OFF */
	{
		as_stop();																/* stop & power off acceleration sensor */
		sAccel.state = MENU_ITEM_NOT_VISIBLE;									/* mark acceleration as in STOP mode */
//		display_symbol(LCD_ARROW_DOWN, SEG_OFF);
		display_symbol(LCD_L2_KM, SEG_OFF);
	}
}


