/***********************************************************************************

    Filename:	  mrfi_link.h

	Description:  MRFI link header file.

***********************************************************************************/

#ifndef MRFI_LINK_H
#define MRFI_LINK_H

#include "mrfi.h"

/*********************** CONSTANTS AND DEFINES ***********************/
#define DEV_ADDR_LOC                0x25DE
#define DEV_ADDR_REM                DEV_ADDR_LOC    // 0x25EB
#define MRFI_CHANNEL                0

#define	MRFI_TX_RESULT_ACK_TIMEOUT		(3)

extern mrfiPacket_t * CODE pPktIn;
extern mrfiPacket_t * CODE pPktOut;

/************************ PUBLIC FUNCTIONS ************************/
void  mrfiLinkInit(uint16_t src, uint16_t dst, uint8_t mrfiChannel);
uint8_t mrfiLinkSend(uint8_t nRetrans);
uint8_t mrfiLinkRecv(void);
uint8_t mrfiLinkDataRdy(void);
void mrfiLinkBroadcast(void);

#endif
