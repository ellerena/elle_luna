/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/


/***************************************** Include section ****************************************/
// system
#include <stdint.h>
#include <stddef.h>
#include <msp430.h>

// driver
#include "display.h"

// logic
#include "menu.h"
//#include "user.h"
#include "clock.h"
#include "date.h"
#include "alarm.h"
#include "stopwatch.h"
#include "temperature.h"
#include "altitude.h"
#include "battery.h"
//#include "bluerobin.h"
//#include "rfsimpliciti.h"
#include "acceleration.h"
#include "rfbsl.h"
//#include "test.h"

/***************************************** Defines section ****************************************/
#define FUNCTION(function)		function

/*************************************** Prototypes section ***************************************/
void dummy(uint16_t);
void display_nothing(uint16_t line, uint16_t update);

/************************************ Global Variable section *************************************/
const struct menu *ptrMenu_L1 = &menu_L1_Time;	// Alarm, Heartrate, Speed, Temperature, Altitude, Acceleration;
const struct menu *ptrMenu_L2 = &menu_L2_Date;	// Stopwatch, Rf, Ppt, Sync, Distance, Calories, Battery, RFBSL;

/*************************************************************************************************
	User navigation ( [____] = default menu item after reset )

	LINE1:[Time] -> Alarm -> Temperature -> Altitude -> Heart rate -> Speed -> Acceleration
	LINE2:[Date] -> Stopwatch -> Battery-> ACC -> PPT -> SYNC -> Calories/Distance --> RFBSL
*************************************************************************************************/

/************************************* LINE 1 - MENU OPTIONS ************************************/
// Line1 - Time
const struct menu menu_L1_Time = {
	FUNCTION(sx_time),				// direct function
	FUNCTION(mx_time),				// sub menu function
	FUNCTION(display_time),			// display function
	FUNCTION(DISP_UPD_TIME),		// new display data
	&menu_L1_Alarm,
};

// Line1 - Alarm
const struct menu menu_L1_Alarm = {
	FUNCTION(sx_alarm),				// direct function
	FUNCTION(mx_alarm),				// sub menu function
	FUNCTION(display_alarm),		// display function
	FUNCTION(DISP_UPD_ALARM),	// new display data
	&menu_L1_Temperature,
};

// Line1 - Temperature
const struct menu menu_L1_Temperature = {
	FUNCTION(dummy),				// direct function
	FUNCTION(mx_temperature),		// sub menu function
	FUNCTION(display_temperature),	// display function
	FUNCTION(DISP_UPD_TEMPERATURE),		// new display data
	&menu_L1_Altitude,
};

// Line1 - Altitude
const struct menu menu_L1_Altitude = {
	FUNCTION(dummy),				// direct function
	FUNCTION(mx_altitude),			// sub menu function
	FUNCTION(display_altitude),		// display function
	FUNCTION(DISP_UPD_TIME),		// new display data
	&menu_L1_Acceleration,
};

// Line1 - Heart Rate
/*const struct menu menu_L1_Heartrate = {
	FUNCTION(sx_bluerobin),			// direct function
	FUNCTION(mx_bluerobin),			// sub menu function
	FUNCTION(display_heartrate),	// display function
	FUNCTION(DISP_UPD_TIME),		// new display data
	&menu_L1_Speed,
};*/

// Line1 - Speed
/*const struct menu menu_L1_Speed = {
	FUNCTION(dummy),				// direct function
	FUNCTION(dummy),				// sub menu function
	FUNCTION(display_speed),		// display function
	FUNCTION(DISP_UPD_TIME),		// new display data
	&menu_L1_Acceleration,
};*/

// Line1 - Acceleration
const struct menu menu_L1_Acceleration = {
	FUNCTION(sx_acceleration),		// direct function
	FUNCTION(dummy),				// sub menu function
	FUNCTION(display_acceleration),	// display function
	FUNCTION(DISP_UPD_ACCEL),		// new display data
	&menu_L1_Time,
};

/************************************* LINE 2 - MENU OPTIONS ************************************/
// Line2 - Date
const struct menu menu_L2_Date = {
	FUNCTION(sx_date),				// direct function
	FUNCTION(mx_date),				// sub menu function
	FUNCTION(display_date),			// display function
	FUNCTION(DISP_UPD_DATE),		// new display data
	&menu_L2_Stopwatch,
};

// Line2 - Stopwatch
const struct menu menu_L2_Stopwatch = {
	FUNCTION(sx_stopwatch),			// direct function
	FUNCTION(mx_stopwatch),			// sub menu function
	FUNCTION(display_stopwatch),	// display function
	FUNCTION(DISP_UPD_STOPWATCH),// new display data
	&menu_L2_Battery,
};

// Line2 - Battery
const struct menu menu_L2_Battery = {
	FUNCTION(dummy),				// direct function
	FUNCTION(dummy),				// sub menu function
	FUNCTION(display_battery_V),	// display function
	FUNCTION(DISP_UPD_BATT_VOLT),	// new display data
	&menu_L2_RFBSL,
};

// Line2 - ACC (acceleration data + button events via SimpliciTI)
/*const struct menu menu_L2_Rf = {
	FUNCTION(sx_rf),				// direct function
	FUNCTION(dummy),				// sub menu function
	FUNCTION(display_rf),			// display function
	FUNCTION(DISP_UPD_TIME),		// new display data
	&menu_L2_Ppt,
};*/

// Line2 - PPT (button events via SimpliciTI)
/*const struct menu menu_L2_Ppt = {
	FUNCTION(sx_ppt),				// direct function
	FUNCTION(dummy),				// sub menu function
	FUNCTION(display_ppt),			// display function
	FUNCTION(DISP_UPD_TIME),		// new display data
	&menu_L2_Sync,
};*/

// Line2 - SXNC (synchronization/data download via SimpliciTI)
/*const struct menu menu_L2_Sync = {
	FUNCTION(sx_sync),				// direct function
	FUNCTION(dummy),				// sub menu function
	FUNCTION(display_sync),			// display function
	FUNCTION(DISP_UPD_TIME),		// new display data
	&menu_L2_Date,
};*/

// Line2 - Calories/Distance
/*const struct menu menu_L2_CalDist = {
	FUNCTION(sx_caldist),			// direct function
	FUNCTION(mx_caldist),			// sub menu function
	FUNCTION(display_caldist),		// display function
	FUNCTION(DISP_UPD_TIME),		// new display data
	&menu_L2_RFBSL,
};*/

// Line2 - RFBSL
const struct menu menu_L2_RFBSL = {
	FUNCTION(sx_rfbsl),				// direct function
	FUNCTION(dummy),				// sub menu function
	FUNCTION(display_rfbsl),		// display function
	FUNCTION(DISP_UPD_TIME),		// new display data
	&menu_L2_Date,
};


//// Line2 - Test & Debugging
//const struct menu menu_L1_TstDbg = {
//	FUNCTION(dummy),				// direct function
//	FUNCTION(dummy),				// sub menu function
//	FUNCTION(display_tst_dbg),		// display function
//	FUNCTION(DISP_UPD_NONE),		// new display data
//	&menu_L2_Date,
//};

/****************************************** Main section ******************************************/
void dummy(uint16_t line)
{
}

void display_nothing(uint16_t line, uint16_t update)
{
}





