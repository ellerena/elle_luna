/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/********************************* Include section *********************************/
/* system */
#include <msp430.h>
#include <stdint.h>

/* driver */
#include "display.h"
#include "adc12.h"

/* logic */
#include "battery.h"

/********************************* Prototypes section *********************************/
void reset_batt_measurement(void);
void battery_measurement(void);

/********************************* Defines section *********************************/

/********************************* Global Variable section *********************************/
struct batt sBatt;

/********************************* Extern section *********************************/
/*extern void (*fptr_lcd_line2)(uint8_t line, uint8_t update); */

/**
 * @brief	Reset temperature measurement module.
*/
void reset_batt_measurement(void)
{
	sBatt.lobatt_count = 0;								/* Reset lobatt display counter */
	sBatt.voltage = BATT_START_VOLT;
}

/**
 * @brief	Init ADC12. Do single conversion of AVCC voltage. Turn off ADC12.
 * @verbatim	Convert ADC value to "x.xx V"
 * 				Ideally we have A11=0->AVCC=0V ... A11=4095(2^12-1)->AVCC=4V
 * 						--> (A11/4095)*4V=AVCC --> AVCC=(A11*4)/4095
*/
void battery_measurement(void)
{
	uint16_t volts;

	volts = adc12_single_conversion(REFVSEL_1,
			ADC12SHT0_10, ADC12INCH_11);				/* Convert external battery voltage (ADC12INCH_11=AVCC-AVSS/2) */
	volts = (volts * 2 * 2) / 41;						/* voltage conversion */
	volts += sBatt.offset;								/* Correct measured voltage with calibration value */

	if (volts > BATT_HIGH_LIMIT)						/* Discard values that are clearly outside the measurement range */
	{
		volts = sBatt.voltage;
	}

	sBatt.voltage = ((volts * 2) +
						(sBatt.voltage * 8)) / 10;		/* change voltage value in small steps */

	if (sBatt.voltage < BATT_LOW_LIMIT)					/* check if battery is low */
	{
		sBatt.lobatt_count = BATT_LOW_MSG_COUNT;		/* start timer to display low batt message for 15 seconds */
	}

	display.flags |= DISP_UPD_BATT_VOLT;				/* flag that a new voltage value needs to be printed */
}

/**
 * @brief	Display routine for battery voltage.
 * @param	tmp		don't care
 * 			update	DISPLAY_LINE_UPD_FULL, DISPLAY_ICONS_OFF
*/
void display_battery_V(uint16_t tmp, uint16_t update)
{
	uint8_t *str;

	if (update == DISPLAY_ICONS_OFF)
	{
		display_symbol(LCD_BATTERY, SEG_OFF);			/* clear battery symbol */
	}
	else												/* DISPLAY_LINE_UPD_FULL or DISPLAY_LINE_UPD_PARTIAL */
	{
		str = int_to_array(sBatt.voltage, 3, 0);		/* calculate result in xx.x format */
		display_chars(LCD_SEG_L2_2_0, str, SEG_ON);		/* print voltage value on LINE2 */

		if (update == DISPLAY_LINE_UPD_FULL)			/* Redraw line */
		{
			display_symbol(LCD_BATTERY, SEG_ON);		/* lid battery icon */
			display_symbol(LCD_SEG_L2_DP, SEG_ON);		/* lid dot point */
		}
	}
}


