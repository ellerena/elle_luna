/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/***************************************** Include section ****************************************/

/*************************************** Prototypes section ***************************************/

// internal functions
extern void reset_alarm(void);
extern void check_alarm(void);
extern void stop_alarm(void);

// menu functions
extern void sx_alarm(uint16_t);
extern void mx_alarm(uint16_t);
extern void display_alarm(uint16_t, uint16_t);

/***************************************** Defines section ****************************************/
#define ALARM_DISABLED					(0u)		/* alarm feature is completely disabled */
#define ALARM_ENABLED					(1 << 0)	/* alarm fature is enabled */

#define ALARM_ON_DURATION				(10u)		/* Keep alarm for 10 on-off cycles */

#define ALARM_DEFAULT_HOUR				(6u)
#define ALARM_DEFAULT_MINUTE			(30u)

#define ALARM_SET_HOUR					(0u)
#define ALARM_SET_MINUTE				(1u)

#define	ALARM_ST_OFFSET_STATE			(0)
#define	ALARM_ST_OFFSET_DURATION		(1)
#define	ALARM_ST_OFFSET_HOUR			(2)
#define	ALARM_ST_OFFSET_MINUTE			(3)

/************************************ Global Variable section *************************************/
struct alarm
{
	uint8_t state;					/* ALARM_DISABLED, ALARM_ENABLED, ALARM_ON */
	uint8_t duration;				/* Alarm duration */
	uint8_t hour;					/* Alarm hour */
	uint8_t minute;					/* Alarm minute */
};
extern struct alarm sAlarm;

/**************************************** Extern section ******************************************/
