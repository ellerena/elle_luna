#include <mrfi_lnk.h>
#include <stdint.h>
#include <string.h>
#include <msp430.h>
#include "bsp.h"
#include "ports.h"
#include "display.h"
#include "vti_as.h"

#define	RFOUT_16(x)		*(uint16_t*)(&MRFI_P_PAYLOAD(pPktOut)[x])

uint16_t i = (1 << 4);
uint16_t acout[3];
int16_t mgrav_table[] = { 18, 36, 71, 143, 286, 571, 1142 };
uint8_t acin[] = {0, 0, 0};

int16_t acc_to_mgrav(int8_t value)
{
	int16_t result, *ptemp, f, i;

	f = value & BIT7;

	if (f)									/* if value is negative get its absolute value */
	{
		value = (~value) + 1;				/* convert 2's complement to positive number */
	}

	result = 0;
	ptemp = mgrav_table;

	for (i = 1; i <0x80; i <<= 1)			/* this loop will iterate throught each bit of value */
	{
		if (value & i)						/* if bit position is set */
			result += *ptemp;				/* accumulate correspondent gravity constant */
		ptemp++;
	}

	return (f ? -result : result);
}

void data_release(void)
{
	int8_t i;

	i = 0;
	do
	{
		MRFI_P_PAYLOAD(pPktOut)[0] = F_MSG_TXT;
		MRFI_P_PAYLOAD(pPktOut)[1] = i;
		RFOUT_16(2) = acc_to_mgrav(i);
		RFOUT_16(4) = acc_to_mgrav(~i);
		RFOUT_16(6) = acc_to_mgrav(i | 0x7f);
		mrfiLinkBroadcast(8);
		BSP_DELAY_USECS(1000);
	} while (++i);
}

void deglitch(void)
{
	uint16_t i;

	for (i = 0; i < 500; i++)
	{
		if (P2IN & ALL_BUTTONS) i = 0;
	}
}

int main(void)
{
	uint8_t *pdat;
	int a = 0;

	BSP_Init();
	PMAPPWD = 0x02D52;								/* Enable write-access to port mapping registers */
	PMAPCTL = PMAPRECFG;							/* Allow reconfiguration during runtime */
	LCD_INIT0;
	BUTTONS_INIT;
	ACM_INIT;
	as_init();										/* Init acceleration sensor */
	mrfiLinkInit();									/* Initialize the MRFI RF link layer */
	BSP_ENABLE_INTERRUPTS();

	while (1)
	{
		switch (i)
		{
			case 0:
				break;
			case (1 << 4):
				clear_display();
				display_chars(LCD_SEG_L2_4_2, (uint8_t *) "ACC", SEG_ON);
				display_symbol(LCD_ICON_BEEPER1, SEG_ON);
				display_symbol(LCD_ICON_BEEPER2, SEG_ON_BLINK_ON);
				display_symbol(LCD_ICON_BEEPER3, SEG_ON_BLINK_ON);
				as_start();
				i = 0;
				break;
			case (1 << 5):
				as_stop();
				clear_display();
				display_symbol(LCD_ICON_BEEPER1, SEG_OFF);
				display_symbol(LCD_ICON_BEEPER2, SEG_OFF_BLINK_OFF);
				display_symbol(LCD_ICON_BEEPER3, SEG_OFF_BLINK_OFF);
				_BIS_SR(LPM4_bits + GIE);			/* To low power mode */
				break;
			case (1 << 6):							/* backlight button */
			case (1 << 7):							/* up button */
			case (1 << 8):							/* down button */
				data_release();
				clear_line(LINE2);
				display_symbol(LCD_ICON_BEEPER1, SEG_ON);
				display_symbol(LCD_ICON_BEEPER2, SEG_ON_BLINK_ON);
				display_symbol(LCD_ICON_BEEPER3, SEG_ON_BLINK_ON);
				display_chars(LCD_SEG_L2_4_0, (uint8_t *) "DATST", SEG_ON);
				i = 0;
				deglitch();
				break;
			default:
				as_get_data(acin);
				MRFI_P_PAYLOAD(pPktOut)[0] = F_MSG_TXT;
				MRFI_P_PAYLOAD(pPktOut)[1] = a++;
				RFOUT_16(2) = acc_to_mgrav(acin[0]);
				RFOUT_16(4) = acc_to_mgrav(acin[1]);
				RFOUT_16(6) = acc_to_mgrav(acin[2]);
				mrfiLinkBroadcast(8);
				if (--i)
				{
					clear_line(LINE1);
					pdat = int_to_array(i+1, 5, 0);
					display_chars(LCD_SEG_L1_3_0, pdat, SEG_ON);
				}
		}
		__no_operation();  // For debugger
	}
}

BSP_ISR_FUNCTION(PORT2_ISR, PORT2_VECTOR)
{
	uint8_t	flag;

	BSP_DISABLE_INTERRUPTS();
	flag = P2IFG & P2IE;

	if (flag & AS_INT_PIN)							/* If DRDY irq is (still) high, request data again */
	{
		i++;
	}
	else if (flag & BUTTON_STAR_PIN)
	{
		i = (1 << 4);
		__bic_SR_register_on_exit(LPM4_bits);		/* Exit LPM3/LPM4 on RETI */
	}
	else if (flag & BUTTON_NUM_PIN)
	{
		i = (1 << 5);
	}
	else if (flag & BUTTON_BACKLIGHT_PIN)
	{
		i = (1 << 6);
	}
	else if (flag & BUTTON_UP_PIN)
	{
		i = (1 << 7);
	}
	else if (flag & BUTTON_DOWN_PIN)
	{
		i = (1 << 8);
		__bic_SR_register_on_exit(LPM4_bits);		/* Exit LPM3/LPM4 on RETI */
	}

	P2IFG = 0;										/* clear all PORT2 irq flags */
	BSP_ENABLE_INTERRUPTS();
}

