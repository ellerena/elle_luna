/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/***************************************** Include section ****************************************/
/* system */
#include <stdint.h>
#include <string.h>
#include <msp430.h>

/* driver */
#include "stopwatch.h"
#include "ports.h"
#include "display.h"
#include "timer.h"

/* logic */
#include "menu.h"

/*************************************** Prototypes section ***************************************/
void start_stopwatch(void);
void stop_stopwatch(void);
void reset_stopwatch(void);
void stopwatch_tick(void);
void stopwatch_update_timer(void);

/***************************************** Defines section ****************************************/

/************************************ Global Variable section *************************************/
struct stopwatch sStopwatch;

/**************************************** Extern section ******************************************/

/***************************************** Main section *******************************************/

/**
 * @brief		Clears stopwatch counter and sets stopwatch state to off.
 */
void reset_stopwatch(void)
{
	memcpy(sStopwatch.time, "00000000",	sizeof(sStopwatch.time));	/* Clear counter */

	sStopwatch.state = STOPWATCH_STOP;								/* Init stopwatch state 'Off' */
	sStopwatch.adjust = 0;											/* reset adjustment flags */
	sStopwatch.viewResol = RESOLUTION_100HZ;						/* Default display style is MM:SS:HH */
}

/**
 * @brief		Starts stopwatch timer interrupt and sets stopwatch state to on.
 */
void start_stopwatch(void)
{
	TA0CCR2 = TA0R;										/* Init CCR register with current timer */
	stopwatch_update_timer();							/* Load CCR register with next capture time */
	TA0CCTL2 = STW_CCTL2_START;							/* enable timer interrupt and reset CCIFG2 IRQ flag */
	sStopwatch.state = STOPWATCH_RUN;					/* Set stopwatch run flag */
	display_symbol(LCD_ICON_STOPWATCH, SEG_ON);			/* Set stopwatch icon */
}

/**
 * @brief	Set new compare time for next 1/1Hz or 1/100Hz interrupt. Takes care for exact 1 second timing.
 *
 * @verbatim
 * 		100Hz timer:
 * 		stopwatch runs too slow (1 sec nominal != 100 interupts * 328 ACLK = 32800 ACLK = 1.00098 sec)
 * 		ideally correct timer value every 10 ticks by (32768 - 32800)/10 = 3.2
 * 		--> correct timer value every 10Hz by 3,
 * 		--> correct timer value every 1Hz correct by 5
 * @endverbatim
 */
void stopwatch_update_timer(void)
{
	uint16_t value;

	value = TA0CCR2;

	if (sStopwatch.viewResol)							/* RESOLUTION_100HZ: set next tick and clear swtIsxxHz */
	{
		value += STOPWATCH_100HZ_TICK;					/* calculate next timer IRQ after 32768/100 = 328 ACLK */

		if (STOPWATCH_ADJ1 == sStopwatch.adjust)		/* apply 1Hz timer correction (due to round 328 ACLK) */
		{
			value -= 5;									/* reduce by 5 */
			sStopwatch.adjust = 0;						/* flag rounding adjustment as completed */
		}
		else if (STOPWATCH_ADJ10 == sStopwatch.adjust)	/* apply 10Hz timer correction (due to round 328 ACLK) */
		{
			value -= 3;									/* reduce by 3 */
			sStopwatch.adjust = 0;						/* flag rounding adjustment as completed */
		}
	}
	else												/* RESOLUTION_1HZ: timer interrupts occur every 32768/1 = 32768 ACLK */
	{
		value += STOPWATCH_1HZ_TICK;					/* calculate next timer IRQ after 32768 ACLK */
	}

	TA0CCR2 = value;									/* Update CCR */
}

/**
 * @brief		Called by 1/100Hz interrupt handler.
 * 				Increases stopwatch counter and triggers display update.
 *
 * @verbatim
 * 				sStopwatch.drawFlag minimizes display update activity:
 * 				1: second L
 * 				2: second H/L
 * 				3: minutes L, second H/L
 * 				4: minutes H/L, second H/L
 * 				5: hours L, minutes H/L, second H/L
 * 				6: hours H/L, minutes H/L, second H/L
 * 				7: 1/10 sec, 1/100 sec
 * 				8: 1/100 sec (every 17/100 sec to reduce display draw activity)
 * @endverbatim
 */
void stopwatch_tick(void)
{
	if (sStopwatch.viewResol)							/* RESOLUTION_100HZ (< 20 minutes): MM:SS:hh */
	{
		sStopwatch.drawFlag = 0;
		if (++STW_F100 > '9')							/* increment 1/100 sec, if > '9', add 1/10 sec */
		{
			STW_F100 = '0';								/* xx:xx:xx:x0 */
			STW_F010++;
			sStopwatch.adjust = STOPWATCH_ADJ10;
			sStopwatch.drawFlag = 7;					/* mark draw flag - we print at 10Hz cause human eye can't process faster */
		}
	}
	else
	{
		STW_F010 = ':';
	}

	if (STW_F010 > '9')									/* char > '9', add 1 sec */
	{
		STW_F010 = '0';									/* xx:xx:xx:00 */
		sStopwatch.adjust = STOPWATCH_ADJ1;
		sStopwatch.drawFlag = 5;						/* Reset draw flag */
		if (++STW_SECL > '9')							/* second	L (0 - 9) xx:xx:xL:00 */
		{
			STW_SECL = '0';								/* xx:xx:x0:00 */
			if (++STW_SECH > '5')						/* second	H (0 - 5) xx:xx:H0:00 */
			{
				STW_SECH = '0';							/* xx:xx:00:00 */
				sStopwatch.drawFlag--;					/* 4 */
				if (++STW_MINL > '9')					/* minutes L (0 - 9) xx:xL:00:00 */
				{
					STW_MINL = '0';						/* xx:x0:00:00 */
					sStopwatch.drawFlag--;				/* 3 */
					if (++STW_MINH > '5')				/* minutes H (0 - 5) xx:H0:00:00 */
					{
						STW_MINH = '0';					/* xx:00:00:00 */
						sStopwatch.drawFlag--;			/* 2 */
						if (++STW_HOUL > '9')			/* hours L (0-9) xL:00:00:00 */
						{
							STW_HOUL = '0';				/* x0:00:00:00 */
							sStopwatch.drawFlag--;		/* 1 */
							if (++STW_HOUH > '1')		/* hours H (0-1) H0:00:00:00 */
							{							/* When reaching 20 hours, start over */
								reset_stopwatch();
								sStopwatch.state = STOPWATCH_RUN;
								display.flags |= DISP_UPD_LINE2;
							}
						}
					}
					else if (STW_MINH > '1')			/* minutes H (0 - 1) */
					{									/* SWT display changes from MM:SS:hh to HH:MM:SS when reaching 20 minutes */
						sStopwatch.viewResol = RESOLUTION_1HZ;
						display.flags |= DISP_UPD_LINE2;
					}

				}
			}
		}
	}

	display.flags |= DISP_UPD_STOPWATCH;				/* set our function flag to request display update */
}

/**
 * @brief		Stops stopwatch timer interrupt and sets stopwatch state to off.
 * 				Does not reset stopwatch count.
 */
void stop_stopwatch(void)
{
	TA0CCTL2 = STW_CCTL2_STOP;							/* firstly stop the timer */
	sStopwatch.state = STOPWATCH_STOP;					/* change stopwatch state */
	display_symbol(LCD_ICON_STOPWATCH, SEG_OFF);		/* turn off stopwatch icon */
	display.flags |= DISP_UPD_LINE2;					/* request upstream to repaint the whole LINE2 */
}

/**
 * @brief		Stopwatch set routine. Mx stops stopwatch and resets count.
 * @param		uint8_t line LINE2
 */
void mx_stopwatch(uint16_t line)
{
	stop_stopwatch();									/* stop stopwatch (in case hasn't been stopped yet) */
	reset_stopwatch();									/* reset stopwatch variables and counter ("00:00:00:00") */
}

/**
 * @brief		Stopwatch direct function. Starts/stops stopwatch, but does not reset count.
 * @param		tmp,	don't care, fixed internally to LINE2
 */
void sx_stopwatch(uint16_t tmp)
{
	if (sStopwatch.state == STOPWATCH_STOP)			/* if stopwatch is stopped, start it */
	{
		start_stopwatch();							/* start stopwatch */
	}
	else											/* if stopwatch in STOPWATCH_RUN or STOPWATCH_HIDE, stop it */
	{
		stop_stopwatch();							/* stop stopwatch */
	}
}

/**
 * @brief		Stopwatch user routine. Sx starts/stops stopwatch, but does not reset count.
 * @param		line LINE2
 * 				update		DISPLAY_LINE_UPD_PARTIAL, DISPLAY_LINE_UPD_FULL
 */
void display_stopwatch(uint16_t temp, uint16_t update)
{
	if (update == DISPLAY_LINE_UPD_PARTIAL)
	{
		temp = sStopwatch.drawFlag;

		if(0 != temp)														/* Check draw flag to minimize workload */
		{
			update = (uint16_t)sStopwatch.time + temp - 1;					/* array offset */
			temp += (sStopwatch.viewResol == RESOLUTION_100HZ) ? 87: 89;	/* select segment [LCD_SEG_L2_5_0..LCD_SEG_L2_1_0] */
			display_chars(temp, (uint8_t*)update, SEG_ON);
		}
	}
	else if (update == DISPLAY_LINE_UPD_FULL)								/* Redraw whole line */
	{
		temp = (uint16_t)(sStopwatch.time);									/* point to 1/100s */

		if (sStopwatch.viewResol == RESOLUTION_100HZ)						/* calculate MM:SS:hh */
		{
			temp += 2;														/* point to minutes */
		}

		display_chars(LCD_SEG_L2_5_0, (uint8_t*)temp, SEG_ON);				/* print current stopwatch value */
		display_symbol(LCD_SEG_L2_COL1, SEG_ON);							/* print 1st colon ' X:AA BB' */
		display_symbol(LCD_SEG_L2_COL0, SEG_ON);							/* print 2nd colon ' X:AA:BB' */
	}
/*	else																	 if DISPLAY_ICONS_OFF
	{
	}*/
}



