/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/***************************************** Include section ****************************************/

/* system */
#include <stdint.h>
#include <msp430.h>

/* driver */
#include "display.h"
#include "buzzer.h"
#include "ports.h"
#include "timer.h"

/* logic */
#include "alarm.h"
#include "user.h"
#include "clock.h"

/*************************************** Prototypes section ***************************************/

/***************************************** Defines section ****************************************/

/************************************ Global Variable section *************************************/
struct alarm sAlarm;

/**************************************** Extern section ******************************************/

/**
 * @brief	Resets alarmData and disables the alarm.
 */
void reset_alarm(void)
{
	sAlarm.state	= ALARM_DISABLED;
	sAlarm.duration	= 0;
	sAlarm.hour		= ALARM_DEFAULT_HOUR;
	sAlarm.minute	= ALARM_DEFAULT_MINUTE;
}

/**
 * @brief	Check if current time matches alarm time
 */
void check_alarm(void)
{
	if (sAlarm.state)									/* if alarm is not enabled nothing to do */
	{
		if (sTime.minute == sAlarm.minute)				/* only 1/60 probability to match */
		{
			if (sTime.hour == sAlarm.hour)
			{
				sAlarm.duration = ALARM_ON_DURATION;	/* start alarm by setting a duration in seconds */
			}
		}
	}
}

/**
 * @brief	Stop active alarm (but leave it in enabled state).
 */
void stop_alarm(void)
{
	sAlarm.duration = 0;								/* stop alarm - but keep the feature enabled */
	stop_buzzer();										/* Stop buzzer */
}

/**
 * @brief	Sx button turns alarm feature ON/OFF.
 * @param	tmp,	don't care (fixed internally to LINE1)
 */
void sx_alarm(uint16_t tmp)
{
	message.flags = (sAlarm.state) ?					/* flag the proper message to show */
				(MSG_PREPARE | MSG_ALARM_OFF)
			:	(MSG_PREPARE | MSG_ALARM_ON);

	sAlarm.state ^= ALARM_ENABLED;						/* invert alarm status (ENABLE <->DISABLED) */
}

/**
 * @brief	Set alarm time.
 * @param	tmp,	don't care (internally fixed to LINE1)
 */
void mx_alarm(uint16_t tmp)
{
	uint8_t *str;
	int16_t hours, minutes;

	clear_display_all();

	hours = sAlarm.hour;								/* save value in case new value is discarded */
	minutes = sAlarm.minute;							/* save value in case new value is discarded */

	tmp = hours * 100 + minutes;						/* combine hours and minutes into a single number */
	str = int_to_array(tmp, 4, 0);						/* convert time value into text */
	display_chars(LCD_SEG_L1_3_0, str, SEG_ON);			/* present the hour text on the screen */
	display_symbol(LCD_SEG_L1_COL, SEG_ON);				/* lid the colon symbol */

	tmp = ALARM_SET_HOUR;								/* Init value index (NOTE: re-use <tmp> is safe */

	while (!(sys.flags & SYS_IDLE_TIMEOUT))				/* Loop values until all are set, timeout or user breaks set */
	{

		if (button.flags & BUTTON_STAR_PIN)				/* STAR (short): save, then exit */
		{
			sAlarm.hour = hours;
			sAlarm.minute = minutes;
			display.flag.line1_full_update = 1;			/* request display to update screen */
			break;
		}

		if(ALARM_SET_HOUR == tmp)						/* Set hour */
		{
			set_value(&hours, 2, 0, 0, 23, SETVAL_ROLLOVER +
					SETVAL_NEXT, LCD_SEG_L1_3_2, display_hours);
			tmp = ALARM_SET_MINUTE;
		}
		else											/* Set minutes */
		{
			set_value(&minutes, 2, 0, 0, 59, SETVAL_ROLLOVER +
					SETVAL_NEXT, LCD_SEG_L1_1_0, display_value);
			tmp = ALARM_SET_HOUR;
		}
	}

//	display.flag.update_alarm = 1;						/* Tell display function that new value is available */
}

/**
 * @brief		Display alarm time. 24H / 12H time format.
 * @param		line	LINE1, LINE2
 * 				update	DISPLAY_LINE_UPD_FULL, DISPLAY_ICONS_OFF
 */
void display_alarm(uint16_t line, uint16_t update)
{
	if (update == DISPLAY_ICONS_OFF)
	{
		update =  (sAlarm.state) ? SEG_ON_BLINK_OFF : SEG_OFF_BLINK_OFF;			/* choose icon alarm state */
		display_symbol(LCD_ICON_ALARM, update);										/* turn off/on alarm icon */
		display_symbol(LCD_AM, SEG_OFF);											/* turn off AM/PM symbol */
	}
	else																			/* DISPLAY_LINE_UPD_FULL or DISPLAY_LINE_UPD_PARTIAL */
	{
		update = sAlarm.hour;														/* get hour in 24H format */

		if(!(sys.flags & SYS_UNITS))
		{
			display_am_pm_symbol(update);											/* display AM/PM symbol */
			update = convert_hour_to_12H_format(update);							/* get hour in 12H format */
		}

		update = (update * 100) + sAlarm.minute;									/* combine hours and minutes in a single value */
		display_chars(switch_seg(line, LCD_SEG_L1_3_0, LCD_SEG_L2_3_0),
									int_to_array(update, 4, 0), SEG_ON);			/* print time */
		display_symbol(switch_seg(line, LCD_SEG_L1_COL, LCD_SEG_L2_COL0), SEG_ON);	/* display colon symbol */
		display_symbol(LCD_ICON_ALARM, SEG_ON_BLINK_ON);							/* blink alarm icon */
	}

}

