/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/***************************************** Include section ****************************************/
/* system */
#include <stdint.h>
#include<msp430.h>

/* driver */
#include "ports.h"
#include "display.h"
#include "timer.h"

/* logic */
#include "clock.h"
#include "user.h"
/*#include "bluerobin.h"*/
#include "date.h"


/*void calc_24H_to_12H(uint8_t * hours, uint8_t * timeAM); */
/*void conv_24H_to_12H(uint8_t * hours24, uint8_t * hours12, uint8_t * timeAMorPM); */

/***************************************** Defines section ****************************************/

/************************************ Global Variable section *************************************/


/* Display values for time format selection */
const uint8_t selection_Timeformat[][3] =
{
	"12<" /* [0] 12h */,
	"24<" /* [1] 24h */
};

/**************************************** Extern section ******************************************/
//extern void (*fptr_lcd_line1)(uint16_t line, uint16_t update); */
//extern void (*fptr_lcd_line2)(uint16_t line, uint16_t update); */

/****************************************** Main section ******************************************/
/**
 * @brief	reset clock time to pre-defined initial values.
 */
void reset_clock(void)
{
	sTime.system = 0;								/* set global system time to 0 */
	sTime.last = 0;									/* reset last activity time counter */
	sTime.display = SHOW_1ST_PART;					/* screen will show hour, minute, day and month */
	sTime.hour = START_HOUR;
	sTime.minute = START_MINUTE;
	sTime.second = START_SECOND;
}

/**
 * @brief	Add 1 second to system time and to display time
 */
void add_second(void)
{
	int temp;

	sTime.system++;									/* Increase global system time */
	temp = 1;										/* sTime.drawFlag = 1: second */

	if (++sTime.second >= 60)						/* Add 1 second, check if minute complete */
	{
		temp++;										/* sTime.drawFlag = 2:minute, second */
		sTime.second = 0;

		if (++sTime.minute >= 60)					/* Add 1 minute, check if hour complete */
		{
			temp++;									/* sTime.drawFlag = 3: hour, minute */
			sTime.minute = 0;

			if (++sTime.hour >= 24)					/* Add 1 hour, check if day complete */
			{
				sTime.hour = 0;
				add_day();
			}
		}
	}
	sTime.drawFlag = temp;
}

/**
 * @brief	Convert internal 24H time to 12H time.
 * @param	hour, Hour in 24H format
 * @return	Hour in 12H format
 *
 * @verbatim
 *
 * 			00:00 .. 11:59 --> AM 00:00 .. 11:59
 * 			12:00 .. 12:59 --> PM 12:00 .. 12:59
 * 			13:00 .. 23:59 --> PM 01:00 .. 11:59
 *
 * @endverbatim
 */
uint16_t convert_hour_to_12H_format(uint16_t hour)
{
	if (hour > 12)				/* 13:00 .. 23:59 --> PM 01:00 .. 11:59 */
	{
		hour -= 12;
	}

	return hour;
}

/**
 * @brief	Display hours, if 12H format then toggle AM/PM icon accordingly.
 * @param	segments	Segments where to display hour data
 * 			value		Hour data
 * 			temp1		don't care, locally fixed to "2"
 * 			temp2		don't care, locally fixed to "0"
*/
void display_hours(uint16_t segments, uint32_t value, uint16_t temp1, uint16_t temp2)
{
	display_value(segments, value, 2, 0);			/* Display hours */

	if (0 == (sys.flags & SYS_UNITS))				/* if 12H format */
	{
		display_am_pm_symbol((uint16_t) value);		/* Toggle the AM/PM symbol */
	}
}

/**
 * @brief	Display time format '12H' or '24H' with blinking.
 * @param	segments	Target segments where to display information
 * 			index		0 ('24H') or 1 ('12H'), index for value string
 * 			tmp1		Not used
 * 			tmp2		Not used
 */
void display_Timeformat(uint16_t segments, uint32_t index, uint16_t tmp1, uint16_t tmp2)
{
	display_chars(segments, (uint8_t *) selection_Timeformat[index&1], SEG_ON_BLINK_ON);
}

/**
 * @brief	Clock set routine.
 * @param	temp, don't care
 */
void mx_time(uint16_t temp)
{
	uint8_t select;
	int16_t units, hours, minutes, seconds;

	units = (sys.flags & SYS_UNITS) ? TIME_METRIC_24H : TIME_USA_12H;				/* grab time format in local variable */
	hours = sTime.hour;
	minutes = sTime.minute;
	seconds = 0;
	select = 0;																		/* init value index */

	while (!(sys.flags & SYS_IDLE_TIMEOUT))											/* Loop until all values are set or user breaks */
	{
		if (button.flags & BUTTON_STAR_PIN)											/* Button STAR (short): save & exit */
		{
			Timer0_Stop();															/* stop clock timer */
			sTime.hour = hours;														/* store hours in global clock time */
			sTime.minute = minutes;													/* store minutes in global clock time */
			sTime.second = seconds;													/* store seconds in global clock time */
			sys.flag.use_metric_units = units;										/* store metric system in global variable */
			Timer0_Start();															/* restart clock timer */
			display_symbol(LCD_AM, SEG_OFF);										/* turn AM/PM icon off */
			break;
		}

		switch (select)
		{
			case 0:																	/******* set 24H/12H time format *******/
				clear_display_all();												/* clear screen required after set_value(seconds) */
				set_value( &units, 1, 0, 0, 1, SETVAL_ROLLOVER + SETVAL_NEXT,
						LCD_SEG_L1_3_1, display_Timeformat);
				select++;
				break;

			case 1:																	/************** set HOURS **************/
				temp = (hours * 100) + minutes;
				display_chars(LCD_SEG_L1_3_0, int_to_array(temp, 4, 0), SEG_ON);	/* print HHMM on LINE1 */
				display_symbol(LCD_SEG_L1_COL, SEG_ON);								/* print ':' on LINE1 */
				set_value(&hours, 2, 0, 0, 23, SETVAL_ROLLOVER + SETVAL_NEXT, 		/* get new hours */
						LCD_SEG_L1_3_2, display_hours);
				select++;
				break;

			case 2:																	/* set minutes (MM) using LINE1 */
				set_value(&minutes, 2, 0, 0, 59, SETVAL_ROLLOVER + SETVAL_NEXT,
						LCD_SEG_L1_1_0, display_value);
				select++;
				break;

			case 3:																	/* set seconds (SS) using LINE2 */
				display_chars(LCD_SEG_L2_1_0, int_to_array(seconds, 2, 0), SEG_ON);	/* print SS on LINE2 */
				set_value(&seconds, 2, 0, 0, 59, SETVAL_ROLLOVER + SETVAL_NEXT,
						LCD_SEG_L2_1_0, display_value);
				select = 0;
				break;
		}
	}
}

/**
 * @brief	Toggles view style between SHOW_1ST_PART (HH:MM) and SHOW_2ND_PART (SS).
 * @param	tmp, don't care.
 */
void sx_time(uint16_t tmp)
{
	sTime.display ^= SHOW_2ND_PART;
}

/**
 * @brief	Clock display routine. Supports metric (12H) and non-metric (24H) format.
 * @param	update	DISPLAY_LINE_UPD_FULL, DISPLAY_LINE_UPD_PARTIAL
 * 			temp	don't care (line fixed to LINE1)
 */
void display_time(uint16_t temp, uint16_t update)
{
	if (update == DISPLAY_LINE_UPD_FULL)									/* Full update */
	{
		if (sTime.display == SHOW_1ST_PART)									/* display hours and minutes */
		{
			temp = sTime.hour;												/* grab hour (24H format) */

			if (!(sys.flags & SYS_UNITS))									/* if English system, show 12H + AM/PM */
			{
				display_am_pm_symbol(temp);									/* lid AM/PM symbol on */
				temp = convert_hour_to_12H_format(temp);
			}

			temp = (temp * 100) + sTime.minute;								/* create virtual integer HHMM */
			display_symbol(LCD_SEG_L1_COL, SEG_ON_BLINK_ON);				/* blink ':' */
			display_chars(LCD_SEG_L1_3_0, int_to_array(temp, 4, 0), SEG_ON);	/* print time to LINE1 */
		}
		else																/* display seconds */
		{
			display_symbol(LCD_SEG_L1_DP1, SEG_ON);							/* print '.' */
			display_time(temp, DISPLAY_LINE_UPD_PARTIAL);					/* recursively print the seconds */
		}
	}
	else if (update == DISPLAY_LINE_UPD_PARTIAL)							/* Partial update only */
	{
		uint16_t seg = LCD_SEG_L1_1_0;										/* assume only seconds or minutes need update */

		if (sTime.display == SHOW_1ST_PART)									/* if displaying hours and minutes */
		{
			switch(sTime.drawFlag)
			{
				case 2:														/* update MM only */
					temp = sTime.minute;
					break;

				case 3:														/* recursively update HHMM */
					display_time(temp, DISPLAY_LINE_UPD_FULL);

				default:													/* seconds are not updated */
					return;
			}
		}
		else																/* SHOW_2ND_PART (seconds)  */
		{
			temp = sTime.second;											/* seconds are always updated */
		}

		display_chars(seg, int_to_array(temp, 2, 0), SEG_ON);
	}
	else																	/* for anything else asume DISPLAY_ICONS_OFF */
	{
		display_symbol(LCD_SEG_L1_COL, SEG_OFF_BLINK_OFF);					/* stop blinking ':' */
		display_symbol(LCD_AM, SEG_OFF);									/* clear AM/PM icon */
		sTime.display = SHOW_1ST_PART;										/* reset time shown to hours and minutes */

	}
}



