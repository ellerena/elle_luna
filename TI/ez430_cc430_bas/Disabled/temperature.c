/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/***************************************** Include section ****************************************/
/* system */
#include <stdint.h>
#include <msp430.h>

/* driver */
#include "display.h"
#include "temperature.h"
#include "ports.h"
#include "adc12.h"
#include "timer.h"

/* logic */
#include "user.h"

/*************************************** Prototypes section ***************************************/
int16_t convert_C_to_F(int16_t);
int16_t convert_F_to_C(int16_t);

/***************************************** Defines section ****************************************/

/************************************ Global Variable section *************************************/
struct temp sTemp;

/**************************************** Extern section ******************************************/

/****************************************** Main section ******************************************/

/**
 * @brief	Reset temperature measurement module.
 */
void reset_temp_measurement(void)
{
	sTemp.state = MENU_ITEM_NOT_VISIBLE;			/* Set flag to off */
	temperature_measurement(INSTANT);				/* measure and update temperature */
}

/**
 * @brief	Init ADC12. Do single conversion of temperature sensor voltage. Turn off ADC12.
 * @param	filter, can be STEPPED (change value in 0.1 steps) or INSTANT
 *
 * @verbatim
 * 				Convert ADC value to "xx.x �C"
 * 				Temperature in Celsius
 * 				((A10*1500mV/4096) - 680mV)*(1/2.25mV) = (A10/4096*667) - 302
 * 				= (A10 - 1855) * (667 / 4096)
 * @endverbatim
 */
void temperature_measurement(uint16_t filter)
{
	uint16_t tmp16;
	int16_t tmp16_;
	volatile int32_t temperature;

	tmp16 = adc12_single_conversion(REFVSEL_0, ADC12SHT0_8,
										ADC12INCH_10);				/* read temperature diode voltage */
	temperature = (((int32_t) ((int32_t) tmp16 - 1855)) *
											6670) / 4096;			/* calculate temperature */
	temperature += sTemp.offset;									/* add temperature offset */
	tmp16_ = (int16_t) temperature;									/* keep temperature in 16 bits signed format */

	if (filter)														/* Change in 0.1� steps towards measured value */
	{
		if (tmp16_ > sTemp.degrees) tmp16_ = sTemp.degrees + 1;
		else if (tmp16_ < sTemp.degrees) tmp16_ = sTemp.degrees - 1;
	}

	sTemp.degrees = tmp16_;											/* Store measured temperature */
	display.flags |= DISP_UPD_TEMPERATURE;						/* New data is available, run display update */
}

/**
 * @brief	Convert �C to �F where TFahrenheit = (( TCelsius � 9 ) / 5 ) + 32
 * @param	value	Temperature in �C
 * @return	Temperature in �F
 */
int16_t convert_C_to_F(int16_t value)
{
	return ((value * 9 * 10) / 5 / 10) + 32 * 10;
}

/**
 * @brief	Convert �F to �C where TCelsius =( TFahrenheit - 32 ) � 5 / 9
 * @param	value	Temperature in 2.1 �F
 * @return	Temperature in 2.1 �C
 */
int16_t convert_F_to_C(int16_t value)
{
	return (((value - 320) * 5)) / 9;
}


/**
 * @brief	Mx button handler to set the temperature offset.
 * @param	temp, don't care. Line is fixed to LINE1
 */
void mx_temperature(uint16_t temp)
{
	int16_t tmp, tmp0;

	clear_display_all();										/* fully clear display */
	display_symbol(LCD_SEG_L1_DP1, SEG_ON);						/* print '.' */
	display_symbol(LCD_L1_DEGREE, SEG_ON);						/* print degrees symbol */

	tmp0 = sTemp.degrees;										/* get temperature in C into local register */
	temp = (sys.flags & SYS_UNITS);								/* record the units system currently in use */
	display_char(LCD_SEG_L1_0, temp ? '@' : 'F', SEG_ON);		/* lid proper degrees unit symbol ('c' or 'F') */
	tmp = temp ? tmp0 : convert_C_to_F(tmp0);					/* get temperature in proper units (for display use) */

	while (! (sys.flags & SYS_IDLE_TIMEOUT))					/* Loop until new value is set or timeout happens */
	{
		if (button.flags & BUTTON_STAR_PIN)						/* Button STAR (short): save, then exit */
		{
			if (!temp)
			{
				tmp = convert_F_to_C(tmp);
			}
			sTemp.offset += (tmp - tmp0);						/* new offset is delta between old and new value */
			sTemp.degrees = tmp;								/* Force filter to new value */
			display.flags |= DISP_UPD_LINE1;					/* Set display update flag */
			break;
		}

		set_value(&tmp, 3, 1, -460, 900, SETVAL_SIGNED,
				LCD_SEG_L1_3_1, display_value);					/* get temperature from user */
	}
}

/**
 * @brief		Common display routine for metric and English units.
 * @param		line	LINE1
 * update		DISPLAY_LINE_UPD_FULL, DISPLAY_ICONS_OFF
 */
void display_temperature(uint16_t tmp, uint16_t update)
{
	uint8_t *str;

	if (update == DISPLAY_LINE_UPD_FULL)						/****** PRINT WHOLE TEMPERATURE READING ON LINE1 ******/
	{
		sTemp.state = MENU_ITEM_VISIBLE;						/* Menu item is visible */

		display_symbol(LCD_SEG_L1_DP1, SEG_ON);					/* print '.' */
		display_symbol(LCD_L1_DEGREE, SEG_ON);					/* print degree symbol */
		display_char(LCD_SEG_L1_0, (sys.flags & SYS_UNITS) ?
				'@' : 'F', SEG_ON);								/* print selected c/F symbol */

		temperature_measurement(INSTANT);						/* do an temperature measurement with instant adjust */
		display_temperature(LINE1, DISPLAY_LINE_UPD_PARTIAL);	/* recursively display temperature */
	}
	else if (update == DISPLAY_LINE_UPD_PARTIAL)				/****** UPDATE ONLY TEMPERATURE VALUE & SIGN ******/
	{
		tmp = sTemp.degrees;
		if (!(sys.flags & SYS_UNITS))
		{
			tmp = convert_C_to_F(tmp);							/* if units system is english convert C to F */
		}

		if (tmp & BITF)											/* indicate temperature sign through arrow up/down icon */
		{
			tmp = -((int16_t)tmp);								/* get temperature's absolute value */
			update = SEG_ON;									/* flag arrow down on to indicate negative sign */
		}
		else													/* Temperature is >= 0 */
		{
			update = SEG_OFF;									/* flag arrod down off to indicate a positive number */
		}

		if (tmp > 999)											/* limit min/max temperature to +/- 99.9 �C / �F */
		{
			tmp = 999;
		}
		str = int_to_array(tmp, 3, 1);							/* convert number into text in xx.x format */
		display_chars(LCD_SEG_L1_3_1, str, SEG_ON);				/* print temperature */
		display_symbol(LCD_ARROW_DOWN, update);					/* turn on/off arrow down symbol to indicate sign */
	}
	else														/* if DISPLAY_ICONS_OFF, turn off all icons */
	{
		sTemp.state = MENU_ITEM_NOT_VISIBLE;					/* flag menu item is not visible */
		display_symbol(LCD_ARROW_DOWN, SEG_OFF);
		display_symbol(LCD_L1_DEGREE, SEG_OFF);
	}
}



