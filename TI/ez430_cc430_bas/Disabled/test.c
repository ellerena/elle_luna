/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/***************************************** Include section ****************************************/
/* system */
#include <stdint.h>
#include <msp430.h>
#include "user.h"

/* driver */
#include "display.h"

/* logic */
#include "test.h"

/************************************ Global Variable section *************************************/

/*************************************** Prototypes section ***************************************/

/**
 * @brief	Toggles view style between SHOW_1ST_PART and SHOW_2ND_PART.
 * @param	tmp, dont care
 */
void sx_tst_dbg(uint16_t tmp)
{
	tmp ^= SHOW_2ND_PART;
}

/**
 * @brief	Display routine.
 * @param	tmp		don't care
 * 			update	DISPLAY_LINE_UPD_FULL, DISPLAY_ICONS_OFF
*/
void display_tst_dbg(uint16_t line, uint16_t update)
{
	const uint8_t str[] = APP_VER;								/* version number of the application */

	if (update != DISPLAY_ICONS_OFF)							/* DISPLAY_LINE_UPD_FULL or DISPLAY_LINE_UPD_PARTIAL */
	{
		display_chars(LCD_SEG_L2_5_0, (uint8_t*)str, SEG_ON);
	}
}





