/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

/***************************************** Include section ****************************************/
/* system */
#include <stdint.h>
#include <msp430.h>

/* driver */
#include "display.h"
#include "ports.h"
#include "timer.h"

/* logic */
#include "date.h"
#include "user.h"

/*************************************** Prototypes section ***************************************/
void reset_date(void);
uint16_t numberOfDays(uint16_t, uint16_t);
void add_day(void);

/***************************************** Defines section ****************************************/

/************************************ Global Variable section *************************************/
struct date sDate;

/**************************************** Extern section ******************************************/
//extern void (*fptr_lcd_line1)(uint16_t line, uint16_t update); */
//extern void (*fptr_lcd_line2)(uint16_t line, uint16_t update); */

/****************************************** Main section ******************************************/

/**
 * @brief	Reset date to start value.
 */
void reset_date(void)
{
	sDate.display = SHOW_1ST_PART;			/* Show day and month on display */
	sDate.year = START_YEAR;
	sDate.month = START_MONTH;
	sDate.day = START_DAY;
}

/**
 * @brief	Return number of days for a given month
 * @param	month	month as char
 * 			year	year as int
 * 	@return	day count for given month
 *
 * 	@verbatim
 * 		For February:
 * 		1. A year that is divisible by 4 is a leap year. (Y % 4) == 0
 * 		2. Exception to rule 1: a year that is divisible by 100 is not a leap year. (Y % 100) != 0
 * 		3. Exception to rule 2: a year that is divisible by 400 is a leap year. (Y % 400) == 0
 * 	@endverbatim
 */
uint16_t numberOfDays(uint16_t month, uint16_t year)
{
	uint16_t result = 31;

	switch (month)
	{
		case 2:													/* february needs special calculation */
			if ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0)))
			{
				result = (29);
			}
			else
			{
				result = (28);
			}
			break;

		case 4:
		case 6:
		case 9:
		case 11:
			result = (30);
	}

	return result;
}

/**
 * @brief	Add one day to current date. Called when clock changes from 23:59 to 00:00
 */
void add_day(void)
{
	if (++sDate.day > numberOfDays(sDate.month, sDate.year))	/* add 1 day... if day overflows into next month */
	{															/* Add 1 month and reset to day to 1 */
		sDate.day = 1;

		if (++sDate.month > 12)									/* add 1 month... if month overflows into next year */
		{														/* Add 1 year and reset month to 1 */
			sDate.month = 1;
			sDate.year++;
		}
	}

	display.flags |= DISP_UPD_DATE;								/* rise flag to indicate date has changed */
}

/**
 * @brief	Date set routine.
 * @param	temp, don't care, fixed to LINE1
 *
 * @verbatim
 * 			LINE1: DD.MM (metric units) or MM.DD (English units)
 * 			LINE2: YYYY (will be drawn by set_value)
 * @endverbatim
 */
void mx_date(uint16_t temp)
{
	int16_t day, month, year, max_days = 0;
	uint8_t select;

	clear_display_all();												/* Clear display */

	day = sDate.day;
	month = sDate.month;
	year = sDate.year;

	temp = (sys.flags & SYS_UNITS) ?
			(day * 100) + month : (month * 100) + day;					/* select DDMM or MMDD format */

	display_chars(LCD_SEG_L1_3_0, int_to_array(temp, 4, 0), SEG_ON);	/* print month and day */
	display_symbol(LCD_SEG_L1_DP1, SEG_ON);								/* print '.' */
	select = 1;															/* Init value index */

	while (!(sys.flags & SYS_IDLE_TIMEOUT))								/* Loop until timeout */
	{
		if (button.flags & BUTTON_STAR_PIN)								/* Button STAR (short): save, then exit */
		{																/* Copy local variables to global variables */
			sDate.day = day;
			sDate.month = month;
			sDate.year = year;
			break;														/* Full display update is done after function returns */
		}

		temp = SETVAL_ROLLOVER + SETVAL_NEXT;			 /* re-use variable <line> */
		switch (select)
		{
			case 1:														/* Set year */
				set_value(&year, 4, 0, 2015, 2100, temp, LCD_SEG_L2_3_0, display_value);
				break;
			case 2:														/* Set month */
				set_value(&month, 2, 0, 1, 12, temp,
					(sys.flags & SYS_UNITS) ? LCD_SEG_L1_1_0 : LCD_SEG_L1_3_2, display_value);
				break;
			default:													/* Set day */
				set_value(&day, 2, 0, 1, max_days, temp,
					(sys.flags & SYS_UNITS) ? LCD_SEG_L1_3_2 : LCD_SEG_L1_1_0, display_value);
				select = 0;
		}

		select++;
		max_days = numberOfDays(month, year);							/* Check if day is still valid */
		if (day > max_days) day = max_days;								/* if not clamp to last day of current month */
	}
}

/**
 * @brief	Date user routine. Toggles view between DD.MM and YYYY.
 * @param	tmp,	not used
 */
void sx_date(uint16_t tmp)
{
	sDate.display ^= SHOW_2ND_PART;
}

/**
 * @brief	Display date in DD.MM format (metric units) or MM.DD (English units).
 * @param	line	LINE1, LINE2
 * 			update	DISPLAY_LINE_UPD_FULL, DISPLAY_LINE_UPD_PARTIAL
 */
void display_date(uint16_t line, uint16_t update)
{
	if (update == DISPLAY_ICONS_OFF)
	{
		sDate.display = SHOW_1ST_PART;													/* Show day and month on display when coming around next time */
	}
	else																				/* DISPLAY_LINE_UPD_PARTIAL or DISPLAY_LINE_UPD_FULL */
	{
		if (sDate.display == SHOW_1ST_PART)												/* print month and day */
		{
			update = sDate.day;															/* Re-use of variable <update> */

			if (sys.flags & SYS_UNITS)
			{
				update = (update * 100) + sDate.month;									/* calculate integer DDMM */
			}
			else
			{
				update = update + (100 * sDate.month);									/* calculate integer MMDD */
			}

			display_symbol(switch_seg(line, LCD_SEG_L1_DP1, LCD_SEG_L2_DP), SEG_ON);	/* print "." */
		}
		else																			/* print year */
		{
			update = sDate.year;
			display_symbol(switch_seg(line, LCD_SEG_L1_DP1, LCD_SEG_L2_DP), SEG_OFF);	/* clear "." */
		}

		display_chars(switch_seg(line, LCD_SEG_L1_3_0, LCD_SEG_L2_3_0),					/* print date as string */
						int_to_array(update, 4, 0), SEG_ON);
	}
}




