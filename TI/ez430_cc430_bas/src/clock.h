/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

#ifndef CLOCK_H_
#define CLOCK_H_

/***************************************** Include section ****************************************/


/***************************************** Defines section ****************************************/
/*
 * We'll build the 'default' time and date at compilation time using Keil's internal macros
 * */
#define BUILD_HOUR_CH0 (__TIME__[0])
#define BUILD_HOUR_CH1 (__TIME__[1])

#define BUILD_MIN_CH0 (__TIME__[3])
#define BUILD_MIN_CH1 (__TIME__[4])

#define BUILD_SEC_CH0 (__TIME__[6])
#define BUILD_SEC_CH1 (__TIME__[7])

#define	START_HOUR				(10 * BUILD_HOUR_CH0 + BUILD_HOUR_CH1 - 528)
#define	START_MINUTE			(10 * BUILD_MIN_CH0 + BUILD_MIN_CH1 - 528)
#define	START_SECOND			(10 * BUILD_SEC_CH0 + BUILD_SEC_CH1 - 528)

/* Definitions for time format */
#define TIME_METRIC_24H			(1u)		/* Metric unit 24h format */
#define TIME_USA_12H			(0u)		/* English unit 12h format */

/*************************************** Prototypes section ***************************************/
extern void reset_clock(void);
extern void sx_time(uint16_t);
extern void mx_time(uint16_t);
extern void add_second(void);
extern void display_hours(uint16_t, uint32_t, uint16_t, uint16_t);
extern void display_Timeformat(uint16_t, uint32_t, uint16_t, uint16_t);
extern void display_time(uint16_t, uint16_t);

/* English units support */
/*extern void calc_24H_to_12H(uint8_t *, uint8_t *);*/
extern uint16_t convert_hour_to_12H_format(uint16_t);

/************************************ Global Variable section *************************************/

/**************************************** Extern section ******************************************/

#endif                          /*CLOCK_H_ */
