/***********************************************************************************

    Filename:	  mrfi_link.h

	Description:  MRFI link header file.

***********************************************************************************/

#ifndef MRFI_LINK_H
#define MRFI_LINK_H

#include "mrfi.h"

/*********************** CONSTANTS AND DEFINES ***********************/
#define DEV_ADDR_LOC                0x25DE
#define DEV_ADDR_REM                DEV_ADDR_LOC    // 0x25EB
#define MRFI_CHANNEL                0

#define  F_MSG_ID                (4)
#define  F_MSG_COM               (2)      /* message conveys executable commands */
#define  F_MSG_TXT               (1)      /* message conveys a text string */
#define  F_MSG_BIN               (0)      /* message conveys binary data values */

#define	MRFI_TX_RESULT_ACK_TIMEOUT		(3)

extern mrfiPacket_t * CODE pPktIn;
extern mrfiPacket_t * CODE pPktOut;

/************************ PUBLIC FUNCTIONS ************************/
void  mrfiLinkInit(void);
//uint8_t mrfiLinkSend(uint8_t nRetrans);
uint8_t mrfiLinkRecv(void);
uint8_t mrfiLinkDataRdy(void);
void mrfiLinkBroadcast(uint16_t len);

#endif
