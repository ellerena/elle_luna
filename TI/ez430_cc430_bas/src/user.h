/*************************************************************************************************

		Copyright (C) 2009 Texas Instruments Incorporated - http://www.ti.com/


		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:

		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.

		Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions and the following disclaimer in the
		documentation and/or other materials provided with the
		distribution.

		Neither the name of Texas Instruments Incorporated nor the names of
		its contributors may be used to endorse or promote products derived
		from this software without specific prior written permission.

		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*************************************************************************************************/

#ifndef USER_H_
#define USER_H_

#define	VMAJ		"01"
#define	VFEA		"02"
#define	VREL		"00"

#define	APP_VER		{VMAJ VFEA VREL}


/***************************************** Include section ****************************************/

/*************************************** Prototypes section ***************************************/

/***************************************** Defines section ****************************************/
#ifndef BIT
	#define	BIT(x)									(1 << x)
#endif

#define SETVAL_ROLLOVER								BIT(0)		/* reaching a limit rolls over the other limit */
#define SETVAL_SHOW									BIT(1)		/* undefined and unused for now */
#define SETVAL_SIGNED								BIT(2)		/* lid up/down arrows for +/- value sign */
#define SETVAL_SELECTION							BIT(3)		/* undefined and unused for now */
#define SETVAL_FAST									BIT(4)		/* long press up/down increases stepping times 10 */
#define SETVAL_NEXT									BIT(5)		/* allow setvalue to exit via an alternative key (NUM) */

#define	REQ_MEASURE_TEMP							BIT(0)
#define	REQ_MEASURE_VOLT							BIT(1)
#define	REQ_MEASURE_ALT								BIT(6)		/* must correspond with the pin position of ALT irq */
#define	REQ_MEASURE_ACC								BIT(5)		/* must correspond with the pin position of ACC irq */
#define	REQ_BUZZ									BIT(4)

#define	SYS_IDLE_TIMEOUT							BIT(0)		/* Timeout after inactivity */
#define	SYS_IDLE_TIMEOUT_ENA						BIT(1)		/* When in set mode, timeout after a given period */
#define	SYS_LOCK_BUTTONS							BIT(2)		/* Lock buttons */
#define	SYS_MASK_BUZZ								BIT(3)		/* Do not output buzz for next button event */
#define	SYS_U_D_REPEAT_ENA							BIT(4)		/* While in set_value(), create virtual UP/DOWN button events */
#define	SYS_DELAY_OVER								BIT(5)		/* Timer delay exhausted */
#define	SYS_LOW_BATT								BIT(6)		/********** UNUSED ***********/
#define	SYS_UNITS									BIT(6)		/********** UNUSED ***********/

#define	SET_VAL_FUNCTION(x)							void (*x)(uint16_t, uint32_t, uint16_t, uint16_t)

/************************************ Global Variable section *************************************/
typedef union
{
	struct
	{
		uint16_t idle_timeout				:1;	/* Timeout after inactivity */
		uint16_t idle_timeout_enabled		:1;	/* When in set mode, timeout after a given period */
		uint16_t lock_buttons				:1;	/* Lock buttons */
		uint16_t mask_buzzer				:1;	/* Do not output buzz for next button event */
		uint16_t up_down_repeat_enabled		:1;	/* While in set_value(), create virtual UP/DOWN button events */
		uint16_t delay_over					:1;	/* 1 = Timer delay over */
/*		uint16_t low_battery				:1;	 1 = Battery is low */
		uint16_t use_metric_units			:1;	/* 0: USA (12h, F, ft) , 1: METRIC SYSTEM (24h, C, m) */
	} flag;
	uint16_t flags;								/* Shortcut to all display flags (for reset) */
} s_system_flags;

extern volatile s_system_flags sys;


typedef union
{
	struct
	{
		uint16_t temperature_measurement	:1;	/* 1 = Measure temperature */
		uint16_t voltage_measurement		:1;	/* 1 = Measure voltage */
		uint16_t altitude_measurement		:1;	/* 1 = Measure air pressure */
		uint16_t acceleration_measurement	:1;	/* 1 = Measure acceleration */
		uint16_t buzzer						:1;	/* 1 = Output buzzer */
	} flag;
	uint16_t flags;								/* Shortcut to all display flags (for reset) */
} s_request_flags;

extern volatile s_request_flags request;

/*************************************** Prototypes section ***************************************/
extern void set_value(int16_t *, uint16_t, uint16_t, int16_t, int16_t, uint16_t, uint16_t,
					void (*fptr_SETVAL_display_function1)(uint16_t, uint32_t, uint16_t, uint16_t));

/**************************************** Extern section ******************************************/


#endif						/*USER_H_ */
