#include "main.h"

/**
 * Static Functions
 */
static uint8_t sCB(linkID_t);								/* Simpliciti Callback fn */
static void fSystemReset(void);								/* System Reset fn */

/**
 * Module scope variables
 */
char cBuffer[100] = HSTVER;									/* gen purpose string buffer, label [45+]*/

uint8_t	msgLenOut,											/* controls length of Simpliciti outgoing msgs*/\
		flag = 0,											/* multi use flag */\
		msgRcvdCount = 0;									/* controls number of Simpliciti recvd msgs */

linkID_t pLinkId = 0;										/* Simpliciti generated link handler */

int main (void)
{
	const char *WIFI_buffer= (cBuffer + 50);
	uint8_t msgLenIn;
															/************* System configuration *************/
	WDTCTL = WDTPW + WDTHOLD;								/* Stop Watch Dog timer */
	BSP_Init();												/* run BSP init (8MHz, 2 leds, 1 button) */
	MACRO_GPIO_INIT											/* configure GPIO port pins */
	MACRO_COM_INIT											/* configure UART @ 9600-8N1 */

	IE2 = UCA0RXIE;											/* Enable COM RX interrupt */

	COM_print(cBuffer, sizeof(HSTVER));						/************** print welcome msg **************/

	SMPL_Init(sCB);											/* Init Simplicti stack. Register sCB callback */

	TURN_OFF_LED(LED_GREEN | LED_RED);						/************** leds off to start **************/

	do														/**************** Link or die!! ****************/
	{
		TOGGLE_LED(LED_RED);								/* toggle red led on each attempt to link */
	} while  (SMPL_SUCCESS != SMPL_LinkListen(&pLinkId));

	COM_print("\r#", 2);									/* Tell user we're ready for input */
	TURN_OFF_LED(LED_RED);
	SMPL_Ioctl( IOCTL_OBJ_RADIO, IOCTL_ACT_RADIO_RXON, 0);	/* Place radio in receive state */

	while (1)												/**************** forever loop ****************/
	{
		if(msgRcvdCount)									/*** Rcvd msg from peer ready for pick up ***/
		{
			TOGGLE_LED(LED_GREEN);
			while (SMPL_SUCCESS != SMPL_Receive(pLinkId, (uint8_t*)WIFI_buffer, &msgLenIn));
			switch (*WIFI_buffer)
			{
				case M_RES:									/* RESET requested */
					fSystemReset();							/* full system reset */
					break;

				case M_WRD:
					COM_TXHex(*(uint16_t*)(WIFI_buffer + 2));
					COM_TXHex(*(uint16_t*)(WIFI_buffer + 4));
					COM_TXHex(*(uint16_t*)(WIFI_buffer + 6));
					COM_TXHex(*(uint16_t*)(WIFI_buffer + 8));
					COM_print("\r#", 2);
					break;

				default:
					COM_print("\b>", 2);					/* backspace and print '>' */
					COM_print(WIFI_buffer, msgLenIn);		/* present the incoming msg */
					COM_print("#", 1);						/* Tell user we're ready for input */
					break;
			}
			msgRcvdCount--;									/* msg processed, reduce counter */
		}
		if(flag)
		{
			if (flag & FLAG_UART_COMMAND_RCVD)				/*** COM text msg ready for delivery ***/
				while ( SMPL_SUCCESS != SMPL_SendOpt(pLinkId, (uint8_t*)cBuffer, msgLenOut, SMPL_TXOPTION_NONE));

			COM_print("#", 1);								/* Tell user we're ready for input */
			msgLenOut = 0;									/* buffer is empty, take note */
			flag = 0;										/* msg sent, take note */
			TOGGLE_LED(LED_RED);
		}
	}

	SMPL_Unlink(pLinkId);									/* if ever reach here... unlink */

}



/**
 * @brief	Callback function for Simpliciti
 */
static uint8_t sCB(linkID_t lId)
{
	if (lId)												/* if a valid call has been made... */
	{
		msgRcvdCount++;										/* just take note that a new msg exists */
		return 0;
	}

	return 1;												/* if ever reach here by mistake */
}


/**
 * @brief	this functions attempts to properly close the connection and reset all.
 */
static void fSystemReset(void)
{
	if (pLinkId)											/* if a link is alive */
	{
		SMPL_SendOpt(pLinkId, (uint8_t*)"~~", 2u, SMPL_TXOPTION_NONE);
		SMPL_Unlink(pLinkId);								/* request to destroy the Simpliciti link */
		pLinkId = 0;										/* release out link handler */
	}

	WDTCTL = 0;												/* SW triggered reset */
}

/**
 *	@brief	Interrupt handler for data received on via UART
 *	@verbatim
 *		Receives byte on the RXD pin of the victual com port. Each
 *		byte received is added to our global buffer.
 *		When the byte received is a return ('\r') a flag is set to the
 *		main program to request processing of the received command.
 */
BSP_ISR_FUNCTION(UART_SPI_RX_handler, USCIAB0RX_VECTOR)
{
	char a;
	static char *buffer = cBuffer;

	a = UCA0RXBUF;											/* read data form UART buffer */
	*buffer++ = a;											/* append rcvd char to command string in ram */

	if (a == '\r')											/* <return> ends the uart command entry */
	{
		buffer = cBuffer;									/* return pointer to begining of buffer */
		flag = FLAG_UART_COMMAND_RCVD;						/* rise a flag to tell main() to ship out the msg */
	}

	msgLenOut++;											/* msg length has incremented */
}

/**
 *	@brief	Interrupt handler for GPIO User buttom - request a system reset
 */
BSP_ISR_FUNCTION(i_user_button, PORT1_VECTOR)
{
	P1IFG &= ~BIT2;											/* Clear pending interrupts - pin 2 */
	fSystemReset();											/* force PUC */
}
