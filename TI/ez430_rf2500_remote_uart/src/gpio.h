/*
 * gpio.h
 *
 *  Created on: Oct 5, 2014
 *      Author: Edison
 */

#ifndef GPIO_H_
#define GPIO_H_

#include "msp430.h"
#include "stdint.h"

#define MACRO_GPIO_INIT 				/* (P1_2 already in BSP_Init */\
	P1REN	= BIT2;						/* P1_2 resistor enable */\
	P1OUT	= BIT0 | BIT1 | BIT2;		/* P1_0/1 OUTPUT, P1_2 PU */\
	P1IFG	= 0;						/* Clear pending interrupts */\
	P1IE	= BIT2;						/* P1_2 Interrupt Enabled */\
	P1IES	= BIT2;						/* P1_2 Interrupt on H-to-L transition */

#define MACRO_TIMER_INIT				/* (already in BSP_Init) */\
	TACTL |= TACLR;						/* Set the TACLR */\
	TACTL = 0x0;						/* Clear all settings */\
	TACTL |= TASSEL_2;					/* Select the clk source to be - SMCLK (Sub-Main CLK) */

#define MACRO_ADC10_INIT	\
	ADC10CTL1 = INCH_15 + CONSEQ_1; 						/* A15..A12, single sequence */\
	ADC10CTL0 = SREF_1 + REFON + REF2_5V  					/* 2.5Vref */\
				+ ADC10SHT_2 + MSC + ADC10ON + ADC10IE;		/* SH 16xCLK, IRQ enabled , ADC10 ON */\
	ADC10DTC1 = 0x04;										/* 4 conversions */\
	ADC10AE1 = 0xF0;										/* A12..15 - P4.3..6 select */

#define TURN_ON_LED(x)		P1DIR |= x
#define TURN_OFF_LED(x)		P1DIR &= ~x
#define TOGGLE_LED(x)		P1DIR ^= x


//void __attribute__((interrupt(PORT1_VECTOR))) i_user_button(void);
//__interrupt void i_user_button(void);

#endif /* GPIO_H_ */
