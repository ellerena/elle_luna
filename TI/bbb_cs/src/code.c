/* Bare metal LED-blink program for the BeagleBone Black. */

#include <stdint.h>

#include "hw_gpio_v2.h"
#include "soc_AM335x.h"
#include "hw_cm_per.h"
#include "hw_types.h"

#define	BIT_CLR(addr, pattern)		*(volatile uint32_t*)(addr) &= ~(pattern)
#define	BIT_SET(addr, pattern)		*(volatile uint32_t*)(addr) |= (pattern)

void __attribute__((weak)) __attribute__((optimize("O0"))) dummy_wait(uint32_t nr_of_nops);
static inline void init_led_output();
static inline void heartbeat_forever(void);

int main(void)
{
    init_led_output();
    heartbeat_forever();

    return 0;
}

void __attribute__((weak)) __attribute__((optimize("O0"))) dummy_wait(uint32_t nr_of_nops)
{
    // compiler optimizations disabled for this function using __attribute__,
    // pragma is as well possible to protect bad code...
    // http://stackoverflow.com/questions/2219829/how-to-prevent-gcc-optimizing-some-statements-in-c

    // i need this statement that the bad code below is not optimized away
    __asm("");

    uint32_t counter = 0;
    for (; counter < nr_of_nops; ++counter)
    {
        ;
    }
}

static inline void init_led_output()
{
	/* enable the GPIO1 module clock */
	HWREG(SOC_CM_PER_REGS + CM_PER_GPIO1_CLKCTRL) =
		CM_PER_GPIO1_CLKCTRL_MODULEMODE_ENABLE | CM_PER_GPIO1_CLKCTRL_OPTFCLKEN_GPIO_1_GDBCLK;
//	HWREG(SOC_CONTROL_REGS + CONTROL_CONF_GPMC_A(7)) = CONTROL_CONF_MUXMODE(7);
//	HWREG(SOC_GPIO_1_REGS + GPIO_IRQSTATUS_CLR(0)) = LED_PATTERN;

	BIT_SET(SOC_GPIO_1_REGS + GPIO_IRQSTATUS_CLR(0), 0xf << 21);	/* disable IRQs for GPIO1 21..24 */
	BIT_CLR(SOC_GPIO_1_REGS + GPIO_OE, 0xf << 21);				/* OUTPUT mode for GPIO1 21..24 */
}

static inline void heartbeat_forever(void)
{
	for (;;)
	{
		BIT_SET(SOC_GPIO_1_REGS + GPIO_DATAOUT, 0x1 << 21);					/* led pattern on */
		dummy_wait(0x007ABE5);										/* blocking wait */
		BIT_CLR(SOC_GPIO_1_REGS + GPIO_DATAOUT, 0xf << 21);					/* led pattern off */
		dummy_wait(0x005BBE5);										/* blocking wait */
		BIT_SET(SOC_GPIO_1_REGS + GPIO_DATAOUT, 0xe << 21);					/* led pattern on */
		dummy_wait(0x007ABE5);										/* blocking wait */
		BIT_CLR(SOC_GPIO_1_REGS + GPIO_DATAOUT, 0xf << 21);					/* led pattern off */
		dummy_wait(0x005BBE5);										/* blocking wait */
	}
}




