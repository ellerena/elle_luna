#include <uart.h>

/**
 *	@brief	This function prints a character string to the UART.<br>
 *			It will stop after the first non-printable character -expected to be '\r'.
 *	@param	s is a pointer to the start of the string.
 * */
void COM_print(const char * s, uint8_t len)
{
	do
	{
		while (!(IFG2 & UCA0TXIFG));				/* USCI_A0 TX buffer empty? */
		UCA0TXBUF = *s++;							/* Send the character */
	} while (--len);								/* repeat <len> times */
}

void com_printf(const char * s)
{
	while (1)
	{
		while (!(IFG2 & UCA0TXIFG));				/* USCI_A0 TX buffer empty? */
		if (*s > 31)								/* transmit printable chars only */
		{
			UCA0TXBUF = *s++;						/* Send the character */
		}
		else										/* transmit new line and then stop */
		{
			COM_print(CRLF, sizeof(CRLF) - 1);
			break;
		}

	}
}

/**
 *	@brief	This function converts and prints an unsigned char using hex format display.
 *	@param	n is the unsigned char number (between o and 255)
 */
void COM_TXHex(uint16_t v)
{
	char msg[] = {"x0000 "}, c;
	unsigned i= 4;

	do
	{
		c = v & 0x0f;								/* extract less significant nibble for processing */
		msg[i] = c < 0x0a ? c+0x30 : c+0x57;		/* convert the nibble into a printable hex */
		v>>=4;										/* extract more significant nibble for processing */
	} while(--i);

	COM_print(msg, 6);								/* send the whole thing for printing */

}


