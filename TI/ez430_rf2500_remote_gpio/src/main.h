#ifndef _MAIN_HEADER
#define _MAIN_HEADER

#define		FLAG_UART_COMMAND_RCVD			0x01u
#define		FLAG_FULL_RESET_REQUEST			0x08u
#define		FLAG_ADC10_DONE					0x04u
#define		LED_RED							BIT0
#define		LED_GREEN						BIT1
#define		NULL							0

#include "CONST_AP.h"

#include <uart.h>
#include <drv.h>

#include "bsp.h"
#include "mrfi.h"

#endif
