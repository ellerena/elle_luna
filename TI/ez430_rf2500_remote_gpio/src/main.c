/**
 * Server: this is the remote side. It works in low power mode all the time in order
 * to save battery.
 * 		The server has no LED indications (except for PUC).
 * 		When running it is sleeping most of the time.
 * 		Every often (20msec) it wakes up, measures its own VCC and the status of P2 & P4
 * 		Then it transmits the data (binary form) and then goes bak to sleep again.
 *
 * Client: this is the host side. It works at full power since it is expected to have
 * 		its own main supply source.
 * 		After PUC, the client posts a message to the UART port.
 * 		Then it loops constantly waiting for a msg received on the MRFI link
 * 		when a msg is read, the 1st byte is used to indicated whether it is text or data
 * 		if text, it is just printed to console.
 * 		if binary data, it is interpreted and posted to the console.
 * 			If the binary data represents a trap triggered, the RED LED is lid.
 * 		It uses a timer to blink an LED (green).
 * 			Fast pulses (aprox. every 0.5sec) indicate NO LINK/REMOTE MISSING
 * 			Slow pulses (aprox. every 1 sec) indicate a LINK is present an active
 * Msgs:
 * 		PORT:x781e, BATT:3069mV, T:2dc		- no trap
 * 		PORT:x781f, BATT:3069mV, T:2dc		- trapped
 * */


#include "main.h"
#include "bsp_leds.h"
#include "mrfi_lnk.h"
#include "pp_utils.h"

#include <stdio.h>

#define		ACLK_TICKS_PER_SEC	(3210u)
#define		TIC_ONE_SEC			(ACLK_TICKS_PER_SEC)
#define		TIC_TEN_MSEC		(ACLK_TICKS_PER_SEC/100)

#ifndef CLT_TARGET
#define		TIM_LED_CYCLE		(20*TIC_ONE_SEC)
#define		TIM_LED_ON			(TIC_TEN_MSEC)
#else
#define		TIM_LED_CYCLE		(TIC_ONE_SEC/2)
#define		TIM_LED_ON			(TIC_TEN_MSEC)
#endif

#define		TIM_LED_CYCLE_LONG	(2*TIM_LED_CYCLE)
#define		MSG_RCVD_FAIL		25

/**
 * Static Functions
 */

static void fSystemReset(void);								/* System Reset fn */

/**
 * Module scope variables
 */
char cBuffer[sizeof(TARGET_VER)] = TARGET_VER;				/* gen purpose string buffer, label [45+]*/

uint8_t	msgLenOut,											/* controls length of Simpliciti outgoing msgs*/\
		flag = LED_GREEN,									/* multi use flag */\
		msgRcvdCount = 0;									/* controls number of Simpliciti recvd msgs */

/* Data format
 * <msg_type:8><gpio:16><batt:16><temp:16>
 */

int main (void)
{
#ifdef CLT_TARGET
	uint8_t msgLenIn;
	uint8_t *p = MRFI_P_PAYLOAD(pPktIn);
#else
	uint8_t *p = MRFI_P_PAYLOAD(pPktOut);
#endif
	BSP_DISABLE_INTERRUPTS();
															/************* System configuration *************/
	WDTCTL = WDTPW + WDTHOLD;								/* Stop Watch Dog timer */
	BSP_Init();												/* run BSP init (8MHz, 2 leds, 1 button) */
	MACRO_GPIO_INIT											/* configure GPIO port pins */
	MACRO_ADC10_INIT
#ifdef CLT_TARGET
	MACRO_COM_INIT											/* configure UART @ 9600-8N1 */
#endif

	BCSCTL1 |= (XT2OFF | DIVA_2);							/* ACLK divider */
	BCSCTL3 = LFXT1S_2;										/* ACLK from VLO */

	TACTL = 0;												// Stop timer
	TACCR0 = TIM_LED_CYCLE;									// define pulse period and start timer
#ifdef CLT_TARGET
	TACCR1 = TIM_LED_ON;									// define pulse width
	TACCTL1 = CCIE;											// CCR1 interrupt enabled
#endif
	TACTL = TASSEL_1 + MC_1 + ID_0 + TAIE;					// Use ACLK, up to CCR0, use IRQ

	mrfiLinkInit();											/* Init MRFI stack */
	com_printf(cBuffer);									/************** print welcome msg **************/

	TURN_OFF_LED(LED_GREEN | LED_RED);						/************** leds off to start **************/
	BSP_ENABLE_INTERRUPTS();

	while (1)												/**************** forever loop ****************/
	{
#ifdef CLT_TARGET											/* CLT_TARGET */
		if (mrfiLinkDataRdy())								/* if data has been received... */
		{
			msgLenIn = mrfiLinkRecv();						/* Receive RF data */
			if(msgLenIn)									/* send packet to UART */
			{
				TACCR0 = TIM_LED_CYCLE_LONG;
				msgRcvdCount = 0;
				if(p[0] == F_MSG_TXT)
				{
					com_printf((char*)p+1);
				}
				else			/* F_MSG_BIN */
				{
					uint16_t *pd, b, t;

					pd = (uint16_t*)(p+1);
					b = pd[1]*3;
					t = pd[2];
					t = (((t*3) - 1010)*282/1024);
					sprintf(cBuffer, "PORT:x%04x, BATT:%umV, T:%x", pd[0], b, t);
					com_printf(cBuffer);
					b = (pd[0] ^ IN_MASK_RF2500);
					if(!b) TURN_ON_LED(LED_RED);			/* Trapp triggered!!! */
					else TURN_OFF_LED(LED_RED);
				}
			}
		}
		else if (msgRcvdCount >= MSG_RCVD_FAIL)
		{
			TACCR0 = TIM_LED_CYCLE;
		}
#else														/* SVR_TARGET */
		ADC10CTL1 = INCH_11;								// ADC sample VREF
	    ADC10CTL0 |= ENC + ADC10SC;           			    // Sampling and conversion start
	    while (ADC10CTL1 & ADC10BUSY);       			    // ADC10BUSY?
	    ADC10CTL0 = ADC10CTL_ARM;
	    p[0] = F_MSG_BIN;
	    p[1] = (P2IN&P2RF2500);
	    p[2] = (P4IN&P4RF2500);
	    *(uint16_t*)&p[3] = ADC10MEM;
		ADC10CTL1 = INCH_10;								// ADC sample TEMP
	    ADC10CTL0 |= ENC + ADC10SC;           			    // Sampling and conversion start
	    while (ADC10CTL1 & ADC10BUSY);       			    // ADC10BUSY?
	    ADC10CTL0 = ADC10CTL_ARM;
	    *(uint16_t*)&p[5] = ADC10MEM;
	    MRFI_WakeUp();
	    mrfiLinkBroadcast(7);
	    MRFI_Sleep();
		_BIS_SR(LPM3_bits + GIE);							/* Enter LPM0 w/ interrupt */
#endif
	}

	return 0;
}

/**
 * @brief	this functions attempts to properly close the connection and reset all.
 */
static void fSystemReset(void)
{
	WDTCTL = 0;												/* SW triggered reset */
}

/**
 *	@brief	Interrupt handler for data received on via UART
 *	@verbatim
 *		Receives byte on the RXD pin of the victual com port. Each
 *		byte received is added to our global buffer.
 *		When the byte received is a return ('\r') a flag is set to the
 *		main program to request processing of the received command.
 */
BSP_ISR_FUNCTION(UART_SPI_RX_handler, USCIAB0RX_VECTOR)
{
	char a;
	static char *buffer = cBuffer;

	a = UCA0RXBUF;											/* read data form UART buffer */
	*buffer++ = a;											/* append rcvd char to command string in ram */

	if (a == '\r')											/* <return> ends the uart command entry */
	{
		buffer = cBuffer;									/* return pointer to begining of buffer */
		flag = FLAG_UART_COMMAND_RCVD;						/* rise a flag to tell main() to ship out the msg */
	}

	msgLenOut++;											/* msg length has incremented */
}

/**
 *	@brief	Interrupt handler for GPIO User buttom - request a system reset
 */
BSP_ISR_FUNCTION(i_user_button, PORT1_VECTOR)
{
	uint16_t t;

	BSP_DISABLE_INTERRUPTS();
	P1IFG &= ~BIT2;											/* Clear pending interrupts - pin 2 */
	for(t = 0; t < 500; t++)
	{
		if (!(P1IN & BIT2)) t = 0;
	}
	fSystemReset();											/* force PUC */
}

BSP_ISR_FUNCTION(mytimer, TIMERA1_VECTOR)
{
	switch (TAIV)
	{
		case TAIV_TAIFG:
#ifndef CLT_TARGET
			LPM3_EXIT;
#else
			if(msgRcvdCount < MSG_RCVD_FAIL) msgRcvdCount++;
			TURN_ON_LED(flag);
			break;
		case TAIV_TACCR1:
			TURN_OFF_LED(flag);
#endif
			break;
		default:
			break;
	}
}
