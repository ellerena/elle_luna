#ifndef _e_CONST
#define _e_CONST

/**
 * @brief	VERSION NUMBER
 * @verbatim
 * 			1			RF Remote UART
 * 			2			RF Remote ADC
 */
#define	VPROJECT		"3"								// RF Remote UART
#define	VMINOR			"0"								// Minor release
#define	VFEATURE		"0"								// Feature Nuber
#define VBUGFIX			"2"								// Bug fix number
#define VALL			VPROJECT "." VMINOR "." VFEATURE "." VBUGFIX
#define	VPRODUCT		"MSP430 RF2500 "

#define BUILD_DATE __DATE__ " " __TIME__ CRLF			// 21

#define TARGET_VER		VPRODUCT RELTYPE VALL " " BUILD_DATE	// [44]

#define ACP_TARGET		a
#define END_TARGET		e
#define HST_TARGET		h

//#define CLT_TARGET

#ifdef CLT_TARGET
#define RELTYPE			"c"
#else
#define RELTYPE			"s"
#endif

#define M_020			' '								// [0x20] Space
#define M_COM			'!'								// [0x21] Command
#define M_WRD			'\"'							// [0x22] " 16bit number
#define M_024			'$'								// [0x24] Quote "
#define M_028			'('								// [0x28] (
#define M_ADC			'@'								// [0x40] ADC conversion
#define M_BYT			'\''							// [0x60] ' 8 bit number
#define M_RES			'~'								// [0x7f] Reset

#define ENTER (uint8_t*) "\r"							// 2
#define PROCC1 (uint8_t*) "."							// 1	// Process: Listening for a link request...
#define STATU1 (uint8_t*) "<>\r"						// 4	// Status: New Link Stablished (good)
#define NOCALLBACKUSED 0

#endif
