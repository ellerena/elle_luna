/******************************************************************************/
// Virtual Com Port Communication
/******************************************************************************/

#ifndef VIRTUAL_COM_CMDS
#define VIRTUAL_COM_CMDS

#include "msp430.h"
#include "stdint.h"

#define CRLF			"\r\n"

#define MACRO_COM_INIT	\
	UCA0CTL1 = UCSWRST;				/* Reset UCSIA */										\
	UCA0BR0 = 0x41;					/* 9600 from 8Mhz = 0x0341; 115200 = 0x0045 */			\
	UCA0BR1 = 0x03;																			\
	UCA0MCTL = UCBRS_2;																		\
	UCA0CTL1 = UCSWRST| UCSSEL_2;	/* SMCLK */												\
	P3SEL = BIT4 | BIT5;			/* P3.4, P3.5 = USCI_A0 TXD/RXD */						\
	UCA0CTL1 = UCSSEL_2;			/* release reset */

void COM_print(const char*, uint8_t);
void com_printf(const char * s);
void COM_TXHex(uint16_t);

#endif
