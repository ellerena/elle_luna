/***********************************************************************************

    Filename:     mrfi_link.c

    Description:  MRFI link implementation.

***********************************************************************************/

/***************************** INCLUDES *****************************/
#include "string.h"
#include "CONST_AP.h"
#include "mrfi_lnk.h"

#define TRUE			1
#define FALSE			0
#define HI_UINT16(a) (((uint16_t)(a) >> 8) & 0xFF)
#define LO_UINT16(a) ((uint16_t)(a) & 0xFF)

/************************ PRIVATE CONSTANTS ************************/
#define MRFI_LINK_PAN_ID				0x2007		// MRFI address definitions

// MRFI doesn't inherently support ACK - adding it at link level
#define MRFI_LINK_DATA					0x7E
#define MRFI_LINK_ACK					0x7F

/************************ PRIVATE VARIABLES ************************/
XDATA mrfiPacket_t pkt_in;
XDATA mrfiPacket_t pkt_out;
XDATA volatile uint8_t mrfiPktRdy;     // TRUE when a valid data packet is ready

/************************ PUBLIC VARIABLES ************************/
mrfiPacket_t * CODE pPktOut = &pkt_out;
mrfiPacket_t * CODE pPktIn = &pkt_in;

/************************ PUBLIC FUNCTIONS ************************/
/**
* @brief      Initialise the MRFI layer. Selects RF channel and addresses.
* @param      src - source address (16 bit)
* @param      dst - destination address (16 bit)
*/
void mrfiLinkInit(void)
{
#ifdef USB_SUSPEND_HOOKS    // Register USB hooks if necessary
    pFnSuspendEnterHook= MRFI_Sleep;
    pFnSuspendExitHook= linkRestore;
#endif

   /* Initialise the addresses */
	MRFI_P_DST_ADDR(pPktOut)[0] = LO_UINT16(MRFI_LINK_PAN_ID);
	MRFI_P_DST_ADDR(pPktOut)[1] = HI_UINT16(MRFI_LINK_PAN_ID);
	MRFI_P_DST_ADDR(pPktOut)[2] = LO_UINT16(DEV_ADDR_REM);
	MRFI_P_DST_ADDR(pPktOut)[3] = HI_UINT16(DEV_ADDR_REM);

	MRFI_P_SRC_ADDR(pPktOut)[0] = LO_UINT16(MRFI_LINK_PAN_ID);
	MRFI_P_SRC_ADDR(pPktOut)[1] = HI_UINT16(MRFI_LINK_PAN_ID);
	MRFI_P_SRC_ADDR(pPktOut)[2] = LO_UINT16(DEV_ADDR_LOC);
	MRFI_P_SRC_ADDR(pPktOut)[3] = HI_UINT16(DEV_ADDR_LOC);

	MRFI_P_PAYLOAD(pPktOut)[0] = F_MSG_TXT;
	MRFI_P_PAYLOAD(pPktOut)[1] = 'r';
	MRFI_P_PAYLOAD(pPktOut)[2] = 'f';
	MRFI_P_PAYLOAD(pPktOut)[3] = '2';
	MRFI_P_PAYLOAD(pPktOut)[4] = '5';
	MRFI_P_PAYLOAD(pPktOut)[5] = '0';
	MRFI_P_PAYLOAD(pPktOut)[6] = '0';
	MRFI_P_PAYLOAD(pPktOut)[7] = '\r';

	MRFI_SET_PAYLOAD_LEN(pPktOut, 0);
													/* Initialise MRFI link housekeeping data */
	mrfiPktRdy= FALSE;
													/* Initialise MRFI */
	MRFI_Init();
	MRFI_WakeUp();
	MRFI_SetLogicalChannel(MRFI_CHANNEL);
#ifndef CLT_TARGET
	mrfiLinkBroadcast(8);
#else
	MRFI_RxOn();
	MRFI_SetRxAddrFilter(MRFI_P_SRC_ADDR(pPktOut));
	MRFI_EnableRxAddrFilter();
#endif
}

/**
* @brief      Read data from the RX buffer
* @param      pBuf - buffer for storage of received data
* @return     Number of bytes received
*/
uint8_t mrfiLinkRecv(void)
{
	uint8_t n;

	n= mrfiPktRdy ? MRFI_GET_PAYLOAD_LEN(&pkt_in) : 0;
	mrfiPktRdy= FALSE;

	return n;
}

/**
 *	@brief	Simplest broadcasting.
 * */
void mrfiLinkBroadcast(uint16_t len)
{
	MRFI_SET_PAYLOAD_LEN(pPktOut, len);

	MRFI_Transmit(&pkt_out, MRFI_TX_TYPE_FORCED);
}


/**
* @brief       Returns true if RF data is ready
* @return      true if data is ready
*/
uint8_t mrfiLinkDataRdy(void)
{
	return mrfiPktRdy;
}

/**
* @brief       This function is called by the ISR of MRFI at RF receive and MUST
*              be included in all applications. It should be as short as possible
*              to avoid lengthy processing in the ISR.
*/
void MRFI_RxCompleteISR()
{
	MRFI_Receive(&pkt_in);

	if (MRFI_GET_PAYLOAD_LEN(&pkt_in)>1)
	{
		mrfiPktRdy= TRUE;
	}
}



