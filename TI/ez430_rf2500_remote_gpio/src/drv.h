/*
 * gpio.h
 *
 *  Created on: Oct 5, 2014
 *      Author: Edison
 */

#ifndef DRV_H_
#define DRV_H_

#include "msp430.h"
#include "stdint.h"

#define	P2RF2500			(0x1f)
#define	P4RF2500			(0x78)
#define	IN_PORT				(0x1ff)

#define	IN_MASK_RF2500		((P4RF2500<<8) | P2RF2500)

#define	ADC10CTL_ARM		(SREF_1 + ADC10SHT_3 + REFON + ADC10ON)

#define MACRO_GPIO_INIT 				/* (P1_2 already in BSP_Init */\
	P1REN	= BIT2;						/* P1_2 resistor enable */\
	P1OUT	= BIT0 | BIT1 | BIT2;		/* P1_0/1 OUTPUT, P1_2 PU */\
	P1IFG	= 0;						/* Clear pending interrupts */\
	P1IE	= BIT2;						/* P1_2 Interrupt Enabled */\
	P1IES	= BIT2;						/* P1_2 Interrupt on H-to-L transition */\
	P2DIR	= 0;						/* P2 is input */\
	P2OUT	= P2RF2500;					/* P2RF2500 pulled up */\
	P2REN	= P2RF2500;					/* P2RF2500 pu/pd resistor enabled (P2DIR assumed as 0) */\
	P2DIR	= 0;						/* P4 is input */\
	P4OUT	= P4RF2500;					/* P4RF2500 pulled up */\
	P4REN	= P4RF2500;					/* P4RF2500 pu/pd resistor enabled */

#define MACRO_TIMER_INIT				/* (already in BSP_Init) */\
	TACTL |= TACLR;						/* Set the TACLR */\
	TACTL = 0x0;						/* Clear all settings */\
	TACTL |= TASSEL_2;					/* Select the clk source to be - SMCLK (Sub-Main CLK) */

#define MACRO_ADC10_INIT	\
	ADC10CTL1 = INCH_11;									/* AVcc/2 */\
	ADC10CTL0 = ADC10CTL_ARM;	\
	BSP_Delay(4);											/* 4 usec delay (30 smclk cycles) */\

#define TURN_ON_LED(x)		P1DIR |= (x)
#define TURN_OFF_LED(x)		P1DIR &= ~(x)
#define TOGGLE_LED(x)		P1DIR ^= (x)


//void __attribute__((interrupt(PORT1_VECTOR))) i_user_button(void);
//__interrupt void i_user_button(void);

#endif /* DRV_H_ */
